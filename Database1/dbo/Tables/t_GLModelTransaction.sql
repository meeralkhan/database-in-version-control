﻿CREATE TABLE [dbo].[t_GLModelTransaction] (
    [OurBranchID]       VARCHAR (30)    NOT NULL,
    [RefNo]             VARCHAR (100)   NOT NULL,
    [AccountID]         VARCHAR (30)    NOT NULL,
    [VoucherID]         DECIMAL (10)    NOT NULL,
    [SerialID]          DECIMAL (10)    NOT NULL,
    [Date]              DATETIME        NOT NULL,
    [ValueDate]         DATETIME        NULL,
    [DescriptionID]     VARCHAR (50)    NULL,
    [Description]       VARCHAR (255)   NULL,
    [CurrencyID]        VARCHAR (30)    NULL,
    [Amount]            MONEY           NULL,
    [ForeignAmount]     MONEY           NULL,
    [ExchangeRate]      DECIMAL (18, 6) NULL,
    [IsCredit]          BIT             NULL,
    [TransactionType]   CHAR (2)        NULL,
    [TransactionMethod] CHAR (1)        NULL,
    [OperatorID]        VARCHAR (30)    NULL,
    [SupervisorID]      VARCHAR (30)    NULL,
    [Indicator]         CHAR (1)        NULL,
    [AdditionalData]    TEXT            NOT NULL,
    [ClearingType]      CHAR (1)        NOT NULL,
    [RegisterDate]      DATETIME        NULL
);

