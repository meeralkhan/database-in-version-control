﻿CREATE TABLE [dbo].[t_FeedbackAns] (
    [CreateBy]       NVARCHAR (30)  NOT NULL,
    [CreateTime]     DATETIME       NOT NULL,
    [CreateTerminal] NVARCHAR (30)  NOT NULL,
    [UpdateBy]       NVARCHAR (30)  NULL,
    [UpdateTime]     DATETIME       NULL,
    [UpdateTerminal] NVARCHAR (30)  NULL,
    [OurBranchID]    NVARCHAR (30)  NOT NULL,
    [SBPCode]        NVARCHAR (30)  NOT NULL,
    [FeedbackID]     DECIMAL (24)   NOT NULL,
    [SerialNo]       DECIMAL (24)   NOT NULL,
    [AccountID]      NVARCHAR (30)  NOT NULL,
    [Answer]         NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_t_FeedbackAns_1] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [SBPCode] ASC, [FeedbackID] ASC, [SerialNo] ASC, [AccountID] ASC)
);

