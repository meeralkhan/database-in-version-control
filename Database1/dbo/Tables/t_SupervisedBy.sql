﻿CREATE TABLE [dbo].[t_SupervisedBy] (
    [OurBranchID]         VARCHAR (30) NOT NULL,
    [AccountID]           VARCHAR (30) NOT NULL,
    [Date]                DATETIME     NULL,
    [ScrollNo]            NUMERIC (10) NULL,
    [SerialNo]            INT          NULL,
    [ChequeID]            VARCHAR (30) NULL,
    [ClearBalance]        MONEY        NULL,
    [Effects]             MONEY        NULL,
    [Limit]               MONEY        NULL,
    [TrxType]             CHAR (1)     NULL,
    [Amount]              MONEY        NULL,
    [Status]              CHAR (1)     NULL,
    [TransactionCategory] CHAR (1)     NULL,
    [SupervisorID]        VARCHAR (30) NOT NULL,
    [RemotePass]          BIT          NULL
);

