﻿CREATE TABLE [dbo].[t_RptSavProfit2] (
    [PayDate]        DATETIME        NOT NULL,
    [FromDate]       DATETIME        NOT NULL,
    [ToDate]         DATETIME        NOT NULL,
    [OurBranchID]    NVARCHAR (30)   NOT NULL,
    [AccountID]      NVARCHAR (30)   NOT NULL,
    [AccountName]    NVARCHAR (100)  NOT NULL,
    [ProductID]      NVARCHAR (30)   NOT NULL,
    [ProductDesc]    NVARCHAR (100)  NOT NULL,
    [CurrencyID]     NVARCHAR (30)   NOT NULL,
    [MeanRate]       DECIMAL (24, 6) NOT NULL,
    [AccAmount]      MONEY           NOT NULL,
    [ActualProfit]   MONEY           NOT NULL,
    [PayAmount]      MONEY           NOT NULL,
    [NetProfit]      MONEY           NOT NULL,
    [TaxAmount]      MONEY           NOT NULL,
    [CreateBy]       NVARCHAR (30)   NOT NULL,
    [CreateTime]     DATETIME        NOT NULL,
    [CreateTerminal] NVARCHAR (30)   NOT NULL
);

