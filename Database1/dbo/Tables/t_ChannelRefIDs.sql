﻿CREATE TABLE [dbo].[t_ChannelRefIDs] (
    [ChannelRefID] VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_t_ChannelRefID] PRIMARY KEY CLUSTERED ([ChannelRefID] ASC)
);

