﻿CREATE TABLE [dbo].[t_ATM_HOAccount] (
    [OurBranchID]    VARCHAR (30)  NOT NULL,
    [HOAccountID]    VARCHAR (30)  NOT NULL,
    [HOAccountTitle] VARCHAR (100) NOT NULL,
    [HOCurrencyID]   VARCHAR (30)  NOT NULL,
    [HOCurrencyName] VARCHAR (100) NOT NULL,
    [HOProductID]    VARCHAR (30)  NOT NULL,
    CONSTRAINT [PK_t_ATM_HOAccount] PRIMARY KEY CLUSTERED ([OurBranchID] ASC)
);

