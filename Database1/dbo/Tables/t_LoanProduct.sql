﻿CREATE TABLE [dbo].[t_LoanProduct] (
    [OurBranchID]           VARCHAR (30)  NULL,
    [ProductID]             VARCHAR (30)  NULL,
    [Description]           VARCHAR (100) NULL,
    [Type]                  VARCHAR (1)   NULL,
    [mFrqMonthly]           DATETIME      NULL,
    [mFrqQtrly]             DATETIME      NULL,
    [mFrqHalfYearly]        DATETIME      NULL,
    [mFrqYearly]            DATETIME      NULL,
    [AutoApplyOnDate]       CHAR (1)      NULL,
    [Catagory]              VARCHAR (3)   NULL,
    [ReceivableProdcutID]   VARCHAR (30)  NULL,
    [CapitalizedProductID]  VARCHAR (30)  NULL,
    [NominatedProductID]    VARCHAR (30)  NULL,
    [NominatedProductID1]   VARCHAR (30)  NULL,
    [NominatedProductID2]   VARCHAR (30)  NULL,
    [IsDeclareAmountPosted] BIT           NULL
);

