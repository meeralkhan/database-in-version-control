﻿CREATE TABLE [dbo].[t_FacilityProduct] (
    [OurBranchID]    NVARCHAR (30)  NOT NULL,
    [ClientID]       NVARCHAR (30)  NOT NULL,
    [FacilityID]     DECIMAL (24)   NOT NULL,
    [ProductID]      NVARCHAR (30)  NOT NULL,
    [Description]    NVARCHAR (100) NOT NULL,
    [CreateBy]       NVARCHAR (30)  NOT NULL,
    [CreateTime]     DATETIME       NOT NULL,
    [CreateTerminal] NVARCHAR (30)  NOT NULL,
    [LimitType]      NVARCHAR (20)  NULL,
    [Limit]          MONEY          NULL,
    [Status]         NVARCHAR (1)   NOT NULL,
    CONSTRAINT [PK_t_FacilityProduct] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [ClientID] ASC, [FacilityID] ASC, [ProductID] ASC)
);

