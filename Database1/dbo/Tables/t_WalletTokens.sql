﻿CREATE TABLE [dbo].[t_WalletTokens] (
    [OurBranchID] NVARCHAR (30)  NOT NULL,
    [AccountID]   NVARCHAR (30)  NOT NULL,
    [AccountName] NVARCHAR (100) NOT NULL,
    [Amount]      MONEY          NOT NULL,
    [Token]       VARCHAR (50)   NOT NULL,
    [CreateTime]  DATETIME       NOT NULL,
    [ExpireTime]  DATETIME       NOT NULL,
    CONSTRAINT [PK_t_WalletTokens] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [AccountID] ASC)
);

