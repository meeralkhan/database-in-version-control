﻿CREATE TABLE [dbo].[t_MoneyMarketAccountM] (
    [CreateBy]          NVARCHAR (30)  NOT NULL,
    [CreateTime]        DATETIME       NOT NULL,
    [CreateTerminal]    NVARCHAR (30)  NOT NULL,
    [UpdateBy]          NVARCHAR (30)  NULL,
    [UpdateTime]        DATETIME       NULL,
    [UpdateTerminal]    NVARCHAR (30)  NULL,
    [AuthStatus]        CHAR (1)       NOT NULL,
    [VerifyBy]          NVARCHAR (30)  NULL,
    [VerifyTime]        DATETIME       NULL,
    [VerifyTerminal]    NVARCHAR (30)  NULL,
    [VerifyStatus]      CHAR (1)       NOT NULL,
    [SuperviseBy]       NVARCHAR (30)  NULL,
    [SuperviseTime]     DATETIME       NULL,
    [SuperviseTerminal] NVARCHAR (30)  NULL,
    [frmName]           NVARCHAR (100) NOT NULL,
    [OurBranchID]       NVARCHAR (30)  NOT NULL,
    [Type]              CHAR (1)       NOT NULL,
    [PartyID]           NVARCHAR (30)  NOT NULL,
    [ClientID]          NVARCHAR (30)  NULL,
    [PartyDescription]  NVARCHAR (100) NULL,
    [Address]           NVARCHAR (500) NULL,
    [City]              NVARCHAR (30)  NULL,
    [Country]           NVARCHAR (30)  NULL,
    [ContactPerson]     NVARCHAR (30)  NULL,
    [EmailID]           NVARCHAR (100) NULL,
    [CounterPartyType]  NVARCHAR (30)  NULL,
    [BankType]          NVARCHAR (20)  NULL,
    [IsCentralBank]     INT            NULL,
    [IsGroupLimits]     SMALLINT       NULL,
    [CLLimit]           MONEY          NULL,
    [SpotLimit]         MONEY          NULL,
    [CBLimit]           MONEY          NULL,
    [RepoLimit]         MONEY          NULL,
    [FXLLimit]          MONEY          NULL,
    [ReverseRepoLimit]  MONEY          NULL,
    [FXBLimit]          MONEY          NULL,
    [ForwardLimit]      MONEY          NULL,
    [SettlementLimit]   MONEY          NULL,
    [SecurityLedgerNo]  NVARCHAR (30)  NULL,
    [AccrualPeriods]    NVARCHAR (10)  NULL,
    [CouponRate]        MONEY          NULL,
    [StartDate]         DATETIME       NULL,
    [DurationDD]        DECIMAL (24)   NULL,
    [DurationTT]        NVARCHAR (20)  NULL,
    [EndDate]           DATETIME       NULL,
    [NoOfDays]          DECIMAL (24)   NULL,
    [SecurityType]      CHAR (1)       NOT NULL,
    CONSTRAINT [PK_t_MoneyMarketAccountM] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [Type] ASC, [PartyID] ASC)
);

