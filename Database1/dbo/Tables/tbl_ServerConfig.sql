﻿CREATE TABLE [dbo].[tbl_ServerConfig] (
    [ID]              INT          IDENTITY (1, 1) NOT NULL,
    [PrimaryServerIP] VARCHAR (50) NOT NULL,
    [CreateTime]      DATETIME     NOT NULL,
    CONSTRAINT [PK_tbl_ServerConfig] PRIMARY KEY CLUSTERED ([ID] ASC)
);

