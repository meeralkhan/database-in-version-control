﻿CREATE TABLE [dbo].[t_QueuePermission] (
    [Id]        INT           IDENTITY (1, 1) NOT NULL,
    [ApiName]   NVARCHAR (50) NULL,
    [QueueName] NVARCHAR (50) NULL,
    [IsAllow]   BIT           NULL
);

