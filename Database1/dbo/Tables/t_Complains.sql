﻿CREATE TABLE [dbo].[t_Complains] (
    [CreateBy]       NVARCHAR (60)   NOT NULL,
    [CreateTime]     DATETIME        NOT NULL,
    [CreateTerminal] NVARCHAR (30)   NOT NULL,
    [UpdateBy]       NVARCHAR (60)   NULL,
    [UpdateTime]     DATETIME        NULL,
    [UpdateTerminal] NVARCHAR (30)   NULL,
    [OurBranchID]    NVARCHAR (30)   NOT NULL,
    [ComplainID]     INT             NOT NULL,
    [userid]         NVARCHAR (60)   NOT NULL,
    [ComplainDate]   DATETIME        NOT NULL,
    [ComplainType]   NVARCHAR (100)  NOT NULL,
    [Details]        NVARCHAR (1000) NOT NULL,
    [AssignTo]       VARCHAR (30)    NULL,
    [Status]         CHAR (1)        NOT NULL,
    CONSTRAINT [PK_t_Complains] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [ComplainID] ASC, [userid] ASC)
);

