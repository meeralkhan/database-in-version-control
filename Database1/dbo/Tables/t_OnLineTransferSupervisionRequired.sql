﻿CREATE TABLE [dbo].[t_OnLineTransferSupervisionRequired] (
    [ScrollNo]        INT             NOT NULL,
    [SerialNo]        INT             NOT NULL,
    [OurBranchID]     VARCHAR (30)    NOT NULL,
    [AccountID]       VARCHAR (30)    NOT NULL,
    [AccountName]     VARCHAR (100)   NOT NULL,
    [ProductID]       VARCHAR (30)    NOT NULL,
    [CurrencyID]      VARCHAR (30)    NOT NULL,
    [AccountType]     CHAR (1)        NOT NULL,
    [ValueDate]       DATETIME        NOT NULL,
    [wDate]           DATETIME        NOT NULL,
    [TrxType]         CHAR (1)        NOT NULL,
    [ChequeID]        VARCHAR (30)    NOT NULL,
    [ChequeDate]      DATETIME        NOT NULL,
    [Amount]          MONEY           NOT NULL,
    [ForeignAmount]   MONEY           NOT NULL,
    [ExchangeRate]    DECIMAL (18, 6) NOT NULL,
    [DescriptionID]   VARCHAR (30)    NOT NULL,
    [Description]     VARCHAR (255)   NOT NULL,
    [Supervision]     CHAR (1)        NOT NULL,
    [IsLocalCurrency] CHAR (1)        NOT NULL,
    [OperatorID]      VARCHAR (30)    NOT NULL,
    [SupervisorID]    VARCHAR (30)    NOT NULL,
    [SourceBranch]    VARCHAR (30)    NULL,
    [TargetBranch]    VARCHAR (30)    NULL,
    [Remarks]         TEXT            NULL,
    [RefNo]           VARCHAR (30)    NOT NULL,
    [IBCANo]          INT             NOT NULL,
    [S_AccountID]     VARCHAR (30)    NULL,
    [S_AccountType]   CHAR (1)        NULL,
    [MethodType]      VARCHAR (2)     NULL,
    [BranchID]        VARCHAR (30)    NULL,
    [SourceBranchID]  VARCHAR (30)    NULL,
    [TargetBranchID]  VARCHAR (30)    NULL,
    [AppWHTax]        BIT             NULL,
    [WHTaxAmount]     MONEY           NULL,
    [WHTaxMode]       CHAR (1)        NULL,
    [WHTaxAccountID]  VARCHAR (30)    NULL,
    [AdditionalData]  TEXT            NULL,
    [InstrumentType]  VARCHAR (10)    NOT NULL,
    [InstrumentNo]    VARCHAR (100)   NOT NULL,
    CONSTRAINT [PK_t_OnLineTransferSupervisionRequired] PRIMARY KEY CLUSTERED ([ScrollNo] ASC, [SerialNo] ASC, [OurBranchID] ASC)
);

