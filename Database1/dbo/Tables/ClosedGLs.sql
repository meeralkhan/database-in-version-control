﻿CREATE TABLE [dbo].[ClosedGLs] (
    [AccountID] NVARCHAR (30) NOT NULL,
    CONSTRAINT [PK_ClosedGLs] PRIMARY KEY CLUSTERED ([AccountID] ASC)
);

