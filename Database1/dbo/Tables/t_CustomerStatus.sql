﻿CREATE TABLE [dbo].[t_CustomerStatus] (
    [OurBranchID]          VARCHAR (30)  NOT NULL,
    [AccountID]            VARCHAR (30)  NOT NULL,
    [GLID]                 VARCHAR (30)  NULL,
    [ScrollNo]             NUMERIC (10)  NULL,
    [SerialNo]             INT           NULL,
    [AccountName]          VARCHAR (100) NULL,
    [Amount]               MONEY         NULL,
    [Rate]                 MONEY         NULL,
    [Status]               CHAR (1)      NULL,
    [TransactionType]      VARCHAR (2)   NULL,
    [IsOverDraft]          BIT           NOT NULL,
    [OperatorID]           VARCHAR (30)  NULL,
    [TellerMessage]        VARCHAR (255) NULL,
    [TellerNewMessage]     BIT           NOT NULL,
    [SupervisorMessage]    VARCHAR (255) NULL,
    [SupervisorNewMessage] BIT           NOT NULL,
    [frmName]              VARCHAR (100) NULL
);

