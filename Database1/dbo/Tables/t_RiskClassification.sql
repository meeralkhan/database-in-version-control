﻿CREATE TABLE [dbo].[t_RiskClassification] (
    [CreateBy]          NVARCHAR (30)  NOT NULL,
    [CreateTime]        DATETIME       NOT NULL,
    [CreateTerminal]    NVARCHAR (30)  NOT NULL,
    [UpdateBy]          NVARCHAR (30)  NULL,
    [UpdateTime]        DATETIME       NULL,
    [UpdateTerminal]    NVARCHAR (30)  NULL,
    [AuthStatus]        CHAR (1)       NOT NULL,
    [SuperviseBy]       NVARCHAR (30)  NULL,
    [SuperviseTime]     DATETIME       NULL,
    [SuperviseTerminal] NVARCHAR (30)  NULL,
    [OurBranchID]       NVARCHAR (30)  NOT NULL,
    [Code]              NVARCHAR (30)  NOT NULL,
    [Description]       NVARCHAR (100) NULL,
    [frmName]           VARCHAR (100)  NULL,
    [VerifyBy]          NVARCHAR (30)  NULL,
    [VerifyTime]        DATETIME       NULL,
    [VerifyTerminal]    NVARCHAR (30)  NULL,
    [VerifyStatus]      CHAR (1)       NOT NULL,
    [TextToView]        NVARCHAR (30)  NULL,
    [BackgroundColor]   NVARCHAR (10)  NULL,
    [TextColor]         NVARCHAR (10)  NULL,
    CONSTRAINT [PK_t_RiskClassification] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [Code] ASC)
);

