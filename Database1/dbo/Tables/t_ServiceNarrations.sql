﻿CREATE TABLE [dbo].[t_ServiceNarrations] (
    [CreateBy]          NVARCHAR (30)   NOT NULL,
    [CreateTime]        DATETIME        NOT NULL,
    [CreateTerminal]    NVARCHAR (30)   NOT NULL,
    [UpdateBy]          NVARCHAR (30)   NULL,
    [UpdateTime]        DATETIME        NULL,
    [UpdateTerminal]    NVARCHAR (30)   NULL,
    [AuthStatus]        CHAR (1)        NOT NULL,
    [SuperviseBy]       NVARCHAR (30)   NULL,
    [SuperviseTime]     DATETIME        NULL,
    [SuperviseTerminal] NVARCHAR (30)   NULL,
    [OurBranchID]       NVARCHAR (30)   NOT NULL,
    [NarrationCode]     NVARCHAR (100)  NOT NULL,
    [Description]       NVARCHAR (100)  NULL,
    [Rate]              NUMERIC (18, 6) NULL,
    [GLAccoutID]        NVARCHAR (100)  NULL,
    [Naration]          NVARCHAR (100)  NULL,
    [NarationID]        NVARCHAR (100)  NULL,
    CONSTRAINT [PK_t_ServiceNarrations] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [NarrationCode] ASC)
);

