﻿CREATE TABLE [dbo].[t_Cashbacks] (
    [ID]            DECIMAL (24)  IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [OurBranchID]   NVARCHAR (30) NOT NULL,
    [ProductID]     NVARCHAR (30) NOT NULL,
    [CampaignID]    NVARCHAR (30) NOT NULL,
    [ClientID]      NVARCHAR (30) NOT NULL,
    [PromoCode]     NVARCHAR (30) NOT NULL,
    [PromoClientID] NVARCHAR (30) NOT NULL,
    [PromoUseTime]  DATETIME      NOT NULL,
    CONSTRAINT [PK_t_Cashbacks] PRIMARY KEY CLUSTERED ([ID] ASC)
);

