﻿CREATE TABLE [dbo].[t_SystemEmails] (
    [CreateBy]          NVARCHAR (30)  NOT NULL,
    [CreateTime]        DATETIME       NOT NULL,
    [CreateTerminal]    NVARCHAR (30)  NOT NULL,
    [UpdateBy]          NVARCHAR (30)  NULL,
    [UpdateTime]        DATETIME       NULL,
    [UpdateTerminal]    NVARCHAR (30)  NULL,
    [AuthStatus]        CHAR (1)       NOT NULL,
    [SuperviseBy]       NVARCHAR (30)  NULL,
    [SuperviseTime]     DATETIME       NULL,
    [SuperviseTerminal] NVARCHAR (30)  NULL,
    [frmName]           NVARCHAR (100) NOT NULL,
    [OurBranchID]       NVARCHAR (30)  NOT NULL,
    [SID]               NVARCHAR (30)  NOT NULL,
    [EmailID]           NVARCHAR (30)  NULL,
    [Subject]           NVARCHAR (200) NULL,
    [EmailBody]         TEXT           NULL,
    [VerifyBy]          NVARCHAR (30)  NULL,
    [VerifyTime]        DATETIME       NULL,
    [VerifyTerminal]    NVARCHAR (30)  NULL,
    [VerifyStatus]      NVARCHAR (30)  NOT NULL,
    CONSTRAINT [PK_t_SystemEmails] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [SID] ASC)
);

