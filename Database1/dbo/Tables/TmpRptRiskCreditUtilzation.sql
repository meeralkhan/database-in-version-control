﻿CREATE TABLE [dbo].[TmpRptRiskCreditUtilzation] (
    [ClientID]               NVARCHAR (30)  NOT NULL,
    [Name]                   NVARCHAR (100) NULL,
    [PeakUtilization12M]     MONEY          NULL,
    [AvgDailyUtilization12M] MONEY          NULL,
    [PeakUtilizationYTD]     MONEY          NULL,
    [AvgDailyUtilizationYTD] MONEY          NULL,
    [RptTime]                DATETIME       NULL
);

