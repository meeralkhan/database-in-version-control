﻿CREATE TABLE [dbo].[t_VerifyApi] (
    [Id]             INT           IDENTITY (1, 1) NOT NULL,
    [ControllerName] NVARCHAR (50) NULL,
    [ActionName]     NVARCHAR (50) NULL,
    [IsVerify]       BIT           NULL,
    CONSTRAINT [PK_t_VerifyApi] PRIMARY KEY CLUSTERED ([Id] ASC)
);

