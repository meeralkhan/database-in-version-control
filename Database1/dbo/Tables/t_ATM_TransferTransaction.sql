﻿CREATE TABLE [dbo].[t_ATM_TransferTransaction] (
    [OurBranchID]       VARCHAR (30)    NOT NULL,
    [ScrollNo]          INT             NOT NULL,
    [SerialNo]          NUMERIC (10)    NOT NULL,
    [Refno]             VARCHAR (100)   NOT NULL,
    [WithdrawlBranchID] VARCHAR (30)    NOT NULL,
    [ATMID]             VARCHAR (30)    NOT NULL,
    [AccountID]         VARCHAR (30)    NOT NULL,
    [AccountName]       VARCHAR (100)   NOT NULL,
    [AccountType]       CHAR (1)        NOT NULL,
    [ProductID]         VARCHAR (30)    NOT NULL,
    [CurrencyID]        VARCHAR (30)    NOT NULL,
    [IsLocalCurrency]   BIT             NOT NULL,
    [ValueDate]         DATETIME        NOT NULL,
    [wDate]             DATETIME        NOT NULL,
    [PHXDate]           DATETIME        NOT NULL,
    [TrxType]           CHAR (1)        NOT NULL,
    [Supervision]       CHAR (1)        NOT NULL,
    [GLID]              VARCHAR (30)    NULL,
    [Amount]            NUMERIC (18, 6) NOT NULL,
    [ForeignAmount]     NUMERIC (18, 6) NULL,
    [ExchangeRate]      DECIMAL (16, 8) NULL,
    [DescriptionID]     VARCHAR (30)    NOT NULL,
    [Description]       VARCHAR (255)   NOT NULL,
    [BankCode]          VARCHAR (30)    NOT NULL,
    [Remarks]           VARCHAR (255)   NOT NULL,
    [OperatorID]        VARCHAR (30)    NOT NULL,
    [SupervisorID]      VARCHAR (30)    NOT NULL,
    [MerchantType]      VARCHAR (30)    NOT NULL,
    [CreditCardNumber]  VARCHAR (50)    NOT NULL,
    [CompanyNumber]     VARCHAR (50)    NULL,
    [STAN]              VARCHAR (30)    NULL,
    [ConvRate]          DECIMAL (15, 9) NULL,
    [AcqCountryCode]    VARCHAR (3)     NULL,
    [CurrCodeTran]      VARCHAR (3)     NULL,
    [CurrCodeSett]      VARCHAR (3)     NULL,
    [SettlmntAmount]    NUMERIC (18, 6) NULL,
    [ATMStatus]         CHAR (1)        NULL,
    [POSEntryMode]      VARCHAR (3)     NULL,
    [POSConditionCode]  VARCHAR (2)     NULL,
    [POSPINCaptCode]    VARCHAR (2)     NULL,
    [AcqInstID]         VARCHAR (15)    NULL,
    [RetRefNum]         VARCHAR (15)    NULL,
    [AuthIDResp]        VARCHAR (6)     NULL,
    [CardAccptID]       VARCHAR (15)    NULL,
    [CardAccptNameLoc]  VARCHAR (50)    NULL,
    [VISATrxID]         VARCHAR (15)    NULL,
    [ProcCode]          VARCHAR (50)    NULL,
    [TrxDesc]           VARCHAR (2)     NULL,
    [MCC]               VARCHAR (10)    NULL,
    [CAVV]              CHAR (1)        NULL,
    [ResponseCode]      VARCHAR (2)     NULL,
    [ForwardInstID]     VARCHAR (10)    NULL,
    [ClearingRefNo]     VARCHAR (100)   NULL,
    CONSTRAINT [PK_t_ATM_TransferTransaction] PRIMARY KEY CLUSTERED ([SerialNo] ASC, [Refno] ASC)
);


GO



create   TRIGGER [dbo].[tr_ATM_TransferTransaction_Update] ON [dbo].[t_ATM_TransferTransaction]     
FOR UPDATE    
AS     
     
 DECLARE @OurBranchID varchar(30)    
 DECLARE @AccountID varchar(30)    
 DECLARE @Amount money    
    
 IF (SELECT AccountType FROM Inserted)='C'     
 BEGIN       
  SELECT @AccountID=AccountID, @OurBranchID=OurBranchID,@Amount=Amount   FROM Inserted    
    
  IF (SELECT Supervision FROM Inserted)='R'     
   IF (SELECT TrxType FROM Inserted)='D'     
   BEGIN    
    UPDATE t_AccountBalance SET ClearBalance=(ClearBalance + @Amount)
	WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID    
   END
   if (SELECT TrxType FROM Inserted)='C'
   BEGIN
    UPDATE t_AccountBalance SET ClearBalance=(ClearBalance - @Amount)
	WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID    
   END
 END
GO


create   TRIGGER [dbo].[tr_ATM_TransferTransaction_Insert] ON [dbo].[t_ATM_TransferTransaction]     
FOR INSERT    
AS     
     
 DECLARE @OurBranchID varchar(30)    
 DECLARE @AccountID varchar(30)     
 DECLARE @ProductID varchar(30)    
 DECLARE @Amount money    
 
 DECLARE @STAN as varchar(30)
 DECLARE @PHXDate as datetime 
 DECLARE @wDate as datetime 
 DECLARE @ForeignAmount as money
 DECLARE @ExchangeRate as decimal(16,8)
 DECLARE @MerchantType as varchar(30)
 DECLARE @AcqCountryCode as varchar(3)
 DECLARE @ATMID as varchar(30)
 DECLARE @CurrCodeTran as varchar(3)
 DECLARE @CurrCodeSett as varchar(3)
 DECLARE @SettlmntAmount as money
 DECLARE @ProcCode as varchar(50)
 DECLARE @POSEntryMode as varchar(3)
 DECLARE @POSConditionCode as varchar(2)
 DECLARE @POSPINCaptCode as varchar(2)
 DECLARE @AcqInstID as varchar(30)  
 DECLARE @RetRefNum as varchar(15)
 DECLARE @CardAccptID as varchar(15)
 DECLARE @CardAccptNameLoc as varchar(50)
 DECLARE @Description       as varchar (255)   
 DECLARE @CAVV       as char (1)   
 DECLARE @DescriptionID as varchar(30)
 DECLARE @RefNo as varchar(100)
 DECLARE @ConvRate as decimal(15,9)
 DECLARE @ForwardInstID as varchar(10)
 DECLARE @VISATrxID as varchar(15)
 DECLARE @ResponseCode as varchar(2)
 DECLARE @AuthIDResp as varchar(6)
  SET @AuthIDResp = '0'
 
 DECLARE @NoOfCWTrxATMMonthly as decimal(24,0)
 DECLARE @NoOfBITrxATMMonthly as decimal(24,0)
 DECLARE @NoOfFreeCW decimal(24,0)
  set @NoOfFreeCW=0
  
 DECLARE @ClearBalance as money
 DECLARE @Limit as money
 DECLARE @FreezeAmount as money
 DECLARE @MinBalance as money
    
 IF (SELECT AccountType FROM Inserted)='C'     
 BEGIN       
  SELECT @OurBranchID=OurBranchID,@AccountID=AccountID,@ProductID=ProductID,@RefNo=RefNo,@STAN=STAN,@PHXDate=PHXDate,@wDate=wDate,@ForeignAmount=ForeignAmount,
  @ExchangeRate=ExchangeRate,@ConvRate=ConvRate,@Amount=Amount,@MerchantType=MerchantType,@AcqCountryCode=AcqCountryCode,@ForwardInstID=ForwardInstID,@ATMID=ATMID,
  @CurrCodeTran=CurrCodeTran,@CurrCodeSett=CurrCodeSett,@SettlmntAmount=SettlmntAmount,@ProcCode=ProcCode,@POSEntryMode=POSEntryMode,@POSConditionCode=POSConditionCode,
  @POSPINCaptCode=POSPINCaptCode,@AcqInstID=AcqInstID,@RetRefNum=RetRefNum,@AuthIDResp=AuthIDResp,@CardAccptID=CardAccptID,@CardAccptNameLoc=CardAccptNameLoc,
  @Description=ISNULL([Description],''),@CAVV=CAVV,@DescriptionID=DescriptionID
  FROM Inserted    
  
  IF (SELECT Supervision FROM Inserted)='C'     
   IF (SELECT TrxType FROM Inserted)='D'     
   BEGIN    
    UPDATE t_AccountBalance SET ClearBalance=(ClearBalance - @Amount)
	WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID    
   END    
   IF (SELECT TrxType FROM Inserted)='C'     
   BEGIN    
    UPDATE t_AccountBalance SET ClearBalance=(ClearBalance + @Amount)
    WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID    
   END    
   
   SELECT @ClearBalance=ISNULL(B.ClearBalance,0),@FreezeAmount=ISNULL(B.FreezeAmount,0),@MinBalance=ISNULL(P.MinBalance,0),@Limit=ISNULL(B.Limit,0)
   FROM t_AccountBalance B           
   INNER JOIN t_Products P ON B.OurBranchID = P.OurBranchID AND B.ProductID = P.ProductID
   WHERE (B.OurBranchID = @OurBranchID) and (B.AccountID = @AccountID)
   
   IF (@Amount > (@ClearBalance+@Limit-@FreezeAmount-@MinBalance))
   BEGIN
    INSERT INTO t_ForceDebitHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, 
    ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, AvailableBalance, AuthIDResp)
    VALUES ( @OurBranchID, @AccountID, @RefNo, @STAN, @PHXDate, @wDate, @Amount, @ForeignAmount, @ExchangeRate, @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID,
    @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, @AcqInstID, @RetRefNum, @CardAccptID, 
    @CardAccptNameLoc, @Description, @VISATrxID, @CAVV, @ResponseCode, @DescriptionID, (@ClearBalance+@Limit-@FreezeAmount-@MinBalance), @AuthIDResp )
   END
 END