﻿CREATE TABLE [dbo].[t_ChainLog] (
    [SerialNo]    DECIMAL (24)   IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Action]      NVARCHAR (10)  NOT NULL,
    [OurBranchID] NVARCHAR (30)  NOT NULL,
    [KeyID1]      NVARCHAR (30)  NOT NULL,
    [KeyID2]      NVARCHAR (30)  NULL,
    [KeyID3]      NVARCHAR (30)  NULL,
    [Description] TEXT           NOT NULL,
    [Completed]   TEXT           NOT NULL,
    [TrxID]       NVARCHAR (100) NOT NULL,
    [FrmName]     NVARCHAR (50)  NOT NULL,
    [UserID]      NVARCHAR (50)  NOT NULL,
    [DateTime]    DATETIME       NOT NULL,
    [Terminal]    NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_t_ChainLog] PRIMARY KEY CLUSTERED ([SerialNo] ASC)
);

