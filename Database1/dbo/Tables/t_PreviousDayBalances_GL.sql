﻿CREATE TABLE [dbo].[t_PreviousDayBalances_GL] (
    [CreateBy]              VARCHAR (30) NOT NULL,
    [CreateTime]            DATETIME     NOT NULL,
    [CreateTerminal]        VARCHAR (30) NOT NULL,
    [OurBranchID]           VARCHAR (30) NOT NULL,
    [WorkingDate]           DATETIME     NOT NULL,
    [AccountID]             VARCHAR (30) NOT NULL,
    [Description]           VARCHAR (50) NOT NULL,
    [AccountClass]          CHAR (1)     NOT NULL,
    [AccountType]           CHAR (1)     NOT NULL,
    [OpeningBalance]        MONEY        NULL,
    [Balance]               MONEY        NULL,
    [ForeignOpeningBalance] MONEY        NULL,
    [ForeignBalance]        MONEY        NULL,
    [TemporaryBalance]      MONEY        NULL,
    [ShadowBalance]         MONEY        NULL,
    CONSTRAINT [PK_t_PreviousDayBalances_GL] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [WorkingDate] ASC, [AccountID] ASC)
);

