﻿CREATE TABLE [dbo].[t_LinkedCNICs] (
    [SerialNo]       NUMERIC (18)  IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [AccountID]      NVARCHAR (30) NOT NULL,
    [CNICNo]         NVARCHAR (30) NOT NULL,
    [OTP]            NVARCHAR (30) NOT NULL,
    [CreateBy]       NVARCHAR (30) NOT NULL,
    [CreateTime]     DATETIME      NOT NULL,
    [CreateTerminal] NVARCHAR (30) NOT NULL,
    [IsUsed]         INT           NOT NULL,
    CONSTRAINT [PK_t_LinkedCNICs] PRIMARY KEY CLUSTERED ([SerialNo] ASC)
);

