﻿CREATE TABLE [dbo].[Quantum_BRF_FX] (
    [DEAL_NO]     VARCHAR (255) NULL,
    [DEAL_DT]     VARCHAR (255) NULL,
    [MATURE_DT]   VARCHAR (255) NULL,
    [CPARTYNAME]  VARCHAR (255) NULL,
    [CPTYPE]      VARCHAR (255) NULL,
    [CPTYPECODE]  VARCHAR (255) NULL,
    [BUYCCY]      VARCHAR (255) NULL,
    [SELLCCY]     VARCHAR (255) NULL,
    [BASE_CCY]    VARCHAR (255) NULL,
    [BASE_RATE]   VARCHAR (255) NULL,
    [INT_RATE]    VARCHAR (255) NULL,
    [BUYAMOUNT]   VARCHAR (255) NULL,
    [SELLAMOUNT]  VARCHAR (255) NULL,
    [TRANS_TYPE]  VARCHAR (255) NULL,
    [SECNAME]     VARCHAR (255) NULL,
    [IN_USE]      VARCHAR (255) NULL,
    [COUNTRY]     VARCHAR (255) NULL,
    [COUNTRYCODE] VARCHAR (255) NULL,
    [LOCCODE]     VARCHAR (255) NULL,
    [LOCNAME]     VARCHAR (255) NULL,
    [CreateDate]  DATETIME      NULL,
    [ToolRunDate] DATETIME      NULL
);

