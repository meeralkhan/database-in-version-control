﻿CREATE TABLE [dbo].[tbl_GLOwnerTransferLog] (
    [AccountID]     NVARCHAR (50) NULL,
    [OurBranchID]   VARCHAR (50)  NULL,
    [FromGLOwnerID] VARCHAR (50)  NULL,
    [ToGLOwnerID]   VARCHAR (50)  NULL,
    [Status]        VARCHAR (50)  NULL,
    [TransferDate]  DATETIME      NULL,
    [AuthorizeDate] DATETIME      NULL,
    [LogID]         INT           NULL
);

