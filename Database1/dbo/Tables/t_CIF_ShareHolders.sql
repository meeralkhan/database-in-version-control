﻿CREATE TABLE [dbo].[t_CIF_ShareHolders] (
    [OurBranchID]   NVARCHAR (30) NOT NULL,
    [ClientID]      NVARCHAR (30) NOT NULL,
    [ShareHolderID] NVARCHAR (30) NOT NULL,
    CONSTRAINT [PK_t_CIF_ShareHolders] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [ClientID] ASC, [ShareHolderID] ASC)
);

