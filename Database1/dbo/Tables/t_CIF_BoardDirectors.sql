﻿CREATE TABLE [dbo].[t_CIF_BoardDirectors] (
    [OurBranchID]     NVARCHAR (30) NOT NULL,
    [ClientID]        NVARCHAR (30) NOT NULL,
    [BoardDirectorID] NVARCHAR (30) NOT NULL,
    CONSTRAINT [PK_t_CIF_BoardDirectors] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [ClientID] ASC, [BoardDirectorID] ASC)
);

