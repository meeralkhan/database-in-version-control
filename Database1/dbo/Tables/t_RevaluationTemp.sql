﻿CREATE TABLE [dbo].[t_RevaluationTemp] (
    [CreateBy]       VARCHAR (30)    NOT NULL,
    [CreateTime]     DATETIME        NOT NULL,
    [CreateTerminal] VARCHAR (30)    NOT NULL,
    [OurBranchID]    VARCHAR (30)    NOT NULL,
    [wDate]          DATETIME        NOT NULL,
    [AccountID]      VARCHAR (30)    NOT NULL,
    [ProductID]      VARCHAR (30)    NOT NULL,
    [AccountType]    CHAR (1)        NOT NULL,
    [AccountName]    VARCHAR (200)   NULL,
    [CurrencyID]     VARCHAR (30)    NOT NULL,
    [Amount]         MONEY           NOT NULL,
    [LocEq]          MONEY           NOT NULL,
    [ExRate]         DECIMAL (18, 6) NOT NULL,
    [RevAmount]      MONEY           NOT NULL,
    [PnL]            MONEY           NOT NULL,
    [TrxType]        CHAR (1)        NOT NULL,
    [GLControl]      VARCHAR (30)    NOT NULL,
    CONSTRAINT [PK_t_RevaluationTemp] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [wDate] ASC, [AccountID] ASC, [ProductID] ASC)
);

