﻿CREATE TABLE [dbo].[ISOCountries] (
    [CountryCode] NVARCHAR (3)   NOT NULL,
    [CountryName] NVARCHAR (100) NULL,
    CONSTRAINT [PK_ISOCountries] PRIMARY KEY CLUSTERED ([CountryCode] ASC)
);

