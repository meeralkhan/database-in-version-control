﻿CREATE TABLE [dbo].[t_JurAuthorities] (
    [CreateBy]          NVARCHAR (30)  NOT NULL,
    [CreateTime]        DATETIME       NOT NULL,
    [CreateTerminal]    NVARCHAR (30)  NOT NULL,
    [UpdateBy]          NVARCHAR (30)  NULL,
    [UpdateTime]        DATETIME       NULL,
    [UpdateTerminal]    NVARCHAR (30)  NULL,
    [AuthStatus]        CHAR (1)       NOT NULL,
    [VerifyBy]          NVARCHAR (30)  NULL,
    [VerifyTime]        DATETIME       NULL,
    [VerifyTerminal]    NVARCHAR (30)  NULL,
    [VerifyStatus]      CHAR (1)       NOT NULL,
    [SuperviseBy]       NVARCHAR (30)  NULL,
    [SuperviseTime]     DATETIME       NULL,
    [SuperviseTerminal] NVARCHAR (30)  NULL,
    [frmName]           NVARCHAR (100) NOT NULL,
    [OurBranchID]       NVARCHAR (30)  NOT NULL,
    [JurEmiratesID]     NVARCHAR (30)  NOT NULL,
    [JurTypeID]         NVARCHAR (30)  NOT NULL,
    [JurAuthorityID]    NVARCHAR (30)  NOT NULL,
    [JurAuthorityDesc]  NVARCHAR (100) NULL,
    CONSTRAINT [PK_t_JurAuthorities] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [JurEmiratesID] ASC, [JurTypeID] ASC, [JurAuthorityID] ASC)
);

