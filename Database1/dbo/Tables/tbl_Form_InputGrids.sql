﻿CREATE TABLE [dbo].[tbl_Form_InputGrids] (
    [frmName]        VARCHAR (30)  NOT NULL,
    [frmToLink]      VARCHAR (30)  NOT NULL,
    [TabId]          VARCHAR (30)  NULL,
    [CreateBy]       VARCHAR (30)  NOT NULL,
    [CreateTime]     DATETIME      NOT NULL,
    [CreateTerminal] VARCHAR (30)  NOT NULL,
    [UpdateBy]       VARCHAR (30)  NULL,
    [UpdateTime]     DATETIME      NULL,
    [UpdateTerminal] VARCHAR (30)  NULL,
    [PortletReq]     VARCHAR (10)  NOT NULL,
    [PortletTitle]   VARCHAR (100) NOT NULL,
    [PortletIcon]    VARCHAR (30)  NOT NULL,
    [PortletColor]   VARCHAR (30)  NOT NULL,
    CONSTRAINT [PK_tbl_Form_InputGrids] PRIMARY KEY CLUSTERED ([frmName] ASC, [frmToLink] ASC)
);

