﻿CREATE TABLE [dbo].[t_CIF_AuthSignatory] (
    [OurBranchID]           NVARCHAR (30) NOT NULL,
    [ClientID]              NVARCHAR (30) NOT NULL,
    [AuthorizedSignatoryID] NVARCHAR (30) NOT NULL,
    CONSTRAINT [PK_t_CIF_AuthSignatory] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [ClientID] ASC, [AuthorizedSignatoryID] ASC)
);

