﻿CREATE TABLE [dbo].[t_GL] (
    [CreateBy]              NVARCHAR (30)  NOT NULL,
    [CreateTime]            DATETIME       NOT NULL,
    [CreateTerminal]        NVARCHAR (30)  NOT NULL,
    [UpdateBy]              NVARCHAR (30)  NULL,
    [UpdateTime]            DATETIME       NULL,
    [UpdateTerminal]        NVARCHAR (30)  NULL,
    [AuthStatus]            CHAR (1)       NOT NULL,
    [SuperviseBy]           NVARCHAR (30)  NULL,
    [SuperviseTime]         DATETIME       NULL,
    [SuperviseTerminal]     NVARCHAR (30)  NULL,
    [OurBranchID]           NVARCHAR (30)  NOT NULL,
    [AccountID]             NVARCHAR (30)  NOT NULL,
    [PrintID]               NVARCHAR (60)  NULL,
    [CategoryID]            NVARCHAR (30)  NULL,
    [Description]           NVARCHAR (100) NULL,
    [AccountClass]          NVARCHAR (30)  NULL,
    [AccountType]           NVARCHAR (30)  NULL,
    [CurrencyID]            NVARCHAR (30)  NULL,
    [IsPosting]             INT            NULL,
    [IsContigent]           INT            NULL,
    [OpeningBalance]        MONEY          NULL,
    [Balance]               MONEY          NULL,
    [ForeignOpeningBalance] MONEY          NULL,
    [ForeignBalance]        MONEY          NULL,
    [TemporaryBalance]      MONEY          NULL,
    [Thru]                  NVARCHAR (30)  NULL,
    [Notes]                 TEXT           NULL,
    [frmName]               VARCHAR (100)  NULL,
    [ShadowBalance]         MONEY          NULL,
    [VerifyBy]              NVARCHAR (30)  NULL,
    [VerifyTime]            DATETIME       NULL,
    [VerifyTerminal]        NVARCHAR (30)  NULL,
    [VerifyStatus]          CHAR (1)       NOT NULL,
    [AvgBalProcessing]      INT            NULL,
    [RevaluationProcess]    INT            NULL,
    [IsReconcile]           INT            NULL,
    [GLOwnerID]             NVARCHAR (30)  NULL,
    [GLFSRollUps]           NVARCHAR (30)  NULL,
    [GLSubGroups]           NVARCHAR (30)  NULL,
    [IsMetaDataIncluded]    INT            NULL,
    [isExpenseMgmt]         INT            NULL,
    [CostTypesSPOID]        NVARCHAR (30)  NULL,
    CONSTRAINT [PK_t_GL] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [AccountID] ASC)
);


GO
create Trigger [dbo].[tr_InsertUpdateGLAccount]  on [dbo].[t_GL]
    AFTER INSERT, UPDATE
    as
    Begin
    declare @AccountID nvarchar(50)= ''
    declare @OurBranchID nvarchar(50)= ''
    select @AccountID = AccountID,@OurBranchID = OurBranchID from inserted
    IF NOT UPDATE(OpeningBalance) AND NOT UPDATE(Balance) AND NOT UPDATE(ForeignBalance) AND NOT UPDATE(ForeignOpeningBalance) AND NOT UPDATE(TemporaryBalance) AND NOT UPDATE(ShadowBalance)
    BEGIN
        insert into t_gl_Logs ([CreateBy],[CreateTime],[CreateTerminal],[UpdateBy],[UpdateTime],[UpdateTerminal],[AuthStatus],[SuperviseBy],[SuperviseTime],
        [SuperviseTerminal],[OurBranchID],[AccountID],[PrintID],[CategoryID],[Description],[AccountClass],[AccountType],[CurrencyID],[IsPosting],[IsContigent],
        [OpeningBalance],[Balance],[ForeignOpeningBalance],[ForeignBalance],[TemporaryBalance],[Thru],[Notes],[frmName],[ShadowBalance],[VerifyBy],
        [VerifyTime],[VerifyTerminal],[VerifyStatus],[AvgBalProcessing],[RevaluationProcess],[IsReconcile],GLOwnerID,GLFSRollUps,GLSubGroups,IsMetaDataIncluded,isExpenseMgmt) Select CreateBy,CreateTime,CreateTerminal,UpdateBy,
        UpdateTime,UpdateTerminal,AuthStatus,SuperviseBy,SuperviseTime,SuperviseTerminal,OurBranchID,AccountID,PrintID,CategoryID,Description,AccountClass,
        AccountType,CurrencyID,IsPosting,IsContigent,OpeningBalance,Balance,ForeignOpeningBalance,ForeignBalance,TemporaryBalance,Thru,Notes,frmName,ShadowBalance,
        VerifyBy,VerifyTime,VerifyTerminal,VerifyStatus,AvgBalProcessing,RevaluationProcess,IsReconcile,GLOwnerID,GLFSRollUps,GLSubGroups,IsMetaDataIncluded,isExpenseMgmt From t_GL where AccountID = @AccountID and OurBranchID = @OurBranchID
    END
END