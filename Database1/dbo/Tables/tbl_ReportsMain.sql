﻿CREATE TABLE [dbo].[tbl_ReportsMain] (
    [ReportId]  VARCHAR (30)  NOT NULL,
    [PkTable]   VARCHAR (50)  NOT NULL,
    [Alias]     VARCHAR (10)  NULL,
    [Columns]   VARCHAR (MAX) NOT NULL,
    [Condition] VARCHAR (MAX) NOT NULL,
    [OrderBy]   VARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_tbl_ReportsMain] PRIMARY KEY CLUSTERED ([ReportId] ASC)
);

