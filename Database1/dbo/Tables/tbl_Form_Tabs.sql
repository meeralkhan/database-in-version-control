﻿CREATE TABLE [dbo].[tbl_Form_Tabs] (
    [frmName] VARCHAR (50)  NOT NULL,
    [TabId]   VARCHAR (50)  NOT NULL,
    [TabIcon] VARCHAR (30)  NOT NULL,
    [TitleEN] VARCHAR (100) NOT NULL,
    [TitleAR] VARCHAR (100) NOT NULL,
    [TitleUR] VARCHAR (100) NOT NULL,
    [OrderNo] INT           NOT NULL,
    CONSTRAINT [PK_tbl_Form_Tabs] PRIMARY KEY CLUSTERED ([frmName] ASC, [TabId] ASC)
);

