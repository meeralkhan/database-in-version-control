﻿CREATE TABLE [dbo].[t_AuthorizationRemarks] (
    [ID]          INT            IDENTITY (1, 1) NOT NULL,
    [UserName]    NVARCHAR (50)  NOT NULL,
    [Date]        DATETIME       NULL,
    [OurBranchID] NVARCHAR (30)  NOT NULL,
    [TableName]   NVARCHAR (30)  NOT NULL,
    [OldValue]    NVARCHAR (MAX) NOT NULL,
    [NewValue]    NVARCHAR (MAX) NOT NULL,
    [Remarks]     NVARCHAR (200) NOT NULL,
    [IsAuthorize] INT            NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

