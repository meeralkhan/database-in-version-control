﻿CREATE TABLE [dbo].[t_TDRRolloverFailed] (
    [SerialNo]         DECIMAL (24)    IDENTITY (1, 1) NOT NULL,
    [wDate]            DATETIME        NOT NULL,
    [OurBranchID]      NVARCHAR (30)   NOT NULL,
    [ReceiptID]        NVARCHAR (30)   NOT NULL,
    [TransferScrollNo] DECIMAL (24)    NOT NULL,
    [ProductID]        NVARCHAR (30)   NOT NULL,
    [AccountID]        NVARCHAR (30)   NOT NULL,
    [IsUnderLien]      INT             NOT NULL,
    [TotalLienAmount]  DECIMAL (21, 3) NOT NULL,
    [CreateBy]         NVARCHAR (30)   NOT NULL,
    [CreateTime]       DATETIME        NOT NULL,
    [CreateTerminal]   NVARCHAR (30)   NOT NULL,
    CONSTRAINT [PK_t_TDRRolloverFailed] PRIMARY KEY CLUSTERED ([SerialNo] ASC)
);

