﻿CREATE TABLE [dbo].[InterBranchDump] (
    [UniqueID]          NVARCHAR (450)  NOT NULL,
    [BranchID]          NVARCHAR (30)   NOT NULL,
    [AccountType]       NVARCHAR (30)   NULL,
    [TrxType]           NVARCHAR (50)   NOT NULL,
    [wDate]             DATETIME        NOT NULL,
    [CurrencyID]        NVARCHAR (30)   NOT NULL,
    [Amount]            DECIMAL (21, 3) NOT NULL,
    [ExchangeRate]      DECIMAL (18, 6) NOT NULL,
    [LocalEq]           DECIMAL (21, 3) NOT NULL,
    [NarrationID]       NVARCHAR (30)   NOT NULL,
    [TrxMethod]         INT             NOT NULL,
    [SystemDateTime]    DATETIME        NOT NULL,
    [TransitAccountID]  NVARCHAR (30)   NULL,
    [MismatchAccountID] NVARCHAR (30)   NULL,
    [ChannelRefId]      NVARCHAR (MAX)  NOT NULL,
    [TrxMemo]           NVARCHAR (MAX)  NULL,
    [TrxNarrationId]    NVARCHAR (MAX)  NULL,
    CONSTRAINT [PK_InterBranchDump] PRIMARY KEY CLUSTERED ([UniqueID] ASC)
);

