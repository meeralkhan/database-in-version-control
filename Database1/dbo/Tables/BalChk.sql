﻿CREATE TABLE [dbo].[BalChk] (
    [SerialNo]         DECIMAL (24)    IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [OurBranchID]      NVARCHAR (30)   NOT NULL,
    [AccountID]        NVARCHAR (30)   NOT NULL,
    [ProductID]        NVARCHAR (30)   NOT NULL,
    [Name]             NVARCHAR (100)  NOT NULL,
    [LastUpdateTime]   DATETIME        NOT NULL,
    [ClearBalance]     DECIMAL (21, 3) NOT NULL,
    [Limit]            DECIMAL (21, 3) NOT NULL,
    [Effects]          DECIMAL (21, 3) NOT NULL,
    [Shadow]           DECIMAL (21, 3) NOT NULL,
    [FreezeAmt]        DECIMAL (21, 3) NOT NULL,
    [MinBalance]       DECIMAL (21, 3) NOT NULL,
    [TotalBalance]     DECIMAL (21, 3) NOT NULL,
    [AvailableBalance] DECIMAL (21, 3) NOT NULL,
    CONSTRAINT [PK_BalChk] PRIMARY KEY CLUSTERED ([SerialNo] ASC)
);

