﻿CREATE TABLE [dbo].[t_ATM_TimeLog] (
    [OurBranchID]       VARCHAR (30)  NOT NULL,
    [wDate]             DATETIME      NOT NULL,
    [Activity]          VARCHAR (100) NOT NULL,
    [OperatorID]        VARCHAR (30)  NOT NULL,
    [ActivityStartTime] VARCHAR (30)  NOT NULL,
    [ActivityEndTime]   VARCHAR (30)  NOT NULL,
    [ActivityTime]      VARCHAR (30)  NOT NULL
);

