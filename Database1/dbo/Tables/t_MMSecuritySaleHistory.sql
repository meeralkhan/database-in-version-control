﻿CREATE TABLE [dbo].[t_MMSecuritySaleHistory] (
    [OurBranchID]   VARCHAR (30) NOT NULL,
    [DealNo]        VARCHAR (30) NOT NULL,
    [SerialNo]      INT          NOT NULL,
    [P_DealNo]      VARCHAR (30) NOT NULL,
    [S_Amount]      MONEY        NOT NULL,
    [SecurityID]    VARCHAR (30) NOT NULL,
    [Status]        CHAR (1)     NOT NULL,
    [S_IntRecv]     MONEY        NOT NULL,
    [S_PreRecvPaid] MONEY        NOT NULL,
    [P_IntPaid]     MONEY        NOT NULL,
    [P_PreRecvPaid] MONEY        NOT NULL,
    [IntRecvd]      MONEY        NOT NULL,
    [IsPledge]      CHAR (1)     NOT NULL,
    [P_AmtExp]      MONEY        NOT NULL
);

