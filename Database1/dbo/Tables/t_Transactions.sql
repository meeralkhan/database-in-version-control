﻿CREATE TABLE [dbo].[t_Transactions] (
    [OurBranchID]          NVARCHAR (30)   NOT NULL,
    [ScrollNo]             NUMERIC (18)    NOT NULL,
    [SerialNo]             INT             NOT NULL,
    [RefNo]                NVARCHAR (100)  NOT NULL,
    [wDate]                DATETIME        NOT NULL,
    [AccountType]          NVARCHAR (30)   NOT NULL,
    [DocumentType]         NVARCHAR (30)   NOT NULL,
    [AccountID]            NVARCHAR (30)   NOT NULL,
    [AccountName]          NVARCHAR (200)  NULL,
    [ProductID]            NVARCHAR (50)   NOT NULL,
    [CurrencyID]           NVARCHAR (50)   NULL,
    [ValueDate]            DATETIME        NOT NULL,
    [TrxType]              CHAR (1)        NOT NULL,
    [ChequeID]             NVARCHAR (30)   NOT NULL,
    [ChequeDate]           DATETIME        NULL,
    [Amount]               MONEY           NOT NULL,
    [ForeignAmount]        MONEY           NOT NULL,
    [ExchangeRate]         DECIMAL (16, 8) NULL,
    [ProfitLoss]           MONEY           NOT NULL,
    [MeanRate]             MONEY           NOT NULL,
    [DescriptionID]        NVARCHAR (50)   NOT NULL,
    [Description]          NVARCHAR (500)  NULL,
    [BankCode]             NVARCHAR (50)   NOT NULL,
    [BranchCode]           NVARCHAR (50)   NOT NULL,
    [TheirAccount]         VARCHAR (30)    NULL,
    [ExtraDetails]         VARCHAR (50)    NULL,
    [ChequeDigit]          CHAR (1)        NULL,
    [VoucherCode]          CHAR (4)        NULL,
    [ReturnCode]           CHAR (4)        NULL,
    [DrawerOrPayee]        VARCHAR (50)    NULL,
    [TrxPrinted]           BIT             NOT NULL,
    [GLID]                 NVARCHAR (30)   NULL,
    [Status]               CHAR (10)       NOT NULL,
    [IsLocalCurrency]      BIT             NOT NULL,
    [OperatorID]           VARCHAR (30)    NOT NULL,
    [SupervisorID]         VARCHAR (30)    NOT NULL,
    [UtilityNumber]        VARCHAR (50)    NULL,
    [AdditionalData]       TEXT            NULL,
    [IsMainTrx]            SMALLINT        NOT NULL,
    [DocType]              CHAR (3)        NOT NULL,
    [GLVoucherID]          DECIMAL (24)    NOT NULL,
    [AppWHTax]             SMALLINT        NOT NULL,
    [WHTaxAmount]          MONEY           NOT NULL,
    [WHTaxMode]            CHAR (1)        NOT NULL,
    [WHTScrollNo]          INT             NOT NULL,
    [SourceBranch]         VARCHAR (30)    NOT NULL,
    [TargetBranch]         VARCHAR (30)    NOT NULL,
    [ClearingType]         CHAR (1)        NOT NULL,
    [RegisterDate]         DATETIME        NULL,
    [ChqStatus]            CHAR (1)        NULL,
    [ChqSuperviseBy]       NVARCHAR (30)   NULL,
    [ChqSuperviseTime]     DATETIME        NULL,
    [ChqSuperviseTerminal] NVARCHAR (30)   NULL,
    [Remarks]              TEXT            NOT NULL,
    [MerchantType]         VARCHAR (30)    NOT NULL,
    [CreditCardNumber]     VARCHAR (30)    NOT NULL,
    [STAN]                 VARCHAR (30)    NOT NULL,
    [CompanyNumber]        VARCHAR (50)    NOT NULL,
    [ChannelId]            NVARCHAR (30)   NOT NULL,
    [VirtualAccountID]     NVARCHAR (30)   NOT NULL,
    [VirtualAccountTitle]  NVARCHAR (100)  NOT NULL,
    [ChannelRefID]         VARCHAR (200)   NULL,
    [ConvRate]             DECIMAL (15, 9) NULL,
    [CurrCodeTran]         VARCHAR (3)     NULL,
    [AcqCountryCode]       VARCHAR (3)     NULL,
    [CurrCodeSett]         VARCHAR (3)     NULL,
    [SettlmntAmount]       MONEY           NULL,
    [ATMStatus]            CHAR (1)        NULL,
    [POSEntryMode]         VARCHAR (3)     NULL,
    [POSConditionCode]     VARCHAR (2)     NULL,
    [POSPINCaptCode]       VARCHAR (2)     NULL,
    [AcqInstID]            VARCHAR (15)    NULL,
    [RetRefNum]            VARCHAR (15)    NULL,
    [AuthIDResp]           VARCHAR (6)     NULL,
    [CardAccptID]          VARCHAR (15)    NULL,
    [CardAccptNameLoc]     VARCHAR (50)    NULL,
    [VISATrxID]            VARCHAR (15)    NULL,
    [ProcCode]             VARCHAR (50)    NULL,
    [TrxTimeStamp]         DATETIME        NOT NULL,
    [TrxDesc]              NVARCHAR (2)    NULL,
    [MCC]                  VARCHAR (10)    NULL,
    [CAVV]                 CHAR (1)        NULL,
    [ResponseCode]         VARCHAR (2)     NULL,
    [ForwardInstID]        VARCHAR (10)    NULL,
    [PrevDayEntry]         INT             NULL,
    [ClearingRefNo]        VARCHAR (100)   NULL,
    [ChargeID]             NVARCHAR (30)   NULL,
    [VatID]                NVARCHAR (30)   NULL,
    [Percent]              MONEY           NULL,
    [RejectRemarks]        NVARCHAR (200)  NULL,
    [PrevYearEntry]        INT             NULL,
    [TClientId]            NVARCHAR (30)   NULL,
    [TAccountid]           NVARCHAR (30)   NULL,
    [TProductId]           NVARCHAR (30)   NULL,
    [CostCenterID]         NVARCHAR (30)   NULL,
    [GLVendorID]           NVARCHAR (30)   NULL,
    [memo2]                NVARCHAR (250)  NULL,
    [VatReferenceID]       NVARCHAR (30)   NULL,
    CONSTRAINT [PK_t_Transactions] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [ScrollNo] ASC, [SerialNo] ASC, [wDate] ASC, [DocumentType] ASC)
);


GO

 CREATE TRIGGER [dbo].[tr_Transactions] ON [dbo].[t_Transactions]              
AFTER INSERT              
AS              
BEGIN            
--CashBack            
   declare @OurBranchID nvarchar(30)              
   declare @AccountID nvarchar(30)              
   declare @TrType nvarchar(1)              
   declare @wDate datetime              
   declare @AccountName nvarchar(100)              
   declare @ProductID nvarchar(30)              
   declare @CurrencyID nvarchar(30)              
   declare @Amount decimal(21,3)              
   declare @ForeignAmount decimal(21,3)              
   declare @ExchangeRate decimal(18,6)              
   declare @DescriptionID nvarchar(30)              
   declare @ChannelRefID nvarchar(100)              
   declare @TrxTimeStamp datetime              
   declare @CashbackPercent money            
   declare @CcyRounding int            
   declare @CashbackAmount money            
   declare @BalanceAmount money            
   declare @BeneficiaryName varchar(50)          
            
   select @OurBranchID=OurBranchID, @AccountID=AccountID, @TrType=TrxType, @wDate=wDate, @AccountName=AccountName,            
   @ProductID=ProductID, @CurrencyID=CurrencyID, @Amount=Amount, @ForeignAmount=ForeignAmount, @ExchangeRate=ExchangeRate,            
   @DescriptionID=DescriptionID, @ChannelRefID = ChannelRefID, @TrxTimeStamp = TrxTimeStamp            
   from inserted            
            
   if exists(select CriteriaID from t_CashbackCriteria where OurBranchID = @OurBranchID and ProductID = @ProductID and DescriptionID = @DescriptionID)            
   begin            
                  
  select @CashbackPercent = Cashback from t_CashbackCriteria            
  where OurBranchID = @OurBranchID and ProductID = @ProductID and DescriptionID = @DescriptionID            
            
  if (@CurrencyID = 'AED')            
  begin            
  select @CcyRounding = 2;            
 set @CashbackAmount = ROUND(@Amount * @CashbackPercent/100 , @CcyRounding);            
  end            
  else            
  begin            
  select @CcyRounding = ISNULL(CRRounding,2) from t_Currencies where OurBranchID = @OurBranchID and CurrencyID = @CurrencyID;            
 set @CashbackAmount = ROUND(@ForeignAmount * @CashbackPercent/100 , @CcyRounding);            
  end            
             
  insert into t_Cashback321 (OurBranchID, AccountID, AccountName, ProductID, CurrencyID, TrType, wDate, ForeignAmount,             
  ExchangeRate, Amount, CashbackPercent, CashbackAmount, DescriptionID, ChannelRefID, TrxTimeStamp, CreateBy, CreateTime, IsPaid)            
  values (@OurBranchID, @AccountID, @AccountName, @ProductID, @CurrencyID, @TrType, @wDate, @ForeignAmount,             
  @ExchangeRate, @Amount, @CashbackPercent, @CashbackAmount, @DescriptionID, @ChannelRefID, @TrxTimeStamp, 'SYSTEM', GETDATE(), 0)            
                  
    end              
            
    --Charity            
                
   SET NOCOUNT ON            
            
   set @OurBranchID =''            
   set @AccountID =''            
   set @TrType =''            
   declare @TrxType nvarchar(30)            
   declare @AccountType nvarchar(1)            
   set @AccountName = ''            
   set @ProductID = ''            
   declare @bCurrencyID nvarchar(30)            
   set @CurrencyID = ''            
   declare @ChequeID nvarchar(30)            
   declare @ChequeDate datetime            
   set @Amount = 0            
   declare @chAmount decimal(21,3)            
   declare @cAmount decimal(21,3)            
   set @ForeignAmount = 0            
   set @ExchangeRate = 0            
   set @DescriptionID = ''            
   declare @Description nvarchar(255)            
   set @ChannelRefID = ''            
   set @TrxTimeStamp = null            
   declare @SMSText nvarchar(max)            
   declare @SMSToSend nvarchar(max)            
   declare @CharityGL nvarchar(30)            
   declare @BankID nvarchar(30)            
            
   select @OurBranchID=OurBranchID, @AccountID=AccountID, @TrType=TrxType, @TrxType=case TrxType when 'D' then 'Debited' else 'Credited' end, @wDate=wDate, @AccountType='Customer', @AccountName=AccountName,            
   @chAmount = 0, @ProductID=ProductID, @CurrencyID=CurrencyID, @ChequeID=ChequeID, @ChequeDate=ChequeDate, @Amount=Amount, @ForeignAmount=ForeignAmount,            
   @ExchangeRate=ExchangeRate, @DescriptionID = DescriptionID, @Description = Description, @ChannelRefID = ChannelRefID, @TrxTimeStamp = TrxTimeStamp            
   from inserted            
            
   if @TrType='D' and @AccountType='C'            
   begin            
   if exists(select DescriptionID from t_TransactionDescriptions where OurBranchID = @OurBranchID and DescriptionID = @DescriptionID and ISNULL(IsCharity,0) = 1)            
   begin            
                  
  if exists(select AccountID from t_Account where OurBranchID = @OurBranchID and AccountID = @AccountID and ISNULL(IsCharity,0) = 1)            
  begin            
                
 select @cAmount = ISNULL(CharityAmount,1) from t_Account where OurBranchID = @OurBranchID and AccountID = @AccountID and ISNULL(IsCharity,0) = 1            
 select @bCurrencyID = LocalCurrency, @BankID = OurBankID from t_GLobalVariables where OurBranchID = @OurBranchID            
            
 if @CurrencyID = @bCurrencyID            
 begin            
    SELECT @chAmount = @cAmount - (@Amount % @cAmount)            
 end            
 else            
 begin            
    SELECT @chAmount = @cAmount - (@ForeignAmount % @cAmount)            
 end            
            
 if @chAmount > 0            
 begin            
    if exists (select top 1 GLAccountID from t_CharityGLs where OurBranchID=@OurBranchID and CurrencyID=@CurrencyID)            
 begin            
              
   select @CharityGL = GLAccountID from t_CharityGLs where OurBranchID=@OurBranchID and CurrencyID=@CurrencyID            
              
   declare @MaxScrollNo decimal(24,0)            
   declare @ClearBalance money            
               declare @Limit money            
               declare @FreezeAmount money            
               declare @ProductMinBalance money            
               declare @DrawableBalance money            
                           
               --exec @Limit = fnc_GetLimit @OurBranchID, @AccountID            
                           
               SELECT @ClearBalance = ab.ClearBalance, @Limit = Limit, @FreezeAmount = ab.FreezeAmount, @ProductMinBalance = ISNULL(P.MinBalance,0)            
               FROM t_AccountBalance ab            
               INNER JOIN t_Products P ON ab.OurBranchID = P.OurBranchID AND ab.ProductID = P.ProductID            
               Where ab.OurBranchID = @OurBranchID and ab.AccountID = @AccountID            
            
               select @DrawableBalance = @ClearBalance + @Limit - @FreezeAmount - @ProductMinBalance            
                           
    create table #tmpCharity (            
  [Status] Int,            
  Supervision varchar(10)            
    )            
            
    if @DrawableBalance >= @chAmount and @CurrencyID = @bCurrencyID            
    begin            
                 
   select @MaxScrollNo = isnull(max(ScrollNo),0) + 1 from t_TransferTransactionModel where OurBranchID=@OurBranchID            
             
   insert into #tmpCharity            
   exec spc_AddEditTransferTransaction            
   @ScrollNo = @MaxScrollNo,             
   @SerialID = 1,            
   @OurBranchID = @OurBranchID,            
   @AccountID = @AccountID,             
   @AccountName = @AccountName,             
   @ProductID = @ProductID,             
   @CurrencyID = @CurrencyID,             
   @AccountType = 'C',            
   @wDate = @wDate,             
   @TrxType = 'D',             
   @Amount = @chAmount,            
   @ForeignAmount = 0,            
   @ExchangeRate = 1,            
   @DescriptionID = '200190',            
   @Description = 'Charity donation',            
   @BankCode = @BankID,             
   @BranchCode = @OurBranchID,             
   @Supervision = 'N',             
   @IsSupervision = '0',             
   @OperatorID = 'COMPUTER',             
   @SupervisorID = 'N/A',             
   @PostingLimit = '999999999999',            
   @IsLocalCurrency = 1,            
   @TrxTimeStamp = @TrxTimeStamp          
             
   insert into #tmpCharity            
   exec spc_AddEditTransferTransaction            
   @ScrollNo = @MaxScrollNo,             
   @SerialID = 2,            
   @OurBranchID = @OurBranchID,            
   @AccountID = @CharityGL,             
   @AccountName = '',             
   @ProductID = 'GL',             
   @CurrencyID = @CurrencyID,             
  @AccountType = 'G',            
   @wDate = @wDate,             
   @TrxType = 'C',             
   @Amount = @chAmount,            
   @ForeignAmount = 0,            
   @ExchangeRate = 1,            
   @DescriptionID = '100001',            
   @Description = 'GL Credit against Charity donation',            
   @BankCode = @BankID,             
   @BranchCode = @OurBranchID,             
   @Supervision = 'N',         
   @IsSupervision = '0',             
   @OperatorID = 'COMPUTER',             
   @SupervisorID = 'N/A',             
   @PostingLimit = '999999999999',            
   @IsLocalCurrency = 1,            
   @TrxTimeStamp = @TrxTimeStamp            
             
    end            
            
  end            
            
  end            
            
   end            
            
   end            
   end            
            
   --Salary             
                 
   select @OurBranchID=OurBranchID, @AccountID=AccountID, @TrType=TrxType, @wDate=wDate, @AccountName=AccountName,                
   @ProductID=ProductID, @CurrencyID=CurrencyID, @Amount=Amount, @ForeignAmount=ForeignAmount,                
   @ExchangeRate=ExchangeRate, @DescriptionID = DescriptionID, @Description = Description, @TrxTimeStamp = TrxTimeStamp                
   from inserted                
                
   if exists(select DescriptionID from t_TransactionDescriptions where OurBranchID = @OurBranchID and DescriptionID = @DescriptionID and ISNULL(IsSalary,0) = 1)                
   begin              
              
   insert into t_CustomerSalaries (OurBranchID,AccountID,TrxType,wDate,AccountName,ProductID,CurrencyID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,TrxTimeStamp)              
   values (@OurBranchID,@AccountID,@TrType,@wDate,@AccountName,@ProductID,@CurrencyID,@Amount,@ForeignAmount,@ExchangeRate,@DescriptionID,@Description,@TrxTimeStamp)              
              
   end                
                     
END