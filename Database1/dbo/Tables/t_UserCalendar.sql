﻿CREATE TABLE [dbo].[t_UserCalendar] (
    [CreateBy]       NVARCHAR (30)  NOT NULL,
    [CreateTime]     DATETIME       NOT NULL,
    [CreateTerminal] NVARCHAR (30)  NOT NULL,
    [UpdateBy]       NVARCHAR (30)  NULL,
    [UpdateTime]     DATETIME       NULL,
    [UpdateTerminal] NVARCHAR (30)  NULL,
    [OurBranchID]    NVARCHAR (30)  NOT NULL,
    [OperatorID]     NVARCHAR (30)  NOT NULL,
    [SerialNo]       INT            NOT NULL,
    [Title]          NVARCHAR (MAX) NOT NULL,
    [BgColor]        NVARCHAR (50)  NULL,
    [AllDay]         INT            NULL,
    [StartTime]      DATETIME       NULL,
    [EndTime]        DATETIME       NULL,
    [Link]           NVARCHAR (100) NULL,
    CONSTRAINT [PK_t_UserCalendar] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [OperatorID] ASC, [SerialNo] ASC)
);

