﻿CREATE TABLE [dbo].[t_DealBalanceHistory] (
    [wDate]                 DATETIME       NOT NULL,
    [OurBranchID]           NVARCHAR (30)  NOT NULL,
    [AccountID]             NVARCHAR (30)  NOT NULL,
    [DealID]                DECIMAL (24)   NOT NULL,
    [ProductID]             NVARCHAR (30)  NOT NULL,
    [Name]                  NVARCHAR (100) NOT NULL,
    [OutstandingBalance]    MONEY          NOT NULL,
    [OutstandingBalanceLCY] MONEY          NOT NULL,
    [Accrual]               MONEY          NOT NULL,
    [PAccrual]              MONEY          NOT NULL,
    [tAccrual]              MONEY          NOT NULL,
    [tPAccrual]             MONEY          NOT NULL,
    [RiskCode]              NVARCHAR (30)  NOT NULL,
    [NoOfDPD]               DECIMAL (24)   NOT NULL,
    CONSTRAINT [PK_t_DealBalanceHistory] PRIMARY KEY CLUSTERED ([wDate] ASC, [OurBranchID] ASC, [AccountID] ASC, [DealID] ASC)
);

