﻿CREATE TABLE [dbo].[t_WorkflowMsgs] (
    [OurBranchID]    NVARCHAR (30)  NOT NULL,
    [ApplicationNo]  DECIMAL (24)   NOT NULL,
    [WorkFlow]       NVARCHAR (30)  NOT NULL,
    [MessageID]      DECIMAL (24)   NOT NULL,
    [Message]        NVARCHAR (MAX) NOT NULL,
    [MessageType]    CHAR (1)       NOT NULL,
    [FormToLoad]     NVARCHAR (250) NOT NULL,
    [Status]         CHAR (1)       NOT NULL,
    [Stage]          NVARCHAR (30)  NOT NULL,
    [AppStatus]      CHAR (10)      NOT NULL,
    [SentBy]         NVARCHAR (30)  NOT NULL,
    [SentTo]         NVARCHAR (MAX) NOT NULL,
    [Type]           CHAR (10)      NOT NULL,
    [CreateTime]     DATETIME       NOT NULL,
    [CreateTerminal] NVARCHAR (30)  NOT NULL,
    [UpdateBy]       NVARCHAR (30)  NULL,
    [UpdateTime]     DATETIME       NULL,
    [UpdateTerminal] NVARCHAR (30)  NULL,
    CONSTRAINT [PK_t_WorkflowMsgs] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [ApplicationNo] ASC, [WorkFlow] ASC, [MessageID] ASC)
);

