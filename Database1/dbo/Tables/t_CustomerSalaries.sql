﻿CREATE TABLE [dbo].[t_CustomerSalaries] (
    [SerialNo]      DECIMAL (24)    IDENTITY (1, 1) NOT NULL,
    [OurBranchID]   NVARCHAR (30)   NOT NULL,
    [AccountID]     NVARCHAR (30)   NOT NULL,
    [TrxType]       NVARCHAR (1)    NOT NULL,
    [wDate]         DATETIME        NOT NULL,
    [AccountName]   NVARCHAR (100)  NOT NULL,
    [ProductID]     NVARCHAR (30)   NOT NULL,
    [CurrencyID]    NVARCHAR (30)   NOT NULL,
    [Amount]        DECIMAL (21, 3) NOT NULL,
    [ForeignAmount] DECIMAL (21, 3) NOT NULL,
    [ExchangeRate]  DECIMAL (18, 6) NOT NULL,
    [DescriptionID] NVARCHAR (30)   NOT NULL,
    [Description]   NVARCHAR (255)  NOT NULL,
    [TrxTimeStamp]  DATETIME        NOT NULL,
    CONSTRAINT [PK_t_CustomerSalaries] PRIMARY KEY CLUSTERED ([SerialNo] ASC)
);

