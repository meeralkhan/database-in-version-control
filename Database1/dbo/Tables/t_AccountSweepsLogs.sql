﻿CREATE TABLE [dbo].[t_AccountSweepsLogs] (
    [SerialNo]             DECIMAL (24)    IDENTITY (1, 1) NOT NULL,
    [OurBranchID]          NVARCHAR (30)   NOT NULL,
    [AccountID]            NVARCHAR (30)   NOT NULL,
    [SweepType]            CHAR (1)        NOT NULL,
    [ExceedAmount]         DECIMAL (21, 3) NOT NULL,
    [AccessFundsAccountID] NVARCHAR (30)   NOT NULL,
    [wDate]                DATETIME        NOT NULL,
    [DateTime]             DATETIME        NOT NULL,
    [Status]               NVARCHAR (10)   NOT NULL,
    [Message]              NVARCHAR (MAX)  NOT NULL,
    CONSTRAINT [PK_t_AccountSweepsLogs] PRIMARY KEY CLUSTERED ([SerialNo] ASC)
);

