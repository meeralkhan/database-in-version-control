﻿CREATE TABLE [dbo].[t_CustProfit] (
    [SerialNo]     DECIMAL (24)    IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ReferenceNo]  NVARCHAR (100)  NOT NULL,
    [BranchID]     NVARCHAR (50)   NOT NULL,
    [ProductID]    NVARCHAR (50)   NOT NULL,
    [AccountID]    NVARCHAR (50)   NOT NULL,
    [ValueDate]    DATETIME        NOT NULL,
    [ProfitDate]   DATETIME        NOT NULL,
    [Amount]       DECIMAL (18, 6) NOT NULL,
    [LAccount]     NVARCHAR (50)   NOT NULL,
    [Status]       NVARCHAR (100)  NOT NULL,
    [Flag]         CHAR (1)        NOT NULL,
    [ProfitAmount] DECIMAL (18, 6) NULL,
    [TaxAmount]    DECIMAL (18, 6) NULL,
    CONSTRAINT [PK_t_CustProfit] PRIMARY KEY CLUSTERED ([SerialNo] ASC)
);

