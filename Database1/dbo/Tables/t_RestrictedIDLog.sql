﻿CREATE TABLE [dbo].[t_RestrictedIDLog] (
    [SerialNo]    INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [OurBranchID] NVARCHAR (30)  NOT NULL,
    [ID]          NVARCHAR (30)  NOT NULL,
    [Action]      NVARCHAR (10)  NOT NULL,
    [IDNo]        NVARCHAR (30)  NOT NULL,
    [Module]      NVARCHAR (50)  NOT NULL,
    [Message]     NVARCHAR (MAX) NOT NULL,
    [Username]    NVARCHAR (30)  NOT NULL,
    [DateTime]    DATETIME       NOT NULL,
    [Terminal]    NVARCHAR (30)  NOT NULL,
    CONSTRAINT [PK_t_RestrictedIDLog] PRIMARY KEY CLUSTERED ([SerialNo] ASC)
);

