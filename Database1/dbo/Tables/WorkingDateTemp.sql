﻿CREATE TABLE [dbo].[WorkingDateTemp] (
    [WorkingDateTime] DATETIME NOT NULL,
    CONSTRAINT [PK_WorkingDateTemp] PRIMARY KEY CLUSTERED ([WorkingDateTime] ASC)
);

