﻿CREATE TABLE [dbo].[t_Country] (
    [CreateBy]          NVARCHAR (30)  NOT NULL,
    [CreateTime]        DATETIME       NOT NULL,
    [CreateTerminal]    NVARCHAR (30)  NOT NULL,
    [UpdateBy]          NVARCHAR (30)  NULL,
    [UpdateTime]        DATETIME       NULL,
    [UpdateTerminal]    NVARCHAR (30)  NULL,
    [AuthStatus]        CHAR (1)       NOT NULL,
    [SuperviseBy]       NVARCHAR (30)  NULL,
    [SuperviseTime]     DATETIME       NULL,
    [SuperviseTerminal] NVARCHAR (30)  NULL,
    [frmName]           NVARCHAR (100) NOT NULL,
    [OurBranchID]       NVARCHAR (30)  NOT NULL,
    [CountryID]         NVARCHAR (30)  NOT NULL,
    [Name]              NVARCHAR (100) NULL,
    [ShortName]         NVARCHAR (2)   NULL,
    [CurrencyID]        NVARCHAR (30)  NULL,
    [VerifyBy]          NVARCHAR (30)  NULL,
    [VerifyTime]        DATETIME       NULL,
    [VerifyTerminal]    NVARCHAR (30)  NULL,
    [VerifyStatus]      CHAR (1)       NOT NULL,
    [Nationality]       NVARCHAR (50)  NULL,
    [CountryCode]       NVARCHAR (3)   NULL,
    [IsGCC]             INT            NULL,
    [RegionID]          NVARCHAR (30)  NULL,
    CONSTRAINT [PK_t_Country] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [CountryID] ASC)
);

