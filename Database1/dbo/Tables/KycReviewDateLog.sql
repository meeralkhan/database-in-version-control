﻿CREATE TABLE [dbo].[KycReviewDateLog] (
    [Id]               NVARCHAR (50) NOT NULL,
    [OldKycReviewDate] DATETIME      NULL,
    [NewKycReviewDate] DATETIME      NULL,
    [RiskProfile]      NVARCHAR (50) NULL,
    [BlackList]        NVARCHAR (50) NULL,
    [PEP]              NVARCHAR (50) NULL,
    [BlackListCorp]    NVARCHAR (50) NULL,
    [PEPCorp]          NVARCHAR (50) NULL,
    [ClientId]         NVARCHAR (50) NULL,
    [CreatedOn]        DATETIME      NOT NULL,
    CONSTRAINT [PK_KycReviewDateLog] PRIMARY KEY CLUSTERED ([Id] ASC)
);

