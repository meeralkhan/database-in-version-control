﻿CREATE TABLE [dbo].[t_Cheque_ChargesRecieved] (
    [OurBranchID]   NVARCHAR (30)  NOT NULL,
    [AccountID]     NVARCHAR (30)  NOT NULL,
    [ChequeStart]   NVARCHAR (30)  NOT NULL,
    [ChequeEnd]     NVARCHAR (30)  NOT NULL,
    [ChargeID]      NVARCHAR (30)  NOT NULL,
    [ChargeSerial]  NVARCHAR (30)  NOT NULL,
    [Description]   NVARCHAR (100) NOT NULL,
    [Type]          NVARCHAR (10)  NOT NULL,
    [Amount]        MONEY          NOT NULL,
    [TotalAmount]   MONEY          NOT NULL,
    [ModeOfCharges] NVARCHAR (10)  NOT NULL,
    [GLSerialID]    NVARCHAR (30)  NOT NULL,
    [RefNo]         NVARCHAR (100) NOT NULL,
    [frmName]       VARCHAR (100)  NULL,
    CONSTRAINT [PK_t_Cheque_ChargesRecieved] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [AccountID] ASC, [ChequeStart] ASC, [ChargeID] ASC, [ChargeSerial] ASC)
);

