﻿CREATE TABLE [dbo].[t_TellerBalancing_Auth] (
    [CreateBy]          NVARCHAR (30)  NOT NULL,
    [CreateTime]        DATETIME       NOT NULL,
    [CreateTerminal]    NVARCHAR (30)  NOT NULL,
    [UpdateBy]          NVARCHAR (30)  NULL,
    [UpdateTime]        DATETIME       NULL,
    [UpdateTerminal]    NVARCHAR (30)  NULL,
    [AuthStatus]        CHAR (1)       NOT NULL,
    [VerifyBy]          NVARCHAR (30)  NULL,
    [VerifyTime]        DATETIME       NULL,
    [VerifyTerminal]    NVARCHAR (30)  NULL,
    [VerifyStatus]      CHAR (1)       NOT NULL,
    [SuperviseBy]       NVARCHAR (30)  NULL,
    [SuperviseTime]     DATETIME       NULL,
    [SuperviseTerminal] NVARCHAR (30)  NULL,
    [frmName]           NVARCHAR (100) NOT NULL,
    [OurBranchID]       NVARCHAR (30)  NOT NULL,
    [OperatorID]        NVARCHAR (30)  NOT NULL,
    [CurrencyID]        NVARCHAR (30)  NOT NULL,
    [SerialID]          DECIMAL (24)   NOT NULL,
    [Balance]           MONEY          NULL,
    [ForeignBalance]    MONEY          NULL,
    [GLID]              NVARCHAR (30)  NULL,
    [AccountName]       NVARCHAR (100) NULL,
    [SafeID]            NVARCHAR (30)  NULL,
    [SafeName]          NVARCHAR (100) NULL,
    [Remarks]           TEXT           NULL,
    [WorkingDate]       DATETIME       NULL,
    CONSTRAINT [PK_t_TellerBalancing_Auth] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [OperatorID] ASC, [CurrencyID] ASC, [SerialID] ASC)
);

