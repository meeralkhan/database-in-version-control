﻿CREATE TABLE [dbo].[t_DealMonthBalances] (
    [Month]             DATETIME       NOT NULL,
    [OurBranchID]       NVARCHAR (30)  NOT NULL,
    [AccountID]         NVARCHAR (30)  NOT NULL,
    [DealID]            DECIMAL (24)   NOT NULL,
    [ProductID]         NVARCHAR (30)  NULL,
    [Name]              NVARCHAR (100) NULL,
    [ClearBalance]      MONEY          NULL,
    [Effects]           MONEY          NULL,
    [LocalClearBalance] MONEY          NULL,
    [LocalEffects]      MONEY          NULL,
    CONSTRAINT [PK_t_DealMonthBalances] PRIMARY KEY CLUSTERED ([Month] ASC, [OurBranchID] ASC, [AccountID] ASC, [DealID] ASC)
);

