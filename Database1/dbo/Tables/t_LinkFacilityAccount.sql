﻿CREATE TABLE [dbo].[t_LinkFacilityAccount] (
    [CreateBy]          NVARCHAR (30)   NOT NULL,
    [CreateTime]        DATETIME        NOT NULL,
    [CreateTerminal]    NVARCHAR (30)   NOT NULL,
    [UpdateBy]          NVARCHAR (30)   NULL,
    [UpdateTime]        DATETIME        NULL,
    [UpdateTerminal]    NVARCHAR (30)   NULL,
    [AuthStatus]        CHAR (1)        NOT NULL,
    [VerifyBy]          NVARCHAR (30)   NULL,
    [VerifyTime]        DATETIME        NULL,
    [VerifyTerminal]    NVARCHAR (30)   NULL,
    [VerifyStatus]      CHAR (1)        NOT NULL,
    [SuperviseBy]       NVARCHAR (30)   NULL,
    [SuperviseTime]     DATETIME        NULL,
    [SuperviseTerminal] NVARCHAR (30)   NULL,
    [frmName]           NVARCHAR (100)  NOT NULL,
    [OurBranchID]       NVARCHAR (30)   NOT NULL,
    [ClientID]          NVARCHAR (30)   NOT NULL,
    [FacilityID]        DECIMAL (24)    NOT NULL,
    [SerialID]          DECIMAL (24)    NOT NULL,
    [AccountID]         NVARCHAR (30)   NULL,
    [Limit]             MONEY           NULL,
    [EffectiveDate]     DATETIME        NULL,
    [Limit1]            MONEY           NULL,
    [Rate1]             DECIMAL (18, 6) NULL,
    [Limit2]            MONEY           NULL,
    [Rate2]             DECIMAL (18, 6) NULL,
    [Limit3]            MONEY           NULL,
    [Rate3]             DECIMAL (18, 6) NULL,
    [Limit4]            MONEY           NULL,
    [Rate4]             DECIMAL (18, 6) NULL,
    [Limit5]            MONEY           NULL,
    [Rate5]             DECIMAL (18, 6) NULL,
    [ExcessRate]        DECIMAL (18, 6) NULL,
    [AccountLimitID]    NVARCHAR (100)  NULL,
    [RateType1]         NVARCHAR (1)    NULL,
    [RateID1]           NVARCHAR (30)   NULL,
    [Margin1]           DECIMAL (18, 6) NULL,
    [RateType2]         NVARCHAR (1)    NULL,
    [RateID2]           NVARCHAR (30)   NULL,
    [RateType3]         NVARCHAR (1)    NULL,
    [RateID3]           NVARCHAR (30)   NULL,
    [Margin2]           DECIMAL (18, 6) NULL,
    [Margin3]           DECIMAL (18, 6) NULL,
    [ExcessRateType]    NVARCHAR (1)    NULL,
    [ExcessRateID]      NVARCHAR (30)   NULL,
    [ExcessMargin]      DECIMAL (18, 6) NULL,
    [MinRate1]          DECIMAL (18, 6) NULL,
    [MinRate2]          DECIMAL (18, 6) NULL,
    [MinRate3]          DECIMAL (18, 6) NULL,
    [ExcessMinRate]     DECIMAL (18, 6) NULL,
    [NextChangeDate]    DATETIME        NULL,
    CONSTRAINT [PK_t_LinkFacilityAccount] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [ClientID] ASC, [FacilityID] ASC, [SerialID] ASC)
);


GO
create TRIGGER [dbo].[tr_LinkFacilityAccount_Edit] ON [dbo].[t_LinkFacilityAccount]
AFTER UPDATE
AS
BEGIN
   
   SET NOCOUNT ON

   declare @OurBranchID nvarchar(30)
   declare @ClientID nvarchar(30)
   declare @FacilityID decimal(24,0)
   declare @SerialID decimal(24,0)

   declare @AccountID nvarchar(30)
   declare @AccountLimitID nvarchar(30)

   declare @Limit money
   declare @EffectiveDate datetime

   declare @Limit1 money
   declare @Rate1 decimal(18,6)
   declare @MinRate1 decimal(18,6)

   declare @Limit2 money
   declare @Rate2 decimal(18,6)
   declare @MinRate2 decimal(18,6)
   
   declare @Limit3 money
   declare @Rate3 decimal(18,6)
   declare @MinRate3 decimal(18,6)
   
   declare @ExcessRate decimal(18,6)
   declare @ExcessMinRate decimal(18,6)
   declare @NextChangeDate datetime

   select @OurBranchID=OurBranchID, @ClientID=ClientID, @FacilityID=FacilityID, @SerialID=SerialID, @AccountID=AccountID, @AccountLimitID = AccountLimitID,
   @Limit=ISNULL(Limit,0), @Limit1=ISNULL(Limit1,0), @Limit2=ISNULL(Limit2,0), @Limit3=ISNULL(Limit3,0),
   @Rate1=ISNULL(Rate1,0), @Rate2=ISNULL(Rate2,0), @Rate3=ISNULL(Rate3,0), @ExcessRate=ISNULL(ExcessRate,0),
   @MinRate1=ISNULL(MinRate1,0), @MinRate2=ISNULL(MinRate2,0), @MinRate3=ISNULL(MinRate3,0), @ExcessMinRate=ISNULL(ExcessMinRate,0),@NextChangeDate = NextChangeDate
   from inserted

   select @EffectiveDate = WorkingDate from t_Last where OurBranchID = @OurBranchID

   delete from t_OverdraftLimits where OurBranchID = @OurBranchID and AccountID = @AccountID and EffectiveDate = @EffectiveDate

   if @ExcessRate = 0
      set @ExcessRate = @Rate1+2
	  
   if @ExcessMinRate = 0
      set @ExcessMinRate = @MinRate1+2

   insert into t_OverdraftLimits (OurBranchID, ClientID, FacilityID, SerialID, AccountID, AccountLimitID, Limit, EffectiveDate,
   Limit1, Rate1, MinRate1, Limit2, Rate2, MinRate2, Limit3, Rate3, MinRate3, ExcessRate, ExcessMinRate, LogDateTime,NextChangeDate)
   VALUES (@OurBranchID, @ClientID, @FacilityID, @SerialID, @AccountID, @AccountLimitID, @Limit, @EffectiveDate,
   @Limit1, @Rate1, @MinRate1, @Limit2, @Rate2, @MinRate2, @Limit3, @Rate3, @MinRate3, @ExcessRate, @ExcessMinRate, GETDATE(),@NextChangeDate)
   
END
GO
CREATE TRIGGER [dbo].[tr_LinkFacilityAccount_Delete] ON [dbo].[t_LinkFacilityAccount]
AFTER DELETE
AS
BEGIN
   
   SET NOCOUNT ON

   declare @OurBranchID nvarchar(30)
   declare @ClientID nvarchar(30)
   declare @FacilityID decimal(24,0)
   declare @SerialID decimal(24,0)

   declare @AccountID nvarchar(30)
   declare @AccountLimitID nvarchar(30)

   declare @Limit money
   declare @EffectiveDate datetime

   declare @Limit1 money
   declare @Rate1 decimal(18,6)
   declare @MinRate1 decimal(18,6)

   declare @Limit2 money
   declare @Rate2 decimal(18,6)
   declare @MinRate2 decimal(18,6)
   
   declare @Limit3 money
   declare @Rate3 decimal(18,6)
   declare @MinRate3 decimal(18,6)
   
   declare @ExcessRate decimal(18,6)
   declare @ExcessMinRate decimal(18,6)

   select @OurBranchID=OurBranchID, @ClientID=ClientID, @FacilityID=FacilityID, @SerialID=SerialID, @AccountID=AccountID, @AccountLimitID = AccountLimitID,
   @Limit=ISNULL(Limit,0), @Limit1=ISNULL(Limit1,0), @Limit2=ISNULL(Limit2,0), @Limit3=ISNULL(Limit3,0),
   @Rate1=ISNULL(Rate1,0), @Rate2=ISNULL(Rate2,0), @Rate3=ISNULL(Rate3,0), @ExcessRate=ISNULL(ExcessRate,0),
   @MinRate1=ISNULL(MinRate1,0), @MinRate2=ISNULL(MinRate2,0), @MinRate3=ISNULL(MinRate3,0), @ExcessMinRate=ISNULL(ExcessMinRate,0)
   from deleted

   select @EffectiveDate = WorkingDate from t_Last where OurBranchID = @OurBranchID

   delete from t_OverdraftLimits where OurBranchID = @OurBranchID and AccountID = @AccountID and EffectiveDate = @EffectiveDate

   insert into t_OverdraftLimits (OurBranchID, ClientID, FacilityID, SerialID, AccountID, AccountLimitID, Limit, EffectiveDate,
   Limit1, Rate1, MinRate1, Limit2, Rate2, MinRate2, Limit3, Rate3, MinRate3, ExcessRate, ExcessMinRate, LogDateTime)
   VALUES (@OurBranchID, @ClientID, @FacilityID, @SerialID, @AccountID, @AccountLimitID, 0, @EffectiveDate,
   0, 0, 0, 0, 0, 0, 0, 0, 0, @ExcessRate, @ExcessMinRate, GETDATE())
   
END
GO
create TRIGGER [dbo].[tr_LinkFacilityAccount_Add] ON [dbo].[t_LinkFacilityAccount]
AFTER INSERT
AS
BEGIN
   
   SET NOCOUNT ON

   declare @OurBranchID nvarchar(30)
   declare @ClientID nvarchar(30)
   declare @FacilityID decimal(24,0)
   declare @SerialID decimal(24,0)

   declare @AccountID nvarchar(30)
   declare @AccountLimitID nvarchar(30)

   declare @Limit money
   declare @EffectiveDate datetime

   declare @Limit1 money
   declare @Rate1 decimal(18,6)
   declare @MinRate1 decimal(18,6)

   declare @Limit2 money
   declare @Rate2 decimal(18,6)
   declare @MinRate2 decimal(18,6)
   
   declare @Limit3 money
   declare @Rate3 decimal(18,6)
   declare @MinRate3 decimal(18,6)
   
   declare @ExcessRate decimal(18,6)
   declare @ExcessMinRate decimal(18,6)
   declare @NextChangeDate datetime

   select @OurBranchID=OurBranchID, @ClientID=ClientID, @FacilityID=FacilityID, @SerialID=SerialID, @AccountID=AccountID, @AccountLimitID = AccountLimitID,
   @Limit=ISNULL(Limit,0), @Limit1=ISNULL(Limit1,0), @Limit2=ISNULL(Limit2,0), @Limit3=ISNULL(Limit3,0),
   @Rate1=ISNULL(Rate1,0), @Rate2=ISNULL(Rate2,0), @Rate3=ISNULL(Rate3,0), @ExcessRate=ISNULL(ExcessRate,0),
   @MinRate1=ISNULL(MinRate1,0), @MinRate2=ISNULL(MinRate2,0), @MinRate3=ISNULL(MinRate3,0), @ExcessMinRate=ISNULL(ExcessMinRate,0), @NextChangeDate = NextChangeDate
   from inserted

   select @EffectiveDate = WorkingDate from t_Last where OurBranchID = @OurBranchID

   delete from t_OverdraftLimits where OurBranchID = @OurBranchID and AccountID = @AccountID and EffectiveDate = @EffectiveDate
   
   if @ExcessRate = 0
      set @ExcessRate = @Rate1+2
	  
   if @ExcessMinRate = 0
      set @ExcessMinRate = @MinRate1+2

   insert into t_OverdraftLimits (OurBranchID, ClientID, FacilityID, SerialID, AccountID, AccountLimitID, Limit, EffectiveDate,
   Limit1, Rate1, MinRate1, Limit2, Rate2, MinRate2, Limit3, Rate3, MinRate3, ExcessRate, ExcessMinRate, LogDateTime,NextChangeDate)
   VALUES (@OurBranchID, @ClientID, @FacilityID, @SerialID, @AccountID, @AccountLimitID, @Limit, @EffectiveDate,
   @Limit1, @Rate1, @MinRate1, @Limit2, @Rate2, @MinRate2, @Limit3, @Rate3, @MinRate3, @ExcessRate, @ExcessMinRate, GETDATE(),@NextChangeDate)
   
END