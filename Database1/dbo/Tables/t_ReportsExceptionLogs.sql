﻿CREATE TABLE [dbo].[t_ReportsExceptionLogs] (
    [SerialId]       INT            IDENTITY (1, 1) NOT NULL,
    [ErrorStartTime] NVARCHAR (MAX) NULL,
    [Exception]      NVARCHAR (MAX) NULL,
    [InnerException] NVARCHAR (MAX) NULL,
    [RequestParams]  NVARCHAR (MAX) NULL,
    [level]          NVARCHAR (MAX) NULL,
    [ErrorEndTime]   NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([SerialId] ASC)
);

