﻿CREATE TABLE [dbo].[t_UserAccessLogs] (
    [OurBranchID]    NVARCHAR (30) NOT NULL,
    [OperatorID]     NVARCHAR (30) NOT NULL,
    [WDateTime]      DATETIME      NOT NULL,
    [SQLDateTime]    DATETIME      NOT NULL,
    [ServerDateTime] DATETIME      NOT NULL,
    [AuthType]       BIT           NOT NULL,
    [frmName]        VARCHAR (50)  NOT NULL,
    [Description]    VARCHAR (150) NOT NULL,
    [Terminal]       VARCHAR (50)  NOT NULL
);

