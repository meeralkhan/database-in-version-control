﻿CREATE TABLE [dbo].[t_BulkECLUpload_Auth] (
    [CreateBy]          NVARCHAR (100) NOT NULL,
    [CreateTime]        DATETIME       NOT NULL,
    [CreateTerminal]    NVARCHAR (30)  NOT NULL,
    [UpdateBy]          NVARCHAR (100) NULL,
    [UpdateTime]        DATETIME       NULL,
    [UpdateTerminal]    NVARCHAR (30)  NULL,
    [AuthStatus]        CHAR (1)       NOT NULL,
    [SuperviseBy]       NVARCHAR (100) NULL,
    [SuperviseTime]     DATETIME       NULL,
    [SuperviseTerminal] NVARCHAR (30)  NULL,
    [VerifyBy]          NVARCHAR (100) NULL,
    [VerifyTime]        DATETIME       NULL,
    [VerifyTerminal]    NVARCHAR (30)  NULL,
    [VerifyStatus]      CHAR (1)       NOT NULL,
    [frmName]           NVARCHAR (100) NULL,
    [OurBranchID]       NVARCHAR (30)  NOT NULL,
    [SerialID]          DECIMAL (24)   NOT NULL,
    [FiletoUse]         NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_t_BulkECLUpload_Auth] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [SerialID] ASC)
);

