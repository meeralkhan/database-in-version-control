﻿CREATE TABLE [dbo].[tbl_ToSync] (
    [SyncID]         DECIMAL (24)  NOT NULL,
    [OurBranchID]    NVARCHAR (30) NOT NULL,
    [KeyID1]         NVARCHAR (30) NOT NULL,
    [KeyID2]         NVARCHAR (30) NOT NULL,
    [KeyID3]         NVARCHAR (30) NOT NULL,
    [frmName]        VARCHAR (100) NOT NULL,
    [CreateBy]       NVARCHAR (30) NOT NULL,
    [CreateTime]     DATETIME      NOT NULL,
    [CreateTerminal] NVARCHAR (30) NOT NULL,
    CONSTRAINT [PK_tbl_ToSync] PRIMARY KEY CLUSTERED ([SyncID] ASC)
);

