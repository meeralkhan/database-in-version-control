﻿CREATE TABLE [dbo].[t_AccountCIF_Auth] (
    [OurBranchID] NVARCHAR (30) NOT NULL,
    [AccountID]   NVARCHAR (30) NOT NULL,
    [ClientID]    NVARCHAR (30) NOT NULL,
    CONSTRAINT [PK_t_AccountCIF_Auth] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [AccountID] ASC, [ClientID] ASC)
);

