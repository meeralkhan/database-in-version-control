﻿CREATE TABLE [dbo].[t_PreviousDayBalances] (
    [WorkingDate]       DATETIME      NOT NULL,
    [OurBranchID]       VARCHAR (30)  NOT NULL,
    [AccountID]         VARCHAR (30)  NOT NULL,
    [ProductID]         VARCHAR (30)  NOT NULL,
    [Name]              VARCHAR (100) NOT NULL,
    [ClearBalance]      MONEY         NOT NULL,
    [Effects]           MONEY         NOT NULL,
    [LocalClearBalance] MONEY         NOT NULL,
    [LocalEffects]      MONEY         NOT NULL
);

