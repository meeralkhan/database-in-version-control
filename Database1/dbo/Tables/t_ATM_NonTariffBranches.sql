﻿CREATE TABLE [dbo].[t_ATM_NonTariffBranches] (
    [BranchID]   VARCHAR (30)  NOT NULL,
    [BranchName] VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_t_ATM_NonTariffBranches] PRIMARY KEY CLUSTERED ([BranchID] ASC)
);

