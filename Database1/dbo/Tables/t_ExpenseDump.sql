﻿CREATE TABLE [dbo].[t_ExpenseDump] (
    [wDate]        DATETIME      NOT NULL,
    [OurBranchID]  NVARCHAR (30) NOT NULL,
    [AccountID]    NVARCHAR (30) NOT NULL,
    [CostCenterID] NVARCHAR (30) NOT NULL,
    [Amount]       MONEY         NOT NULL,
    CONSTRAINT [PK_t_ExpenseDump] PRIMARY KEY CLUSTERED ([wDate] ASC, [OurBranchID] ASC, [AccountID] ASC, [CostCenterID] ASC)
);

