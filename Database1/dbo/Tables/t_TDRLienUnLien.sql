﻿CREATE TABLE [dbo].[t_TDRLienUnLien] (
    [OurBranchID]    NVARCHAR (30)   NOT NULL,
    [RecieptID]      NVARCHAR (30)   NOT NULL,
    [SerialNo]       DECIMAL (24)    NOT NULL,
    [AccountID]      NVARCHAR (30)   NOT NULL,
    [IsLien]         INT             NOT NULL,
    [LienAmount]     DECIMAL (21, 3) NOT NULL,
    [LienAccountID]  NVARCHAR (30)   NOT NULL,
    [LienRemarks]    NVARCHAR (100)  NOT NULL,
    [LienBy]         NVARCHAR (30)   NOT NULL,
    [LienTime]       DATETIME        NOT NULL,
    [LienTerminal]   NVARCHAR (30)   NOT NULL,
    [UnLienRemarks]  NVARCHAR (100)  NULL,
    [UnLienBy]       NVARCHAR (30)   NULL,
    [UnLienTime]     DATETIME        NULL,
    [UnLienTerminal] NVARCHAR (30)   NULL,
    CONSTRAINT [PK_t_TDRLienUnLien] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [RecieptID] ASC, [SerialNo] ASC)
);

