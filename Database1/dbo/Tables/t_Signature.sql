﻿CREATE TABLE [dbo].[t_Signature] (
    [OurBranchID]        NVARCHAR (30)  NOT NULL,
    [AccountID]          NVARCHAR (30)  NOT NULL,
    [SerialID]           DECIMAL (18)   IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [AccountName]        VARCHAR (100)  NOT NULL,
    [Picture]            NVARCHAR (MAX) NULL,
    [wDate]              DATETIME       NOT NULL,
    [OperatorID]         VARCHAR (30)   NULL,
    [SupervisorID]       VARCHAR (30)   NULL,
    [LastModifyDate]     DATETIME       NOT NULL,
    [ComputerName]       VARCHAR (50)   NOT NULL,
    [LastUpdateTerminal] VARCHAR (50)   NOT NULL,
    [LastUpdateUser]     VARCHAR (30)   NULL,
    CONSTRAINT [PK_t_Signature] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [AccountID] ASC, [SerialID] ASC)
);

