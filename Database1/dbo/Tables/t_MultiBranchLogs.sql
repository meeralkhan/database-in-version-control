﻿CREATE TABLE [dbo].[t_MultiBranchLogs] (
    [Id]               INT            IDENTITY (1, 1) NOT NULL,
    [ChannelRefid]     NVARCHAR (100) NULL,
    [ErrorMessage]     NVARCHAR (100) NULL,
    [ExceptionMessage] NVARCHAR (MAX) NULL,
    [Module]           NVARCHAR (50)  NULL,
    [ApiName]          NVARCHAR (50)  NULL,
    [Level]            NVARCHAR (50)  NULL,
    [RequestParameter] NVARCHAR (MAX) NULL,
    [RequestResponse]  NVARCHAR (MAX) NULL,
    [CreateBy]         NVARCHAR (50)  NOT NULL,
    [CreatedOn]        DATETIME       NOT NULL
);

