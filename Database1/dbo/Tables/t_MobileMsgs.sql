﻿CREATE TABLE [dbo].[t_MobileMsgs] (
    [SerialNo] DECIMAL (24)  IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [MobileNo] VARCHAR (50)  NOT NULL,
    [Message]  VARCHAR (160) NOT NULL,
    [DateTime] DATETIME      NOT NULL,
    CONSTRAINT [PK_t_MobileMsgs] PRIMARY KEY CLUSTERED ([SerialNo] ASC)
);

