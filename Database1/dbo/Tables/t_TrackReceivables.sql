﻿CREATE TABLE [dbo].[t_TrackReceivables] (
    [CreateBy]        NVARCHAR (30) NOT NULL,
    [CreateTime]      DATETIME      NOT NULL,
    [CreateTerminal]  NVARCHAR (30) NOT NULL,
    [UpdateBy]        NVARCHAR (30) NULL,
    [UpdateTime]      DATETIME      NULL,
    [UpdateTerminal]  NVARCHAR (30) NULL,
    [OurBranchID]     NVARCHAR (30) NOT NULL,
    [AccountID]       NVARCHAR (30) NOT NULL,
    [DealID]          NVARCHAR (30) NOT NULL,
    [HoldRefNo]       NVARCHAR (50) NOT NULL,
    [IsFreeze]        INT           NOT NULL,
    [wDate]           DATETIME      NOT NULL,
    [Amount]          MONEY         NOT NULL,
    [Comments]        TEXT          NULL,
    [ReleaseDate]     DATETIME      NULL,
    [ReleaseComments] TEXT          NULL,
    [Type]            CHAR (1)      NOT NULL,
    [InstNo]          DECIMAL (24)  NOT NULL,
    CONSTRAINT [PK_t_TrackReceivables] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [AccountID] ASC, [DealID] ASC, [HoldRefNo] ASC)
);

