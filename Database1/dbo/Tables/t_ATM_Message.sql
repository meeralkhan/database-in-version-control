﻿CREATE TABLE [dbo].[t_ATM_Message] (
    [Message]     VARCHAR (500) NOT NULL,
    [MessageDate] DATETIME      NOT NULL,
    [Remarks]     VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_t_ATM_Message] PRIMARY KEY CLUSTERED ([MessageDate] ASC)
);

