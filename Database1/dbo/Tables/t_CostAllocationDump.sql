﻿CREATE TABLE [dbo].[t_CostAllocationDump] (
    [wDate]         DATETIME        NULL,
    [OurBranchID]   NVARCHAR (30)   NOT NULL,
    [AccountID]     NVARCHAR (30)   NOT NULL,
    [CurrencyID]    VARCHAR (30)    NOT NULL,
    [TrxType]       VARCHAR (1)     NOT NULL,
    [ChannelRefID]  VARCHAR (200)   NULL,
    [Amount]        MONEY           NOT NULL,
    [ForeignAmount] MONEY           NOT NULL,
    [ExchangeRate]  DECIMAL (16, 8) NULL,
    [DescriptionID] VARCHAR (50)    NOT NULL,
    [Description]   NVARCHAR (500)  NULL,
    [CostCenterID]  NVARCHAR (30)   NULL,
    [AccountName]   NVARCHAR (200)  NULL
);

