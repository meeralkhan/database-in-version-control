﻿CREATE TABLE [dbo].[t_UploadGLsCsv] (
    [SerialNo]      DECIMAL (24)    IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ReferenceNo]   NVARCHAR (100)  NOT NULL,
    [Region]        NVARCHAR (50)   NOT NULL,
    [BranchID]      NVARCHAR (50)   NOT NULL,
    [BranchName]    NVARCHAR (100)  NOT NULL,
    [AccountID]     NVARCHAR (30)   NOT NULL,
    [GLName]        NVARCHAR (100)  NOT NULL,
    [GLDate]        DATETIME        NOT NULL,
    [GLType]        NVARCHAR (30)   NOT NULL,
    [GLSubType]     NVARCHAR (30)   NOT NULL,
    [GLSubTypeDesc] NVARCHAR (50)   NOT NULL,
    [Balance]       DECIMAL (21, 3) NOT NULL,
    [GLSAPCode]     NVARCHAR (30)   NULL,
    [GLSAPName]     NVARCHAR (100)  NULL,
    [SAPBalance]    DECIMAL (21, 3) NULL,
    CONSTRAINT [PK_t_UploadGLsCsv] PRIMARY KEY CLUSTERED ([SerialNo] ASC)
);

