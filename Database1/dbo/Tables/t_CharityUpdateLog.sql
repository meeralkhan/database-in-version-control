﻿CREATE TABLE [dbo].[t_CharityUpdateLog] (
    [OurBranchID]        NVARCHAR (10) NOT NULL,
    [CharityProcessDate] DATETIME      NOT NULL,
    [UpdateDate]         DATETIME      NULL,
    CONSTRAINT [PK_t_CharityUpdateLog] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [CharityProcessDate] ASC)
);

