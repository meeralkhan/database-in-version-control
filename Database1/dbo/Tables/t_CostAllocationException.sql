﻿CREATE TABLE [dbo].[t_CostAllocationException] (
    [wDate]        DATETIME      NOT NULL,
    [OurBranchID]  NVARCHAR (30) NOT NULL,
    [AccountID]    NVARCHAR (30) NOT NULL,
    [CostCenterID] NVARCHAR (30) NOT NULL,
    [Amount]       MONEY         NULL,
    CONSTRAINT [PK_t_CostAllocationException] PRIMARY KEY CLUSTERED ([wDate] ASC, [OurBranchID] ASC, [AccountID] ASC, [CostCenterID] ASC)
);

