﻿CREATE TABLE [dbo].[t_TDRIssuance] (
    [CreateBy]               NVARCHAR (30)   NOT NULL,
    [CreateTime]             DATETIME        NOT NULL,
    [CreateTerminal]         NVARCHAR (30)   NOT NULL,
    [UpdateBy]               NVARCHAR (30)   NULL,
    [UpdateTime]             DATETIME        NULL,
    [UpdateTerminal]         NVARCHAR (30)   NULL,
    [AuthStatus]             CHAR (1)        NOT NULL,
    [SuperviseBy]            NVARCHAR (30)   NULL,
    [SuperviseTime]          DATETIME        NULL,
    [SuperviseTerminal]      NVARCHAR (30)   NULL,
    [frmName]                NVARCHAR (100)  NOT NULL,
    [OurBranchID]            NVARCHAR (30)   NOT NULL,
    [RecieptID]              NVARCHAR (30)   NOT NULL,
    [ProductID]              NVARCHAR (30)   NULL,
    [AccountID]              NVARCHAR (30)   NULL,
    [NAccountType]           NVARCHAR (1)    NULL,
    [NAccount]               NVARCHAR (30)   NULL,
    [NProductID]             NVARCHAR (30)   NULL,
    [Amount]                 MONEY           NULL,
    [ExchangeRate]           DECIMAL (18, 6) NULL,
    [IssueDate]              DATETIME        NULL,
    [StartDate]              DATETIME        NULL,
    [MaturityDate]           DATETIME        NULL,
    [Rate]                   MONEY           NULL,
    [AutoProfit]             NVARCHAR (10)   NULL,
    [PaymentPeriod]          NVARCHAR (30)   NULL,
    [Treatment]              NVARCHAR (50)   NULL,
    [AutoRollover]           NVARCHAR (30)   NULL,
    [OptionsAtMaturity]      NVARCHAR (50)   NULL,
    [IsPremiumRate]          INT             NULL,
    [AdditionalData]         TEXT            NULL,
    [RefNo]                  NVARCHAR (30)   NULL,
    [IsUnderLien]            INT             NULL,
    [IsLost]                 INT             NULL,
    [LostRemarks]            TEXT            NULL,
    [VerifyBy]               NVARCHAR (30)   NULL,
    [VerifyTime]             DATETIME        NULL,
    [VerifyTerminal]         NVARCHAR (30)   NULL,
    [VerifyStatus]           CHAR (1)        NOT NULL,
    [Interest]               MONEY           NOT NULL,
    [InterestPaid]           MONEY           NOT NULL,
    [InterestPaidupTo]       DATETIME        NULL,
    [IsClosed]               INT             NULL,
    [CloseDate]              DATETIME        NULL,
    [Accrual]                MONEY           NOT NULL,
    [AccrualUpto]            DATETIME        NULL,
    [tAccrual]               MONEY           NOT NULL,
    [Penalty]                NVARCHAR (2)    NULL,
    [PenaltyRateType]        NVARCHAR (2)    NULL,
    [PenaltyAmountRate]      MONEY           NULL,
    [TotalPenaltyAmount]     MONEY           NULL,
    [PartialAmount]          MONEY           NULL,
    [TotalLienAmount]        DECIMAL (21, 3) NULL,
    [OldReceiptID]           DECIMAL (24)    NULL,
    [ParentReceiptID]        VARCHAR (100)   NULL,
    [PartialDate]            DATETIME        NULL,
    [PartialWithdrawRemarks] NVARCHAR (200)  NULL,
    [CloseRemarks]           NVARCHAR (200)  NULL,
    [NoOfDays]               INT             NULL,
    [PartialInterestAmount]  MONEY           NULL,
    CONSTRAINT [PK_t_TDRIssuance] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [RecieptID] ASC)
);


GO
CREATE TRIGGER [dbo].[tr_TDRIssuanceHistory]  
ON [dbo].[t_TDRIssuance]  
AFTER  INSERT,UPDATE  
AS  
IF UPDATE(Penalty) OR UPDATE(PenaltyRateType) OR UPDATE(PenaltyAmountRate) OR UPDATE(TotalPenaltyAmount) OR UPDATE(Amount) OR UPDATE(UpdateTime)  
 begin  
 declare @RecieptID nvarchar(30)  
 select @RecieptID = RecieptID from inserted  
  insert into t_TDRIssuanceHistory (CreateBy,CreateTime,CreateTerminal,UpdateBy,UpdateTime,UpdateTerminal,AuthStatus,SuperviseBy,SuperviseTime,SuperviseTerminal,frmName,SerialNo,OurBranchID,RecieptID,ProductID,AccountID,NAccountType,NAccount,NProductID,Amount,ExchangeRate,IssueDate,StartDate,MaturityDate,Rate,AutoProfit,PaymentPeriod,Treatment,AutoRollover,  
  OptionsAtMaturity,IsPremiumRate,AdditionalData,RefNo,IsUnderLien,IsLost,LostRemarks,VerifyBy,VerifyTime,VerifyTerminal,VerifyStatus,Interest,InterestPaid,InterestPaidupTo,IsClosed,CloseDate,Accrual,AccrualUpto  
  ,tAccrual,Penalty,PenaltyRateType,PenaltyAmountRate,TotalPenaltyAmount,PartialAmount,TotalLienAmount,OldReceiptID,ParentReceiptID,PartialDate) select CreateBy,CreateTime,CreateTerminal,UpdateBy,UpdateTime,UpdateTerminal,AuthStatus,SuperviseBy,SuperviseTime,SuperviseTerminal,frmName,(select isnull(max(serialno),0)+1 from   
  t_TDRIssuanceHistory where RecieptID = @RecieptID),OurBranchID,RecieptID,ProductID,AccountID,NAccountType,NAccount,NProductID,Amount,ExchangeRate,IssueDate,StartDate,MaturityDate,Rate,AutoProfit,PaymentPeriod,Treatment,AutoRollover,  
  OptionsAtMaturity,IsPremiumRate,AdditionalData,RefNo,IsUnderLien,IsLost,LostRemarks,VerifyBy,VerifyTime,VerifyTerminal,VerifyStatus,Interest,InterestPaid,InterestPaidupTo,IsClosed,CloseDate,Accrual,AccrualUpto  
  ,tAccrual,Penalty,PenaltyRateType,PenaltyAmountRate,TotalPenaltyAmount,PartialAmount,TotalLienAmount,OldReceiptID,ParentReceiptID,PartialDate from t_TDRIssuance where RecieptID = @RecieptID  
 end