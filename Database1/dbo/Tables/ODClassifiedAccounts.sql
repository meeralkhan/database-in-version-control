﻿CREATE TABLE [dbo].[ODClassifiedAccounts] (
    [OurBranchID]      NVARCHAR (30)  NOT NULL,
    [AccountID]        NVARCHAR (30)  NOT NULL,
    [WorkingDate]      DATETIME       NOT NULL,
    [ClientID]         NVARCHAR (30)  NOT NULL,
    [Name]             NVARCHAR (100) NOT NULL,
    [ProductID]        NVARCHAR (30)  NOT NULL,
    [CurrencyID]       NVARCHAR (60)  NOT NULL,
    [OverdraftClassID] NVARCHAR (30)  NOT NULL,
    CONSTRAINT [PK_ODClassifiedAccounts] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [AccountID] ASC, [WorkingDate] ASC)
);

