﻿CREATE TABLE [dbo].[t_ExpiredFacilities] (
    [SerialNo]       DECIMAL (24)   IDENTITY (1, 1) NOT NULL,
    [OurBranchID]    NVARCHAR (30)  NOT NULL,
    [ClientID]       NVARCHAR (30)  NOT NULL,
    [FacilityID]     DECIMAL (24)   NOT NULL,
    [ReferenceNo]    NVARCHAR (30)  NOT NULL,
    [FacilityDesc]   NVARCHAR (100) NOT NULL,
    [ReviewDate]     DATETIME       NOT NULL,
    [ExpiryDate]     DATETIME       NOT NULL,
    [Limit]          MONEY          NOT NULL,
    [ExpiredWDate]   DATETIME       NOT NULL,
    [CreateBy]       NVARCHAR (30)  NOT NULL,
    [CreateTime]     DATETIME       NOT NULL,
    [CreateTerminal] NVARCHAR (30)  NOT NULL,
    CONSTRAINT [PK_t_ExpiredFacilities] PRIMARY KEY CLUSTERED ([SerialNo] ASC)
);

