﻿CREATE TABLE [dbo].[t_InternetIBCANo] (
    [IBCA]         INT          NOT NULL,
    [OurBranchID]  VARCHAR (30) NOT NULL,
    [TargetBranch] VARCHAR (30) NOT NULL,
    [IBDA]         INT          NOT NULL
);

