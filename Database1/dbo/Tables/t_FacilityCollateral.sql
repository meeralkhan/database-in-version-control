﻿CREATE TABLE [dbo].[t_FacilityCollateral] (
    [OurBranchID]          NVARCHAR (30)  NOT NULL,
    [ClientID]             NVARCHAR (30)  NOT NULL,
    [FacilityID]           DECIMAL (24)   NOT NULL,
    [CollateralID]         DECIMAL (24)   NOT NULL,
    [Description]          NVARCHAR (100) NOT NULL,
    [ReviewDate]           DATETIME       NOT NULL,
    [ExpiryDate]           DATETIME       NOT NULL,
    [CollateralValue]      MONEY          NOT NULL,
    [AfterDeductingMargin] MONEY          NOT NULL,
    [CreateBy]             NVARCHAR (30)  NOT NULL,
    [CreateTime]           DATETIME       NOT NULL,
    [CreateTerminal]       NVARCHAR (30)  NOT NULL,
    CONSTRAINT [PK_t_FacilityCollateral_1] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [ClientID] ASC, [FacilityID] ASC, [CollateralID] ASC)
);

