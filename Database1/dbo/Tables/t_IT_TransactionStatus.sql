﻿CREATE TABLE [dbo].[t_IT_TransactionStatus] (
    [OurBranchID]     VARCHAR (30)  NOT NULL,
    [ScrollNo]        NUMERIC (18)  NULL,
    [SerialNo]        INT           NULL,
    [RefNo]           VARCHAR (30)  NULL,
    [AccountID]       VARCHAR (30)  NOT NULL,
    [AccountName]     VARCHAR (100) NULL,
    [CurrencyID]      VARCHAR (30)  NULL,
    [Amount]          MONEY         NULL,
    [ForeignAmount]   MONEY         NULL,
    [ExchangeRate]    MONEY         NULL,
    [TrxType]         VARCHAR (2)   NULL,
    [IsLocalCurrency] BIT           NULL,
    [userid]          VARCHAR (60)  NULL
);

