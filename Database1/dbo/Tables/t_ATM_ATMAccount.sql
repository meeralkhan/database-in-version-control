﻿CREATE TABLE [dbo].[t_ATM_ATMAccount] (
    [OurBranchID]     VARCHAR (30)  NOT NULL,
    [ATMID]           VARCHAR (30)  NOT NULL,
    [ATMAccountID]    VARCHAR (30)  NOT NULL,
    [ATMAccountTitle] VARCHAR (100) NULL,
    [ATMCurrencyID]   VARCHAR (30)  NOT NULL,
    [ATMCurrencyName] VARCHAR (100) NULL,
    [ATMProductID]    VARCHAR (30)  NOT NULL,
    [CDMAccountID]    VARCHAR (30)  NOT NULL,
    [CDMAccountTitle] VARCHAR (100) NULL,
    [CDMCurrencyID]   VARCHAR (30)  NOT NULL,
    [CDMCurrencyName] VARCHAR (100) NULL,
    [CDMProductID]    VARCHAR (30)  NOT NULL,
    [Description]     VARCHAR (100) NULL,
    CONSTRAINT [PK_t_ATM_GLParameters] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [ATMID] ASC)
);

