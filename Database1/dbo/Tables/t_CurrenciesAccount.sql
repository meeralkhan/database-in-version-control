﻿CREATE TABLE [dbo].[t_CurrenciesAccount] (
    [OurBranchID]  VARCHAR (30) NOT NULL,
    [CurrencyID]   VARCHAR (30) NOT NULL,
    [CashSerialID] INT          NOT NULL,
    [HOSerialID]   INT          NOT NULL,
    CONSTRAINT [PK_t_CurrenciesAccount] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [CurrencyID] ASC)
);

