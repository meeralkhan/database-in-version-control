﻿CREATE TABLE [dbo].[t_RptAccrual] (
    [OurBranchID]       NVARCHAR (30) NULL,
    [AccountID]         NVARCHAR (30) NULL,
    [ProductID]         NVARCHAR (30) NULL,
    [CurrencyID]        NVARCHAR (30) NULL,
    [TotalProduct]      MONEY         NULL,
    [AverageBalance]    MONEY         NULL,
    [Profit]            MONEY         NULL,
    [Tax]               MONEY         NULL,
    [Rate]              MONEY         NULL,
    [wDate]             DATETIME      NULL,
    [PrevAccrualAmount] MONEY         NULL,
    [PrevAccrualDate]   DATETIME      NULL
);

