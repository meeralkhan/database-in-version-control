﻿CREATE TABLE [dbo].[t_RptSavAccruals] (
    [AccrualDate]    DATETIME        NOT NULL,
    [OurBranchID]    NVARCHAR (30)   NOT NULL,
    [AccountID]      NVARCHAR (30)   NOT NULL,
    [SerialNo]       DECIMAL (24)    IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [AccountName]    NVARCHAR (100)  NOT NULL,
    [ProductID]      NVARCHAR (30)   NOT NULL,
    [ProductDesc]    NVARCHAR (100)  NOT NULL,
    [Basis]          INT             NOT NULL,
    [CurrencyID]     NVARCHAR (30)   NOT NULL,
    [ValueDate]      DATETIME        NOT NULL,
    [RateChgDate]    DATETIME        NOT NULL,
    [Days]           SMALLINT        NOT NULL,
    [Balance]        MONEY           NOT NULL,
    [Rate]           DECIMAL (24, 6) NOT NULL,
    [Amount]         MONEY           NOT NULL,
    [CreateBy]       NVARCHAR (30)   NOT NULL,
    [CreateTime]     DATETIME        NOT NULL,
    [CreateTerminal] NVARCHAR (30)   NOT NULL,
    CONSTRAINT [PK_t_RptSavAccruals] PRIMARY KEY CLUSTERED ([AccrualDate] ASC, [OurBranchID] ASC, [AccountID] ASC, [SerialNo] ASC)
);

