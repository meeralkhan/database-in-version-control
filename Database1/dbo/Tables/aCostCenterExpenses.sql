﻿CREATE TABLE [dbo].[aCostCenterExpenses] (
    [OurBranchID] NVARCHAR (30)   NOT NULL,
    [AccountID]   NVARCHAR (30)   NOT NULL,
    [CCPercent]   DECIMAL (38, 3) NULL
);

