﻿CREATE TABLE [dbo].[Quantum_BRF_5] (
    [DEAL_NO]     VARCHAR (255) NULL,
    [DEAL_DT]     VARCHAR (255) NULL,
    [INVTYPE]     VARCHAR (255) NULL,
    [BBGID]       VARCHAR (255) NULL,
    [FACE_VALUE]  VARCHAR (255) NULL,
    [COUNTRY]     VARCHAR (255) NULL,
    [COUNTRYCODE] VARCHAR (255) NULL,
    [CCY]         VARCHAR (255) NULL,
    [REMAIN_FV]   VARCHAR (255) NULL,
    [RATING]      VARCHAR (255) NULL,
    [RATINGTYPE]  VARCHAR (255) NULL,
    [MKT_VALUE]   VARCHAR (255) NULL,
    [CPTYPE]      VARCHAR (255) NULL,
    [CreateDate]  DATETIME      NULL,
    [ToolRunDate] DATETIME      NULL
);

