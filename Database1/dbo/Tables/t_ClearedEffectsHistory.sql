﻿CREATE TABLE [dbo].[t_ClearedEffectsHistory] (
    [OurBranchID]   VARCHAR (30)    NOT NULL,
    [RefNo]         VARCHAR (100)   NOT NULL,
    [ScrollNo]      INT             NOT NULL,
    [SerialNo]      DECIMAL (10)    NOT NULL,
    [AccountID]     VARCHAR (30)    NOT NULL,
    [AccountName]   CHAR (100)      NOT NULL,
    [ProductID]     VARCHAR (30)    NOT NULL,
    [CurrencyID]    VARCHAR (30)    NOT NULL,
    [AccountType]   CHAR (1)        NOT NULL,
    [ValueDate]     DATETIME        NOT NULL,
    [wDate]         DATETIME        NOT NULL,
    [TrxType]       CHAR (1)        NOT NULL,
    [ChequeID]      VARCHAR (30)    NOT NULL,
    [ChequeDate]    DATETIME        NOT NULL,
    [Amount]        DECIMAL (18, 6) NOT NULL,
    [ForeignAmount] DECIMAL (18, 6) NOT NULL,
    [ExchangeRate]  DECIMAL (18, 6) NOT NULL,
    [BankCode]      VARCHAR (30)    NOT NULL,
    [OperatorID]    VARCHAR (30)    NOT NULL,
    CONSTRAINT [PK_t_ClearedEffectsHistory] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [RefNo] ASC)
);

