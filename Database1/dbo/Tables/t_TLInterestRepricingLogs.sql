﻿CREATE TABLE [dbo].[t_TLInterestRepricingLogs] (
    [OurBranchID]    NVARCHAR (30)  NOT NULL,
    [AccountID]      NVARCHAR (30)  NOT NULL,
    [wDate]          DATETIME       NOT NULL,
    [DealID]         DECIMAL (24)   NOT NULL,
    [ReferenceNo]    NVARCHAR (200) NOT NULL,
    [RequestParams]  NVARCHAR (MAX) NULL,
    [ResponseParams] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_t_TLInterestRepricingLogs] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [AccountID] ASC, [wDate] ASC, [DealID] ASC)
);

