﻿CREATE TABLE [dbo].[t_ATM_UtilityCharges] (
    [OurBranchID]                VARCHAR (30)  NOT NULL,
    [CompanyNumber]              VARCHAR (30)  NOT NULL,
    [WithdrawalCharges]          MONEY         NOT NULL,
    [WithdrawlChargesPercentage] MONEY         NOT NULL,
    [IssuerAccountID]            VARCHAR (30)  NOT NULL,
    [IssuerAccountTitle]         VARCHAR (100) NOT NULL,
    [AcquiringAccountID]         VARCHAR (30)  NULL,
    [AcquiringAccountTitle]      VARCHAR (100) NULL,
    [IssuerChargesAccountID]     VARCHAR (30)  NULL,
    [IssuerTitle]                VARCHAR (100) NULL,
    [AcquiringChargesAccountID]  VARCHAR (30)  NULL,
    [AcquiringTitle]             VARCHAR (100) NULL,
    [ExciseDutyAccountID]        VARCHAR (30)  NULL,
    [ExciseDutyPercentage]       MONEY         NULL,
    [ChargeType]                 CHAR (1)      NULL,
    CONSTRAINT [PK_t_ATM_UtilityCharges] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [CompanyNumber] ASC)
);

