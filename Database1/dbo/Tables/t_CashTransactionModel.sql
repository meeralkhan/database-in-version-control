﻿CREATE TABLE [dbo].[t_CashTransactionModel] (
    [ScrollNo]           NUMERIC (10)    NOT NULL,
    [OurBranchID]        VARCHAR (30)    NOT NULL,
    [RefNo]              VARCHAR (100)   NULL,
    [AccountID]          VARCHAR (30)    NOT NULL,
    [AccountName]        VARCHAR (100)   NULL,
    [ProductID]          VARCHAR (30)    NOT NULL,
    [CurrencyID]         VARCHAR (30)    NULL,
    [AccountType]        CHAR (1)        NOT NULL,
    [ValueDate]          DATETIME        NULL,
    [wDate]              DATETIME        NULL,
    [TrxType]            CHAR (1)        NOT NULL,
    [ChequeID]           VARCHAR (30)    NULL,
    [ChequeDate]         DATETIME        NULL,
    [Amount]             MONEY           NOT NULL,
    [ForeignAmount]      MONEY           NULL,
    [ExchangeRate]       DECIMAL (18, 6) NULL,
    [DescriptionID]      VARCHAR (30)    NOT NULL,
    [Description]        VARCHAR (255)   NOT NULL,
    [BankCode]           VARCHAR (30)    NOT NULL,
    [BranchCode]         VARCHAR (30)    NOT NULL,
    [TrxPrinted]         BIT             NOT NULL,
    [ProfitOrLoss]       MONEY           NOT NULL,
    [GlID]               VARCHAR (30)    NULL,
    [Supervision]        CHAR (1)        NULL,
    [RemoteDescription]  VARCHAR (100)   NULL,
    [IsSupervision]      BIT             NOT NULL,
    [IsLocalCurrency]    INT             NOT NULL,
    [OperatorID]         VARCHAR (30)    NOT NULL,
    [SupervisorID]       VARCHAR (30)    NOT NULL,
    [DebitPostingLimit]  MONEY           NULL,
    [CreditPostingLimit] MONEY           NULL,
    [AppWHTax]           BIT             NOT NULL,
    [WHTaxAmount]        MONEY           NOT NULL,
    [WHTaxMode]          CHAR (1)        NOT NULL,
    [WHTaxScrollNo]      INT             NOT NULL,
    [UtilityNumber]      VARCHAR (100)   NULL,
    [AppChg]             VARCHAR (1)     NULL,
    [ChgAmount]          MONEY           NULL,
    [ChgMode]            VARCHAR (1)     NULL,
    [ChgScrollno]        MONEY           NULL,
    [AdditionalData]     TEXT            NULL,
    CONSTRAINT [PK_t_CashTransactionModel] PRIMARY KEY CLUSTERED ([ScrollNo] ASC, [OurBranchID] ASC)
);


GO

CREATE TRIGGER [dbo].[tr_CashTrxCashReject] ON [dbo].[t_CashTransactionModel]   
  
FOR  UPDATE   
AS  
  
 If ((Select supervision from inserted)='R' AND  (select ACCOUNTTYPE from inserted)='C'  
  AND (Select DescriptionID from inserted) IN (select DescriptionID from t_TransactionDescriptions   
  where ISNULL(IsInternal,1) = 0 and IsCredit = 0) And (select TRXTYPE from inserted)='D')  
  
 Update t_Account SET CashTotDr=CashTotDr-(SELECT abs(AMOUNT) FROM INSERTED),  
  CashTotDrF=CashTotDrF-(SELECT abs(ForeignAmount) FROM INSERTED)  
  
 WHERE OurBranchID=(SELECT OurBranchID FROM INSERTED) AND AccountID=(SELECT AccountID FROM INSERTED)   
 AND  (select ACCOUNTTYPE from inserted)='C'  And (select TRXTYPE from inserted)='D'
GO

CREATE TRIGGER [dbo].[tr_CashTrxCashDr] ON [dbo].[t_CashTransactionModel]    
FOR INSERT    
AS    
    
 IF (Select TRXTYPE from inserted)='D' and (Select ACCOUNTTYPE from Inserted)='C'    
 AND (Select DescriptionID from inserted) IN (select DescriptionID from t_TransactionDescriptions where ISNULL(IsInternal,1) = 0 and IsCredit = 0)    
    
 Update t_Account SET CashTotDr=ISNULL(CashTotDr,0)+(SELECT Abs(AMOUNT) FROM INSERTED),    
  CashTotDrF=ISNULL(CashTotDrF,0)+(SELECT Abs(ForeignAmount) FROM INSERTED)    
    
 WHERE OurBranchID=(SELECT OurBranchID FROM INSERTED) AND AccountID=(SELECT AccountID FROM INSERTED)     
 AND  (select ACCOUNTTYPE from inserted)='C'  And (select TRXTYPE from inserted)='D'