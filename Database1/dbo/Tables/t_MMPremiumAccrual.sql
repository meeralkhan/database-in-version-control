﻿CREATE TABLE [dbo].[t_MMPremiumAccrual] (
    [DealNo]      VARCHAR (30) NOT NULL,
    [RefID]       SMALLINT     NOT NULL,
    [OurBranchID] VARCHAR (30) NOT NULL,
    [FromDate]    VARCHAR (10) NOT NULL,
    [ToDate]      VARCHAR (10) NOT NULL,
    [Amount]      MONEY        NOT NULL,
    [Days]        SMALLINT     NOT NULL,
    [IsPost]      CHAR (1)     NOT NULL,
    [IsPaid]      CHAR (1)     NOT NULL
);

