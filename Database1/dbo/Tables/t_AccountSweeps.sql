﻿CREATE TABLE [dbo].[t_AccountSweeps] (
    [OurBranchID]    NVARCHAR (30)  NOT NULL,
    [AccountID]      NVARCHAR (30)  NOT NULL,
    [SerialNo]       DECIMAL (24)   NOT NULL,
    [ReferenceID]    NVARCHAR (450) NOT NULL,
    [Type]           CHAR (1)       NOT NULL,
    [Action]         CHAR (1)       NOT NULL,
    [SIAccountID]    NVARCHAR (30)  NULL,
    [Amount]         MONEY          NULL,
    [CreateBy]       NVARCHAR (30)  NOT NULL,
    [CreateTime]     DATETIME       NOT NULL,
    [CreateTerminal] NVARCHAR (30)  NOT NULL,
    [SIAccountID2]   NVARCHAR (30)  NULL,
    [Comments]       NVARCHAR (250) NULL,
    [ChargesTrxRef]  NVARCHAR (100) NULL,
    CONSTRAINT [PK_t_AccountSweeps] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [AccountID] ASC, [SerialNo] ASC)
);

