﻿CREATE TABLE [dbo].[SwiftMessagesLog] (
    [Id]            BIGINT        IDENTITY (1, 1) NOT NULL,
    [Json_Message]  VARCHAR (MAX) NULL,
    [Message_Type]  VARCHAR (200) NULL,
    [Status]        VARCHAR (200) NULL,
    [Comments]      VARCHAR (MAX) NULL,
    [Reference]     VARCHAR (200) NULL,
    [Txn_Id]        VARCHAR (200) NULL,
    [Submitted_At]  DATETIME      NULL,
    [SQLPostedDate] DATETIME      NULL,
    CONSTRAINT [PK_SwiftMessagesLog] PRIMARY KEY CLUSTERED ([Id] ASC)
);

