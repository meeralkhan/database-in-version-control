﻿CREATE TABLE [dbo].[t_FailedEmails] (
    [OurBranchID]  NVARCHAR (30) NOT NULL,
    [eSerialNo]    DECIMAL (24)  NOT NULL,
    [SerialNo]     DECIMAL (24)  NOT NULL,
    [ErrorMessage] TEXT          NOT NULL,
    [eDateTime]    DATETIME      NOT NULL,
    [eTerminal]    NVARCHAR (30) NOT NULL,
    CONSTRAINT [PK_t_FailedEmails] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [eSerialNo] ASC, [SerialNo] ASC)
);

