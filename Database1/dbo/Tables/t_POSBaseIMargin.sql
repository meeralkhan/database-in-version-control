﻿CREATE TABLE [dbo].[t_POSBaseIMargin] (
    [CreateBy]          NVARCHAR (30)   NOT NULL,
    [CreateTime]        DATETIME        NOT NULL,
    [CreateTerminal]    NVARCHAR (30)   NOT NULL,
    [UpdateBy]          NVARCHAR (30)   NULL,
    [UpdateTime]        DATETIME        NULL,
    [UpdateTerminal]    NVARCHAR (30)   NULL,
    [Auth]              INT             NOT NULL,
    [AuthStatus]        CHAR (1)        NOT NULL,
    [SuperviseBy]       NVARCHAR (30)   NULL,
    [SuperviseTime]     DATETIME        NULL,
    [SuperviseTerminal] NVARCHAR (30)   NULL,
    [OurBranchID]       NVARCHAR (30)   NOT NULL,
    [SerialNo]          DECIMAL (24)    NOT NULL,
    [Percent]           DECIMAL (22, 2) NOT NULL,
    [MaxAmount]         DECIMAL (18, 6) NOT NULL,
    [frmName]           VARCHAR (100)   NULL,
    [VerifyBy]          NVARCHAR (30)   NULL,
    [VerifyTime]        DATETIME        NULL,
    [VerifyTerminal]    NVARCHAR (30)   NULL,
    [VerifyStatus]      CHAR (1)        NOT NULL,
    CONSTRAINT [PK_t_POSBaseIMargin] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [SerialNo] ASC)
);

