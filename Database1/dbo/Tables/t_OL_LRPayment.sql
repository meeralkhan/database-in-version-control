﻿CREATE TABLE [dbo].[t_OL_LRPayment] (
    [ScrollNo]       INT           NOT NULL,
    [DocumentType]   VARCHAR (2)   NOT NULL,
    [IssueBranchID]  VARCHAR (30)  NOT NULL,
    [wDate]          DATETIME      NOT NULL,
    [AccountID]      VARCHAR (30)  NOT NULL,
    [AccountName]    VARCHAR (100) NOT NULL,
    [InstrumentType] VARCHAR (50)  NOT NULL,
    [IssueDate]      DATETIME      NOT NULL,
    [InstrumentNo]   INT           NOT NULL,
    [ControlNo]      INT           NOT NULL,
    [Amount]         MONEY         NOT NULL,
    [ForeignAmount]  MONEY         NOT NULL,
    [Beneficiary]    VARCHAR (100) NOT NULL,
    [DrawanBank]     VARCHAR (100) NOT NULL,
    [DrawanBranch]   VARCHAR (100) NOT NULL,
    [BankID]         VARCHAR (30)  NOT NULL,
    [BranchID]       VARCHAR (30)  NOT NULL,
    CONSTRAINT [PK_t_OL_LRPayment] PRIMARY KEY CLUSTERED ([ScrollNo] ASC, [DocumentType] ASC, [IssueBranchID] ASC, [wDate] ASC)
);

