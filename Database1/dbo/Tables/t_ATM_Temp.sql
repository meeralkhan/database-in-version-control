﻿CREATE TABLE [dbo].[t_ATM_Temp] (
    [OurBranchID]  VARCHAR (30) NOT NULL,
    [AccountID]    VARCHAR (30) NOT NULL,
    [ValueDate]    DATETIME     NOT NULL,
    [wDate]        DATETIME     NOT NULL,
    [Amount]       MONEY        NOT NULL,
    [TrxType]      VARCHAR (30) NOT NULL,
    [TrxDesc]      VARCHAR (30) NOT NULL,
    [DocumentType] VARCHAR (30) NULL
);

