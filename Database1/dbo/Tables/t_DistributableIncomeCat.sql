﻿CREATE TABLE [dbo].[t_DistributableIncomeCat] (
    [CreateBy]          NVARCHAR (30)  NOT NULL,
    [CreateTime]        DATETIME       NOT NULL,
    [CreateTerminal]    NVARCHAR (30)  NOT NULL,
    [UpdateBy]          NVARCHAR (30)  NULL,
    [UpdateTime]        DATETIME       NULL,
    [UpdateTerminal]    NVARCHAR (30)  NULL,
    [AuthStatus]        CHAR (1)       NOT NULL,
    [VerifyBy]          NVARCHAR (30)  NULL,
    [VerifyTime]        DATETIME       NULL,
    [VerifyTerminal]    NVARCHAR (30)  NULL,
    [VerifyStatus]      CHAR (1)       NOT NULL,
    [SuperviseBy]       NVARCHAR (30)  NULL,
    [SuperviseTime]     DATETIME       NULL,
    [SuperviseTerminal] NVARCHAR (30)  NULL,
    [frmName]           NVARCHAR (100) NOT NULL,
    [OurBranchID]       NVARCHAR (30)  NOT NULL,
    [CategoryID]        DECIMAL (24)   IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [CategoryName]      NVARCHAR (50)  NULL,
    [OrderNo]           DECIMAL (24)   NULL,
    CONSTRAINT [PK_t_DistributableIncomeCat] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [CategoryID] ASC)
);

