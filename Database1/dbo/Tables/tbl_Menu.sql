﻿CREATE TABLE [dbo].[tbl_Menu] (
    [MenuId]       VARCHAR (30)  NOT NULL,
    [LevelId]      INT           NOT NULL,
    [ParentId]     VARCHAR (30)  NOT NULL,
    [Label_EN]     VARCHAR (100) NOT NULL,
    [Label_AR]     VARCHAR (100) NULL,
    [Label_UR]     VARCHAR (100) NULL,
    [MenuType]     CHAR (1)      NOT NULL,
    [LinkId]       VARCHAR (100) NOT NULL,
    [TabIcon]      VARCHAR (100) NOT NULL,
    [OrderNo]      INT           NOT NULL,
    [NewWindow]    INT           NULL,
    [frmOnDisplay] VARCHAR (100) NULL,
    [frmName]      VARCHAR (100) NULL,
    CONSTRAINT [PK_tbl_Menu] PRIMARY KEY CLUSTERED ([MenuId] ASC)
);

