﻿CREATE TABLE [dbo].[t_ChequePaid] (
    [OurBranchID]       NVARCHAR (30) NOT NULL,
    [AccountID]         NVARCHAR (30) NOT NULL,
    [ChequeID]          NVARCHAR (30) NOT NULL,
    [Date]              DATETIME      NOT NULL,
    [AccountType]       CHAR (1)      NOT NULL,
    [CreateBy]          NVARCHAR (30) NOT NULL,
    [CreateTime]        DATETIME      NOT NULL,
    [CreateTerminal]    NVARCHAR (30) NOT NULL,
    [SuperviseBy]       NVARCHAR (30) NULL,
    [SuperviseTime]     DATETIME      NULL,
    [SuperviseTerminal] NVARCHAR (30) NULL,
    [frmName]           VARCHAR (100) NULL,
    [TrxTimeStamp]      DATETIME      NOT NULL,
    CONSTRAINT [PK_t_ChequePaid] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [AccountID] ASC, [ChequeID] ASC)
);

