﻿CREATE TABLE [dbo].[t_FCYEODPnLLogs] (
    [OurBranchID] NVARCHAR (50)  NOT NULL,
    [ID]          INT            IDENTITY (1, 1) NOT NULL,
    [Description] NVARCHAR (MAX) NULL,
    [ScrollNo]    NVARCHAR (50)  NULL,
    [IsError]     BIT            NOT NULL,
    [Remarks]     NVARCHAR (500) NULL,
    [wDate]       DATETIME       NULL,
    [AccountID]   NVARCHAR (30)  NULL,
    CONSTRAINT [PK_t_FCYEODPnLLogs] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [ID] ASC)
);

