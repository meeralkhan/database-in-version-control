﻿CREATE TABLE [dbo].[t_StandingOrders] (
    [CreateBy]       NVARCHAR (30) NOT NULL,
    [CreateTime]     DATETIME      NOT NULL,
    [CreateTerminal] NVARCHAR (30) NOT NULL,
    [UpdateBy]       NVARCHAR (30) NULL,
    [UpdateTime]     DATETIME      NULL,
    [UpdateTerminal] NVARCHAR (30) NULL,
    [OurBranchID]    NVARCHAR (30) NOT NULL,
    [AccountID]      NVARCHAR (30) NOT NULL,
    [ZXAccountID]    NVARCHAR (30) NOT NULL,
    [Type]           NVARCHAR (10) NOT NULL,
    [AutoRenewal]    NVARCHAR (5)  NOT NULL,
    CONSTRAINT [PK_t_StandingOrders] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [AccountID] ASC)
);

