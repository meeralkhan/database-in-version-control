﻿CREATE TABLE [dbo].[t_RateIP0_Auth] (
    [CreateBy]          NVARCHAR (30)   NOT NULL,
    [CreateTime]        DATETIME        NOT NULL,
    [CreateTerminal]    NVARCHAR (30)   NOT NULL,
    [UpdateBy]          NVARCHAR (30)   NULL,
    [UpdateTime]        DATETIME        NULL,
    [UpdateTerminal]    NVARCHAR (30)   NULL,
    [AuthStatus]        CHAR (1)        NOT NULL,
    [VerifyBy]          NVARCHAR (30)   NULL,
    [VerifyTime]        DATETIME        NULL,
    [VerifyTerminal]    NVARCHAR (30)   NULL,
    [VerifyStatus]      CHAR (1)        NOT NULL,
    [SuperviseBy]       NVARCHAR (30)   NULL,
    [SuperviseTime]     DATETIME        NULL,
    [SuperviseTerminal] NVARCHAR (30)   NULL,
    [frmName]           NVARCHAR (100)  NOT NULL,
    [OurBranchID]       NVARCHAR (30)   NOT NULL,
    [AccountID]         NVARCHAR (30)   NOT NULL,
    [DealID]            DECIMAL (24)    NOT NULL,
    [ColumnID]          NVARCHAR (30)   NOT NULL,
    [ChangeDate]        DATETIME        NULL,
    [Rate1]             MONEY           NULL,
    [Margin]            DECIMAL (18, 6) NULL,
    [RateType]          NVARCHAR (30)   NULL,
    [DealRateType]      NVARCHAR (20)   NULL,
    CONSTRAINT [PK_t_RateIP0_Auth] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [AccountID] ASC, [DealID] ASC, [ColumnID] ASC)
);

