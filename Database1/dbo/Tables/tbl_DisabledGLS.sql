﻿CREATE TABLE [dbo].[tbl_DisabledGLS] (
    [OurBranchID]  NVARCHAR (30)  NOT NULL,
    [AccountID]    NVARCHAR (30)  NOT NULL,
    [AccountClass] NVARCHAR (30)  NULL,
    [Description]  NVARCHAR (100) NULL,
    [CurrencyID]   NVARCHAR (30)  NULL,
    [GLOwnerID]    NVARCHAR (30)  NULL,
    [OldIsPosting] INT            NULL,
    [IsPosting]    INT            NOT NULL,
    [wDate]        DATETIME       NOT NULL,
    [LogID]        INT            NOT NULL
);

