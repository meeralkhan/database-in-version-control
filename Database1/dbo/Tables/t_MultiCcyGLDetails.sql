﻿CREATE TABLE [dbo].[t_MultiCcyGLDetails] (
    [CurrencyID]   NVARCHAR (30)  NOT NULL,
    [CurrencyCode] NVARCHAR (20)  NOT NULL,
    [AccountID]    NVARCHAR (30)  NOT NULL,
    [OurBranchID]  NVARCHAR (30)  NOT NULL,
    [Description]  NVARCHAR (100) NOT NULL,
    [AuthStatus]   CHAR (1)       NULL,
    CONSTRAINT [PK_t_MultiCcyGLDetails] PRIMARY KEY CLUSTERED ([CurrencyID] ASC, [CurrencyCode] ASC, [AccountID] ASC, [OurBranchID] ASC)
);

