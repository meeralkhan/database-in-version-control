﻿CREATE TABLE [dbo].[TmpDisabledGL] (
    [AccountID]      NVARCHAR (30)  NOT NULL,
    [Description]    NVARCHAR (100) NULL,
    [OurBranchID]    NVARCHAR (30)  NOT NULL,
    [AccountType]    NVARCHAR (30)  NULL,
    [CurrencyID]     NVARCHAR (30)  NULL,
    [Balance]        MONEY          NULL,
    [ForeignBalance] MONEY          NULL,
    [LastTrxTime]    DATETIME       NULL,
    [DisabledDate]   DATE           NULL,
    [OwnerName]      NVARCHAR (100) NOT NULL,
    [Department]     NVARCHAR (100) NOT NULL,
    [IsPosting]      INT            NULL,
    [Maker]          NVARCHAR (30)  NULL,
    [Checker]        NVARCHAR (30)  NOT NULL
);

