﻿CREATE TABLE [dbo].[t_CustomerSicCodeCorp] (
    [OurBranchId] NVARCHAR (30) NOT NULL,
    [ClientID]    NVARCHAR (30) NOT NULL,
    [SicCodeCorp] NVARCHAR (30) NOT NULL,
    [Type]        NVARCHAR (1)  NOT NULL,
    CONSTRAINT [PK_t_CustomerSicCodeCorp] PRIMARY KEY CLUSTERED ([OurBranchId] ASC, [ClientID] ASC, [SicCodeCorp] ASC)
);

