﻿CREATE TABLE [dbo].[t_ShareHolders] (
    [CreateBy]                 NVARCHAR (30)   NOT NULL,
    [CreateTime]               DATETIME        NOT NULL,
    [CreateTerminal]           NVARCHAR (30)   NOT NULL,
    [UpdateBy]                 NVARCHAR (30)   NULL,
    [UpdateTime]               DATETIME        NULL,
    [UpdateTerminal]           NVARCHAR (30)   NULL,
    [AuthStatus]               CHAR (1)        NOT NULL,
    [VerifyBy]                 NVARCHAR (30)   NULL,
    [VerifyTime]               DATETIME        NULL,
    [VerifyTerminal]           NVARCHAR (30)   NULL,
    [VerifyStatus]             CHAR (1)        NOT NULL,
    [SuperviseBy]              NVARCHAR (30)   NULL,
    [SuperviseTime]            DATETIME        NULL,
    [SuperviseTerminal]        NVARCHAR (30)   NULL,
    [frmName]                  NVARCHAR (100)  NOT NULL,
    [OurBranchID]              NVARCHAR (30)   NOT NULL,
    [ShareHolderID]            DECIMAL (24)    IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ShareHolderName]          NVARCHAR (200)  NULL,
    [ShareHolderAddress]       NVARCHAR (500)  NULL,
    [ShareholderType]          NVARCHAR (9)    NULL,
    [ShareholdersIDExpirydate] DATETIME        NULL,
    [CountryofIncorporation]   NVARCHAR (30)   NULL,
    [BoardMember]              NVARCHAR (3)    NULL,
    [AuthorisedSignatory]      NVARCHAR (3)    NULL,
    [BeneficialOwner]          NVARCHAR (3)    NULL,
    [ResidentNonResident]      NVARCHAR (3)    NULL,
    [CountryofResidence]       NVARCHAR (30)   NULL,
    [OwnershipPercentage]      DECIMAL (22, 2) NULL,
    [Blacklist]                NVARCHAR (6)    NULL,
    [PEP]                      NVARCHAR (3)    NULL,
    [FATCARelevant]            NVARCHAR (3)    NULL,
    [CRSRelevant]              NVARCHAR (3)    NULL,
    [USCitizen]                NVARCHAR (3)    NULL,
    [AreyouaUSTaxResident]     NVARCHAR (3)    NULL,
    [WereyoubornintheUS]       NVARCHAR (3)    NULL,
    [USAEntity]                NVARCHAR (30)   NULL,
    [FFI]                      NVARCHAR (3)    NULL,
    [FFICategory]              NVARCHAR (6)    NULL,
    [GIINNo]                   NVARCHAR (30)   NULL,
    [GIINNA]                   NVARCHAR (3)    NULL,
    [SponsorName]              NVARCHAR (200)  NULL,
    [SponsorGIIN]              NVARCHAR (50)   NULL,
    [NFFE]                     NVARCHAR (3)    NULL,
    [StockExchange]            NVARCHAR (100)  NULL,
    [TradingSymbol]            NVARCHAR (50)   NULL,
    [USAORUAE]                 NVARCHAR (3)    NULL,
    [TINAvailable]             NVARCHAR (3)    NULL,
    [TaxCountry1]              NVARCHAR (30)   NULL,
    [TaxCountry2]              NVARCHAR (30)   NULL,
    [TaxCountry3]              NVARCHAR (30)   NULL,
    [TaxCountry4]              NVARCHAR (30)   NULL,
    [TaxCountry5]              NVARCHAR (30)   NULL,
    [FATCANoReason]            NVARCHAR (10)   NULL,
    [ReasonB]                  NVARCHAR (50)   NULL,
    [ShareholdersIDtype]       NVARCHAR (30)   NULL,
    [TinNo1]                   NVARCHAR (15)   NULL,
    [TinNo2]                   NVARCHAR (15)   NULL,
    [TinNo3]                   NVARCHAR (15)   NULL,
    [TinNo4]                   NVARCHAR (15)   NULL,
    [tinno5]                   NVARCHAR (15)   NULL,
    [ShareholdersIDNo]         NVARCHAR (30)   NULL,
    [SHCountry]                NVARCHAR (30)   NULL,
    [SHState]                  NVARCHAR (30)   NULL,
    [SHCity]                   NVARCHAR (30)   NULL,
    [SHIDIssuanceCountry]      NVARCHAR (30)   NULL,
    [IDGender]                 NVARCHAR (1)    NULL,
    [DateOfBirth]              DATETIME        NULL,
    [IDNationality]            NVARCHAR (30)   NULL,
    [JurEmiratesId]            NVARCHAR (30)   NULL,
    [JurTypeId]                NVARCHAR (30)   NULL,
    [JurAuthorityId]           NVARCHAR (30)   NULL,
    [GroupCodeType]            NVARCHAR (30)   NULL,
    [JurOther]                 NVARCHAR (50)   NULL,
    [POBox]                    NVARCHAR (30)   NULL,
    [ParentShareHolderId]      NVARCHAR (100)  NULL,
    [PlaceofBirth]             NVARCHAR (120)  NULL,
    [EntityType]               NVARCHAR (30)   NULL,
    CONSTRAINT [PK_t_ShareHolders] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [ShareHolderID] ASC)
);

