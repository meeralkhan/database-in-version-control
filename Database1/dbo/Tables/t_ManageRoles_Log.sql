﻿CREATE TABLE [dbo].[t_ManageRoles_Log] (
    [CreateBy]          NVARCHAR (30)  NULL,
    [CreateTime]        DATETIME       NULL,
    [CreateTerminal]    NVARCHAR (30)  NULL,
    [UpdateBy]          NVARCHAR (30)  NULL,
    [UpdateTime]        DATETIME       NULL,
    [UpdateTerminal]    NVARCHAR (30)  NULL,
    [AuthStatus]        CHAR (1)       NULL,
    [VerifyBy]          NVARCHAR (30)  NULL,
    [VerifyTime]        DATETIME       NULL,
    [VerifyTerminal]    NVARCHAR (30)  NULL,
    [VerifyStatus]      CHAR (1)       NULL,
    [SuperviseBy]       NVARCHAR (30)  NULL,
    [SuperviseTime]     DATETIME       NULL,
    [SuperviseTerminal] NVARCHAR (30)  NULL,
    [frmName]           NVARCHAR (100) NULL,
    [ReturnMessage]     TEXT           NULL,
    [OurBranchID]       NVARCHAR (30)  NOT NULL,
    [GUID]              NVARCHAR (50)  NOT NULL,
    [RoleID]            NVARCHAR (30)  NULL,
    [Description]       NVARCHAR (100) NULL,
    CONSTRAINT [PK_t_ManageRoles_Log] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [GUID] ASC)
);

