﻿CREATE TABLE [dbo].[StoredProcedureExecutionLog] (
    [LogId]            BIGINT       IDENTITY (1, 1) NOT NULL,
    [ReportName]       VARCHAR (50) NULL,
    [StartDate]        VARCHAR (50) NULL,
    [EndDate]          VARCHAR (50) NULL,
    [ReportParameter1] VARCHAR (50) NULL,
    [LogDate]          DATETIME     NULL,
    CONSTRAINT [PK_AutoExecutionLog] PRIMARY KEY CLUSTERED ([LogId] ASC)
);

