﻿CREATE TABLE [dbo].[t_LinkFeedbacks] (
    [CreateBy]       NVARCHAR (30) NOT NULL,
    [CreateTime]     DATETIME      NOT NULL,
    [CreateTerminal] NVARCHAR (30) NOT NULL,
    [OurBranchID]    NVARCHAR (30) NOT NULL,
    [SBPCode]        NVARCHAR (30) NOT NULL,
    [FeedbackID]     DECIMAL (24)  NOT NULL,
    [Status]         VARCHAR (10)  NOT NULL,
    CONSTRAINT [PK_t_LinkFeedbacks] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [SBPCode] ASC, [FeedbackID] ASC)
);

