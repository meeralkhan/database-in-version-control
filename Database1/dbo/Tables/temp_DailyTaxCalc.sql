﻿CREATE TABLE [dbo].[temp_DailyTaxCalc] (
    [AppWHTax]      BIT            NOT NULL,
    [ChargeRate]    MONEY          NOT NULL,
    [ChargesAmount] MONEY          NOT NULL,
    [GLSerialID]    NVARCHAR (MAX) NOT NULL,
    [AccountID]     VARCHAR (30)   NOT NULL,
    [Description]   VARCHAR (50)   NOT NULL,
    [CurrencyID]    VARCHAR (30)   NOT NULL
);

