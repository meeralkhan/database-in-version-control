﻿CREATE TABLE [dbo].[tbl_ReportsChild] (
    [ReportId]      VARCHAR (30)  NOT NULL,
    [FkTable]       VARCHAR (50)  NOT NULL,
    [Alias]         VARCHAR (10)  NULL,
    [JoinType]      VARCHAR (50)  NOT NULL,
    [JoinCondition] VARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_tbl_ReportsChild] PRIMARY KEY CLUSTERED ([ReportId] ASC, [FkTable] ASC)
);

