﻿CREATE TABLE [dbo].[t_MurabahahInvPool] (
    [CreateBy]          NVARCHAR (30)  NOT NULL,
    [CreateTime]        DATETIME       NOT NULL,
    [CreateTerminal]    NVARCHAR (30)  NOT NULL,
    [UpdateBy]          NVARCHAR (30)  NULL,
    [UpdateTime]        DATETIME       NULL,
    [UpdateTerminal]    NVARCHAR (30)  NULL,
    [AuthStatus]        CHAR (1)       NOT NULL,
    [SuperviseBy]       NVARCHAR (30)  NULL,
    [SuperviseTime]     DATETIME       NULL,
    [SuperviseTerminal] NVARCHAR (30)  NULL,
    [OurBranchID]       NVARCHAR (30)  NOT NULL,
    [PoolID]            NVARCHAR (30)  NOT NULL,
    [Description]       NVARCHAR (100) NULL,
    [frmName]           VARCHAR (100)  NULL,
    [VerifyBy]          NVARCHAR (30)  NULL,
    [VerifyTime]        DATETIME       NULL,
    [VerifyTerminal]    NVARCHAR (30)  NULL,
    [VerifyStatus]      CHAR (1)       NOT NULL,
    [PoolType]          NVARCHAR (30)  NULL,
    [StartDate]         DATETIME       NULL,
    [EndDate]           DATETIME       NULL,
    [Status]            NVARCHAR (1)   NULL,
    [RabulMaalShare]    MONEY          NULL,
    CONSTRAINT [PK_t_MurabahahInvPool] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [PoolID] ASC)
);

