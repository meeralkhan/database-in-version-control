﻿CREATE TABLE [dbo].[t_MismatchGLLogs] (
    [SerialNo]         INT           IDENTITY (1, 1) NOT NULL,
    [OurBranchId]      NVARCHAR (60) NOT NULL,
    [CurrencyId]       NVARCHAR (50) NOT NULL,
    [AccountId]        NVARCHAR (60) NOT NULL,
    [InvalidAccountId] NVARCHAR (60) NOT NULL,
    [TrxDateTime]      DATETIME      NOT NULL,
    [WorkingDate]      DATETIME      NOT NULL,
    CONSTRAINT [PK_t_MismatchGLLogs] PRIMARY KEY CLUSTERED ([SerialNo] ASC)
);

