﻿CREATE TABLE [dbo].[t_OL_TransactionStatus] (
    [OurBranchID]     VARCHAR (30)  NOT NULL,
    [ScrollNo]        NUMERIC (18)  NULL,
    [SerialNo]        INT           NULL,
    [RefNo]           VARCHAR (30)  NULL,
    [AccountID]       VARCHAR (30)  NOT NULL,
    [AccountName]     VARCHAR (100) NULL,
    [CurrencyID]      VARCHAR (30)  NULL,
    [Amount]          MONEY         NULL,
    [ForeignAmount]   MONEY         NULL,
    [ExchangeRate]    MONEY         NULL,
    [Status]          CHAR (1)      NULL,
    [Supervised]      BIT           NULL,
    [TrxType]         VARCHAR (2)   NULL,
    [IsLocalCurrency] BIT           NULL,
    [TransactionMode] CHAR (1)      NULL,
    [TransactionType] CHAR (1)      NULL,
    [OperatorID]      VARCHAR (30)  NULL
);

