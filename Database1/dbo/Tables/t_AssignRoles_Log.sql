﻿CREATE TABLE [dbo].[t_AssignRoles_Log] (
    [CreateBy]          NVARCHAR (30) NULL,
    [CreateTime]        DATETIME      NULL,
    [CreateTerminal]    NVARCHAR (30) NULL,
    [UpdateBy]          NVARCHAR (30) NULL,
    [UpdateTime]        DATETIME      NULL,
    [UpdateTerminal]    NVARCHAR (30) NULL,
    [SuperviseBy]       NVARCHAR (30) NULL,
    [SuperviseTime]     DATETIME      NULL,
    [SuperviseTerminal] NVARCHAR (30) NULL,
    [OurBranchID]       NVARCHAR (30) NOT NULL,
    [RoleID]            NVARCHAR (30) NOT NULL,
    [OperatorID]        NVARCHAR (30) NOT NULL,
    [Auth]              INT           NULL,
    [SerialNo]          INT           NOT NULL,
    CONSTRAINT [PK_t_AssignRoles_Log] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [RoleID] ASC, [OperatorID] ASC, [SerialNo] ASC)
);

