﻿CREATE TABLE [dbo].[t_CIFNumberAndTiers] (
    [CreateBy]          NVARCHAR (30)   NOT NULL,
    [CreateTime]        DATETIME        NOT NULL,
    [CreateTerminal]    NVARCHAR (30)   NOT NULL,
    [UpdateBy]          NVARCHAR (30)   NULL,
    [UpdateTime]        DATETIME        NULL,
    [UpdateTerminal]    NVARCHAR (30)   NULL,
    [AuthStatus]        CHAR (1)        NOT NULL,
    [SuperviseBy]       NVARCHAR (30)   NULL,
    [SuperviseTime]     DATETIME        NULL,
    [SuperviseTerminal] NVARCHAR (30)   NULL,
    [OurBranchID]       NVARCHAR (30)   NOT NULL,
    [CustomerNumber]    NVARCHAR (100)  NOT NULL,
    [TierName]          NVARCHAR (100)  NULL,
    [Rate]              NUMERIC (18, 6) NULL,
    [GLAccountID]       NVARCHAR (100)  NOT NULL,
    CONSTRAINT [PK_t_CIFNumberAndTiers] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [CustomerNumber] ASC)
);

