﻿CREATE TABLE [dbo].[t_OnLineIBCANo] (
    [IBCA]         INT          NOT NULL,
    [OurBranchID]  VARCHAR (30) NOT NULL,
    [OperatorID]   VARCHAR (30) NOT NULL,
    [TargetBranch] VARCHAR (30) NOT NULL,
    [IBDA]         INT          NOT NULL
);

