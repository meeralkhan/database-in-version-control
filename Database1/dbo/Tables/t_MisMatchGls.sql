﻿CREATE TABLE [dbo].[t_MisMatchGls] (
    [OurBranchId] NVARCHAR (60) NOT NULL,
    [CurrencyId]  NVARCHAR (60) NOT NULL,
    [AccountId]   NVARCHAR (60) NOT NULL,
    CONSTRAINT [PK_t_MisMatchGls] PRIMARY KEY CLUSTERED ([OurBranchId] ASC, [CurrencyId] ASC, [AccountId] ASC)
);

