﻿CREATE TABLE [dbo].[t_Disbursement] (
    [CreateBy]                  NVARCHAR (30)   NOT NULL,
    [CreateTime]                DATETIME        NOT NULL,
    [CreateTerminal]            NVARCHAR (30)   NOT NULL,
    [UpdateBy]                  NVARCHAR (30)   NULL,
    [UpdateTime]                DATETIME        NULL,
    [UpdateTerminal]            NVARCHAR (30)   NULL,
    [AuthStatus]                CHAR (1)        NOT NULL,
    [SuperviseBy]               NVARCHAR (30)   NULL,
    [SuperviseTime]             DATETIME        NULL,
    [SuperviseTerminal]         NVARCHAR (30)   NULL,
    [frmName]                   VARCHAR (50)    NULL,
    [OurBranchID]               NVARCHAR (30)   NOT NULL,
    [AccountID]                 NVARCHAR (30)   NOT NULL,
    [DealID]                    DECIMAL (24)    NOT NULL,
    [ReferenceNo]               NVARCHAR (200)  NULL,
    [DisbursementDate]          DATETIME        NULL,
    [FirstPaymentOn]            DATETIME        NULL,
    [AdvancesDate]              DATETIME        NULL,
    [MaturityDate]              DATETIME        NULL,
    [ProfitRateType]            NVARCHAR (20)   NULL,
    [ProfitRate]                MONEY           NULL,
    [Amount]                    MONEY           NULL,
    [Method]                    NVARCHAR (100)  NULL,
    [PaymentMethod]             NVARCHAR (50)   NULL,
    [PoolID]                    NVARCHAR (30)   NULL,
    [Remarks]                   NVARCHAR (800)  NULL,
    [TrxAccountType]            NVARCHAR (320)  NULL,
    [BaseRate]                  NVARCHAR (30)   NULL,
    [BasePercent]               MONEY           NULL,
    [Margin]                    MONEY           NULL,
    [ChargeID]                  NVARCHAR (30)   NULL,
    [BrokerID]                  NVARCHAR (30)   NULL,
    [Fee]                       MONEY           NULL,
    [Charges]                   MONEY           NULL,
    [TrxAccID]                  NVARCHAR (30)   NULL,
    [TrxGLAccID]                NVARCHAR (30)   NULL,
    [AuthStatus2]               NVARCHAR (10)   NULL,
    [ScrollNo]                  DECIMAL (24)    NULL,
    [CloseDate]                 DATETIME        NULL,
    [IsClosed]                  INT             NULL,
    [Status]                    NVARCHAR (10)   NULL,
    [StopDate]                  DATETIME        NULL,
    [StoppedBy]                 NVARCHAR (30)   NULL,
    [StopReason]                NVARCHAR (100)  NULL,
    [VerifyBy]                  NVARCHAR (30)   NULL,
    [VerifyTime]                DATETIME        NULL,
    [VerifyTerminal]            NVARCHAR (30)   NULL,
    [VerifyStatus]              CHAR (1)        NOT NULL,
    [GracePeriod]               DECIMAL (24)    NULL,
    [Exempt]                    DECIMAL (24)    NULL,
    [ExemptType]                NVARCHAR (10)   NULL,
    [ExemptPayType]             NVARCHAR (30)   NULL,
    [AssetValue]                MONEY           NULL,
    [CategoryID]                NVARCHAR (30)   NULL,
    [RiskCode]                  NVARCHAR (30)   NULL,
    [InstrumentType]            NVARCHAR (20)   NULL,
    [DrawnBank]                 NVARCHAR (30)   NULL,
    [DrawnBranch]               NVARCHAR (30)   NULL,
    [TTMode]                    NVARCHAR (20)   NULL,
    [InFavourOf]                NVARCHAR (100)  NULL,
    [RemitCharges]              MONEY           NULL,
    [LocalOpeningBalance]       MONEY           NULL,
    [LocalClosingBalance]       MONEY           NULL,
    [ForeignOpeningBalance]     MONEY           NULL,
    [ForeignClosingBalance]     MONEY           NULL,
    [BalanceMeanRate]           MONEY           NULL,
    [Accrual]                   MONEY           NULL,
    [tAccrual]                  MONEY           NOT NULL,
    [PAccrual]                  MONEY           NOT NULL,
    [tPAccrual]                 MONEY           NOT NULL,
    [AccrualUpto]               DATETIME        NULL,
    [FSVCollateralValue]        MONEY           NULL,
    [LiquidAssetValue]          MONEY           NULL,
    [ProvStage]                 CHAR (1)        NOT NULL,
    [ProvAmount]                MONEY           NOT NULL,
    [LastProvDate]              DATETIME        NULL,
    [LastPayDate]               DATETIME        NULL,
    [IsPreMatureClosed]         INT             NOT NULL,
    [SplitRate]                 MONEY           NULL,
    [FloorRate]                 MONEY           NULL,
    [CapRate]                   MONEY           NULL,
    [FacilityID]                NVARCHAR (100)  NULL,
    [minEsFee]                  DECIMAL (21, 3) NULL,
    [esFeePercentage]           DECIMAL (18, 6) NULL,
    [feesAccountID]             NVARCHAR (30)   NULL,
    [interestAccountID]         NVARCHAR (30)   NULL,
    [esFeeType]                 NVARCHAR (100)  NULL,
    [Tenure]                    NVARCHAR (200)  NULL,
    [TenureType]                NVARCHAR (200)  NULL,
    [AdvPenalRate]              DECIMAL (18, 6) NULL,
    [FacilitySerialID]          NVARCHAR (30)   NULL,
    [OutstandingBalance]        MONEY           NULL,
    [EffectiveDate]             DATETIME        NULL,
    [NextChangeDate]            DATETIME        NULL,
    [ECLStage]                  NVARCHAR (30)   NULL,
    [ECLAmount]                 MONEY           NULL,
    [SpecificProvisioning]      MONEY           NULL,
    [GeneralProvisioning]       MONEY           NULL,
    [DailyPenalInt]             MONEY           NULL,
    [DisableAutoClassification] INT             NULL,
    [NoOfDPD]                   DECIMAL (24)    NULL,
    CONSTRAINT [PK_t_Disbursement] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [AccountID] ASC, [DealID] ASC)
);


GO
CREATE   TRIGGER [dbo].[tr_Disbursement_Edit] ON [dbo].[t_Disbursement]
AFTER UPDATE
AS
BEGIN
   
   SET NOCOUNT ON
   
   IF UPDATE (ProfitRate)
   begin
      
	  declare @OurBranchID nvarchar(30)
      declare @ClientID nvarchar(30)
      declare @FacilityID decimal(24,0)
      declare @AccountID nvarchar(30)
      declare @ProductID nvarchar(30)
      declare @CurrencyID nvarchar(30)
      declare @DealID decimal(24,0)
      declare @DealRefNo nvarchar(30)
      declare @Limit money
      declare @EffectiveDate datetime
      declare @ProfitRateType nvarchar(30)
      declare @ProfitRate decimal(18,6)
      declare @BaseRate nvarchar(30)
      declare @BasePercent decimal(18,6)
      declare @Margin decimal(18,6)
      declare @FloorRate decimal(18,6)
      declare @CapRate decimal(18,6)
      declare @Limit1 money
      declare @Rate1 decimal(18,6)
      declare @ExcessRate decimal(18,6)
      declare @NextChangeDate datetime
      
	  select @OurBranchID=d.OurBranchID, @ClientID=a.ClientID, @FacilityID=FacilitySerialID, @AccountID=d.AccountID, @ProductID=a.ProductID, @CurrencyID=a.CurrencyID,
      @DealID=d.DealID, @DealRefNo=d.ReferenceNo, @Limit=Amount, @ProfitRateType=ProfitRateType, @ProfitRate=ProfitRate, @BaseRate=BaseRate, @BasePercent=BasePercent,
      @Margin=Margin, @FloorRate=FloorRate, @CapRate=CapRate, @Limit1=Amount, @Rate1=ProfitRate, @ExcessRate=AdvPenalRate, @NextChangeDate=NextChangeDate
      from inserted d
      inner join t_Account a on d.OurBranchID = a.OurBranchID and d.AccountID = a.AccountID
      
	  select @EffectiveDate = WorkingDate from t_Last where OurBranchID = @OurBranchID

	  declare @ReducedLimit money = 0
	  select @ReducedLimit = ISNULL(SUM(PrincipalAmount),0) from t_Adv_PaymentSchedule
	  where OurBranchID=@OurBranchID and AccountID=@AccountID and DealID=@DealID and PrincipalAmount > 0 and DueDate <= @EffectiveDate

	  set @Limit1 = @Limit1 - @ReducedLimit;
	  if @Limit1 < 0
	     set @Limit1 = 0

      delete from t_TermLoanLimits where OurBranchID = @OurBranchID and AccountID = @AccountID and DealID = @DealID and EffectiveDate = @EffectiveDate
      
      insert into t_TermLoanLimits (OurBranchID, ClientID, FacilityID, AccountID, ProductID, CurrencyID, DealID, DealRefNo, Limit, EffectiveDate, ProfitRateType,
      ProfitRate, BaseRate, BasePercent, Margin, FloorRate, CapRate, Limit1, Rate1, ExcessRate, LogDateTime, NextChangeDate)
      VALUES (@OurBranchID, @ClientID, @FacilityID, @AccountID, @ProductID, @CurrencyID, @DealID, @DealRefNo, @Limit, @EffectiveDate, @ProfitRateType, 
      @ProfitRate, @BaseRate, @BasePercent, @Margin, @FloorRate, @CapRate, @Limit1, @Rate1, @ExcessRate, GETDATE(),@NextChangeDate)

   end
END
GO
CREATE   TRIGGER [dbo].[tr_Disbursement_Add] ON [dbo].[t_Disbursement]
AFTER INSERT
AS
BEGIN
   
   SET NOCOUNT ON
   
   declare @OurBranchID nvarchar(30)
   declare @ClientID nvarchar(30)
   declare @FacilityID decimal(24,0)
   declare @AccountID nvarchar(30)
   declare @ProductID nvarchar(30)
   declare @CurrencyID nvarchar(30)
   declare @DealID decimal(24,0)
   declare @DealRefNo nvarchar(30)
   declare @Limit money
   declare @EffectiveDate datetime
   declare @ProfitRateType nvarchar(30)
   declare @ProfitRate decimal(18,6)
   declare @BaseRate nvarchar(30)
   declare @BasePercent decimal(18,6)
   declare @Margin decimal(18,6)
   declare @FloorRate decimal(18,6)
   declare @CapRate decimal(18,6)
   declare @Limit1 money
   declare @Rate1 decimal(18,6)
   declare @ExcessRate decimal(18,6)
   declare @NextChangeDate datetime
   
   select @OurBranchID=d.OurBranchID, @ClientID=a.ClientID, @FacilityID=FacilitySerialID, @AccountID=d.AccountID, @ProductID=a.ProductID, @CurrencyID=a.CurrencyID,
   @DealID=d.DealID, @DealRefNo=d.ReferenceNo, @Limit=Amount, @ProfitRateType=ProfitRateType, @ProfitRate=ProfitRate, @BaseRate=BaseRate, @BasePercent=BasePercent,
   @Margin=Margin, @FloorRate=FloorRate, @CapRate=CapRate, @Limit1=Amount, @Rate1=ProfitRate, @ExcessRate=AdvPenalRate, @NextChangeDate=NextChangeDate
   from inserted d
   inner join t_Account a on d.OurBranchID = a.OurBranchID and d.AccountID = a.AccountID

   select @EffectiveDate = WorkingDate from t_Last where OurBranchID = @OurBranchID
   
   delete from t_TermLoanLimits where OurBranchID = @OurBranchID and AccountID = @AccountID and DealID = @DealID and EffectiveDate = @EffectiveDate

   insert into t_TermLoanLimits (OurBranchID, ClientID, FacilityID, AccountID, ProductID, CurrencyID, DealID, DealRefNo, Limit, EffectiveDate, ProfitRateType,
   ProfitRate, BaseRate, BasePercent, Margin, FloorRate, CapRate, Limit1, Rate1, ExcessRate, LogDateTime, NextChangeDate)
   VALUES (@OurBranchID, @ClientID, @FacilityID, @AccountID, @ProductID, @CurrencyID, @DealID, @DealRefNo, @Limit, @EffectiveDate, @ProfitRateType, 
   @ProfitRate, @BaseRate, @BasePercent, @Margin, @FloorRate, @CapRate, @Limit1, @Rate1, @ExcessRate, GETDATE(),@NextChangeDate)
   
END