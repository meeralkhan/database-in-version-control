﻿CREATE TABLE [dbo].[t_Last] (
    [OurBranchID] NVARCHAR (30) NOT NULL,
    [LASTEOD]     DATETIME      NULL,
    [LASTBOD]     DATETIME      NULL,
    [EODEND]      DATETIME      NULL,
    [CONFRETI]    DATETIME      NULL,
    [CONFRETB]    DATETIME      NULL,
    [CONFRETT]    DATETIME      NULL,
    [LASTDAY]     DATETIME      NULL,
    [LASTDAYBAK]  DATETIME      NULL,
    [LASTEODBAK]  DATETIME      NULL,
    [LSTAGEFFDT]  DATETIME      NULL,
    [LSTODINTDT]  DATETIME      NULL,
    [LASTINTDT]   DATETIME      NULL,
    [INTDUEON]    DATETIME      NULL,
    [STARTYEAR]   DATETIME      NULL,
    [ENDYEAR]     DATETIME      NULL,
    [EOMCALCDT]   DATETIME      NULL,
    [EOMUPDATE]   DATETIME      NULL,
    [WORKINGDATE] DATETIME      NULL,
    [frmName]     VARCHAR (100) NULL
);

