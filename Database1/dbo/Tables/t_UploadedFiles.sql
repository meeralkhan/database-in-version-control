﻿CREATE TABLE [dbo].[t_UploadedFiles] (
    [UploadID]       VARCHAR (30)   NOT NULL,
    [UploadPath]     NVARCHAR (MAX) NOT NULL,
    [CreateBy]       NVARCHAR (30)  NOT NULL,
    [CreateTime]     DATETIME       NOT NULL,
    [CreateTerminal] NVARCHAR (30)  NOT NULL,
    CONSTRAINT [PK_t_UploadedFiles] PRIMARY KEY CLUSTERED ([UploadID] ASC)
);

