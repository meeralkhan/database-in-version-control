﻿CREATE TABLE [dbo].[t_UploadDepCsv] (
    [SerialNo]       DECIMAL (24)    IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ReferenceNo]    NVARCHAR (100)  NOT NULL,
    [Dated]          DATETIME        NOT NULL,
    [BranchID]       NVARCHAR (50)   NOT NULL,
    [ProductID]      NVARCHAR (50)   NOT NULL,
    [AccountID]      NVARCHAR (50)   NOT NULL,
    [AccountBalance] DECIMAL (21, 3) NOT NULL,
    [AccountStatus]  CHAR (1)        NOT NULL,
    [MinSlabAmount]  DECIMAL (18, 6) NULL,
    [MaxSlabAmount]  DECIMAL (18, 6) NULL,
    [CloseDate]      DATETIME        NULL,
    [CloseRefNo]     NVARCHAR (100)  NULL,
    [UpdateTime]     DATETIME        NULL,
    [ClosingBalance] DECIMAL (21, 3) NULL,
    [ValueDate]      DATETIME        NULL,
    [ProfitAmount]   DECIMAL (21, 3) NULL,
    [LinkAccount]    NVARCHAR (50)   NULL,
    [ProfitUpto]     DATETIME        NULL,
    CONSTRAINT [PK_t_UploadDepCsv] PRIMARY KEY CLUSTERED ([SerialNo] ASC)
);

