﻿CREATE TABLE [dbo].[t_AccountBalanceHistory] (
    [wDate]             DATETIME       NOT NULL,
    [OurBranchID]       VARCHAR (30)   NOT NULL,
    [AccountID]         VARCHAR (30)   NOT NULL,
    [ProductID]         VARCHAR (30)   NOT NULL,
    [AccountName]       NVARCHAR (200) NULL,
    [ClearBalance]      MONEY          NOT NULL,
    [LocalClearBalance] MONEY          NOT NULL,
    [Effects]           MONEY          NOT NULL,
    [LocalEffects]      MONEY          NOT NULL,
    [Limit]             MONEY          NOT NULL,
    [OverdraftClassID]  NVARCHAR (30)  NOT NULL,
    [NoOfDPD]           DECIMAL (24)   NOT NULL,
    CONSTRAINT [PK_t_AccountBalanceHistory] PRIMARY KEY CLUSTERED ([wDate] ASC, [OurBranchID] ASC, [AccountID] ASC)
);

