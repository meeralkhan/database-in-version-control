﻿CREATE TABLE [dbo].[t_ZXAutoRenewelLogs] (
    [SerialNo]    DECIMAL (24)   IDENTITY (1, 1) NOT NULL,
    [OurBranchID] NVARCHAR (30)  NOT NULL,
    [ZXAccountID] NVARCHAR (30)  NOT NULL,
    [AccountID]   NVARCHAR (30)  NOT NULL,
    [wDate]       DATETIME       NOT NULL,
    [DateTime]    DATETIME       NOT NULL,
    [Status]      NVARCHAR (50)  NULL,
    [ScrollNo]    NVARCHAR (50)  NULL,
    [JsonData]    NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_t_ZXAutoRenewelLogs] PRIMARY KEY CLUSTERED ([SerialNo] ASC, [OurBranchID] ASC, [AccountID] ASC, [wDate] ASC)
);

