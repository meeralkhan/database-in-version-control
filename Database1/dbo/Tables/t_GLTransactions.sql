﻿CREATE TABLE [dbo].[t_GLTransactions] (
    [OurBranchID]         NVARCHAR (30)   NOT NULL,
    [RefNo]               VARCHAR (100)   NOT NULL,
    [AccountID]           NVARCHAR (30)   NOT NULL,
    [VoucherID]           NUMERIC (10)    NOT NULL,
    [SerialID]            NUMERIC (10)    NOT NULL,
    [Date]                DATETIME        NOT NULL,
    [ValueDate]           DATETIME        NULL,
    [DescriptionID]       VARCHAR (50)    NOT NULL,
    [Description]         NVARCHAR (500)  NULL,
    [CurrencyID]          VARCHAR (30)    NOT NULL,
    [Amount]              MONEY           NOT NULL,
    [ForeignAmount]       MONEY           NOT NULL,
    [ExchangeRate]        DECIMAL (16, 8) NULL,
    [IsCredit]            BIT             NOT NULL,
    [TransactionType]     VARCHAR (32)    NOT NULL,
    [TransactionMethod]   CHAR (1)        NULL,
    [OperatorID]          VARCHAR (30)    NOT NULL,
    [SupervisorID]        VARCHAR (30)    NOT NULL,
    [Indicator]           CHAR (2)        NOT NULL,
    [ScrollNo]            INT             NOT NULL,
    [SerialNo]            INT             NOT NULL,
    [AdditionalData]      TEXT            NULL,
    [Status]              CHAR (1)        NOT NULL,
    [IsMainTrx]           SMALLINT        NOT NULL,
    [DocType]             CHAR (3)        NOT NULL,
    [GlID]                NVARCHAR (30)   NOT NULL,
    [IDRefNo]             NVARCHAR (30)   NOT NULL,
    [SourceBranch]        VARCHAR (30)    NOT NULL,
    [TargetBranch]        VARCHAR (30)    NOT NULL,
    [ClearingType]        CHAR (1)        NOT NULL,
    [RegisterDate]        DATETIME        NULL,
    [Remarks]             TEXT            NOT NULL,
    [MerchantType]        VARCHAR (30)    NOT NULL,
    [CreditCardNumber]    VARCHAR (30)    NOT NULL,
    [STAN]                VARCHAR (30)    NOT NULL,
    [CompanyNumber]       VARCHAR (50)    NOT NULL,
    [ChannelId]           NVARCHAR (30)   NOT NULL,
    [VirtualAccountID]    NVARCHAR (30)   NOT NULL,
    [VirtualAccountTitle] NVARCHAR (100)  NOT NULL,
    [ChannelRefID]        VARCHAR (200)   NULL,
    [ConvRate]            DECIMAL (15, 9) NULL,
    [CurrCodeTran]        VARCHAR (3)     NULL,
    [AcqCountryCode]      VARCHAR (3)     NULL,
    [CurrCodeSett]        VARCHAR (3)     NULL,
    [SettlmntAmount]      NUMERIC (18, 6) NULL,
    [POSEntryMode]        VARCHAR (3)     NULL,
    [POSConditionCode]    VARCHAR (2)     NULL,
    [POSPINCaptCode]      VARCHAR (2)     NULL,
    [AcqInstID]           VARCHAR (15)    NULL,
    [RetRefNum]           VARCHAR (15)    NULL,
    [AuthIDResp]          VARCHAR (6)     NULL,
    [CardAccptID]         VARCHAR (15)    NULL,
    [CardAccptNameLoc]    VARCHAR (50)    NULL,
    [VISATrxID]           VARCHAR (15)    NULL,
    [ProcCode]            VARCHAR (50)    NULL,
    [TrxTimeStamp]        DATETIME        NOT NULL,
    [TrxDesc]             VARCHAR (2)     NULL,
    [MCC]                 VARCHAR (10)    NULL,
    [CAVV]                CHAR (1)        NULL,
    [ResponseCode]        VARCHAR (2)     NULL,
    [ForwardInstID]       VARCHAR (10)    NULL,
    [PrevDayEntry]        INT             NULL,
    [VatID]               NVARCHAR (30)   NULL,
    [Percent]             MONEY           NULL,
    [RejectRemarks]       NVARCHAR (200)  NULL,
    [PrevYearEntry]       INT             NULL,
    [TClientId]           NVARCHAR (30)   NULL,
    [TAccountid]          NVARCHAR (30)   NULL,
    [TProductId]          NVARCHAR (30)   NULL,
    [CostCenterID]        NVARCHAR (30)   NULL,
    [IsYearEndProcess]    INT             NULL,
    [GLVendorID]          NVARCHAR (30)   NULL,
    [memo2]               NVARCHAR (250)  NULL,
    [VendorID]            NVARCHAR (30)   NULL,
    [CostTypeID]          NVARCHAR (30)   NULL,
    [CustomerLifecycleID] NVARCHAR (30)   NULL,
    [ProjectId]           NVARCHAR (100)  NULL,
    [PostExpenseID]       NVARCHAR (30)   NULL,
    [VatReferenceID]      NVARCHAR (30)   NULL
);


GO
CREATE TRIGGER [dbo].[tr_GLTransactionIns] ON [dbo].[t_GLTransactions]       
FOR INSERT      
as      
 declare @LocalCurrency varchar(4)      
 select @localCurrency=LocalCurrency From t_GlobalVariables  
  
 declare @PrevDayEntry int  
 select @PrevDayEntry = ISNULL(PrevDayEntry,0) from inserted  
  
 if (select TransactionMethod from inserted ) = 'L'      
 BEGIN      
 if (select Indicator from inserted ) = '*'      
 BEGIN      
  if (select Iscredit from inserted) = 1           
  BEGIN        
  Update t_gl      
  SET t_gl.ShadowBalance = t_gl.ShadowBalance +  (select Amount from Inserted)      
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances      
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance +  (select Amount from Inserted)      
       WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
  else       
  BEGIN        
  Update t_gl      
  SET t_gl.ShadowBalance = t_gl.ShadowBalance -  (select Amount from Inserted)      
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances      
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance -  (select Amount from Inserted)      
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
 END      
 else      
 BEGIN      
  if (select Iscredit from inserted) = 1           
  BEGIN        
  Update t_gl      
  SET t_gl.Balance = t_gl.Balance +  (select Amount from Inserted)      
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances      
       SET t_PrevGLBalances.Balance = t_PrevGLBalances.Balance +  (select Amount from Inserted)  
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
  else       
  BEGIN        
  Update t_gl      
  SET t_gl.Balance = t_gl.Balance -  (select Amount from Inserted)      
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances      
       SET t_PrevGLBalances.Balance = t_PrevGLBalances.Balance -  (select Amount from Inserted)      
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
 END      
      
 END      
 else      
 if (select TransactionMethod from inserted ) = 'F'      
 BEGIN      
 if (select Indicator from inserted ) = '*'      
 BEGIN      
  if (select Iscredit from inserted) = 1           
  BEGIN        
  Update t_gl      
  SET t_gl.ShadowBalance = t_gl.ShadowBalance +  (select ForeignAmount from Inserted)      
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances      
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance +  (select ForeignAmount from Inserted)      
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
  else       
  BEGIN        
  Update t_gl      
  SET t_gl.ShadowBalance = t_gl.ShadowBalance -  (select ForeignAmount from Inserted)      
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances      
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance -  (select ForeignAmount from Inserted)      
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
 END      
 else      
 BEGIN      
  if (select Iscredit from inserted) = 1           
  BEGIN        
  Update t_gl      
  SET t_gl.ForeignBalance = t_gl.ForeignBalance +  (select ForeignAmount from Inserted),      
  t_gl.Balance = t_gl.Balance +  (select Amount from Inserted)       
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances      
       SET t_PrevGLBalances.ForeignBalance = t_PrevGLBalances.ForeignBalance +  (select ForeignAmount from Inserted),  
    t_PrevGLBalances.Balance = t_PrevGLBalances.Balance +  (select Amount from Inserted)       
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
  else       
  BEGIN        
  Update t_gl      
  SET t_gl.ForeignBalance = t_gl.foreignBalance -  (select ForeignAmount from Inserted),      
  t_gl.Balance = t_gl.Balance +  (select Amount from Inserted)       
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances      
       SET t_PrevGLBalances.ForeignBalance = t_PrevGLBalances.foreignBalance -  (select ForeignAmount from Inserted),  
    t_PrevGLBalances.Balance = t_PrevGLBalances.Balance +  (select Amount from Inserted)       
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
 END      
      
 END      
 else      
 if (select TransactionMethod from inserted)='A'      
 BEGIN      
 if (select currencyid from inserted) <> @LocalCurrency      
 BEGIN      
 if (select Indicator from inserted ) = '*'      
 BEGIN      
  if (select Iscredit from inserted) = 1           
  BEGIN        
  Update t_gl      
  SET t_gl.ShadowBalance = t_gl.ShadowBalance +  (select ForeignAmount from Inserted)      
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance +  (select ForeignAmount from Inserted)      
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
  else       
  BEGIN        
  Update t_gl      
  SET t_gl.ShadowBalance = t_gl.ShadowBalance -  (select ForeignAmount  from Inserted)      
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance -  (select ForeignAmount  from Inserted)      
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
 END      
 else      
 BEGIN      
  if (select Iscredit from inserted) = 1           
  BEGIN        
  Update t_gl      
  SET t_gl.ForeignBalance = t_gl.ForeignBalance +  (select ForeignAmount from Inserted),      
  t_gl.Balance = t_gl.Balance +  (select Amount  from Inserted)      
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ForeignBalance = t_PrevGLBalances.ForeignBalance +  (select ForeignAmount from Inserted),  
    t_PrevGLBalances.Balance = t_PrevGLBalances.Balance +  (select Amount  from Inserted)      
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
  else       
  BEGIN        
  Update t_gl      
  SET t_gl.ForeignBalance = t_gl.foreignBalance -  (select ForeignAmount from Inserted),      
  t_gl.Balance = t_gl.Balance -  (select Amount from Inserted)      
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ForeignBalance = t_PrevGLBalances.foreignBalance -  (select ForeignAmount from Inserted),  
    t_PrevGLBalances.Balance = t_PrevGLBalances.Balance -  (select Amount from Inserted)      
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
 END      
      
 END      
 else      
 BEGIN      
 if (select Indicator from inserted ) = '*'      
 BEGIN      
  if (select Iscredit from inserted) = 1           
  BEGIN        
  Update t_gl      
  SET t_gl.ShadowBalance = t_gl.ShadowBalance +  (select Amount from Inserted)      
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance +  (select Amount from Inserted)      
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
  else       
  BEGIN        
  Update t_gl      
  SET t_gl.ShadowBalance = t_gl.ShadowBalance -  (select Amount  from Inserted)      
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance -  (select Amount  from Inserted)      
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
 END      
 else      
 BEGIN      
  if (select Iscredit from inserted) = 1           
  BEGIN        
  Update t_gl      
  SET t_gl.Balance = t_gl.Balance +  (select Amount from Inserted)             
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.Balance = t_PrevGLBalances.Balance +  (select Amount from Inserted)             
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
  else       
  BEGIN        
  Update t_gl      
  SET t_gl.Balance = t_gl.Balance -  (select Amount  from Inserted)             
  WHERE t_gl.AccountID= (select AccountId from inserted) AND      
  t_gl.OurBranchID = (select OurBranchID from inserted)      
    IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.Balance = t_PrevGLBalances.Balance -  (select Amount  from Inserted)             
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from inserted)  
    AND t_PrevGLBalances.AccountID= (select AccountId from inserted)  
  END      
 END      
      
 END      
END
GO
CREATE TRIGGER [dbo].[tr_GLTransactionDelete] ON [dbo].[t_GLTransactions]         
FOR DELETE        
as        
 declare @LocalCurrency varchar(4)        
 select @localCurrency=LocalCurrency From t_GlobalVariables        
   
 declare @PrevDayEntry int    
 select @PrevDayEntry = ISNULL(PrevDayEntry,0) from deleted    
  
 if (select TransactionMethod from deleted ) = 'L'        
 BEGIN        
 if (select Indicator from deleted ) = '*'        
 BEGIN        
  if (select Iscredit from deleted ) = 1             
  BEGIN          
  Update t_gl        
  SET t_gl.ShadowBalance = t_gl.ShadowBalance -  (select Amount from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance -  (select Amount from deleted )  
       WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
  END        
  else         
  BEGIN          
  Update t_gl        
  SET t_gl.ShadowBalance = t_gl.ShadowBalance +  (select Amount from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance +  (select Amount from deleted )  
       WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
  END        
 END        
 else        
 BEGIN        
  if (select Iscredit from deleted ) = 1             
  BEGIN          
  Update t_gl        
  SET t_gl.Balance = t_gl.Balance -  (select Amount from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.Balance = t_PrevGLBalances.Balance -  (select Amount from deleted )  
       WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
  END        
  else         
  BEGIN          
  Update t_gl        
  SET t_gl.Balance = t_gl.Balance +  (select Amount from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.Balance = t_PrevGLBalances.Balance +  (select Amount from deleted )  
       WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
  END        
 END        
        
 END        
 else        
 if (select TransactionMethod from  deleted ) = 'F'        
 BEGIN        
 if (select Indicator from deleted ) = '*'        
 BEGIN        
  if (select Iscredit from deleted ) = 1             
  BEGIN          
  Update t_gl        
  SET t_gl.ShadowBalance = t_gl.ShadowBalance -  (select ForeignAmount from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance -  (select ForeignAmount from deleted )  
       WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
  END        
  else         
  BEGIN          
  Update t_gl        
  SET t_gl.ShadowBalance = t_gl.ShadowBalance +  (select ForeignAmount from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance +  (select ForeignAmount from deleted )  
       WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
  END        
 END        
 else        
 BEGIN        
  if (select Iscredit from deleted ) = 1             
  BEGIN          
  Update t_gl        
  SET t_gl.ForeignBalance = t_gl.ForeignBalance -  (select ForeignAmount from deleted ),        
  t_gl.Balance = t_gl.Balance -  (select Amount from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ForeignBalance = t_PrevGLBalances.ForeignBalance -  (select ForeignAmount from deleted ),  
    t_PrevGLBalances.Balance = t_PrevGLBalances.Balance -  (select Amount from deleted )  
       WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
  END        
  else         
  BEGIN          
  Update t_gl        
  SET t_gl.ForeignBalance = t_gl.foreignBalance +  (select ForeignAmount from deleted ),        
  t_gl.Balance = t_gl.Balance -  (select Amount from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ForeignBalance = t_PrevGLBalances.foreignBalance +  (select ForeignAmount from deleted ),  
    t_PrevGLBalances.Balance = t_PrevGLBalances.Balance -  (select Amount from deleted )  
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
  END        
 END        
        
 END        
 else        
 if (select TransactionMethod from deleted )='A'        
 BEGIN        
 if (select currencyid from deleted ) <> @LocalCurrency        
 BEGIN        
 if (select Indicator from deleted ) = '*'        
 BEGIN        
  if (select Iscredit from deleted ) = 1             
  BEGIN          
  Update t_gl        
  SET t_gl.ShadowBalance = t_gl.ShadowBalance -  (select ForeignAmount  from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance -  (select ForeignAmount  from deleted )  
       WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
  END        
  else         
  BEGIN          
  Update t_gl        
  SET t_gl.ShadowBalance = t_gl.ShadowBalance +  (select ForeignAmount  from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance +  (select ForeignAmount  from deleted )  
       WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
 END        
 END        
 else        
 BEGIN        
  if (select Iscredit from deleted ) = 1             
  BEGIN          
  Update t_gl        
  SET t_gl.ForeignBalance = t_gl.ForeignBalance -  (select ForeignAmount  from deleted ),        
  t_gl.Balance = t_gl.Balance -  (select Amount  from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ForeignBalance = t_PrevGLBalances.ForeignBalance -  (select ForeignAmount  from deleted ),  
    t_PrevGLBalances.Balance = t_PrevGLBalances.Balance -  (select Amount  from deleted )  
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
  END        
  else         
  BEGIN          
  Update t_gl        
  SET t_gl.ForeignBalance = t_gl.foreignBalance +  (select ForeignAmount  from deleted ),        
  t_gl.Balance = t_gl.Balance +  (select Amount from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ForeignBalance = t_PrevGLBalances.foreignBalance +  (select ForeignAmount  from deleted ),  
    t_PrevGLBalances.Balance = t_PrevGLBalances.Balance +  (select Amount from deleted )  
    WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
  END        
 END        
 END        
 else        
 BEGIN        
 if (select Indicator from deleted ) = '*'        
 BEGIN        
  if (select Iscredit from deleted ) = 1             
  BEGIN          
  Update t_gl        
  SET t_gl.ShadowBalance = t_gl.ShadowBalance -  (select Amount  from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance -  (select Amount  from deleted )  
       WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
  END        
  else         
  BEGIN          
  Update t_gl        
  SET t_gl.ShadowBalance = t_gl.ShadowBalance +  (select Amount  from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.ShadowBalance = t_PrevGLBalances.ShadowBalance +  (select Amount  from deleted )  
       WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
  END        
 END        
 else        
 BEGIN        
  if (select Iscredit from deleted ) = 1             
  BEGIN          
  Update t_gl        
/*       SET t_gl.ForeignBalance = t_gl.ForeignBalance -  (select Amount from deleted ),*/        
  set t_gl.Balance = t_gl.Balance -  (select Amount from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       set t_PrevGLBalances.Balance = t_PrevGLBalances.Balance -  (select Amount from deleted )  
       WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
  END        
  else         
  BEGIN          
  Update t_gl        
/*       SET t_gl.ForeignBalance = t_gl.foreignBalance +  (select Amount from deleted ),*/        
  SET t_gl.Balance = t_gl.Balance +  (select Amount  from deleted )        
  WHERE t_gl.AccountID= (select AccountId from deleted ) AND        
  t_gl.OurBranchID = (select OurBranchID from deleted )        
      IF @PrevDayEntry = 1  
       Update t_PrevGLBalances  
       SET t_PrevGLBalances.Balance = t_PrevGLBalances.Balance +  (select Amount  from deleted )  
       WHERE t_PrevGLBalances.OurBranchID = (select OurBranchID from deleted)  
       AND t_PrevGLBalances.AccountID= (select AccountId from deleted)  
  END        
 END        
 END        
           
END