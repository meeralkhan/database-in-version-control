﻿CREATE TABLE [dbo].[tbl_UserTokens] (
    [OurBranchID] NVARCHAR (30) NOT NULL,
    [Username]    NVARCHAR (30) NOT NULL,
    [Token]       VARCHAR (50)  NOT NULL,
    [CreateTime]  DATETIME      NOT NULL,
    [ExpireTime]  DATETIME      NOT NULL,
    [Type]        CHAR (1)      NOT NULL,
    CONSTRAINT [PK_tbl_UserTokens] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [Username] ASC)
);

