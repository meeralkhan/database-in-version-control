﻿CREATE TABLE [dbo].[t_EmailAccounts] (
    [CreateBy]          NVARCHAR (30)  NOT NULL,
    [CreateTime]        DATETIME       NOT NULL,
    [CreateTerminal]    NVARCHAR (30)  NOT NULL,
    [UpdateBy]          NVARCHAR (30)  NULL,
    [UpdateTime]        DATETIME       NULL,
    [UpdateTerminal]    NVARCHAR (30)  NULL,
    [AuthStatus]        CHAR (1)       NOT NULL,
    [SuperviseBy]       NVARCHAR (30)  NULL,
    [SuperviseTime]     DATETIME       NULL,
    [SuperviseTerminal] NVARCHAR (30)  NULL,
    [frmName]           NVARCHAR (100) NOT NULL,
    [OurBranchID]       NVARCHAR (30)  NOT NULL,
    [EmailId]           NVARCHAR (30)  NOT NULL,
    [Name]              NVARCHAR (200) NULL,
    [Host]              NVARCHAR (100) NULL,
    [Pass]              NVARCHAR (500) NULL,
    [SMTPSecurity]      NVARCHAR (60)  NULL,
    [Key]               NVARCHAR (100) NULL,
    [VerifyBy]          NVARCHAR (30)  NULL,
    [VerifyTime]        DATETIME       NULL,
    [VerifyTerminal]    NVARCHAR (30)  NULL,
    [VerifyStatus]      NVARCHAR (30)  NOT NULL,
    CONSTRAINT [PK_t_EmailAccounts] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [EmailId] ASC)
);

