﻿CREATE TABLE [dbo].[t_CriticalLog] (
    [OurBranchID] NVARCHAR (30)  NOT NULL,
    [SerialNo]    DECIMAL (24)   IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Username]    NVARCHAR (30)  NOT NULL,
    [DateTime]    DATETIME       NOT NULL,
    [Terminal]    NVARCHAR (30)  NOT NULL,
    [Module]      NVARCHAR (100) NOT NULL,
    [Severity]    INT            NOT NULL,
    [Message]     TEXT           NOT NULL,
    CONSTRAINT [PK_t_CriticalLog] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [SerialNo] ASC)
);

