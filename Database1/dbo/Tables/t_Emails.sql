﻿CREATE TABLE [dbo].[t_Emails] (
    [OurBranchID]   NVARCHAR (30)  NOT NULL,
    [SerialNo]      DECIMAL (24)   IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [eFrom]         NVARCHAR (100) NOT NULL,
    [eFromName]     NVARCHAR (100) NOT NULL,
    [eHost]         NVARCHAR (100) NOT NULL,
    [eSMTPSecurity] NVARCHAR (30)  NOT NULL,
    [ePass]         NVARCHAR (300) NOT NULL,
    [eKey]          NVARCHAR (300) NOT NULL,
    [eTo]           NVARCHAR (100) NOT NULL,
    [eToName]       NVARCHAR (100) NOT NULL,
    [eSubject]      NVARCHAR (100) NOT NULL,
    [eBody]         TEXT           NOT NULL,
    [eSent]         INT            NOT NULL,
    [eSentTime]     DATETIME       NULL,
    [eAttempts]     INT            NOT NULL,
    [eDateTime]     DATETIME       NOT NULL,
    [eTerminal]     NVARCHAR (30)  NOT NULL,
    CONSTRAINT [PK_t_Emails] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [SerialNo] ASC)
);

