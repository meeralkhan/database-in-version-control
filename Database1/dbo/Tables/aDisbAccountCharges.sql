﻿CREATE TABLE [dbo].[aDisbAccountCharges] (
    [CreateBy]         NVARCHAR (30)  NOT NULL,
    [CreateTime]       DATETIME       NOT NULL,
    [CreateTerminal]   NVARCHAR (30)  NOT NULL,
    [UpdateBy]         NVARCHAR (30)  NULL,
    [UpdateTime]       DATETIME       NULL,
    [UpdateTerminal]   NVARCHAR (30)  NULL,
    [frmName]          VARCHAR (100)  NULL,
    [OurBranchID]      NVARCHAR (30)  NOT NULL,
    [AccountID]        NVARCHAR (30)  NOT NULL,
    [DealID]           DECIMAL (24)   NOT NULL,
    [ChargeID]         NVARCHAR (30)  NOT NULL,
    [Description]      NVARCHAR (400) NOT NULL,
    [ChargeType]       NVARCHAR (40)  NOT NULL,
    [Charges]          MONEY          NOT NULL,
    [VerifyBy]         NVARCHAR (30)  NULL,
    [VerifyTime]       DATETIME       NULL,
    [VerifyTerminal]   NVARCHAR (30)  NULL,
    [VerifyStatus]     CHAR (1)       NOT NULL,
    [FeeReverseAmount] DECIMAL (18)   NULL,
    [IsChargePenalty]  BIT            NULL,
    [SerialNo]         INT            NOT NULL
);

