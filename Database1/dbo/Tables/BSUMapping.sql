﻿CREATE TABLE [dbo].[BSUMapping] (
    [GUID]             NVARCHAR (150) NOT NULL,
    [OperatorID]       VARCHAR (50)   NOT NULL,
    [OurBranchID]      VARCHAR (50)   NOT NULL,
    [LastActivityTime] DATETIME       NOT NULL,
    [BSUData]          NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_BSUMapping] PRIMARY KEY CLUSTERED ([OperatorID] ASC, [OurBranchID] ASC)
);

