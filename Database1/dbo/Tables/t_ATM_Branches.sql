﻿CREATE TABLE [dbo].[t_ATM_Branches] (
    [OurBranchID] VARCHAR (30)  NOT NULL,
    [BranchID]    VARCHAR (30)  NOT NULL,
    [BranchName]  VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_t_ATM_Branches] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [BranchID] ASC)
);

