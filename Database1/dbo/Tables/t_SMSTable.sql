﻿CREATE TABLE [dbo].[t_SMSTable] (
    [OurBranchID]       NVARCHAR (30)   NULL,
    [AccountID]         NVARCHAR (30)   NULL,
    [TrxType]           NVARCHAR (1)    NULL,
    [wDate]             DATETIME        NULL,
    [AccountType]       NVARCHAR (1)    NULL,
    [AccountName]       NVARCHAR (100)  NULL,
    [ProductID]         NVARCHAR (30)   NULL,
    [CurrencyID]        NVARCHAR (30)   NULL,
    [ChequeID]          NVARCHAR (30)   NULL,
    [ChequeDate]        DATETIME        NULL,
    [Amount]            DECIMAL (21, 3) NULL,
    [ForeignAmount]     DECIMAL (21, 3) NULL,
    [ExchangeRate]      DECIMAL (18, 6) NULL,
    [DescriptionID]     NVARCHAR (30)   NULL,
    [Description]       NVARCHAR (500)  NULL,
    [ChannelRefID]      NVARCHAR (100)  NULL,
    [TrxTimeStamp]      DATETIME        NULL,
    [SMSText]           NVARCHAR (MAX)  NULL,
    [SMSToSend]         NVARCHAR (MAX)  NULL,
    [Id]                INT             IDENTITY (1, 1) NOT NULL,
    [IsSend]            BIT             NULL,
    [RequestParameter]  NVARCHAR (MAX)  NULL,
    [ResponseParameter] NVARCHAR (MAX)  NULL,
    [Message]           NVARCHAR (MAX)  NULL,
    [UpdateTime]        DATETIME        NULL,
    CONSTRAINT [PK_t_SMSTable] PRIMARY KEY CLUSTERED ([Id] ASC)
);

