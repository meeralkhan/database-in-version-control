﻿CREATE TABLE [dbo].[ISOCurrencies] (
    [CurrencyCode]        NVARCHAR (3)   NOT NULL,
    [CurrencyDescription] NVARCHAR (100) NULL,
    [CurrencyAlphaCode]   NVARCHAR (3)   NULL,
    CONSTRAINT [PK_ISOCurrencies] PRIMARY KEY CLUSTERED ([CurrencyCode] ASC)
);

