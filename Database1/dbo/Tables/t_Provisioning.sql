﻿CREATE TABLE [dbo].[t_Provisioning] (
    [OurBranchID]    NVARCHAR (30)   NOT NULL,
    [AccountID]      NVARCHAR (30)   NOT NULL,
    [DealID]         NVARCHAR (30)   NOT NULL,
    [SerialID]       INT             NOT NULL,
    [ProvDate]       DATETIME        NOT NULL,
    [AccountName]    NVARCHAR (100)  NOT NULL,
    [ProductID]      NVARCHAR (30)   NOT NULL,
    [CurrencyID]     NVARCHAR (30)   NOT NULL,
    [MeanRate]       DECIMAL (18, 6) NOT NULL,
    [LastProvStage]  CHAR (1)        NOT NULL,
    [LastProvAmount] MONEY           NOT NULL,
    [LastProvDate]   DATETIME        NULL,
    [NewProvStage]   CHAR (1)        NOT NULL,
    [NewProvAmount]  MONEY           NOT NULL,
    [NewProvDate]    DATETIME        NOT NULL,
    [FSVBenefit]     DECIMAL (18, 6) NOT NULL,
    [Provisioning]   DECIMAL (18, 6) NOT NULL,
    [Type]           CHAR (1)        NOT NULL,
    [ReverseAmount]  MONEY           NOT NULL,
    CONSTRAINT [PK_t_Provisioning] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [AccountID] ASC, [DealID] ASC, [SerialID] ASC)
);

