﻿CREATE TABLE [dbo].[t_AccSaveODAccounts] (
    [OurBranchID]  NVARCHAR (30)   NOT NULL,
    [AccountID]    NVARCHAR (30)   NOT NULL,
    [Name]         NVARCHAR (200)  NULL,
    [ClientID]     NVARCHAR (30)   NOT NULL,
    [ProductID]    NVARCHAR (30)   NULL,
    [CurrencyID]   NVARCHAR (60)   NULL,
    [MeanRate]     DECIMAL (18, 6) NULL,
    [CcyRounding]  DECIMAL (24)    NULL,
    [IBAN]         NVARCHAR (100)  NULL,
    [Status]       NVARCHAR (10)   NULL,
    [OpenDate]     DATETIME        NULL,
    [Accrual]      MONEY           NULL,
    [AccrualUpto]  DATETIME        NULL,
    [PAccrual]     MONEY           NULL,
    [PAccrualUpto] DATETIME        NULL,
    [IsProcessed]  INT             NOT NULL,
    CONSTRAINT [PK_t_AccSaveODAccounts] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [AccountID] ASC)
);

