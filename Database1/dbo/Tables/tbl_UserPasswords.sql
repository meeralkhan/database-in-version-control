﻿CREATE TABLE [dbo].[tbl_UserPasswords] (
    [OurBranchID]   NVARCHAR (30) NOT NULL,
    [Username]      NVARCHAR (30) NOT NULL,
    [Password]      VARCHAR (MAX) NOT NULL,
    [CreateTime]    DATETIME      NOT NULL,
    [CreateTeminal] VARCHAR (30)  NOT NULL,
    [PType]         CHAR (1)      NOT NULL
);

