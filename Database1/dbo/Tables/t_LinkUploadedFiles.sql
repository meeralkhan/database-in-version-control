﻿CREATE TABLE [dbo].[t_LinkUploadedFiles] (
    [UploadID]       VARCHAR (30)   NOT NULL,
    [FrmName]        VARCHAR (30)   NOT NULL,
    [PKID1]          VARCHAR (30)   NOT NULL,
    [PKID2]          VARCHAR (30)   NOT NULL,
    [PKID3]          VARCHAR (30)   NULL,
    [PKID4]          VARCHAR (30)   NULL,
    [Description]    VARCHAR (100)  NOT NULL,
    [UploadPath]     NVARCHAR (MAX) NOT NULL,
    [CreateBy]       NVARCHAR (30)  NOT NULL,
    [CreateTime]     DATETIME       NOT NULL,
    [CreateTerminal] NVARCHAR (30)  NOT NULL,
    CONSTRAINT [PK_t_LinkUploadedFiles] PRIMARY KEY CLUSTERED ([UploadID] ASC)
);

