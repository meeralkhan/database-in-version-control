﻿CREATE TABLE [dbo].[t_Payments] (
    [OurBranchID]  NVARCHAR (30) NOT NULL,
    [AccountID]    NVARCHAR (30) NOT NULL,
    [DealID]       NVARCHAR (30) NOT NULL,
    [PaymentID]    NVARCHAR (30) NOT NULL,
    [SerialID]     INT           NOT NULL,
    [wDate]        DATETIME      NOT NULL,
    [ValueDate]    DATETIME      NOT NULL,
    [TrxMode]      CHAR (1)      NOT NULL,
    [ScrollNo]     INT           NOT NULL,
    [SerialNo]     INT           NOT NULL,
    [OperatorID]   NVARCHAR (30) NOT NULL,
    [SupervisorID] NVARCHAR (30) NOT NULL,
    CONSTRAINT [PK_t_Payments_1] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [AccountID] ASC, [DealID] ASC, [PaymentID] ASC, [SerialID] ASC)
);

