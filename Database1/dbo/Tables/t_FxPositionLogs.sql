﻿CREATE TABLE [dbo].[t_FxPositionLogs] (
    [Id]                NVARCHAR (80)  NOT NULL,
    [CreatedBy]         NVARCHAR (50)  NOT NULL,
    [CreatedOn]         DATETIME       NOT NULL,
    [UpdatedBy]         NVARCHAR (50)  NULL,
    [UpdatedOn]         DATETIME       NULL,
    [RequestBody]       NVARCHAR (MAX) NOT NULL,
    [RequestParameter]  NVARCHAR (MAX) NULL,
    [ResponseParameter] NVARCHAR (MAX) NULL,
    [Message]           NVARCHAR (MAX) NULL,
    [IsSuccess]         BIT            NOT NULL,
    [ChannelRefId]      NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_t_FxPositionLogs] PRIMARY KEY CLUSTERED ([Id] ASC)
);

