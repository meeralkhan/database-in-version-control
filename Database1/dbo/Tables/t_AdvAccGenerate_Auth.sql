﻿CREATE TABLE [dbo].[t_AdvAccGenerate_Auth] (
    [CreateBy]              NVARCHAR (30)  NOT NULL,
    [CreateTime]            DATETIME       NOT NULL,
    [CreateTerminal]        NVARCHAR (30)  NOT NULL,
    [UpdateBy]              NVARCHAR (30)  NULL,
    [UpdateTime]            DATETIME       NULL,
    [UpdateTerminal]        NVARCHAR (30)  NULL,
    [AuthStatus]            CHAR (1)       NOT NULL,
    [VerifyBy]              NVARCHAR (30)  NULL,
    [VerifyTime]            DATETIME       NULL,
    [VerifyTerminal]        NVARCHAR (30)  NULL,
    [VerifyStatus]          CHAR (1)       NOT NULL,
    [SuperviseBy]           NVARCHAR (30)  NULL,
    [SuperviseTime]         DATETIME       NULL,
    [SuperviseTerminal]     NVARCHAR (30)  NULL,
    [frmName]               NVARCHAR (100) NOT NULL,
    [OurBranchID]           NVARCHAR (30)  NOT NULL,
    [ClientID]              NVARCHAR (30)  NOT NULL,
    [ProductID]             NVARCHAR (30)  NOT NULL,
    [LoanAccountID]         NVARCHAR (30)  NULL,
    [ReceivableAccountID]   NVARCHAR (30)  NULL,
    [ReceivableProductID]   NVARCHAR (30)  NULL,
    [CapitalizedAccountID]  NVARCHAR (30)  NULL,
    [CapitalizedProductID]  NVARCHAR (30)  NULL,
    [TransactionAccountID]  NVARCHAR (30)  NULL,
    [TransactionProductID]  NVARCHAR (30)  NULL,
    [FirstSearchAccountID]  NVARCHAR (30)  NULL,
    [FirstSearchProductID]  NVARCHAR (30)  NULL,
    [SecondSearchAccountID] NVARCHAR (30)  NULL,
    [SecondSearchProductID] NVARCHAR (30)  NULL,
    CONSTRAINT [PK_t_AdvAccGenerate_Auth] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [ClientID] ASC, [ProductID] ASC)
);

