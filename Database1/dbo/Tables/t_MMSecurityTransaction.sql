﻿CREATE TABLE [dbo].[t_MMSecurityTransaction] (
    [RefNo]      SMALLINT     NOT NULL,
    [DealNo]     VARCHAR (30) NOT NULL,
    [PartyID]    VARCHAR (30) NOT NULL,
    [SecurityID] VARCHAR (30) NOT NULL,
    [Amount]     MONEY        NOT NULL,
    [CloseDate]  DATETIME     NOT NULL
);

