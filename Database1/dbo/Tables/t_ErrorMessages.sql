﻿CREATE TABLE [dbo].[t_ErrorMessages] (
    [ErrorCode]    VARCHAR (100)  NOT NULL,
    [ErrorMessage] NVARCHAR (MAX) NULL,
    [LocalCode]    VARCHAR (100)  NULL,
    [isError]      BIT            NULL,
    [data]         TEXT           NULL,
    CONSTRAINT [PK_ErrorMessages] PRIMARY KEY CLUSTERED ([ErrorCode] ASC)
);

