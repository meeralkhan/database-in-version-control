﻿CREATE TABLE [dbo].[AnonymousApi] (
    [Id]             INT            IDENTITY (1, 1) NOT NULL,
    [ControllerName] NVARCHAR (100) NOT NULL,
    [ActionName]     NVARCHAR (100) NOT NULL,
    CONSTRAINT [PK_AnonymousApi] PRIMARY KEY CLUSTERED ([Id] ASC)
);

