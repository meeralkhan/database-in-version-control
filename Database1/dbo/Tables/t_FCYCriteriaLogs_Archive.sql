﻿CREATE TABLE [dbo].[t_FCYCriteriaLogs_Archive] (
    [ChannelRefID]          NVARCHAR (100) NULL,
    [LogTime]               DATETIME       NULL,
    [ProcessedTime]         DATETIME       NULL,
    [OurBranchID]           NVARCHAR (30)  NOT NULL,
    [GUID]                  NVARCHAR (50)  NOT NULL,
    [mGUID]                 NVARCHAR (50)  NULL,
    [FailedDescription]     NVARCHAR (100) NULL,
    [CriteriaDetails]       NVARCHAR (MAX) NULL,
    [ActualTrxDetails]      NVARCHAR (MAX) NULL,
    [AdditionalTrxRequest]  NVARCHAR (MAX) NULL,
    [AdditionalTrxResponse] NVARCHAR (MAX) NULL,
    [TrxPostedWithCriteria] BIT            NULL,
    CONSTRAINT [PK_t_FCYCriteriaLogs_Archive] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [GUID] ASC)
);

