﻿CREATE TABLE [dbo].[t_DisabledGLS] (
    [OurBranchID]  NVARCHAR (30)  NOT NULL,
    [AccountID]    NVARCHAR (30)  NOT NULL,
    [wDate]        DATETIME       NOT NULL,
    [AccountClass] NVARCHAR (30)  NULL,
    [Description]  NVARCHAR (100) NULL,
    [CurrencyID]   NVARCHAR (30)  NULL,
    [GLOwnerID]    NVARCHAR (30)  NULL,
    [OldIsPosting] INT            NULL,
    [IsPosting]    INT            NOT NULL,
    [LogID]        INT            NOT NULL,
    CONSTRAINT [PK_tbl_DisabledGLS] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [AccountID] ASC, [wDate] ASC)
);

