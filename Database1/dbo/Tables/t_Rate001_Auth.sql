﻿CREATE TABLE [dbo].[t_Rate001_Auth] (
    [CreateBy]          NVARCHAR (30)  NOT NULL,
    [CreateTime]        DATETIME       NOT NULL,
    [CreateTerminal]    NVARCHAR (30)  NOT NULL,
    [UpdateBy]          NVARCHAR (30)  NULL,
    [UpdateTime]        DATETIME       NULL,
    [UpdateTerminal]    NVARCHAR (30)  NULL,
    [AuthStatus]        CHAR (1)       NOT NULL,
    [SuperviseBy]       NVARCHAR (30)  NULL,
    [SuperviseTime]     DATETIME       NULL,
    [SuperviseTerminal] NVARCHAR (30)  NULL,
    [frmName]           NVARCHAR (100) NOT NULL,
    [OurBranchID]       NVARCHAR (30)  NOT NULL,
    [AccountID]         NVARCHAR (30)  NOT NULL,
    [LimitID]           DECIMAL (24)   NOT NULL,
    [ColumnID]          DECIMAL (24)   NOT NULL,
    [ChangeDate]        DATETIME       NULL,
    [Limit1]            MONEY          NULL,
    [Rate1]             MONEY          NULL,
    [Limit2]            MONEY          NULL,
    [Rate2]             MONEY          NULL,
    [Limit3]            MONEY          NULL,
    [Rate3]             MONEY          NULL,
    [ExcessRate]        MONEY          NULL,
    [VerifyBy]          NVARCHAR (30)  NULL,
    [VerifyTime]        DATETIME       NULL,
    [VerifyTerminal]    NVARCHAR (30)  NULL,
    [VerifyStatus]      CHAR (1)       NOT NULL,
    CONSTRAINT [PK_t_Rate001_Auth] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [AccountID] ASC, [LimitID] ASC, [ColumnID] ASC)
);

