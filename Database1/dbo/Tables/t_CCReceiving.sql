﻿CREATE TABLE [dbo].[t_CCReceiving] (
    [CreateBy]          NVARCHAR (30)  NOT NULL,
    [CreateTime]        DATETIME       NOT NULL,
    [CreateTerminal]    NVARCHAR (30)  NOT NULL,
    [UpdateBy]          NVARCHAR (30)  NULL,
    [UpdateTime]        DATETIME       NULL,
    [UpdateTerminal]    NVARCHAR (30)  NULL,
    [AuthStatus]        CHAR (1)       NOT NULL,
    [VerifyBy]          NVARCHAR (30)  NULL,
    [VerifyTime]        DATETIME       NULL,
    [VerifyTerminal]    NVARCHAR (30)  NULL,
    [VerifyStatus]      CHAR (1)       NOT NULL,
    [SuperviseBy]       NVARCHAR (30)  NULL,
    [SuperviseTime]     DATETIME       NULL,
    [SuperviseTerminal] NVARCHAR (30)  NULL,
    [frmName]           NVARCHAR (100) NOT NULL,
    [OurBranchID]       NVARCHAR (30)  NOT NULL,
    [ScrollNo]          DECIMAL (24)   NOT NULL,
    [BeneficiaryID]     NVARCHAR (30)  NULL,
    [BenMobile]         NVARCHAR (30)  NULL,
    [BenEmailID]        NVARCHAR (100) NULL,
    [BenAddress]        NVARCHAR (100) NULL,
    [PayeeID]           NVARCHAR (30)  NULL,
    [PayeeMobile]       NVARCHAR (30)  NULL,
    [PayeeEmailID]      NVARCHAR (100) NULL,
    [PayeeAddress]      NVARCHAR (100) NULL,
    [Amount]            MONEY          NULL,
    [Commission]        MONEY          NULL,
    [TotalAmount]       MONEY          NULL,
    [RefNo]             NVARCHAR (30)  NOT NULL,
    [IsPaid]            INT            NOT NULL,
    [PaidBy]            NVARCHAR (50)  NULL,
    [PaidTime]          DATETIME       NULL,
    [PaidTerminal]      NVARCHAR (50)  NULL,
    [Pic]               NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_t_CCReceiving] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [ScrollNo] ASC)
);

