﻿CREATE TABLE [dbo].[t_PostExpenseDetails] (
    [CreateBy]            NVARCHAR (30)  NOT NULL,
    [CreateTime]          DATETIME       NOT NULL,
    [CreateTerminal]      NVARCHAR (30)  NOT NULL,
    [UpdateBy]            NVARCHAR (30)  NULL,
    [UpdateTime]          DATETIME       NULL,
    [UpdateTerminal]      NVARCHAR (30)  NULL,
    [AuthStatus]          CHAR (1)       NOT NULL,
    [VerifyBy]            NVARCHAR (30)  NULL,
    [VerifyTime]          DATETIME       NULL,
    [VerifyTerminal]      NVARCHAR (30)  NULL,
    [VerifyStatus]        CHAR (1)       NOT NULL,
    [SuperviseBy]         NVARCHAR (30)  NULL,
    [SuperviseTime]       DATETIME       NULL,
    [SuperviseTerminal]   NVARCHAR (30)  NULL,
    [frmName]             NVARCHAR (100) NOT NULL,
    [OurBranchID]         NVARCHAR (30)  NOT NULL,
    [PostExpenseID]       NVARCHAR (30)  NOT NULL,
    [PostExpenseDetails]  NVARCHAR (30)  NOT NULL,
    [CostCenterID]        NVARCHAR (30)  NULL,
    [AccountID]           NVARCHAR (30)  NULL,
    [Percent]             MONEY          NULL,
    [Amount]              MONEY          NULL,
    [Type]                NVARCHAR (100) NULL,
    [EntryType]           NVARCHAR (10)  NULL,
    [VendorID]            NVARCHAR (30)  NULL,
    [NarrationID]         NVARCHAR (30)  NULL,
    [GLBranchID]          NVARCHAR (30)  NULL,
    [CostTypeID]          NVARCHAR (30)  NULL,
    [CustomerLifecycleID] NVARCHAR (30)  NULL,
    [Remarks]             NVARCHAR (100) NULL,
    [InvoiceNo]           NVARCHAR (100) NULL,
    CONSTRAINT [PK_t_PostExpenseDetails] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [PostExpenseID] ASC, [PostExpenseDetails] ASC)
);

