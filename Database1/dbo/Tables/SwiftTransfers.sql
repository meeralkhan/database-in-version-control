﻿CREATE TABLE [dbo].[SwiftTransfers] (
    [Id]              BIGINT          IDENTITY (1, 1) NOT NULL,
    [ChannelRefId]    VARCHAR (50)    NULL,
    [Country]         VARCHAR (150)   NULL,
    [InwardOutward]   VARCHAR (50)    NULL,
    [TrxPurpose]      VARCHAR (150)   NULL,
    [AmountInAED]     NUMERIC (25, 4) NULL,
    [DateOfTrx]       DATETIME        NULL,
    [SQLPostedDate]   DATETIME        NULL,
    [Txn_Id]          VARCHAR (200)   NULL,
    [ToAccountNumber] VARCHAR (100)   NULL,
    CONSTRAINT [PK_SwiftTransfers] PRIMARY KEY CLUSTERED ([Id] ASC)
);

