﻿CREATE TABLE [dbo].[t_AccountMonthBalances] (
    [Month]             DATETIME       NOT NULL,
    [OurBranchID]       NVARCHAR (30)  NOT NULL,
    [AccountID]         NVARCHAR (30)  NOT NULL,
    [ProductID]         VARCHAR (30)   NOT NULL,
    [Name]              NVARCHAR (200) NULL,
    [ClearBalance]      MONEY          NULL,
    [Effects]           MONEY          NULL,
    [LocalClearBalance] MONEY          NULL,
    [LocalEffects]      MONEY          NULL,
    [PrevMonthBalance]  MONEY          NULL,
    CONSTRAINT [PK_t_AccountMonthBalances] PRIMARY KEY CLUSTERED ([Month] ASC, [OurBranchID] ASC, [AccountID] ASC)
);

