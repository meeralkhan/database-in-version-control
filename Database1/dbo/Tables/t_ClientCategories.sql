﻿CREATE TABLE [dbo].[t_ClientCategories] (
    [CreateBy]          NVARCHAR (30)  NOT NULL,
    [CreateTime]        DATETIME       NOT NULL,
    [CreateTerminal]    NVARCHAR (30)  NOT NULL,
    [UpdateBy]          NVARCHAR (30)  NULL,
    [UpdateTime]        DATETIME       NULL,
    [UpdateTerminal]    NVARCHAR (30)  NULL,
    [AuthStatus]        CHAR (1)       NULL,
    [SuperviseBy]       NVARCHAR (30)  NULL,
    [SuperviseTime]     DATETIME       NULL,
    [SuperviseTerminal] NVARCHAR (30)  NULL,
    [OurBranchID]       NVARCHAR (30)  NOT NULL,
    [CategoryId]        NVARCHAR (30)  NOT NULL,
    [Description]       NVARCHAR (200) NULL,
    [frmName]           VARCHAR (100)  NULL,
    [VerifyBy]          NVARCHAR (30)  NULL,
    [VerifyTime]        DATETIME       NULL,
    [VerifyTerminal]    NVARCHAR (30)  NULL,
    [VerifyStatus]      CHAR (1)       NOT NULL,
    [KYCLinkForm]       NVARCHAR (100) NULL,
    CONSTRAINT [PK_t_ClientCategories] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [CategoryId] ASC)
);

