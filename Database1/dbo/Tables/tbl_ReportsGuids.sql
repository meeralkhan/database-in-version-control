﻿CREATE TABLE [dbo].[tbl_ReportsGuids] (
    [ID]         INT           IDENTITY (1, 1) NOT NULL,
    [GUID]       VARCHAR (MAX) NULL,
    [CreateTime] DATETIME      NULL,
    [ReportName] VARCHAR (MAX) NULL
);

