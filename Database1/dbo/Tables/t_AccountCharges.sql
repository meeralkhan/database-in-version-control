﻿CREATE TABLE [dbo].[t_AccountCharges] (
    [OurBranchId]        NVARCHAR (30) NOT NULL,
    [ReceiptID]          NVARCHAR (30) NOT NULL,
    [AccountID]          NVARCHAR (30) NOT NULL,
    [ChargeId]           NVARCHAR (30) NOT NULL,
    [ChargeSerial]       NVARCHAR (30) NOT NULL,
    [Amount]             MONEY         NULL,
    [ChargeonPrincipal]  INT           NOT NULL,
    [ChargeonInterest]   INT           NOT NULL,
    [Type]               NVARCHAR (10) NULL,
    [IsApplicable]       INT           NOT NULL,
    [ModeOfCharges]      NVARCHAR (10) NULL,
    [IsRoundingRequired] INT           NOT NULL,
    [RoundingPrecision]  NVARCHAR (20) NOT NULL,
    [CreateBy]           NVARCHAR (30) NOT NULL,
    [CreateTime]         DATETIME      NOT NULL,
    [CreateTerminal]     NVARCHAR (30) NOT NULL,
    [UpdateBy]           NVARCHAR (30) NULL,
    [UpdateTime]         DATETIME      NULL,
    [UpdateTerminal]     NVARCHAR (30) NULL,
    CONSTRAINT [PK_t_AccountCharges] PRIMARY KEY CLUSTERED ([OurBranchId] ASC, [ReceiptID] ASC, [ChargeId] ASC, [ChargeSerial] ASC)
);

