﻿CREATE TABLE [dbo].[t_ProfitPayHistory] (
    [OurBranchID]    NVARCHAR (30)   NOT NULL,
    [RecieptID]      NVARCHAR (30)   NOT NULL,
    [Date]           DATETIME        NOT NULL,
    [SerialNo]       INT             IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ProductID]      NVARCHAR (30)   NOT NULL,
    [Amount]         MONEY           NOT NULL,
    [FromDate]       DATETIME        NOT NULL,
    [ToDate]         DATETIME        NOT NULL,
    [RateType]       NVARCHAR (30)   NOT NULL,
    [Rate]           DECIMAL (24, 6) NOT NULL,
    [Days]           INT             NOT NULL,
    [PerDay]         MONEY           NOT NULL,
    [SubTotal]       MONEY           NOT NULL,
    [CreateBy]       NVARCHAR (30)   NOT NULL,
    [CreateTime]     DATETIME        NOT NULL,
    [CreateTerminal] NVARCHAR (30)   NOT NULL,
    CONSTRAINT [PK_t_ProfitPayHistory] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [RecieptID] ASC, [Date] ASC, [SerialNo] ASC)
);

