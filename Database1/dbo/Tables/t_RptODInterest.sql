﻿CREATE TABLE [dbo].[t_RptODInterest] (
    [SerialNo]       DECIMAL (24)    IDENTITY (1, 1) NOT NULL,
    [ReceiveDate]    DATETIME        NOT NULL,
    [FromDate]       DATETIME        NOT NULL,
    [ToDate]         DATETIME        NOT NULL,
    [OurBranchID]    NVARCHAR (30)   NOT NULL,
    [ClientID]       NVARCHAR (30)   NOT NULL,
    [AccountID]      NVARCHAR (30)   NOT NULL,
    [AccountName]    NVARCHAR (100)  NOT NULL,
    [ProductID]      NVARCHAR (30)   NOT NULL,
    [CurrencyID]     NVARCHAR (30)   NOT NULL,
    [MeanRate]       DECIMAL (18, 6) NOT NULL,
    [AccruedAmount]  DECIMAL (21, 3) NOT NULL,
    [PostedAmount]   DECIMAL (21, 3) NOT NULL,
    [CreateBy]       NVARCHAR (30)   NOT NULL,
    [CreateTime]     DATETIME        NOT NULL,
    [CreateTerminal] NVARCHAR (30)   NOT NULL,
    CONSTRAINT [PK_t_RptODInterest] PRIMARY KEY CLUSTERED ([SerialNo] ASC)
);

