﻿CREATE TABLE [dbo].[tbl_UserFailedLogins] (
    [BranchId]     NVARCHAR (30) NOT NULL,
    [Username]     NVARCHAR (30) NOT NULL,
    [Count]        INT           NOT NULL,
    [UserTime]     DATETIME      NOT NULL,
    [UserTerminal] VARCHAR (30)  NOT NULL,
    CONSTRAINT [PK_tbl_UserFailedLogins] PRIMARY KEY CLUSTERED ([BranchId] ASC, [Username] ASC)
);

