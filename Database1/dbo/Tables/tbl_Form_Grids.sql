﻿CREATE TABLE [dbo].[tbl_Form_Grids] (
    [FrmName]        VARCHAR (50) NOT NULL,
    [GridId]         VARCHAR (50) NOT NULL,
    [IsSelect]       INT          NOT NULL,
    [TabId]          VARCHAR (50) NULL,
    [PortletId]      VARCHAR (50) NULL,
    [SectionId]      VARCHAR (50) NULL,
    [CreateBy]       VARCHAR (30) NOT NULL,
    [CreateTime]     DATETIME     NOT NULL,
    [CreateTerminal] VARCHAR (30) NOT NULL,
    [UpdateBy]       VARCHAR (30) NULL,
    [UpdateTime]     DATETIME     NULL,
    [UpdateTerminal] VARCHAR (30) NULL,
    CONSTRAINT [PK_tbl_Form_Grids] PRIMARY KEY CLUSTERED ([FrmName] ASC, [GridId] ASC)
);

