﻿CREATE TABLE [dbo].[TLClassifiedDeals] (
    [OurBranchID] NVARCHAR (30)  NOT NULL,
    [AccountID]   NVARCHAR (30)  NOT NULL,
    [DealID]      DECIMAL (24)   NOT NULL,
    [WorkingDate] DATETIME       NOT NULL,
    [ClientID]    NVARCHAR (30)  NOT NULL,
    [Name]        NVARCHAR (200) NOT NULL,
    [DealRefNo]   NVARCHAR (200) NOT NULL,
    [ProductID]   NVARCHAR (30)  NOT NULL,
    [CurrencyID]  NVARCHAR (60)  NOT NULL,
    [RiskCode]    NVARCHAR (30)  NOT NULL,
    CONSTRAINT [PK_TLClassifiedDeals] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [AccountID] ASC, [DealID] ASC, [WorkingDate] ASC)
);

