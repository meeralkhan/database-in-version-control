﻿CREATE TABLE [dbo].[t_MMAccruals] (
    [DealNo]      VARCHAR (30) NOT NULL,
    [RefID]       SMALLINT     NOT NULL,
    [OurBranchID] VARCHAR (30) NOT NULL,
    [SecurityID]  VARCHAR (30) NOT NULL,
    [FromDate]    VARCHAR (10) NOT NULL,
    [ToDate]      VARCHAR (10) NOT NULL,
    [Amount]      MONEY        NOT NULL,
    [Days]        SMALLINT     NOT NULL
);

