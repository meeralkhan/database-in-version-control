﻿CREATE TABLE [dbo].[t_CIF_BeneficialOwners] (
    [OurBranchID]        NVARCHAR (30) NOT NULL,
    [ClientID]           NVARCHAR (30) NOT NULL,
    [BeneficialOwnersID] NVARCHAR (30) NOT NULL,
    CONSTRAINT [PK_t_CIF_BeneficialOwners] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [ClientID] ASC, [BeneficialOwnersID] ASC)
);

