﻿CREATE TABLE [dbo].[t_UserExceptionsLog] (
    [OurBranchID]    NVARCHAR (30)  NOT NULL,
    [wdate]          DATETIME       NOT NULL,
    [Activity]       NVARCHAR (MAX) NULL,
    [CreateBy]       NVARCHAR (30)  NOT NULL,
    [CreateTime]     DATETIME       NOT NULL,
    [CreateTerminal] NVARCHAR (30)  NOT NULL
);

