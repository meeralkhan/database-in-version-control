﻿CREATE TABLE [dbo].[SmsTable_Log] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [Exception]    NVARCHAR (MAX) NULL,
    [CreatedDate]  DATETIME       NULL,
    [ChannelRefId] NVARCHAR (200) NULL,
    [Level]        NVARCHAR (20)  NULL,
    [SmsId]        INT            NULL,
    CONSTRAINT [PK_SmsTable_Log] PRIMARY KEY CLUSTERED ([Id] ASC)
);

