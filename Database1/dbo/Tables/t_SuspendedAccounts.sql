﻿CREATE TABLE [dbo].[t_SuspendedAccounts] (
    [wDate]               DATETIME       NOT NULL,
    [OurBranchID]         NVARCHAR (30)  NOT NULL,
    [AccountID]           NVARCHAR (30)  NOT NULL,
    [DealID]              DECIMAL (24)   NOT NULL,
    [ClientID]            NVARCHAR (30)  NOT NULL,
    [Type]                VARCHAR (2)    NOT NULL,
    [Name]                NVARCHAR (200) NULL,
    [ProductID]           NVARCHAR (30)  NULL,
    [CurrencyID]          NVARCHAR (60)  NULL,
    [RiskCode]            NVARCHAR (30)  NULL,
    [Accrual]             MONEY          NOT NULL,
    [PAccrual]            MONEY          NOT NULL,
    [GLInterestReceived]  NVARCHAR (30)  NULL,
    [GLSuspendedInterest] NVARCHAR (30)  NULL,
    [IsPosted]            INT            NOT NULL,
    [IsReversed]          INT            NOT NULL,
    CONSTRAINT [PK_t_SuspendedAccounts] PRIMARY KEY CLUSTERED ([wDate] ASC, [OurBranchID] ASC, [AccountID] ASC, [DealID] ASC)
);

