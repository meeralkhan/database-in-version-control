﻿CREATE TABLE [dbo].[t_AccountDailyBalance] (
    [OurBranchID] NVARCHAR (30) NOT NULL,
    [AccountID]   NVARCHAR (30) NOT NULL,
    [ProductID]   NVARCHAR (30) NOT NULL,
    [Wdate]       DATETIME      NOT NULL,
    [Amount]      MONEY         NOT NULL,
    CONSTRAINT [PK_t_AccountDailyBalance] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [AccountID] ASC, [ProductID] ASC, [Wdate] ASC)
);

