﻿CREATE TABLE [dbo].[t_PoolDates] (
    [SerialNo]       DECIMAL (24)   IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ID]             NVARCHAR (100) NOT NULL,
    [GUID]           NVARCHAR (100) NOT NULL,
    [StartDate]      DATETIME       NOT NULL,
    [EndDate]        DATETIME       NOT NULL,
    [CreateBy]       NVARCHAR (100) NOT NULL,
    [CreateTime]     DATETIME       NOT NULL,
    [CreateTerminal] NVARCHAR (100) NOT NULL,
    CONSTRAINT [PK_t_PoolDates] PRIMARY KEY CLUSTERED ([SerialNo] ASC)
);

