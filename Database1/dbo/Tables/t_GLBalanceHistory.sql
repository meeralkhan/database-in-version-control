﻿CREATE TABLE [dbo].[t_GLBalanceHistory] (
    [OurBranchID]    VARCHAR (30)  NOT NULL,
    [wDate]          DATETIME      NOT NULL,
    [AccountID]      VARCHAR (30)  NOT NULL,
    [Description]    VARCHAR (100) NOT NULL,
    [CurrencyID]     VARCHAR (30)  NOT NULL,
    [AccountType]    CHAR (1)      NOT NULL,
    [ForeignBalance] MONEY         NULL,
    [Balance]        MONEY         NOT NULL,
    CONSTRAINT [PK_t_GLBalanceHistory] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [wDate] ASC, [AccountID] ASC)
);

