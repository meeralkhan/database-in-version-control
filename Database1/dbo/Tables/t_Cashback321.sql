﻿CREATE TABLE [dbo].[t_Cashback321] (
    [SerialID]        DECIMAL (24)    IDENTITY (1, 1) NOT NULL,
    [OurBranchID]     NVARCHAR (30)   NOT NULL,
    [AccountID]       NVARCHAR (30)   NOT NULL,
    [AccountName]     NVARCHAR (100)  NOT NULL,
    [ProductID]       NVARCHAR (30)   NOT NULL,
    [CurrencyID]      NVARCHAR (30)   NOT NULL,
    [TrType]          NVARCHAR (1)    NOT NULL,
    [wDate]           DATETIME        NOT NULL,
    [ForeignAmount]   DECIMAL (21, 3) NOT NULL,
    [ExchangeRate]    DECIMAL (18, 6) NOT NULL,
    [Amount]          DECIMAL (21, 3) NOT NULL,
    [CashbackPercent] MONEY           NOT NULL,
    [CashbackAmount]  MONEY           NOT NULL,
    [DescriptionID]   NVARCHAR (30)   NOT NULL,
    [ChannelRefID]    NVARCHAR (100)  NOT NULL,
    [TrxTimeStamp]    DATETIME        NOT NULL,
    [CreateBy]        NVARCHAR (30)   NOT NULL,
    [CreateTime]      DATETIME        NOT NULL,
    [IsPaid]          INT             NOT NULL,
    CONSTRAINT [PK_t_Cashback321] PRIMARY KEY CLUSTERED ([SerialID] ASC)
);

