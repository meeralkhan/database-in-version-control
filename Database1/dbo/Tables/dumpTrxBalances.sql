﻿CREATE TABLE [dbo].[dumpTrxBalances] (
    [OurBranchID]  NVARCHAR (30) NOT NULL,
    [AccountID]    NVARCHAR (30) NOT NULL,
    [CurrencyID]   NVARCHAR (50) NULL,
    [ClearBalance] MONEY         NULL,
    [TrxBalance]   MONEY         NULL
);

