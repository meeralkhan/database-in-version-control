﻿CREATE TABLE [dbo].[t_MapDisk] (
    [CreateBy]          NVARCHAR (30)  NOT NULL,
    [CreateTime]        DATETIME       NOT NULL,
    [CreateTerminal]    NVARCHAR (30)  NOT NULL,
    [UpdateBy]          NVARCHAR (30)  NULL,
    [UpdateTime]        DATETIME       NULL,
    [UpdateTerminal]    NVARCHAR (30)  NULL,
    [AuthStatus]        CHAR (1)       NOT NULL,
    [SuperviseBy]       NVARCHAR (30)  NULL,
    [SuperviseTime]     DATETIME       NULL,
    [SuperviseTerminal] NVARCHAR (30)  NULL,
    [OurBranchID]       NVARCHAR (30)  NOT NULL,
    [FormToRun]         NVARCHAR (30)  NOT NULL,
    [InternalCode]      NVARCHAR (30)  NULL,
    [frmName]           VARCHAR (100)  NULL,
    [ProgramDesc]       NVARCHAR (200) NULL,
    [VerifyBy]          NVARCHAR (30)  NULL,
    [VerifyTime]        DATETIME       NULL,
    [VerifyTerminal]    NVARCHAR (30)  NULL,
    [VerifyStatus]      CHAR (1)       NOT NULL,
    CONSTRAINT [PK_t_MapDisk] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [FormToRun] ASC)
);

