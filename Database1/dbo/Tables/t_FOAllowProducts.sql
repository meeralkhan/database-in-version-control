﻿CREATE TABLE [dbo].[t_FOAllowProducts] (
    [OurBranchID] VARCHAR (30) NOT NULL,
    [ProductID]   VARCHAR (30) NOT NULL,
    CONSTRAINT [PK_t_FOAllowProducts] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [ProductID] ASC)
);

