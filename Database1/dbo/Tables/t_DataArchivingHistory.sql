﻿CREATE TABLE [dbo].[t_DataArchivingHistory] (
    [ParentTableName] VARCHAR (50)  NOT NULL,
    [TableName]       NVARCHAR (50) NOT NULL,
    [Date]            DATETIME      NOT NULL,
    CONSTRAINT [PK_t_DataArchivingHistory] PRIMARY KEY CLUSTERED ([ParentTableName] ASC, [TableName] ASC)
);

