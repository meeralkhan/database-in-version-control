﻿CREATE TABLE [dbo].[Quantum_BRF_46] (
    [DEAL_NO]            VARCHAR (255) NULL,
    [DEAL_DT]            VARCHAR (255) NULL,
    [CPARTYNAME]         VARCHAR (255) NULL,
    [FACE_VALUE]         VARCHAR (255) NULL,
    [COUNTRY]            VARCHAR (255) NULL,
    [CONTRYCODE]         VARCHAR (255) NULL,
    [INVESTMENTCATEGORY] VARCHAR (255) NULL,
    [CPTYPE]             VARCHAR (255) NULL,
    [SECTYPENAME]        VARCHAR (255) NULL,
    [BOR_INVEST]         VARCHAR (255) NULL,
    [MM_OR_SEC]          VARCHAR (255) NULL,
    [CreateDate]         DATETIME      NULL,
    [ToolRunDate]        DATETIME      NULL
);

