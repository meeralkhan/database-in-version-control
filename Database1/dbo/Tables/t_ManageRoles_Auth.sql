﻿CREATE TABLE [dbo].[t_ManageRoles_Auth] (
    [CreateBy]          NVARCHAR (30)  NOT NULL,
    [CreateTime]        DATETIME       NOT NULL,
    [CreateTerminal]    NVARCHAR (30)  NOT NULL,
    [UpdateBy]          NVARCHAR (30)  NULL,
    [UpdateTime]        DATETIME       NULL,
    [UpdateTerminal]    NVARCHAR (30)  NULL,
    [AuthStatus]        CHAR (1)       NOT NULL,
    [VerifyBy]          NVARCHAR (30)  NULL,
    [VerifyTime]        DATETIME       NULL,
    [VerifyTerminal]    NVARCHAR (30)  NULL,
    [VerifyStatus]      CHAR (1)       NOT NULL,
    [SuperviseBy]       NVARCHAR (30)  NULL,
    [SuperviseTime]     DATETIME       NULL,
    [SuperviseTerminal] NVARCHAR (30)  NULL,
    [frmName]           NVARCHAR (100) NOT NULL,
    [ReturnMessage]     TEXT           NULL,
    [OurBranchID]       NVARCHAR (30)  NOT NULL,
    [RoleID]            NVARCHAR (30)  NOT NULL,
    [Description]       NVARCHAR (100) NULL,
    CONSTRAINT [PK_t_ManageRoles_Auth] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [RoleID] ASC)
);

