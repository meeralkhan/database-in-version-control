﻿CREATE TABLE [dbo].[t_AssignRoles_Auth] (
    [CreateBy]          NVARCHAR (30) NOT NULL,
    [CreateTime]        DATETIME      NOT NULL,
    [CreateTerminal]    NVARCHAR (30) NOT NULL,
    [UpdateBy]          NVARCHAR (30) NULL,
    [UpdateTime]        DATETIME      NULL,
    [UpdateTerminal]    NVARCHAR (30) NULL,
    [SuperviseBy]       NVARCHAR (30) NULL,
    [SuperviseTime]     DATETIME      NULL,
    [SuperviseTerminal] NVARCHAR (30) NULL,
    [OurBranchID]       NVARCHAR (30) NOT NULL,
    [RoleID]            NVARCHAR (30) NOT NULL,
    [OperatorID]        NVARCHAR (30) NOT NULL,
    [Auth]              INT           NULL,
    CONSTRAINT [PK_t_AssignRoles_Auth] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [RoleID] ASC, [OperatorID] ASC)
);

