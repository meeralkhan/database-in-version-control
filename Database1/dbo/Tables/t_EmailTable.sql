﻿CREATE TABLE [dbo].[t_EmailTable] (
    [Id]                     INT            IDENTITY (1, 1) NOT NULL,
    [AccountId]              NVARCHAR (30)  NULL,
    [AccountName]            NVARCHAR (100) NULL,
    [EmailMessage]           NVARCHAR (MAX) NULL,
    [Email]                  NVARCHAR (200) NOT NULL,
    [IsEmailSend]            BIT            NOT NULL,
    [CreatedOn]              DATETIME       NULL,
    [EmailSendDate]          DATETIME       NULL,
    [Subject]                NVARCHAR (400) NOT NULL,
    [ChannelRefId]           NVARCHAR (200) NULL,
    [RequestParameter]       NVARCHAR (MAX) NULL,
    [ResponseParameter]      NVARCHAR (MAX) NULL,
    [Message]                NVARCHAR (MAX) NULL,
    [UpdateTime]             DATETIME       NULL,
    [attachmentBase64]       NVARCHAR (MAX) NULL,
    [attachmentBaseFileName] NVARCHAR (200) NULL,
    CONSTRAINT [PK_t_EmailTable] PRIMARY KEY CLUSTERED ([Id] ASC)
);

