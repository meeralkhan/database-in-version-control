﻿CREATE TABLE [dbo].[t_Feedbacks] (
    [CreateBy]       NVARCHAR (30)  NOT NULL,
    [CreateTime]     DATETIME       NOT NULL,
    [CreateTerminal] NVARCHAR (30)  NOT NULL,
    [UpdateBy]       NVARCHAR (30)  NULL,
    [UpdateTime]     DATETIME       NULL,
    [UpdateTerminal] NVARCHAR (30)  NULL,
    [OurBranchID]    NVARCHAR (30)  NOT NULL,
    [FeedbackID]     DECIMAL (24)   NOT NULL,
    [SerialNo]       DECIMAL (24)   NOT NULL,
    [Description]    NVARCHAR (100) NOT NULL,
    [Type]           NVARCHAR (30)  NOT NULL,
    [Label]          NVARCHAR (MAX) NULL,
    [Adjustment]     NVARCHAR (15)  NULL,
    [Required]       INT            NULL,
    [ComboItems]     TEXT           NULL,
    [OrderId]        INT            NULL,
    [RangeTo]        INT            NOT NULL,
    CONSTRAINT [PK_t_Feedbacks] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [FeedbackID] ASC, [SerialNo] ASC)
);

