﻿CREATE TABLE [dbo].[t_FDBalanceHistory] (
    [wDate]            DATETIME        NOT NULL,
    [OurBranchID]      NVARCHAR (30)   NOT NULL,
    [AccountID]        NVARCHAR (30)   NOT NULL,
    [RecieptID]        NVARCHAR (30)   NOT NULL,
    [ProductID]        NVARCHAR (30)   NULL,
    [Amount]           MONEY           NULL,
    [ExchangeRate]     DECIMAL (18, 6) NULL,
    [IssueDate]        DATETIME        NULL,
    [StartDate]        DATETIME        NULL,
    [MaturityDate]     DATETIME        NULL,
    [Rate]             MONEY           NULL,
    [Interest]         MONEY           NOT NULL,
    [InterestPaid]     MONEY           NOT NULL,
    [InterestPaidupTo] DATETIME        NULL,
    [Accrual]          MONEY           NOT NULL,
    [tAccrual]         MONEY           NOT NULL,
    [AccrualUpto]      DATETIME        NULL,
    CONSTRAINT [PK_t_FDBalanceHistory] PRIMARY KEY CLUSTERED ([wDate] ASC, [OurBranchID] ASC, [AccountID] ASC, [RecieptID] ASC)
);

