﻿CREATE TABLE [dbo].[t_ErrorHandling] (
    [ErrorCode] DECIMAL (24)   IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [ErrorName] NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_t_ErrorHandling] PRIMARY KEY CLUSTERED ([ErrorCode] ASC)
);

