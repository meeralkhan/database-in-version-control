﻿CREATE TABLE [dbo].[SchedulerTest] (
    [LogID]       NVARCHAR (450) NOT NULL,
    [Description] NVARCHAR (MAX) NULL,
    [DateTime]    DATETIME       NULL,
    CONSTRAINT [PK_SchedulerTest] PRIMARY KEY CLUSTERED ([LogID] ASC)
);

