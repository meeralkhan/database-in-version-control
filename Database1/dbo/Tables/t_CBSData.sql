﻿CREATE TABLE [dbo].[t_CBSData] (
    [OurBranchID]            VARCHAR (30)  NOT NULL,
    [AccountID]              VARCHAR (30)  NOT NULL,
    [TerminalName]           VARCHAR (30)  NOT NULL,
    [Description]            VARCHAR (100) NOT NULL,
    [Transaction_Particular] VARCHAR (255) NOT NULL,
    [ValueDate]              DATETIME      NOT NULL,
    [TrxDate]                DATETIME      NOT NULL,
    [TrxIdentifier]          VARCHAR (100) NULL,
    [TerminalID]             VARCHAR (30)  NULL,
    [Customer_Account_Date]  DATETIME      NOT NULL,
    [TrxTime]                VARCHAR (30)  NULL,
    [STAN]                   VARCHAR (30)  NULL,
    [AuthIDResp]             VARCHAR (30)  NULL,
    [RetRefNum]              VARCHAR (30)  NULL,
    [PAN]                    VARCHAR (30)  NULL,
    [TrxType]                CHAR (1)      NOT NULL,
    [Amount]                 MONEY         NOT NULL,
    [Balance]                MONEY         NOT NULL,
    [CardAccptID]            VARCHAR (30)  NULL
);

