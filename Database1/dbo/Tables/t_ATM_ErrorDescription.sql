﻿CREATE TABLE [dbo].[t_ATM_ErrorDescription] (
    [ErrorSerial]      NUMERIC (18)  IDENTITY (1, 1) NOT NULL,
    [ErrorDate]        DATETIME      NOT NULL,
    [ErrorTime]        VARCHAR (50)  NOT NULL,
    [ErrorDescription] VARCHAR (500) NOT NULL
);

