﻿CREATE TABLE [dbo].[t_DepositLienHistory] (
    [OurBranchID]         NVARCHAR (30)  NOT NULL,
    [ReceiptID]           NVARCHAR (30)  NOT NULL,
    [SerialID]            DECIMAL (18)   NOT NULL,
    [AccountID]           NVARCHAR (30)  NOT NULL,
    [LienDate]            DATETIME       NOT NULL,
    [LienRemarks]         VARCHAR (300)  NOT NULL,
    [IsLienMarkInAccount] CHAR (1)       NOT NULL,
    [LienMarkAccountID]   NVARCHAR (30)  NOT NULL,
    [LienMarkAccountName] NVARCHAR (100) NOT NULL,
    [LienMarkOperatorID]  VARCHAR (15)   NOT NULL,
    [UnLienDate]          DATETIME       NULL,
    [UnLienRemarks]       VARCHAR (300)  NULL,
    [UnLienOperatorID]    VARCHAR (15)   NULL
);

