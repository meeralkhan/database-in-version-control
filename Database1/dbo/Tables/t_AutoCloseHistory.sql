﻿CREATE TABLE [dbo].[t_AutoCloseHistory] (
    [OurBranchID]     VARCHAR (30)  NOT NULL,
    [wDate]           DATETIME      NOT NULL,
    [ProductID]       VARCHAR (30)  NOT NULL,
    [AccountID]       VARCHAR (30)  NOT NULL,
    [AccountName]     VARCHAR (100) NOT NULL,
    [ClearBalance]    MONEY         NOT NULL,
    [BeforMarkStatus] CHAR (1)      NOT NULL,
    [TerminalID]      VARCHAR (50)  NOT NULL,
    [OperatorID]      VARCHAR (30)  NOT NULL,
    CONSTRAINT [PK_t_AutoCloseHistory] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [wDate] ASC, [ProductID] ASC, [AccountID] ASC)
);

