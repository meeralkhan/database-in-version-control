﻿CREATE TABLE [dbo].[t_AccKYCExcLogs] (
    [OurBranchID] NVARCHAR (30)  NOT NULL,
    [SerialNo]    DECIMAL (24)   IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [AccountID]   NVARCHAR (30)  NOT NULL,
    [DateTime]    DATETIME       NOT NULL,
    [KYCParam]    NVARCHAR (100) NOT NULL,
    [Message]     TEXT           NOT NULL,
    CONSTRAINT [PK_t_AccKYCExcLogs] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [SerialNo] ASC)
);

