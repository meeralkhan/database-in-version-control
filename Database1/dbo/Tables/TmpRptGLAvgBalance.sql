﻿CREATE TABLE [dbo].[TmpRptGLAvgBalance] (
    [OurBranchID]   NVARCHAR (30)  NOT NULL,
    [Accountid]     NVARCHAR (30)  NOT NULL,
    [Description]   NVARCHAR (100) NULL,
    [CurrencyID]    NVARCHAR (30)  NULL,
    [LCYClosingBal] MONEY          NULL,
    [FCYClosingBal] MONEY          NULL,
    [AccountType]   VARCHAR (22)   NOT NULL,
    [Date]          DATETIME       NULL
);

