﻿CREATE TABLE [dbo].[t_MurabahahBaseRate_History] (
    [CreateBy]          NVARCHAR (30)   NOT NULL,
    [CreateTime]        DATETIME        NOT NULL,
    [CreateTerminal]    NVARCHAR (30)   NOT NULL,
    [UpdateBy]          NVARCHAR (30)   NULL,
    [UpdateTime]        DATETIME        NULL,
    [UpdateTerminal]    NVARCHAR (30)   NULL,
    [AuthStatus]        CHAR (1)        NOT NULL,
    [SuperviseBy]       NVARCHAR (30)   NULL,
    [SuperviseTime]     DATETIME        NULL,
    [SuperviseTerminal] NVARCHAR (30)   NULL,
    [OurBranchID]       NVARCHAR (30)   NOT NULL,
    [RateID]            NVARCHAR (30)   NOT NULL,
    [ChangeDate]        DATETIME        NOT NULL,
    [Description]       NVARCHAR (100)  NULL,
    [frmName]           VARCHAR (100)   NULL,
    [VerifyBy]          NVARCHAR (30)   NULL,
    [VerifyTime]        DATETIME        NULL,
    [VerifyTerminal]    NVARCHAR (30)   NULL,
    [VerifyStatus]      CHAR (1)        NOT NULL,
    [MarketRate]        DECIMAL (18, 6) NOT NULL,
    CONSTRAINT [PK_t_MurabahahBaseRate_History] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [RateID] ASC, [ChangeDate] ASC)
);

