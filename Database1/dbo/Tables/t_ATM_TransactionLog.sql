﻿CREATE TABLE [dbo].[t_ATM_TransactionLog] (
    [OurBranchId]      VARCHAR (30)   NOT NULL,
    [SerialNo]         NUMERIC (18)   IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [IDate]            DATETIME       NOT NULL,
    [IMessage]         NVARCHAR (MAX) NULL,
    [ODate]            DATETIME       NOT NULL,
    [OMessage]         NVARCHAR (MAX) NULL,
    [ResponseCode]     VARCHAR (2)    NOT NULL,
    [Remarks]          VARCHAR (100)  NOT NULL,
    [ErrorQueryString] VARCHAR (500)  NOT NULL,
    [ErrorErrDesc]     VARCHAR (500)  NOT NULL,
    CONSTRAINT [PK_t_ATM_TransactionLog] PRIMARY KEY CLUSTERED ([OurBranchId] ASC, [SerialNo] ASC)
);

