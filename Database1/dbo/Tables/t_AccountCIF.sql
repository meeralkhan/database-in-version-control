﻿CREATE TABLE [dbo].[t_AccountCIF] (
    [OurBranchID] NVARCHAR (30) NOT NULL,
    [AccountID]   NVARCHAR (30) NOT NULL,
    [ClientID]    NVARCHAR (30) NOT NULL,
    CONSTRAINT [PK_t_AccountCIF] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [AccountID] ASC, [ClientID] ASC)
);

