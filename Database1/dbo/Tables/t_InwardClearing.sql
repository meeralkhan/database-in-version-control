﻿CREATE TABLE [dbo].[t_InwardClearing] (
    [SerialNo]        INT             NOT NULL,
    [OurBranchID]     VARCHAR (30)    NOT NULL,
    [BranchID]        CHAR (30)       NOT NULL,
    [BankID]          VARCHAR (30)    NOT NULL,
    [AccountID]       VARCHAR (30)    NOT NULL,
    [ProductID]       VARCHAR (30)    NOT NULL,
    [ChequeID]        VARCHAR (30)    NOT NULL,
    [ChequeDate]      DATETIME        NOT NULL,
    [CurrencyID]      VARCHAR (30)    NULL,
    [AccountName]     VARCHAR (100)   NULL,
    [AccountType]     CHAR (1)        NULL,
    [TransactionType] CHAR (1)        NOT NULL,
    [wDate]           DATETIME        NULL,
    [ValueDate]       DATETIME        NULL,
    [DescriptionID]   VARCHAR (30)    NULL,
    [Description]     VARCHAR (255)   NOT NULL,
    [Payee]           VARCHAR (100)   NULL,
    [Amount]          MONEY           NOT NULL,
    [ForeignAmount]   MONEY           NULL,
    [ExchangeRate]    DECIMAL (18, 6) NULL,
    [CheckedBy]       VARCHAR (30)    NULL,
    [Reason]          VARCHAR (255)   NULL,
    [Status]          CHAR (1)        NULL,
    [OperatorID]      VARCHAR (30)    NOT NULL,
    [SupervisorID]    VARCHAR (30)    NOT NULL,
    [IsApplyCharges]  BIT             NOT NULL,
    [ChargesScrollNo] INT             NOT NULL,
    [ChargesAmount]   MONEY           NOT NULL,
    [AdditionalData]  TEXT            NULL,
    [AppWHTax]        INT             NOT NULL,
    [WHTaxAmount]     MONEY           NOT NULL,
    [WHTaxMode]       CHAR (1)        NOT NULL,
    [WHTScrollNo]     INT             NOT NULL,
    CONSTRAINT [PK_t_InwardClearing] PRIMARY KEY CLUSTERED ([SerialNo] ASC, [OurBranchID] ASC)
);


GO

CREATE TRIGGER [dbo].[tr_InwardTrxCashDr] ON [dbo].[t_InwardClearing]    
FOR INSERT    
AS    
    
	IF (Select ACCOUNTTYPE from Inserted)='C'    
	AND (Select DescriptionID from inserted) IN (select DescriptionID from t_TransactionDescriptions where ISNULL(IsInternal,1) = 0 and IsCredit = 0)    
    
	Update t_Account SET CashTotDr=ISNULL(CashTotDr,0)+(SELECT Abs(AMOUNT) FROM INSERTED),    
	CashTotDrF=ISNULL(CashTotDrF,0)+(SELECT Abs(ForeignAmount) FROM INSERTED)  
   
	WHERE OurBranchID=(SELECT OurBranchID FROM INSERTED) AND AccountID=(SELECT AccountID FROM INSERTED) AND (select ACCOUNTTYPE from inserted)='C'
GO

CREATE TRIGGER [dbo].[tr_InwardTrxCashReject] ON [dbo].[t_InwardClearing]   
  
FOR  UPDATE   
AS  
  
	If ((Select [Status] from inserted)='R' AND  (select ACCOUNTTYPE from inserted)='C'  
	AND (Select DescriptionID from inserted) IN (select DescriptionID from t_TransactionDescriptions   
	where ISNULL(IsInternal,1) = 0 and IsCredit = 0))  
  
	Update t_Account SET CashTotDr=CashTotDr-(SELECT abs(AMOUNT) FROM INSERTED),  
	CashTotDrF=CashTotDrF-(SELECT abs(ForeignAmount) FROM INSERTED)  
  
	WHERE OurBranchID=(SELECT OurBranchID FROM INSERTED) AND AccountID=  
	(SELECT AccountID FROM INSERTED) AND (select ACCOUNTTYPE from inserted)='C'
GO
 
 CREATE TRIGGER [dbo].[tr_InwardTrxCashDr_Delete] ON [dbo].[t_InwardClearing]  
FOR DELETE  
AS    
    
	IF (Select ACCOUNTTYPE from Deleted)='C'    
	AND (Select DescriptionID from Deleted) IN (select DescriptionID from t_TransactionDescriptions  
	where ISNULL(IsInternal,1) = 0 and IsCredit = 0)    
    
	Update t_Account SET CashTotDr=ISNULL(CashTotDr,0)-(SELECT Abs(AMOUNT) FROM Deleted),    
	CashTotDrF=ISNULL(CashTotDrF,0)-(SELECT Abs(ForeignAmount) FROM Deleted)  
   
	WHERE OurBranchID=(SELECT OurBranchID FROM Deleted) AND AccountID=(SELECT AccountID FROM Deleted)  
	AND (select ACCOUNTTYPE from Deleted)='C'