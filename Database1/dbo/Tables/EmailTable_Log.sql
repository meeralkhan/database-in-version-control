﻿CREATE TABLE [dbo].[EmailTable_Log] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [Exception]    NVARCHAR (MAX) NULL,
    [CreatedDate]  DATETIME       NULL,
    [ChannelRefId] NVARCHAR (200) NULL,
    [Level]        NVARCHAR (20)  NULL,
    [EmailId]      INT            NULL,
    CONSTRAINT [PK_EmailTable_Log] PRIMARY KEY CLUSTERED ([Id] ASC)
);

