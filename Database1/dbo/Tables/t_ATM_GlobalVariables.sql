﻿CREATE TABLE [dbo].[t_ATM_GlobalVariables] (
    [Version]           VARCHAR (8)   NOT NULL,
    [CheckLimit]        BIT           NOT NULL,
    [TaxDeduction]      MONEY         NOT NULL,
    [TaxLimit]          MONEY         NOT NULL,
    [TaxAccountID]      VARCHAR (30)  NOT NULL,
    [UBPS_73_Allowed]   BIT           NOT NULL,
    [TitleFetch_62]     BIT           NOT NULL,
    [3PFT_40]           BIT           NOT NULL,
    [IBFT_48]           BIT           NOT NULL,
    [CashDeposit_15]    BIT           NOT NULL,
    [CreditCardBill_22] BIT           NOT NULL,
    [NonTariff]         BIT           NOT NULL,
    [LocalCurrencyID]   VARCHAR (30)  NOT NULL,
    [LocalCurrency]     VARCHAR (30)  NOT NULL,
    [BalanceUploaded]   BIT           NOT NULL,
    [OurBranchID]       VARCHAR (30)  NOT NULL,
    [AppDomain]         VARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_t_ATM_GlobalVariables] PRIMARY KEY CLUSTERED ([OurBranchID] ASC)
);

