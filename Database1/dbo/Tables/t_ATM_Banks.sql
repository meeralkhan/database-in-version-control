﻿CREATE TABLE [dbo].[t_ATM_Banks] (
    [OurBranchID]   VARCHAR (30)  NOT NULL,
    [BankIMD]       VARCHAR (30)  NOT NULL,
    [BankShortName] VARCHAR (30)  NOT NULL,
    [BankName]      VARCHAR (100) NOT NULL,
    [ODLimitAllow]  BIT           NOT NULL,
    CONSTRAINT [PK_t_ATM_Banks] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [BankIMD] ASC)
);

