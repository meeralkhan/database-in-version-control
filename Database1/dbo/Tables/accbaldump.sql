﻿CREATE TABLE [dbo].[accbaldump] (
    [OurBranchID]  NVARCHAR (30) NOT NULL,
    [AccountID]    NVARCHAR (30) NOT NULL,
    [ProductID]    NVARCHAR (30) NULL,
    [CurrencyID]   NVARCHAR (60) NULL,
    [ClearBalance] MONEY         NULL,
    [TrxBalance]   MONEY         NULL,
    [BalDiff]      MONEY         NULL
);

