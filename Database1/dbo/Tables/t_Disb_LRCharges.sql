﻿CREATE TABLE [dbo].[t_Disb_LRCharges] (
    [CreateBy]       NVARCHAR (30)  NOT NULL,
    [CreateTime]     DATETIME       NOT NULL,
    [CreateTerminal] NVARCHAR (30)  NOT NULL,
    [UpdateBy]       NVARCHAR (30)  NULL,
    [UpdateTime]     DATETIME       NULL,
    [UpdateTerminal] NVARCHAR (30)  NULL,
    [frmName]        VARCHAR (100)  NULL,
    [OurBranchID]    NVARCHAR (30)  NOT NULL,
    [AccountID]      NVARCHAR (30)  NOT NULL,
    [DealID]         DECIMAL (24)   NOT NULL,
    [ChargeID]       NVARCHAR (30)  NOT NULL,
    [BankID]         NVARCHAR (30)  NOT NULL,
    [BranchID]       NVARCHAR (30)  NOT NULL,
    [ChargeType]     NVARCHAR (50)  NOT NULL,
    [ChargeName]     NVARCHAR (100) NOT NULL,
    [Charges]        MONEY          NOT NULL,
    [VerifyBy]       NVARCHAR (30)  NULL,
    [VerifyTime]     DATETIME       NULL,
    [VerifyTerminal] NVARCHAR (30)  NULL,
    [VerifyStatus]   CHAR (1)       NOT NULL,
    CONSTRAINT [PK_t_Disb_LRCharges] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [AccountID] ASC, [DealID] ASC, [ChargeID] ASC)
);

