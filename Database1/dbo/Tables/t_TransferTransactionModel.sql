﻿CREATE TABLE [dbo].[t_TransferTransactionModel] (
    [ScrollNo]            INT             NOT NULL,
    [SerialNo]            NUMERIC (10)    NOT NULL,
    [RefNo]               VARCHAR (100)   NULL,
    [OurBranchID]         VARCHAR (30)    NOT NULL,
    [AccountID]           VARCHAR (30)    NOT NULL,
    [AccountName]         CHAR (100)      NULL,
    [ProductID]           VARCHAR (30)    NOT NULL,
    [CurrencyID]          VARCHAR (30)    NULL,
    [AccountType]         CHAR (1)        NOT NULL,
    [ValueDate]           DATETIME        NULL,
    [wDate]               DATETIME        NULL,
    [TrxType]             CHAR (1)        NOT NULL,
    [ChequeID]            VARCHAR (30)    NULL,
    [ChequeDate]          DATETIME        NULL,
    [Amount]              NUMERIC (18, 6) NOT NULL,
    [ForeignAmount]       NUMERIC (18, 6) NULL,
    [ExchangeRate]        DECIMAL (18, 6) NULL,
    [DescriptionID]       VARCHAR (30)    NOT NULL,
    [Description]         NVARCHAR (500)  NULL,
    [BankCode]            VARCHAR (30)    NOT NULL,
    [BranchCode]          VARCHAR (30)    NOT NULL,
    [TrxPrinted]          BIT             NOT NULL,
    [ProfitOrLoss]        MONEY           NOT NULL,
    [GlID]                VARCHAR (30)    NOT NULL,
    [Supervision]         CHAR (1)        NULL,
    [RemoteDescription]   VARCHAR (100)   NULL,
    [IsSupervision]       BIT             NOT NULL,
    [IsLocalCurrency]     INT             NOT NULL,
    [OperatorID]          VARCHAR (30)    NOT NULL,
    [SupervisorID]        VARCHAR (30)    NOT NULL,
    [AdditionalData]      TEXT            NOT NULL,
    [AppWHTax]            BIT             NOT NULL,
    [WHTaxScrollNo]       INT             NOT NULL,
    [ChannelId]           NVARCHAR (30)   NOT NULL,
    [VirtualAccountID]    NVARCHAR (30)   NOT NULL,
    [VirtualAccountTitle] NVARCHAR (100)  NOT NULL,
    [ChannelRefID]        VARCHAR (200)   NULL,
    [TrxTimeStamp]        DATETIME        NOT NULL,
    [PrevDayEntry]        INT             NULL,
    [ChargeID]            NVARCHAR (30)   NULL,
    [VatID]               NVARCHAR (30)   NULL,
    [Percent]             MONEY           NULL,
    [RejectRemarks]       NVARCHAR (200)  NULL,
    [PrevYearEntry]       INT             NULL,
    [TClientId]           NVARCHAR (30)   NULL,
    [TAccountid]          NVARCHAR (30)   NULL,
    [TProductId]          NVARCHAR (30)   NULL,
    [CostCenterID]        NVARCHAR (30)   NULL,
    [GLVendorID]          NVARCHAR (30)   NULL,
    [memo2]               NVARCHAR (250)  NULL,
    [VendorID]            NVARCHAR (30)   NULL,
    [CostTypeID]          NVARCHAR (30)   NULL,
    [CustomerLifecycleID] NVARCHAR (30)   NULL,
    [ProjectId]           NVARCHAR (100)  NULL,
    [PostExpenseID]       NVARCHAR (30)   NULL,
    [VatReferenceID]      NVARCHAR (30)   NULL,
    CONSTRAINT [PK_t_TransferTransactionModel] PRIMARY KEY CLUSTERED ([ScrollNo] ASC, [SerialNo] ASC, [OurBranchID] ASC)
);


GO

CREATE TRIGGER [dbo].[tr_TransferTrxCashReject] ON [dbo].[t_TransferTransactionModel]   
  
FOR  UPDATE   
AS  
 declare @TaxFiler int  
   
 select @TaxFiler = ISNULL(TaxFiler,0) FROM t_Account  
 WHERE OurBranchID = (SELECT OurBranchID FROM INSERTED)  
 AND AccountID = (SELECT AccountID FROM INSERTED)  
   
 IF @TaxFiler = 0  
 BEGIN  
      
     If ((Select supervision from inserted)='R' AND  (select ACCOUNTTYPE from inserted)='C'  
  AND (Select DescriptionID from inserted) IN (select DescriptionID from t_TransactionDescriptions   
  where ISNULL(IsInternal,1) = 0 and IsCredit = 0) And (select TRXTYPE from inserted)='D')  
    
  Update t_Account SET CashTotDr=CashTotDr-(SELECT abs(AMOUNT) FROM INSERTED),  
  CashTotDrF=CashTotDrF-(SELECT abs(ForeignAmount) FROM INSERTED)  
  WHERE OurBranchID=(SELECT OurBranchID FROM INSERTED) AND AccountID=(SELECT AccountID FROM INSERTED)  
  AND (select ACCOUNTTYPE from inserted)='C'  And (select TRXTYPE from inserted)='D'  
    
 END
GO

CREATE TRIGGER [dbo].[tr_TransferTrxCashDr] ON [dbo].[t_TransferTransactionModel]    
FOR INSERT    
AS    
   
 declare @TaxFiler int  
   
 select @TaxFiler = ISNULL(TaxFiler,0) FROM t_Account  
 WHERE OurBranchID = (SELECT OurBranchID FROM INSERTED)  
 AND AccountID = (SELECT AccountID FROM INSERTED)  
   
 IF @TaxFiler = 0  
 BEGIN  
      
    IF (Select TRXTYPE from inserted)='D' and (Select ACCOUNTTYPE from Inserted)='C'    
    AND (Select DescriptionID from inserted) IN (select DescriptionID from t_TransactionDescriptions  
    where ISNULL(IsInternal,1) = 0 and IsCredit = 0)  
      
    Update t_Account SET CashTotDr=ISNULL(CashTotDr,0)+(SELECT Abs(AMOUNT) FROM INSERTED),  
    CashTotDrF=ISNULL(CashTotDrF,0)+(SELECT Abs(ForeignAmount) FROM INSERTED)  
    WHERE OurBranchID=(SELECT OurBranchID FROM INSERTED) AND AccountID=(SELECT AccountID FROM INSERTED)  
    AND (select ACCOUNTTYPE from inserted)='C'  And (select TRXTYPE from inserted)='D'  
      
 END