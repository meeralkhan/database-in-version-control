﻿CREATE TABLE [dbo].[t_PreviousDayBalances_Cust] (
    [CreateBy]          VARCHAR (30) NOT NULL,
    [CreateTime]        DATETIME     NOT NULL,
    [CreateTerminal]    VARCHAR (30) NOT NULL,
    [OurBranchID]       VARCHAR (30) NOT NULL,
    [WorkingDate]       DATETIME     NOT NULL,
    [AccountID]         VARCHAR (30) NOT NULL,
    [ProductID]         VARCHAR (30) NOT NULL,
    [Name]              VARCHAR (50) NULL,
    [ClearBalance]      MONEY        NULL,
    [Effects]           MONEY        NULL,
    [LocalClearBalance] MONEY        NULL,
    [LocalEffects]      MONEY        NULL,
    CONSTRAINT [PK_t_PreviousDayBalances_Cust] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [WorkingDate] ASC, [AccountID] ASC)
);

