﻿CREATE TABLE [dbo].[t_Cheque_ChargesExempt] (
    [LogID]             INT            IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [OurBranchID]       NVARCHAR (30)  NOT NULL,
    [AccountID]         NVARCHAR (30)  NOT NULL,
    [ChequeStart]       NVARCHAR (30)  NOT NULL,
    [ChequeEnd]         NVARCHAR (30)  NOT NULL,
    [ChargeID]          NVARCHAR (30)  NOT NULL,
    [ChargeSerial]      NVARCHAR (100) NOT NULL,
    [Description]       NVARCHAR (100) NOT NULL,
    [Type]              NVARCHAR (10)  NOT NULL,
    [Amount]            MONEY          NOT NULL,
    [TotalAmount]       MONEY          NOT NULL,
    [ModeOfCharges]     NVARCHAR (10)  NOT NULL,
    [CreateBy]          NVARCHAR (30)  NOT NULL,
    [CreateTime]        DATETIME       NOT NULL,
    [CreateTerminal]    NVARCHAR (30)  NOT NULL,
    [frmName]           NVARCHAR (30)  NOT NULL,
    [SuperviseBy]       NVARCHAR (30)  NULL,
    [SuperviseTime]     DATETIME       NULL,
    [SuperviseTerminal] NVARCHAR (30)  NULL,
    CONSTRAINT [PK_t_Cheque_ChargesExempt] PRIMARY KEY CLUSTERED ([LogID] ASC)
);

