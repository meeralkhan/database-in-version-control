﻿CREATE TABLE [dbo].[t_ATM_OneLinkChargesSlab] (
    [OurBranchID] VARCHAR (30) NOT NULL,
    [SerialID]    INT          NOT NULL,
    [AmountFrom]  MONEY        NOT NULL,
    [AmountTo]    MONEY        NOT NULL,
    [Charges]     MONEY        NOT NULL,
    CONSTRAINT [PK_t_ATM_OneLinkChargesSlab] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [SerialID] ASC)
);

