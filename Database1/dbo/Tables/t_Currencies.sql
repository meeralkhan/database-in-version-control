﻿CREATE TABLE [dbo].[t_Currencies] (
    [CreateBy]          NVARCHAR (30)   NOT NULL,
    [CreateTime]        DATETIME        NOT NULL,
    [CreateTerminal]    NVARCHAR (30)   NOT NULL,
    [UpdateBy]          NVARCHAR (30)   NULL,
    [UpdateTime]        DATETIME        NULL,
    [UpdateTerminal]    NVARCHAR (30)   NULL,
    [AuthStatus]        CHAR (1)        NOT NULL,
    [SuperviseBy]       NVARCHAR (30)   NULL,
    [SuperviseTime]     DATETIME        NULL,
    [SuperviseTerminal] NVARCHAR (30)   NULL,
    [OurBranchID]       NVARCHAR (30)   NOT NULL,
    [CurrencyID]        NVARCHAR (30)   NOT NULL,
    [Description]       NVARCHAR (100)  NULL,
    [BuyingRate]        DECIMAL (18, 6) NULL,
    [SellingRate]       DECIMAL (18, 6) NULL,
    [PreferredBuy]      DECIMAL (18, 6) NULL,
    [PreferredSell]     DECIMAL (18, 6) NULL,
    [NotesBuy]          DECIMAL (18, 6) NULL,
    [NotesSell]         DECIMAL (18, 6) NULL,
    [MeanRate]          DECIMAL (18, 6) NULL,
    [CRRounding]        DECIMAL (24)    NULL,
    [DBRounding]        DECIMAL (24)    NULL,
    [CurrencyCode]      NVARCHAR (20)   NULL,
    [frmName]           VARCHAR (100)   NULL,
    [AccCurrencyID]     NVARCHAR (30)   NULL,
    [VerifyBy]          NVARCHAR (30)   NULL,
    [VerifyTime]        DATETIME        NULL,
    [VerifyTerminal]    NVARCHAR (30)   NULL,
    [VerifyStatus]      CHAR (1)        NOT NULL,
    [RetainedEarningGL] NVARCHAR (30)   NULL,
    [CBUAEBuy]          DECIMAL (18, 6) NULL,
    [CBUAESell]         DECIMAL (18, 6) NULL,
    CONSTRAINT [PK_t_Currencies] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [CurrencyID] ASC)
);


GO

CREATE Trigger [dbo].[Edit_CurrencyHistory] ON [dbo].[t_Currencies]        
for update        
AS        
BEGIN        
SET NOCOUNT ON         
        
  IF EXISTS(SELECT * FROM inserted)        
  BEGIN        
   Declare @WorkingDate datetime, @AuthStatus char(1)        
        
   SELECT @WorkingDate=WorkingDate + ' ' + convert(varchar(10), getdate(), 108) from t_Last         
   WHERE OurBranchID = (Select OurBranchID from Inserted)      
         
   select @AuthStatus = AuthStatus from Inserted        
                
   insert into t_Currencies_History (CreateBy, CreateTime, CreateTerminal, UpdateBy, UpdateTime,         
   UpdateTerminal, AuthStatus, SuperviseBy, SuperviseTime, SuperviseTerminal, OurBranchID, CurrencyID,         
   ChangeDate, [Description], BuyingRate, SellingRate, PreferredBuy, PreferredSell, NotesBuy, NotesSell,         
   MeanRate, CRRounding, DBRounding, CurrencyCode, frmName, AccCurrencyID)        
   select CreateBy, CreateTime, CreateTerminal, UpdateBy, UpdateTime, UpdateTerminal, @AuthStatus,         
   SuperviseBy, SuperviseTime, SuperviseTerminal, OurBranchID, CurrencyID, @WorkingDate, [Description],         
   BuyingRate, SellingRate, PreferredBuy, PreferredSell, NotesBuy, NotesSell, MeanRate, CRRounding, DBRounding,         
   CurrencyCode, frmName, AccCurrencyID        
   FROM Inserted  
   where isnull(AuthStatus,'') = ''        
  END        
END
GO
CREATE Trigger [dbo].[Delete_CurrencyHistory] ON [dbo].[t_Currencies]        
after delete     
AS        
BEGIN        
 SET NOCOUNT ON         
        
  IF EXISTS(SELECT * FROM deleted)        
  BEGIN        
         
   Declare @WorkingDate datetime        
         
   SELECT @WorkingDate=WorkingDate + ' ' + convert(varchar(10), getdate(), 108) from t_Last       
   WHERE OurBranchID = (Select OurBranchID from deleted)      
                
   -- inserted        
   insert into t_Currencies_History (CreateBy, CreateTime, CreateTerminal, UpdateBy, UpdateTime,         
   UpdateTerminal, AuthStatus, SuperviseBy, SuperviseTime, SuperviseTerminal, OurBranchID, CurrencyID,         
   ChangeDate, [Description], BuyingRate, SellingRate, PreferredBuy, PreferredSell, NotesBuy, NotesSell,         
   MeanRate, CRRounding, DBRounding, CurrencyCode, frmName, AccCurrencyID)        
   select CreateBy, CreateTime, CreateTerminal, UpdateBy, UpdateTime, UpdateTerminal, AuthStatus,         
   SuperviseBy, SuperviseTime, SuperviseTerminal, OurBranchID, CurrencyID, @WorkingDate, [Description],         
   BuyingRate, SellingRate, PreferredBuy, PreferredSell, NotesBuy, NotesSell, MeanRate, CRRounding, DBRounding,         
   CurrencyCode, frmName, AccCurrencyID        
   FROM deleted        
  END        
END
GO
  
CREATE Trigger [dbo].[Add_CurrencyHistory] ON [dbo].[t_Currencies]    
for insert    
AS          
BEGIN          
SET NOCOUNT ON           
          
  IF EXISTS(SELECT * FROM inserted)          
  BEGIN          
   Declare @WorkingDate datetime, @AuthStatus char(1)          
          
   SELECT @WorkingDate=WorkingDate + ' ' + convert(varchar(10), getdate(), 108) from t_Last           
   WHERE OurBranchID = (Select OurBranchID from inserted)    
           
   select @AuthStatus = AuthStatus from inserted    
                  
   insert into t_Currencies_History (CreateBy, CreateTime, CreateTerminal, UpdateBy, UpdateTime,           
   UpdateTerminal, AuthStatus, SuperviseBy, SuperviseTime, SuperviseTerminal, OurBranchID, CurrencyID,           
   ChangeDate, [Description], BuyingRate, SellingRate, PreferredBuy, PreferredSell, NotesBuy, NotesSell,           
   MeanRate, CRRounding, DBRounding, CurrencyCode, frmName, AccCurrencyID)          
   select CreateBy, CreateTime, CreateTerminal, UpdateBy, UpdateTime, UpdateTerminal, @AuthStatus,           
   SuperviseBy, SuperviseTime, SuperviseTerminal, OurBranchID, CurrencyID, @WorkingDate, [Description],           
   BuyingRate, SellingRate, PreferredBuy, PreferredSell, NotesBuy, NotesSell, MeanRate, CRRounding, DBRounding,           
   CurrencyCode, frmName, AccCurrencyID          
   FROM inserted    
   where isnull(AuthStatus,'') = ''          
  END          
END