﻿CREATE TABLE [dbo].[t_HO_Account] (
    [OurBranchID]          NVARCHAR (30)  NOT NULL,
    [AccountID]            NVARCHAR (30)  NOT NULL,
    [ClientID]             NVARCHAR (30)  NOT NULL,
    [CreateBy]             NVARCHAR (30)  NOT NULL,
    [CreateTime]           DATETIME       NOT NULL,
    [CreateTerminal]       NVARCHAR (30)  NOT NULL,
    [SuperviseBy]          NVARCHAR (30)  NOT NULL,
    [SuperviseTime]        DATETIME       NOT NULL,
    [SuperviseTerminal]    NVARCHAR (30)  NOT NULL,
    [VerifyBy]             NVARCHAR (30)  NULL,
    [VerifyTime]           DATETIME       NULL,
    [VerifyTerminal]       NVARCHAR (30)  NULL,
    [VerifyStatus]         CHAR (1)       NOT NULL,
    [LastRecWDate]         DATETIME       NULL,
    [RealTimeComment]      NVARCHAR (250) NULL,
    [ShortageComment]      NVARCHAR (250) NULL,
    [ExcessComment]        NVARCHAR (250) NULL,
    [CloseAccountTrxRefNo] NVARCHAR (50)  NULL,
    CONSTRAINT [PK_t_HO_Account] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [AccountID] ASC)
);

