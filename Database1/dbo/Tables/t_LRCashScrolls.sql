﻿CREATE TABLE [dbo].[t_LRCashScrolls] (
    [OurBranchID] VARCHAR (30) NOT NULL,
    [AccountID]   VARCHAR (30) NOT NULL,
    [wDate]       DATETIME     NOT NULL,
    [ScrollNo]    DECIMAL (24) NOT NULL,
    [Amount]      MONEY        NOT NULL,
    CONSTRAINT [PK_t_LRCashScrolls] PRIMARY KEY CLUSTERED ([OurBranchID] ASC, [AccountID] ASC, [wDate] ASC, [ScrollNo] ASC)
);

