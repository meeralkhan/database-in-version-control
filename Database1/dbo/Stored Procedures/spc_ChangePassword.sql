﻿CREATE PROCEDURE [dbo].[spc_ChangePassword]    
(                  
  @OurBranchID varchar(30),                  
  @Username varchar(30),                  
  @OPassword varchar(max),                  
  @NPassword varchar(max),                  
  @CPassword varchar(max),            
  @WorkingDate datetime,      
  @UserTerminal varchar(30)            
)                  
AS                    
        
set nocount on             
             
BEGIN TRY                  
                   
 declare @OldPassword varchar(255)                  
                   
 if @NPassword != @CPassword                  
 begin                  
 select 'Error' AS ReturnStatus, 'PasswordDoesNotMatch' AS ReturnMessage;                  
 end                  
 else IF (select COUNT(*) FROM t_Map where OurBranchID = @OurBranchID AND OperatorID = @Username AND (ISNULL(CannotChgPwd,0) = 0 OR ISNULL(ChgPwdNext,1) = 1)) > 0                  
 begin              
                   
 select @OldPassword = [Password] FROM t_Map where OurBranchID = @OurBranchID AND OperatorID = @Username;                  
                   
 if @OldPassword != @OPassword                  
 begin                  
  select 'Error' AS ReturnStatus, 'IncorrectOldPassword' AS ReturnMessage;                  
 end                  
 else                  
 begin                  
        
  IF EXISTS (SELECT [Password] FROM tbl_UserPasswords WHERE OurBranchID = @OurBranchID AND Username = @Username AND             
  @NPassword IN (SELECT TOP 11 [Password] FROM tbl_UserPasswords WHERE OurBranchID = @OurBranchID AND Username = @Username ORDER BY CreateTime DESC))            
  BEGIN            
                 
     select 'Error' AS ReturnStatus, 'PasswordAlreadyUsed' AS ReturnMessage;            
                 
  END            
  ELSE            
  BEGIN            
                 
     UPDATE t_Map SET [Password] = @NPassword, LastPasswordChg = @WorkingDate, ChgPwdNext = 0 WHERE OurBranchID = @OurBranchID AND OperatorID = @Username;            
           
     INSERT INTO tbl_UserPasswords (OurBranchID, Username, [Password], CreateTime, CreateTeminal)            
     VALUES (@OurBranchID, @Username, @NPassword, GETDATE(), @UserTerminal);            
                 
     select 'Ok' AS ReturnStatus, 'PasswordChanged' AS ReturnMessage;            
                 
  END            
              
              
 end                  
 end                  
 else                  
 begin                  
 select 'Error' AS ReturnStatus, 'UnSufficientRights' AS ReturnMessage;                  
 end                  
                   
END TRY                  
BEGIN CATCH                  
 select 'Error' AS ReturnStatus, ERROR_NUMBER() AS ReturnErrorNo, ERROR_MESSAGE() AS ReturnMessage;                  
END CATCH