﻿CREATE PROCEDURE [dbo].[sp_ATM_AddEditTransactionLog]      
 (      
  @OurBranchID    varchar(30),      
  @IDate    datetime='01/01/1900',      
  @IMessage    nvarchar(max),      
  @ODate    datetime='01/01/1900',      
  @OMessage    nvarchar(max),      
  @ResponseCode    varchar(2)='',      
  @Remarks    varchar(50)='',      
  @ErrorQueryString varchar(500)='',      
  @ErrorErrDesc  varchar(500)='',      
  @NewRecord           bit=1      
 )      
       
AS      
 IF cast(@NewRecord as bit) = 1      
  BEGIN      
   INSERT INTO t_ATM_TransactionLog (OurBranchID, IDate, IMessage, ODate, OMessage, ResponseCode, Remarks, ErrorQueryString, ErrorErrDesc)  
   VALUES (@OurBranchID, @IDate, @IMessage, @ODate, @OMessage, @ResponseCode, @Remarks, @ErrorQueryString, @ErrorErrDesc)  
  END  
  ELSE  
  BEGIN   
   UPDATE t_ATM_TransactionLog      
   SET  ODate=@ODate,       
    OMessage=@OMessage,  
    ResponseCode=@ResponseCode,       
    Remarks=@Remarks,      
    ErrorQueryString=@ErrorQueryString,      
    ErrorErrDesc=@ErrorErrDesc      
   WHERE (OurBranchID=@OurBranchID) and (IMessage=@IMessage)       
   and SerialNo = (      
    select max(SerialNo) from  t_ATM_TransactionLog       
    where (OurBranchID=@OurBranchID) and (IMessage=@IMessage)      
    )      
  END