﻿CREATE PROCEDURE [dbo].[spc_UpdateLastTrxDate]        
(        
  @OurBranchID varchar(30)        
)        
AS        
        
SET NOCOUNT ON        
      
UPDATE t_AccountBalance SET    
NoOfCrTrx = 0, NoOfDrTrx = 0, AmountOfCrTrx = 0, AmountOfDrTrx = 0  
, NoOfCWTrxATM = 0, NoOfBITrxATM = 0, AmountOfCWTrxATM = 0, AmountOfBITrxATM = 0    
where OurBranchID = @OurBranchID        
        
Declare @wdate datetime        
Select @wdate = WorkingDate from t_Last where OurBranchID = @OurBranchID        
        
UPDATE t_Account SET LastDebitTransaction = @wDate, CashTotDr=0,CashTotCr=0,CashTotDrF=0,CashTotCrF=0        
WHERE OurBranchID = @OurBranchID AND AccountID IN (Select AccountID From t_Transactions        
Where OurBranchID = @OurBranchID AND wDate >= @wDate AND TrxType = 'D' AND DescriptionID        
Not In('I01','I02','I03','I04','I10','I20','I30','I40','A01','A02','SCR','P01','R01'))        
        
UPDATE t_Account SET LastCreditTransaction = @wDate        
WHERE OurBranchID = @OurBranchID AND AccountID IN (Select AccountID From t_Transactions        
Where OurBranchID = @OurBranchID AND wDate >= @wDate AND TrxType = 'C' AND DescriptionID        
Not In('I01','I02','I03','I04','I10','I20','I30','I40','A01','A02','SCR','P01','R01'))        
        
select 'Ok' AS RetStatus