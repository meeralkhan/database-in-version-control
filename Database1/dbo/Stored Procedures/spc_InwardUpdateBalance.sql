﻿CREATE PROCEDURE [dbo].[spc_InwardUpdateBalance]    
      
 @OurBranchID varchar(30),      
 @AccountID varchar(30),      
 @Amount money,      
 @ForeignAmount Money,      
 @Status Char(1)='',      
 @IsLocalCurrency integer=1,      
 @TransactionType char(1)='D',    
 @NewRecord bit = 0      
      
 AS      
     
 SET NOCOUNT ON    
     
 if @Status=''  /* If No Supervision Required */      
 BEGIN      
  if @TransactionType = 'D'      
  BEGIN      
   if @IsLocalCurrency = 1      
   BEGIN      
    Update t_AccountBalance      
    Set ClearBalance = ClearBalance - @Amount      
    Where  OurBranchID = @OurBranchID and AccountID = @AccountID      
   END      
   else      
   BEGIN      
    Update t_AccountBalance      
    Set ClearBalance = ClearBalance - @ForeignAmount,      
    LocalClearBalance = LocalClearBalance - @Amount      
    Where  OurBranchID = @OurBranchID and AccountID = @AccountID      
   END      
      
  END      
  else      
  BEGIN      
   if @IsLocalCurrency = 1      
   BEGIN        
    Update t_AccountBalance      
    Set ClearBalance = ClearBalance + @Amount      
    Where  OurBranchID = @OurBranchID and AccountID = @AccountID      
   END      
   else      
   BEGIN        
    Update t_AccountBalance      
    Set ClearBalance = ClearBalance + @ForeignAmount,      
    LocalClearBalance = LocalClearBalance + @Amount      
    Where  OurBranchID = @OurBranchID and AccountID = @AccountID      
   END      
      
  END      
 END      
 else      
 BEGIN      
  if @TransactionType = 'D'      
  BEGIN      
   if @IsLocalCurrency = 1      
   BEGIN      
    Update t_AccountBalance      
    Set ShadowBalance = ShadowBalance - @Amount      
    Where  OurBranchID = @OurBranchID and AccountID = @AccountID      
   END      
   else      
   BEGIN      
    Update t_AccountBalance      
    Set ShadowBalance = ShadowBalance - @ForeignAmount      
    Where  OurBranchID = @OurBranchID and AccountID = @AccountID      
   END      
  END      
  else      
  BEGIN      
   if @IsLocalCurrency = 1      
   BEGIN      
    Update t_AccountBalance      
    Set ShadowBalance = ShadowBalance + @Amount      
    Where  OurBranchID = @OurBranchID and AccountID = @AccountID      
   END      
   else      
   BEGIN      
    Update t_AccountBalance      
    Set ShadowBalance = ShadowBalance + @ForeignAmount      
    Where  OurBranchID = @OurBranchID and AccountID = @AccountID      
   END      
  END      
 END      
      
 IF @@Error = 0      
  RETURN(0)      
 ELSE      
  RETURN(30)