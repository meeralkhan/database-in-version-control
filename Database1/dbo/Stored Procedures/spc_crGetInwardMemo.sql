﻿Create Procedure [dbo].[spc_crGetInwardMemo]  
(  
 @OurBranchID as Varchar(30)=''   
)  
As  
  
set nocount on  
  
 If Exists(SELECT t.AccountID FROM t_InwardClearing t LEFT OUTER JOIN t_branches br ON t.OurBranchID = br.OurBranchID AND   
           t.BankID = br.BankID AND t.BranchID = br.BranchID LEFT OUTER JOIN t_Banks b ON t.OurBranchID = b.OurBranchID AND   
           t.BankID = b.BankID Where t.OurBranchID=@OurBranchID and Status = 'R')  
  
   SELECT '1' As ReturnValue, t.AccountID, t.ChequeDate, t.AccountName, t.ChequeID, t.Amount, t.Reason, b.FullName, t.Status, br.Name  
   FROM t_InwardClearing t LEFT OUTER JOIN t_branches br ON t.OurBranchID = br.OurBranchID AND t.BankID = br.BankID   
   AND t.BranchID = br.BranchID LEFT OUTER JOIN t_Banks b ON t.OurBranchID = b.OurBranchID AND t.BankID = b.BankID  
   Where t.OurBranchID = @OurBranchID and Status = 'R'   
  
 Else  
   
   Select '0' As ReturnValue