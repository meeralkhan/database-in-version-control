﻿create PROCEDURE [dbo].[spc_GetGLStatement]          
(                   
 @OurBranchID varchar(30),                   
 @AccountID varchar(30),                   
 @sDate Datetime,                   
 @eDate Datetime                   
)                   
                   
AS                   
                   
 SET NOCOUNT ON                   
                   
 DECLARE @OpeningBalance as money                   
 DECLARE @crOpBal as money                   
 DECLARE @drOpBal as money                   
                   
 DECLARE @WorkingDate as datetime                   
                   
 SELECT @WorkingDate = WorkingDate From t_Last                   
 WHERE OurBranchID = @OurBranchID                   
                   
 SET @OpeningBalance = 0                   
 SET @crOpBal = 0                   
 SET @drOpBal = 0                   
                   
                   
                   
 SELECT @OpeningBalance = IsNull(OpeningBalance,0)                   
 FROM t_GL                   
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                   
                   
 SELECT @crOpBal = IsNull(SUM(Amount),0) From t_GLTransactions                   
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                   
 AND IsCredit = '1' and [Status] <> 'R'              
 --AND convert(varchar(10) , Date,101) < @sDate                   
 AND Date < @sDate                   
                   
 SELECT @drOpBal = IsNull(SUM(Amount),0) From t_GLTransactions                   
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                   
 AND IsCredit = '0' and [Status] <> 'R'              
 --AND convert(varchar(10) , Date,101) < @sDate                   
 AND Date < @sDate                   
                   
 SET @OpeningBalance = @OpeningBalance + @crOpBal - @drOpbal                   
                   
 IF @WorkingDate = @eDate                   
 BEGIN                   
 SELECT '' CostCenterID,'' SupervisorID,'' OperatorID,'01/01/1900' as wDate, '01/01/1900' as TrxTimeStamp, @sDate as ValueDate ,'Opening Balance' as Description,'' as ChequeID, '' as TrxType,IsNull(@OpeningBalance,0) as Amount                   
 ,'OP' As TableStr,0 ScrollNo ,'' DocumentType, '' as Remarks      ,'' ChannelRefID    
                   
 UNION ALL                   
                   
 SELECT CostCenterID,SupervisorID,OperatorID,Date as wDate, TrxTimeStamp,ValueDate , Description,'' as ChequeID,CASE IsCredit WHEN 1 THEN 'C' ELSE 'D' END As TrxType, Amount                   
 ,'H' As TableStr,ScrollNo ,TransactionType DocumentType, Remarks      ,ChannelRefID    
 FROM t_GLTransactions                   
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID and [Status] <> 'R'              
 AND convert(varchar(10) , Date,101) >= @sDate AND convert(varchar(10) , Date,101) <= @eDate      
 order by TrxTimeStamp      
                    
/*                        
 UNION ALL                   
              
--t_CashTransactionModel                   
 SELECT wDate,ValueDate , Description, ChequeID,TrxType, Amount                   
 ,'C' As TableStr,ScrollNo , 'C' DocumentType                   
 FROM t_CashTransactionModel                   
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                   
 AND Supervision = 'C'                   
                   
 UNION ALL                   
                   
--t_CashTransactionModel Credit Side                   
 SELECT wDate,ValueDate , Description, ChequeID,'D' as TrxType, Amount                   
 ,'CCr' As TableStr,ScrollNo ,'C' DocumentType                   
 FROM t_CashTransactionModel                   
 WHERE OurBranchID = @OurBranchID AND GLID = @AccountID                   
 AND Supervision = 'C' AND TrxType = 'C'                   
                   
 UNION ALL                   
                   
--t_CashTransactionModel Debit Side                   
 SELECT wDate,ValueDate , Description, ChequeID,'C' as TrxType, Amount                   
 ,'CDr' As TableStr,ScrollNo , 'C' DocumentType                   
 FROM t_CashTransactionModel                   
 WHERE OurBranchID = @OurBranchID AND GLID = @AccountID                   
 AND Supervision = 'C' AND TrxType = 'D'                   
                   
 UNION ALL                   
              
--t_TransferTransactionModel                   
 SELECT wDate,ValueDate , Description, ChequeID,TrxType, Amount                   
 ,'T' As TableStr,ScrollNo , 'T' DocumentType              
 FROM t_TransferTransactionModel                   
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                   
 AND Supervision = 'C' AND AccountType ='G'                   
                   
 UNION ALL                   
              
--t_InWardClearing                   
 SELECT wDate,ValueDate , Description, ChequeID,TransactionType as TrxType, Amount                   
 ,'I' As TableStr,SerialNO ScrollNo ,'ID' DocumentType                   
 FROM t_InWardClearing                   
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                   
 AND Status <>'R' AND AccountType ='G'                   
                   
 UNION ALL                   
                   
--t_InWardClearing Main Office Account With Gl Parameter Serial No - 2                   
 SELECT wDate,ValueDate , Description, ChequeID,'C' as TrxType, Amount                   
 ,'IMo' As TableStr,SerialNo ScrollNo , 'ID' DocumentType                   
 FROM t_InWardClearing                   
 WHERE OurBranchID = @OurBranchID AND AccountID = (SELECT AccountID From t_GLParameters WHERE SerialID ='2' AND OurBranchID = @OurBranchID )                   
 AND Status <>'R' AND AccountType ='G'                   
                   
 UNION ALL                   
              
              
--t_BureaDeChange - Cash Account ID                   
 SELECT wDate, wDate AS ValueDate, Description, ChequeID,                   
 CASE SellOrBuy WHEN 'B' THEN 'C' ELSE 'D' END AS TrxType, NetAmount, 'B' As TableStr,                   
 ScrollNo, 'B' DocumentType                   
 FROM t_BureaDeChange                   
 WHERE OurBranchID = @OurBranchID                   
 AND CashAccountID = @AccountID                   
 AND Supervision = 'C'                   
                   
 UNION ALL                   
                   
--t_BureaDeChange - Comm. Account ID                   
 SELECT wDate, wDate AS ValueDate, Description, ChequeID, 'C' AS TrxType,                   
 Commission AS Amount, 'B' As TableStr, ScrollNo, 'B' DocumentType                   
 FROM t_BureaDeChange                   
 WHERE OurBranchID = @OurBranchID                   
 AND CommAccountID = @AccountID                   
 AND Supervision = 'C'                   
               
 UNION ALL                   
                   
--t_ATM_CashTransaction                   
 SELECT wDate,ValueDate , Description,'' as ChequeID,'C' as TrxType, Amount                   
 ,'ATM' As TableStr,ScrollNo , 'AT' DocumentType                   
 FROM t_ATM_CashTransaction                   
 WHERE OurBranchID = @OurBranchID AND GLID = @AccountID                   
 AND Supervision = 'C'                   
                   
 UNION ALL                   
                   
--t_ATM_CashTransaction                   
 SELECT wDate,ValueDate , Description,'' as ChequeID,'D' as TrxType, Amount                   
 ,'ATM' As TableStr,ScrollNo , 'AT' DocumentType                   
 FROM t_ATM_CashTransaction                   
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                   
 AND Supervision = 'C'                   
                   
 UNION ALL                   
                   
--'t_ATM_TransferTransaction                   
 SELECT wDate,ValueDate , Description,'' as ChequeID,TrxType, Amount                   
 ,'ATMT' As TableStr,ScrollNo , 'AT' DocumentType                   
 FROM t_ATM_TransferTransaction                   
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                   
 AND Supervision = 'C' AND AccountType ='G'                   
               
 UNION ALL                   
                   
--'t_OnLine_CashTransaction                   
 SELECT wDate,ValueDate , Description, 'V' ChequeID,TrxType, Amount                   
 ,'OL' As TableStr, ScrollNo , 'OL' DocumentType                   
 FROM t_OnLineCashTransaction                   
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                   
 AND Supervision <> 'R' AND AccountType ='G'                   
               
 UNION ALL                   
                   
--'t_OL_LocalTransferTransaction                   
 SELECT wDate,ValueDate , Description, ChequeID,TrxType, Amount                   
 ,'OT' As TableStr, ScrollNo,'OT' as DocumentType                   
 FROM t_OL_LocalTransferTransaction                   
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                   
 AND Supervision <> 'R' AND AccountType ='G'                   
 */                   
                   
 END                   
                   
 ELSE -- If Statement not in workingdate                   
                   
 BEGIN                   
                   
 SELECT '' CostCenterID,'' SupervisorID,'' OperatorID,'01/01/1900' as wDate, '01/01/1900' as TrxTimeStamp, @sDate as ValueDate ,'Opening Balance' as Description,'' as ChequeID, '' as TrxType,IsNull(@OpeningBalance,0) as Amount                   
 ,'OP' As TableStr,0 ScrollNo ,'' DocumentType, '' Remarks      ,'' ChannelRefID    
                   
 UNION ALL                   
                   
 SELECT CostCenterID,SupervisorID,OperatorID,Date as wDate, TrxTimeStamp,ValueDate , Description,'' as ChequeID,CASE IsCredit WHEN 1 THEN 'C' ELSE 'D' END As TrxType, Amount                   
 ,'H' As TableStr,ScrollNo, TransactionType DocumentType, Remarks      ,ChannelRefID    
 FROM t_GLTransactions                   
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID and [Status] <> 'R'              
 AND convert(varchar(10) , Date,101) >= @sDate AND convert(varchar(10) , Date,101) <= @eDate                   
 order by TrxTimeStamp      
                   
 END