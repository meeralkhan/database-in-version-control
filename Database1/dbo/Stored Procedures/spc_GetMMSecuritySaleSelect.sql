﻿CREATE Procedure [dbo].[spc_GetMMSecuritySaleSelect]
(  
  @OurBranchID varchar(30),  
  @SecurityID varchar(30),  
  @IsDisplay char(1)='0',  
  @SettlementDate datetime,  
  @DealNo varchar(30)=''
)

As

SET NOCOUNT ON

IF @IsDisplay='0'
  
  SELECT * FROM v_MMSecuritySaleSelect
  WHERE OurBranchID = @OurBranchID AND Status<>'R' AND
  SecurityIssue= @SecurityID and Settlementdate <= @Settlementdate-- AND IsPledge <> '1' --AND Balance <>0 AND IsPostedGL <>'1'
  
ELSE
  
  SELECT P_DealNo, S_Amount as SaleAmount , S_IntRecv, S_PreRecvPaid, P_IntPaid, P_PreRecvPaid, IntRecvd,P_AmtExp,
  SecurityID,IsPledge From t_MMSecuritySaleHistory WHERE OurBranchID=@OurBranchID AND DealNo=@DealNo AND IsPledge = '0'