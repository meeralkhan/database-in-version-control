﻿CREATE   PROCEDURE [dbo].[sp_ATM_AddEditCashReversalTransaction]  
(  
 @OurBranchID varchar(30),  
 @RefNo varchar(50),  
 @VISATrxID varchar(30)='',  
 @CAVV char(1)='',  
 @ResponseCode varchar(2)='',
 @AuthIDResp as varchar(6)=''
)  
 AS  
 SET NOCOUNT ON  
  
 DECLARE @Status            as int  
 DECLARE @vCount1           as int = 0  
 DECLARE @AccountID         as varchar (30)  
 DECLARE @mAccountID         as varchar (30)  
 DECLARE @Description       as varchar (255)   
 DECLARE @mwDate            as datetime  
 DECLARE @mLastEOD          as datetime  
 DECLARE @mCONFRETI         as datetime  
 DECLARE @vIsClose          as char(1)  
 DECLARE @vClearBalance as money        
 DECLARE @AvailableBalance as money        
 DECLARE @Value as varchar(2)='00'  
 DECLARE @vClearedEffects as money        
 DECLARE @vFreezeAmount as money        
 DECLARE @vMinBalance as money        
 DECLARE @Limit as money        
 DECLARE @CurrencyCode as varchar(30)  
 DECLARE @vCheckLimit as bit  
     SET @vCheckLimit = 0  
 DECLARE @vODLimitAllow as bit  
     SET @vODLimitAllow = 0  
 DECLARE @HoldStatus as char(1)  
 DECLARE @HoldAmount as money  
 DECLARE @FreezeAmount as money  
 DECLARE @AcqInstID as varchar(30)  
 DECLARE @TrxCount as int = 0
  DECLARE @GLTrxCount as int = 0
  DECLARE @cCount as int = 0
  DECLARE @sCount as int = 0
  
  DECLARE @IsCredit as bit
  DECLARE @mOurBranchID as varchar(30)
  DECLARE @mScrollNo as numeric(18,0)
  DECLARE @ScrollNo as numeric(18,0)
  DECLARE @SerialNo as int
  DECLARE @SerialID as numeric(10,0)
  DECLARE @mRefNo as varchar(50)
  DECLARE @wDate as Datetime
  DECLARE @AccountType as varchar(30)
  DECLARE @DocumentType as varchar(30)
  DECLARE @AccountName as varchar(100)
  DECLARE @ProductID as varchar(30)
  DECLARE @CurrencyID as varchar(30)
  DECLARE @ValueDate as Datetime
  DECLARE @TrxType as char(1)
  DECLARE @ChequeID as varchar(30) 
  DECLARE @ChequeDate as Datetime
  DECLARE @Amount as money
  DECLARE @ForeignAmount as money
  DECLARE @ExchangeRate as decimal(16,8)
  DECLARE @ProfitLoss as money
  DECLARE @MeanRate as money
  DECLARE @DescriptionID as varchar(30)
  DECLARE @BankCode as varchar(30)
  DECLARE @BranchCode as varchar(30)
  DECLARE @TrxPrinted as bit
  DECLARE @GLID as varchar(30)
  DECLARE @IsLocalCurrency as bit
  DECLARE @OperatorID as varchar(30)
  DECLARE @SupervisorID as varchar(30)
  DECLARE @IsMainTrx as smallint
  DECLARE @DocType as char(3)
  DECLARE @GLVoucherID as decimal(24,0)
  DECLARE @VoucherID as decimal(24,0)
  DECLARE @Remarks as varchar(500)
  DECLARE @MerchantType as varchar(30)
  DECLARE @STAN as varchar(30)
  DECLARE @SettlmntAmount as money
  DECLARE @ConvRate as decimal(15,9)
  DECLARE @AcqCountryCode as varchar(3)
  DECLARE @CurrCodeTran as varchar(3)
  DECLARE @CurrCodeSett as varchar(3)
  DECLARE @ATMStatus as char(1)
  DECLARE @POSEntryMode as varchar(3)
  DECLARE @POSConditionCode as varchar(2)
  DECLARE @POSPINCaptCode as varchar(2)
  DECLARE @RetRefNum as varchar(15)
  DECLARE @CardAccptID as varchar(15)
  DECLARE @CardAccptNameLoc as varchar(50)
  DECLARE @ChannelId as varchar(30)
  DECLARE @ChannelRefID as varchar(200)
  DECLARE @ProcCode as varchar(50)
  DECLARE @ForwardInstID as varchar(10)
  DECLARE @ClearingRefNo as varchar(100)
  DECLARE @TransactionType as varchar(32)
  DECLARE @TransactionMethod as char(1)
  DECLARE @Indicator as char(2)
  DECLARE @AppWHTax as smallint
  DECLARE @WHTaxAmount as money
  DECLARE @WHTScrollNo as numeric(18,0)
   SET @AppWHTax = 0
   SET @WHTaxAmount = 0
  DECLARE @PHXDate as datetime 
  DECLARE @ATMID as varchar(30)
  
  DECLARE @MobileNo nvarchar(30)
  DECLARE @Email nvarchar(100)
  DECLARE @rCurrencyID nvarchar(30)
  DECLARE @ClientID nvarchar(30)
  DECLARE @NarrationID nvarchar(30)
   SET @MobileNo = ''
   SET @Email = ''
   SET @rCurrencyID = ''
   SET @ClientID = ''
   SET @NarrationID = ''
  
  DECLARE @VATID nvarchar(30)
  DECLARE @VATPercent money
  DECLARE @TClientId nvarchar(30)
  DECLARE @TAccountid nvarchar(30)
  DECLARE @TProductId nvarchar(30)
  DECLARE @VatReferenceID as varchar(30)  
   SET @VATID = NULL
   SET @VATPercent = NULL
   SET @TClientId = NULL
   SET @TAccountid = NULL
   SET @TProductId = NULL
  
  if (@AuthIDResp = '')
  begin
   SET @AuthIDResp = '0'
  end
 
 SELECT @mLastEOD=LastEOD,@mwDate=WorkingDate,@mCONFRETI=CONFRETI FROM t_Last WHERE (OurBranchID = @OurBranchID)  
 
 SELECT @AccountID=AccountID,@STAN=STAN,@PHXDate=PHXDate,@ForeignAmount=ForeignAmount,@ExchangeRate=ExchangeRate,@Amount=Amount,@MerchantType=MerchantType,
 @AcqCountryCode=AcqCountryCode,@ForwardInstID=ForwardInstID,@ATMID=ATMID,@CurrCodeTran=CurrCodeTran,@CurrCodeSett=CurrCodeSett,@SettlmntAmount=SettlmntAmount,@ProcCode=ProcCode,
 @POSEntryMode=POSEntryMode,@POSConditionCode=POSConditionCode,@POSPINCaptCode=POSPINCaptCode,@AcqInstID=AcqInstID,@RetRefNum=RetRefNum,
 @AuthIDResp = Case when @AuthIDResp = '0' then ISNULL(AuthIDResp,'0') else @AuthIDResp end,
 @CardAccptID=CardAccptID,@CardAccptNameLoc=CardAccptNameLoc,@Description=ISNULL([Description],''),@CAVV=CAVV,@DescriptionID=DescriptionID
 FROM t_ATM_HoldTrxs WHERE OurBranchID=@OurBranchID AND RefNo = @RefNo
 
 --SET @NarrationID = @DescriptionID
 SET @NarrationID = 'FREVXX'
  
 IF @mLastEOD = @mwDate or @mCONFRETI = '1/1/9999'  
 BEGIN --INVALID_DATE  
 
  INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, 
  ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, 
  CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
  VALUES (@OurBranchID, @AccountID, @RefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @ForeignAmount, @ExchangeRate, @ConvRate, 
  @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, 
  @AcqInstID, @RetRefNum, @CardAccptID, @CardAccptNameLoc, @Description, @VISATrxID, @CAVV, @ResponseCode, @DescriptionID, '95', 'Day End', 'Reversal Trx', @AuthIDResp, '')
  
  SELECT @Status = 95, @AvailableBalance = 0, @vClearBalance = 0
  SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value,'784' as CurrencyCode, @AuthIDResp as AuthIDResp, 
  @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
  RETURN (1)   
 END  
  
 /* First check whether the pre-authorization exists for the trx */  
   
 SELECT @vCount1=count(*),@AccountID=AccountID,@HoldStatus=HoldStatus,@AcqInstID=AcqInstID,@AuthIDResp=ISNULL(AuthIDResp,'0'),@Description=[Description]  
 FROM t_ATM_HoldTrxs   
 WHERE (OurBranchID=@OurBranchID) AND (RefNo IN (@RefNo,@RefNo+'|CHGS1',@RefNo+'|CHGS2'))  
 group by AccountID,HoldStatus,AcqInstID,AuthIDResp,[Description]  
  
 SELECT @vCheckLimit=CheckLimit FROM t_ATM_GlobalVariables Where OurBranchID = @OurBranchID  
 SELECT @vODLimitAllow=ODLimitAllow FROM t_ATM_Banks WHERE OurBranchID = @OurBranchID AND BankIMD = @AcqInstID  
  
 IF (@vCount1 > 0)  
 BEGIN  
  SELECT @vClearBalance=ISNULL(B.ClearBalance,0),@vClearedEffects=ISNULL(B.Effects,0),@vFreezeAmount=ISNULL(B.FreezeAmount,0),@Limit=ISNULL(B.Limit,0),
  @vMinBalance=ISNULL(P.MinBalance,0),@vIsClose=ISNULL(A.Status,''),@CurrencyCode = ISNULL(c.CurrencyCode,''),@value=ISNULL(p.productatmcode,'00'), 
  @MobileNo=ISNULL(cust.MobileNo,''),@Email=ISNULL(cust.Email,''),@ClientID=cust.ClientID
  FROM t_AccountBalance B   
  INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND  B.ProductID = A.ProductID   
  INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND  A.ProductID = P.ProductID   
  INNER JOIN t_Currencies C ON A.OurBranchID = C.OurBranchID AND A.CurrencyID = C.CurrencyID  
  INNER JOIN t_Customer cust ON A.OurBranchID = cust.OurBranchID AND a.ClientID = cust.ClientID
  WHERE (A.OurBranchID = @OurBranchID) AND (A.AccountID = @AccountID)  
     
  IF @Limit > 0 AND @vCheckLimit = 1 AND @vODLimitAllow = 1  
  BEGIN  
   SET @vClearBalance=(@vClearBalance+@Limit)  
  END  
  
  IF (@HoldStatus = 'R' OR @HoldStatus = 'P')  
  BEGIN --ORIG_ALREADY_REVERSED        
  
   INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, 
   ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, 
   CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
   VALUES (@OurBranchID, @AccountID, @RefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @ForeignAmount, @ExchangeRate, @ConvRate, 
   @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, 
   @AcqInstID, @RetRefNum, @CardAccptID, @CardAccptNameLoc, @Description, @VISATrxID, @CAVV, @ResponseCode, @DescriptionID, '35', 'Orig Already Reversed', 'Reversal Trx', 
   @AuthIDResp, '')
  
   SELECT @Status = 35,   
   @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,  
   @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100  
  
   SELECT @Status As Status , @vClearBalance As ClearBalance , @AvailableBalance As AvailableBalance , @value As [value], @CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp, 
   @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
   RETURN (1)   
  END    
  
  IF (@HoldStatus = 'H')  
  BEGIN  
   --Reverse Hold Trx Status  
   UPDATE t_ATM_HoldTrxs SET HoldStatus = 'R', ReleaseDate = @mwDate + ' ' + convert(varchar(12), GetDate(), 114), [Description] = 'REV-'+@Description, ResponseCode = @ResponseCode  
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND HoldStatus = 'H' AND RefNo IN (@RefNo,@RefNo+'|CHGS1',@RefNo+'|CHGS2')   
  
   --Get Hold Amount  
   SELECT @HoldAmount=sum(Amount) FROM t_AccountFreeze   
   WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND IsFreeze = 1 AND ATMTrxHoldRef IN (@RefNo, @RefNo+'|CHGS1', @RefNo+'|CHGS2')  
  
   --update account freeze to unfreeze  
   Update t_AccountFreeze SET IsFreeze = 0, UnFreezedDate = @mwDate + ' ' + convert(varchar(12), GetDate(), 114), ReleaseComments = 'REV-'+@Description  
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND ATMTrxHoldRef IN (@RefNo,@RefNo+'|CHGS1',@RefNo+'|CHGS2')  
   
   Update t_AccountBalance SET FreezeAmount = ISNULL(FreezeAmount,0) - ISNULL(@HoldAmount,0)  
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID  
   
   SELECT @FreezeAmount = FreezeAmount FROM t_AccountBalance WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID  
     
   IF (@FreezeAmount <= 0)  
   BEGIN  
    Update t_AccountBalance SET IsFreezed = 0  
    WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID  
   END  
     
   /* Log the Updated Balance after releasing freeze */  
        
   Insert Into BalChk   
   (  
    OurBranchID,AccountID,ProductID,[Name],LastUpdateTime,ClearBalance,Limit,Effects,Shadow,FreezeAmt,MinBalance,TotalBalance,AvailableBalance  
   )  
   SELECT OurBranchID,AccountID,ProductID,[Name],@mwDate + ' ' + convert(varchar(12), GetDate(), 114),ISNULL(ClearBalance,0),ISNULL(Limit,0),ISNULL(Effects,0),
   ISNULL(ShadowBalance,0),ISNULL(FreezeAmount,0), @vMinBalance,   
   (isnull(@Limit,0)+ISNULL(ClearBalance,0)-ISNULL(Effects,0)),  
   (isnull(@Limit,0)+ISNULL(ClearBalance,0)-ISNULL(FreezeAmount,0)-@vMinBalance)  
   FROM t_AccountBalance WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID  
     
   /* Get Latest Balance again.. */  
     
   SELECT @vClearBalance=ISNULL(B.ClearBalance,0),@vClearedEffects=ISNULL(B.Effects,0),@vFreezeAmount=ISNULL(B.FreezeAmount,0),@Limit=ISNULL(B.Limit,0),
   @vMinBalance=ISNULL(P.MinBalance,0),@vIsClose=ISNULL(A.Status,''),@CurrencyCode = ISNULL(c.CurrencyCode,''),@value=ISNULL(p.productatmcode,'00'),
   @MobileNo=ISNULL(cust.MobileNo,''),@Email=ISNULL(cust.Email,''),@ClientID=cust.ClientID
   FROM t_AccountBalance B   
   INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND  B.ProductID = A.ProductID   
   INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND  A.ProductID = P.ProductID   
   INNER JOIN t_Currencies C ON A.OurBranchID = C.OurBranchID AND A.CurrencyID = C.CurrencyID  
   INNER JOIN t_Customer cust ON A.OurBranchID = cust.OurBranchID AND A.ClientID = cust.ClientID
   WHERE (A.OurBranchID = @OurBranchID) AND (A.AccountID = @AccountID)  
     
   --exec @Limit = [fnc_GetLimit] @OurBranchID, @AccountID  
     
   IF @Limit > 0 AND @vCheckLimit = 1 AND @vODLimitAllow = 1  
   BEGIN  
    SET @vClearBalance=(@vClearBalance+@Limit)  
   END  
  
   SELECT @Status = 0,     
   @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,  
   @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100  
     
   SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value,@CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp, 
   @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
   RETURN (0)  
   
  END  
  ELSE  
  BEGIN  
   INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, 
   ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, 
   CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
   VALUES (@OurBranchID, @AccountID, @RefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @ForeignAmount, @ExchangeRate, @ConvRate, 
   @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, 
   @AcqInstID, @RetRefNum, @CardAccptID, @CardAccptNameLoc, @Description, @VISATrxID, @CAVV, @ResponseCode, @DescriptionID, '82', 'Permission Denied', 'Reversal Trx', 
   @AuthIDResp, '')
   
   SELECT @Status = 82, @AvailableBalance = 0, @vClearBalance = 0  
   SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value,'784' as CurrencyCode, @AuthIDResp as AuthIDResp, @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
   RETURN (1)  
  END  
 END  
 ELSE  
 BEGIN  
 
  SELECT @TrxCount=count(AccountID) FROM t_Transactions
  WHERE (OurBranchID=@OurBranchID) and (RefNo IN (@RefNo,@RefNo+'|CHGS1',@RefNo+'|CHGS2')) and (Status = 'C') AND (DocType = 'ATC')
  and CONVERT(varchar(10), wDate, 120) <= CONVERT(varchar(10), @mwDate, 120)
  
  if (@TrxCount = 0) 
  begin
  
   INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, 
   ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, 
   CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
   VALUES (@OurBranchID, @AccountID, @RefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @ForeignAmount, @ExchangeRate, @ConvRate, 
   @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, 
   @AcqInstID, @RetRefNum, @CardAccptID, @CardAccptNameLoc, @Description, @VISATrxID, @CAVV, @ResponseCode, @DescriptionID, '21', 'No Transaction on Account', 'Reversal Trx', 
   @AuthIDResp, '')
  
   SELECT @Status = 21, @AvailableBalance = 0, @vClearBalance = 0  
   SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value,'784' as CurrencyCode, @AuthIDResp as AuthIDResp, 
   @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
   RETURN (1)  
   
  end
  ELSE 
  BEGIN
  
   SET @sCount = 0    
   SET @cCount = 1    
      
   WHILE (@sCount <> @cCount)      
   BEGIN      
    exec @mScrollNo = fnc_GetRandomScrollNo    
    if not exists (SELECT ScrollNo from t_ATM_CashTransaction Where OurBranchID = @OurBranchID AND ScrollNo = @mScrollNo)    
    BEGIN    
     SET @sCount = @sCount + 1      
    END    
   END
   
   SET @sCount = 0
   SET @VoucherID = @mScrollNo
    
   WHILE (@sCount <> @TrxCount)
   BEGIN      
   
    SET @sCount = @sCount + 1  
   
    SELECT @mOurBranchID=OurBranchID,@ScrollNo=ScrollNo,@SerialNo=SerialNo,@mRefNo=RefNo,@wDate=wDate,@AccountType=AccountType,@DocumentType=DocumentType,@mAccountID=AccountID,@AccountID=AccountID,@AccountName=AccountName,@ProductID=ProductID,@CurrencyID=CurrencyID,@TrxType=TrxType,@ChequeID=ChequeID,@ChequeDate=ChequeDate,@Amount=Amount,@ForeignAmount=ForeignAmount,@ExchangeRate=ExchangeRate,@ProfitLoss=ProfitLoss,@MeanRate=MeanRate,@DescriptionID=DescriptionID,@Description=[Description],@BankCode=BankCode,@BranchCode=BranchCode,@TrxPrinted=TrxPrinted,@GLID=GLID,@IsLocalCurrency=IsLocalCurrency,@OperatorID=OperatorID,@SupervisorID=SupervisorID,@IsMainTrx=IsMainTrx,@DocType=DocType,@GLVoucherID=GLVoucherID,@Remarks=Remarks,@MerchantType=MerchantType,@STAN=STAN,@SettlmntAmount=SettlmntAmount,@ConvRate=ConvRate,@AcqCountryCode=AcqCountryCode,@CurrCodeTran=CurrCodeTran,@CurrCodeSett=CurrCodeSett,@ATMStatus=ATMStatus,@POSEntryMode=POSEntryMode,@POSConditionCode=POSConditionCode,@POSPINCaptCode=POSPINCaptCode,@AcqInstID=AcqInstID,@RetRefNum=RetRefNum,@AuthIDResp=ISNULL(AuthIDResp,'0'),@CardAccptID=CardAccptID,@CardAccptNameLoc=CardAccptNameLoc,@VISATrxID=VISATrxID,@ChannelId=ChannelId,@ChannelRefID=ChannelRefID,@ProcCode=ProcCode,@CAVV=CAVV,@ResponseCode=ResponseCode,@ForwardInstID=ForwardInstID,@ClearingRefNo=ClearingRefNo,@AppWHTax=AppWHTax,@WHTaxAmount=WHTaxAmount,@WHTScrollNo=WHTScrollNo,@VATID=VATID,@VATPercent=[Percent],@TClientId=TClientId,@TAccountid=TAccountid,@TProductId=TProductId,@VatReferenceID=VatReferenceID
	
    From t_Transactions WHERE (OurBranchID=@OurBranchID) and (RefNo IN (@RefNo,@RefNo+'|CHGS1',@RefNo+'|CHGS2')) and (Status = 'C') 
    and CONVERT(varchar(10), wDate, 120) <= CONVERT(varchar(10), @mwDate, 120) AND SerialNo = @sCount
    
    IF @TrxType = 'C'
    BEGIN
     Set @TrxType = 'D'
    END
    ELSE IF @TrxType = 'D'
    BEGIN
     Set @TrxType = 'C'
    END
	
    INSERT INTO t_ATM_CashTransaction       
    (
     ScrollNo,SerialNo,RefNo,OurBranchID,CustomerBranchID,CustomerAccountID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,ValueDate,wDate,PHXDate,TrxType,
     Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,[Description],ATMID,Remarks,OperatorID,SupervisorID,MerchantType,STAN,SettlmntAmount,ConvRate,AcqCountryCode,
     CurrCodeTran,CurrCodeSett,ATMStatus,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,CardAccptID,CardAccptNameLoc,VISATrxID,ProcCode,CAVV,ResponseCode,ForwardInstID,ClearingRefNo
    )      
    VALUES          
    ( 
	 @mScrollNo,@SerialNo,'R'+@mRefNo,@OurBranchID,@OurBranchID,@AccountID,@AccountID,@AccountName,@AccountType,@ProductID,@CurrencyID,@IsLocalCurrency,@mwDate,
     @mwDate + ' ' + convert(varchar(12), GetDate(), 114),@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@TrxType,'C',@GLID,@Amount,@ForeignAmount,@ExchangeRate,@DescriptionID,
     'REV '+@Description,@OperatorID,'REV '+@Remarks,@OperatorID,@SupervisorID,@MerchantType,@STAN,@SettlmntAmount,@ConvRate,@AcqCountryCode,@CurrCodeTran,@CurrCodeSett,@ATMStatus,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AcqInstID,@RetRefNum,@AuthIDResp,@CardAccptID,@CardAccptNameLoc,@VISATrxID,@ProcCode,@CAVV,@ResponseCode,@ForwardInstID,
	 @ClearingRefNo
    )    
       
    INSERT INTO t_Transactions     
    (      
     OurBranchID,ScrollNo,SerialNo,RefNo,wDate,AccountType,DocumentType,AccountID,AccountName,ProductID,CurrencyID,ValueDate,TrxType,ChequeID,ChequeDate,Amount,
	 ForeignAmount,ExchangeRate,ProfitLoss,MeanRate,DescriptionID,[Description],BankCode,BranchCode,TrxPrinted,GLID,[Status],IsLocalCurrency,OperatorID,SupervisorID,
	 AdditionalData,IsMainTrx,DocType,GLVoucherID,Remarks,MerchantType,STAN,SettlmntAmount,ConvRate,AcqCountryCode,CurrCodeTran,CurrCodeSett,ATMStatus,POSEntryMode,
	 POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,CardAccptID,CardAccptNameLoc,VISATrxID,ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,
	 ForwardInstID,ClearingRefNo,AppWHTax,WHTaxAmount,WHTScrollNo, VATID,[Percent],TClientId,TAccountid,TProductId,VatReferenceID
    )      
    VALUES       
    (      
     @mOurBranchID,@mScrollNo,@SerialNo,'R'+@mRefNo,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@AccountType,@DocumentType,@AccountID,@AccountName,@ProductID,
	 @CurrencyID,@mwDate,@TrxType,'',@mwDate,@Amount,@ForeignAmount,@ExchangeRate,@ProfitLoss,@MeanRate,@DescriptionID,'REV '+@Description,@BankCode,@BranchCode,@TrxPrinted,
	 @GLID,'C',@IsLocalCurrency,@OperatorID,@SupervisorID,null,@IsMainTrx,@DocType,@VoucherID,@Remarks,@MerchantType,@STAN,@SettlmntAmount,@ConvRate,@AcqCountryCode,@CurrCodeTran,
	 @CurrCodeSett,'C',@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AcqInstID,@RetRefNum,@AuthIDResp,@CardAccptID,@CardAccptNameLoc,@VISATrxID,@ChannelId,@ChannelRefID,
	 @ProcCode,@CAVV,@ResponseCode,@ForwardInstID,@RefNo,@AppWHTax,@WHTaxAmount,@WHTScrollNo, @VATID,@VATPercent,@TClientId,@TAccountid,@TProductId,@VatReferenceID
    )
   
   END
   
   
   SELECT @vClearBalance=ISNULL(B.ClearBalance,0),@vClearedEffects=ISNULL(B.Effects,0),@vFreezeAmount=ISNULL(B.FreezeAmount,0),@Limit=ISNULL(B.Limit,0),
   @vMinBalance=ISNULL(P.MinBalance,0),@vIsClose=ISNULL(A.Status,''),@CurrencyCode = ISNULL(c.CurrencyCode,''),@value=ISNULL(p.productatmcode,'00'), 
   @MobileNo=ISNULL(cust.MobileNo,''),@Email=ISNULL(cust.Email,''),@ClientID=cust.ClientID
   FROM t_AccountBalance B   
   INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND  B.ProductID = A.ProductID   
   INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND  A.ProductID = P.ProductID   
   INNER JOIN t_Currencies C ON A.OurBranchID = C.OurBranchID AND A.CurrencyID = C.CurrencyID  
   INNER JOIN t_Customer cust ON A.OurBranchID = cust.OurBranchID AND A.ClientID = cust.ClientID
   WHERE (A.OurBranchID = @OurBranchID) AND (A.AccountID = @mAccountID)  
      
   IF @Limit > 0 AND @vCheckLimit = 1 AND @vODLimitAllow = 1  
   BEGIN  
    SET @vClearBalance=(@vClearBalance+@Limit)  
   END  
   
   SET @sCount = 0    
	 
   SELECT @GLTrxCount=count(AccountID) FROM t_GLTransactions
   WHERE (OurBranchID=@OurBranchID) and (RefNo IN (@RefNo,@RefNo+'|CHGS1',@RefNo+'|CHGS2')) 
   AND VoucherID = @GLVoucherID AND convert(varchar(10), Date, 120) <= CONVERT(varchar(10), @mwDate, 120)
    
   WHILE (@sCount <> @GLTrxCount)
   BEGIN
   
	SET @sCount = @sCount + 1    
	
	SELECT @mOurBranchID=OurBranchID,@mRefNo=RefNo,@AccountID=AccountID,@SerialID=SerialID,@DescriptionID=DescriptionID,@Description=[Description],@CurrencyID=CurrencyID,
	@Amount=Amount,@ForeignAmount=ForeignAmount,@ExchangeRate=ExchangeRate,@IsCredit=IsCredit,@TransactionType=TransactionType,@TransactionMethod=TransactionMethod,
	@OperatorID=OperatorID,@SupervisorID=SupervisorID,@Indicator=Indicator,@ScrollNo=ScrollNo,@SerialNo=SerialNo,@IsMainTrx=IsMainTrx,@DocType=DocType,@Remarks=Remarks,
	@MerchantType=MerchantType,@STAN=STAN,@SettlmntAmount=SettlmntAmount,@ConvRate=ConvRate,@AcqCountryCode=AcqCountryCode,@CurrCodeTran=CurrCodeTran,@CurrCodeSett=CurrCodeSett,
	@POSEntryMode=POSEntryMode,@POSConditionCode=POSConditionCode,@POSPINCaptCode=POSPINCaptCode,@AcqInstID=AcqInstID,@RetRefNum=RetRefNum,@AuthIDResp=ISNULL(AuthIDResp,'0'),
	@CardAccptID=CardAccptID,@CardAccptNameLoc=CardAccptNameLoc,@VISATrxID=VISATrxID,@ChannelId=ChannelId,@ChannelRefID=ChannelRefID,@ProcCode=ProcCode,@CAVV=CAVV,
	@ResponseCode=ResponseCode,@ForwardInstID=ForwardInstID, @VATID=VATID,@VATPercent=[Percent],@TClientId=TClientId,@TAccountid=TAccountid,@TProductId=TProductId,
	@VatReferenceID=VatReferenceID
    FROM t_GLTransactions
    WHERE (OurBranchID=@OurBranchID) and (RefNo IN (@RefNo,@RefNo+'|CHGS1',@RefNo+'|CHGS2')) 
	AND VoucherID = @GLVoucherID AND convert(varchar(10), Date, 120) <= CONVERT(varchar(10), @mwDate, 120)
	AND SerialID = @sCount
	
	if (@IsCredit = 1)
	BEGIN
	 SET @IsCredit = 0
	END
	ELSE
	BEGIN
	 SET @IsCredit = 1
	END
	
	INSERT INTO t_GLTransactions
    (      
     OurBranchID,RefNo,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,ExchangeRate,IsCredit,TransactionType,TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,IsMainTrx,DocType,Remarks,MerchantType,STAN,SettlmntAmount,ConvRate,AcqCountryCode,CurrCodeTran,CurrCodeSett,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,CardAccptID,CardAccptNameLoc,VISATrxID,ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,ForwardInstID, VATID,[Percent],TClientId,TAccountid,TProductId,VatReferenceID
    )      
    VALUES      
    (      
     @mOurBranchID,'R'+@mRefNo,@AccountID,@VoucherID,@SerialID,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@mwDate,@DescriptionID,'REV '+@Description,@CurrencyID,@Amount,@ForeignAmount,@ExchangeRate,@IsCredit,@TransactionType,@TransactionMethod,@OperatorID,@SupervisorID,@Indicator,@mScrollNo,@sCount,null,@IsMainTrx,@DocType,@Remarks,
	 @MerchantType,@STAN,@SettlmntAmount,@ConvRate,@AcqCountryCode,@CurrCodeTran,@CurrCodeSett,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AcqInstID,@RetRefNum,@AuthIDResp,
	 @CardAccptID,@CardAccptNameLoc,@VISATrxID,@ChannelId,@ChannelRefID,@ProcCode,@CAVV,@ResponseCode,@ForwardInstID, @VATID,@VATPercent,@TClientId,@TAccountid,@TProductId,@VatReferenceID
    )
   END
   
   Insert Into BalChk   
   (  
    OurBranchID,AccountID,ProductID,[Name],LastUpdateTime,ClearBalance,Limit,Effects,Shadow,FreezeAmt,MinBalance,TotalBalance,AvailableBalance  
   )  
   SELECT OurBranchID,AccountID,ProductID,[Name],@mwDate + ' ' + convert(varchar(12), GetDate(), 114),ISNULL(ClearBalance,0),ISNULL(Limit,0),ISNULL(Effects,0),
   ISNULL(ShadowBalance,0),ISNULL(FreezeAmount,0), @vMinBalance,   
   (isnull(@Limit,0)+ISNULL(ClearBalance,0)-ISNULL(Effects,0)),  
   (isnull(@Limit,0)+ISNULL(ClearBalance,0)-ISNULL(FreezeAmount,0)-@vMinBalance)  
   FROM t_AccountBalance WHERE OurBranchID = @OurBranchID AND AccountID = @mAccountID  
     
   /* Get Latest Balance again.. */  
     
   SELECT @vClearBalance=ISNULL(B.ClearBalance,0),@vClearedEffects=ISNULL(B.Effects,0),@vFreezeAmount=ISNULL(B.FreezeAmount,0),@Limit=ISNULL(B.Limit,0),
   @vMinBalance=ISNULL(P.MinBalance,0),@vIsClose=ISNULL(A.Status,''),@CurrencyCode = ISNULL(c.CurrencyCode,''),@value=ISNULL(p.productatmcode,'00'),
   @MobileNo=ISNULL(cust.MobileNo,''),@Email=ISNULL(cust.Email,''),@ClientID=cust.ClientID
   FROM t_AccountBalance B   
   INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND  B.ProductID = A.ProductID   
   INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND  A.ProductID = P.ProductID   
   INNER JOIN t_Currencies C ON A.OurBranchID = C.OurBranchID AND A.CurrencyID = C.CurrencyID  
   INNER JOIN t_Customer cust ON A.OurBranchID = cust.OurBranchID AND A.ClientID = cust.ClientID
   WHERE (A.OurBranchID = @OurBranchID) AND (A.AccountID = @mAccountID)  
     
   IF @Limit > 0 AND @vCheckLimit = 1 AND @vODLimitAllow = 1  
   BEGIN  
    SET @vClearBalance=(@vClearBalance+@Limit)  
   END  
  
   SELECT @Status = 0,     
   @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,  
   @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100  
     
   SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value, @CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp, 
   @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
   RETURN (0)  
   
  END
 END