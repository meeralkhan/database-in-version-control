﻿CREATE   PROCEDURE [dbo].[spc_GetDebitBalanceValue_All]
AS
SET NOCOUNT ON

BEGIN
   
   select p.OurBranchID, p.ProductID, p.GLControl, p.GLDebitBalance, c.MeanRate, a.CurrencyID, c.CRRounding AS Rounding,
   a.OpeningBalance + a.Balance AS LocalControlBalance, a.ForeignOpeningBalance + a.ForeignBalance as ForeignControlBalance,
   b.OpeningBalance + b.Balance AS LocalDebitBalance, b.ForeignOpeningBalance + b.ForeignBalance as ForeignDebitBalance
   from t_Products p
   inner join t_Currencies c on p.OurBranchID = c.OurBranchID and p.CurrencyID = c.CurrencyID
   INNER JOIN t_GL a ON p.OurBranchID = a.OurBranchID AND p.GLControl = a.AccountID
   INNER JOIN t_GL b ON p.OurBranchID = b.OurBranchID AND p.GLDebitBalance = b.AccountID
   
END