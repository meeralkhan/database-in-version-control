﻿
CREATE   PROCEDURE [dbo].[sp_Transfer]       
(      
 @OurBrID VARCHAR(30),      
 @fdate Varchar(50),      
 @tdate Varchar(50),      
 @minscroll VARCHAR(30),      
 @maxscroll VARCHAR(30),      
 @rtpname VARCHAR(30)      
)      
      
AS      
BEGIN      
      
 DECLARE @sDate datetime        
 DECLARE @eDate datetime      
 DECLARE @WorkingDate datetime      
 DECLARE @str nVarchar(MAX)      
 DECLARE @flag Varchar(10)      
      
 SET @sDate = CAST(@fdate as date)      
 SET @eDate = CAST(@tdate as date)      
 SET @WorkingDate = (select CAST(WORKINGDATE as date) from t_Last where OurBranchID = @OurBrID)      
      
 IF (@sDate = @eDate AND @sDate = @WorkingDate)      
 BEGIN       
       
  SET @str = 'select ValueDate,OurBranchID, ProductID,AccountID,AccountName,ForeignAmount,TrxType,      
   Description,AdditionalData,ChequeID,ScrollNo,      
   Amount,wDate,OperatorID,CurrencyID, SupervisorID,      
   case trxType when ''D'' then Amount  else 0 end as debitTrxType,      
   case trxType when ''C'' then Amount  else 0 end as CreditTrxType, CurrencyID,      
   case trxType when ''D'' then 1 else 0 end as debitCount,case trxType when ''C'' then 1  else 0 end as CreditCount,       
   CASE Supervision      
    WHEN ''C'' THEN ''Cleared''      
    WHEN ''R'' THEN ''Rejected''      
    WHEN ''*'' THEN ''Under Supervision''      
    ELSE ''UnChecked''      
   END AS Status       
   from t_TransferTransactionModel      
   WHERE ScrollNo BETWEEN ' + @minscroll + ' AND ' + @maxscroll;      
      
   if(@rtpname <> 'cjv')      
   BEGIN      
    SET @str = CONCAT(@str , ' AND OurBranchId = ' + @OurBrID);      
   END     
    
   if(@rtpname = 'jv')      
   BEGIN      
    SET @str = CONCAT(@str , ' AND OperatorID IN (select OperatorID from t_Map) ');      
   END      
   Else if(@rtpname = 'rejected')      
   BEGIN      
    SET @str = CONCAT(@str , ' AND Supervision = ''R'' ');      
   END      
   Else if(@rtpname = 'TransactionPosted')      
   BEGIN      
    SET @str = CONCAT(@str , ' AND Supervision = ''C'' ');      
   END      
 END      
      
       
 ELSE IF (@sDate < @eDate AND @eDate = @WorkingDate)      
 BEGIN       
        
  if object_id('dbo.t_Transfer_' + FORMAT (@sDate, 'ddMMyyyy')) IS NOT NULL      
  BEGIN      
   SET @str = CONCAT(' select ValueDate,OurBranchID, ProductID,AccountID,AccountName,ForeignAmount,TrxType,      
   Description,AdditionalData,ChequeID,ScrollNo,      
   Amount,wDate,OperatorID,CurrencyID, SupervisorID,      
   case trxType when ''D'' then Amount  else 0 end as debitTrxType,      
   case trxType when ''C'' then Amount  else 0 end as CreditTrxType, CurrencyID,      
   case trxType when ''D'' then 1 else 0 end as debitCount,case trxType when ''C'' then 1  else 0 end as CreditCount,       
   CASE Supervision      
    WHEN ''C'' THEN ''Cleared''      
    WHEN ''R'' THEN ''Rejected''      
    WHEN ''*'' THEN ''Under Supervision''      
    ELSE ''UnChecked''      
   END AS Status        
   from t_Transfer_', FORMAT (@sDate, 'ddMMyyyy') , '     
   WHERE ScrollNo BETWEEN ' + @minscroll + ' AND ' + @maxscroll);      
      
   if(@rtpname <> 'cjv')      
   BEGIN      
    SET @str = CONCAT(@str , ' AND OurBranchId = ' + @OurBrID);      
   END     
    
   if(@rtpname = 'jv')      
   BEGIN      
    SET @str = CONCAT(@str , ' AND OperatorID IN (select OperatorID from t_Map) ');      
   END      
   Else if(@rtpname = 'rejected')      
   BEGIN      
    SET @str = CONCAT(@str , ' AND Supervision = ''R'' ');      
   END      
   Else if(@rtpname = 'TransactionPosted')      
   BEGIN      
    SET @str = CONCAT(@str , ' AND Supervision = ''C'' ');      
   END      
      
   SET @flag = 'TRUE'      
  END      
  ELSE      
  BEGIN       
   SET @flag = 'FALSE'      
  END      
      
  SET @sDate = CAST(DATEADD(DAY,1,@sDate) as date);      
      
  WHILE(@sDate < @eDate)      
  BEGIN      
   if object_id('dbo.t_Transfer_' + FORMAT (@sDate, 'ddMMyyyy')) IS NOT NULL      
   BEGIN      
    if (@flag = 'TRUE')      
    BEGIN      
     SET @str = CONCAT(@str , ' UNION ALL select ValueDate,OurBranchID, ProductID,AccountID,AccountName,ForeignAmount,TrxType,      
     Description,AdditionalData,ChequeID,ScrollNo,      
     Amount,wDate,OperatorID,CurrencyID, SupervisorID,      
     case trxType when ''D'' then Amount  else 0 end as debitTrxType,      
     case trxType when ''C'' then Amount  else 0 end as CreditTrxType, CurrencyID,      
     case trxType when ''D'' then 1 else 0 end as debitCount,case trxType when ''C'' then 1  else 0 end as CreditCount,       
     CASE Supervision      
      WHEN ''C'' THEN ''Cleared''      
      WHEN ''R'' THEN ''Rejected''      
      WHEN ''*'' THEN ''Under Supervision''      
      ELSE ''UnChecked''      
     END AS Status         
     from t_Transfer_' + FORMAT (@sDate, 'ddMMyyyy') + '        
    WHERE ScrollNo BETWEEN ' + @minscroll + ' AND ' + @maxscroll);      
      
    if(@rtpname <> 'cjv')      
    BEGIN      
  SET @str = CONCAT(@str , ' AND OurBranchId = ' + @OurBrID);      
    END     
       
     if(@rtpname = 'jv')      
     BEGIN      
      SET @str = CONCAT(@str , ' AND OperatorID IN (select OperatorID from t_Map) ');      
     END      
     Else if(@rtpname = 'rejected')      
     BEGIN      
      SET @str = CONCAT(@str , ' AND Supervision = ''R'' ');      
     END      
   Else if(@rtpname = 'TransactionPosted')      
   BEGIN      
    SET @str = CONCAT(@str , ' AND Supervision = ''C'' ');      
   END      
      
    END      
    ELSE       
    BEGIN      
     SET @str = CONCAT(@str , ' select ValueDate,OurBranchID, ProductID,AccountID,AccountName,ForeignAmount,TrxType,      
     Description,AdditionalData,ChequeID,ScrollNo,      
     Amount,wDate,OperatorID,CurrencyID, SupervisorID,      
     case trxType when ''D'' then Amount  else 0 end as debitTrxType,      
     case trxType when ''C'' then Amount  else 0 end as CreditTrxType, CurrencyID,      
     case trxType when ''D'' then 1 else 0 end as debitCount,case trxType when ''C'' then 1  else 0 end as CreditCount,       
     CASE Supervision      
      WHEN ''C'' THEN ''Cleared''      
      WHEN ''R'' THEN ''Rejected''      
      WHEN ''*'' THEN ''Under Supervision''      
      ELSE ''UnChecked''      
     END AS Status          
     from t_Transfer_' + FORMAT (@sDate, 'ddMMyyyy') + '        
   WHERE ScrollNo BETWEEN ' + @minscroll + ' AND ' + @maxscroll);      
      
   if(@rtpname <> 'cjv')      
   BEGIN      
    SET @str = CONCAT(@str , ' AND OurBranchId = ' + @OurBrID);      
   END     
     
           
     if(@rtpname = 'jv')      
     BEGIN      
      SET @str = CONCAT(@str , ' AND OperatorID IN (select OperatorID from t_Map) ');      
     END      
     Else if(@rtpname = 'rejected')      
     BEGIN      
      SET @str = CONCAT(@str , ' AND Supervision = ''R'' ');      
     END      
   Else if(@rtpname = 'TransactionPosted')      
   BEGIN      
    SET @str = CONCAT(@str , ' AND Supervision = ''C'' ');      
   END      
      
     SET @flag = 'TRUE'      
    END      
   END      
   SET @sDate = CAST(DATEADD(DAY,1,@sDate) as date);      
  END      
         
        
  if (@flag = 'TRUE')      
  BEGIN      
   SET @str = CONCAT(@str , ' UNION ALL select ValueDate,OurBranchID, ProductID,AccountID,AccountName,ForeignAmount,TrxType,      
     Description,AdditionalData,ChequeID,ScrollNo,      
     Amount,wDate,OperatorID,CurrencyID, SupervisorID,      
     case trxType when ''D'' then Amount  else 0 end as debitTrxType,      
     case trxType when ''C'' then Amount  else 0 end as CreditTrxType, CurrencyID,      
     case trxType when ''D'' then 1 else 0 end as debitCount,case trxType when ''C'' then 1  else 0 end as CreditCount,       
     CASE Supervision      
      WHEN ''C'' THEN ''Cleared''      
      WHEN ''R'' THEN ''Rejected''      
      WHEN ''*'' THEN ''Under Supervision''      
      ELSE ''UnChecked''      
     END AS Status          
     from t_TransferTransactionModel       
   WHERE ScrollNo BETWEEN ' + @minscroll + ' AND ' + @maxscroll);      
      
   if(@rtpname <> 'cjv')         BEGIN      
    SET @str = CONCAT(@str , ' AND OurBranchId = ' + @OurBrID);      
   END     
    
   if(@rtpname = 'jv')      
   BEGIN      
    SET @str = CONCAT(@str , ' AND OperatorID IN (select OperatorID from t_Map) ');      
   END      
   Else if(@rtpname = 'rejected')      
   BEGIN      
    SET @str = CONCAT(@str , ' AND Supervision = ''R'' ');      
   END      
   Else if(@rtpname = 'TransactionPosted')      
   BEGIN      
    SET @str = CONCAT(@str , ' AND Supervision = ''C'' ');      
  END      
  END      
  ELSE       
  BEGIN      
      
   SET @str = CONCAT(@str , ' select ValueDate,OurBranchID, ProductID,AccountID,AccountName,ForeignAmount,TrxType,      
      Description,AdditionalData,ChequeID,ScrollNo,      
      Amount,wDate,OperatorID,CurrencyID, SupervisorID,      
      case trxType when ''D'' then Amount  else 0 end as debitTrxType,      
      case trxType when ''C'' then Amount  else 0 end as CreditTrxType, CurrencyID,      
      case trxType when ''D'' then 1 else 0 end as debitCount,case trxType when ''C'' then 1  else 0 end as CreditCount,       
      CASE Supervision      
       WHEN ''C'' THEN ''Cleared''      
       WHEN ''R'' THEN ''Rejected''      
       WHEN ''*'' THEN ''Under Supervision''      
       ELSE ''UnChecked''      
      END AS Status           
      from t_TransferTransactionModel       
   WHERE ScrollNo BETWEEN ' + @minscroll + ' AND ' + @maxscroll);      
      
   if(@rtpname <> 'cjv')      
   BEGIN      
    SET @str = CONCAT(@str , ' AND OurBranchId = ' + @OurBrID);      
   END     
    
   if(@rtpname = 'jv')      
   BEGIN      
    SET @str = CONCAT(@str , ' AND OperatorID IN (select OperatorID from t_Map) ');      
   END      
   Else if(@rtpname = 'rejected')      
   BEGIN      
    SET @str = CONCAT(@str , ' AND Supervision = ''R'' ');      
   END      
   Else if(@rtpname = 'TransactionPosted')      
   BEGIN      
    SET @str = CONCAT(@str , ' AND Supervision = ''C'' ');      
   END      
      
  END      
 END      
      
      
 ELSE       
 BEGIN      
  if object_id('dbo.t_Transfer_' + FORMAT (@sDate, 'ddMMyyyy')) IS NOT NULL      
  BEGIN      
   SET @str = CONCAT(' select ValueDate,OurBranchID, ProductID,AccountID,AccountName,ForeignAmount,TrxType,      
      Description,AdditionalData,ChequeID,ScrollNo,      
      Amount,wDate,OperatorID,CurrencyID, SupervisorID,      
      case trxType when ''D'' then Amount  else 0 end as debitTrxType,      
      case trxType when ''C'' then Amount  else 0 end as CreditTrxType, CurrencyID,      
      case trxType when ''D'' then 1 else 0 end as debitCount,case trxType when ''C'' then 1  else 0 end as CreditCount,       
      CASE Supervision      
       WHEN ''C'' THEN ''Cleared''      
       WHEN ''R'' THEN ''Rejected''      
       WHEN ''*'' THEN ''Under Supervision''      
       ELSE ''UnChecked''      
      END AS Status            
      from t_Transfer_', FORMAT (@sDate, 'ddMMyyyy') , '        
   WHERE ScrollNo BETWEEN ' + @minscroll + ' AND ' + @maxscroll);      
      
   if(@rtpname <> 'cjv')      
   BEGIN      
    SET @str = CONCAT(@str , ' AND OurBranchId = ' + @OurBrID);      
   END     
    
   if(@rtpname = 'jv')      
   BEGIN      
    SET @str = CONCAT(@str , ' AND OperatorID IN (select OperatorID from t_Map) ');      
   END      
   Else if(@rtpname = 'rejected')      
   BEGIN      
    SET @str = CONCAT(@str , ' AND Supervision = ''R'' ');      
   END      
   Else if(@rtpname = 'TransactionPosted')      
   BEGIN      
    SET @str = CONCAT(@str , ' AND Supervision = ''C'' ');      
   END      
      
   SET @flag = 'TRUE'      
  END      
  ELSE      
  BEGIN       
   SET @flag = 'FALSE'      
  END      
      
       
  WHILE(@sDate < @eDate)      
  BEGIN      
   SET @sDate = CAST(DATEADD(DAY,1,@sDate) as date);      
   if object_id('dbo.t_Transfer_' + FORMAT (@sDate, 'ddMMyyyy')) IS NOT NULL      
   BEGIN      
    if (@flag = 'TRUE')      
    BEGIN      
     SET @str = CONCAT(@str , ' UNION ALL select ValueDate,OurBranchID, ProductID,AccountID,AccountName,ForeignAmount,TrxType,      
      Description,AdditionalData,ChequeID,ScrollNo,      
      Amount,wDate,OperatorID,CurrencyID, SupervisorID,      
      case trxType when ''D'' then Amount  else 0 end as debitTrxType,      
      case trxType when ''C'' then Amount  else 0 end as CreditTrxType, CurrencyID,      
      case trxType when ''D'' then 1 else 0 end as debitCount,case trxType when ''C'' then 1  else 0 end as CreditCount,       
      CASE Supervision      
       WHEN ''C'' THEN ''Cleared''      
       WHEN ''R'' THEN ''Rejected''      
       WHEN ''*'' THEN ''Under Supervision''      
       ELSE ''UnChecked''      
      END AS Status             
      from t_Transfer_' + FORMAT (@sDate, 'ddMMyyyy') + '        
   WHERE ScrollNo BETWEEN ' + @minscroll + ' AND ' + @maxscroll);      
      
   if(@rtpname <> 'cjv')      
   BEGIN      
    SET @str = CONCAT(@str , ' AND OurBranchId = ' + @OurBrID);      
   END     
    
    if(@rtpname = 'jv')      
    BEGIN      
     SET @str = CONCAT(@str , ' AND OperatorID IN (select OperatorID from t_Map) ');      
    END      
    Else if(@rtpname = 'rejected')      
    BEGIN      
     SET @str = CONCAT(@str , ' AND Supervision = ''R'' ');      
    END      
   Else if(@rtpname = 'TransactionPosted')      
   BEGIN      
    SET @str = CONCAT(@str , ' AND Supervision = ''C'' ');      
   END      
      
    END      
    ELSE       
    BEGIN      
     SET @str = CONCAT(@str , ' select ValueDate,OurBranchID, ProductID,AccountID,AccountName,ForeignAmount,TrxType,      
      Description,AdditionalData,ChequeID,ScrollNo,      
      Amount,wDate,OperatorID,CurrencyID, SupervisorID,      
      case trxType when ''D'' then Amount  else 0 end as debitTrxType,      
      case trxType when ''C'' then Amount  else 0 end as CreditTrxType, CurrencyID,      
      case trxType when ''D'' then 1 else 0 end as debitCount,case trxType when ''C'' then 1  else 0 end as CreditCount,       
      CASE Supervision      
       WHEN ''C'' THEN ''Cleared''      
       WHEN ''R'' THEN ''Rejected''      
       WHEN ''*'' THEN ''Under Supervision''      
       ELSE ''UnChecked''      
      END AS Status              
      from t_Transfer_' , FORMAT (@sDate, 'ddMMyyyy') + '        
   WHERE ScrollNo BETWEEN ' + @minscroll + ' AND ' + @maxscroll);      
      
   if(@rtpname <> 'cjv')      
   BEGIN      
    SET @str = CONCAT(@str , ' AND OurBranchId = ' + @OurBrID);      
   END     
    
     if(@rtpname = 'jv')      
     BEGIN      
      SET @str = CONCAT(@str , ' AND OperatorID IN (select OperatorID from t_Map) ');      
     END      
     Else if(@rtpname = 'rejected')      
     BEGIN      
      SET @str = CONCAT(@str , ' AND Supervision = ''R'' ');      
     END      
   Else if(@rtpname = 'TransactionPosted')      
   BEGIN      
    SET @str = CONCAT(@str , ' AND Supervision = ''C'' ');      
   END      
      
     SET @flag = 'TRUE'      
    END      
   END      
      
  END      
 END      
      
      
  EXEC  ( @str )      
  print ( @str )      
  select @str       
END