﻿CREATE Procedure [dbo].[spc_OL_GetGLBalanceHistory]
(  
  @OurBranchID  varchar(30),  
  @AccountID  varchar(30),  
  @WorkingDate  datetime  
)  
  
As  
  
Declare @OpeningBalance   As money  
Declare @ForeignOpeningBalance   As money  
  
Declare @TotalDebit    As money  
Declare @TotalDebitForeign   As money  
  
Declare @TotalCredit    As money  
Declare @TotalCreditForeign   As money  
  
  
  
Select @ForeignOpeningBalance = IsNull(GL.ForeignOpeningBalance,0),    
       @OpeningBalance  = IsNull(GL.OpeningBalance,0),   
       @TotalDebit =  (Select IsNull(Sum(t_GLTransactions.Amount),0)  
          From t_GLTransactions   
         Where t_GLTransactions.OurBranchID = GL.OurBranchID  
           And t_GLTransactions.AccountID   = GL.AccountID  
           And t_GLTransactions.Date        < @WorkingDate  
           And t_GLTransactions.IsCredit    = 0) ,   
  
       @TotalDebitForeign = (Select IsNull(Sum(t_GLTransactions.ForeignAmount),0)    
          From t_GLTransactions   
         Where t_GLTransactions.OurBranchID = GL.OurBranchID  
           And t_GLTransactions.AccountID   = GL.AccountID  
           And t_GLTransactions.Date        < @WorkingDate  
           And t_GLTransactions.IsCredit    = 0) ,   
  
 @TotalCredit = (Select IsNull(Sum(t_GLTransactions.Amount),0)   
    From t_GLTransactions   
   Where t_GLTransactions.OurBranchID = GL.OurBranchID  
     And t_GLTransactions.AccountID   = GL.AccountID  
     And t_GLTransactions.Date        < @WorkingDate  
     And t_GLTransactions.IsCredit    = 1) ,  
  
 @TotalCreditForeign = (Select IsNull(Sum(t_GLTransactions.ForeignAmount),0)  
    From t_GLTransactions   
   Where t_GLTransactions.OurBranchID = GL.OurBranchID  
     And t_GLTransactions.AccountID   = GL.AccountID  
     And t_GLTransactions.Date        < @WorkingDate  
     And t_GLTransactions.IsCredit    = 1)   
  
  From t_GL As GL  
 Where GL.OurBranchID = @OurBranchID  
   And GL.AccountID   = @AccountID  

Select (@OpeningBalance - @TotalDebit + @TotalCredit) As OpeningBalance ,   
       (@ForeignOpeningBalance - @TotalDebitForeign + @TotalCreditForeign) As OpeningBalanceForeign