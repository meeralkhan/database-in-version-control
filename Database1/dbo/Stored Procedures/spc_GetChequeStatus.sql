﻿CREATE PROCEDURE [dbo].[spc_GetChequeStatus]        
(         
 @OurBranchID   varchar(30),
 @AccountID   varchar(30),        
 @ChequeID   varchar(11)=''        
)        
        
AS        
 DECLARE @Status   as int         
        
 SET NOCOUNT ON        
        
 SELECT @Status = -1        
        
 -- Get Validate Cheaque No        
 EXEC @Status = spc_ValidateChequeNo @OurBranchID, @AccountID , @ChequeID, @ChequeID      
        
 IF @Status <> 0        
  BEGIN              
   SELECT @Status as Status        
   SET NOCOUNT OFF        
   RETURN(1)        
  END        
         
 if exists ( select AccountID from t_Cheque where OurBranchID = @OurBranchID AND AccountID = @AccountID    
               AND (CAST(@ChequeID AS money) BETWEEN CAST(ChequeStart as money) AND CAST(ChequeEnd as money))    
               AND (CAST(@ChequeID AS money) BETWEEN CAST(ChequeStart as money) AND (CAST (ChequeEnd as money)))     
               AND ISNULL(AuthStatus,'') != ''    
             )    
 BEGIN    
   SELECT '6' as Status        
   SET NOCOUNT OFF        
   RETURN(1)        
 END    
     
 SELECT @Status As Status        
 SET NOCOUNT OFF        
 RETURN (0)