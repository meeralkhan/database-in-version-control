﻿create procedure [dbo].[spc_GetNostroAcc]
(
   @OurBranchID nvarchar(30),
   @BankID nvarchar(30)
)
AS
select s.SerialID, s.SettlementBankID AS BankID, b.FullName AS BankName, b.CountryID, c.Name AS CountryName, b.SWIFTCode, b.SORTCode,
br.BranchID, br.Name AS BranchName, br.Address, br.City, s.IBAN, s.AccountNo, s.AccountTitle, s.CurrencyID, case s.CurrencyID
when (select LocalCurrency from t_GlobalVariables where OurBranchID = s.OurBranchID)
then (select Balance from t_GL where OurBranchID = s.OurBranchID and AccountID = s.SettlementAccountID)
else (select ForeignBalance from t_GL where OurBranchID = s.OurBranchID and AccountID = s.SettlementAccountID)
end AS AvailableBalance, s.MinimumBalance, s.CreditLine, 'NOSTRO' AS AccountType
from t_BankSettlementAcc s
inner join t_Banks b on s.OurBranchID = b.OurBranchID and s.SettlementBankID = b.BankID
left join t_Branches br on s.OurBranchID = br.OurBranchID and s.SettlementBankID = br.BankID and s.SettlementBranchID = br.BranchID
inner join t_Country c on b.OurBranchID = c.OurBranchID and b.CountryID = c.CountryID
where s.OurBranchID = @OurBranchID and s.BankID = @BankID