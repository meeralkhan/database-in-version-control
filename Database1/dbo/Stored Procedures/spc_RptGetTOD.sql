﻿CREATE procedure [dbo].[spc_RptGetTOD]    
       
 @OurBranchID varchar(30),      
 @ProductID varchar(30)=''      
AS      
  SET NOCOUNT ON    
 if @ProductID <> ''      
      
  select * from vc_RptGetTOD where OurBranchID=@OurBranchID and productid=@productid 
  and accountid not in (    
 select accountid from t_Adv_Account_LimitMaintain     
 where OurBranchID=@OurBranchID AND ISNULL(IsBlocked, '0') = '0' AND ISNULL(IsCancelled, '0') = '0'    
  )      
      
 else      
  select * from vc_RptGetTOD where OurBranchID=@OurBranchID       
  and accountid not in (    
 select accountid from t_Adv_Account_LimitMaintain     
 where OurBranchID=@OurBranchID AND ISNULL(IsBlocked, '0') = '0' AND ISNULL(IsCancelled, '0') = '0'    
  )