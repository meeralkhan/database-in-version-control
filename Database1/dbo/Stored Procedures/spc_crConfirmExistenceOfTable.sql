﻿CREATE Procedure [dbo].[spc_crConfirmExistenceOfTable]
(
  @NameOfTable VARCHAR(50)
)

As

SET NOCOUNT ON
begin
if Exists (SELECT name FROM sysobjects WHERE name = @NameOfTable and xtype = 'u')
   SELECT 'Ok' AS ReturnValue;
else
   SELECT 'NoTableFound' as ReturnValue;
end