﻿CREATE    PROCEDURE [dbo].[spc_GetNarrations]            
 @OurBranchID nvarchar(30),            
 @DescriptionID nvarchar(30)='',            
 @IsCredit bit=0,            
 @Type char(1)='T'          
 AS            
 SET NOCOUNT ON        
 if @DescriptionID <> ''            
  begin            
   if exists (select [Description] from t_TransactionDescriptions            
    Where OurBranchID = @OurBranchID and DescriptionID = @DescriptionID and IsCredit = @IsCredit and [Type] = @Type AND IsNULL(IsEnable,1) = 1)             
            
    select *, 1 as ReturnValue from t_TransactionDescriptions            
    Where (OurBranchID = @OurBranchID)and (DescriptionID = @DescriptionID) and (IsCredit = @IsCredit) and ([Type] = @Type)          
              
   else            
    select 0 as ReturnValue            
  end            
            
 else            
  begin            
   select DescriptionID, [Description] From t_TransactionDescriptions            
   Where (OurBranchID = @OurBranchID)              
  end