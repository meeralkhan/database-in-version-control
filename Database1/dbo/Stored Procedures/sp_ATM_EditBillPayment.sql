﻿
CREATE PROCEDURE [dbo].[sp_ATM_EditBillPayment]
(
	@OurBranchID		     varchar(30),	
	@WithdrawlBranchID	     varchar(30)='',	
	@CustomerBranchID	     varchar(30),
	@CustomerBranchID2	     varchar(30),
	@ATMID		     varchar(30),	
	@AccountID 	   	     varchar(30),
	@AccountID2 	   	     varchar(30),
	@Amount 		     money,
	@USDAmount		     money=0,
	@OtherCurrencyAmount	     money=0,
	@USDRate	    	     money=0,
	@Supervision		     char(1),	
	@RefNo 		     varchar(50),
	@PHXDate 		     datetime,
	@MerchantType	     varchar(30),
	@OurIMD		     varchar(30),
	@AckInstIDCode	     varchar(30),
	@Currency		     varchar(30),
	@NameLocation	     varchar(100),
	@MCC			     varchar(30),
	@CompanyNumber varchar(30) ,
	@CreditCardNumber varchar(50)='',
	@NewRecord		     bit=1
)

AS
	DECLARE @BankShortName as varchar(30)

	DECLARE @mScrollNo as int
	DECLARE @mSerialNo as smallint
	DECLARE @mAccountID as varchar(30)
	DECLARE @mAccountName as varchar(100)
	DECLARE @mAccountType as char
	DECLARE @mProductID as varchar(30)
	DECLARE @mCurrencyID as varchar(30)
	DECLARE @mIsLocalCurrency as bit
		SET @mIsLocalCurrency=1	
	DECLARE @mwDate as datetime
	DECLARE @mTrxType as char
	DECLARE @mGLID as varchar(30)
		SET @mGLID = ''	
	DECLARE @mForeignAmount as money
		SET @mForeignAmount=0	
	DECLARE @mExchangeRate as money
		SET @mExchangeRate=1	
	DECLARE @mDescriptionID as varchar(30)
		SET @mDescriptionID='000'	
	DECLARE @mDescription as varchar(255)
		BEGIN 
			SET @BankShortName = 'N/A'
			SELECT @BankShortName = BankShortName From t_ATM_Banks
			WHERE (BankIMD = @OurIMD)
			
			IF @MerchantType <> '0000' 
				IF @Currency = '586'
					BEGIN

						SET @mDescription='INT.BK. BILL PAID AT BK.' + @BankShortName  + ' (BR.' + @CustomerBranchID --+ ') TO : A/c # ' + @AccountID2 
					END
				ELSE 
					IF @Currency = '840'
						BEGIN
							SET @mDescription='INT.BK. BILL PAID ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') AT ' + @NameLocation 
						END
					ELSE 
						BEGIN
							SET @mDescription='INT.BK. BILL PAID(' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) AT ' + @NameLocation 
						END
			ELSE 
				SET @mDescription='IB(' + @ATMID + ') BILL PAID AT BK.' + @BankShortName  + ' (BR.' + @WithdrawlBranchID
				
--14-05-2004 Local Branch Changes				SET @mDescription='ATM(' + @ATMID + ') IB-CSH WDRL AT BK.' + @BankShortName  + ' (BR.' + @WithdrawlBranchID + ')' 
		END 
	DECLARE @mDescriptionH as varchar(255)
		BEGIN 
			IF @MerchantType <> '0000' 
				IF @Currency = '586'
					BEGIN
						SET @mDescriptionH='INT.BK. BILL PAID AT BK.' + @BankShortName   + ' (BR.' + @CustomerBranchID + ') From : A/c # ' + @AccountID
					END
				ELSE 

					IF @Currency = '840'
						BEGIN
							SET @mDescriptionH='INT.BK. BILL PAID ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') AT ' +  + @NameLocation + ' From : A/c # ' + @AccountID
						END
					ELSE 
						BEGIN
							SET @mDescriptionH='INT.BK. BILL PAID (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) AT ' +  + @NameLocation + ' From : A/c # ' + @AccountID
						END
			ELSE 
				SET @mDescriptionH='ATM (' + @ATMID + ') BILL PAID AT BK.' + @BankShortName   + ' (BR.' + @CustomerBranchID + ') From : A/c # ' + @AccountID
		END 
	DECLARE @mRemarks as varchar(200)
	DECLARE @mBankCode as varchar(30)
		SET @mBankCode='1279'	
	DECLARE @mOperatorID as varchar(30)
		SET @mOperatorID = 'ATM-' + @ATMID
	DECLARE @mSupervisorID as varchar(30)
		SET @mSupervisorID = @OurIMD	

--Checking Variables
	DECLARE @vCount1 as int
	DECLARE @vCount2 as bit
	DECLARE @vCount3 as bit
	DECLARE @vCount4 as bit
	DECLARE @vAccountID as varchar(30)
	DECLARE @vClearBalance as money

	DECLARE @vClearBalance2 as money
	DECLARE @vClearedEffects as money
	DECLARE @vFreezeAmount as money
	DECLARE @vMinBalance as money
	DECLARE @limit as money
	DECLARE @vIsClose as char
	DECLARE @vAreCheckBooksAllowed as bit
	DECLARE @vAllowDebitTransaction as bit
	DECLARE @vBranchName as varchar(100)
	DECLARE @Status as int	

	DECLARE @mLastEOD as datetime
--	DECLARE @mWorkingDate as datetime
	DECLARE @mCONFRETI as datetime
	DECLARE @mWithdrawlBranchName as varchar(100)

	DECLARE @WithDrawlCharges as money
	SET @WithDrawlCharges = 0
	DECLARE @WithDrawlChargesPercentage as money
	SET @WithDrawlChargesPercentage = 0
	DECLARE @IssuerAccountID as varchar(30)
	DECLARE @IssuerAccountTitle as varchar(100)
	DECLARE @IssuerChargesAccountID as varchar(30)
	DECLARE @IssuerTitle as varchar(100)
	DECLARE @mRemarksCharges as varchar(200)
	DECLARE @AvailableBalance as money
	DECLARE @value as varchar(1)
	DECLARE @Super as varchar(1)

	SET NOCOUNT ON

	SELECT @Status = -1
---------------------

	BEGIN

			SELECT @mLastEOD=LastEOD,@mwDate=WorkingDate,@mCONFRETI=CONFRETI FROM t_Last WHERE (OurBranchID = @OurBranchID)
			IF @mLastEOD = @mwDate or @mCONFRETI = '1/1/9999'
				BEGIN
					 
					SELECT @Status=9		--INVALID_DATE
					SELECT @Status  as Status
					RETURN (1)	
				END


			SELECT @vCount1=count(*),@Super=Supervision From t_ATM_TransferTransaction
				WHERE (OurBranchID = @OurBranchID) AND ( AccountID=@AccountID) AND
			              (Refno=@Refno) and (AtmID=@ATMID)	 
					group by Supervision
				IF @vCount1=1 and @Super='C'
					BEGIN
						 
						SELECT @Status=10		--DUP_TRAN
						SELECT @Status  as Status
						RETURN (1)	
					END
	
				IF @vCount1=1 and @Super='R'
					BEGIN
						 
						SELECT @Status=35		--ORIG_ALREADY_REVERSED
						SELECT @Status  as Status
						RETURN (1)	
					END

			SELECT @mScrollNo=(ISNULL(MAX(ScrollNo),0)+1) FROM t_ATM_TransferTransaction WHERE (OurBranchID = @OurBranchID) and (wDate = @mwDate)	

------------------------
------------------------
--Debit Transaction			


-- 			SELECT @vCount1=count(*) From t_ATM_UtilityCharges
-- 			WHERE (OurBranchID = @OurBranchID) AND (AccountID in (@AccountID))
-- 			IF  @vCount1=0
-- 				BEGIN
-- 					 
-- 					SELECT @Status=2		--INVALID_ACCOUNT
-- 					SELECT @Status  as Status
-- 					RETURN (1)	
-- 				END

			SELECT @Accountid=t_ATM_UtilityCharges.AcquiringAccountID,
			@mAccountName=B.Name, @mProductID=B.ProductID, @mCurrencyID=P.CurrencyID,@mAccountType='C',@vClearBalance=B.ClearBalance,@vClearedEffects=B.ClearedEffects,@vFreezeAmount=B.FreezeAmount,@vMinBalance=A.MinBalance,@limit=isnull(b.limit,0),@vIsClose=A.Status, @vAreCheckBooksAllowed=P.AreCheckBooksAllowed, @vAllowDebitTransaction=isnull(S.AllowDebitTransaction,0)
			FROM t_AccountBalance B INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND  B.ProductID = A.ProductID INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND  A.ProductID = P.ProductID LEFT OUTER JOIN t_AccountSpecialCondition S ON B.OurBranchID = S.OurBranchID AND B.AccountID = S.AccountID
 			inner join t_ATM_UtilityCharges on 
				B.OurBranchID = t_ATM_UtilityCharges.OurBranchID and
 				B.AccountID = t_ATM_UtilityCharges.AcquiringAccountID
			WHERE  (A.OurBranchID = @OurBranchID) AND (A.AccountID = t_ATM_UtilityCharges.AcquiringAccountID)
			SET @vClearBalance2=@vClearBalance

			IF @vIsClose = 'C' or @vIsClose = 'T' or @vIsClose = 'X' or @vAllowDebitTransaction = 1
				BEGIN
					 
					SELECT @Status=3		--INACTIVE/CLOSE_ACCOUNT
					SELECT @Status  as Status
					RETURN (1)	
				END
-- 			IF @Amount >(@vClearBalance-@vClearedEffects+@limit)-@WithDrawlCharges or @Amount >((@vClearBalance-@vClearedEffects+@limit)-@vFreezeAmount-@vMinBalance-@WithDrawlCharges) or @Amount >((@vClearBalance-@vClearedEffects+@limit)-@vMinBalance-@WithDrawlCharges)
-- 				BEGIN
-- 					 
-- 					SELECT @Status=4		--LOW_BALANCE
-- 					SELECT @Status  as Status
-- 					RETURN (1)	
-- 				END
			IF @vAreCheckBooksAllowed = 0

				BEGIN
					 
					SELECT @Status=3		--INVALID_PRODUCT
					SELECT @Status  as Status
					RETURN (1)	
				END
			IF @mCurrencyID <> 'PKR'
				BEGIN
					 
					SELECT @Status=3		--INVALID_CURRENCY
					SELECT @Status  as Status
					RETURN (1)	
				END



 			SELECT 	 @AccountID2=IssuerAccountID,@mAccountName=IssuerAccountTitle
 				From t_ATM_UtilityCharges
 			WHERE (CompanyNumber = @CompanyNumber)


			Set @mSerialNo = 1
			Set @mTrxType = 'D'
			Set @mAccountType='G'
			SET @vBranchName = 'N/A'
			SET @mProductid ='GL'
			SET @mCurrencyID='PKR'

			SELECT @vBranchName=Heading2 FROM t_GlobalVariables WHERE (OurBranchID = @OurBranchID)

			SELECT @vCount4=count(*) From t_ATM_Branches
			WHERE BranchID = @WithdrawlBranchID

			IF @MerchantType <> '0000' 
				BEGIN
					IF @Currency = '586'
						BEGIN
								Set @mRemarks='Being Amount of INT.BK. Inter Bank Bill Payment Transaction From : ' + @CustomerBranchID + ' By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)
						END
					ELSE 
						IF @Currency = '840'
							BEGIN
								Set @mRemarks='Being Amount of INT.BK. Inter Bank Bill Payment Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)

							END
						ELSE 
							BEGIN
								Set @mRemarks='Being Amount of INT.BK. Inter Bank Bill Payment Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) From : ' + @NameLocation + ' By  A/c # ' + @AccountID +  ' At : '  + upper(@vBranchName)
							END
				END
			ELSE
				IF @vCount4 > 0 
					BEGIN
						SET  @mWithdrawlBranchName = 'N/A'
						SELECT @mWithdrawlBranchName=BranchName FROM t_ATM_Branches
						WHERE BranchID = @WithdrawlBranchID
						Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Branch Bill Payment Transaction From : BK.' + @BankShortName +  ' (' + upper(@mWithdrawlBranchName) + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)
					END
				--IF @vCount4 = 0 
				ELSE
					BEGIN
						Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Branch Bill Payment Transaction From : BK.' + @BankShortName +  ' (BR.' +  @WithdrawlBranchID + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)
					END

			IF @MerchantType <> '0000' 

				INSERT INTO t_ATM_TransferTransaction
					(
						ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,
						valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,
						BankCode,Remarks,OperatorID,SupervisorID,MCC,MerchantType,CompanyNumber,CreditCardNumber
					)		
		
				VALUES
					(
						@mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@AccountID2,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,
						@PHXDate,@mwDate,@PHXDate,@mTrxType,@Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescriptionH,@ATMID,
						@mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MCC,@MerchantType,@CompanyNumber,@CreditCardNumber
					)
			else
				INSERT INTO t_ATM_TransferTransaction
					(
						ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,
						valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,
						BankCode,Remarks,OperatorID,SupervisorID,MCC,MerchantType,CompanyNumber,CreditCardNumber
					)		
		
				VALUES
					(
						@mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@AccountID2,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,
						@mwDate,@mwDate,@PHXDate,@mTrxType,@Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescriptionH,@ATMID,
						@mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MCC,@MerchantType,@CompanyNumber,@CreditCardNumber
					)
		 
						SELECT @Status=@@ERROR
		
						IF @Status <> 0 
							BEGIN
								 
								RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR
								RETURN (5000)
							END
--Credit Transaction		


	
SET @mAccountName = ''
SET @mProductID = ''
SET @mCurrencyID = ''
SET @mAccountType = ''
--SET @vClearBalance = 0
--SET @vFreezeAmount = 0
--SET @vMinBalance = 0
SET @vIsClose = ''
SET @vAreCheckBooksAllowed = ''
SET @vAllowDebitTransaction = ''


			SELECT @Accountid=t_ATM_UtilityCharges.AcquiringAccountID,
			@mAccountName=B.Name, @mProductID=B.ProductID, @mCurrencyID=P.CurrencyID,@mAccountType='C',@vClearBalance=B.ClearBalance,@vClearedEffects=B.ClearedEffects,@vFreezeAmount=B.FreezeAmount,@vMinBalance=A.MinBalance,@limit=isnull(b.limit,0),@vIsClose=A.Status, @vAreCheckBooksAllowed=P.AreCheckBooksAllowed, @vAllowDebitTransaction=isnull(S.AllowDebitTransaction,0)
			FROM t_AccountBalance B INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND  B.ProductID = A.ProductID INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND  A.ProductID = P.ProductID LEFT OUTER JOIN t_AccountSpecialCondition S ON B.OurBranchID = S.OurBranchID AND B.AccountID = S.AccountID
 			inner join t_ATM_UtilityCharges on 
				B.OurBranchID = t_ATM_UtilityCharges.OurBranchID and
 				B.AccountID = t_ATM_UtilityCharges.AcquiringAccountID
			WHERE  (A.OurBranchID = @OurBranchID) AND (A.AccountID = t_ATM_UtilityCharges.AcquiringAccountID)

------------------------------Riz

			SELECT @vCount1=count(*) From t_Account
			WHERE (OurBranchID = @OurBranchID) AND( AccountID in (@AccountID2))
			IF  @vCount1=0
				BEGIN
					 
					SELECT @Status=2		--INVALID_ACCOUNT
					SELECT @Status  as Status
					RETURN (1)	
			END

-------------------------------End Riz



			Set @mSerialNo = 2
			Set @mTrxType = 'C'
			SET @vBranchName = 'N/A'
			SELECT @vBranchName=Heading2 FROM t_GlobalVariables WHERE (OurBranchID = @OurBranchID)

			SELECT @vCount4=count(*) From t_ATM_Branches
			WHERE BranchID = @WithdrawlBranchID

			IF @MerchantType <> '0000' 
				BEGIN
					IF @Currency = '586'
						BEGIN
							Set @mRemarks='Being Amount of INT.BK. Inter Bank Bill Payment Transaction From : ' + @CustomerBranchID + ' By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)
						END
					ELSE 
						IF @Currency = '840'
							BEGIN
								Set @mRemarks='Being Amount of INT.BK. Inter Bank Bill Payment Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)

							END
						ELSE 
							BEGIN
								Set @mRemarks='Being Amount of INT.BK. Inter Bank Bill Payment Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) From : ' + @NameLocation + ' By  A/c # ' + @AccountID +  ' At : '  + upper(@vBranchName)							END
				END
			ELSE
				IF @vCount4 > 0 
					BEGIN
						SET  @mWithdrawlBranchName = 'N/A'
						SELECT @mWithdrawlBranchName=BranchName FROM t_ATM_Branches
						WHERE BranchID = @WithdrawlBranchID
						Set @mRemarks='Being Amount of INT.BK. Inter Bank Bill Payment Transaction From : BK.' + @BankShortName +  ' (' + upper(@mWithdrawlBranchName) + ') By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)
					END
				--IF @vCount4 = 0 
				ELSE
					BEGIN
						Set @mRemarks='Being Amount of IB (' + @ATMID+ ') Inter Branch Fund Transaction From : BK.' + @BankShortName +  ' (BR.' +  @WithdrawlBranchID + ') By  A/c # ' + @AccountID +  ' At : '  + upper(@vBranchName)
					END
		IF @MerchantType <> '0000' 

			INSERT INTO t_ATM_TransferTransaction
				(
					ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,
					valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,
					BankCode,Remarks,OperatorID,SupervisorID,MCC,MerchantType,CompanyNumber,CreditCardNumber
				)		
	
			VALUES
	
				(
	
					@mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@AccountID2,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,
					@PHXDate,@mwDate,@PHXDate,@mTrxType,@Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescription,@ATMID,
					@mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MCC,@MerchantType,@CompanyNumber,@CreditCardNumber
				)
	
		else
			INSERT INTO t_ATM_TransferTransaction
				(
					ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,
					valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,
					BankCode,Remarks,OperatorID,SupervisorID,MCC,MerchantType,CompanyNumber,CreditCardNumber
				)		
	
			VALUES
	
				(
	
					@mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@AccountID2,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,
					@mwDate,@mwDate,@PHXDate,@mTrxType,@Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescription,@ATMID,
					@mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MCC,@MerchantType,@CompanyNumber,@CreditCardNumber
				)


				SELECT @Status=@@ERROR

				IF @Status <> 0 

					BEGIN
						 
						RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR

						RETURN (5000)
					END


-- 			SELECT @mAccountName=B.Name, @mProductID=B.ProductID, @mCurrencyID=P.CurrencyID,@mAccountType='C',@vClearBalance=B.ClearBalance,@vClearedEffects=B.ClearedEffects,@vFreezeAmount=B.FreezeAmount,@vMinBalance=A.MinBalance,@limit=isnull(b.limit,0),@vIsClose=A.Status, @vAreCheckBooksAllowed=P.AreCheckBooksAllowed, @vAllowDebitTransaction=isnull(S.AllowDebitTransaction,0)
-- 			FROM t_AccountBalance B INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND  B.ProductID = A.ProductID INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND  A.ProductID = P.ProductID LEFT OUTER JOIN t_AccountSpecialCondition S ON B.OurBranchID = S.OurBranchID AND B.AccountID = S.AccountID
-- 			inner join t_ATM_UtilityCharges on 
-- --				B.OurBranchID = t_ATM_UtilityCharges.OurBranchID 
-- 				B.AccountID = t_ATM_UtilityCharges.IssuerAccountID
-- 
-- 			WHERE  (A.OurBranchID = @OurBranchID) AND (A.AccountID = @AccountID)
-- 			SET @vClearBalance2=@vClearBalance
-- 			IF @vIsClose = 'C' or @vIsClose = 'T' or @vIsClose = 'X' or @vAllowDebitTransaction = 1
-- 				BEGIN
-- 					 
-- 					SELECT @Status=3		--INACTIVE/CLOSE_ACCOUNT
-- 					SELECT @Status  as Status
-- 					RETURN (1)	
-- 				END
-- 			IF @vAreCheckBooksAllowed = 0
-- 
-- 				BEGIN
-- 					 
-- 					SELECT @Status=3		--INVALID_PRODUCT
-- 					SELECT @Status  as Status
-- 					RETURN (1)	
-- 				END
-- 			IF @mCurrencyID <> 'PKR'
-- 				BEGIN
-- 					 
-- 					SELECT @Status=3		--INVALID_CURRENCY
-- 					SELECT @Status  as Status
-- 					RETURN (1)	
-- 				END


--		SELECT @Status = 0 , @vClearBalance2 = ((IsNull(@vClearBalance2,0)-IsNull(@vClearedEffects,0))-IsNull(@Amount,0)-isNull(@WithDrawlCharges,0))*100
--		SELECT @Status As Status , @vClearBalance2 As ClearBalance
--		RETURN 0


		SELECT @Status = 0 , @AvailableBalance = 0, @vClearBalance = 0,@Value='2'
		SELECT @Status As Status , @vClearBalance As ClearBalance , @AvailableBalance As AvailableBalance , @value As value

		RETURN 0

		 
	END