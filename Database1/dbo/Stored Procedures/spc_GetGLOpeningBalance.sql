﻿create PROCEDURE [dbo].[spc_GetGLOpeningBalance]          
 (            
  @OurBranchID varchar(30),             
  @AccountID varchar(30),            
  @ToDate datetime,            
  @@OpeningBalance numeric(18,2) = 0 OUTPUT,            
  @@ForeignOpeningBalance numeric(18,2) = 0 OUTPUT,            
  @@Balance numeric(18,2) = 0 OUTPUT,            
  @@ForeignBalance numeric(18,2) = 0 OUTPUT,            
  @@IsBalanced char(1)='T' OUTPUT            
 )            
AS            
            
              
 DECLARE             
  @LocalCurrency varchar(30),            
  @CurrencyID varchar(30)            
            
 DECLARE             
  @TempDate datetime,            
  @TempAmount numeric(18,2),            
  @TempForeignAmount numeric(18,2),            
  @TempIsCredit char(1)            
            
  SELECT @CurrencyID = CurrencyID FROM t_GL WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID            
             
  SELECT @LocalCurrency = LocalCurrency FROM t_GlobalVariables WHERE OurBranchID = @OurBranchID       
             
  if @CurrencyID = @LocalCurrency                
   SELECT @@OpeningBalance = OpeningBalance,@@ForeignOpeningBalance =0 FROM t_GL WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID              
  else            
   SELECT @@OpeningBalance = OpeningBalance,@@ForeignOpeningBalance =ForeignOpeningBalance FROM t_GL WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID              
            
  SELECT @@Balance = Balance,@@ForeignBalance =ForeignBalance FROM t_GL WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID              
            
  DECLARE Transactions CURSOR  FOR             
  SELECT Date, Amount, ForeignAmount, IsCredit FROM t_GLTransactions            
  WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND (Date< convert(datetime, @ToDate) )             
  ORDER BY Date            
            
  OPEN Transactions            
            
  FETCH NEXT FROM Transactions INTO @TempDate, @TempAmount, @TempForeignAmount, @TempIsCredit            
            
  WHILE @@FETCH_STATUS = 0            
  BEGIN            
   IF @CurrencyID = @LocalCurrency            
    BEGIN            
     SELECT @@OpeningBalance =             
     CASE @TempIsCredit            
      WHEN 1 THEN @@OpeningBalance + @TempAmount            
      WHEN 0 THEN @@OpeningBalance - @TempAmount            
     END             
     SELECT @@ForeignOpeningBalance =0            
    END                
   ELSE            
    BEGIN            
     SELECT @@OpeningBalance =             
     CASE @TempIsCredit            
      WHEN 1 THEN @@OpeningBalance + @TempAmount            
      WHEN 0 THEN @@OpeningBalance - @TempAmount            
     END            
     SELECT @@ForeignOpeningBalance =             
     CASE @TempIsCredit            
      WHEN 1 THEN @@ForeignOpeningBalance + @TempForeignAmount            
      WHEN 0 THEN @@ForeignOpeningBalance - @TempForeignAmount            
     END            
             
    END                
              
   FETCH NEXT FROM Transactions INTO @TempDate, @TempAmount, @TempForeignAmount, @TempIsCredit            
            
  END            
  CLOSE Transactions            
  DEALLOCATE Transactions            
 IF @@OpeningBalance = @@Balance             
  set  @@IsBalanced='T'            
 ELSE            
  set @@IsBalanced = 'F'            
             
            
             
  SELECT @@OpeningBalance  AS OpeningBalance ,@@ForeignOpeningBalance as ForeignOpeningBalance,          
  @@Balance  AS Balance ,@@ForeignBalance as ForeignBalance, @@IsBalanced as IsBalanced