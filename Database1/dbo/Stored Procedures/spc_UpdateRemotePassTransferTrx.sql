﻿CREATE PROCEDURE [dbo].[spc_UpdateRemotePassTransferTrx]        
(                  
 @OurBranchID varchar(30),                  
 @AccountID varchar(30),                  
 @SupervisorID varchar(30),                  
 @ScrollNo numeric(10),                  
 @SerialNo numeric(10),                  
 @Status char(1)='C',                
 @UseOD int=1,  
 @AddData text = ''  
)                  
                  
AS                  
   SET NOCOUNT ON                  
     
   DECLARE @vDate datetime                   
   DECLARE @mDate Varchar(32)                  
   DECLARE @ChequeID varchar(11)                  
   DECLARE @ClearBalance money                  
   DECLARE @Effects money                  
   DECLARE @Limit money                  
   DECLARE @TrxType char(1)                  
   DECLARE @Amount money                  
   DECLARE @ForeignAmount money                  
   DECLARE @IsLocalCurrency bit                  
   DECLARE @OldStatus char(1)                  
   DECLARE @AccountType char(1)                   
   DECLARE @OperatorID varchar(8)                  
     
   DECLARE @wDate datetime  
   DECLARE @VoucherID decimal(24,0)  
   DECLARE @RefNo varchar(100)  
   DECLARE @GLID varchar(30)  
   DECLARE @tGLID varchar(30)  
   DECLARE @TrxPrinted int  
   DECLARE @ProductID varchar(30)  
   DECLARE @GLControl varchar(30)  
   DECLARE @AccountName varchar(100)  
   DECLARE @CurrencyID varchar(30)  
   DECLARE @ChequeDate varchar(30)  
   DECLARE @DescriptionID varchar(30)  
   DECLARE @Description varchar(100)  
   DECLARE @BankCode varchar(30)  
   DECLARE @BranchCode varchar(30)  
   DECLARE @ExchangeRate decimal(24,6)  
   DECLARE @AppWHTax int  
   DECLARE @WHTScrollNo int  
     
   BEGIN                  
      /* Get Transfer Transaction */                  
      SELECT @OldStatus = Supervision, @ForeignAmount=ForeignAmount,@IsLocalCurrency=IsLocalCurrency,@vDate=wDate,@ChequeID= ChequeID,  
      @AccountName = AccountName,  
      @TrxType=TrxType,@Amount = Amount,@OperatorID=OperatorID, @AccountType=AccountType,@ProductID=ProductID,@CurrencyID = CurrencyID,  
      @ChequeDate=ChequeDate,@ExchangeRate=ExchangeRate,@DescriptionID=DescriptionID,@Description=Description,@TrxPrinted=TrxPrinted,@wDate=wDate,  
      @BankCode=BankCode,@BranchCode=BranchCode,@AddData=AdditionalData,@GLID=GLID,@AppWHTax=AppWHTax,@WHTScrollNo=WHTaxScrollNo,@RefNo=RefNo  
      FROM t_TransferTransactionModel  
      WHERE (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (ScrollNo = @ScrollNo AND SerialNo = @SerialNo)  
        
      set @tGLID = @GLID  
      if @tGLID = ''  
         set @tGLID = NULL  
           
      IF @OldStatus = 'C'                  
         BEGIN                  
            SELECT '2' as RetStatus -- Already Cleared                  
            RETURN                  
         END                  
      ELSE IF @OldStatus = 'R'                  
         BEGIN                  
            SELECT '3' as RetStatus --Already Rejected                  
            RETURN                  
         END                  
                  
      SELECT @mDate = Convert(varchar(10),@vDate ,101) + ' ' + convert(varchar(20),GetDate(),114)                  
                  
      /*UpDate Transaction File Status*/                
      if @UseOD = 1                
      begin                
         UPDATE t_TransferTransactionModel              
         SET Supervision = @Status, SupervisorID = @SupervisorID, RemoteDescription = 'Use OD Authority'              
         WHERE (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (ScrollNo = @ScrollNo AND SerialNo = @SerialNo)              
      end                
      else                
      begin                
         UPDATE t_TransferTransactionModel              
         SET Supervision = @Status, SupervisorID = @SupervisorID, RemoteDescription = 'Funds Available'              
         WHERE (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (ScrollNo = @ScrollNo AND SerialNo = @SerialNo)              
      end                
                    
      IF @AccountType = 'C'                  
         BEGIN                  
            /* Get Current ClearBalance, Effective Balance and Limit */                   
            SELECT @ClearBalance=ClearBalance,@Effects=Effects,@limit=Limit                   
            FROM t_AccountBalance                  
            WHERE (OurBranchID = @OurBranchID) and (AccountID = @AccountID)                   
                  
   IF @OldStatus = 'P' --OR @OldStatus = '*'                  
    BEGIN                  
     IF @IsLocalCurrency = 1                  
                    Update t_AccountBalance                  
                    Set ShadowBalance = ShadowBalance + @Amount,                  
                      ClearBalance = ClearBalance - @Amount                        
                    Where  OurBranchID = @OurBranchID and AccountID = @AccountID                  
     ELSE                  
                    Update t_AccountBalance                  
                    Set ShadowBalance = ShadowBalance + @ForeignAmount,                  
       ClearBalance = ClearBalance - @ForeignAmount,                  
       LocalClearBalance = LocalClearBalance - @Amount                  
                    Where  OurBranchID = @OurBranchID and AccountID = @AccountID           
    END                  
            ELSE                  
    BEGIN                  
      IF @Status = 'C'                   
      BEGIN                  
       IF @IsLocalCurrency = 1                  
          Update t_AccountBalance                  
                      Set ClearBalance = ClearBalance - @Amount                        
                      Where  OurBranchID = @OurBranchID and AccountID = @AccountID                  
       ELSE                  
                      Update t_AccountBalance                  
                      Set ClearBalance = ClearBalance - @ForeignAmount,                  
         LocalClearBalance = LocalClearBalance - @Amount                  
                      Where  OurBranchID = @OurBranchID and AccountID = @AccountID                  
      END                  
               END                  
                        
                  
         END                  
                  
      ELSE                  
         SELECT @ClearBalance=0,@Effects=0,@limit=0                   
                       
                  
      /* Create a New record For Superviser */                  
      INSERT INTO t_SupervisedBy                  
               (OurBranchID,AccountID,[Date],ScrollNo,ChequeID,ClearBalance,Effects,Limit,TrxType,Amount,Status,SupervisorID,TransactionCategory,RemotePass)                  
      VALUES                  
               (@OurBranchID,@AccountID,@mDate,@ScrollNo,@ChequeID,@ClearBalance,@Effects,@Limit,@TrxType,@Amount,@Status,@SupervisorID,'T',1)                   
                            
      /* Update Customer Status */                
      if @UseOD = 1                
      begin                
         Update t_CustomerStatus Set Status = @Status              
         Where (OurBranchID=@OurBranchID) and (AccountID = @AccountID)              
         and (ScrollNo = @ScrollNo) AND (SerialNo = @SerialNo) and  (Left(TransactionType,1)='T')              
      end                
      else                
      begin              
         Update t_CustomerStatus Set IsOverDraft = 0, Status = @Status              
         Where (OurBranchID=@OurBranchID) and (AccountID = @AccountID)              
         and (ScrollNo = @ScrollNo) AND (SerialNo = @SerialNo) and  (Left(TransactionType,1)='T')              
      end                
                    
   /** rameez starts                
                   
      /*@Update OtherTransaction Table For Cash*/                  
      Update t_OtherTransactionStatus                  
      Set Status = @Status                  
      Where (OurBranchID=@OurBranchID) and (AccountID = @AccountID) and (ScrollNo = @ScrollNo)                   
               AND (SerialNo = @SerialNo) and  (Left(TransactionType,1)='T')                  
                  
                  
-----------------------------------------                  
-----------------------------------------        ** rameez ends **/            
                  
--Local Remittance Reject                  
             
 if ((select COUNT(*) from t_TransferTransactionModel            
 Where (OurBranchID=@OurBranchID) and (ScrollNo = @ScrollNo) and Supervision <> 'C') = 0)            
 begin            
               
   Update t_LR_Issuance            
   Set Supervision = @Status, SuperviseBy = @SupervisorID, SuperviseTime = @mDate        
   Where (OurBranchID=@OurBranchID) and  (ScrollNo = @ScrollNo)            
   and (convert(varchar(10), wDate, 101) = convert(varchar(10), @vDate, 101))      
         
   Update t_LR_Issuance      
   Set Supervision = @Status, SuperviseBy = @SupervisorID, SuperviseTime = @mDate      
   Where (OurBranchID=@OurBranchID) and (PaidScrollNo = @ScrollNo) and PaidMode = 'TR'      
   and (convert(varchar(10), wDate, 101) = convert(varchar(10), @vDate, 101))      
       
   Update t_LR_Issuance      
   Set Supervision = @Status, SuperviseBy = @SupervisorID, SuperviseTime = @mDate    
   Where (OurBranchID=@OurBranchID) and (CancellScrollNo = @ScrollNo) and Cancel = '1'    
   and (convert(varchar(10), CancelDate, 101) = convert(varchar(10), @vDate, 101))      
         
 end            
             
/** rameez starts            
            
--Local Remittance Advice Reject                  
 Update t_LR_Advice                  
 Set Supervision = @Status                  
 Where (OurBranchID=@OurBranchID) and  (ScrollNo = @ScrollNo) and  (wDate = @vDate)                  
--------------------------------------------                  
-----------------                  
  ** rameez ends **/            
              
      IF @Status = 'R' AND UPPER(LTRIM(RTRIM(@ChequeID))) <> 'V' AND LTRIM(RTRIM(@ChequeID)) <> ''                   
         BEGIN                  
            DELETE FROM t_ChequePaid                  
            WHERE (OurBranchID = @OurBranchID) and (AccountID = @AccountID)                   
                     and (ChequeID = @ChequeID)                  
         END                  
        
      select @VoucherID = ISNULL(MAX(VoucherID),0) + 1 from t_GLTransactions where OurBranchID = @OurBranchID  
        
      declare @IsCredit int  
      declare @TransactionMethod char(1)  
        
      if @AccountType = 'C'  
      begin  
           
         insert into t_Transactions (OurBranchID, ScrollNo, SerialNo, RefNo, wDate, AccountType, DocumentType, AccountID, AccountName,  
         ProductID, CurrencyID, ValueDate, TrxType, ChequeID, ChequeDate, Amount, ForeignAmount, ExchangeRate, ProfitLoss, MeanRate,  
         DescriptionID, [Description], BankCode, BranchCode, TrxPrinted, GLID, [Status], IsLocalCurrency, OperatorID,  
         SupervisorID, AdditionalData, IsMainTrx, DocType, GLVoucherID, AppWHTax, WHTaxMode, WHTaxAmount, WHTScrollNo)  
           
         VALUES (@OurBranchID,@ScrollNo,@SerialNo,@RefNo,@wDate,@AccountType,'T',@AccountID,@AccountName,  
         @ProductID,@CurrencyID,@vDate,@TrxType,@ChequeID,@ChequeDate, @Amount, @ForeignAmount,@ExchangeRate,0,@ExchangeRate,  
         @DescriptionID,@Description,@BankCode,@BranchCode, @TrxPrinted, @tGLID, 'C', @IsLocalCurrency, @OperatorID, @SupervisorID,   
         @AddData,'1','T', @VoucherID, @AppWHTax, 'T', '0', @WHTScrollNo)  
           
         if @TrxType = 'C'  
         begin  
           set @IsCredit = 1  
           set @DescriptionID = 'TR0'  
           set @Description = 'Account ID: ' + @AccountID +', Account Name: ' + @AccountName +', Module: Transfer Credit Entry...'  
         end  
         else  
         begin  
           set @IsCredit = 0  
           set @DescriptionID = 'TR1'  
           set @Description = 'Account ID: ' + @AccountID +', Account Name: ' + @AccountName +' , Module: Transfer Debit Entry...'  
         end  
           
         SELECT @GLControl = ISNULL(GLControl, '') from t_Products where OurBranchID = @OurBranchID and ProductID = @ProductID  
           
         if @IsLocalCurrency = 1  
         begin  
           set @ForeignAmount = 0  
           set @ExchangeRate = 1  
           set @TransactionMethod = 'L'  
         end  
         else  
         begin  
           set @TransactionMethod = 'A'  
         end  
           
         insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],  
         CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,  
         Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType)  
           
         values (@OurBranchID, @GLControl, @VoucherID, '1', @wDate, @vDate, @DescriptionID, @Description,  
         @CurrencyID, @Amount, @ForeignAmount, @ExchangeRate, @IsCredit, 'Transfer', @TransactionMethod, @OperatorID, @SupervisorID,  
         '', '0', '0', @AddData, '0', 'T')  
           
      end  
      else  
      begin  
           
         if @TrxType = 'C'  
           set @IsCredit = 1  
         else  
           set @IsCredit = 0  
           
         if @IsLocalCurrency = 1  
         begin  
           set @ForeignAmount = 0  
           set @ExchangeRate = 1  
           set @TransactionMethod = 'L'  
         end  
         else  
         begin  
           set @TransactionMethod = 'A'  
         end  
           
         insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],  
         CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,  
         Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType)  
           
         values (@OurBranchID, @AccountID, @VoucherID, '1', @wDate, @vDate, @DescriptionID, @Description,  
         @CurrencyID, @Amount, @ForeignAmount, @ExchangeRate, @IsCredit, 'Transfer', @TransactionMethod, @OperatorID, @SupervisorID,  
         '', @ScrollNo, @SerialNo, @AddData, '1', 'T')  
           
      end  
        
      SELECT '0' as RetStatus                  
      RETURN                  
   END