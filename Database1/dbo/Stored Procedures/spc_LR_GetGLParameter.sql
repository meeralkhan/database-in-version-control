﻿CREATE PROCEDURE [dbo].[spc_LR_GetGLParameter]    
(    
 @OurBranchID varchar(30),    
 @InstrumentType varchar(30)    
)    
AS    
BEGIN    
    
 SET NOCOUNT ON    
     
 BEGIN TRY    
     
  IF (@InstrumentType = 'PO')    
  BEGIN    
       
   IF EXISTS ( SELECT PaymentOrder FROM t_LR_GLParameters WHERE OurBranchID = @OurBranchID AND SerialID = 1 )    
   BEGIN    
    SELECT 'OK' as ReturnStatus, PaymentOrder as AccountID   
    FROM t_LR_GLParameters  
    WHERE OurBranchID = @OurBranchID AND SerialID = 1    
   END    
   ELSE    
   BEGIN    
    Raiserror('LRGLParamUndefine', 15, 1)    
   END    
       
  END    
  ELSE IF (@InstrumentType = 'BC')    
  BEGIN    
      
   IF EXISTS ( SELECT BankersCheque FROM t_LR_GLParameters WHERE OurBranchID = @OurBranchID AND SerialID = 1 )    
   BEGIN    
    SELECT 'OK' as ReturnStatus, BankersCheque as AccountID  
    FROM t_LR_GLParameters  
    WHERE OurBranchID = @OurBranchID AND SerialID = 1    
   END    
   ELSE    
   BEGIN    
    Raiserror('LRGLParamUndefine', 15, 1)    
   END    
      
  END    
  ELSE IF (@InstrumentType = 'PS')    
  BEGIN    
       
   IF EXISTS ( SELECT PaySlip FROM t_LR_GLParameters WHERE OurBranchID = @OurBranchID AND SerialID = 1 )    
   BEGIN    
    SELECT 'OK' as ReturnStatus, PaySlip as AccountID  
    FROM t_LR_GLParameters  
    WHERE OurBranchID = @OurBranchID AND SerialID = 1    
   END    
   ELSE    
   BEGIN    
    Raiserror('LRGLParamUndefine', 15, 1)    
   END    
      
  END    
  ELSE IF (@InstrumentType = 'DD')    
  BEGIN    
      
   IF EXISTS ( SELECT DemandDraft FROM t_LR_GLParameters WHERE OurBranchID = @OurBranchID AND SerialID = 1 )    
   BEGIN    
    SELECT 'OK' as ReturnStatus, DemandDraft as AccountID  
    FROM t_LR_GLParameters  
    WHERE OurBranchID = @OurBranchID AND SerialID = 1    
   END    
   ELSE    
   BEGIN    
    Raiserror('LRGLParamUndefine', 15, 1)    
   END    
       
  END    
  ELSE IF (@InstrumentType = 'TT')    
  BEGIN    
      
   IF EXISTS ( SELECT TeleTransfer FROM t_LR_GLParameters WHERE OurBranchID = @OurBranchID AND SerialID = 1 )    
   BEGIN    
    SELECT 'OK' as ReturnStatus, TeleTransfer as AccountID  
    FROM t_LR_GLParameters   
    WHERE OurBranchID = @OurBranchID AND SerialID = 1    
   END    
   ELSE    
   BEGIN    
    Raiserror('LRGLParamUndefine', 15, 1)    
   END    
       
  END    
  ELSE    
  BEGIN    
   raiserror('InvalidInstType', 15, 1);    
  END    
    
 END TRY    
     
 BEGIN CATCH    
      
  SELECT 'Error' as ReturnStatus, Error_Message() as ReturnMessage    
     
 END CATCH    
END