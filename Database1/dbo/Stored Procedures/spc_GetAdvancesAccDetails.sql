﻿CREATE procedure [dbo].[spc_GetAdvancesAccDetails] (  
 @OurBranchID varchar(30),  
 @AccountID varchar(30),  
 @ProductID varchar(30)  
)  
AS  
BEGIN  
  
SET NOCOUNT ON   
  
 BEGIN TRY   
   
  select 'Ok' as ReturnStatus, b.AccountID, b.Name, b.ProductID, c.[Description] as ProductDescription,   
  d.CurrencyID, d.[Description] as CurrencyDescription, a.AuthStatus as LoanAuthStatus,   
  b.AuthStatus as NotLoanAuthStatus, ISNULL(a.Status, '') as LoanStatus, ISNULL(b.Status,'') as NotLoanStatus  
  from t_Account a  
  INNER JOIN t_Account b on a.OurBranchID=b.OurBranchID AND a.ClientID=b.ClientID AND b.ProductID = @ProductID  
  INNER JOIN t_Products c on a.OurBranchID=c.OurBranchID AND b.ProductID=c.ProductID   
  INNER JOIN t_Currencies d on a.OurBranchID=d.OurBranchID AND c.CurrencyID=d.CurrencyID  
  where a.OurBranchID=@OurBranchID AND a.AccountID = @AccountID  
  
 END TRY  
  
 BEGIN CATCH  
  Select 'Error' as ReturnStatus, ERROR_MESSAGE() as ReturnMessage  
  
 END CATCH  
END