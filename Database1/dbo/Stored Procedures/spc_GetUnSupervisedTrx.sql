﻿CREATE PROCEDURE [dbo].[spc_GetUnSupervisedTrx]  
(   
 @OurBranchID varchar(30)  
)   
AS  
  
set nocount on  
  
BEGIN TRY   
    
  SELECT 'Ok' AS ReturnStatus, 'C' Module, ScrollNo, '1' SerialNo, AccountID, AccountName, ProductID, CurrencyID, TrxType, ChequeID, ChequeDate, Amount,   
  ForeignAmount, ExchangeRate, Supervision, OperatorID FROM t_CashTransactionModel WHERE OurBranchID = @OurBranchID AND Supervision NOT IN ('C','R')  
  UNION ALL  
  SELECT 'Ok' AS ReturnStatus, 'T' Module, ScrollNo, SerialNo, AccountID, AccountName, ProductID, CurrencyID, TrxType, ChequeID, ChequeDate, Amount,   
  ForeignAmount, ExchangeRate, Supervision, OperatorID FROM t_TransferTransactionModel WHERE OurBranchID = @OurBranchID AND Supervision NOT IN ('C','R')  
  UNION ALL  
  SELECT 'Ok' AS ReturnStatus, 'I' Module, SerialNo ScrollNo, '0' SerialNo, AccountID, AccountName, ProductID, CurrencyID, 'D' TrxType, ChequeID,   
  ChequeDate, Amount, ForeignAmount, ExchangeRate, [Status] Supervision, OperatorID FROM t_InwardClearing   
  WHERE OurBranchID = @OurBranchID AND [Status] NOT IN ('','R')  
  UNION ALL  
  SELECT 'Ok' AS ReturnStatus, 'O' Module, ScrollNo, SerialNo, AccountID, AccountName, ProductID, CurrencyID, 'C' TrxType, ChequeID, ChequeDate, Amount,   
  ForeignAmount, ExchangeRate, [Status] Supervision, OperatorID FROM t_OutwardClearing WHERE OurBranchID = @OurBranchID AND [Status] NOT IN ('C','R')  
  UNION ALL  
  SELECT 'Ok' AS ReturnStatus, 'B' Module, ScrollNo, '1' SerialNo, '' AccountID, '' AccountName, 'GL' ProductID, CurrencyID, TrxType, ChequeID,   
  wDate ChequeDate, Amount, ForeignAmount, ExchangeRate, Supervision, OperatorID FROM t_BureaDeChange WHERE OurBranchID = @OurBranchID AND   
  Supervision NOT IN ('C','R')  
  UNION ALL  
  SELECT 'Ok' AS ReturnStatus, 'OC' Module, ScrollNo, SerialNo, AccountID, AccountName, ProductID, CurrencyID, TrxType, ChequeID, ChequeDate, Amount,   
  ForeignAmount, ExchangeRate, Supervision, OperatorID FROM t_OnLineCashSupervisionRequired WHERE OurBranchID = @OurBranchID AND Supervision NOT IN ('C','R')  
  UNION ALL  
  SELECT 'Ok' AS ReturnStatus, 'OT' Module, ScrollNo, SerialNo, AccountID, AccountName, ProductID, CurrencyID, TrxType, ChequeID, ChequeDate, Amount,   
  ForeignAmount, ExchangeRate, Supervision, OperatorID FROM t_OnLineTransferSupervisionRequired WHERE OurBranchID = @OurBranchID AND Supervision NOT IN ('C','R')  
    
END TRY   
BEGIN CATCH   
    
  select 'Error' AS ReturnStatus, ERROR_NUMBER() AS ReturnErrorNo, ERROR_MESSAGE() AS ReturnMessage;   
    
END CATCH