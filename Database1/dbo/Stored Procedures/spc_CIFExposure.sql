﻿
CREATE   PROCEDURE [dbo].[spc_CIFExposure]
(
  @OurBranchID nvarchar(30),
  @ClientID nvarchar(30)
)
AS

SET NOCOUNT ON

select l.ReferenceNo AS FacilityID, l.FacilityDesc, l.GroupID AS ReferenceNo, p.ProductID, p.Description AS ProductDesc, l.LimitSecType, p.LimitType, p.Limit,
--ISNULL(l.FacilityDP,0) AS DrawingPower,
(
   select abs(ISNULL(SUM(ClearBalance),0)) from t_AccountBalance ab
   inner join t_Account a ON ab.OurBranchID = a.OurBranchID and ab.AccountID = a.AccountID and l.ClientID = a.ClientID
   inner join t_LinkFacilityAccount al ON ab.OurBranchID = al.OurBranchID and l.ClientID = al.ClientID and l.SerialNo = al.FacilityID and ab.AccountID = al.AccountID
   where a.OurBranchID = @OurBranchID and a.ClientID = @ClientID and a.ProductID = p.ProductID and ab.ClearBalance <= 0
) AS Utilized,
(p.Limit + (
   select ISNULL(SUM(ClearBalance),0) from t_AccountBalance ab
   inner join t_Account a ON ab.OurBranchID = a.OurBranchID and ab.AccountID = a.AccountID and l.ClientID = a.ClientID
   inner join t_LinkFacilityAccount al ON ab.OurBranchID = al.OurBranchID and l.ClientID = al.ClientID and l.SerialNo = al.FacilityID and ab.AccountID = al.AccountID
   where a.OurBranchID = @OurBranchID and a.ClientID = @ClientID and a.ProductID = p.ProductID and ab.ClearBalance <= 0
   )
) AS Outstanding, l.DateReview AS ReviewDate, l.DateExpiry AS ExpiryDate
from v_Facilities l
inner join t_FacilityProduct p on l.OurBranchID = p.OurBranchID and l.ClientID = p.ClientID and l.SerialNo = p.FacilityID and p.Status = ''
where l.OurBranchID = @OurBranchID AND l.ClientID = @ClientID
and ((l.Status = '' and Status2 = '') or (l.Status = 'C' and Status2 = 'A') or (l.Status = 'B' and Status2 = 'A'))