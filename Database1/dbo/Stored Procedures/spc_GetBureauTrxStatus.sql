﻿CREATE PROCEDURE [dbo].[spc_GetBureauTrxStatus]   
(   
 @OurBranchID varchar(4),   
 @GLID varchar(30)='',   
 @OperatorID varchar(8)='',   
 @CreditSupervisionLimit money=0,   
 @DebitSupervisionLimit money=0,   
 @DebitMinSupervisionLimit money=0,   
 @CreditMinSupervisionLimit money=0,   
 @TransactionType char(1)='',   
 @Program varchar(50)='',   
 @Filter char(1)='',   
 @IsOverDraft bit=0,   
 @ScrollNo money=0   
)   
   
AS   
 SET NOCOUNT ON   
   
 if @DebitMinSupervisionLimit is Null   
 Select @DebitMinSupervisionLimit = 0   
   
 if @CreditMinSupervisionLimit is Null   
 Select @CreditMinSupervisionLimit = 0   
   
 IF @OperatorID = ''   
 BEGIN   
 IF @ScrollNo <> 0   
 BEGIN   
 IF (SELECT Count(ScrollNo) From t_CustomerStatus   
 WHERE (OurBranchID = @OurBranchID AND ScrollNo = @ScrollNo )   
 AND ((ABS(Amount*Rate) <= @CreditSupervisionLimit AND (Amount*Rate) >= @CreditMinSupervisionLimit and Amount >0)   
 OR (ABS(Amount*Rate) <= @DebitSupervisionLimit AND ABS(Amount*Rate) >= @DebitMinSupervisionLimit AND Amount < 0)) AND   
 (LEFT(TransactionType,1) = @TransactionType) and status = '*' ) > 0   
 BEGIN   
 SELECT '0' As RetVal, Scrollno,SerialNo,Accountid,Accountname,Amount,Status,TransactionType,Operatorid from t_CustomerStatus   
 WHERE (OurBranchID = @OurBranchID AND ScrollNo = @ScrollNo )   
 AND ((ABS(Amount*Rate) <= @CreditSupervisionLimit AND (Amount*Rate) >= @CreditMinSupervisionLimit and Amount >0)   
 OR (ABS(Amount*Rate) <= @DebitSupervisionLimit AND ABS(Amount*Rate) >= @DebitMinSupervisionLimit AND Amount < 0)) AND   
 (LEFT(TransactionType,1) = @TransactionType) and status = '*'   
   
 Order by SerialNo   
 END   
 ELSE   
 BEGIN   
 SELECT '1' As RetVal   
 Return   
 END   
 END   
 ELSE   
 BEGIN   
 IF @GLID = '' AND @Filter = ''   
 BEGIN   
 SELECT Scrollno,SerialNo,Accountid,Accountname,Amount,Status,TransactionType,Operatorid from t_CustomerStatus   
 WHERE (OurBranchID = @OurBranchID)   
 AND ((ABS(Amount*Rate) <= @CreditSupervisionLimit AND (Amount*Rate) >= @CreditMinSupervisionLimit and Amount >0)   
 OR (ABS(Amount*Rate) <= @DebitSupervisionLimit AND ABS(Amount*Rate) >= @DebitMinSupervisionLimit AND Amount < 0)) AND   
 (LEFT(TransactionType,1) = @TransactionType) and status = '*' -- <> 'P'   
 Order by ScrollNo desc   
 END   
   
 ELSE IF @GLID = '' AND @Filter = 'A'   
 BEGIN   
 Select Scrollno,SerialNo,Accountid,Accountname,Amount,Status,TransactionType,Operatorid from t_CustomerStatus   
 Where (OurBranchID = @OurBranchID)   
 AND ((Abs(Amount*Rate) <= @CreditSupervisionLimit and (Amount*Rate) >= @CreditMinSupervisionLimit and Amount > 0)   
 OR (Abs(Amount*Rate) <= @DebitSupervisionLimit and abs(Amount*Rate) >= @DebitMinSupervisionLimit and Amount < 0)) and (IsOverdraft = @IsOverDraft)   
 and (left(TransactionType,1) = @TransactionType)   
 Order by ScrollNo desc   
 END   
   
 ELSE IF @GLID = ''   
 BEGIN   
 Select Scrollno,SerialNo,Accountid,Accountname,Amount,Status,TransactionType,Operatorid from t_CustomerStatus   
 Where (OurBranchID = @OurBranchID)   
 AND ((Abs(Amount*Rate) <= @CreditSupervisionLimit and Amount >= @CreditMinSupervisionLimit and Amount > 0)   
 OR (Abs(Amount*Rate) <= @DebitSupervisionLimit and abs(Amount*Rate) >= @DebitMinSupervisionLimit and Amount < 0))   
 and Status = @Filter and (IsOverdraft = @IsOverDraft) and (left(TransactionType,1) = @TransactionType)   
   
 Order by ScrollNo desc   
 END   
   
 ELSE IF @GLID <> ''   
 BEGIN   
 Select Scrollno,SerialNo,Accountid,Accountname,Amount,Status,TransactionType,Operatorid from t_CustomerStatus   
 Where (OurBranchID = @OurBranchID) and (GLID = @GLID) and (left(TransactionType,1) = @TransactionType)   
 Order by ScrollNo desc   
 END   
 END   
 END   
------------------------------------------------   
 ELSE   
 BEGIN   
 IF @ScrollNo <> 0   
 BEGIN   
 IF (SELECT Count(ScrollNo) From t_CustomerStatus   
 WHERE (OurBranchID = @OurBranchID AND ScrollNo = @ScrollNo )   
 AND ((ABS(Amount*Rate) <= @CreditSupervisionLimit AND (Amount*Rate) >= @CreditMinSupervisionLimit and Amount >0)   
 OR (ABS(Amount*Rate) <= @DebitSupervisionLimit AND ABS(Amount*Rate) >= @DebitMinSupervisionLimit AND Amount < 0)) AND   
 (LEFT(TransactionType,1) = @TransactionType) and status = '*' ) > 0   
 BEGIN   
 SELECT '0' As RetVal, Scrollno,SerialNo,Accountid,Accountname,Amount,Status,TransactionType,Operatorid from t_CustomerStatus   
 WHERE (OurBranchID = @OurBranchID AND ScrollNo = @ScrollNo )   
 AND ((ABS(Amount*Rate) <= @CreditSupervisionLimit AND (Amount*Rate) >= @CreditMinSupervisionLimit and Amount >0)   
 OR (ABS(Amount*Rate) <= @DebitSupervisionLimit AND ABS(Amount*Rate) >= @DebitMinSupervisionLimit AND Amount < 0)) AND   
 (LEFT(TransactionType,1) = @TransactionType) and status = '*'   
   
 Order by SerialNo   
 END   
 ELSE   
 BEGIN   
 SELECT '1' As RetVal   
 Return   
 END   
 END   
 ELSE   
   
 BEGIN   
 IF @GLID = '' AND @Filter = ''   
 BEGIN   
 Select Scrollno,SerialNo,Accountid,Accountname,Amount,Status,TransactionType,Operatorid from t_CustomerStatus   
 Where (OurBranchID = @OurBranchID) and (OperatorID = @OperatorID) and (left(TransactionType,1) = @TransactionType)   
 Order by ScrollNo desc   
 END   
 ELSE IF @GLID = '' AND (@Filter = 'R' OR @Filter = 'C')   
 BEGIN   
 Select Scrollno,SerialNo,Accountid,Accountname,Amount,Status,TransactionType,Operatorid from t_CustomerStatus   
 Where (OurBranchID = @OurBranchID) and (OperatorID = @OperatorID) and (left(TransactionType,1) = @TransactionType)   
 and Status = @Filter   
 Order by ScrollNo desc   
 END   
 ELSE IF @GLID = ''   
 BEGIN   
 Select Scrollno,SerialNo,Accountid,Accountname,Amount,Status,TransactionType,Operatorid from t_CustomerStatus   
 Where (OurBranchID = @OurBranchID) and (OperatorID = @OperatorID) and (left(TransactionType,1) = @TransactionType)   
 and Status in ('*','P')   
 Order by ScrollNo desc   
 END   
 ELSE IF @GLID <> '' AND @Filter = ''   
 BEGIN   
 Select Scrollno,SerialNo,Accountid,Accountname,Amount,Status,TransactionType,Operatorid from t_CustomerStatus   
 Where (OurBranchID = @OurBranchID)   
 and ((Abs(Amount) <= @CreditSupervisionLimit and (Amount*Rate) >= @CreditMinSupervisionLimit and Amount*Rate > 0) OR (Abs(Amount*Rate) <= @DebitSupervisionLimit and abs(Amount*Rate) >= @DebitMinSupervisionLimit and Amount*Rate < 0)) and   
 Status = '*' and (left(TransactionType,1) = @TransactionType)   
 Order by ScrollNo desc   
 END   
 ELSE IF @GLID <> '' AND @Filter = 'A'   
 BEGIN   
 Select Scrollno,SerialNo,Accountid,Accountname,Amount,Status,TransactionType,Operatorid from t_CustomerStatus   
 Where (OurBranchID = @OurBranchID) and (left(TransactionType,1) = @TransactionType) and   
 ((Abs(Amount*Rate) <= @CreditSupervisionLimit and (Amount*Rate) > @CreditMinSupervisionLimit and Amount > 0) OR (Abs(Amount*Rate) <= @DebitSupervisionLimit and abs(Amount*Rate) > @DebitMinSupervisionLimit and Amount < 0)) and   
 (Status <> 'P')   
 Order by ScrollNo desc   
 END   
   
 ELSE IF @GLID <> ''   
 BEGIN   
 Select Scrollno,SerialNo,Accountid,Accountname,Amount,Status,TransactionType,Operatorid from t_CustomerStatus   
 Where (OurBranchID = @OurBranchID) and (left(TransactionType,1) = @TransactionType) and   
 ((Abs(Amount*Rate) <= @CreditSupervisionLimit and (Amount*Rate) > @CreditMinSupervisionLimit and Amount > 0) OR (Abs(Amount*Rate) <= @DebitSupervisionLimit and abs(Amount*Rate) > @DebitMinSupervisionLimit and Amount < 0)) and   
 Status = @Filter   
 Order by ScrollNo desc   
 END   
   
 END   
   
 END