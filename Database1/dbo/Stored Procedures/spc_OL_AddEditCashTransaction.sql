﻿CREATE PROCEDURE [dbo].[spc_OL_AddEditCashTransaction]                
(                
 @ScrollNo int=0,                
 @RefNo varchar(30)='',                
 @IBCANo int=0,                
 @SerialNo int=0,                
 @OurBranchID varchar(30),                
 @CustomerAccountID varchar(30),                
 @CustomerAccountName varchar(100),                
 @CustomerProductID varchar(30),                
 @CustomerCurrencyID varchar(30),                
 @CustomerAccountType char(1),                
 @CustomerTrxType  char(1),                
 @CustomerIsLocalCurrency Char(1),                
 @wDate datetime,                
 @ValueDate datetime=@wDate,                
 @ChequeID varchar(30),                
 @ChequeDate DateTime=@wDate,                
 @Amount money,                
 @ForeignAmount Money=0,                
 @ExchangeRate Money=1,                
 @DescriptionID varchar(30),                
 @Description varchar(255),                
 @Supervision char(1),                
 @AddData text=NULL,            
                 
 @AccountID varchar(30),                
 @AccountName varchar(100),                
                 
 @Supervised bit,                
 @TransactionMode char(1),                
                 
 @OperatorID varchar(30),                  
 @SupervisorID varchar(30),                  
 @SourceBranch Varchar(30),                
 @TargetBranch varchar(30),                
 @GLID varchar(30),                
 @Remarks varchar(255),                  
 @MethodType varchar(2)='',                  
 @DocumentType Varchar(2)='',                  
 @InstrumentType varchar(50)='',                  
 @IssueDate  datetime='01/01/1900',                  
 @InstrumentNo int=0,                  
 @ControlNo  int=0,                  
 @Beneficiary varchar(255)='',                  
 @DrawanBank  varchar(100)='',                  
 @DrawanBranch varchar(100)='',                    
 @AppWHTax  Char(1)=0,                  
 @WHTaxAmount   money=0,                  
 @WHTaxMode  Char(1)='',                  
 @WHTaxAccountID  VarChar(30)='',                  
 @NewRecord bit=1                
)                
                
AS                
    
declare @cRefNo int    
    
set nocount on                  
                   
 IF @NewRecord<>0                   
  BEGIN                   
                   
   SET @SerialNo=1                
                   
   SELECT @IBCANo = ISNULL(MAX(IBCA),0)+1 FROM t_OnLineIBCANo WHERE OurBranchID = @OurBranchID        
           
   SELECT @ScrollNo = ISNULL(MAX(ScrollNo),0)+1 FROM t_OnLineCashSupervisionRequired WHERE OurBranchID = @OurBranchID        
           
   select @RefNo = @OurBranchID + convert(varchar(13), @IBCANo)                
       
   select @cRefNo = convert(int, @RefNo)    
       
   INSERT INTO t_OnLineCashSupervisionRequired                  
   (                  
    ScrollNo,RefNo,IBCANo,SerialNo,OurBranchID,AccountID,AccountName,ProductID,CurrencyID,AccountType,ValueDate,wDate,TrxType,                  
    ChequeID,ChequeDate, Amount, ForeignAmount,ExchangeRate, DescriptionID,Description,Supervision,                  
    OperatorID,SupervisorID,IsLocalCurrency,SourceBranch, TargetBranch,GLID,Remarks,MethodType, AppWHTax, WHTaxAmount, WHTaxMode,                  
    WHTaxAccountID, AdditionalData            
   )                  
   VALUES                  
   (                  
    @ScrollNo,@cRefNo,@IBCANo, @SerialNo,@OurBranchID,@CustomerAccountID,@CustomerAccountName,@CustomerProductID,                  
    @CustomerCurrencyID,@CustomerAccountType,@ValueDate,@wDate + ' ' + convert(varchar(10), GetDate(), 108),@CustomerTrxType,@ChequeID,@ChequeDate,                  
    @Amount, @ForeignAmount,@ExchangeRate, @DescriptionID,@Description,@Supervision, @OperatorID,@SupervisorID,                  
    @CustomerIsLocalCurrency,@SourceBranch, @TargetBranch,@GLID, @Remarks,@MethodType,@AppWHTax,@WHTaxAmount,@WHTaxMode,                  
    @WHTaxAccountID, @AddData            
   )                  
             
  IF @MethodType = 'LR'                  
   BEGIN                  
    INSERT INTO t_OL_LRPayment                  
    (                  
     ScrollNo,DocumentType,IssueBranchID,wDate,AccountID,AccountName,InstrumentType,IssueDate, InstrumentNo, ControlNo,          
     Amount,ForeignAmount,Beneficiary, DrawanBank,DrawanBranch, BankID, BranchID          
    )                  
                   
    VALUES                  
    (                  
     @cRefNo,@DocumentType, @TargetBranch,@wDate + ' ' + convert(varchar(10), GetDate(), 108),@CustomerAccountID,@CustomerAccountName,@InstrumentType,                  
     @IssueDate,@InstrumentNo,@ControlNo, @Amount, @ForeignAmount,@Beneficiary, @DrawanBank, @DrawanBranch, @DrawanBank, @OurBranchID          
    )                  
   END                  
                   
   INSERT INTO t_OnLineIBCAno (IBCA,OurBranchID,OperatorID,TargetBranch)                
                   
   VALUES (@IBCANo, @OurBranchID, @OperatorID, @TargetBranch)                
                   
   INSERT INTO t_OL_TransactionStatus (OurBranchID,ScrollNo,SerialNo,RefNo,AccountID,AccountName,CurrencyID,Amount,                 
   ForeignAmount,ExchangeRate,Status,Supervised,TrxType,IsLocalCurrency,TransactionMode,TransactionType,OperatorID)                
                   
   VALUES (@OurBranchID,@ScrollNo,@SerialNo,@cRefNo,@AccountID,@AccountName,@CustomerCurrencyID,@Amount,@ForeignAmount,                
   @ExchangeRate,'C',@Supervised,@CustomerTrxType,@CustomerIsLocalCurrency,@TransactionMode,'C',@OperatorID)                
                   
  END                  
                  
 ELSE                  
  BEGIN                  
   UPDATE t_OnLineCashSupervisionRequired SET                  
    AccountID=@CustomerAccountID,AccountName=@CustomerAccountName,                  
    ProductID=@CustomerProductID,CurrencyID=@CustomerCurrencyID,AccountType=@CustomerAccountType,                  
    ValueDate=@ValueDate,wDate=@wDate + ' ' + convert(varchar(10), GetDate(), 108),TrxType=@CustomerTrxType,                  
    ChequeID=@ChequeID,ChequeDate=@ChequeDate, Amount=@Amount,                  
    ForeignAmount=@ForeignAmount,ExchangeRate=@ExchangeRate,                  
    DescriptionID=@DescriptionID,Description=@Description,Supervision=@Supervision,                  
    OperatorID=@OperatorID,SupervisorID=@SupervisorID,                  
    IsLocalCurrency=@CustomerIsLocalCurrency,SourceBranch=@SourceBranch,                   
    TargetBranch=@TargetBranch,Remarks=@Remarks,                  
    AppWHTax = @AppWHTax, WHTaxAmount = @WHTaxAmount, WHTaxMode = @WHTaxMode, WHTaxAccountID = @WHTaxAccountID,            
    AdditionalData = @AddData            
                  
   WHERE OurBranchID=@OurBranchID AND ScrollNo=@ScrollNo                  
  END                  
                  
 select 'Ok' AS RetStatus, @ScrollNo AS ScrollNo, @IBCANo AS IBCANo, @cRefNo AS RefNo, @MethodType MethodType