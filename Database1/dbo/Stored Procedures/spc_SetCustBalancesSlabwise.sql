﻿CREATE PROCEDURE [dbo].[spc_SetCustBalancesSlabwise]
(
  @sDate datetime,
  @eDate datetime
)
AS

SET NOCOUNT ON

update t_UploadDepCsv set MinSlabAmount = NULL, MaxSlabAmount = NULL WHERE Dated between @sDate and @eDate;

declare @ProductID nvarchar(30)
declare @MinSlabAmount money
declare @MaxSlabAmount money

declare curs CURSOR for select ProductID, MinSlabAmount, MaxSlabAmount from t_PoolProductSlabs

open curs
FETCH NEXT FROM curs INTO @ProductID, @MinSlabAmount, @MaxSlabAmount

WHILE (@@FETCH_STATUS = 0)
BEGIN
   
   update t_UploadDepCsv set MinSlabAmount = @MinSlabAmount, MaxSlabAmount = @MaxSlabAmount where ProductID = @ProductID and AccountBalance between @MinSlabAmount and @MaxSlabAmount and Dated between @sDate and @eDate

   FETCH NEXT FROM curs INTO @ProductID, @MinSlabAmount, @MaxSlabAmount

END

CLOSE curs
DEALLOCATE curs

select 'Ok' AS ReturnStatus