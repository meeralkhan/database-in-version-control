﻿create PROCEDURE [dbo].[spc_MarkAutoCloseProcess]
(             
 @OurBranchID as varchar(30),          
 @ProductID as varchar(30),          
 @DateFrom as datetime,  
 @Dateto  as datetime,          
 @DifferenceDays as int,  
 @OperatorID as Varchar(30),          
 @ComputerName as Varchar(50)          
)          
AS          
BEGIN          
          
SET NOCOUNT ON          
             
   DECLARE @mDate as VarChar(50)            
   DECLARE @Count as int      
   DECLARE @Count2 as int      
   DECLARE @RetStat as int        
   SET @RetStat = 0  
   SET @Count = 0            
   SET @Count2 = 0   
            
   SELECT @Count = Count(*) FROM t_Account a
   INNER JOIN t_Products p ON a.OurBranchID = p.OurBranchID
   WHERE a.OurBranchID = @OurBranchID AND a.ProductID = @ProductID AND p.AutoCloseDays > '0' 
   AND ((ISNULL(p.IsAdvancesProduct,'No') = 'No' AND ISNULL(p.IsTermDeposit,'No') = 'No'))
   AND a.AccountID In (	Select AccountID             
						From t_AccountStatusMarking  
						Where t_AccountStatusMarking.OurBranchID = @OurBranchID            
						AND DateAdd(Day, +@DifferenceDays, t_AccountStatusMarking.WorkingDate) <= @Dateto  
						AND IsNull(t_AccountStatusMarking.Status,'') = 'I'
					  )   
					  AND IsNull(a.Status,'') = 'I' 
  
   SELECT @Count2 = Count(*) FROM t_Account a
   INNER JOIN t_Products p ON a.OurBranchID = p.OurBranchID
   WHERE a.OurBranchID = @OurBranchID AND a.ProductID = @ProductID AND a.OpenDate <= @DateFrom AND p.AutoCloseDays > '0' 
   AND ((ISNULL(p.IsAdvancesProduct,'No') = 'No' AND ISNULL(p.IsTermDeposit,'No') = 'No'))
   AND a.AccountID Not In (	Select AccountID               
							From t_Transactions               
							Where t_Transactions.OurBranchID = @OurBranchID 
							And convert(varchar(10), t_Transactions.wdate, 120) >= @DateFrom And     
							convert(varchar(10), t_Transactions.wdate, 120) <= @DateTo              
							And t_Transactions.DescriptionID Not In('I01','I02','I03','I04','I10','I20','I30','I40','A01','A02','SCR') 
							AND IsNull(t_Transactions.[Status],'') <> 'R'
						  )
						  AND IsNull(a.Status,'') Not IN ('C','T','X','D','I') 
  
   SELECT @mDate = Convert(varchar(10),@Dateto ,120) + ' ' + convert(varchar(20),GetDate(),114)  

   IF @Count > 0  
   BEGIN          
       
     Insert INTO t_AutoCloseHistory  
     (OurBranchID, wDate, ProductID, AccountID, AccountName, ClearBalance, BeforMarkStatus, TerminalID, OperatorID)          
              
     SELECT @OurBranchID, @mDate, a.ProductID, a.AccountID, a.Name, ab.ClearBalance, IsNull(a.Status,''), @ComputerName, @OperatorID
     FROM t_Account a
     INNER JOIN t_AccountBalance ab On a.OurBranchID = ab.OurBranchID AND a.AccountID = ab.AccountID   
     AND a.ProductID = @ProductID And (a.CloseDate Is Null or a.CloseDate = '1/1/1900')
     INNER JOIN t_Products p On a.OurBranchID = p.OurBranchID And a.ProductId = p.ProductID  
     WHERE a.OurBranchID = @OurBranchID AND a.ProductID = @ProductID AND p.AutoCloseDays > '0' 
     AND ((ISNULL(p.IsAdvancesProduct,'No') = 'No' AND ISNULL(p.IsTermDeposit,'No') = 'No'))
	 And a.AccountID In (	Select AccountID             
							From t_AccountStatusMarking  
							Where t_AccountStatusMarking.OurBranchID = @OurBranchID            
							AND DateAdd(Day, +@DifferenceDays, t_AccountStatusMarking.WorkingDate) <= @Dateto  
							AND IsNull(t_AccountStatusMarking.[Status],'') = 'I'
						)   
						AND IsNull(a.[Status],'') = 'I' 
  
     SET @RetStat = 1  
   
   END   
   ELSE IF @Count2 > 0   
   BEGIN  
      
    Insert INTO t_AutoCloseHistory  
    (OurBranchID, wDate, ProductID, AccountID, AccountName, ClearBalance, BeforMarkStatus, TerminalID, OperatorID)          
              
    SELECT @OurBranchID, @mDate, a.ProductID, a.AccountID, a.Name, ab.ClearBalance, IsNull(a.Status,''), @ComputerName, @OperatorID
    FROM t_Account a
    INNER JOIN t_AccountBalance ab On a.OurBranchID = ab.OurBranchID AND a.AccountID = ab.AccountID   
    AND a.ProductID = @ProductID And (a.CloseDate Is Null or a.CloseDate = '1/1/1900') 
    INNER JOIN t_Products p On a.OurBranchID = p.OurBranchID And a.ProductId = p.ProductID  
    WHERE a.OurBranchID = @OurBranchID AND a.ProductID = @ProductID AND a.OpenDate <= @DateFrom AND p.AutoCloseDays > '0' 
    AND ((ISNULL(p.IsAdvancesProduct,'No') = 'No' AND ISNULL(p.IsTermDeposit,'No') = 'No'))
	And a.AccountID Not In (	Select AccountID 
                                From t_Transactions            
                                Where t_Transactions.OurBranchID = @OurBranchID            
                                And Convert(varchar(10),t_Transactions.wdate,120) Between @DateFrom And @DateTo            
                                And t_Transactions.DescriptionID Not In('I01','I02','I03','I04','I10','I20','I30','I40','A01','A02','SCR')     
								AND IsNull(t_Transactions.[Status],'') <> 'R'
							) 
							AND IsNull(a.[Status],'') Not IN ('C','T','X','D','I')     
  
     SET @RetStat = 2  
   
   END  
   ELSE  
   BEGIN  
  
    SET @RetStat = 0  
   
   END  
  
   IF @RetStat = 1 OR @RetStat = 2  
   BEGIN  
  
    SELECT @RetStat as RetStat, h.AccountID, a.[Name] as AccountName, h.wDate, h.ProductID, a.CurrencyID, AM.Serial, AM.Comments, 
	ab.ClearBalance, c.MeanRate, p.GLControl
    FROM t_AutoCloseHistory h   
    INNER JOIN	t_Products p on h.OurBranchID = p.OurBranchID AND h.ProductID = p.ProductID
	INNER JOIN	t_Account a ON h.OurBranchID = a.OurBranchID AND h.ProductID = a.ProductID
	INNER JOIN	t_AccountBalance ab ON a.OurBranchID = ab.OurBranchID AND a.AccountID = ab.AccountID
	INNER JOIN	t_Currencies c ON p.OurBranchID = c.OurBranchID AND p.CurrencyID = c.CurrencyID
	LEFT JOIN	(
		SELECT TOP 1 s.OurBranchID, s.AccountID, s.Serial, s.Comments FROM t_AccountStatusMarking s
		WHERE DateAdd(Day, +@DifferenceDays, s.WorkingDate) <= @Dateto AND IsNull(s.Status,'') = 'I'
		Order by s.Serial Desc
	) AM ON h.OurBranchID = AM.OurBranchID AND h.AccountID = AM.AccountID
	where h.OurBranchID = @OurBranchID AND h.ProductID = @ProductID AND CONVERT(varchar(10), h.wDate, 120) = @Dateto  
    ORDER BY h.ProductID  

   END  
   ELSE  
   BEGIN  
  
    SELECT @RetStat as RetStat  
  
   END  
 END