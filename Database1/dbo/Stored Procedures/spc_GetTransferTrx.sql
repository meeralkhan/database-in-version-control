﻿CREATE    PROCEDURE [dbo].[spc_GetTransferTrx]                  
(                  
 @OurBranchID varchar(30),                  
 @ScrollNo numeric(10,0),                  
 @SerialNo numeric(10,0)=0,                  
 @Status Varchar(15)=''                  
)                  
                  
AS                  
        
SET NOCOUNT ON        
                     
   DECLARE @AccountType as Char(1)                  
                  
   BEGIN                     
      IF @SerialNo = 0 AND @ScrollNo = 0                   
         BEGIN                  
            IF (SELECT count(OurBranchID) FROM t_TransferTransactionModel                  
               WHERE OurBranchID = @OurBranchID ) > 0                  
               BEGIN                  
                  Print 'Right Now Not Used'                  
               END                  
            ELSE                  
               BEGIN                  
                  SELECT '0' as RetStatus --Trx Not Found                  
                  RETURN                  
               END                  
         END                  
                  
      ELSE IF @SerialNo <> 0 AND @ScrollNo <> 0                  
         BEGIN                  
            IF (SELECT count(ScrollNo) FROM t_TransferTransactionModel                  
               WHERE OurBranchID = @OurBranchID AND ScrollNo = @ScrollNo AND SerialNo = @SerialNo ) > 0                  
               BEGIN                  
                     SELECT @AccountType = AccountType --,@AccountID = AccountID                   
                     FROM t_TransferTransactionModel                  
                     WHERE (OurBranchID = @OurBranchID AND ScrollNo = @ScrollNo AND SerialNo = @SerialNo)                  
                  
                     IF @AccountType = 'C'                  
                        BEGIN                  
                           SELECT '1' as RetStatus, T.RefNo, T.AccountID, T.AccountName,T.ProductID, T.CurrencyID,T.AccountType,                   
                               T.wDate,T.ValueDate,T.memo2, T.TrxType,T.ChequeID, T.ChequeDate, T.Amount, T.ForeignAmount,                   
                               T.ExchangeRate, T.DescriptionID, T.Description Memo, T.AdditionalData,T.GLID, T.Supervision,                   
                               T.OperatorID,T.SupervisorID, T.IsLocalCurrency, B.ClearBalance, B.Reminder,                  
                               B.Effects, B.ShadowBalance, B.Limit, B.FreezeAmount as BlockedAmount,                  
                               ISNULL(P.MinBalance,0) AS ProductMinBalance,    T.ChannelRefID,(select top 1 a.Description from t_TransactionDescriptions a where T.OurBranchID = a.OurBranchID and T.DescriptionID = a.DescriptionID) Description,      
                               P.Description as ProductDescription, C.Description as CurrencyDescription,T.TClientId,T.TAccountid,T.TProductId  
                           FROM t_TransferTransactionModel as T, t_AccountBalance as B,                   
                                t_Products as P, t_Currencies as C , t_Account Acc, t_Customer Cus, t_Products Pro                 
                              
                           WHERE T.OurBranchID = B.OurBranchID AND T.AccountID = B.AccountID                  
                                 AND T.OurBranchID = P.OurBranchID AND T.ProductID = P.ProductID                  
                                 AND T.OurBranchID = C.OurBranchID AND T.CurrencyID = C.CurrencyID                  
                                 AND T.OurBranchID = @OurBranchID AND T.ScrollNo = @ScrollNo AND T.SerialNo = @SerialNo    
                        END                  
                     ELSE                  
                  
                        BEGIN                  
                           SELECT '1' as RetStatus,T.RefNo, T.AccountID, T.AccountName,T.ProductID, T.CurrencyID,T.AccountType,                   
         T.wDate,T.ValueDate,T.memo2, T.TrxType,T.ChequeID, T.ChequeDate, T.Amount, T.ForeignAmount,                   
   T.ExchangeRate, T.DescriptionID, T.Description Memo, T.AdditionalData,T.GLID, T.Supervision,                   
     T.OperatorID,T.SupervisorID, T.IsLocalCurrency, CASE  T.IsLocalCurrency WHEN 0 THEN G.Balance WHEN 1 THEN G.ForeignBalance END AS ClearBalance, '' as Reminder,          
                               0 as Effects, G.ShadowBalance, 0 as Limit, 0 BlockedAmount, 0 ProductMinBalance,   T.ChannelRefID,     
          (select top 1 a.Description from t_TransactionDescriptions a where T.OurBranchID = a.OurBranchID and T.DescriptionID = a.DescriptionID) Description,      
                  'General Ledger' as ProductDescription, C.Description as CurrencyDescription,T.TClientId,T.TAccountid,T.TProductId  
                           FROM t_TransferTransactionModel as T, t_GL as G,                   
                                t_Currencies as C  
                              
                 WHERE T.OurBranchID = G.OurBranchID AND T.AccountID = G.AccountID                                                   
                    AND T.OurBranchID = C.OurBranchID AND T.CurrencyID = C.CurrencyID                  
                                 AND T.OurBranchID = @OurBranchID AND T.ScrollNo = @ScrollNo AND T.SerialNo = @SerialNo        
                        END                  
                  END                  
            ELSE                  
               BEGIN                  
                  SELECT '0' as RetStatus -- Trx Not Found                  
                  RETURN                  
               END                  
         END                  
                  
      ELSE                  
                  
         BEGIN                     
            IF (SELECT count(OurBranchID) FROM t_TransferTransactionModel                  
               WHERE OurBranchID = @OurBranchID AND ScrollNo = @ScrollNo AND Supervision = @Status ) > 0                  
               BEGIN                  
                           SELECT '1' as RetStatus,SerialNo, T.RefNo, T.AccountID, T.AccountName,T.ProductID, T.CurrencyID,T.AccountType,                   
                               T.wDate,T.ValueDate,T.memo2, T.TrxType,T.ChequeID, T.ChequeDate, T.Amount, T.ForeignAmount,                   
                               T.ExchangeRate, T.DescriptionID, T.Description, T.AdditionalData,T.GLID, T.Supervision,                   
                               T.OperatorID,T.SupervisorID, T.IsLocalCurrency, B.ClearBalance, B.Reminder,                  
                               B.Effects, B.ShadowBalance, B.Limit, B.FreezeAmount as BlockedAmount,                  
                               ISNULL(P.MinBalance,0) AS ProductMinBalance,          
                               P.Description as ProductDescription, C.Description as CurrencyDescription,T.TClientId,T.TAccountid,T.TProductId  
          FROM t_TransferTransactionModel as T, t_AccountBalance as B,                   
                                t_Products as P, t_Currencies as C  
  
                           WHERE T.OurBranchID = B.OurBranchID AND T.AccountID = B.AccountID                  
                                 AND T.OurBranchID = P.OurBranchID AND T.ProductID = P.ProductID                  
                                 AND T.OurBranchID = C.OurBranchID AND T.CurrencyID = C.CurrencyID                  
                                 AND T.OurBranchID = @OurBranchID AND T.ScrollNo = @ScrollNo AND Supervision = @Status      
                  
         UNION ALL                  
                  
                           SELECT '1' as RetStatus,SerialNo,T.RefNo, T.AccountID, T.AccountName,T.ProductID, T.CurrencyID,T.AccountType,                   
                               T.wDate,T.ValueDate,T.memo2, T.TrxType,T.ChequeID, T.ChequeDate, T.Amount, T.ForeignAmount,                   
                               T.ExchangeRate, T.DescriptionID, T.Description, T.AdditionalData, T.GLID, T.Supervision,        
                               T.OperatorID, T.SupervisorID,T.IsLocalCurrency, CASE  T.IsLocalCurrency WHEN 0 THEN G.Balance WHEN 1 THEN G.ForeignBalance END AS ClearBalance, '' as Reminder,                  
            0 as Effects, G.ShadowBalance, 0 as Limit, 0 BlockedAmount, 0 ProductMinBalance,          
                               'General Ledger' as ProductDescription, C.Description as CurrencyDescription,T.TClientId,T.TAccountid,T.TProductId  
                           FROM t_TransferTransactionModel as T, t_GL as G,                
                                t_Currencies as C  
                                 
                           WHERE T.OurBranchID = G.OurBranchID AND T.AccountID = G.AccountID                          
                                 AND T.OurBranchID = C.OurBranchID AND T.CurrencyID = C.CurrencyID                  
                                 AND T.OurBranchID = @OurBranchID AND T.ScrollNo = @ScrollNo  AND Supervision = @Status       
                      
                  
               END                  
            ELSE                  
               BEGIN                  
                  SELECT '0' as RetStatus -- Trx Not Found                  
                  RETURN                  
               END                  
         END                  
                  
   END