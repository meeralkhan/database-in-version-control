﻿CREATE PROCEDURE [dbo].[spc_RptGetTodayTransactions]        
(                
 @OurBranchID varchar(30),                
 @AccountID varchar(30),                
 @AccountType varchar(1)                
)                
 AS                
 set nocount on            
 IF @AccountType = 'C'                
 begin              
                
  select wDate,ValueDate,TrxType,ChequeID,Description,Amount,ForeignAmount                
  from t_CashTransactionModel                 
  where OurBranchID = @OurBranchID and AccountID = @AccountID and Supervision = 'C'                
  union ALL              
  select wDate,ValueDate,TrxType,ChequeID,Description,Amount,ForeignAmount                
  from t_TransferTransactionModel                 
  where OurBranchID = @OurBranchID and AccountID = @AccountID and Supervision = 'C'                
  union ALL              
  select wDate,ValueDate,TransactionType,ChequeID,Description,Amount,ForeignAmount                
  from t_InwardClearing where OurBranchID = @OurBranchID and AccountID = @AccountID                
  union ALL              
  select wDate,ValueDate,TrxType,ChequeID,Description,Amount,ForeignAmount                
  from t_AllModelTransaction where OurBranchID = @OurBranchID and AccountID = @AccountID                
  /*      
  union ALL              
  select wDate,ValueDate,TrxType,'v' as ChequeID,Description,Amount,ForeignAmount                
  from t_ATM_CashTransaction where OurBranchID = @OurBranchID and AccountID = @AccountID                
  */  
  union ALL              
  select wDate,ValueDate,TrxType,ChequeID,Description,Amount,ForeignAmount                
  from t_OnLineCashTransaction where OurBranchID = @OurBranchID and AccountID = @AccountID                
    
 end              
 ELSE                
 begin              
                
  declare @MOAccountID varchar(30)
                
  SELECT @MOAccountID=AccountID                
  FROM t_GlParameters WHERE SerialID='2' AND OurBranchID=@OurBranchID /* Main Office Account */                
                
  select wDate,ValueDate,TrxType,ChequeID,Description,Amount,ForeignAmount                
  from t_CashTransactionModel                 
  where  OurBranchID = @OurBranchID and AccountID = @AccountID and  Supervision = 'C'                 
  union                
  select wDate,ValueDate,'D' as TrxType,ChequeID,Description,Amount,ForeignAmount                
  from t_CashTransactionModel                 
  where  OurBranchID = @OurBranchID and GLID = @AccountID and  Supervision = 'C' and TrxType = 'C'                
  union                
  select wDate,ValueDate,'C' as TrxType,ChequeID,Description,Amount,ForeignAmount                
  from t_CashTransactionModel                 
  where  OurBranchID = @OurBranchID and GLID = @AccountID and  Supervision = 'C' and TrxType = 'D'                
  union                
  select wDate,ValueDate,TrxType,ChequeID,Description,Amount,ForeignAmount                
  from t_TransferTransactionModel                
  where  OurBranchID = @OurBranchID and AccountID = @AccountID and  Supervision = 'C'                 
  union                
  select wDate,ValueDate,TransactionType,ChequeID,Description,Amount,ForeignAmount                
  from t_InwardClearing                
  where  OurBranchID = @OurBranchID and AccountID = @AccountID                
  union                
  select wDate,ValueDate,'C' as TransactionType,ChequeID,Description,Amount,ForeignAmount                
  from t_InwardClearing                
  where OurBranchID = @OurBranchID and AccountID = @MOAccountID                
/*                
  select *  from v_GetTodayGLCashTransactions                
  where  OurBranchID = @OurBranchID and AccountID = @AccountID                 
  union                
  select *  from v_GetTodayGLCashCredit                
  where  OurBranchID = @OurBranchID and GLID = @AccountID                
  union                
  select *  from v_GetTodayGLCashDebit                
  where  OurBranchID = @OurBranchID and GLID = @AccountID                
  union                
  select *  from v_GetTodayGLTransferTransactions         
  where OurBranchID = @OurBranchID and AccountID = @AccountID                
  union                
  select *  from v_GetTodayGLInwardTransaction       
  where OurBranchID = @OurBranchID and AccountID = @AccountID                
  union                
  select *  from v_GetTodayGLInwardMO                
  where OurBranchID = @OurBranchID and AccountID = @MOAccountID                
*/                
                
  end