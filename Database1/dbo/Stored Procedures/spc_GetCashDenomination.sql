﻿CREATE PROCEDURE [dbo].[spc_GetCashDenomination] (            
 @OurBranchID varchar(30),            
 @CurrencyID varchar(30),              
 @wDate DateTime              
)            
AS              
SET NOCOUNT ON        
BEGIN            
 Select a.*, c.Description as CurrencyDescription From t_CashDenomination a       
 INNER JOIN t_Currencies c ON a.OurBranchID=c.OurBranchID AND a.CurrencyID=c.CurrencyID      
 Where (a.OurBranchID = @OurBranchID) and (a.CurrencyID = @CurrencyID) and (convert(varchar(10), a.wDate, 120) = @wDate)              
END