﻿
CREATE PROCEDURE [dbo].[sp_ATM_AddEditTransferTransaction_Remote]  
(      
 @OurBranchID       varchar(30),       
 @ATMID       varchar(30),       
 @MerchantType      varchar(30),      
 @Amount        money,      
 @WithdrawlBranchID      varchar(30)='',       
 @AccountID           varchar(30),  
 @AccountName         varchar(100),  
 @Supervision       char(1),       
 @RefNo        varchar(36),  
 @PHXDate        datetime,      
 @OurIMD       varchar(30),  
 @Currency       varchar(3),      
 @NameLocation      varchar(40),  
 @MCC        varchar(30),      
 @TrxDesc        varchar(30)='',      
 @NewRecord       bit=1  
)      

AS      
 DECLARE @BankShortName as varchar(30)      
      
 DECLARE @mScrollNo as int      
 DECLARE @mSerialNo as smallint      
 DECLARE @mAccountID as varchar(30)      
 DECLARE @mAccountType as char      
 DECLARE @mProductID as varchar(30)      
 DECLARE @mCurrencyID as varchar(30)      
 DECLARE @mIsLocalCurrency as bit      
  SET @mIsLocalCurrency=1       
 DECLARE @mwDate as datetime      
 DECLARE @mTrxType as char      
 DECLARE @mGLID as varchar(30)      
  SET @mGLID = ''       
 DECLARE @mForeignAmount as money      
  SET @mForeignAmount=0       
 DECLARE @mExchangeRate as money      
  SET @mExchangeRate=1       
 DECLARE @mDescriptionID as varchar(3)      
  SET @mDescriptionID='000'       
 DECLARE @mDescription as varchar(255)      
 DECLARE @STAN as varchar(30)
  BEGIN       
  
   SET @BankShortName = 'N/A'      
   
   SELECT @BankShortName = BankShortName From t_ATM_Banks      
   WHERE (BankIMD = @OurIMD)      
   
   SET @mDescription='ATM(' + @ATMID + ') IB-CSH WDRL AT BK.' + @BankShortName  + ' (BR.' + @WithdrawlBranchID + ')'       
   
   IF @MerchantType <> '0000'       
--    SET @mDescription='ATM(' + @ATMID + ') IBK-CSH WDRL AT BK.' + @BankShortName  + ' (BR.' + @WithdrawlBranchID + ')'       
    IF @Currency = '586'      
     BEGIN      
      SET @mDescription='ATM(' + @ATMID + ') IBK-CSH WDRL AT ' + @NameLocation       
     END   
     ELSE       
      SET @mDescription='ATM(' + @ATMID + ') IB-CSH WDRL AT BK.' + @BankShortName  + ' (BR.' + @WithdrawlBranchID + ')'       
     END      
  
 DECLARE @mDescriptionCharges as varchar(255)      
  BEGIN       
   IF @MerchantType <> '0000'       
--    SET @mDescriptionCharges='ATM(' + @ATMID + ') IBK-CSH WDRL-CHG AT BK.' + @BankShortName  + ' (BR.' + @WithdrawlBranchID + ')'       
    IF @Currency = '586'      
     BEGIN      
      SET @mDescriptionCharges='ATM(' + @ATMID + ') IBK-CSH WDRL-CHG AT ' + @NameLocation       
     END      
  END       
 DECLARE @mDescriptionH as varchar(255)      
  BEGIN       
   IF @MerchantType <> '0000'       
--    SET @mDescriptionH='ATM(' + @ATMID + ') IBK-CSH WDRL AT BK.' + @BankShortName   + ' (BR.' + @WithdrawlBranchID + ') ' + ' From : A/c # ' + @AccountID      
    IF @Currency = '586'      
     BEGIN      
      SET @mDescriptionH='ATM(' + @ATMID + ') IBK-CSH WDRL AT ' +  + @NameLocation + ' From : A/c # ' + @AccountID      
     END      
   ELSE       
    SET @mDescriptionH='ATM(' + @ATMID + ') IB-CSH WDRL AT BK.' + @BankShortName   + ' (BR.' + @WithdrawlBranchID + ') ' + ' From : A/c # ' + @AccountID      
  END       
 DECLARE @mDescriptionHCharges as varchar(255)      
  BEGIN       
   IF @MerchantType <> '0000'       
--    SET @mDescriptionHCharges='ATM(' + @ATMID + ') IBK-CSH WDRL-CHG AT BK.' + @BankShortName   + ' (BR.' + @WithdrawlBranchID + ') ' + ' From : A/c # ' + @AccountID      
    IF @Currency = '586'      
     BEGIN      
      SET @mDescriptionHCharges='ATM(' + @ATMID + ') IBK-CSH WDRL-CHG AT ' + @NameLocation + ' From : A/c # ' + @AccountID      
     END      
  END       
 DECLARE @mRemarks as varchar(200)      
 DECLARE @mBankCode as varchar(30)      
  SET @mBankCode='1279'       
 DECLARE @mOperatorID as varchar(30)      
  SET @mOperatorID = 'ATM-' + @ATMID      
 DECLARE @mSupervisorID as varchar(30)      
  SET @mSupervisorID = @OurIMD       
      
--Checking Variables      
 DECLARE @vCount1 as bit      
 DECLARE @vCount2 as bit      
 DECLARE @vCount3 as bit      
 DECLARE @vCount4 as bit      
 DECLARE @vAccountID as varchar(30)      
 DECLARE @vClearBalance as money      
      
 DECLARE @AvailableBalance as money      
 DECLARE @value as varchar(1)      
      
 DECLARE @vClearedEffects as money      
 DECLARE @vFreezeAmount as money      
 Declare @Limit as money      
 DECLARE @vMinBalance as money      
 DECLARE @vIsClose as char      
 DECLARE @vAreCheckBooksAllowed as bit      
 DECLARE @vAllowDebitTransaction as bit      
 DECLARE @vBranchName as varchar(50)      
 DECLARE @Status as int       
      
 DECLARE @mLastEOD as datetime      
-- DECLARE @mWorkingDate as datetime      
 DECLARE @mCONFRETI as datetime      
 DECLARE @mWithdrawlBranchName as varchar(50)      
      
 DECLARE @WithDrawlCharges as money      
 DECLARE @WithDrawlChargesPercentage as money      
 DECLARE @IssuerAccountID as varchar(30)      
 DECLARE @IssuerAccountTitle as varchar(50)      
 DECLARE @IssuerChargesAccountID as varchar(30)      
 DECLARE @IssuerTitle as varchar(50)      
 DECLARE @mRemarksCharges as varchar(200)      
 Declare @BalanceEnquiryCharges as money      
      
 SET NOCOUNT ON      
      
 SELECT @Status = -1      
---------------------      
      
   SELECT @mLastEOD=LastEOD,@mwDate=WorkingDate,@mCONFRETI=CONFRETI FROM t_Last WHERE (OurBranchID = @OurBranchID)      
   IF @mLastEOD = @mwDate or @mCONFRETI = '1/1/9999'      
    BEGIN      
            
     SELECT @Status=59  --REMOTE INVALID_DATE  
     SELECT @Status  as Status   
     RETURN (1)       
    END      
   
   IF CONVERT(varchar(20), @PHXDate, 101) <> CONVERT(varchar(20), @mwDate, 101)  
    BEGIN      
            
     SELECT @Status=58  --Source & Remote INVALID_DATE  
     SELECT @Status  as Status  
     RETURN (1)       
    END      
   
   SELECT @mScrollNo=(ISNULL(MAX(ScrollNo),0)+1) FROM t_ATM_TransferTransaction WHERE (OurBranchID = @OurBranchID) and (convert(varchar(10), wDate, 120) = convert(varchar(10), @mwDate, 120)) 
     
   SELECT @vCount1=count(*) From t_GL WHERE (OurBranchID = @OurBranchID) AND( AccountID = (select ATMAccountID from t_ATM_ATMAccount where ATMID = @ATMID))      
   IF @vCount1=0      
    BEGIN      
            
     SELECT @Status=52  --INVALID_ATM_ACCOUNT      
     SELECT @Status  as Status      
     RETURN (1)       
    END      
     
   SELECT @vCount1=count(*) From t_GL WHERE (OurBranchID = @OurBranchID) AND( AccountID = (select IssuerAccountID from t_ATM_SwitchCharges where MerchantType = @MerchantType))      
   IF @vCount1=0      
    BEGIN      
            
     SELECT @Status=53  --INVALID_ATM_HO_ACCOUNT      
     SELECT @Status  as Status      
     RETURN (1)       
    END      
     
   Declare @DbAccount varchar(30)  
   Declare @CrAccount varchar(30)  
     
   Declare @DbDesc varchar(100)  
   Declare @CrDesc varchar(100)  
     
   Declare @DbCurrency varchar(30)  
   Declare @CrCurrency varchar(30)  
     
   SELECT @DbAccount = AccountID, @DbDesc = Description, @DbCurrency = CurrencyID FROM t_GL  
   WHERE (OurBranchID = @OurBranchID) AND( AccountID = (select IssuerAccountID from t_ATM_SwitchCharges where MerchantType = @MerchantType))  
     
   SELECT @CrAccount = AccountID, @CrDesc = Description, @CrCurrency = CurrencyID FROM t_GL  
   WHERE (OurBranchID = @OurBranchID) AND( AccountID = (select ATMAccountID from t_ATM_ATMAccount where ATMID = @ATMID))  
     
   IF @mCurrencyID <> 'PKR'      
    BEGIN      
            
     SELECT @Status=55  --INVALID_CURRENCY      
     SELECT @Status  as Status      
     RETURN (1)       
    END      
     
   IF @DbCurrency <> 'PKR'      
    BEGIN      
            
     SELECT @Status=55  --INVALID_CURRENCY      
     SELECT @Status  as Status      
     RETURN (1)       
    END      
     
   IF @CrCurrency <> 'PKR'      
    BEGIN      
            
     SELECT @Status=55  --INVALID_CURRENCY      
     SELECT @Status  as Status      
     RETURN (1)       
    END      
      
   Set @mSerialNo = 1      
   Set @mTrxType = 'D'      
   SET @vBranchName = 'N/A'      
   SELECT @vBranchName=Heading2 FROM t_GlobalVariables WHERE (OurBranchID = @OurBranchID)      
      
   Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Branch Cash Withdrawl Transaction From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@AccountName) + ' At : '  + upper(@vBranchName)      
   
   SET @STAN = RIGHT(@RefNo,6)
   
   INSERT INTO t_ATM_TransferTransaction      
   (      
    ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
   )        
      
  VALUES      
   (       
    @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@OurBranchID,@DbAccount,@DbDesc,'G','GL',@DbCurrency, '1', @mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,@Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescription + ' STAN : ' + @STAN,@ATMID, @mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MCC,@TrxDesc,@MerchantType,@STAN
   ) 
       
    SELECT @Status=@@ERROR      
      
    IF @Status <> 0        
     BEGIN      
             
      RAISERROR('There Is An Error Occured While Creating Transaction In The Remote Branch',16,1) WITH SETERROR       
      RETURN (5000)      
     END      
    
   Set @mSerialNo = 2      
   Set @mTrxType = 'C'  
   
   SET @STAN = RIGHT(@RefNo,6)
     
   INSERT INTO t_ATM_TransferTransaction      
   (
    ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
   )        
      
  VALUES      
      
   (       
    @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@OurBranchID,@CrAccount,@CrDesc,'G','GL',@CrCurrency,'1',@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,@Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescription+' STAN : '+@STAN,@ATMID, @mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MCC,@TrxDesc,@MerchantType,@STAN
   )      
       
    SELECT @Status=@@ERROR      
      
    IF @Status <> 0        
     BEGIN      
             
      RAISERROR('There Is An Error Occured While Creating Transaction In The Remote Branch',16,1) WITH SETERROR       
      RETURN (5000)      
     END      
    
--Debit Transaction Switch Charges         
      
  SELECT 0 As Status  
  RETURN 0