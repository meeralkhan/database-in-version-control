﻿CREATE Procedure [dbo].[spc_AddEditTDRMarkLien]          
            
 (            
  @OurBranchID    varchar (30),            
  @AccountID   varchar(30)='',            
  @ReceiptID    varchar (30)='',            
  @IsLien    char(1),            
  @LienDate   datetime,            
  @Remarks   varchar(300)='',            
  @IsLienMarkInAccount  varchar(1)='',            
  @LienMarkAccountID  varchar(30)='',            
  @LienMarkAccountName  varchar(100)='',            
  @OperatorID   varchar(30)='',            
  @UnLienDate   datetime='01/01/1900',        
  @LienAmount money = 0,        
  @LienTerminal nvarchar(30) = '',        
  @LienID decimal(24,0) = 0        
 )            
            
AS            
            
  SET NOCOUNT ON          
            
 DECLARE @result AS varchar(MAX)          
           
 DECLARE @SerialNo AS INT            
 DECLARE @SerialID AS DECIMAL(24,0)        
 DECLARE @Amount as money            
 DECLARE @BalAmount as money          
 DECLARE @mDate as Varchar(50)          
            
 IF @IsLien='1'            
  BEGIN            
            
   SELECT @SerialNo=IsNull(Max(SerialID) ,0) + 1 FROM t_DepositLienHistory            
    WHERE OurBranchID=@OurBranchID AND ReceiptID=@ReceiptID AND AccountID=@AccountID          
            
 SELECT @SerialID=IsNull(Max(SerialNo) ,0) + 1 FROM t_TDRLienUnLien        
    WHERE OurBranchID=@OurBranchID AND RecieptID=@ReceiptID        
        
   Select @Amount=IsNull(Amount,0) from t_TDRIssuance          
    WHERE OurBranchID=@OurBranchID AND RecieptID=@ReceiptID AND AccountID=@AccountID          
            
   INSERT INTO t_DepositLienHistory            
       (OurBranchID, AccountID, ReceiptID, SerialID,LienDate, LienRemarks,IsLienMarkInAccount,LienMarkAccountID, LienMarkAccountName, LienMarkOperatorID)          
   VALUES            
    (@OurBranchID, @AccountID,@ReceiptID,@SerialNo, @LienDate,@Remarks, @IsLienMarkInAccount,@LienMarkAccountID, @LienMarkAccountName,@OperatorID)          
          
  select @mDate = Convert(varchar(10), WorkingDate,101) + ' ' + convert(varchar(20),GetDate(),114) from t_Last          
           
   INSERT INTO t_TDRLienUnLien            
       (OurBranchID, RecieptID, SerialNo, AccountID, IsLien, LienAmount, LienAccountID, LienRemarks, LienBy, LienTime, LienTerminal)        
   VALUES            
    (@OurBranchID, @ReceiptID, @SerialID, @AccountID, '1', @LienAmount, @LienMarkAccountID, @Remarks, @OperatorID, @mDate, @LienTerminal)          
               
   UPDATE t_TDRIssuance          
    SET IsUnderLien = @IsLien, TotalLienAmount = ISNULL(TotalLienAmount,0)+@LienAmount        
   WHERE OurBranchID=@OurBranchID AND RecieptID=@ReceiptID        
        
  Insert into t_AccountFreeze             
    (OurBranchID, AccountID, IsFreeze, Date, Amount, Comments, CreateBy, CreateTime, CreateTerminal, AuthStatus)             
   Values            
    (@OurBranchID, @AccountID, @isLien, @LienDate, @LienAmount, @Remarks, @OperatorID, @mDate, '', '')                
            
   Update t_AccountBalance            
    Set isFreezed=@isLien, FreezeAmount=isnull(FreezeAmount,0)+@LienAmount            
    WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID             
             
   select 'Ok' AS ReturnStatus;          
             
  END            
            
 ELSE            
  BEGIN            
           
   select @mDate = Convert(varchar(10), WorkingDate,101) + ' ' + convert(varchar(20),GetDate(),114) from t_Last          
       
   declare @tRecAmount decimal(21,3)    
   declare @tLienAmount decimal(21,3)    
   declare @uLienAmount decimal(21,3)    
    
   select @tRecAmount = Amount, @tLienAmount = ISNULL(TotalLienAmount,0), @uLienAmount = 0 from t_TDRIssuance    
   WHERE OurBranchID=@OurBranchID AND RecieptID=@ReceiptID AND AccountID=@AccountID    
    
   select @uLienAmount = ISNULL(LienAmount,0) from t_TDRLienUnLien    
   WHERE OurBranchID=@OurBranchID AND RecieptID=@ReceiptID AND AccountID=@AccountID    
    
   select @tLienAmount = @tLienAmount - @uLienAmount;    
    
   UPDATE t_TDRLienUnLien SET        
   IsLien = 0,        
   UnLienBy = @OperatorID,        
   UnLienTime = @mDate,        
   UnLienTerminal = @LienTerminal,        
   UnLienRemarks = @Remarks        
   WHERE OurBranchID = @OurBranchID        
   AND RecieptID = @ReceiptID        
   AND SerialNo = @LienID        
        
   UPDATE t_DepositLienHistory            
    SET UnLienDate=@UnLienDate, UnLienRemarks=@Remarks, UnLienOperatorID=@OperatorID            
    WHERE OurBranchID=@OurBranchID AND ReceiptID=@ReceiptID AND AccountID=@AccountID           
        
 declare @IsUnderLien int        
 if @tLienAmount <= 0        
    set @IsUnderLien = 0        
 else        
    set @IsUnderLien = 1        
        
 UPDATE t_TDRIssuance          
     SET IsUnderLien = @IsUnderLien, TotalLienAmount = @tLienAmount        
    WHERE OurBranchID=@OurBranchID AND RecieptID=@ReceiptID AND AccountID=@AccountID    
    
   Select @BalAmount=isnull(FreezeAmount,0) from t_AccountBalance               
    WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID             
            
   Update TOP (1) t_AccountFreeze            
    Set UnfreezedDate=@UnLienDate, IsFreeze=0, ReleaseComments = @Remarks            
    WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID and Amount=@uLienAmount and IsFreeze=1            
            
   if @BalAmount-@uLienAmount <= 0             
    set @isLien='0'            
   else            
    set @isLien='1'            
            
   Update t_AccountBalance            
    Set isFreezed=@isLien, FreezeAmount=@BalAmount-@uLienAmount    
    WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID             
             
   select 'Ok' AS ReturnStatus;          
             
  END