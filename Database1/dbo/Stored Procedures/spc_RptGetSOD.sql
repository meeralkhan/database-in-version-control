﻿CREATE PROCEDURE [dbo].[spc_RptGetSOD]
(
  @OurBranchID varchar(30)
)
AS

SET NOCOUNT ON

SELECT A.ClientID, C.Name AS ClientName, L.AccountID, A.Name AS AccountName, L.SanctionDate, L.AccountLimit as LimitAmount,
L.DrawingPower, L.ExpDate, R.ChangeDate, R.Limit1, R.Rate1, R.Limit2, R.Rate2, R.Limit3, R.Rate3, AAS.ValueOfSecurity,
AAS.Margin, S.[Description] AS SecurityDescription, ST.[Description] AS SecurityType, L.IsBlocked, L.IsCancelled, P.CurrencyID,
AB.ClearBalance + AB.Effects AS ClearBalance, L.OurBranchID, AB.Limit, L.AccountID AS LimitAccountID
--v_RptGetDailyChanges.DailyChange,
--v_RptGetDailyChanges.DailyLocalChange,
--v_RptGetDailyChanges.PreviousBalance,
--v_RptGetDailyChanges.PreviousLocalBalance

FROM t_Account A
/* INNER JOIN v_RptGetDailyChanges ON A.AccountID = v_RptGetDailyChanges.AccountID */
INNER JOIN t_AccountBalance AB ON A.OurBranchID = AB.OurBranchID AND A.AccountID = AB.AccountID
INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND A.ProductID = P.ProductID         
INNER JOIN t_Customer C ON A.OurBranchID = C.OurBranchID AND A.ClientID = C.ClientID         
INNER JOIN t_Adv_Account_LimitMaintain L ON A.OurBranchID = L.OurBranchID AND A.AccountID = L.AccountID        
INNER JOIN t_Securities S         
INNER JOIN t_Adv_Account_Security AAS ON S.OurBranchID = AAS.OurBranchID AND S.SecurityID = AAS.SecurityID ON L.OurBranchID = AAS.OurBranchID AND L.AccountID = AAS.AccountID         
INNER JOIN t_Rate001 R ON L.OurBranchID = R.OurBranchID AND L.AccountID = R.AccountID        
INNER JOIN t_SecurityTypes ST ON S.OurBranchID = ST.OurBranchID AND S.SecurityTypeID = ST.SecurityTypeId

WHERE A.OurBranchID = @OurBranchID AND (AB.Limit <> 0) AND (AB.ClearBalance + AB.Effects < 0)