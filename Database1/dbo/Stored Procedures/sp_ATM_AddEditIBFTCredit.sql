﻿CREATE PROCEDURE [dbo].[sp_ATM_AddEditIBFTCredit]
(  
 @OurBranchID            varchar(30),   
 @WithdrawlBranchID      varchar(30)='',
 @CustomerBranchID       varchar(30),   
 @CustomerBranchID2      varchar(30),   
 @ATMID                  varchar(30),   
 @AccountID              varchar(30),  
 @AccountID2             varchar(30),  
 @Amount                 money,  
 @USDAmount              money=0,  
 @OtherCurrencyAmount    money=0,  
 @USDRate                money=0,  
 @Supervision            char(1),   
 @RefNo                  varchar(36),  
 @PHXDate                datetime,  
 @MerchantType           varchar(30),  
 @OurIMD                 varchar(30),  
 @AckInstIDCode          varchar(30),  
 @Currency               varchar(30),  
 @NameLocation           varchar(40),  
 @MCC                    varchar(30),
 @TrxDesc                varchar(30)='',
 @NewRecord              bit=1  
)
AS
BEGIN

 DECLARE @BankShortName as varchar(30)  
  SET @BankShortName = 'N/A'  
 DECLARE @mScrollNo as int  
 DECLARE @mSerialNo as smallint  
  SET @mSerialNo = 0
 DECLARE @mAccountID as varchar(30)  
 DECLARE @mAccountName as varchar(50)  
 DECLARE @mAccountType as char  
 DECLARE @mProductID as varchar(30)  
 DECLARE @mCurrencyID as varchar(30)  
 DECLARE @mIsLocalCurrency as bit  
  SET @mIsLocalCurrency=1   
 DECLARE @mwDate as datetime  
 DECLARE @mTrxType as char  
 DECLARE @STAN as varchar(30)
 DECLARE @mGLID as varchar(30)  
  SET @mGLID = ''   
 DECLARE @mForeignAmount as money  
  SET @mForeignAmount=0   
 DECLARE @mExchangeRate as money  
  SET @mExchangeRate=1   
 DECLARE @mDescriptionID as varchar(3)  
  SET @mDescriptionID='000'   
 DECLARE @mDescription as varchar(255)  
 DECLARE @mDescriptionH as varchar(255)
 DECLARE @mRemarks as varchar(200)  
 DECLARE @mBankCode as varchar(30)  
  SET @mBankCode='1279'
 DECLARE @mOperatorID as varchar(30)  
  SET @mOperatorID = 'ATM-' + @ATMID  
 DECLARE @mSupervisorID as varchar(30)  
  SET @mSupervisorID = @OurIMD   
 
 --Checking Variables
 DECLARE @vCount1 as int  
 DECLARE @vCount4 as bit  
 DECLARE @vClearBalance as money  
 DECLARE @vClearBalance2 as money  
 DECLARE @vClearedEffects as money  
 DECLARE @vFreezeAmount as money  
 DECLARE @vMinBalance as money  
 DECLARE @vIsClose as char  
 DECLARE @vAreCheckBooksAllowed as bit  
 DECLARE @vAllowCreditTransaction as bit  
 DECLARE @vBranchName as varchar(50)  
  SET @vBranchName = 'N/A'  
 DECLARE @Status as int
 DECLARE @mLastEOD as datetime  
 --DECLARE @mWorkingDate as datetime  
 DECLARE @mCONFRETI as datetime  
 DECLARE @mWithdrawlBranchName as varchar(50)
 DECLARE @Super as varchar(1)  
 DECLARE @AccountDigit as varchar(15)  
  SET @AccountDigit= 14
 DECLARE @VoucherID int
 DECLARE @IsCredit int
 DECLARE @GLControl nvarchar(30)
 
 SELECT @BankShortName = BankShortName From t_ATM_Banks WHERE (OurBranchID = @OurBranchID AND BankIMD = @OurIMD)
 
 IF @MerchantType <> '0000'
 BEGIN
  IF @Currency = '586'
  BEGIN
   SET @mDescription='INT.BK. FUND TRF. AT BK.' + @BankShortName  + ' (BR.' + @CustomerBranchID + ') TO : A/c # ' + @AccountID2
   SET @mDescriptionH='INT.BK. FUND TRF. AT BK.' + @BankShortName   + ' (BR.' + @CustomerBranchID + ') From : A/c # ' + @AccountID  
  END  
  ELSE   
  BEGIN
   IF @Currency = '840'
   BEGIN
    SET @mDescription='INT.BK. FUND TRF. ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') AT ' + @NameLocation   
	SET @mDescriptionH='INT.BK. FUND TRF. ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') AT ' +  + @NameLocation + ' From : A/c # ' + @AccountID  
   END  
   ELSE   
   BEGIN  
    SET @mDescription='INT.BK. FUND TRF.(' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) AT ' + @NameLocation   
	SET @mDescriptionH='INT.BK. FUND TRF. (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) AT ' +  + @NameLocation + ' From : A/c # ' + @AccountID  
   END  
  END
 END
 ELSE   
 BEGIN 
  SET @mDescription='IB(' + @ATMID + ') FUND TRF AT BK.' + @BankShortName  + ' (BR.' + @WithdrawlBranchID + ') TO : A/c # ' + @AccountID2  
  SET @mDescriptionH='ATM (' + @ATMID + ') FUND TRF AT BK.' + @BankShortName   + ' (BR.' + @CustomerBranchID + ') From : A/c # ' + @AccountID  
 END
  
 SET NOCOUNT ON  
  
 SELECT @Status = -1  
 ---------------------
 
 SELECT @vCount1=count(*),@Super=Supervision FROM t_ATM_TransferTransaction 
 WHERE (OurBranchID = @OurBranchID) AND (AccountID=@AccountID2) AND (Refno=@Refno) AND (AtmID=@ATmID) GROUP BY Supervision
 
 IF @vCount1=1 and @Super='C'
 BEGIN  
     
  SELECT @Status=10  --DUP_TRAN  
  SELECT @Status  as Status  
  RETURN (1)   
 END  
 
 IF @vCount1=1 and @Super='R'  
 BEGIN  
   
  SELECT @Status=35  --ORIG_ALREADY_REVERSED  
  SELECT @Status  as Status  
  RETURN (1)   
 END  
 
 IF len(@AccountID2) <>  @Accountdigit
 BEGIN
     
  SELECT @Status=9 --FLD Error  
  SELECT @Status  as Status  
  RETURN (1)   
 END  
 
 IF @Amount<=0  
 BEGIN  
     
  SELECT @Status=48 --BAD AMOUNT  
  SELECT @Status  as Status  
  RETURN (1)   
 END
 
 SELECT @mLastEOD=LastEOD,@mwDate=WorkingDate,@mCONFRETI=CONFRETI FROM t_Last WHERE (OurBranchID = @OurBranchID)
 
 IF @mLastEOD = @mwDate or @mCONFRETI = '1/1/9999'  
 BEGIN
     
  SELECT @Status=9  --INVALID_DATE  
  SELECT @Status  as Status  
  RETURN (1)   
 END
 
 SELECT @mScrollNo=(ISNULL(MAX(ScrollNo),0)+1) FROM t_ATM_TransferTransaction 
 WHERE (OurBranchID = @OurBranchID) and (convert(varchar(10), wDate, 120) = convert(varchar(10), @mwDate, 120))
 
 SELECT @vCount1=count(*) From t_Account WHERE (OurBranchID = @OurBranchID) AND (AccountID = @AccountID2)
 
 IF @vCount1=0
 BEGIN  
     
  SELECT @Status=2  --INVALID_ACCOUNT  
  SELECT @Status  as Status  
  RETURN (1)   
 END
 
 SELECT @vBranchName=Heading2 FROM t_GlobalVariables WHERE (OurBranchID = @OurBranchID)
 SELECT @vCount4=count(*) From t_ATM_Branches WHERE OurBranchID = @OurBranchID AND BranchID = @WithdrawlBranchID  
 
 SELECT @mAccountID=IssuerAccountID,@mAccountName=IssuerAccountTitle
 FROM t_ATM_SwitchCharges WHERE (OurBranchID = @OurBranchID AND MerchantType = @MerchantType)
 
 SET @mSerialNo = @mSerialNo + 1  
 SET @mAccountType='G'  
 SET @mProductid ='GL'  
 SET @mCurrencyID='PKR'  
 SET @mTrxType = 'D'  
 SET @IsCredit = 0
 
 SELECT @VoucherID = ISNULL(MAX(VoucherID),0) + 1 from t_GLTransactions where OurBranchID = @OurBranchID
 
 IF @MerchantType <> '0000'
 BEGIN  
  IF @Currency = '586'  
  BEGIN  
   SET @mRemarks='Being Amount of INT.BK. Fund TRF Transaction From : ' + @CustomerBranchID + ' By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)  
  END       
  ELSE   
  BEGIN
   IF @Currency = '840'
   BEGIN  
    SET @mRemarks='Being Amount of INT.BK. Fund TRF Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)  
   END  
   ELSE   
   BEGIN  
    SET @mRemarks='Being Amount of INT.BK. Fund TRF Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) From : ' + @NameLocation + ' By  A/c # ' + @AccountID +  ' At : '  + upper(@vBranchName)
   END  
  END  
 END
 ELSE  
 BEGIN
  IF @vCount4 > 0
  BEGIN  
   SET @mWithdrawlBranchName = 'N/A'  
   SELECT @mWithdrawlBranchName=BranchName FROM t_ATM_Branches WHERE OurBranchID = @OurBranchID AND BranchID = @WithdrawlBranchID
   SET @mRemarks='Being Amount of INT.BK. Fund TRF Transaction From : BK.' + @BankShortName +  ' (' + upper(@mWithdrawlBranchName) + ') By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)
  END  
  ELSE  
  BEGIN  
   SET @mRemarks='Being Amount of IB (' + @ATMID+ ') INT.BK. Fund TRF Transaction From : BK.' + @BankShortName +  ' (BR.' +  @WithdrawlBranchID + ') By  A/c # ' + @AccountID +  ' At : '  + upper(@vBranchName)  
  END  
 END
 
 SET @STAN = RIGHT(@RefNo,6)
 
 IF @MerchantType <> '0000'
 BEGIN
  INSERT INTO t_ATM_TransferTransaction 
  (
   ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
   IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
   [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
  )
  VALUES
  (
   @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@mAccountID,@mAccountName,@mAccountType,@mProductID,
   @mCurrencyID,@mIsLocalCurrency, @mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,
   @Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescription+' STAN: '+@STAN,@ATMID,@mBankCode,
   @mRemarks,@mOperatorID,@mSupervisorID,@MCC,@TrxDesc,@MerchantType,@STAN
  )
 END
 ELSE
 BEGIN
  INSERT INTO t_ATM_TransferTransaction
  (
   ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
   IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
   [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
  )
  VALUES
  (
   @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@mAccountID,@mAccountName,@mAccountType,@mProductID,
   @mCurrencyID,@mIsLocalCurrency, @mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,
   @Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescription+' STAN: '+@STAN,@ATMID,@mBankCode,
   @mRemarks,@mOperatorID,@mSupervisorID,@MCC,@TrxDesc,@MerchantType,@STAN
  )
 END
 
 INSERT INTO t_GLTransactions 
 (
  OurBranchID,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,
  ExchangeRate,IsCredit,TransactionType,TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,
  IsMainTrx,DocType,Remarks,TrxDesc,MerchantType,STAN
 )
 VALUES 
 (
  @OurBranchID,@mAccountID,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@mwDate,
  @mDescriptionID,@mDescription+' STAN: '+@STAN,@mCurrencyID,@Amount,@mForeignAmount,@mExchangeRate,@IsCredit,'ATMTransfer',
  'L',@mOperatorID,@mSupervisorID,'',@mScrollNo,@mSerialNo,null,'1','AT',@mRemarks,@TrxDesc,@MerchantType,@STAN 
 )
 
 SELECT @Status=@@ERROR
  
 IF @Status <> 0   
 BEGIN  
     
  RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR  
  RETURN (5000)  
 END
 
 SELECT @mAccountName=B.Name,@mProductID=B.ProductID,@mCurrencyID=P.CurrencyID,@mAccountType='C',@vClearBalance=B.ClearBalance,
 @vClearedEffects=B.ClearedEffects,@vFreezeAmount=B.FreezeAmount,@vMinBalance=P.MinBalance,@vIsClose=A.[Status],
 @vAreCheckBooksAllowed=P.AreCheckBooksAllowed,@vAllowCreditTransaction=ISNULL(A.AllowCreditTransaction,'0')
 FROM t_AccountBalance B 
 INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND B.ProductID = A.ProductID 
 INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND A.ProductID = P.ProductID 
 WHERE (A.OurBranchID = @OurBranchID) AND (A.AccountID = @AccountID2)
 
 IF @vIsClose = 'C' or @vIsClose = 'T' or @vIsClose = 'X' or @VIsClose = 'D' or @vAllowCreditTransaction = 1
 BEGIN  
     
  SELECT @Status=3  --INACTIVE/CLOSE_ACCOUNT  
  SELECT @Status  as Status  
  RETURN (1)   
 END
 
 IF @vAreCheckBooksAllowed = 0  
 BEGIN  
     
  SELECT @Status=21  --INVALID_PRODUCT  
  SELECT @Status  as Status  
  RETURN (1)   
 END  
 
 IF @mCurrencyID <> 'PKR'
 BEGIN  
     
  SELECT @Status=19  --INVALID_CURRENCY  
  SELECT @Status  as Status  
  RETURN (1)   
 END  
 
 --Credit Transaction
 SET @mSerialNo = @mSerialNo + 1  
 SET @mTrxType = 'C'
 SET @IsCredit = 1
 
 IF @MerchantType <> '0000'   
 BEGIN  
  IF @Currency = '586'  
  BEGIN  
   SET @mRemarks='Being Amount of INT.BK. Fund TRF Transaction From : ' + @CustomerBranchID + ' By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)  
  END      
  ELSE   
  BEGIN
   IF @Currency = '840'  
   BEGIN  
    SET @mRemarks='Being Amount of INT.BK. Fund TRF Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)  
   END  
   ELSE   
   BEGIN  
    SET @mRemarks='Being Amount of INT.BK. Fund TRF Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) From : ' + @NameLocation + ' By  A/c # ' + @AccountID +  ' At : '  + upper(@vBranchName)
   END  
  END  
 END
 ELSE  
 BEGIN
  IF @vCount4 > 0
  BEGIN  
   SET @mWithdrawlBranchName = 'N/A'  
   SELECT @mWithdrawlBranchName=BranchName FROM t_ATM_Branches WHERE OurBranchID = @OurBranchID AND BranchID = @WithdrawlBranchID  
   
   SET @mRemarks='Being Amount of ATM (' + @ATMID+ ') INT.BK Fund TRF Transaction From : BK.' + @BankShortName +  ' (' + upper(@mWithdrawlBranchName) + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
  END  
  ELSE  
  BEGIN  
   SET @mRemarks='Being Amount of ATM (' + @ATMID+ ') INT.BK. Fund TRF Transaction From : BK.' + @BankShortName +  ' (BR.' +  @WithdrawlBranchID + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)
  END  
 END
 
 IF @MerchantType <> '0000'
 BEGIN
  INSERT INTO t_ATM_TransferTransaction
  (
   ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
   IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
   [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
  )
  VALUES
  (
   @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@AccountID2,@mAccountName,@mAccountType,@mProductID,
   @mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,
   @Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescriptionH+' STAN: '+@STAN,@ATMID,
   @mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MCC,@TrxDesc,@MerchantType,@STAN
  )  
 END
 ELSE
 BEGIN  
  INSERT INTO t_ATM_TransferTransaction
  (
   ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
   IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
   [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
  )
  VALUES
  (
   @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@AccountID2,@mAccountName,@mAccountType,@mProductID,
   @mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,
   @Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescriptionH+' STAN: '+@STAN,@ATMID,
   @mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MCC,@TrxDesc,@MerchantType,@STAN
  )
 END
 
 select @GLControl = ISNULL(GLControl, '') from t_Products where OurBranchID = @OurBranchID and ProductID = @mProductID
    
 INSERT INTO t_Transactions 
 ( 
  OurBranchID,ScrollNo,SerialNo,RefNo,wDate,AccountType,DocumentType,AccountID,AccountName,ProductID,CurrencyID,ValueDate,
  TrxType,ChequeID,ChequeDate,Amount,ForeignAmount,ExchangeRate,ProfitLoss,MeanRate,DescriptionID,[Description],BankCode,
  BranchCode,TrxPrinted,GLID,[Status],IsLocalCurrency,OperatorID,SupervisorID,AdditionalData,IsMainTrx,DocType,GLVoucherID,
  Remarks,TrxDesc,MerchantType,STAN 
 )
 VALUES 
 ( 
  @OurBranchID,@mScrollNo,@mSerialNo,@RefNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@mAccountType,'AT',
  @AccountID2,@mAccountName,@mProductID,@mCurrencyID,@mwDate,@mTrxType,'',@mwDate,@Amount,@mForeignAmount,@mExchangeRate,0,
  @mExchangeRate,@mDescriptionID,@mDescriptionH+' STAN: '+@STAN,@mBankCode,@WithdrawlBranchID,0,null,@Supervision,
  @mIsLocalCurrency,@mOperatorID,@mSupervisorID,null,'1','AT',@VoucherID,@mRemarks,@TrxDesc,@MerchantType,@STAN 
 )
    
 INSERT INTO t_GLTransactions 
 ( 
  OurBranchID,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,
  ExchangeRate,IsCredit,TransactionType,TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,
  IsMainTrx,DocType,Remarks,TrxDesc,MerchantType,STAN 
 )
 
 VALUES
 ( 
  @OurBranchID,@GLControl,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@mwDate,@mDescriptionID,
  @mDescriptionH+' STAN: '+@STAN,@mCurrencyID,@Amount,@mForeignAmount,@mExchangeRate,@IsCredit,'ATMTransfer','L',@mOperatorID,
  @mSupervisorID,'',@mScrollNo,@mSerialNo,null,'0','AT',@mRemarks,@TrxDesc,@MerchantType,@STAN 
 )
  
 SELECT @Status=@@ERROR
  
 IF @Status <> 0   
 BEGIN  
     
  RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR  
  RETURN (5000)  
 END
 
 SELECT @Status = 0, 
 @vClearBalance2 = ((IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0))+IsNull(@Amount,0))*100
 SELECT @Status As Status , @vClearBalance2 As ClearBalance  
 RETURN 0   
  
    
END