﻿CREATE PROCEDURE [dbo].[sp_GenerateReportsCreditRisk_ChkData]
(
@CIF Varchar(15),
@OurBrID VARCHAR(30),
@sdate Varchar(50),
@edate Varchar(50),
@GroupCIF varchar(1)				 
)

AS
BEGIN
	Set nocount on

	DECLARE @ClientID varchar(15)  DECLARE @OurBranchID VARCHAR(30)	  DECLARE @cCount as decimal(24,0)     declare @sCount as decimal(24,0)		DECLARE @Counter as INT		DECLARE @ClearBalance AS MONEY
	DECLARE @ssDate datetime 	DECLARE @eeDate datetime		DECLARE @GroupID VARCHAR(50)	DECLARE @AccountID VARCHAR(30)	DECLARE @DueDate AS DATETIME	DECLARE @DealID AS VARCHAR	DECLARE @SerialID AS VARCHAR
    DECLARE @Flag AS BIT	DECLARE @DPD AS NUMERIC		DECLARE @CurrentDPD AS NUMERIC		DECLARE @PeakDPD AS NUMERIC		DECLARE @Bal AS MONEY	DECLARE @ABHBal AS MONEY	DECLARE @SettleDate AS DATETIME	DECLARE @Limit AS MONEY		DECLARE @MaxClearBalance AS MONEY
	DECLARE @SHName AS VARCHAR(50)		DECLARE @SHCountry AS VARCHAR(30)		DECLARE @ABHBal1 AS MONEY	DECLARE @ABHBalOld AS MONEY DECLARE @ABHBal2 AS MONEY	DECLARE @ABHCount as INT
	DECLARE @PeakUtilization12M AS MONEY	DECLARE @PeakUtilizationYTD AS MONEY	DECLARE @PeakUtilizationDTD AS MONEY	DECLARE @TotDPD AS NUMERIC	DECLARE @SHParentID as decimal(24,0)
	DECLARE @AvgDailyUtilization12M AS MONEY	DECLARE @AvgDailyUtilizationYTD AS MONEY	DECLARE @AvgDailyUtilizationDTD AS MONEY					DECLARE @SHID as decimal(24,0)
	DECLARE @OverDraftClassID VARCHAR(30)		DECLARE @Classification VARCHAR(30)			DECLARE @SubClassification VARCHAR(30)						DECLARE @SHOwnerShip AS DECIMAL(22,2)
	DECLARE @RiskCode VARCHAR(30)				DECLARE @RiskDescription VARCHAR(100)		DECLARE @SHcCount as decimal(24,0)     declare @SHsCount as decimal(24,0)	
	DECLARE @YTDDate DATETIME	DECLARE @M12TDDate DATETIME		DECLARE @ccCount as decimal(24,0)     declare @ssCount as decimal(24,0)		DECLARE @SanctionDate DATETIME
	DECLARE	@PeakDTDABHBal AS MONEY		DECLARE @PeakDTDBal AS MONEY		DECLARE @PeakDTDCounter AS DECIMAL(24,0)
	DECLARE	@Peak12MABHBal AS MONEY		DECLARE @Peak12MBal AS MONEY		DECLARE @Peak12MCounter AS DECIMAL(24,0)
	DECLARE	@PeakYTDABHBal AS MONEY		DECLARE @PeakYTDBal AS MONEY		DECLARE @PeakYTDCounter AS DECIMAL(24,0)
	DECLARE @NoOfReschedule AS INT		DECLARE @LastRescheduleDate AS DATETIME		DECLARE @Provisioning as MONEY		--decimal(18,6)
	declare @Chk varchar(15)

	SET @M12TDDate = convert(varchar(10), DATEADD(d,1,DATEADD(yy,-1,@edate)), 120)
	SET @YTDDate = convert(varchar(10), DATEADD(yy, DATEDIFF(yy, 0, @edate), 0), 120)

	SET @ssDate = convert(varchar(10), @sdate, 120)
	SET @eeDate = convert(varchar(10), @edate, 120)
	SET @ClientID = @CIF
	SET @OurBranchID = @OurBrID

	SET @Chk = NULL
	Set @Counter = 0
	
	SELECT @GroupID=ISNULL(GroupID,'') FROM dbo.t_Customer WHERE ClientID = @ClientID AND OurBranchID = @OurBranchID

	IF @GroupCIF = '1'
	BEGIN

		SELECT Top 1 @Chk = c.ClientID FROM dbo.t_Customer c
		INNER JOIN t_Account a ON a.OurBranchID = c.OurBranchID AND a.ClientID = c.ClientID
		INNER JOIN dbo.vc_AccountBalanceHistory ab ON ab.AccountID = a.AccountID
		INNER JOIN dbo.t_Adv_PaymentSchedule p ON p.OurBranchID = a.OurBranchID AND p.AccountID = a.AccountID and DueDate <= @eeDate
		INNER JOIN t_Disbursement d ON a.AccountID = d.AccountID AND d.OurBranchID = a.OurBranchID AND d.DealID = p.DealID AND d.DisbursementDate <= @eeDate
		INNER JOIN dbo.t_RiskClassification rc ON rc.OurBranchID = d.OurBranchID AND rc.Code = d.RiskCode
		INNER JOIN t_FacilityProduct l ON l.OurBranchID = a.OurBranchID AND l.FacilityID = d.DealID AND c.ClientID = l.ClientID  AND a.ProductID = l.ProductID
		LEFT JOIN t_LinkCIFCollateral cc ON cc.OurBranchID = l.OurBranchID AND cc.ClientID = l.ClientID AND cc.SerialNo = l.FacilityID		
		LEFT JOIN dbo.t_RelationshipOfficer r ON r.OurBranchID = c.OurBranchID AND r.OfficerID = c.RManager
		LEFT JOIN dbo.t_ManageGroups mg ON mg.OurBranchID = c.OurBranchID AND mg.GroupID = c.GroupID
		WHERE c.OurBranchID = @OurBranchID AND c.ClientID = @ClientID AND ab.wDate = @eeDate 
		--AND ((p.IsPosted = 0 AND p.DueDate <= @eeDate) OR (p.IsPosted = 1 AND (p.PayDate > p.DueDate AND p.PayDate BETWEEN @ssDate AND @eeDate))) 

		IF @Chk is not NULL
		BEGIN
			SET @Counter = @Counter + 1
			SET @Chk = NULL
		END
		
		SELECT Top 1 @Chk = c.ClientID FROM dbo.t_LinkFacilityAccount l
		INNER JOIN dbo.t_Account a ON a.AccountID = l.AccountID AND l.OurBranchID = a.OurBranchID
		INNER JOIN dbo.t_Customer c ON c.ClientID = a.ClientID AND c.OurBranchID = a.OurBranchID
		INNER JOIN dbo.vc_AccountBalanceHistory ab ON ab.AccountID = l.AccountID
		LEFT JOIN dbo.t_LinkCIFCollateral cc ON cc.OurBranchID = l.OurBranchID AND cc.ClientID = l.ClientID AND cc.SerialNo = l.FacilityID
		LEFT join dbo.t_RelationshipOfficer r ON r.OurBranchID = c.OurBranchID AND r.OfficerID = c.RManager
		LEFT JOIN dbo.t_ManageGroups mg ON mg.OurBranchID = c.OurBranchID AND mg.GroupID = c.GroupID
		WHERE c.OurBranchID = @OurBranchID AND c.ClientID = @ClientID AND l.AccountID NOT IN (SELECT AccountID FROM dbo.t_Disbursement) --AND l.EffectiveDate <= @eeDate
		AND a.ProductID IN (SELECT ProductID FROM dbo.t_Products WHERE ProductType IN ('AE')) AND ab.wDate = @eeDate AND l.EffectiveDate <= @eeDate

		IF @Chk is not NULL
		BEGIN
			SET @Counter = @Counter + 1
			SET @Chk = NULL
		END

	END --- @GROUPCIF	
	IF @GroupCIF = '0'
	BEGIN
		SELECT Top 1 @Chk = c.ClientID 	FROM dbo.t_Customer c
		INNER JOIN t_Account a ON a.OurBranchID = c.OurBranchID AND a.ClientID = c.ClientID
		INNER JOIN dbo.vc_AccountBalanceHistory ab ON ab.AccountID = a.AccountID
		INNER JOIN dbo.t_Adv_PaymentSchedule p ON p.OurBranchID = a.OurBranchID AND p.AccountID = a.AccountID and DueDate <= @eeDate
		INNER JOIN t_Disbursement d ON a.AccountID = d.AccountID AND d.OurBranchID = a.OurBranchID AND d.DealID = p.DealID AND d.DisbursementDate <= @eeDate
		INNER JOIN dbo.t_RiskClassification rc ON rc.OurBranchID = d.OurBranchID AND rc.Code = d.RiskCode
		INNER JOIN t_FacilityProduct l ON l.OurBranchID = a.OurBranchID AND l.FacilityID = d.DealID AND c.ClientID = l.ClientID  AND a.ProductID = l.ProductID
		LEFT JOIN t_LinkCIFCollateral cc ON cc.OurBranchID = l.OurBranchID AND cc.ClientID = l.ClientID AND cc.SerialNo = l.FacilityID
		LEFT JOIN dbo.t_RelationshipOfficer r ON r.OurBranchID = c.OurBranchID AND r.OfficerID = c.RManager
		LEFT JOIN dbo.t_ManageGroups mg ON mg.OurBranchID = c.OurBranchID AND mg.GroupID = c.GroupID
		WHERE c.OurBranchID = @OurBranchID AND ab.wDate = @eeDate AND (((p.IsPosted = 0 AND p.DueDate <= @eeDate) OR 
		(p.IsPosted = 1 AND (p.PayDate > p.DueDate AND p.PayDate BETWEEN @ssDate AND @eeDate))) OR d.RiskCode <> '0001') 
		
		IF @Chk is not NULL
		BEGIN
			SET @Counter = @Counter + 1
			SET @Chk = NULL
		END
		
		SELECT Top 1 @Chk = c.ClientID FROM dbo.t_LinkFacilityAccount l
		INNER JOIN dbo.t_Account a ON a.AccountID = l.AccountID AND l.OurBranchID = a.OurBranchID
		INNER JOIN dbo.t_Customer c ON c.ClientID = a.ClientID AND c.OurBranchID = a.OurBranchID
		INNER JOIN dbo.vc_AccountBalanceHistory ab ON ab.AccountID = l.AccountID
		INNER JOIN dbo.t_OverdraftClassifications oc ON oc.OverdraftClassID = ISNULL(a.OverdraftClassID,'001')
		LEFT JOIN dbo.t_LinkCIFCollateral cc ON cc.OurBranchID = l.OurBranchID AND cc.ClientID = l.ClientID AND cc.SerialNo = l.FacilityID
		LEFT join dbo.t_RelationshipOfficer r ON r.OurBranchID = c.OurBranchID AND r.OfficerID = c.RManager
		LEFT JOIN dbo.t_ManageGroups mg ON mg.OurBranchID = c.OurBranchID AND mg.GroupID = c.GroupID
		WHERE c.OurBranchID = @OurBranchID AND l.AccountID NOT IN (SELECT AccountID FROM dbo.t_Disbursement) AND l.EffectiveDate <= @eeDate
		AND a.ProductID IN (SELECT ProductID FROM dbo.t_Products WHERE ProductType IN ('AE')) AND ab.wDate = @eeDate AND (ab.limit + ab.ClearBalance < 0 OR 
		(Select Top 1 isnull(OverdraftClassID,'001') from ODClassifiedAccounts where WorkingDate <= @eeDate AND AccountID = a.AccountID Order By WorkingDate desc) <> '001')

		IF @Chk is not NULL
		BEGIN
			SET @Counter = @Counter + 1
			SET @Chk = NULL
		END
	END
	
	IF @GroupCIF = ''
	BEGIN
		SELECT Top 1 @Chk = c.ClientID 	FROM dbo.t_Customer c
		INNER JOIN t_Account a ON a.OurBranchID = c.OurBranchID AND a.ClientID = c.ClientID
		INNER JOIN dbo.vc_AccountBalanceHistory ab ON ab.AccountID = a.AccountID
		INNER JOIN dbo.t_Adv_PaymentSchedule p ON p.OurBranchID = a.OurBranchID AND p.AccountID = a.AccountID and DueDate <= @eeDate
		INNER JOIN t_Disbursement d ON a.AccountID = d.AccountID AND d.OurBranchID = a.OurBranchID AND d.DealID = p.DealID AND d.DisbursementDate <= @eeDate
		INNER JOIN dbo.t_RiskClassification rc ON rc.OurBranchID = d.OurBranchID AND rc.Code = d.RiskCode
		INNER JOIN t_FacilityProduct l ON l.OurBranchID = a.OurBranchID AND l.FacilityID = d.DealID AND c.ClientID = l.ClientID  AND a.ProductID = l.ProductID
		LEFT JOIN t_LinkCIFCollateral cc ON cc.OurBranchID = l.OurBranchID AND cc.ClientID = l.ClientID AND cc.SerialNo = l.FacilityID
		LEFT JOIN dbo.t_RelationshipOfficer r ON r.OurBranchID = c.OurBranchID AND r.OfficerID = c.RManager
		LEFT JOIN dbo.t_ManageGroups mg ON mg.OurBranchID = c.OurBranchID AND mg.GroupID = c.GroupID
		WHERE c.OurBranchID = @OurBranchID AND ab.wDate = @eeDate AND ab.ClearBalance < 0
		--AND (((p.IsPosted = 0 AND p.DueDate <= @eeDate) OR (p.IsPosted = 1 AND (p.PayDate > p.DueDate AND p.PayDate BETWEEN @ssDate AND @eeDate))) OR d.RiskCode <> '0001')

		IF @Chk is not NULL
		BEGIN
			SET @Counter = @Counter + 1
			SET @Chk = NULL
		END
		
		SELECT Top 1 @Chk = c.ClientID  FROM dbo.t_LinkFacilityAccount l
		INNER JOIN dbo.t_Account a ON a.AccountID = l.AccountID AND l.OurBranchID = a.OurBranchID
		INNER JOIN dbo.t_Customer c ON c.ClientID = a.ClientID AND c.OurBranchID = a.OurBranchID
		INNER JOIN dbo.vc_AccountBalanceHistory ab ON ab.AccountID = l.AccountID
		INNER JOIN dbo.t_OverdraftClassifications oc ON oc.OverdraftClassID = ISNULL(a.OverdraftClassID,'001')
		LEFT JOIN dbo.t_LinkCIFCollateral cc ON cc.OurBranchID = l.OurBranchID AND cc.ClientID = l.ClientID AND cc.SerialNo = l.FacilityID
		LEFT join dbo.t_RelationshipOfficer r ON r.OurBranchID = c.OurBranchID AND r.OfficerID = c.RManager
		LEFT JOIN dbo.t_ManageGroups mg ON mg.OurBranchID = c.OurBranchID AND mg.GroupID = c.GroupID
		WHERE c.OurBranchID = @OurBranchID AND l.AccountID NOT IN (SELECT AccountID FROM dbo.t_Disbursement) AND l.EffectiveDate <= @eeDate
		AND a.ProductID IN (SELECT ProductID FROM dbo.t_Products WHERE ProductType IN ('AE')) AND ab.wDate = @eeDate AND ab.ClearBalance < 0
		--(ab.limit + ab.ClearBalance < 0 OR (Select Top 1 isnull(OverdraftClassID,'001') from ODClassifiedAccounts where WorkingDate <= @eeDate AND AccountID = a.AccountID Order By WorkingDate desc) <> '001')

		IF @Chk is not NULL
		BEGIN
			SET @Counter = @Counter + 1
			SET @Chk = NULL
		END
	END
	
	select @Counter as abc
	
END