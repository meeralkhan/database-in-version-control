﻿CREATE PROCEDURE [dbo].[spc_GetCashierBalances]      
(      
  @OurBranchID varchar(4)      
)        
AS      
      
set nocount on      
      
Declare @DrAmount money      
Declare @CrAmount money      
Declare @Amount money      
Declare @Accountid varchar(30)      
      
DECLARE Main_Cursor CURSOR FOR      
      
SELECT OurBranchid, Accountid=GLId from t_Cashiermulticurrency where OurBranchid=@OurBranchid      
      
OPEN Main_Cursor      
      
FETCH NEXT FROM Main_Cursor INTO @OurBranchid,@Accountid      
      
WHILE (@@FETCH_STATUS = 0)      
BEGIN      
        
  select @DrAmount = 0      
  select @CrAmount = 0      
        
  select @DrAmount=amount from v_GetCashierBalances where  TrxType='D'      
  and OurBranchid=@OurBranchid and Accountid=@Accountid      
        
  select @CrAmount =amount from v_GetCashierBalances where  TrxType='C' and      
  OurBranchid=@OurBranchid and Accountid=@Accountid      
        
  select @Amount = @DrAmount - @CrAmount      
        
  if @Amount <> 0      
  Begin      
           
     select @Amount as Amount, c.OperatorID, c.CurrencyID, g.AccountID, g.Description      
     from t_CashierMultiCurrency c      
     INNER JOIN t_GL g ON c.OurBranchID = g.OurBranchID AND c.GLID = g.AccountID      
     WHERE c.OurBranchID = @OurBranchID and c.GLID = @Accountid      
           
     CLOSE Main_Cursor      
     DEALLOCATE Main_Cursor      
     return (1)      
  end      
FETCH NEXT FROM Main_Cursor INTO @OurBranchid,@Accountid      
END      
      
CLOSE Main_Cursor      
      
DEALLOCATE Main_Cursor      
      
select 0 as Amount, '' as AccountID