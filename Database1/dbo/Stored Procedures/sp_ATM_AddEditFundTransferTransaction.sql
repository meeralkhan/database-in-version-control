﻿CREATE PROCEDURE [dbo].[sp_ATM_AddEditFundTransferTransaction]
(  
 @OurBranchID              varchar(30),   
 @WithdrawlBranchID        varchar(30)='',   
 @CustomerBranchID         varchar(30),   
 @CustomerBranchID2        varchar(30)='',   
 @ATMID                    varchar(30),   
 @AccountID                varchar(30),  
 @AccountID2               varchar(30),  
 @Amount                   money,  
 @USDAmount                money=0,  
 @OtherCurrencyAmount      money=0,  
 @USDRate                  money=0,  
 @Supervision              char(1),   
 @RefNo                    varchar(50),  
 @PHXDate                  datetime,  
 @MerchantType             varchar(30),  
 @OurIMD                   varchar(30),  
 @AckInstIDCode            varchar(30),  
 @Currency                 varchar(30),  
 @NameLocation             varchar(100),  
 @MCC                      varchar(30),
 @TrxDesc                  varchar(30)='',
 @NewRecord                bit=1  
)      
AS  
BEGIN

 --This procedure is for Fund Transfer Transaction for Same Branch 

 DECLARE @BankShortName as varchar(30)  
  SET @BankShortName = 'N/A'
 DECLARE @mScrollNo as int  
 DECLARE @mSerialNo as smallint  
  SET @mSerialNo = 0
 DECLARE @mAccountID as varchar(30)  
 DECLARE @mAccountName as varchar(100)  
 DECLARE @mAccountType as char  
 DECLARE @mProductID as varchar(30)  
 DECLARE @mCurrencyID as varchar(30)  
 DECLARE @mAccountName2 as varchar(100)  
 DECLARE @mAccountType2 as char  
 DECLARE @mProductID2 as varchar(30)  
 DECLARE @mCurrencyID2 as varchar(30)
 DECLARE @mIsLocalCurrency as bit  
  SET @mIsLocalCurrency=1   
 DECLARE @mwDate as datetime  
 DECLARE @mTrxType as char  
 DECLARE @STAN as varchar(30)
 DECLARE @mGLID as varchar(30)  
  SET @mGLID = ''   
 DECLARE @mForeignAmount as money  
  SET @mForeignAmount=0   
 DECLARE @mExchangeRate as money  
  SET @mExchangeRate=1   
 DECLARE @mDescriptionID as varchar(30)  
  SET @mDescriptionID='000'   
 DECLARE @mDescription as varchar(255)
 DECLARE @mDescriptionCharges as varchar(255)
 DECLARE @mDescriptionH as varchar(255)  
 DECLARE @mDescriptionHCharges as varchar(255)  
 DECLARE @mRemarks as varchar(200)
 DECLARE @mBankCode as varchar(30)  
  SET @mBankCode='1279'
 DECLARE @mOperatorID as varchar(30)  
  SET @mOperatorID = 'ATM-' + @ATMID  
 DECLARE @mSupervisorID as varchar(30)  
  SET @mSupervisorID = @OurIMD   
 --Checking Variables  
 DECLARE @vCount1 as int  
 DECLARE @vCount2 as bit  
 DECLARE @vCount3 as bit  
 DECLARE @vCount4 as bit  
 DECLARE @vAccountID as varchar(30)  
 DECLARE @vClearBalance as money  
 DECLARE @vClearBalance2 as money  
 DECLARE @vClearBalance3 as money  
 DECLARE @vClearedEffects as money  
 DECLARE @vClearedEffects2 as money  
 DECLARE @vFreezeAmount as money  
 DECLARE @vMinBalance as money
 DECLARE @vFreezeAmount2 as money  
 DECLARE @vMinBalance2 as money  
 DECLARE @Limit as money  
 DECLARE @vIsClose as char  
 DECLARE @vIsClose2 as char  
 DECLARE @vAreCheckBooksAllowed as bit  
 DECLARE @vAreCheckBooksAllowed2 as bit  
 DECLARE @vAllowDebitTransaction as bit  
 DECLARE @vAllowDebitTransaction2 as bit  
 DECLARE @vBranchName as varchar(100)  
  SET @vBranchName = 'N/A'  
 DECLARE @Status as int
 DECLARE @mLastEOD as datetime  
 -- DECLARE @mWorkingDate as datetime  
 DECLARE @mCONFRETI as datetime  
 DECLARE @mWithdrawlBranchName as varchar(100)  
 DECLARE @IssuerAccountID as varchar(30)  
 DECLARE @IssuerAccountTitle as varchar(100)  
 DECLARE @IssuerChargesAccountID as varchar(30)  
 DECLARE @IssuerTitle as varchar(100)  
 DECLARE @mRemarksCharges as varchar(200)  
 DECLARE @Super as varchar(1)  
 DECLARE @AccountDigit as varchar(30)  
  set @AccountDigit=14
 DECLARE @ExciseDutyAccountID as varchar(30)  
 DECLARE @ExciseDutyPercentage as money  
 DECLARE @ExciseDutyAmount as money  
 DECLARE @FEDAccID as varchar(30)
 DECLARE @FEDAccTitle as varchar(100)
 DECLARE @FTCharges as money
  SET @ExciseDutyPercentage = 0
  SET @ExciseDutyAmount = 0
  SET @FTCharges = 0
 DECLARE @VoucherID int
 DECLARE @IsCredit int
 DECLARE @GLControl nvarchar(30)          
  
 SELECT @BankShortName = BankShortName From t_ATM_Banks WHERE (OurBranchID = @OurBranchID AND BankIMD = @OurIMD)
 
 SET @mDescription='ATM(' + @ATMID + ') FUND TRF AT BK.' + @BankShortName  + ' (BR.' + @WithdrawlBranchID + ') TO : A/c # ' + @AccountID2  
 SET @mDescriptionCharges='FUND TRF. AT BK.' + @BankShortName   + ' (BR.' + @CustomerBranchID + ') From : A/c # ' + @AccountID
 SET @mDescriptionH='ATM(' + @ATMID + ') FUND TRF AT BK.' + @BankShortName   + ' (BR.' + @WithdrawlBranchID + ') From : A/c # ' + @AccountID
 SET @mDescriptionHCharges = 'FUND TRF. AT BK.' +  @BankShortName  + ' (BR.' + @CustomerBranchID + ') ' + ' From : A/c # ' + @AccountID  
  
 SET NOCOUNT ON  
 SELECT @Status = -1  
  
 SELECT @vCount1=count(*),@Super=Supervision From t_ATM_TransferTransaction 
 WHERE (OurBranchID = @OurBranchID) AND( AccountID=@AccountID) 
 AND (Refno=@RefNo) and (AtmID=@ATmID)  group by Supervision  
 
 IF @vCount1=1 and @Super='C'
 BEGIN  
     
  SELECT @Status=10  --DUP_TRAN  
  SELECT @Status  as Status  
  RETURN (1)   
 END  
  
 IF @vCount1=1 and @Super='R'  
 BEGIN  
     
  SELECT @Status=35  --ORIG_ALREADY_REVERSED  
  SELECT @Status  as Status  
  RETURN (1)   
 END

 IF len(@Accountid) <>  @Accountdigit  
 BEGIN  
     
  SELECT @Status=9 --FLD Error  
  SELECT @Status  as Status  
  RETURN (1)   
 END  

 IF @Amount<=0  
 BEGIN  
     
  SELECT @Status=48 --BAD AMOUNT  
  SELECT @Status  as Status  
  RETURN (1)   
 END
 
 SELECT @Phxdate=WorkingDate,@mLastEOD=LastEOD,@mwDate=WorkingDate,@mCONFRETI=CONFRETI 
 FROM t_Last WHERE (OurBranchID = @OurBranchID)
 
 IF @mLastEOD = @mwDate or @mCONFRETI = '1/1/9999'
 BEGIN  
     
  SELECT @Status=95  --INVALID_DATE  
  SELECT @Status  as Status  
  RETURN (1)   
 END  

 IF @Phxdate <> @mwDate   
 BEGIN  
     
  SELECT @Status=18  --INVALID_DATE  
  SELECT @Status  as Status  
  RETURN (1)   
 END
 
 SELECT @mScrollNo=(ISNULL(MAX(ScrollNo),0)+1) FROM t_ATM_TransferTransaction 
 WHERE (OurBranchID = @OurBranchID) AND (convert(varchar(10), wDate, 120) = convert(varchar(10), @mwDate, 120))
 
 SELECT @vCount1=count(*) From t_Account WHERE (OurBranchID = @OurBranchID) AND( AccountID IN (@AccountID,@AccountID2))
 
 IF @vCount1=0 or @vCount1=1  
 BEGIN  
     
  SELECT @Status=2  --INVALID_ACCOUNT  
  SELECT @Status  as Status  
  RETURN (1)   
 END  
 
 ------------------------  
 ------------------------  
 --Debit Transaction
  
 SELECT @mAccountName=B.Name, @mProductID=B.ProductID, @mCurrencyID=P.CurrencyID, @mAccountType='C', 
 @vClearBalance=B.ClearBalance, @vClearedEffects=B.ClearedEffects, @vFreezeAmount=B.FreezeAmount, @vMinBalance=P.MinBalance, 
 @limit=isnull(limit,0), @vIsClose=A.Status, @vAreCheckBooksAllowed=P.AreCheckBooksAllowed, 
 @vAllowDebitTransaction=ISNULL(A.AllowDebitTransaction,'0')
 FROM t_AccountBalance B 
 INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND  B.ProductID = A.ProductID 
 INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND  A.ProductID = P.ProductID 
 WHERE  (A.OurBranchID = @OurBranchID) AND (A.AccountID = @AccountID)
 
 SET @vClearBalance2=@vClearBalance  
 
 SELECT @IssuerChargesAccountID=IssuerChargesAccountID,@IssuerTitle=IssuerTitle,@ExciseDutyAccountID= ExciseDutyAccountID,@ExciseDutyPercentage=ExciseDutyPercentage,@FTCharges=FTCharges
 FROM t_ATM_SwitchCharges
 WHERE (OurBranchID = @OurBranchID AND MerchantType = @MerchantType)
 
 IF @vIsClose = 'C' or @vIsClose = 'T' or @vIsClose = 'X' or @vIsClose = 'D' or @vAllowDebitTransaction = 1  
 BEGIN  
     
  SELECT @Status=3  --INACTIVE/CLOSE_ACCOUNT  
  SELECT @Status  as Status  
  RETURN (1)   
 END
 
 IF @Amount > (@vClearBalance-@vClearedEffects+@limit)-@FTCharges OR 
    @Amount > ((@vClearBalance-@vClearedEffects+@limit)-@vFreezeAmount-@vMinBalance-@FTCharges) OR 
    @Amount > ((@vClearBalance-@vClearedEffects+@limit)-@vMinBalance-@FTCharges)  
 BEGIN
     
  SELECT @Status=4  --LOW_BALANCE  
  SELECT @Status  as Status  
  RETURN (1)   
 END  
 
 IF @vAreCheckBooksAllowed = 0
 BEGIN  
     
  SELECT @Status=21  --INVALID_PRODUCT  
  SELECT @Status  as Status  
  RETURN (1)   
 END  
 
 IF @mCurrencyID <> 'PKR'
 BEGIN  
     
  SELECT @Status=19  --INVALID_CURRENCY  
  SELECT @Status  as Status  
  RETURN (1)   
 END  
 
 SELECT @vBranchName=Heading2 FROM t_GlobalVariables WHERE (OurBranchID = @OurBranchID)
 SELECT @mWithdrawlBranchName=BranchName FROM t_ATM_Branches WHERE OurBranchID = @OurBranchID AND BranchID = @WithdrawlBranchID  
 
 SET @mSerialNo = @mSerialNo + 1
 SET @mTrxType = 'D'  
 SET @STAN = RIGHT(@RefNo,6)
 SET @IsCredit = 0
 
 SET @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Branch Fund Transfer Transaction From : BK.' + @BankShortName +  ' (' + upper(@mWithdrawlBranchName) + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)
 
 INSERT INTO t_ATM_TransferTransaction
 (  
  ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
  IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
  [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
 )
 VALUES
 (
  @mScrollNo,@mSerialNo,@RefNo,@OurBranchID,@WithdrawlBranchID,@AccountID,@mAccountName,@mAccountType,@mProductID,
  @mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,
  @Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescription+' STAN: '+@STAN,@ATMID,
  @mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MCC,@TrxDesc,@MerchantType,@STAN
 )
 
 select @GLControl = ISNULL(GLControl, '') from t_Products where OurBranchID = @OurBranchID and ProductID = @mProductID
 select @VoucherID = ISNULL(MAX(VoucherID),0) + 1 from t_GLTransactions where OurBranchID = @OurBranchID
    
 insert into t_Transactions 
 ( 
  OurBranchID,ScrollNo,SerialNo,RefNo,wDate,AccountType,DocumentType,AccountID,AccountName,ProductID,CurrencyID,ValueDate,
  TrxType,ChequeID,ChequeDate,Amount,ForeignAmount,ExchangeRate,ProfitLoss,MeanRate,DescriptionID,[Description],BankCode,
  BranchCode,TrxPrinted,GLID,[Status],IsLocalCurrency,OperatorID,SupervisorID,AdditionalData,IsMainTrx,DocType,GLVoucherID,
  Remarks,TrxDesc,MerchantType,STAN 
 )
 VALUES 
 ( 
  @OurBranchID,@mScrollNo,@mSerialNo,@RefNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@mAccountType,'AT',
  @AccountID,@mAccountName,@mProductID,@mCurrencyID,@mwDate,@mTrxType,'',@mwDate,@Amount,@mForeignAmount,@mExchangeRate,0,
  @mExchangeRate,@mDescriptionID,@mDescription+' STAN: '+@STAN,@mBankCode,@WithdrawlBranchID,0,null,'C',@mIsLocalCurrency,
  @mOperatorID,@mSupervisorID,null,'1','AT',@VoucherID,@mRemarks,@TrxDesc,@MerchantType,@STAN 
 )
    
 insert into t_GLTransactions 
 (
  OurBranchID,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,
  ExchangeRate,IsCredit,TransactionType,TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,
  IsMainTrx,DocType,Remarks,TrxDesc,MerchantType,STAN
 )
 values 
 ( 
  @OurBranchID,@GLControl,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@mwDate,@mDescriptionID,
  @mDescription+' STAN: '+@STAN,@mCurrencyID,@Amount,@mForeignAmount,@mExchangeRate,@IsCredit,'ATMTransfer','L',@mOperatorID,
  @mSupervisorID,'',@mScrollNo,@mSerialNo,null,'0','AT',@mRemarks,@TrxDesc,@MerchantType,@STAN 
 )
 
 SELECT @Status=@@ERROR
 
 IF @Status <> 0
 BEGIN
     
  RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR   
  RETURN (5000)  
 END
 
 SELECT @mAccountName2=B.Name, @mProductID2=B.ProductID, @mCurrencyID2=P.CurrencyID,@mAccountType2='C',
 @vClearBalance3=B.ClearBalance,@vClearedEffects2=B.ClearedEffects,@vFreezeAmount2=B.FreezeAmount,
 @vMinBalance2=P.MinBalance,@vIsClose2=A.Status, @vAreCheckBooksAllowed2=P.AreCheckBooksAllowed,
 @vAllowDebitTransaction2=ISNULL(A.AllowCreditTransaction,'0')
 FROM t_AccountBalance B
 INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND  B.ProductID = A.ProductID 
 INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND  A.ProductID = P.ProductID 
 WHERE (A.OurBranchID = @OurBranchID) AND (A.AccountID = @AccountID2)
 
 IF @vIsClose2 = 'C' or @vIsClose2 = 'T' or @vIsClose2 = 'X' or @vIsClose2 = 'D' or @vAllowDebitTransaction2 = 1
 BEGIN  
     
  SELECT @Status=3  --INACTIVE/CLOSE_ACCOUNT  
  SELECT @Status  as Status  
  RETURN (1)   
 END
 
 IF @vAreCheckBooksAllowed2 = 0
 BEGIN
     
  SELECT @Status=21  --INVALID_PRODUCT  
  SELECT @Status  as Status  
  RETURN (1)   
 END
 
 IF @mCurrencyID2 <> 'PKR'
 BEGIN
     
  SELECT @Status=19  --INVALID_CURRENCY  
  SELECT @Status  as Status  
  RETURN (1)
 END
 
 SET @mSerialNo = @mSerialNo + 1
 SET @mTrxType = 'C'
 SET @IsCredit = 1
 
 SET @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Branch Fund Transfer Transaction From : BK.' + @BankShortName +  ' (' + upper(@mWithdrawlBranchName) + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)

 INSERT INTO t_ATM_TransferTransaction
 (
  ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
  IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
  [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
 )
 VALUES 
 (
  @mScrollNo,@mSerialNo,@RefNo,@OurBranchID,@WithdrawlBranchID,@AccountID2,@mAccountName2,@mAccountType2,@mProductID2,
  @mCurrencyID2,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,
  @Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescriptionH+' STAN: '+@STAN,@ATMID,
  @mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MCC,@TrxDesc,@MerchantType,@STAN
 )
 
 select @GLControl = ISNULL(GLControl, '') from t_Products where OurBranchID = @OurBranchID and ProductID = @mProductID2
 
 insert into t_Transactions 
 ( 
  OurBranchID,ScrollNo,SerialNo,RefNo,wDate,AccountType,DocumentType,AccountID,AccountName,ProductID,CurrencyID,ValueDate,
  TrxType,ChequeID,ChequeDate,Amount,ForeignAmount,ExchangeRate,ProfitLoss,MeanRate,DescriptionID,[Description],BankCode,
  BranchCode,TrxPrinted,GLID,[Status],IsLocalCurrency,OperatorID,SupervisorID,AdditionalData,IsMainTrx,DocType,GLVoucherID,
  Remarks,TrxDesc,MerchantType,STAN 
 ) 
 VALUES 
 ( 
  @OurBranchID,@mScrollNo,@mSerialNo,@RefNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@mAccountType2,'AT',
  @AccountID2,@mAccountName2,@mProductID2,@mCurrencyID2,@mwDate,@mTrxType,'',@mwDate,@Amount,@mForeignAmount,@mExchangeRate,
  0,@mExchangeRate,@mDescriptionID,@mDescriptionH+' STAN: '+@STAN,@mBankCode,@WithdrawlBranchID,0,null,'C',@mIsLocalCurrency,
  @mOperatorID,@mSupervisorID,null,'1','AT',@VoucherID,@mRemarks,@TrxDesc,@MerchantType,@STAN 
 )
    
 insert into t_GLTransactions 
 (
  OurBranchID,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,
  ExchangeRate,IsCredit,TransactionType,TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,
  IsMainTrx,DocType,Remarks,TrxDesc,MerchantType,STAN
 )
 values 
 ( 
  @OurBranchID,@GLControl,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@mwDate,@mDescriptionID,
  @mDescriptionH+' STAN: '+@STAN,@mCurrencyID2,@Amount,@mForeignAmount,@mExchangeRate,@IsCredit,'ATMTransfer','L',@mOperatorID,
  @mSupervisorID,'',@mScrollNo,@mSerialNo,null,'0','AT',@mRemarks,@TrxDesc,@MerchantType,@STAN 
 )
 
 SELECT @Status=@@ERROR
 
 IF @Status <> 0   
 BEGIN  
     
  RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR
  RETURN (5000)  
 END

 --Debit Transaction Switch Charges     
 --Same Branch FT Charges are not applied. if Same Branch FT Charges are to be applied then uncomment this code.
 /*
 IF @FTCharges > 0  
 BEGIN
  
  SET @ExciseDutyAmount = (@FTCharges * @ExciseDutyPercentage) / 116
 
  SET @mSerialNo = @mSerialNo + 1
  SET @mTrxType = 'D'
  
  SET @mRemarksCharges='Being Amount of ATM (' + @ATMID+ ') Fund Transfer Charges Transaction From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
  
  INSERT INTO t_ATM_TransferTransaction 
  (
   ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,[Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,STAN
  )
  VALUES
  (
   @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@AccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10),GetDate(),108),@PHXDate,@mTrxType,@Supervision,@mGLID,@FTCharges,0,1,@mDescriptionID,@mDescriptionHCharges+' STAN: '+@STAN,@ATMID,@mBankCode,@mRemarksCharges,@mOperatorID,@mSupervisorID,@MCC,@TrxDesc,@STAN
  )
  
  SELECT @Status=@@ERROR
  
  IF @Status <> 0
  BEGIN
      
   RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR  
   RETURN (5000)  
  END
  
  --Credit Transaction Switch Charges
  
  SET @mSerialNo = @mSerialNo + 1
  SET @mTrxType = 'C'
  
  INSERT INTO t_ATM_TransferTransaction 
  (  
   ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,[Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,STAN
  )
  VALUES
  (
   @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@IssuerChargesAccountID,@IssuerTitle,'G','GL','PKR',@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,@Supervision,@mGLID,(@FTCharges-@ExciseDutyAmount),0,1,@mDescriptionID,@mDescriptionHCharges+' STAN: '+@STAN,@ATMID,@mBankCode,@mRemarksCharges,@mOperatorID,@mSupervisorID,@MCC,@TrxDesc,@STAN
  )
  
  SELECT @Status=@@ERROR
  
  IF @Status <> 0
  BEGIN
      
   RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR  
   RETURN (5000)  
  END  
  
  --FED Charges Credit Transaction
  
  if (@ExciseDutyPercentage > 0)
  BEGIN
  
   SELECT @FEDAccID = AccountID, @FEDAccTitle = [Description] FROM t_GL WHERE OurBranchID = @OurBranchID AND AccountID = @ExciseDutyAccountID
  
   SET @mSerialNo = @mSerialNo + 1
   SET @mTrxType = 'C'
   SET @mAccountType='G'
   SET @mProductid ='GL'
   SET @mCurrencyID='PKR'
  
   SET @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Branch Fund Transfer FED Charges From : BK.' + @BankShortName +  ' (BR.' +  @WithdrawlBranchID + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)
   
   INSERT INTO t_ATM_TransferTransaction 
   (
    ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,[Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
   )
   VALUES
   (
    @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@FEDAccID,@FEDAccTitle,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,@Supervision,@mGLID,@ExciseDutyAmount,0,1,@mDescriptionID,@mDescriptionH+' STAN: '+@STAN,@ATMID,@mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MCC,@TrxDesc,@MerchantType,@STAN
   )
  
   SELECT @Status=@@ERROR
  
   IF @Status <> 0
   BEGIN
       
    RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR  
    RETURN (5000)  
   END
  END
 END
 
 IF @FTCharges > 0
 BEGIN
  SELECT @Status = 0, 
  @vClearBalance2 = ((IsNull(@vClearBalance2,0)-IsNull(@vClearedEffects,0))-IsNull(@Amount,0)-isNull(@FTCharges,0))*100
  SELECT @Status As Status , @vClearBalance2 As ClearBalance  
  RETURN 0   
 END
 ELSE
 BEGIN
  SELECT @Status = 0 , @vClearBalance2 = ((IsNull(@vClearBalance2,0)-IsNull(@vClearedEffects,0))-IsNull(@Amount,0))*100
  SELECT @Status As Status , @vClearBalance2 As ClearBalance  
  RETURN 0   
 END
 */
 
 SELECT @Status = 0 , @vClearBalance2 = ((IsNull(@vClearBalance2,0)-IsNull(@vClearedEffects,0))-IsNull(@Amount,0))*100
 SELECT @Status As Status , @vClearBalance2 As ClearBalance  
 RETURN 0   
    
END