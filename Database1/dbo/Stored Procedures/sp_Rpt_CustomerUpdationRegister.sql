﻿CREATE Procedure [dbo].[sp_Rpt_CustomerUpdationRegister]  
@OurBranchID Varchar(5),  
@FromDate Date,  
@ToDate Date  
  
AS  
Set nocount on    
 Declare @cols Varchar(MAX),@cols1 Varchar(MAX);  
 Declare @sql Varchar(MAX),@sql1  Varchar(MAX),@Insertsql Varchar(MAX), @sql2 Varchar(MAX),@UpdateCols VARCHAR(MAX),@UpdateCols1 VARCHAR(MAX);  
   
 --Set @OurBranchID ='03'  
 IF OBJECT_ID('tempdb..#CustomerHistoryTemp') IS NOT NULL  
  DROP TABLE #CustomerHistoryTemp  
  
 Create Table #CustomerHistoryTemp (CreateBy nvarchar(Max) null, CreateTime nvarchar(Max) null)  
  
 Select @cols = COALESCE(@cols + ',  ' + name+ ' nvarchar(Max) null', name+ ' nvarchar(Max) null')   
        From sys.columns where object_id in (Select object_id from sys.tables where name='t_Customer_History')   
  Order By column_id Offset 2 rows fetch next 150 rows only   
  Select @cols1 = COALESCE(@cols1 + ',  ' + name+ ' nvarchar(Max) null', name+ ' nvarchar(Max) null')   
        From sys.columns where object_id in (Select object_id from sys.tables where name='t_Customer_History')   
  Order By column_id Offset 152 rows fetch next 150 rows only   
  
 Select @sql=N'Alter Table #CustomerHistoryTemp ADD '+@cols+' ';  
 Select @sql1=N'Alter Table #CustomerHistoryTemp ADD '+@cols1+', SNo int; ';  
  
 Select @Insertsql=N'Insert into  #CustomerHistoryTemp Select * From (Select *,ROW_NUMBER() OVER (PARTITION BY ClientID ORDER BY wDate DESC) AS SNo '  
 Select @Insertsql=@Insertsql+'from t_Customer_History where UpdateTime is not null And OurBranchID='''+@OurBranchID+''') as t Where t.SNo<=2 ;'  
  
 Select @UpdateCols = 'Update #CustomerHistoryTemp Set '+STUFF((SELECT  ', ' + QUOTENAME(Substring(FieldId,4,100))+'=IsNull('+QUOTENAME(Substring(FieldId,4,100))+', '''')'                      
 FROM tbl_Form_Fields where FrmName ='frmClient' And  IsDisplayField=0 And Substring(FieldId,4,100) Not In ('ClientID','Name','ShareHolderID','AuthorisedSignatoriesID','BeneficialOwnersID','BoardDirectorID')  
 Order by tabid,OrderNo     Offset 0 rows fetch next 150 rows only                                  
 FOR XML PATH(''), TYPE).value('.', 'VARCHAR(MAX)') ,1,1,'')  
  
 Select @UpdateCols1 = 'Update #CustomerHistoryTemp Set '+STUFF((SELECT  ', ' + QUOTENAME(Substring(FieldId,4,100))+'=IsNull('+QUOTENAME(Substring(FieldId,4,100))+', '''')'                      
 FROM tbl_Form_Fields where FrmName ='frmClient' And  IsDisplayField=0 And Substring(FieldId,4,100) Not In ('ClientID','Name','GeographicCoverageofBusinessTwo','SicCodeCorp','SICCodeType','BoardDirectorID')  
 Order by tabid,OrderNo     Offset 150 rows fetch next 150 rows only                                  
 FOR XML PATH(''), TYPE).value('.', 'VARCHAR(MAX)') ,1,1,'')  
    
 --print @sql  
 exec(@sql)  
 exec(@sql1)  
 exec(@Insertsql)  
 print @UpdateCols  
 exec(@UpdateCols)  
 exec(@UpdateCols1)   
   
  
 Select Distinct  OV.ClientID,OV.Name,OV.ColName,OldValue,NewValue,OV.CreateBy,OV.SuperviseBy,OV.wdate  
 From (  
 Select *  
 from dbo.#CustomerHistoryTemp Where SNo=2) as OV   
 UNPIVOT  
 (OldValue For ColName IN ( [CategoryId], [Address],  [Phone1],  [Phone2],  [MobileNo],  [Fax],  [Email],  [NIC],  [DOB],  [NicExpirydate],  [NTN],  [CIBCode],  [RelationShipCode],  [frmName],  [VerifyBy],  [VerifyTime],  [VerifyTerminal],  [VerifyStatus]
,    
    [CID],  [NameOfFather],  [NameOfMother],  [Gender],  [MaritalStatus],  [POBCountry],  [POBCity],  [OldClientID],  [FZType],  [DOI],  [TradeLicenseNo],  [CompanyType],  [GroupID],  [ParentClientID],  [GroupCode],    
    [CloseDate],  [RiskProfile],  [FirstName],  [MiddleName],  [LastName],  [FATCARelevantInd],  [Nationality],  [IDIssueCountry],  [PassportNumber],  [PassportExpiry],  [PassportIssueCountry],  [PassportGender],    
    [OnboardingCountry],  [Profession],  [EmployerName],  [IncomeBracket],  [KYCReviewDate],  [DualCitizenship],  [CitizenCountry2],  [PEP],  [BlackList],  [CRSRelevantInd],  [AreYouAuSCitizenInd],  [AreYouAUSTaxResidentInd],    
    [WereYouBornInTheUSInd],  [CountryUSOrUAEInd],  [TinInd],  [TaxCountry1Ind],  [TaxCountry2Ind],  [TaxCountry3Ind],  [TaxCountry4Ind],  [TaxCountry5Ind],  [EntityNameCorp],  [YearofestablishmentCorp],  [RegisteredAddressCorp],    
    [ContactName1Corp],  [ContactID1Corp],  [Address1Corp],  [PhoneNumberCorp],  [AlternatePhonenumberCorp],  [EmailIDCorp],  [ContactName2Corp],  [ContactID2Corp],  [Address2Corp],  [PhoneNumber2Corp],  [AlternatePhonenumber2Corp],    
    [EmailID2Corp],  [BusinessActivity],  [IndustryCorp],  [PresenceinCISCountryCorp],  [DealingwithCISCountryCorp],  [TradeLicenseNumberCorp],  [CountryofIncorporationCorp],  [TradeLicenseIssuedateCorp],  [TradeLicenseExpirydateCorp],    
    [TradeLicenseIssuanceAuthorityCorp],  [TRNNoCorp],  [PEPCorp],  [BlackListCorp],  [KYCReviewDateCorp],  [FATCARelevantCorp],  [CRSRelevantCorp],  [USAEntityCorp],  [FFICorp],  [GIINNoCorp],  [SponsorNameCorp],  [SponsorGIIN],  [StockExchangeCorp],   
 
    [TradingSymbolCorp],  [USAORUAECorp],  [TINAvailableCorp],  [TaxCountry1Corp],  [TaxCountry2Corp],  [TaxCountry3Corp],  [TaxCountry4Corp],  [TaxCountry5Corp],  [ReasonACorp],  [ReasonBCorp],  [FATCANoReason],  [ReasonBind],  [HNI],  [CountryCors],    
    [StateCors],  [CityCors],  [CountryCurr],  [StateCurr],  [CityCurr],  [AddressHomeCountry],  [CountryHome],  [StateHome],  [CityHome],  [ResidentNonResident],  [AddressCors],  [AddressCurr],  [Tinno1Ind],  [Tinno2Ind],  [Tinno3Ind],  [Tinno4Ind],    
    [Tinno5Ind],  [TinNo1Corp],  [TinNo2Corp],  [TinNo3Corp],  [TinNo4Corp],  [TinNo5Corp],  [EntityTypeCorp],  [IDTypeCorp], [SICCODESCORP], [FFICategoryCorp], [NFFECorp], [GIINNACorp], [IDType2], [CountryOfResidence], [CustomerPromoCode],   
    [RefererPromoCode], [MDMID], [RegAddCountryCorp], [RegAddStateCorp], [RegAddCityCorp], [Add1CountryCorp], [Add1StateCorp], [Add1CityCorp], [Add2CountryCorp], [Add2StateCorp], [Add2CityCorp], [CompanyWebsite], [CompCashIntensive], [PubLimCompCorp],   
    [VATRegistered], [RManager], [GroupCodeType], [DeathDate], [IsVIP], [JurEmiratesID], [JurTypeID], [JurAuthorityID], [POBox], [JurOther], [Status], [LicenseType], [CountryID], [MOAMOU], [SigningAuthority], [POAResidenceCountry], [POAAddress],   
    [POACountry], [POAState], [POACity], [AuditedFinancials], [Assets], [Revenue], [Profits], [BusinessTurnOver], [CompanyCapital], [CompanyCountry])) as OV  
 Inner Join   
 (  
 Select *  
 from dbo.#CustomerHistoryTemp Where SNo=1) as NV   
 UNPIVOT  
 (NewValue For ColName IN ([CategoryId], [Address],  [Phone1],  [Phone2],  [MobileNo],  [Fax],  [Email],  [NIC],  [DOB],  [NicExpirydate],  [NTN],  [CIBCode],  [RelationShipCode],  [frmName],  [VerifyBy],  [VerifyTime],  [VerifyTerminal],  [VerifyStatus],
    
    [CID],  [NameOfFather],  [NameOfMother],  [Gender],  [MaritalStatus],  [POBCountry],  [POBCity],  [OldClientID],  [FZType],  [DOI],  [TradeLicenseNo],  [CompanyType],  [GroupID],  [ParentClientID],  [GroupCode],    
    [CloseDate],  [RiskProfile],  [FirstName],  [MiddleName],  [LastName],  [FATCARelevantInd],  [Nationality],  [IDIssueCountry],  [PassportNumber],  [PassportExpiry],  [PassportIssueCountry],  [PassportGender],    
    [OnboardingCountry],  [Profession],  [EmployerName],  [IncomeBracket],  [KYCReviewDate],  [DualCitizenship],  [CitizenCountry2],  [PEP],  [BlackList],  [CRSRelevantInd],  [AreYouAuSCitizenInd],  [AreYouAUSTaxResidentInd],    
    [WereYouBornInTheUSInd],  [CountryUSOrUAEInd],  [TinInd],  [TaxCountry1Ind],  [TaxCountry2Ind],  [TaxCountry3Ind],  [TaxCountry4Ind],  [TaxCountry5Ind],  [EntityNameCorp],  [YearofestablishmentCorp],  [RegisteredAddressCorp],    
    [ContactName1Corp],  [ContactID1Corp],  [Address1Corp],  [PhoneNumberCorp],  [AlternatePhonenumberCorp],  [EmailIDCorp],  [ContactName2Corp],  [ContactID2Corp],  [Address2Corp],  [PhoneNumber2Corp],  [AlternatePhonenumber2Corp],    
    [EmailID2Corp],  [BusinessActivity],  [IndustryCorp],  [PresenceinCISCountryCorp],  [DealingwithCISCountryCorp],  [TradeLicenseNumberCorp],  [CountryofIncorporationCorp],  [TradeLicenseIssuedateCorp],  [TradeLicenseExpirydateCorp],    
    [TradeLicenseIssuanceAuthorityCorp],  [TRNNoCorp],  [PEPCorp],  [BlackListCorp],  [KYCReviewDateCorp],  [FATCARelevantCorp],  [CRSRelevantCorp],  [USAEntityCorp],  [FFICorp],  [GIINNoCorp],  [SponsorNameCorp],  [SponsorGIIN],  [StockExchangeCorp],    
    [TradingSymbolCorp],  [USAORUAECorp],  [TINAvailableCorp],  [TaxCountry1Corp],  [TaxCountry2Corp],  [TaxCountry3Corp],  [TaxCountry4Corp],  [TaxCountry5Corp],  [ReasonACorp],  [ReasonBCorp],  [FATCANoReason],  [ReasonBind],  [HNI],  [CountryCors],    
    [StateCors],  [CityCors],  [CountryCurr],  [StateCurr],  [CityCurr],  [AddressHomeCountry],  [CountryHome],  [StateHome],  [CityHome],  [ResidentNonResident],  [AddressCors],  [AddressCurr],  [Tinno1Ind],  [Tinno2Ind],  [Tinno3Ind],  [Tinno4Ind],    
    [Tinno5Ind],  [TinNo1Corp],  [TinNo2Corp],  [TinNo3Corp],  [TinNo4Corp],  [TinNo5Corp],  [EntityTypeCorp],  [IDTypeCorp], [SICCODESCORP], [FFICategoryCorp], [NFFECorp], [GIINNACorp], [IDType2], [CountryOfResidence], [CustomerPromoCode],   
    [RefererPromoCode], [MDMID], [RegAddCountryCorp], [RegAddStateCorp], [RegAddCityCorp], [Add1CountryCorp], [Add1StateCorp], [Add1CityCorp], [Add2CountryCorp], [Add2StateCorp], [Add2CityCorp], [CompanyWebsite], [CompCashIntensive], [PubLimCompCorp],   
    [VATRegistered], [RManager], [GroupCodeType], [DeathDate], [IsVIP], [JurEmiratesID], [JurTypeID], [JurAuthorityID], [POBox], [JurOther], [Status], [LicenseType], [CountryID], [MOAMOU], [SigningAuthority], [POAResidenceCountry], [POAAddress],   
    [POACountry], [POAState], [POACity], [AuditedFinancials], [Assets], [Revenue], [Profits], [BusinessTurnOver], [CompanyCapital], [CompanyCountry])) as NV On OV.ColName=NV.ColName And OV.ClientID=NV.ClientID  
 Where IsNull(OV.OldValue,'') <>IsNull(NV.NewValue,'')   
 Order By OV.ClientID