﻿CREATE PROCEDURE [dbo].[spc_GetClosingBalance]    
(      
  @OurBranchID varchar(30),       
  @AccountID varchar(30),      
  @Date datetime,      
  @GLOrAccount char(1)='C',      
  @DateOrValue char(1)='D',      
  @ClearOrTotal char(1)='T'      
)      
AS      
      
 DECLARE @ClosingBalance numeric(18,2)      
      
 DECLARE       
  @ProductID varchar(30),      
  @frmDate datetime,      
  @LocalCurrency varchar(30),      
  @CurrencyID varchar(30),      
  @TotalAmountLCY numeric(18,2),      
  @TotalAmountFCY numeric(18,2),      
  @crAmountLCY numeric(18,2),      
  @crAmountFCY numeric(18,2),      
  @dbAmountLCY numeric(18,2),      
  @dbAmountFCY numeric(18,2)      
        
 SET @TotalAmountLCY = 0      
 SET @TotalAmountFCY = 0      
 SET @crAmountLCY = 0      
 SET @crAmountFCY = 0      
 SET @dbAmountLCY = 0      
 SET @dbAmountFCY = 0      
        
 SET @ClosingBalance = 0      
 SELECT @LocalCurrency = LocalCurrency FROM t_GlobalVariables where OurBranchID = @OurBranchID      
       
 SELECT @frmDate = convert(varchar,month(@Date)) + '/01/' + convert(varchar,year(@Date))      
       
 IF @GLOrAccount = 'C'      
 BEGIN      
       
  IF @ClearOrTotal = 'C'      
  BEGIN      
        
   SELECT @ClosingBalance = ClearBalance + Effects FROM t_AccountMonthBalances       
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID and Month(Month)= Month(@Date)       
   and Year(Month)= Year(@Date)      
         
  END      
  ELSE      
  BEGIN      
        
   SELECT @ClosingBalance = ClearBalance + Effects FROM t_AccountMonthBalances       
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID and Month(Month)= Month(@Date)       
   and Year(Month)= Year(@Date)      
         
  END      
        
  SELECT @ProductID = ProductID, @CurrencyID = CurrencyID FROM t_Account WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID      
      
  IF @DateOrValue = 'D'      
  BEGIN      
   SELECT @dbAmountLCY = ISNULL(SUM(Amount),0), @dbAmountFCY = ISNULL(SUM(ForeignAmount), 0)      
   FROM t_Transactions      
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID      
   AND convert(varchar(10), wDate, 120) < @Date      
   AND convert(varchar(10), wDate, 120) >= @frmDate AND TrxType IN ('Z','D','I') AND Status <> 'R'      
   GROUP BY TrxType      
         
   SELECT @crAmountLCY = ISNULL(SUM(Amount),0), @crAmountFCY = ISNULL(SUM(ForeignAmount), 0)      
   FROM t_Transactions      
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID      
   AND convert(varchar(10), wDate, 120) < @Date      
   AND convert(varchar(10), wDate, 120) >= @frmDate AND TrxType IN ('A','C','O') AND Status <> 'R'      
   GROUP BY TrxType      
  END      
  ELSE      
  BEGIN      
   SELECT @dbAmountLCY = ISNULL(SUM(Amount),0), @dbAmountFCY = ISNULL(SUM(ForeignAmount), 0)      
   FROM t_Transactions      
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID      
   AND convert(varchar(10), ValueDate, 120) < @Date      
   AND convert(varchar(10), ValueDate, 120) >= @frmDate AND TrxType IN ('Z','D','I') AND Status <> 'R'      
   GROUP BY TrxType      
         
   SELECT @crAmountLCY = ISNULL(SUM(Amount),0), @dbAmountFCY = ISNULL(SUM(ForeignAmount), 0)      
   FROM t_Transactions      
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID      
   AND convert(varchar(10), ValueDate, 120) < @Date      
   AND convert(varchar(10), ValueDate, 120) >= @frmDate AND TrxType IN ('A','C','O') AND Status <> 'R'      
   GROUP BY TrxType      
  END      
        
  SET @TotalAmountLCY = @crAmountLCY - @dbAmountLCY      
  SET @TotalAmountFCY = @crAmountFCY - @dbAmountFCY      
      
  IF @CurrencyID = @LocalCurrency      
  BEGIN      
   SET @ClosingBalance = @ClosingBalance + @TotalAmountLCY      
  END      
  ELSE      
  BEGIN      
   SET @ClosingBalance = @ClosingBalance + @TotalAmountFCY      
  END      
 END      
 ELSE      
 BEGIN      
       
  SELECT @CurrencyID = CurrencyID FROM t_GL WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID      
        
  IF @CurrencyID = @LocalCurrency      
  BEGIN      
   SELECT @ClosingBalance = OpeningBalance FROM t_GL WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID      
  END      
  ELSE      
  BEGIN      
   SELECT @ClosingBalance = ForeignOpeningBalance FROM t_GL WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID      
  END      
      
  IF @DateOrValue = 'D'      
  BEGIN      
         
   SELECT @dbAmountLCY = ISNULL(SUM(Amount),0), @dbAmountFCY = ISNULL(SUM(ForeignAmount), 0)      
   FROM t_GLTransactions      
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID      
   AND convert(varchar(10), Date, 120) < @Date AND IsCredit = 0 AND Status <> 'R'      
   GROUP BY IsCredit      
         
   SELECT @crAmountLCY = ISNULL(SUM(Amount),0), @crAmountFCY = ISNULL(SUM(ForeignAmount), 0)      
   FROM t_GLTransactions      
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID      
   AND convert(varchar(10), Date, 120) < @Date AND IsCredit = 1 AND Status <> 'R'      
   GROUP BY IsCredit      
         
  END      
  ELSE      
  BEGIN      
         
   SELECT @dbAmountLCY = ISNULL(SUM(Amount),0), @dbAmountFCY = ISNULL(SUM(ForeignAmount), 0)      
   FROM t_GLTransactions      
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID      
   AND convert(varchar(10), ValueDate, 120) <= @Date AND IsCredit = 0 AND Status <> 'R'      
   GROUP BY IsCredit      
         
   SELECT @crAmountLCY = ISNULL(SUM(Amount),0), @crAmountFCY = ISNULL(SUM(ForeignAmount), 0)      
   FROM t_GLTransactions      
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID      
   AND convert(varchar(10), ValueDate, 120) <= @Date AND IsCredit = 1 AND Status <> 'R'      
   GROUP BY IsCredit      
         
  END      
        
  SET @TotalAmountLCY = @crAmountLCY - @dbAmountLCY      
  SET @TotalAmountFCY = @crAmountFCY - @dbAmountFCY      
      
  IF @CurrencyID = @LocalCurrency      
  BEGIN      
   SET @ClosingBalance = @ClosingBalance + @TotalAmountLCY      
  END      
  ELSE      
  BEGIN      
   SET @ClosingBalance = @ClosingBalance + @TotalAmountFCY      
  END      
 END      
       
 SELECT @ClosingBalance AS ClosingBalance