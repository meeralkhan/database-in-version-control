﻿CREATE PROCEDURE [dbo].[spc_TransferVoucher]          
 @OurBranchID varchar(4),            
 @ScrollNo numeric(10,0),            
 @SerialNo int=0,            
 @Flag int=0          
          
AS          
      
SET NOCOUNT ON      
          
IF @Flag = 0            
           
 IF @SerialNo=0            
              
    SELECT t.ScrollNo, t.SerialNo, t.Refno, t.OurBranchID, b.Name AS OurBranchName, t.AccountID, t.AccountName,           
    t.ValueDate, t.wDate, t.TrxType, t.Amount, t.ForeignAmount, t.Description, t.IsLocalCurrency, t.AdditionalData,    
    c.Description AS CurDescription, t.CurrencyID, t.ExchangeRate, t.Supervision, b.BankID, t.OperatorID, t.AccountType,           
    t.ChequeID, ISNULL(a.Address, '') AS Address, isnull(p.Description,'GL') AS Product, t.ChequeDate,t.SupervisorID           
              
    FROM t_TransferTransactionModel t           
    LEFT JOIN t_Products p ON t.OurBranchID = p.OurBranchID AND t.ProductID = p.ProductID          
    LEFT JOIN t_Account a ON t.OurBranchID = a.OurBranchID AND t.AccountID = a.AccountID          
    INNER JOIN t_GlobalVariables g ON g.OurBranchID = t.OurBranchID          
    INNER JOIN t_branches b ON t.OurBranchID = b.OurBranchID AND g.OurBankID = b.BankID AND t.OurBranchID = b.BranchID           
    INNER JOIN t_Currencies c ON t.OurBranchID = c.OurBranchID AND t.CurrencyID = c.CurrencyID           
              
    Where t.OurBranchID = @OurBranchID AND t.ScrollNo = @ScrollNo AND t.Supervision <> 'R'          
              
 ELSE          
              
    SELECT t.ScrollNo, t.SerialNo, t.Refno, t.OurBranchID, b.Name AS OurBranchName, t.AccountID, t.AccountName,           
    t.ValueDate, t.wDate, t.TrxType, t.Amount, t.ForeignAmount, t.Description, t.IsLocalCurrency, t.AdditionalData,    
    c.Description AS CurDescription, t.CurrencyID, t.ExchangeRate, t.Supervision, b.BankID, t.OperatorID, t.AccountType,           
    t.ChequeID, ISNULL(a.Address, '') AS Address, isnull(p.Description,'GL') AS Product, t.ChequeDate,t.SupervisorID           
              
    FROM t_TransferTransactionModel t           
    LEFT JOIN t_Products p ON t.OurBranchID = p.OurBranchID AND t.ProductID = p.ProductID          
    LEFT JOIN t_Account a ON t.OurBranchID = a.OurBranchID AND t.AccountID = a.AccountID          
    INNER JOIN t_GlobalVariables g ON g.OurBranchID = t.OurBranchID          
    INNER JOIN t_branches b ON t.OurBranchID = b.OurBranchID AND g.OurBankID = b.BankID AND t.OurBranchID = b.BranchID           
    INNER JOIN t_Currencies c ON t.OurBranchID = c.OurBranchID AND t.CurrencyID = c.CurrencyID           
              
    Where t.OurBranchID = @OurBranchID AND t.ScrollNo = @ScrollNo AND SerialNo = @SerialNo AND t.Supervision <> 'R'          
              
ELSE IF @Flag = 1          
              
    SELECT t.ScrollNo, t.SerialNo, t.Refno, t.OurBranchID, b.Name AS OurBranchName, t.AccountID, t.AccountName,           
    t.ValueDate, t.wDate, t.TrxType, t.Amount, t.ForeignAmount, t.Description, t.IsLocalCurrency, t.AdditionalData,    
    c.Description AS CurDescription, t.CurrencyID, t.ExchangeRate, t.Supervision, b.BankID, t.OperatorID, t.AccountType,           
    t.ChequeID, ISNULL(a.Address, '') AS Address, isnull(p.Description,'GL') AS Product, t.ChequeDate,t.SupervisorID           
              
    FROM t_TransferTransactionModel t           
    LEFT JOIN t_Products p ON t.OurBranchID = p.OurBranchID AND t.ProductID = p.ProductID          
    LEFT JOIN t_Account a ON t.OurBranchID = a.OurBranchID AND t.AccountID = a.AccountID          
    INNER JOIN t_GlobalVariables g ON g.OurBranchID = t.OurBranchID          
    INNER JOIN t_branches b ON t.OurBranchID = b.OurBranchID AND g.OurBankID = b.BankID AND t.OurBranchID = b.BranchID           
    INNER JOIN t_Currencies c ON t.OurBranchID = c.OurBranchID AND t.CurrencyID = c.CurrencyID           
              
    Where t.OurBranchID = @OurBranchID AND t.Supervision <> 'R'