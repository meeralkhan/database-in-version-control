﻿
CREATE PROCEDURE [dbo].[spc_ChkContigentIsBalanced]
(
  @OurBranchID varchar(4)
)
AS

SET NOCOUNT ON

DECLARE @TotalAsset money
DECLARE @TotalLiability money

SELECT @TotalAsset = ISNULL(SUM(Balance),0) From t_GL
WHERE OurBranchID = @OurBranchID AND AccountType = 'A' AND IsContigent = 1 AND Balance <> 0

SELECT @TotalLiability = ISNULL(SUM(Balance),0) From t_GL
WHERE OurBranchID = @OurBranchID AND AccountType = 'L' AND IsContigent = 1 AND Balance <> 0

IF @TotalLiability - @TotalAsset = 0
Begin
  select 'Ok' AS RetStatus, (@TotalLiability - @TotalAsset) AS Amount
end
else
begin
  select 'Error' AS RetStatus, (@TotalLiability - @TotalAsset) AS Amount
end