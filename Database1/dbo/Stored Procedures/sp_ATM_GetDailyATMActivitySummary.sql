﻿CREATE PROCEDURE [dbo].[sp_ATM_GetDailyATMActivitySummary]
(
	@OurBranchID	varchar(30),
	@ATMID	varchar(30),
	@wDate	datetime
)
AS
	BEGIN
		SELECT COUNT(*) AS 'SummaryAllTotal',

		        (SELECT COUNT(*)
		      FROM t_ATM_TransactionHistory
		      WHERE (OurBranchID = @OurBranchID) AND (ATMID = @ATMID) AND 
		           (Supervision = 'R') and (wDate = @wDate)) AS 'SummaryAllRejected',

		        (SELECT COUNT(*)
		      FROM t_ATM_TransactionHistory
		      WHERE (OurBranchID = @OurBranchID) AND (ATMID = @ATMID) AND 
		           (Supervision = 'C') and (wDate = @wDate)) AS 'SummaryAllCleared',

		        (SELECT COUNT(*)
		      FROM t_ATM_TransactionHistory
		      WHERE (OurBranchID = @OurBranchID) AND (ATMID = @ATMID) AND 
		           (wDate = @wDate) and  (OurBranchID = CustomerBranchID)) AS 'SummaryLocalTotal',

		        (SELECT COUNT(*)
		      FROM t_ATM_TransactionHistory
		      WHERE (OurBranchID = @OurBranchID) AND (ATMID = @ATMID) AND 
		           (Supervision = 'R') and (wDate = @wDate) and  (OurBranchID = CustomerBranchID)) AS 'SummaryLocalRejected',

		        (SELECT COUNT(*)
		      FROM t_ATM_TransactionHistory
		      WHERE (OurBranchID = @OurBranchID) AND (ATMID = @ATMID) AND 
		           (Supervision = 'C') and (wDate = @wDate) and  (OurBranchID = CustomerBranchID)) AS 'SummaryLocalCleared',

		        (SELECT isnull(SUM(Amount),0)
		      FROM t_ATM_TransactionHistory
		      WHERE (OurBranchID = @OurBranchID) AND (ATMID = @ATMID) AND 
		           (Supervision = 'C') and (wDate = @wDate) and  (OurBranchID = CustomerBranchID)) AS 'SummaryLocalCredit',

		        (SELECT COUNT(*)
		      FROM t_ATM_TransactionHistory
		      WHERE (OurBranchID = @OurBranchID) AND (ATMID = @ATMID) AND 
		          (wDate = @wDate) and  (OurBranchID <> CustomerBranchID)) AS 'SummaryInterBranchTotal',

		        (SELECT COUNT(*)
		      FROM t_ATM_TransactionHistory
		      WHERE (OurBranchID = @OurBranchID) AND (ATMID = @ATMID) AND 
		           (Supervision = 'R') and (wDate = @wDate) and  (OurBranchID <> CustomerBranchID)) AS 'SummaryInterBranchRejected',

		        (SELECT COUNT(*)
		      FROM t_ATM_TransactionHistory
		      WHERE (OurBranchID = @OurBranchID) AND (ATMID = @ATMID) AND 
		           (Supervision = 'C') and (wDate = @wDate) and  (OurBranchID <> CustomerBranchID)) AS 'SummaryInterBranchCleared',

		        (SELECT isnull(SUM(Amount),0)
		      FROM t_ATM_TransactionHistory
		      WHERE (OurBranchID = @OurBranchID) AND (ATMID = @ATMID) AND 
		           (Supervision = 'C') and (wDate = @wDate) and  (OurBranchID <> CustomerBranchID)) AS 'SummaryInterBranchCredit',

		        (SELECT  isnull(SUM(Amount),0)
		      FROM t_ATM_TransactionHistory
		      WHERE (OurBranchID = @OurBranchID) AND (ATMID = @ATMID) AND 
		           (Supervision = 'C') and (wDate = @wDate)) AS 'SummaryCredit'

		FROM t_ATM_TransactionHistory
		WHERE (OurBranchID = @OurBranchID) AND (ATMID = @ATMID) and (wDate = @wDate)
	END