﻿create procedure [dbo].[sp_MarkDormant]
(
   @OurBranchID nvarchar(30),
   @AccountID nvarchar(30)
)
AS

SET NOCOUNT ON

declare @Status nvarchar(1)
select @Status = ISNULL(Status,'') from t_Account where OurBranchID = @OurBranchID and AccountID = @AccountID

if @Status = ''
begin
   
   declare @SerialNo int
   select @SerialNo = ISNULL(MAX(Serial),0)+1 from t_AccountStatusMarking
   where OurBranchID = @OurBranchID and AccountID = @AccountID

   declare @wDate datetime
   select @wDate = WORKINGDATE from t_Last where OurBranchID = @OurBranchID

   update t_Account set UpdateBy = 'SYSTEM', UpdateTime = GETDATE(), UpdateTerminal = '::1', Status = 'T'
   where OurBranchID = @OurBranchID and AccountID = @AccountID

   insert into t_AccountStatusMarking (CreateBy,CreateTime,CreateTerminal,AuthStatus,VerifyStatus,OurBranchID,AccountID,Serial,WorkingDate,Status,Comments,frmName)
   values ('SYSTEM',GETDATE(),'::1','','Y',@OurBranchID,@AccountID,@SerialNo,@wDate,'T','Marked from Procedure','test_MarkDormant')

   select 'Account marked as Dormant' AS ReturnStatus, 'T' AS [Status]
end
else
begin
   if ISNULL(@Status,'')=''
   begin
      select 'Account Not Found' AS ReturnStatus, '' AS [Status]
   end
   else
   begin
      select 'Account Not Active' AS ReturnStatus, @Status AS [Status]
   end
end