﻿CREATE PROCEDURE [dbo].[spc_GetInwardLook]  
    
 @OurBranchID varchar(30),    
 @BankID varchar(4)='',    
 @DebitSupervisionLimit money=0,    
 @CreditSupervisionLimit money=0,    
 @ScrollNo int=0,    
 @Status Char(1) = ''     
AS    
   
 SET NOCOUNT ON  
   
 IF @ScrollNo=0    
  BEGIN    
   --Status 'A' Show Display All Cleared Transactions    
  IF @Status <> 'A'    
   BEGIN     
    IF @BankID = ''    
     Select  '1' as RetStatus,SerialNo,OurBranchID ,BankID, AccountID, AccountName, AccountType,ChequeID,    
       CurrencyID,ChequeDate, ValueDate, Amount, ForeignAmount, Status, OperatorID,SupervisorID,Reason,ProductID,Isapplycharges,ChargesScrollno    
     from t_InwardClearing    
     Where (OurBranchID = @OurBranchID) and ( Status not in ('','R')) and    
           ((Amount <=  @CreditSupervisionLimit and  Amount > 0) OR (Amount <=  @DebitSupervisionLimit and Amount > 0))     
     order by serialno    
    ELSE    
     Select  '1' as RetStatus,SerialNo,OurBranchID ,BankID, AccountID, AccountName, AccountType,ChequeID,    
      CurrencyID,ChequeDate, ValueDate, Amount, ForeignAmount, Status, OperatorID,SupervisorID, Reason,ProductID,Isapplycharges,ChargesScrollno    
     from t_InwardClearing    
     Where (OurBranchID = @OurBranchID) and    
                 (BankID = @BankID) and ( Status not in ('','R')) and     
                   ((Amount <=  @CreditSupervisionLimit and  Amount > 0) OR (Amount <=  @DebitSupervisionLimit and Amount < 0))                
     order by serialno    
   END    
    
  ELSE    
   BEGIN    
    IF @BankID = ''    
      Select  '1' as RetStatus,SerialNo,OurBranchID ,BankID, AccountID, AccountName, AccountType,ChequeID,    
        CurrencyID,ChequeDate, ValueDate, Amount, ForeignAmount, Status, OperatorID,SupervisorID,Reason,ProductID,Isapplycharges,ChargesScrollno    
      from t_InwardClearing    
      Where (OurBranchID = @OurBranchID) and ( Status in ('')) and    
            ((Amount <=  @CreditSupervisionLimit and  Amount > 0) OR (Amount <=  @DebitSupervisionLimit and Amount > 0))     
      order by serialno    
     ELSE    
      Select  '1' as RetStatus,SerialNo,OurBranchID ,BankID, AccountID, AccountName, AccountType,ChequeID,    
       CurrencyID,ChequeDate, ValueDate, Amount, ForeignAmount, Status, OperatorID,SupervisorID, Reason,ProductID,Isapplycharges,ChargesScrollno    
      from t_InwardClearing    
      Where (OurBranchID = @OurBranchID) and    
                  (BankID = @BankID) and ( Status in ('')) and   
                    ((Amount <=  @CreditSupervisionLimit and  Amount > 0) OR (Amount <=  @DebitSupervisionLimit and Amount < 0))                
      order by serialno    
    END    
   END    
    
 Else    
  BEGIN    
   IF @BankID = ''    
    
    Select  '1' as RetStatus,SerialNo,OurBranchID ,BankID, AccountID, AccountName, AccountType,ChequeID,    
      CurrencyID,ChequeDate, ValueDate, Amount, ForeignAmount, Status, OperatorID,SupervisorID,Reason,ProductID,Isapplycharges,ChargesScrollno    
    from t_InwardClearing    
    Where (OurBranchID = @OurBranchID) and ( Status not in ('R')) and    
          ((Amount <=  @CreditSupervisionLimit and  Amount > 0) OR (Amount <=  @DebitSupervisionLimit and Amount > 0))     
     And SerialNo=@ScrollNo    
    order by serialno    
   ELSE    
    Select  '1' as RetStatus,SerialNo,OurBranchID ,BankID, AccountID, AccountName, AccountType,ChequeID,    
     CurrencyID,ChequeDate, ValueDate, Amount, ForeignAmount, Status, OperatorID,SupervisorID, Reason,ProductID,Isapplycharges,ChargesScrollno    
    from t_InwardClearing    
    Where (OurBranchID = @OurBranchID) and    
                (BankID = @BankID) and ( Status not in ('R')) and   
                  ((Amount <=  @CreditSupervisionLimit and  Amount > 0) OR (Amount <=  @DebitSupervisionLimit and Amount < 0))     
           And SerialNo=@ScrollNo    
    order by serialno    
  END