﻿create PROCEDURE [dbo].[spc_OL_VerifyInstrumentIBD]      
(      
 @OurBranchID varchar(30),      
 @InstrumentType varchar(50)='',      
 @IssueDate datetime='01/01/1900',      
 @InstrumentNo int=0,      
 @ControlNo int=0,        
 @Amount money=0      
)      
      
AS      
       
 DECLARE @IsPaid as Bit      
 DECLARE @IsCancel as Bit      
 DECLARE @IsStop as Bit      
 DECLARE @IsOverLimit as Bit      
 DECLARE @IsLost as Bit      
 DECLARE @IsErase as Bit      
     
 SELECT @IsPaid = 1, @IsCancel = 1, @IsStop = 1, @IsOverLimit = 1, @IsLost = 1, @IsErase = 1      
     
 IF (SELECT Count(*) From t_LR_Issuance      
  WHERE OurBranchID = @OurBranchID AND InstrumentType = @InstrumentType AND InstrumentNo = @InstrumentNo       
    AND ControlNo = @ControlNo AND (wDate = @IssueDate Or DuplicateDate = @IssueDate)       
    AND Amount = @Amount AND Supervision = 'C') > 0      
      
  BEGIN      
         
   SELECT @IsPaid = IsNull(Paid,0), @IsCancel = IsNull(Cancel ,0), @IsStop = IsNull(Stop ,0), @IsOverLimit = IsNull(OverLimit ,0),      
      @IsLost = IsNull(Lost ,0), @IsErase = IsNull(Erase ,0) From t_LR_Issuance      
   WHERE OurBranchID = @OurBranchID AND InstrumentType = @InstrumentType AND InstrumentNo = @InstrumentNo       
    AND ControlNo = @ControlNo AND (wDate = @IssueDate Or DuplicateDate = @IssueDate)       
    AND Amount = @Amount AND Supervision = 'C'      
      
   IF @IsPaid = 1      
    RETURN (21) -- Paid        
      
   IF @IsCancel = 1      
    RETURN (22) -- cancel      
      
   IF @IsLost = 1      
    RETURN (23) -- Lost      
      
   IF @IsStop = 1      
    RETURN (24) -- stop      
      
   IF @IsErase = 1      
    RETURN (25) -- Erase      
      
   IF @IsOverLimit = 1      
    RETURN (26) -- Instrument marked as Still, Revalidate It First From The Issue Branch...      
      
   RETURN (0)      
  END      
 ELSE      
  RETURN (20) -- Instrument Not Found try again 