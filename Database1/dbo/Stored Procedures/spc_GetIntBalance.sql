﻿CREATE PROCEDURE [dbo].[spc_GetIntBalance]        
(        
 @OurBranchID varchar(30)='',          
 @AccountID varchar(30)='',          
 @ProductID varchar(30)='',                  
 @FromDate datetime,          
 @ToDate datetime,          
    @StartDate datetime='01/01/1900',          
    @EndDate datetime='01/01/1900',          
    @IntProcedure varchar(30)=''          
)        
AS        
BEGIN        
     
 set nocount on    
     
 DECLARE @Amount as money          
 DECLARE @ForeignAmount as money        
 DECLARE @vAmount as money          
 DECLARE @vForeignAmount as money        
    DECLARE @LocalCurrency as varchar(20)          
 DECLARE @CurrencyID as varchar(30)          
          
 SELECT @LocalCurrency =UPPER(LocalCurrency) From t_GLobalVariables WHERE OurBranchID = @OurBranchID   
         
 SELECT @CurrencyID =UPPER(CurrencyID) From t_Products        
 WHERE OurBranchID = @OurBranchID AND ProductID = @ProductID        
          
 SELECT @Amount = 0, @ForeignAmount = 0, @vAmount = 0, @vForeignAmount = 0        
          
 IF @StartDate = '01/01/1900' OR @EndDate = '01/01/1900'        
 BEGIN        
  SELECT @Amount = 0, @ForeignAmount = 0          
 END        
 ELSE        
 BEGIN        
  IF @FromDate > @StartDate        
  BEGIN        
   SELECT @Amount = 0, @ForeignAmount = 0        
  END        
  ELSE        
  BEGIN        
   IF @IntProcedure = '001'        
   BEGIN        
    SELECT @Amount = IsNull(Sum(Amount),0), @ForeignAmount = IsNull(Sum(ForeignAmount),0)        
    FROM t_Transactions        
    WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID AND TrxType = 'C'        
    AND wDate >= @StartDate AND wDate <= @EndDate AND ValueDate < @FromDate          
            
    SELECT @Amount = @Amount + IsNull(Sum(Amount),0),        
    @ForeignAmount = @ForeignAmount  + IsNull(Sum(ForeignAmount),0)        
    FROM t_TransferTransactionModel        
    WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID AND TrxType = 'C'        
    AND ValueDate < @StartDate AND Supervision = 'C'        
            
    SELECT @Amount = @Amount - IsNull(Sum(Amount),0),        
    @ForeignAmount = @ForeignAmount - IsNull(Sum(ForeignAmount),0)        
    FROM t_Transactions        
    WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID AND TrxType = 'D'        
    AND wDate >= @StartDate AND wDate <= @EndDate AND ValueDate < @FromDate        
            
    SELECT @Amount = @Amount - IsNull(Sum(Amount),0),        
    @ForeignAmount = @ForeignAmount  - IsNull(Sum(ForeignAmount),0)        
    FROM t_TransferTransactionModel        
    WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID AND TrxType = 'D'         
    AND ValueDate < @StartDate AND Supervision = 'C'          
   END          
  END        
 END        
          
 IF @IntProcedure <> '001'        
 BEGIN        
  SELECT @vAmount = IsNull(Sum(Amount),0), @vForeignAmount = IsNull(Sum(ForeignAmount),0)        
  FROM t_Transactions        
  WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID AND TrxType = 'C'         
  AND wDate >= @FromDate AND wDate <= @ToDate AND ValueDate < @FromDate        
        
  SELECT @vAmount = @vAmount + IsNull(Sum(Amount),0),        
  @vForeignAmount = @vForeignAmount  + IsNull(Sum(ForeignAmount),0)        
  FROM t_TransferTransactionModel        
  WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID AND TrxType = 'C'        
  AND ValueDate < @FromDate AND Supervision = 'C'        
          
  SELECT @vAmount = @vAmount - IsNull(Sum(Amount),0),        
  @vForeignAmount = @vForeignAmount - IsNull(Sum(ForeignAmount),0)        
  FROM t_Transactions        
  WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID AND TrxType = 'D'        
  AND wDate >= @FromDate AND wDate <= @ToDate        
  AND ValueDate < @FromDate        
          
  SELECT @vAmount = @vAmount - IsNull(Sum(Amount),0),        
  @vForeignAmount = @vForeignAmount  - IsNull(Sum(ForeignAmount),0)        
  FROM t_TransferTransactionModel        
  WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID AND TrxType = 'D'        
  AND ValueDate < @FromDate AND Supervision = 'C'        
 END        
         
 IF @IntProcedure = '006' OR @IntProcedure = '106' OR @IntProcedure = '206' OR @IntProcedure = '306'        
  OR @IntProcedure = '406' OR @IntProcedure = '506' OR @IntProcedure = '606' OR @IntProcedure = '706'         
  OR @IntProcedure = '806' OR @IntProcedure = '906'        
 BEGIN        
  SELECT @Amount = isNull(Sum(Amount),0) , @ForeignAmount =IsNull( Sum(ForeignAmount),0)        
  From t_Transactions        
  WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID         
  AND ValueDate = @FromDate AND TrxType = 'C'          
        
  SELECT @Amount = @Amount - IsNull(Sum(Amount),0) , @ForeignAmount = @ForeignAmount - IsNull(Sum(ForeignAmount),0)        
  From t_Transactions          
  WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID         
  AND ValueDate = @FromDate AND TrxType = 'D'          
          
  IF @Amount < 0        
   SET @Amount = 0        
        
  IF @ForeignAmount < 0         
   SET @ForeignAmount = 0        
 END        
        
 IF @LocalCurrency = @CurrencyID        
 BEGIN        
  SELECT [Month], OurBranchID, AccountID, ProductID, Name,         
  (ClearBalance + @Amount + @vAmount) as ClearBalance, Effects, LocalClearBalance, LocalEffects        
  From t_AccountMonthBalances        
  WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID        
  AND [Month] BETWEEN @FromDate AND @ToDate        
 END        
 ELSE        
 BEGIN        
  SELECT [Month], OurBranchID, AccountID, ProductID, Name,        
  (ClearBalance + @ForeignAmount + @vForeignAmount) as ClearBalance, Effects, LocalClearBalance, LocalEffects        
  From t_AccountMonthBalances        
  WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID        
  AND [Month] BETWEEN @FromDate AND @ToDate        
 END        
END