﻿CREATE PROCEDURE [dbo].[spc_GetValueDate] (      
 @OurBranchID nvarchar(30),
 @ClearingType char(1)
)      
AS      
BEGIN      
      
 SET NOCOUNT ON      
       
 Declare @WorkingDate datetime      
 Declare @ValueDate datetime      
 Declare @ClearDays decimal(8,0)      
 Declare @C decimal(8,0)      
         
 set @C = 0      
       
 select @WorkingDate = WorkingDate from t_Last  WHere OurBranchID = @OurBranchID    
 select @ClearDays = case @ClearingType when 'I' then isnull(InterCityClearDays, 0) else
 isnull(ClearDays, 0) end from t_GlobalVariables where OurBranchID = @OurBranchID        
 select @ValueDate = dateadd(day,1,@WorkingDate)      
       
 WHILE (@C < @ClearDays)        
 BEGIN        
  IF (select count(*) from t_holidays where OurBranchID = @OurBranchID and date = @ValueDate) = 0         
  BEGIN      
   set @C = @C +1        
  END      
  ELSE      
  BEGIN      
   set @C = @C      
  END      
        
  IF @C >= @ClearDays         
  BEGIN      
   break      
  END      
        
  select @ValueDate = dateadd(day,1,@ValueDate)       
 END        
 
 select @ValueDate as ValueDate
 
END