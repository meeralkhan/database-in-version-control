﻿CREATE   PROCEDURE [dbo].[spc_GetTotalCRBalances]      
 @OurBranchID varchar(30),        
 @ProductID varchar(30),        
 @CurrencyID varchar(30),        
 @LDBTotalBalance money=0,        
 @LCRTotalBalance money=0,        
 @FDBTotalBalance money=0,        
 @FCRTotalBalance money=0,        
 @LocalCurrency varchar(30)=''        
 AS      
       
 SET NOCOUNT ON      
       
 Select @LocalCurrency = LocalCurrency from t_GlobalVariables where OurBranchID = @OurBranchID    
       
 BEGIN        
  if @CurrencyID = @LocalCurrency        
  BEGIN        
        
   Select @LDBTotalBalance = SUM(ClearBalance) from t_AccountBalance        
   Where (OurBranchID = @OurBranchID)  and (ProductID = @ProductID) and ((ClearBalance) > 0)          
        
   Select @LCRTotalBalance = SUM(ClearBalance) from t_AccountBalance        
   Where (OurBranchID = @OurBranchID)  and (ProductID = @ProductID) and ((ClearBalance) < 0)          
  END        
  else        
  BEGIN        
   Select  @LDBTotalBalance=SUM(LocalClearBalance) from t_AccountBalance        
   Where (OurBranchID = @OurBranchID)  and (ProductID = @ProductID) and LocalClearBalance > 0        
        
   Select @LCRTotalBalance = SUM(LocalClearBalance) from t_AccountBalance        
   Where (OurBranchID = @OurBranchID)  and (ProductID = @ProductID) and ((LocalClearBalance) < 0)          
        
   Select @FDBTotalBalance = SUM(ClearBalance) from t_AccountBalance        
   Where (OurBranchID = @OurBranchID)  and (ProductID = @ProductID) and ((ClearBalance) > 0)          
        
   Select @FCRTotalBalance = SUM(ClearBalance) from t_AccountBalance        
   Where (OurBranchID = @OurBranchID)  and (ProductID = @ProductID) and ((ClearBalance) < 0)          
  END        
        
  END        
        
 IF @LDBTotalBalance is null        
  select @LDBTotalBalance = 0        
        
 IF @LCRTotalBalance is null        
  select @LCRTotalBalance = 0        
        
 IF @FDBTotalBalance is null        
  select @FDBTotalBalance = 0        
        
 IF @FCRTotalBalance is null        
  select @FCRTotalBalance = 0        
        
 Select  @LDBTotalBalance as  LDBTotalBalance, @LCRTotalBalance as LCRTotalBalance, @FDBTotalBalance as FDBTotalBalance,     
 @FCRTotalBalance as FCRTotalBalance, @LocalCurrency as LocalCurrency