﻿CREATE PROCEDURE [dbo].[spc_RptGetStatementOfCon]
(              
 @OurBranchID varchar(30),
 @WorkDate AS DATETIME='1/1/1900'        
 --@DROPZERO AS INT=1              
)              
AS            
  set nocount on            
              
  --set @WorkDate=dateadd(dd,1,@WorkDate)              
              
 select a.OurBranchid, a.Accountid,a.Description,     
 OpeningBalance=
	a.openingbalance +     
	isnull( ( select sum(B.CrAmount)    
           from vc_rptgetstatementofcon B     
           where a.OurBranchid=B.OurBranchid and a.Accountid=B.Accountid and B.valuedate < @WorkDate )    
           -     
           (select sum(B.DrAmount)    
           from vc_rptgetstatementofcon B     
           where a.OurBranchid=B.OurBranchid and a.Accountid=b.Accountid and B.valuedate < @WorkDate ) ,0),    
 DrAmount = ( select isnull(sum(B.DrAmount), 0)    
              from vc_rptgetstatementofcon B     
              where a.OurBranchid=B.OurBranchid and a.Accountid=B.Accountid and B.valuedate = @WorkDate ),    
 CrAmount = ( select isnull(sum(B.CrAmount) ,0)    
              from vc_rptgetstatementofcon B     
              where a.OurBranchid=b.OurBranchid and a.Accountid=b.Accountid and B.valuedate =@WorkDate )    
 from vc_rptgetstatementofcon a    
 WHERE OurBranchId = @OurBranchID    
 group by a.OurBranchid,a.Accountid,a.Description,a.openingbalance              
              
 --union              
              
 -- select ourbranchid,Accountid,Description,openingbalance,dramount=convert(money,lastdebit),              
 -- cramount=convert(money,lastcredit) from t_GLCashBalance              
 -- order by a.Accountid              
              
              
              
             
              
      /*        
IF @DROPZERO=1              
              
 SELECT * FROM(              
 SELECT   T_GL.OURBRANCHID,T_GL.ACCOUNTID,T_GL.DESCRIPTION,ISNULL(T.OPENINGBALANCE,0)AS OPENINGBALANCE,ISNULL(T.CRAMOUNT,0)AS CRAMOUNT,ISNULL(T.DRAMOUNT,0) AS DRAMOUNT,T_GL.BALANCE,T_GL.CURRENCYID              
 FROM T_GL              
 LEFT OUTER JOIN              
               
 (              
 SELECT A.OURBRANCHID,A.ACCOUNTID,A.OPENINGBALANCE,ISNULL(B.CRAMOUNT,0) AS CRAMOUNT,ISNULL(DRAMOUNT,0) AS DRAMOUNT              
 FROM              
 (SELECT ISNULL(CR.OURBRANCHID,DR.OURBRANCHID)AS OURBRANCHID,ISNULL(CR.ACCOUNTID,DR.ACCOUNTID)AS ACCOUNTID,(ISNULL(CR.AMT,0)-ISNULL(DR.AMT,0)) AS OPENINGBALANCE              
 FROM              
 (SELECT OURBRANCHID,ACCOUNTID,SUM(AMOUNT) AS AMT FROM T_GLTRANSACTIONS WHERE VALUEDATE<dateadd(dd,1,@WorkDate) AND ISCREDIT=1 GROUP BY ACCOUNTID,OURBRANCHID) AS CR              
 FULL OUTER JOIN              
 (SELECT OURBRANCHID,ACCOUNTID,SUM(AMOUNT) AS AMT FROM T_GLTRANSACTIONS WHERE VALUEDATE<dateadd(dd,1,@WorkDate) AND ISCREDIT=0 GROUP BY ACCOUNTID,OURBRANCHID) DR              
 ON CR.OURBRANCHID=DR.OURBRANCHID AND CR.ACCOUNTID=DR.ACCOUNTID              
 ) AS A              
 LEFT OUTER JOIN               
 --------------------------------------------------------------------------------              
 (SELECT ISNULL(CR.OURBRANCHID,DR.OURBRANCHID)AS OURBRANCHID,ISNULL(CR.ACCOUNTID,DR.ACCOUNTID) AS ACCOUNTID,SUM(ISNULL(CR.AMOUNT,0))AS CRAMOUNT,SUM(ISNULL(DR.AMOUNT,0)) AS DRAMOUNT              
 FROM              
 (SELECT OURBRANCHID,ACCOUNTID,AMOUNT=sum(AMOUNT) FROM T_GLTRANSACTIONS WHERE VALUEDATE=dateadd(dd,1,@WorkDate) AND ISCREDIT=1              
 group by OURBRANCHID,ACCOUNTID) AS CR              
 FULL OUTER JOIN              
 (SELECT OURBRANCHID,ACCOUNTID,AMOUNT=sum(AMOUNT) FROM T_GLTRANSACTIONS WHERE VALUEDATE=dateadd(dd,1,@WorkDate) AND ISCREDIT=0              
 group by OURBRANCHID,ACCOUNTID) DR              
               
 ON CR.OURBRANCHID=DR.OURBRANCHID AND CR.ACCOUNTID=DR.ACCOUNTID              
 GROUP BY CR.OURBRANCHID,CR.ACCOUNTID,DR.OURBRANCHID,DR.ACCOUNTID              
 ) AS B              
 ON A.ACCOUNTID=B.ACCOUNTID              
 ) AS T              
               
 ON T_GL.ACCOUNTID=T.ACCOUNTID              
               
 ) AS GTAB              
               
 WHERE BALANCE<>0  --and  Accountid not in (select accountid from t_GLCashBalance)              
      
 --union            
              
 --select ourbranchid,Accountid,Description=Description,openingbalance=10500000,dramount=convert(money,lastdebit),              
 --cramount=convert(money,lastcredit),balance=0,currencyid='PKR'              
              
 --from t_GLCashBalance              
              
ELSE              
              
 SELECT   T_GL.OURBRANCHID,T_GL.ACCOUNTID,T_GL.DESCRIPTION,ISNULL(T_GL.OPENINGBALANCE,0)AS OPENINGBALANCE,ISNULL(T.CRAMOUNT,0)AS CRAMOUNT,ISNULL(T.DRAMOUNT,0) AS DRAMOUNT,T_GL.BALANCE,T_GL.CURRENCYID              
 FROM T_GL              
 LEFT OUTER JOIN              
               
 (              
 SELECT A.OURBRANCHID,A.ACCOUNTID,A.OPENINGBALANCE,ISNULL(B.CRAMOUNT,0) AS CRAMOUNT,ISNULL(DRAMOUNT,0) AS DRAMOUNT              
 FROM              
 (SELECT ISNULL(CR.OURBRANCHID,DR.OURBRANCHID)AS OURBRANCHID,ISNULL(CR.ACCOUNTID,DR.ACCOUNTID)AS ACCOUNTID,(ISNULL(CR.AMT,0)-ISNULL(DR.AMT,0)) AS OPENINGBALANCE              
 FROM              
 (SELECT OURBRANCHID,ACCOUNTID,SUM(AMOUNT) AS AMT FROM T_GLTRANSACTIONS WHERE VALUEDATE<=dateadd(dd,1,@WorkDate) AND ISCREDIT=1 GROUP BY ACCOUNTID,OURBRANCHID) AS CR              
 FULL OUTER JOIN              
 (SELECT OURBRANCHID,ACCOUNTID,SUM(AMOUNT) AS AMT FROM T_GLTRANSACTIONS WHERE VALUEDATE<=dateadd(dd,1,@WorkDate) AND ISCREDIT=0 GROUP BY ACCOUNTID,OURBRANCHID) DR              
 ON CR.OURBRANCHID=DR.OURBRANCHID AND CR.ACCOUNTID=DR.ACCOUNTID              
 ) AS A              
 LEFT OUTER JOIN               
 --------------------------------------------------------------------------------              
 (SELECT ISNULL(CR.OURBRANCHID,DR.OURBRANCHID)AS OURBRANCHID,ISNULL(CR.ACCOUNTID,DR.ACCOUNTID) AS ACCOUNTID,SUM(ISNULL(CR.AMOUNT,0))AS CRAMOUNT,SUM(ISNULL(DR.AMOUNT,0)) AS DRAMOUNT              
 FROM              
 (SELECT OURBRANCHID,ACCOUNTID,AMOUNT=sum(amount) FROM T_GLTRANSACTIONS WHERE VALUEDATE=dateadd(dd,1,@WorkDate) AND ISCREDIT=1              
 group by OURBRANCHID,ACCOUNTID) AS CR              
 FULL OUTER JOIN              
 (SELECT OURBRANCHID,ACCOUNTID,AMOUNT=sum(amount) FROM T_GLTRANSACTIONS WHERE VALUEDATE=dateadd(dd,1,@WorkDate) AND ISCREDIT=0              
 group by OURBRANCHID,ACCOUNTID) DR              
               
 ON CR.OURBRANCHID=DR.OURBRANCHID AND CR.ACCOUNTID=DR.ACCOUNTID              
 GROUP BY CR.OURBRANCHID,CR.ACCOUNTID,DR.OURBRANCHID,DR.ACCOUNTID              
 ) AS B              
 ON A.ACCOUNTID=B.ACCOUNTID              
 ) AS T              
               
 ON T_GL.ACCOUNTID=T.ACCOUNTID --and  T_GL.Accountid not in (select accountid from t_GLCashBalance)              
          
         
 union              
          
 select ourbranchid,Accountid,Description=Description,openingbalance=10500000,dramount=convert(money,lastdebit),              
 cramount=convert(money,lastcredit),balance=0,currencyid='PKR'              
              
 from t_GLCashBalance              
    */