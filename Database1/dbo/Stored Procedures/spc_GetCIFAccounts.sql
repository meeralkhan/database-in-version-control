﻿create procedure [dbo].[spc_GetCIFAccounts]    
(    
   @OurBranchID nvarchar(30),    
   @ClientID nvarchar(30)    
)    
AS    
    
SET NOCOUNT ON;    
    
select ClientID, AccountID, IBAN, Name, ProductID, CurrencyID, case ISNULL(Status,'')    
when '' then 'Active'    
when 'I' then 'InActive'    
when 'T' then 'Dormant'    
when 'X' then 'Deceased'    
when 'D' then 'Blocked'    
when 'C' then 'Closed'    
else 'Undefined' end AS [Status]  ,  
ISNULL(AllowDebitTransaction,0) AS DebitFrozen,  
ISNULL(AllowCreditTransaction,0) AS CreditFrozen  
from t_Account    
where OurBranchID = @OurBranchID    
and ClientID = @ClientID    
and ProductID IN (select ProductID from t_Products where OurBranchID = @OurBranchID and ProductType IN ('L','AE') and ISNULL(IsTermDeposit,'No')='No')