﻿CREATE PROCEDURE [dbo].[spc_AdvGetDealIssuanceReport]    
 (          
 @OurBranchID varchar(30)='',          
 @ToDate datetime,          
 @FromDate datetime,          
 @Accountid varchar(30)=''          
 )          
AS          
 set nocount on        
       
BEGIN          
        
 SELECT DISTINCT           
     t_Disbursement.AccountID, t_Disbursement.OurBranchID,           
     t_Account.Name, t_Disbursement.DealID,           
     t_Disbursement.Amount,         
     ProfitRate = case        
  when ProfitRateType = 'Fixed' then ProfitRate        
  when ProfitRateType = 'Floating' then BaseRate        
  end,        
     t_Disbursement.AdvancesDate, t_Disbursement.MaturityDate,           
     t_Products.Description, t_Disbursement.CloseDate,           
     --t_Disbursement.TaxAmount,         
     t_Disbursement.DisbursementDate, Status=ISNULL(t_Disbursement.Status,space(1)),          
     t_Products.CurrencyID           
 FROM t_Disbursement         
 INNER JOIN t_Account ON t_Disbursement.OurBranchID = t_Account.OurBranchID and         
 t_Disbursement.AccountID = t_Account.AccountID        
 INNER JOIN t_Products ON t_Disbursement.OurBranchID = t_Products.OurBranchID AND           
 t_Account.ProductID = t_Products.ProductID         
  WHERE t_Disbursement.OurBranchID=@OurBranchID AND           
 (t_Disbursement.AdvancesDate BETWEEN @FromDate AND @ToDate) and isnull(IsClosed,0)<>'1'          
 ORDER BY t_Disbursement.AdvancesDate          
END