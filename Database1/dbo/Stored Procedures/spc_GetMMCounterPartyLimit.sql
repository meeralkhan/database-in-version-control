﻿CREATE PROCEDURE [dbo].[spc_GetMMCounterPartyLimit]    
(    
  @OurBranchID varchar(30),    
  @PartyID varchar(30),    
  @DealType varchar(2)      
)    
AS    
     
 IF @DealType='RE'    
    
  SELECT 'Ok' AS ReturnStatus, PartyDescription,ClientID, IsCentralBank,IsGroupLimits, RepoLimit  as Limit From t_MoneyMarketAccountM    
  WHERE OurBranchID = @OurBranchID AND PartyID = @PartyID And Type='N'    
    
 ELSE IF @DealType = 'RR'    
    
  SELECT 'Ok' AS ReturnStatus, PartyDescription, ClientID, IsCentralBank,IsGroupLimits,ReverseRepoLimit  as Limit From t_MoneyMarketAccountM    
  WHERE OurBranchID = @OurBranchID AND PartyID = @PartyID And Type='N'    
    
 ELSE IF @DealType = 'CB'    
    
  SELECT 'Ok' AS ReturnStatus, PartyDescription, ClientID, IsCentralBank,IsGroupLimits,CBLimit  as Limit From t_MoneyMarketAccountM    
  WHERE OurBranchID = @OurBranchID AND PartyID = @PartyID And Type='N'    
    
 ELSE IF @DealType = 'CL'  OR @DealType = 'CN'   OR @DealType = 'CI'  OR @DealType = 'BL'     
    
  SELECT 'Ok' AS ReturnStatus, PartyDescription, ClientID, IsCentralBank,IsGroupLimits,CLLimit  as Limit From t_MoneyMarketAccountM    
  WHERE OurBranchID = @OurBranchID AND PartyID = @PartyID And Type='N'    
    
 ELSE IF @DealType = 'FB'    
    
  SELECT 'Ok' AS ReturnStatus, PartyDescription, ClientID, IsCentralBank,IsGroupLimits,FXBLimit  as Limit From t_MoneyMarketAccountM    
  WHERE OurBranchID = @OurBranchID AND PartyID = @PartyID And Type='N'    
    
 ELSE IF @DealType = 'FL'    
    
  SELECT 'Ok' AS ReturnStatus, PartyDescription, ClientID, IsCentralBank,IsGroupLimits,FXlLimit  as Limit From t_MoneyMarketAccountM    
  WHERE OurBranchID = @OurBranchID AND PartyID = @PartyID And Type='N'    
    
 ELSE IF @DealType = 'OP' OR @DealType = 'OS'    
    
  SELECT 'Ok' AS ReturnStatus, PartyDescription, ClientID, IsCentralBank,IsGroupLimits,9999999999999  as Limit From t_MoneyMarketAccountM    
  WHERE OurBranchID = @OurBranchID AND PartyID = @PartyID And Type='N'    
  
 ELSE
  
  SELECT 'Error' AS ReturnStatus, 'Invalid Event' AS ReturnMessage