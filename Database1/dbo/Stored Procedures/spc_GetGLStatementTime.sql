﻿CREATE PROCEDURE [dbo].[spc_GetGLStatementTime]        
(                 
 @OurBranchID varchar(30),                 
 @AccountID varchar(30),                 
 @sDate Datetime,                 
 @eDate Datetime                 
)                 
                 
AS                 
                 
 SET NOCOUNT ON                 
                 
 DECLARE @OpeningBalance as money                 
 DECLARE @crOpBal as money                 
 DECLARE @drOpBal as money                 
     
 DECLARE @CurrencyID as nvarchar(30)    
 set @CurrencyID = ''      
    
 --DECLARE @WorkingDate as datetime    
 --SELECT @WorkingDate = WorkingDate From t_Last WHERE OurBranchID = @OurBranchID    
    
 SET @OpeningBalance = 0                 
 SET @crOpBal = 0                 
 SET @drOpBal = 0                 
                 
                 
                 
 SELECT @CurrencyID = CurrencyID, @OpeningBalance = IsNull(OpeningBalance,0)                 
 FROM t_GL                 
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                 
                 
 SELECT @crOpBal = IsNull(SUM(Amount),0) From t_GLTransactions                 
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                 
 AND IsCredit = '1' and [Status] <> 'R'            
 AND convert(varchar(10), Date, 101) < @sDate                 
                 
 SELECT @drOpBal = IsNull(SUM(Amount),0) From t_GLTransactions                 
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                 
 AND IsCredit = '0' and [Status] <> 'R'            
 AND convert(varchar(10), Date, 101) < @sDate                 
                 
 SET @OpeningBalance = @OpeningBalance + @crOpBal - @drOpbal                 
    
 SELECT @OurBranchID AS OurBranchID, @AccountID AS AccountID, @CurrencyID AS CurrencyID, IsNull(@OpeningBalance,0) as Amount, 0 AS ForeignAmount, 0 AS IsCredit,      
 @sDate as ValueDate, '01/01/1900' as [Date], '' AS ChannelRefID, '' As TrxType, 0 AS VoucherID, 0 AS SerialID, '' AS DescriptionID, 'Opening Balance' as [Description],       
 GETDATE() AS TrxTimeStamp, 'OP' As TableStr      
       
 UNION ALL                   
                   
 SELECT @OurBranchID AS OurBranchID, @AccountID AS AccountID, @CurrencyID AS CurrencyID, Amount, 0 AS ForeignAmount, IsCredit,      
 ValueDate, Date as wDate, ChannelRefID, CASE IsCredit WHEN 1 THEN 'C' ELSE 'D' END As TrxType, VoucherID, SerialID,      
 DescriptionID, Description, TrxTimeStamp,      
 'H' As TableStr      
 FROM t_GLTransactions      
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID  and [Status] <> 'R'      
 AND convert(varchar(10), Date, 101) >= @sDate AND convert(varchar(10), Date, 101) <= @eDate