﻿
CREATE PROCEDURE [dbo].[spc_GetGLTransactionVoucher]    
(    
    @OurBranchID    varchar(30),           
    @VoucherId_From numeric(9,0)=0,      
    @VoucherId_To   numeric(9,0)=0,      
    @FromDate       datetime=null,        
    @ToDate         datetime=null         
)       
AS          
      
 set nocount on          
              
 Declare @strQRY nvarchar(max)    
 Declare @VoucherDate_From varchar(10)    
 Declare @VoucherDate_To varchar(10)    
    
 Select @VoucherDate_From = isnull(Convert(Char,@FromDate,120),'')    
 Select @VoucherDate_To = isnull(Convert(Char,@ToDate,120),'')    
 
 SET @strQry = ' SELECT OurBranchID, OperatorID, VoucherID, Description, ValueDate, SerialID, AccountID, AccountName, CurrencyName, ExchangeRate, Amount, IsCredit, DescriptionID, CurrencyID from vc_GLTransactionsVoucher '     
    
 If (@OurBranchID     <> '') Or (@VoucherId_From  <> 0) Or (@VoucherId_To    <> 0)  Or (@VoucherDate_From <> '') Or (@VoucherDate_To <> '')    
 BEGIN    
   SET @strQry = @strQry + ' Where '              
 END    
    
 If @OurBranchID  <> ''    
 BEGIN    
   SET @strQry = @strQry  + ' OurBranchID = ' + + '''' + @OurBranchID + ''''    
 END    
       
 If @VoucherId_From <> 0    
 BEGIN    
   If @OurBranchID  <> ''    
   BEGIN     
     SET @strQry = @strQry  + ' And '              
   END    
         
   SET @strQry = @strQry  + ' VoucherID >= ' + Convert(varchar(10),@VoucherId_From)    
 End    
       
 if @VoucherId_To <> 0    
 BEGIN    
       
   If (@OurBranchID  <> '') Or (@VoucherId_From <> 0)    
   BEGIN     
     SET @strQry = @strQry  + ' And '    
   END    
    
   SET @strQry = @strQry  + ' VoucherID <= ' + Convert(varchar(10),@VoucherId_To)    
 End    
    
 if @VoucherDate_From <> ''    
 BEGIN    
         
   If (@OurBranchID  <> '') Or (@VoucherId_From <> 0) Or (@VoucherId_To <> 0)      
   BEGIN     
     SET @strQry = @strQry  + ' And '    
   END    
    
   SET @strQry = @strQry  + ' convert(varchar(10), Date, 120) >= ' + '''' + @VoucherDate_From + ''''    
 End    
    
 if @VoucherDate_To <> ''    
 BEGIN    
   If (@OurBranchID  <> '') Or (@VoucherId_From <> 0) Or (@VoucherId_To <> 0)  Or (@VoucherDate_From <> '')    
   BEGIN     
     SET @strQry = @strQry  + ' And '    
   END    
         
   SET @strQry = @strQry  + ' convert(varchar(10), Date, 120) <= ' + '''' + @VoucherDate_To + ''''    
 End      
      
 SET @strQry = @strQry + ' UNION ALL '              
 SET @strQry = @strQry + ' SELECT OurBranchID, OperatorID, VoucherID, Description, ValueDate, SerialID, AccountID, AccountName, CurrencyName, ExchangeRate, Amount, '    
 SET @strQry = @strQry + ' IsCredit = Case TrxType When ''C'' then ''1'' When ''D'' then ''0'' Else ''Not Defined'' End, DescriptionID, CurrencyID from vc_GetTransactionsVoucher '              
    
    
 If (@OurBranchID <> '') Or (@VoucherId_From <> 0) Or (@VoucherId_To <> 0) Or (@VoucherDate_From <> '') Or (@VoucherDate_To <> '')    
 BEGIN    
   SET @strQry = @strQry + ' Where '              
 END      
       
 If @OurBranchID  <> ''    
 BEGIN    
   SET @strQry = @strQry  + ' OurBranchID = ' + + '''' + @OurBranchID + ''''      
 END    
    
 If @VoucherId_From <> 0    
 BEGIN    
       
   If @OurBranchID  <> ''    
   BEGIN     
     SET @strQry = @strQry  + ' And '      
   END    
         
   SET @strQry = @strQry  + ' VoucherID >= ' +  '''' + Convert(varchar(10),@VoucherId_From) + ''''     
 End      
       
 if @VoucherId_To <> 0    
 BEGIN    
       
   If (@OurBranchID  <> '') Or (@VoucherId_From <> 0)    
   BEGIN     
     SET @strQry = @strQry  + ' And '      
   END    
         
   SET @strQry = @strQry  + ' VoucherID <= ' +  '''' + Convert(varchar(10),@VoucherId_To) +  ''''     
         
 End    
         
 if @VoucherDate_From <> ''    
 BEGIN    
         
   If (@OurBranchID  <> '') Or (@VoucherId_From <> 0) Or (@VoucherId_To <> 0)      
   BEGIN     
           
     SET @strQry = @strQry  + ' And '      
   END    
         
   SET @strQry = @strQry  + ' convert(varchar(10), Date, 120) >= ' + '''' + @VoucherDate_From + ''''      
 End    
    
    
 if @VoucherDate_To <> ''    
 BEGIN    
       
   If (@OurBranchID  <> '') Or (@VoucherId_From <> 0) Or (@VoucherId_To <> 0)  Or (@VoucherDate_From <> '')    
   BEGIN     
     SET @strQry = @strQry  + ' And '              
   END    
         
   SET @strQry = @strQry  + ' convert(varchar(10), Date, 120) <= ' + '''' + @VoucherDate_To + ''''    
      
 End         
 SET @strQry = @strQry + ' ORDER BY OurBranchID, VoucherID, SerialID'              
       
 EXECUTE sp_executesql @strQry