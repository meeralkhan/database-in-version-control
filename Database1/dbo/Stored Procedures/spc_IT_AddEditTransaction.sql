﻿CREATE PROCEDURE [dbo].[spc_IT_AddEditTransaction]    
(    
 @OurBranchID varchar(30),           
 @CustomerAccountID varchar(30),           
 @CustomerAccountName varchar(100),           
 @CustomerProductID varchar(30),           
 @CustomerCurrencyID varchar(30),           
 @CustomerAccountType char(1),           
 @CustomerTrxType char(1),           
 @CustomerIsLocalCurrency Char(1),           
 @ValueDate datetime,           
 @wDate datetime='',           
 @Amount money,           
 @ForeignAmount Money,           
 @ExchangeRate decimal(18,6),           
 @DescriptionID varchar(30),           
 @Description varchar(255),           
 @SourceBranch Varchar(30),           
 @TargetBranch varchar(30),           
 @Remarks varchar(1000),           
 @RefNo varchar(30),    
     
 @ScrollNo int,           
 @IBCANo int,    
 @SerialNo int    
)    
AS            
           
 DECLARE @mDate varchar(50)            
 DECLARE @AccountClass char(1)          
 DECLARE @IsPosting bit          
 DECLARE @AllowCrDr bit          
 DECLARE @IsFreezed bit          
 DECLARE @RetStatus as int      
 DECLARE @Status varchar(30)    
 Declare @TotalAmount money    
           
 set nocount on            
           
 BEGIN          
     
   SET @TotalAmount = @Amount    
   SELECT @wDate=WorkingDate from t_Last WHERE OurBranchID = @OurBranchID        
             
   SET @Description = @Description + ' --- ' + cast(GETDATE() as varchar(50))            
   SET @mDate = convert(varchar(10),@wDate,101) + ' ' + convert(varchar(20), getdate(),114)            
       
   declare @DailyTaxCalcAppWHTax as bit        
   set @DailyTaxCalcAppWHTax = 0        
               
   if @CustomerTrxType = 'D' AND @CustomerAccountType = 'C'         
   begin                  
       exec spc_GetCashParameterNew @OurBranchID, @Amount, @CustomerAccountID, @CustomerProductID        
               
       declare @ClubWHTaxAmt as money                  
       declare @WHTChargesAmount money                  
               
       select @DailyTaxCalcAppWHTax = AppWHTax, @WHTChargesAmount=ChargesAmount                  
       from temp_DailyTaxCalc                  
               
       if @DailyTaxCalcAppWHTax = 1                   
       begin                  
           set @ClubWHTaxAmt = @Amount + @WHTChargesAmount                  
               
           SET @TotalAmount = @ClubWHTaxAmt    
               
           EXEC @RetStatus = spc_AmountVerify @OurBranchID, @CustomerAccountID, @CustomerTrxType, @ClubWHTaxAmt,         
           @CustomerAccountType, 'N', '999999999999.99', 'COMPUTER', @Amount        
       end                  
       else                  
       begin                
           EXEC @RetStatus = spc_AmountVerify @OurBranchID, @CustomerAccountID, @CustomerTrxType, @Amount,         
           @CustomerAccountType, 'N', '999999999999.99', '', @Amount        
       end      
   end    
   else                  
   begin                
     if @CustomerProductID <> 'GL'          
     begin          
       EXEC @RetStatus = spc_AmountVerify @OurBranchID, @CustomerAccountID, @CustomerTrxType, @Amount,         
       'C', 'N', '999999999999.99', '', @Amount        
     end    
   end         
       
   if @RetStatus <> '97'    
   BEGIN    
      SELECT 'Error' AS RetStatus, 'ErrPostingTrx' As RetMessage          
      RETURN(0)          
   END    
             
   if @CustomerProductID = 'GL'          
   begin          
               
     IF (SELECT Count(AccountID) From t_GL WHERE OurBranchID=@OurBranchID AND AccountID=@CustomerAccountID) > 0          
     begin          
                 
       SELECT @AccountClass = AccountClass, @IsPosting = IsPosting From t_GL          
       WHERE OurBranchID=@OurBranchID AND AccountID=@CustomerAccountID          
                 
       IF UPPER(@AccountClass) <> 'P'          
       BEGIN          
         SELECT 'Error' AS RetStatus, 'NotAPostingAccount' As RetMessage          
         RETURN(0)          
       END          
                 
       IF @IsPosting = 0          
       BEGIN          
         SELECT 'Error' AS RetStatus, 'GLPostingNotAllowed' As RetMessage          
         RETURN(0)          
       END          
                 
       IF (SELECT Count(*) From t_Products WHERE OurBranchID = @OurBranchID AND           
       (GLControl=@CustomerAccountID OR GLDebitBalance = @CustomerAccountID)) > 0          
       BEGIN          
         SELECT 'Error' AS RetStatus, 'GLPostingNotAllowedControlAccount' As RetMessage          
         RETURN(0)          
       END          
                 
     end          
     ELSE          
     BEGIN          
       SELECT 'Error' AS RetStatus, 'AccountNotFound' As RetMessage          
       RETURN(0)          
     END                  
   end          
       
   declare @WHTScrollNo int                
       set @WHTScrollNo = 0                
       
   if @DailyTaxCalcAppWHTax = 1 AND @CustomerTrxType = 'D' AND @CustomerAccountType = 'C'         
   begin                  
      declare @WHTChargeRate money                  
      declare @WHTGLSerialID int                  
      declare @WHTAccountID varchar(15)                  
      declare @WHTDescription varchar(50)                  
      declare @WHTDescription2 varchar(255)                  
      declare @WHTCurrencyID varchar(4)             
           
      select @WHTScrollNo=isnull(Max(ScrollNo), 0) + 1 from t_InternetTransaction where OurBranchID = @OurBranchID          
              
      select @WHTChargeRate=ChargeRate, @WHTChargesAmount=ChargesAmount, @WHTGLSerialID=GLSerialID,                  
      @WHTAccountID=AccountID, @WHTDescription=[Description], @WHTCurrencyID=CurrencyID                  
      from temp_DailyTaxCalc                   
              
   end          
           
   if @WHTScrollNo = @ScrollNo                  
   begin                  
      set @WHTScrollNo = @WHTScrollNo + 1                        
   end          
       
       
   INSERT INTO t_InternetTransaction            
   (            
    ScrollNo, SerialNo, OurBranchID, AccountID, AccountName, ProductID, CurrencyID, AccountType, ValueDate, wDate,     
    TrxType, Amount, ForeignAmount, ExchangeRate, DescriptionID, [Description], IsLocalCurrency, SourceBranch,     
    TargetBranch, Remarks, RefNo, IBCANo, AppWHTax, WHTaxAmount, WHTaxAccountID, WHTaxScrollNo    
   )    
             
   VALUES          
   (            
    @ScrollNo, @SerialNo, @OurBranchID, @CustomerAccountID, @CustomerAccountName, @CustomerProductID,     
    @CustomerCurrencyID, @CustomerAccountType, @ValueDate, @mDate, @CustomerTrxType, @Amount, @ForeignAmount,    
    @ExchangeRate, @DescriptionID, @Description, @CustomerIsLocalCurrency, @SourceBranch, @TargetBranch,    
    @Remarks,@RefNo, @IBCANo, @DailyTaxCalcAppWHTax, @WHTChargesAmount, @WHTAccountID, @WHTScrollNo        
   )            
       
   if @DailyTaxCalcAppWHTax = 1 AND @CustomerTrxType = 'D' AND @CustomerAccountType = 'C' AND @CustomerIsLocalCurrency = '1'    
   begin                  
       --Customer                  
               
       set @WHTDescription2 = 'Charge ' + cast(@WHTChargeRate as varchar(100)) + '/- Withholding Tax On Internet Transfer Transaction(s) Amount: ' + cast(@Amount as varchar(50))         
                  
       INSERT INTO t_InternetTransaction        
       (                    
        ScrollNo, SerialNo, OurBranchID, AccountID, AccountName, ProductID, CurrencyID, AccountType, ValueDate, wDate,         
        TrxType, Amount, ForeignAmount, ExchangeRate, DescriptionID, [Description], IsLocalCurrency, SourceBranch,         
        TargetBranch, Remarks, RefNo, IBCANo, AppWHTax, WHTaxAmount, WHTaxAccountID, WHTaxScrollNo        
       )         
       VALUES         
       (        
        @WHTScrollNo, 1, @OurBranchID, @CustomerAccountID, @CustomerAccountName, @CustomerProductID, @CustomerCurrencyID,        
        @CustomerAccountType, @ValueDate, @mDate, @CustomerTrxType, @WHTChargesAmount, 0.00, 1.00, 'WH1',         
        @WHTDescription2, @CustomerIsLocalCurrency, @SourceBranch, @TargetBranch, @Remarks,         
        'WHTax-'+cast(@WHTScrollNo as varchar(10)), @IBCANo, 0, 0, '', ''        
       )        
               
       /* Get Current ClearBalance, Effective Balance and Limit */         
           
       EXEC @RetStatus = spc_CashUpdateBalance @OurBranchID, @CustomerAccountID, @WHTChargesAmount, 0, 1, 'C', 1, 0    
               
       -- GL                  
                     
       set @WHTDescription2 = 'Charge ' + cast(@WHTChargeRate as varchar(100)) + '/- Withholding Tax On Internet Transfer Transaction(s) While A/C# ' + @CustomerAccountID + ' A/C Name: ' + @CustomerAccountName + ' ProductID: ' + @CustomerProductID + ' And   Amount: ' + cast(@Amount as varchar(50))         
          
       INSERT INTO t_InternetTransaction        
       (         
        ScrollNo, SerialNo, OurBranchID, AccountID, AccountName, ProductID, CurrencyID, AccountType, ValueDate, wDate,         
        TrxType, Amount, ForeignAmount, ExchangeRate, DescriptionID, [Description], IsLocalCurrency, SourceBranch,         
        TargetBranch, Remarks, RefNo, IBCANo, AppWHTax, WHTaxAmount, WHTaxAccountID, WHTaxScrollNo        
       )         
       VALUES         
       (        
        @WHTScrollNo, 2, @OurBranchID, @WHTAccountID, @WHTDescription, 'GL', @WHTCurrencyID, 'G', @ValueDate, @mDate,         
        'C', @WHTChargesAmount, 0.00, 1.00, 'WH2', @WHTDescription2, @CustomerIsLocalCurrency, @SourceBranch,         
        @TargetBranch, @Remarks, 'WHTax-'+cast(@WHTScrollNo as varchar(10)), @IBCANo, 0, 0, '', ''        
       )        
               
   end     
       
   declare @IsDebit bit    
       
   if @CustomerAccountType = 'C' OR @CustomerAccountType = 'B'    
   Begin    
  if @CustomerTrxType = 'D'    
  BEGIN    
     SET @IsDebit = 1    
  end    
  else    
  begin    
     SET @IsDebit = 0    
  end    
      
     EXEC @RetStatus = spc_CashUpdateBalance @OurBranchID, @CustomerAccountID, @Amount, @ForeignAmount, @IsDebit,     
     'C', @CustomerIsLocalCurrency, 0    
         
   end    
             
   SELECT @Status=@@ERROR            
             
   IF @Status <> 0             
   BEGIN            
     SELECT 'Error' AS RetStatus, 'ChkStatus' As RetMessage, @Status AS Status          
     RETURN(0)          
   END              
             
   SELECT 'Ok' AS RetStatus, 'TrxPosted' As RetMessage, @IBCANo IBCANo, @ScrollNo ScrollNo, @RefNo RefNo    
   RETURN(0)          
             
 END