﻿CREATE PROCEDURE [dbo].[spc_AmountVerify]                    
(                      
    @OurBranchID  varchar(30)='',                      
    @AccountID    varchar(30)='',                      
    @TrxType      varchar(5)='',                      
    @Amount       money=0,                      
    @AccountType  char(1)='G',                      
    @Supervision  char(1)='C',                      
    @PostingLimit money=0,                      
    @OperatorID   varchar(30)='',                      
    @LocEq        money=0                      
)                      
                      
AS                      
                  
  SET NOCOUNT ON                  
                          
   DECLARE @Status as Char(1)                      
   DECLARE @ProductID as VarChar(30)                      
   DECLARE @ClearBalance as Money                      
   DECLARE @Effects as Money                      
   DECLARE @ShadowBalance as Money                      
   DECLARE @Limit as Money                      
                      
   DECLARE @BlockedAmount as Money                      
   DECLARE @MinAmount as Money                      
   DECLARE @VpOD as Money                      
                      
   DECLARE @IsFreezed as Bit                      
   DECLARE @CanGoCrDr as Bit                      
   DECLARE @AllowCrDr as Bit                      
   DECLARE @ByPassSupervision as Bit                      
   DECLARE @IsProductFreezed as Bit                      
 DECLARE @EveryTrxGoesInSupervision as Bit                      
                      
   SElECT @ByPassSupervision = 0, @EveryTrxGoesInSupervision = 0, @VpOD =0, @BlockedAmount = 0, @MinAmount =0                       
   , @Limit = 0, @ClearBalance = 0, @IsFreezed = 0, @Status = ''                      
                      
   IF @Supervision='N'                      
      SET @ByPassSupervision = 1                      
                      
   SET @Supervision='C'                      
------------------------------------------------------------------------------                              
                    
   IF @AccountType ='C' AND @TrxType='C'                      
   BEGIN                
     SELECT @Status=IsNull(Status,''), @ProductID = ProductID                
     From t_Account WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                
     IF IsNull(@Status,'')<> ''                
     BEGIN                
        IF @Status = 'C'                
           RETURN(15)--A/c is Closed                
        ELSE IF @Status = 'I'                
           RETURN(24)--A/c is InActive
        --ELSE IF @Status ='D' AND (@ByPassSupervision = 0 AND UPPER(@OperatorID) <> 'COMPUTER')            
        --   RETURN(16) -- A/c Is blocked Long time                
        --ELSE IF @Status ='X' AND (@ByPassSupervision = 0 AND UPPER(@OperatorID) <> 'COMPUTER')            
        --   RETURN(18)   -- A/c Mark Decessed                
  End                
                    
  SELECT @IsProductFreezed = IsFreezed,                
  @AllowCrDr = Case @TrxType WHEN 'C' THEN AllowCredit WHEN 'D' THEN AllowDebit END,                
 @CanGoCrDr = Case @TrxType WHEN 'C' THEN CanGoInCredit WHEN 'D' THEN CanGoInDebit END,                
 @MinAmount =IsNull( MinBalance,0)                
 From t_Products                
 WHERE OurBranchID = @OurBranchID AND ProductID = @ProductID                
           
 IF @IsProductFreezed = 1                
    RETURN(19) -- Product Is Freezed                
                 
 IF @AllowCrDr = 0                
    RETURN(21) -- In Product Condition Dr/Cr Trx Not Allowed                
                 
 IF (@ClearBalance + @Amount) > 0                
 BEGIN                
   IF @CanGoCrDr = 0                      
      BEGIN                
        IF (@ByPassSupervision = 0 AND UPPER(@OperatorID) <> 'COMPUTER')            
          RETURN(23) -- Cannot Go In Credit                
      END                
    END                   
                 
 SELECT @AllowCrDr = Case @TrxType WHEN 'C' THEN AllowCreditTransaction WHEN 'D' THEN AllowDebitTransaction END,                
 @EveryTrxGoesInSupervision = Case @TrxType WHEN 'C' THEN CreditNeedsSupervision WHEN 'D' THEN DebitNeedsSupervision END                
 From t_Account                
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                
                 
 IF @AllowCrDr = 1 AND (@ByPassSupervision = 0 AND UPPER(@OperatorID) <> 'COMPUTER')            
  RETURN(20) -- In Account Special Condition Dr/Cr Transaction Not Allowed                
                  
   END                
------------------------------------------------------------------------------                              
   IF @AccountType ='C' AND @TrxType='D'                      
      BEGIN                      
      --Checking A/c Status                      
                      
   SELECT @Status=IsNull(Status,''), @ProductID = ProductID                
            From t_Account                      
            WHERE   OurBranchID = @OurBranchID AND AccountID = @AccountID                      
                      
            IF IsNull(@Status,'')<>''                      
               BEGIN                      
                  IF @Status = 'C'                      
                     RETURN(15)--A/c is Closed                      
                  --ELSE IF @Status ='D' AND (@ByPassSupervision = 0 AND UPPER(@OperatorID) <> 'COMPUTER')            
                  --   RETURN(16) -- A/c Is blocked Long time                      
                  ELSE IF @Status ='T' AND (@ByPassSupervision = 0 AND UPPER(@OperatorID) <> 'COMPUTER')            
                     RETURN(17)   -- A/c Mark Dormant                      
                  --ELSE IF @Status ='X' AND (@ByPassSupervision = 0 AND UPPER(@OperatorID) <> 'COMPUTER')            
                  --   RETURN(18)   -- A/c Mark Decessed                      
				  ELSE IF @Status ='I' AND (@ByPassSupervision = 0 AND UPPER(@OperatorID) <> 'COMPUTER')            
                     RETURN(24)   -- A/c Mark In-Active        
               End                      
                      
    --exec @Limit = --fnc_GetLimit @OurBranchID, @AccountID      
      
            SELECT @ClearBalance=ClearBalance, @Effects=Effects,                      
                   @ShadowBalance=ShadowBalance, @Limit = Limit,                     
                   @BlockedAmount = CASE IsFreezed WHEN 1 THEN FreezeAmount                      
                                                   WHEN 0 THEN 0      END,                      
                   @IsFreezed =ABS( IsFreezed), @ProductID = ProductID                      
            From t_AccountBalance                      
            WHERE   OurBranchID = @OurBranchID AND AccountID = @AccountID                      
                      
                      
            SELECT @IsProductFreezed=IsFreezed,                      
                   @AllowCrDr = Case @TrxType    WHEN 'C' THEN AllowCredit                      
                                 WHEN 'D' THEN AllowDebit      END ,                      
                    @CanGoCrDr = Case @TrxType    WHEN 'C' THEN CanGoInCredit                      
                                 WHEN 'D' THEN CanGoInDebit    END ,                      
                       @MinAmount =IsNull( MinBalance,0)                      
            From t_Products                      
            WHERE   OurBranchID = @OurBranchID AND ProductID = @ProductID                      
                   
            IF @IsProductFreezed = 1            
               RETURN(19) -- Product Is Freezed                      
                                
            IF @AllowCrDr = 0            
      RETURN(21) -- In Product Condition Dr/Cr Trx Not Allowed                    
                       
   SELECT @AllowCrDr = Case @TrxType WHEN 'C' THEN AllowCreditTransaction WHEN 'D' THEN AllowDebitTransaction END,                    
            @EveryTrxGoesInSupervision = Case @TrxType WHEN 'C' THEN CreditNeedsSupervision WHEN 'D' THEN DebitNeedsSupervision END                    
            From t_Account                    
            WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                      
                                
            IF @AllowCrDr = 1 AND (@ByPassSupervision = 0 AND UPPER(@OperatorID) <> 'COMPUTER')            
               RETURN(20) -- In Account Special Condition Dr/Cr Transaction Not Allowed                    
                                   
            --END Checking A/c Status                      
                      
            --SET @VpOD = (@Amount * -1) - (@Limit - ( @ClearBalance + @Effects ))                      
            SET @VpOD = @Limit                      
                      
                      
         IF @IsFreezed = 1                      
                  SELECT @MinAmount = @MinAmount + @BlockedAmount                      
                      
                      
            IF @IsFreezed = 1 AND (@ClearBalance + @VpOD ) < (@MinAmount + @Amount)                      
               BEGIN                      
     IF @CanGoCrDr = 0                      
      BEGIN                      
                     IF (@ByPassSupervision = 0 AND UPPER(@OperatorID) <> 'COMPUTER')            
                        RETURN(23) -- RETURN(22) -- A/c Is Freezed Cannot Go In Debit  
      END                      
     ELSE                      
                     SET @Supervision = 'P'                      
                      
                      
               END            
            ELSE                      
                      
                BEGIN                      
                      
                    IF @Amount > @ClearBalance + @VpOD - @MinAmount                      
                        BEGIN                      
                      
                            IF @CanGoCrDr = 1                      
                               SET @Supervision = 'P'                      
                            ELSE                      
                               BEGIN                      
                                  IF (@ByPassSupervision = 0 AND UPPER(@OperatorID) <> 'COMPUTER')            
                                     RETURN(23) -- This Trx Cannot Go In Debit                      
                                  ELSE                      
                                     SET @Supervision = 'P'                      
                               END                      
                        END                      
                END                      
      END                      
-----------------------------------------------------------------------------------                      
--print 'Supervision :' + @Supervision                      
      IF @ByPassSupervision = 1                      
         SET @Supervision = 'C'                      
      Else                      
        BEGIN                      
            IF @Supervision <> 'P'                      
                BEGIN                      
                    IF @LocEq > @PostingLimit                      
                        SET @Supervision = '*'                      
                                        
                End                      
        End                      
-------------------------------------------------------------------------------------                              
                      
      IF @Supervision = 'C'                      
   IF @EveryTrxGoesInSupervision = 1 AND (@ByPassSupervision = 0 AND UPPER(@OperatorID) <> 'COMPUTER')            
    RETURN (98)  -- Every Trx Goes In Supervision                      
   ELSE                      
    RETURN (97)                      
                      
      ELSE IF @Supervision = '*'                      
         RETURN (98)                      
      ELSE IF @Supervision = 'P'                      
         RETURN (99)                      
                      
   IF @EveryTrxGoesInSupervision = 1                      
   RETURN (98)  -- Every Trx Goes In Supervision 