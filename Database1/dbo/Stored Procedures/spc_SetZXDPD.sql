﻿
CREATE   PROCEDURE [dbo].[spc_SetZXDPD]
AS
SET NOCOUNT ON;
SET ANSI_WARNINGS OFF;
UPDATE A SET
A.NoOfDPD = ISNULL(B.NoOfDPD, 0)
from t_Account a
inner join v_CalculateZXDPD b on a.OurBranchID = b.OurBranchID and a.AccountID = b.AccountID;
SELECT 'Ok' AS RetStatus;