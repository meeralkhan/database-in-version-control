﻿CREATE PROCEDURE [dbo].[spc_GetDebitBalanceValue]        
   @OurBranchID varchar(30)        
AS        
        
SET NOCOUNT ON        
        
BEGIN        
           
   select p.ProductID, p.GLControl, p.GLDebitBalance,        
   a.ForeignBalance as ForeignControlBalance, a.Balance AS LocalControlBalance, a.CurrencyID,        
   b.Balance AS LocalDebitBalance, b.ForeignBalance as ForeignDebitBalance        
   from t_Products p        
   INNER JOIN t_GL a ON p.OurBranchID = a.OurBranchID AND p.GLControl = a.AccountID        
   INNER JOIN t_GL b ON p.OurBranchID = b.OurBranchID AND p.GLDebitBalance = b.AccountID        
   where p.OurBranchID = @OurBranchID        
           
END