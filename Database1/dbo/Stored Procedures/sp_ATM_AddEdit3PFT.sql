﻿CREATE PROCEDURE [dbo].[sp_ATM_AddEdit3PFT]  
(  
 @OurBranchID       varchar(30),   
 @WithdrawlBranchID      varchar(30),   
 @ATMID             varchar(30),   
 @AccountID               varchar(30),  
 @CustomerBranchID      varchar(30),  
 @AccountID2           varchar(30),  
 @CustomerBranchID2      varchar(30),  
 @Amount            money,  
 @USDAmount           money=0,  
 @OtherCurrencyAmount  money=0,  
 @USDRate           money=0,  
 @Supervision       char(1),   
 @RefNo              varchar(50),  
 @PHXDate            datetime,  
 @MerchantType        varchar(30),  
 @OurIMD             varchar(30),  
 @AckInstIDCode        varchar(30),  
 @Currency           varchar(30),  
 @NameLocation        varchar(100),  
 @MCC            varchar(30),  
 @PHXDateTime    varchar(14),  
 @NewRecord           bit=1  
)  
AS  
 DECLARE @BankShortName as varchar(30)  
  
 DECLARE @mScrollNo as int  
 DECLARE @mSerialNo as smallint  
 DECLARE @mAccountID as varchar(30)  
 DECLARE @mAccountName as varchar(100)  
 DECLARE @mAccountType as char  
 DECLARE @mProductID as varchar(30)  
 DECLARE @mCurrencyID as varchar(30)  
 DECLARE @mIsLocalCurrency as bit  
  SET @mIsLocalCurrency=1   
 DECLARE @mwDate as datetime  
 DECLARE @mTrxType as char  
 DECLARE @mGLID as varchar(30)  
  SET @mGLID = ''   
 DECLARE @mForeignAmount as money  
  SET @mForeignAmount=0   
 DECLARE @mExchangeRate as money  
  SET @mExchangeRate=1   
 DECLARE @mDescriptionID as varchar(30)  
  SET @mDescriptionID='987'   
 DECLARE @mDescription as varchar(255)  
 DECLARE @mDescription2 as varchar(255)  
 DECLARE @mDescriptionCharges as varchar(255)  
 DECLARE @mDescriptionH as varchar(255)  
 DECLARE @mDescriptionHCharges as varchar(255)  
 DECLARE @mDescriptionChargesExcise as varchar(255)  
 DECLARE @mDescriptionHChargesExcise as varchar(255)  
 DECLARE @LocalCurrencyID as varchar(30)  
 DECLARE @LocalCurrency as varchar(30)  
  
  BEGIN   
   SELECT @LocalCurrencyID=LocalCurrencyID,@LocalCurrency=LocalCurrency FROM t_ATM_GlobalVariables  
   SET @BankShortName = 'N/A'  
   SELECT @BankShortName = BankShortName From t_ATM_Banks  
   WHERE (BankIMD = @OurIMD)  
   BEGIN  
    SET @mDescription=Right(@RefNo,6) + '-ATM(' + @ATMID + ') 3PFT TO A/c#' + @AccountID2 + '(' + @CustomerBranchID2 + ')'  
    SET @mDescription = @mDescription + '-' + @PHXDateTime  
    SET @mDescription2=Right(@RefNo,6) + '-ATM(' + @ATMID + ') 3PFT FROM A/c#' + @AccountID + '(' + @CustomerBranchID + ')'  
    SET @mDescription2 = @mDescription2 + '-' + @PHXDateTime  
    SET @mDescriptionCharges=Right(@RefNo,6) + '-ATM(' + @ATMID + ') 3PFT-CHRG TO A/c#' + @AccountID2 + '(' + @CustomerBranchID2 + ')'  
    SET @mDescriptionCharges = @mDescriptionCharges + '-' + @PHXDateTime  
    SET @mDescriptionHCharges=Right(@RefNo,6) + '-ATM(' + @ATMID + ') 3PFT-CHRG TO A/c#' + @AccountID2 + '(' + @CustomerBranchID2 + ') FROM A/c#' + @AccountID + '(' + @CustomerBranchID + ')'  
    SET @mDescriptionHCharges = @mDescriptionHCharges + '-' + @PHXDateTime  
    SET @mDescriptionChargesExcise=Right(@RefNo,6) + '-ATM(' + @ATMID + ') 3PFT-FED TO A/c#' + @AccountID2 + '(' + @CustomerBranchID2 + ')'  
    SET @mDescriptionChargesExcise = @mDescriptionChargesExcise + '-' + @PHXDateTime  
    SET @mDescriptionHChargesExcise=Right(@RefNo,6) + '-ATM(' + @ATMID + ') 3PFT-FED TO A/c#' + @AccountID2 + '(' + @CustomerBranchID2 + ') FROM A/c#' + @AccountID + '(' + @CustomerBranchID + ')'  
    SET @mDescriptionHChargesExcise = @mDescriptionHChargesExcise + '-' + @PHXDateTime  
   END  
  END   
  
 DECLARE @mRemarks as varchar(200)  
 DECLARE @mBankCode as varchar(30)  
  SET @mBankCode='093'   
 DECLARE @mOperatorID as varchar(30)  
  SET @mOperatorID = 'ATM-' + @ATMID  
 DECLARE @mSupervisorID as varchar(30)  
  SET @mSupervisorID = @OurIMD   
  
--Checking Variables  
 DECLARE @vCount1 as bit  
 DECLARE @vCount2 as bit  
 DECLARE @vCount3 as bit  
 DECLARE @vCount4 as bit  
 DECLARE @vAccountID as varchar(30)  
 DECLARE @vClearBalance as money  
 DECLARE @vClearedEffects as money  
 DECLARE @vFreezeAmount as money  
 DECLARE @vMinBalance as money  
 DECLARE @vIsClose as char  
 DECLARE @vAreCheckBooksAllowed as bit  
 DECLARE @vAllowDebitTransaction as bit  
 DECLARE @vAllowCreditTransaction as bit  
 DECLARE @vTaxAmount as money  
  SET @vTaxAmount = 0  
  
 DECLARE @vAccountLimit as money  
 DECLARE @vCashAmt as money  
 DECLARE @vExpiryDate as datetime  
 DECLARE @vCheckLimit as bit  
  SET @vCheckLimit = 0  
 DECLARE @vODLimitAllow as bit  
  SET @vODLimitAllow = 0  
 DECLARE @vTaxAccountID as varchar(30)  
  SET @vTaxAccountID = ''  
 DECLARE @vTaxDeduction as money  
  SET @vTaxDeduction = 0  
 DECLARE @vTaxLimit as money  
  SET @vTaxLimit = 0  
  
 DECLARE @vBranchName as varchar(100)  
 DECLARE @Status as int   
  
 DECLARE @mLastEOD as datetime  
-- DECLARE @mWorkingDate as datetime  
 DECLARE @mCONFRETI as datetime  
 DECLARE @mWithdrawlBranchName as varchar(100)  
  
 DECLARE @mRemarksCharges as varchar(200)  
 DECLARE @mRemarksTax as varchar(200)  
  
 DECLARE @3PFTCharges as money  
 DECLARE @3PFTAccountID as varchar(30)  
  
 DECLARE @WRDLCHGExciseDuty as money --samad  
 DECLARE @ExciseDutyPercentage as money --samad  
 DECLARE @ExciseDutyAccountID as varchar(30) --samad  
  
 SET NOCOUNT ON  
  
 SELECT @Status = -1  
---------------------  
  
 BEGIN  
------------------------  
------------------------  
  
  BEGIN  
   SELECT @3PFTCharges=[3PFTCharges], @3PFTAccountID=[3PFTAccountID],  
                               @ExciseDutyPercentage=ExciseDutyPercentage,@ExciseDutyAccountID=ExciseDutyAccountID   --samad  
   From t_ATM_SwitchCharges  
   WHERE (MerchantType = @MerchantType)  
  
   SET @WRDLCHGExciseDuty = (@3PFTCharges*@ExciseDutyPercentage)/100  
  END  
  
  IF @CustomerBranchID = @CustomerBranchID2 AND @CustomerBranchID = (SELECT OurBranchID FROM t_GlobalVariables)  
   BEGIN  
    SET @3PFTCharges = 0  
    SET @ExciseDutyPercentage = 0  
    SET @WRDLCHGExciseDuty = 0  
   END  
  
--Debit Transaction     
  
  
  IF @CustomerBranchID = (SELECT OurBranchID FROM t_GlobalVariables)  
   BEGIN  
    SELECT @mLastEOD=LastEOD,@mwDate=WorkingDate,@mCONFRETI=CONFRETI FROM t_Last WHERE (OurBranchID = @OurBranchID)  
    IF @mLastEOD = @mwDate or @mCONFRETI = '1/1/9999'  
     BEGIN  
      SELECT @Status=9  --INVALID_DATE  
      SELECT @Status  as Status  
      RETURN (1)   
     END  
    
    SELECT @mScrollNo=(ISNULL(MAX(ScrollNo),0)+1) FROM t_ATM_TransferTransaction WHERE (OurBranchID = @OurBranchID) and (wDate = @mwDate)   
  
    SELECT @vCount1=count(*) From t_Account  
    WHERE (OurBranchID = @OurBranchID) AND( AccountID=@AccountID)  
    IF @vCount1=0  
     BEGIN  
      SELECT @Status=2  --INVALID_ACCOUNT  
      SELECT @Status  as Status  
      RETURN (1)   
     END  
   
    SELECT Top 1 @vCheckLimit=CheckLimit,@vTaxAccountID=TaxAccountID,@vTaxDeduction=TaxDeduction,@vTaxLimit=TaxLimit FROM t_ATM_GlobalVariables  
    SELECT Top 1 @vODLimitAllow=ODLimitAllow FROM t_ATM_Banks WHERE BankIMD = left(@RefNo,6)  
  
    SELECT @mAccountName=B.Name, @mProductID=B.ProductID, @mCurrencyID=P.CurrencyID,@mAccountType='C',@vClearBalance=B.ClearBalance,@vClearedEffects=B.ClearedEffects,@vFreezeAmount=B.FreezeAmount,@vMinBalance=A.MinBalance,@vIsClose=A.Status, @vAreCheckBooksAllowed=P.AreCheckBooksAllowed, @vAllowDebitTransaction=S.AllowDebitTransaction, @vAccountLimit=B.Limit, @vCashAmt=B.CashAmt  
    FROM t_AccountBalance B INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND  B.ProductID = A.ProductID INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND  A.ProductID = P.ProductID LEFT OUTER JOIN t_AccountSpecialCondition S ON B.OurBranchID = S.OurBranchID AND B.AccountID = S.AccountID  
    WHERE  (A.OurBranchID = @OurBranchID) AND (A.AccountID = @AccountID)  
   
    IF @vAccountLimit > 0 AND @vCheckLimit = 1 AND @vODLimitAllow = 1  
     BEGIN  
      SELECT @vExpiryDate=ExpiryDate FROm t_AccountLimits WHERE (OurBranchID = @OurBranchID) and (AccountID = @AccountID)  
      IF @vExpiryDate > @mwDate  
       BEGIN  
        SET @vClearBalance=(@vClearBalance+@vAccountLimit)  
       END  
     END  
    IF @vIsClose = 'C' or @vIsClose = 'T' or @vIsClose = 'X' or @vAllowDebitTransaction = 1  
     BEGIN  
      SELECT @Status=3  --INACTIVE/CLOSE_ACCOUNT  
      SELECT @Status  as Status  
      RETURN (1)   
     END  
    IF @Amount >(@vClearBalance-@vClearedEffects)-@3PFTCharges-@WRDLCHGExciseDuty-@vTaxAmount or @Amount >((@vClearBalance-@vClearedEffects)-@vFreezeAmount-@vMinBalance-@3PFTCharges-@WRDLCHGExciseDuty-@vTaxAmount) or @Amount >((@vClearBalance-@vClearedEffects)-@vMinBalance-@3PFTCharges-@WRDLCHGExciseDuty-@vTaxAmount)  
     BEGIN  
      SELECT @Status=4  --LOW_BALANCE  
      SELECT @Status  as Status  
      RETURN (1)   
     END  
    IF @vAreCheckBooksAllowed = 0  
     BEGIN  
      SELECT @Status=3  --INVALID_PRODUCT  
      SELECT @Status  as Status  
      RETURN (1)   
     END  
    IF @mCurrencyID <> @LocalCurrency  
     BEGIN  
      SELECT @Status=3  --INVALID_CURRENCY  
      SELECT @Status  as Status  
      RETURN (1)   
     END  
   
    Set @mSerialNo = 1  
    Set @mTrxType = 'D'  
     
    SET @vBranchName = 'N/A'  
    SELECT @vBranchName=Heading2 FROM t_GlobalVariables WHERE (OurBranchID = @OurBranchID)  
   
    SELECT @vCount4=count(*) From t_ATM_Branches  
    WHERE BranchID = @WithdrawlBranchID  
   
    IF @MerchantType <> '0000'   
     BEGIN  
      IF @Currency = @LocalCurrencyID  
       BEGIN  
        Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Transaction From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
        Set @mRemarksTax='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Tax Transaction From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
        Set @mRemarksCharges='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Charges Transaction From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
       END  
      ELSE   
       IF @Currency = '840'  
        BEGIN  
         Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
         Set @mRemarksTax='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Tax Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
         Set @mRemarksCharges='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Charges Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
   
        END  
       ELSE   
        BEGIN  
         Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
         Set @mRemarksTax='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Tax Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
         Set @mRemarksCharges='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Charges Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
        END  
     END  
    ELSE  
     IF @vCount4 > 0   
      BEGIN  
       SET  @mWithdrawlBranchName = 'N/A'  
       SELECT @mWithdrawlBranchName=BranchName FROM t_ATM_Branches  
       WHERE BranchID = @WithdrawlBranchID  
       Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Branch Cash Withdrawl Transaction From : BK.' + @BankShortName +  ' (' + upper(@mWithdrawlBranchName) + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
       Set @mRemarksTax='Being Amount of ATM (' + @ATMID+ ') Inter Branch Cash Withdrawl Tax Transaction From : BK.' + @BankShortName +  ' (' + upper(@mWithdrawlBranchName) + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
      END  
     --IF @vCount4 = 0   
     ELSE  
      BEGIN  
       Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Branch Cash Withdrawl Transaction From : BK.' + @BankShortName +  ' (BR.' +  @WithdrawlBranchID + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
       Set @mRemarksTax='Being Amount of ATM (' + @ATMID+ ') Inter Branch Cash Withdrawl Tax Transaction From : BK.' + @BankShortName +  ' (BR.' +  @WithdrawlBranchID + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
      END  
   
    INSERT INTO t_ATM_TransferTransaction  
     (  
      ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,  
      valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,  
      BankCode,Remarks,OperatorID,SupervisorID,MCC,MerchantType  
     )    
    
    VALUES  
    
     (  
    
      @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@AccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,  
      @mwDate,@mwDate,@PHXDate,@mTrxType,@Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescription,@ATMID,  
      @mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MCC,@MerchantType  
     )  
    
    
    IF @CustomerBranchID <> @CustomerBranchID2 AND @3PFTCharges > 0  
    BEGIN  
      Set @mRemarksCharges='-'  
      Set @mSerialNo = 3  
      Set @mTrxType = 'D'  
      
      INSERT INTO t_ATM_TransferTransaction  
       (  
        ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,  
        valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,  
        BankCode,Remarks,OperatorID,SupervisorID,MCC,MerchantType  
       )    
      
      VALUES  
      
       (  
        @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@AccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,  
        @mwDate,@mwDate,@PHXDate,@mTrxType,@Supervision,@mGLID,@3PFTCharges,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescriptionCharges,@ATMID,  
        @mBankCode,@mRemarksCharges,@mOperatorID,@mSupervisorID,@MCC,@MerchantType  
       )  
    END  
  
    IF @CustomerBranchID <> @CustomerBranchID2 AND @3PFTCharges > 0 AND @WRDLCHGExciseDuty > 0 AND @ExciseDutyAccountID <> ''  
    BEGIN  
      Set @mRemarksCharges='-'  
      Set @mSerialNo = 5  
      Set @mTrxType = 'D'  
      
      INSERT INTO t_ATM_TransferTransaction  
       (  
        ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,  
        valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,  
        BankCode,Remarks,OperatorID,SupervisorID,MCC,MerchantType  
       )    
      
      VALUES  
      
       (  
        @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@AccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,  
        @mwDate,@mwDate,@PHXDate,@mTrxType,@Supervision,@mGLID,@WRDLCHGExciseDuty,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescriptionChargesExcise,@ATMID,  
        @mBankCode,@mRemarksCharges,@mOperatorID,@mSupervisorID,@MCC,@MerchantType  
       )  
    END  
  
      SELECT @Status=@@ERROR  
    
      IF @Status <> 0   
    
       BEGIN  
        RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR  
        RETURN (5000)  
       END  
  
   END  
  ELSE  
   BEGIN  
    SELECT @mLastEOD=LastEOD,@mwDate=WorkingDate,@mCONFRETI=CONFRETI FROM t_Last WHERE (OurBranchID = @CustomerBranchID2)  
    IF @mLastEOD = @mwDate or @mCONFRETI = '1/1/9999'  
     BEGIN  
      SELECT @Status=9  --INVALID_DATE  
      SELECT @Status  as Status  
      RETURN (1)   
     END  
    
    SELECT @mScrollNo=(ISNULL(MAX(ScrollNo),0)+1) FROM t_ATM_TransferTransaction WHERE (OurBranchID = @CustomerBranchID2) and (wDate = @mwDate)   
  
    SELECT @vCount1=count(*) From t_Account  
    WHERE (OurBranchID = @CustomerBranchID2) AND( AccountID=@AccountID2)  
    IF @vCount1=0  
     BEGIN  
      SELECT @Status=2  --INVALID_ACCOUNT  
      SELECT @Status  as Status  
      RETURN (1)   
     END  
   
--    SELECT @mAccountName=B.Name, @mProductID=B.ProductID, @mCurrencyID=P.CurrencyID,@mAccountType='C',@vClearBalance=B.ClearBalance,@vClearedEffects=B.ClearedEffects,@vFreezeAmount=B.FreezeAmount,@vMinBalance=A.MinBalance,@vIsClose=A.Status, @vAreCheckBooksAllowed=P.AreCheckBooksAllowed, @vAllowCreditTransaction=S.AllowCreditTransaction, @vAccountLimit=B.Limit, @vCashAmt=B.CashAmt  
    SELECT @mAccountName=B.Name, @mProductID=B.ProductID, @mCurrencyID=P.CurrencyID,@mAccountType='C',@vIsClose=A.Status, @vAreCheckBooksAllowed=P.AreCheckBooksAllowed, @vAllowCreditTransaction=S.AllowCreditTransaction  
    FROM t_AccountBalance B INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND  B.ProductID = A.ProductID INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND  A.ProductID = P.ProductID LEFT OUTER JOIN t_AccountSpecialCondition S ON B.OurBranchID = S.OurBranchID AND B.AccountID = S.AccountID  
    WHERE  (A.OurBranchID = @CustomerBranchID2) AND (A.AccountID = @AccountID2)  
   
    SET @3PFTCharges = 0  
    SET @ExciseDutyPercentage = 0  
    SET @WRDLCHGExciseDuty = 0  
  
  
    IF @vIsClose = 'C' or @vIsClose = 'T' or @vIsClose = 'X' or @vAllowCreditTransaction = 1  
     BEGIN  
      SELECT @Status=3  --INACTIVE/CLOSE_ACCOUNT  
      SELECT @Status  as Status  
      RETURN (1)   
     END  
    IF @vAreCheckBooksAllowed = 0  
     BEGIN  
      SELECT @Status=3  --INVALID_PRODUCT  
      SELECT @Status  as Status  
      RETURN (1)   
     END  
    IF @mCurrencyID <> @LocalCurrency  
     BEGIN  
      SELECT @Status=3  --INVALID_CURRENCY  
      SELECT @Status  as Status  
      RETURN (1)   
     END  
   
    Set @mSerialNo = 1  
    Set @mTrxType = 'C'  
     
    SET @vBranchName = 'N/A'  
    SELECT @vBranchName=Heading2 FROM t_GlobalVariables WHERE (OurBranchID = @CustomerBranchID2)  
   
    SELECT @vCount4=count(*) From t_ATM_Branches  
    WHERE BranchID = @WithdrawlBranchID  
   
    IF @MerchantType <> '0000'   
     BEGIN  
      IF @Currency = @LocalCurrencyID  
       BEGIN  
        Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Transaction From : ' + @NameLocation + ' By  A/c # ' + @AccountID2 + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
        Set @mRemarksTax='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Tax Transaction From : ' + @NameLocation + ' By  A/c # ' + @AccountID2 + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
        Set @mRemarksCharges='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Charges Transaction From : ' + @NameLocation + ' By  A/c # ' + @AccountID2 + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
       END  
      ELSE   
       IF @Currency = '840'  
        BEGIN  
         Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID2 + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
         Set @mRemarksTax='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Tax Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID2 + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
         Set @mRemarksCharges='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Charges Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID2 + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
   
        END  
       ELSE   
        BEGIN  
         Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) From : ' + @NameLocation + ' By  A/c # ' + @AccountID2 + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
         Set @mRemarksTax='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Tax Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) From : ' + @NameLocation + ' By  A/c # ' + @AccountID2 + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
         Set @mRemarksCharges='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Charges Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) From : ' + @NameLocation + ' By  A/c # ' + @AccountID2 + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
        END  
     END  
    ELSE  
     IF @vCount4 > 0   
      BEGIN  
       SET  @mWithdrawlBranchName = 'N/A'  
       SELECT @mWithdrawlBranchName=BranchName FROM t_ATM_Branches  
       WHERE BranchID = @WithdrawlBranchID  
       Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Branch Cash Withdrawl Transaction From : BK.' + @BankShortName +  ' (' + upper(@mWithdrawlBranchName) + ') By  A/c # ' + @AccountID2 + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
       Set @mRemarksTax='Being Amount of ATM (' + @ATMID+ ') Inter Branch Cash Withdrawl Tax Transaction From : BK.' + @BankShortName +  ' (' + upper(@mWithdrawlBranchName) + ') By  A/c # ' + @AccountID2 + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
      END  
     --IF @vCount4 = 0   
     ELSE  
      BEGIN  
       Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Branch Cash Withdrawl Transaction From : BK.' + @BankShortName +  ' (BR.' +  @WithdrawlBranchID + ') By  A/c # ' + @AccountID2 + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
       Set @mRemarksTax='Being Amount of ATM (' + @ATMID+ ') Inter Branch Cash Withdrawl Tax Transaction From : BK.' + @BankShortName +  ' (BR.' +  @WithdrawlBranchID + ') By  A/c # ' + @AccountID2 + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
      END  
   
    INSERT INTO t_ATM_TransferTransaction  
     (  
      ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,  
      valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,  
      BankCode,Remarks,OperatorID,SupervisorID,MCC,MerchantType  
     )    
    
    VALUES  
    
     (  
    
      @mScrollNo,@mSerialNo,@Refno,@CustomerBranchID2,@WithdrawlBranchID,@AccountID2,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,  
      @mwDate,@mwDate,@PHXDate,@mTrxType,@Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescription2,@ATMID,  
      @mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MCC,@MerchantType  
     )  
    
    
      SELECT @Status=@@ERROR  
    
      IF @Status <> 0   
    
       BEGIN  
        RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR  
        RETURN (5000)  
       END  
   END  
--Credit Transaction    
 IF @CustomerBranchID = @CustomerBranchID2   
  BEGIN  
   SELECT @mAccountName=B.Name, @mProductID=B.ProductID, @mCurrencyID=P.CurrencyID,@mAccountType='C',@vIsClose=A.Status, @vAreCheckBooksAllowed=P.AreCheckBooksAllowed, @vAllowCreditTransaction=S.AllowCreditTransaction  
   FROM t_AccountBalance B INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND  B.ProductID = A.ProductID INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND  A.ProductID = P.ProductID LEFT OUTER JOIN t_AccountSpecialCondition S ON B.OurBranchID = S.OurBranchID AND B.AccountID = S.AccountID  
   WHERE  (A.OurBranchID = @OurBranchID) AND (A.AccountID = @AccountID2)  
   IF @vIsClose = 'C' or @vIsClose = 'T' or @vIsClose = 'X' or @vAllowCreditTransaction = 1  
    BEGIN  
     SELECT @Status=3  --INACTIVE/CLOSE_ACCOUNT  
     SELECT @Status  as Status  
     RETURN (1)   
    END  
   IF @vAreCheckBooksAllowed = 0  
    BEGIN  
     SELECT @Status=3  --INVALID_PRODUCT  
     SELECT @Status  as Status  
     RETURN (1)   
    END  
   IF @mCurrencyID <> @LocalCurrency  
    BEGIN  
     SELECT @Status=3  --INVALID_CURRENCY  
     SELECT @Status  as Status  
     RETURN (1)   
    END  
  
   Set @mSerialNo = 2  
   Set @mTrxType = 'C'  
  
   SET @vBranchName = 'N/A'  
   SELECT @vBranchName=Heading2 FROM t_GlobalVariables WHERE (OurBranchID = @OurBranchID)  
  
   SELECT @vCount4=count(*) From t_ATM_Branches  
   WHERE BranchID = @WithdrawlBranchID  
  
   IF @MerchantType <> '0000'   
    BEGIN  
     IF @Currency = @LocalCurrencyID  
      BEGIN  
       Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Transaction From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
       Set @mRemarksTax='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Tax Transaction From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
       Set @mRemarksCharges='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Charges Transaction From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
      END  
     ELSE   
      IF @Currency = '840'  
       BEGIN  
        Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
        Set @mRemarksTax='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Tax Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
        Set @mRemarksCharges='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Charges Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
  
       END  
      ELSE   
       BEGIN  
        Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
        Set @mRemarksTax='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Tax Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
        Set @mRemarksCharges='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Charges Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
       END  
    END  
   ELSE  
    IF @vCount4 > 0   
     BEGIN  
      SET  @mWithdrawlBranchName = 'N/A'  
      SELECT @mWithdrawlBranchName=BranchName FROM t_ATM_Branches  
      WHERE BranchID = @WithdrawlBranchID  
      Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Branch Cash Withdrawl Transaction From : BK.' + @BankShortName +  ' (' + upper(@mWithdrawlBranchName) + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
      Set @mRemarksTax='Being Amount of ATM (' + @ATMID+ ') Inter Branch Cash Withdrawl Tax Transaction From : BK.' + @BankShortName +  ' (' + upper(@mWithdrawlBranchName) + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
     END  
    --IF @vCount4 = 0   
    ELSE  
     BEGIN  
      Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Branch Cash Withdrawl Transaction From : BK.' + @BankShortName +  ' (BR.' +  @WithdrawlBranchID + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
      Set @mRemarksTax='Being Amount of ATM (' + @ATMID+ ') Inter Branch Cash Withdrawl Tax Transaction From : BK.' + @BankShortName +  ' (BR.' +  @WithdrawlBranchID + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
     END  
  
  INSERT INTO t_ATM_TransferTransaction  
   (  
    ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,  
    valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,  
    BankCode,Remarks,OperatorID,SupervisorID,MCC,MerchantType  
   )    
  
  VALUES  
  
   (  
  
    @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@AccountID2,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,  
    @mwDate,@mwDate,@PHXDate,@mTrxType,@Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescription2,@ATMID,  
    @mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MCC,@MerchantType  
   )  
  
  
    SELECT @Status=@@ERROR  
  
    IF @Status <> 0   
  
     BEGIN  
      RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR  
      RETURN (5000)  
     END  
  END  
 ELSE   
  BEGIN  
   SELECT @vAccountID=AccountID, @mAccountName=Description,@mProductID='GL', @mCurrencyID=CurrencyID,@mAccountType='G' FROM t_GL   
   WHERE (AccountID =  @3PFTAccountID)  
      
   Set @mSerialNo = 2  
  
   IF @CustomerBranchID = (SELECT OurBranchID FROM t_GlobalVariables)  
    BEGIN  
     SET @mDescriptionH=Right(@RefNo,6) + '-ATM(' + @ATMID + ') 3PFT TO A/c#' + @AccountID2 + '(' + @CustomerBranchID2 + ') FROM A/c#' + @AccountID + '(' + @CustomerBranchID + ')'  
     SET @mDescriptionH = @mDescriptionH + '-' + @PHXDateTime  
     Set @mTrxType = 'C'  
    END   
   ELSE  
    BEGIN  
     SET @mDescriptionH=Right(@RefNo,6) + '-ATM(' + @ATMID + ') 3PFT FROM A/c#' + @AccountID + '(' + @CustomerBranchID + ') TO A/c#' + @AccountID2 + '(' + @CustomerBranchID2 + ')'  
     SET @mDescriptionH = @mDescriptionH + '-' + @PHXDateTime  
     Set @mTrxType = 'D'  
    END   
   
   INSERT INTO t_ATM_TransferTransaction  
    (  
     ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,  
     valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,  
     BankCode,Remarks,OperatorID,SupervisorID,MCC,MerchantType  
    )    
   
   VALUES  
    (  
     @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@vAccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,  
     @mwDate,@mwDate,@PHXDate,@mTrxType,@Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescriptionH,@ATMID,  
     @mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MCC,@MerchantType  
    )  
   
   IF @CustomerBranchID = (SELECT OurBranchID FROM t_GlobalVariables) AND @3PFTCharges > 0  
   BEGIN  
    Set @mRemarksCharges='-'  
    Set @mSerialNo = 4  
    Set @mTrxType = 'C'  
    
    INSERT INTO t_ATM_TransferTransaction  
     (  
      ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,  
      valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,  
      BankCode,Remarks,OperatorID,SupervisorID,MCC,MerchantType  
     )    
    
    VALUES  
     (  
      @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@vAccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,  
      @mwDate,@mwDate,@PHXDate,@mTrxType,@Supervision,@mGLID,@3PFTCharges,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescriptionHCharges,@ATMID,  
      @mBankCode,@mRemarksCharges,@mOperatorID,@mSupervisorID,@MCC,@MerchantType  
     )  
  
   END  
  
   IF @CustomerBranchID = (SELECT OurBranchID FROM t_GlobalVariables) AND @3PFTCharges > 0 AND @WRDLCHGExciseDuty > 0 AND @ExciseDutyAccountID <> ''  
   BEGIN  
    SELECT @vAccountID=AccountID, @mAccountName=Description,@mProductID='GL', @mCurrencyID=CurrencyID,@mAccountType='G' FROM t_GL   
    WHERE (AccountID =  @ExciseDutyAccountID)  
  
    Set @mRemarksCharges='-'  
    Set @mSerialNo = 6  
    Set @mTrxType = 'C'  
    
    INSERT INTO t_ATM_TransferTransaction  
     (  
      ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,  
      valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,  
      BankCode,Remarks,OperatorID,SupervisorID,MCC,MerchantType  
     )    
    
    VALUES  
     (  
      @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@vAccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,  
      @mwDate,@mwDate,@PHXDate,@mTrxType,@Supervision,@mGLID,@WRDLCHGExciseDuty,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescriptionHChargesExcise,@ATMID,  
      @mBankCode,@mRemarksCharges,@mOperatorID,@mSupervisorID,@MCC,@MerchantType  
     )  
  
   END  
  
     SELECT @Status=@@ERROR  
   
     IF @Status <> 0   
      BEGIN  
       RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR  
       RETURN (5000)  
      END  
  
  END  
 IF @vAccountLimit > 0 AND @vCheckLimit = 1 AND @vODLimitAllow = 1  AND @vExpiryDate > @mwDate  
  BEGIN  
   SELECT @Status = 0 , @vClearBalance = ((IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0)-@vAccountLimit)-IsNull(@Amount,0)-isNull(@3PFTCharges,0)-isNull(@vTaxAmount,0)-isnull(@WRDLCHGExciseDuty,0))*100  
   SELECT @Status As Status , Round(@vClearBalance,0) As ClearBalance  
  END   
 ELSE  
  BEGIN  
   SELECT @Status = 0 , @vClearBalance = ((IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0))-IsNull(@Amount,0)-isNull(@3PFTCharges,0)-isNull(@vTaxAmount,0)-isnull(@WRDLCHGExciseDuty,0))*100  
   SELECT @Status As Status , Round(@vClearBalance,0) As ClearBalance  
  END   
  
  RETURN 0  
  
 END