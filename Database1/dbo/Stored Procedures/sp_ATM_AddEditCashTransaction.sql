﻿create   PROCEDURE [dbo].[sp_ATM_AddEditCashTransaction]      
(          
 @OurBranchID         varchar(30),           
 @CustomerBranchID    varchar(30)='',           
 @WithdrawlBranchID   varchar(30)='',             
 @ATMID               varchar(30),           
 @AccountID           varchar(30),          
 @Amount              numeric(18,6)=0,          
 @USDAmount           numeric(18,6)=0,          
 @OtherCurrencyAmount numeric(18,6)=0,          
 @USDRate             decimal(16,8)=0,          
 @Supervision         char(1),           
 @RefNo               varchar(100),          
 @PHXDate             datetime,           
 @MerchantType        varchar(30)='',          
 @AckInstIDCode       varchar(30)='',          
 @Currency            varchar(3),          
 @NameLocation        varchar(50),          
 @ProcCode             varchar(50)='',      
 @SettlmntAmount      numeric(18,6)=0,    
 @ConvRate            decimal(15,9)=0,    
 @AcqCountryCode      varchar(3)='',    
 @CurrCodeTran        varchar(3)='',    
 @CurrCodeSett        varchar(3)='',    
 @ForwardInstID    varchar(10)='',    
 @MessageType    varchar(10)='',    
 @OrgTrxRefNo    varchar(100)='',    
    
 @POSEntryMode    varchar(3)='',    
 @POSConditionCode   varchar(2)='',    
 @POSPINCaptCode   varchar(2)='',    
 @RetRefNum     varchar(15)='',    
 @CardAccptID    varchar(15)='',    
 @VISATrxID     varchar(15)='',    
 @CAVV      char(1)='',    
 @ResponseCode    varchar(2)='',    
 @ExtendedData    varchar(2)='',    
    
 /* TO BE UPDATE LATER */    
 @ChannelId     nvarchar(30)='4',    
 @ChannelRefID    nvarchar(100)='',    
 @AuthIDResp varchar(6)='',
 @NewRecord           bit=1          
)          
AS      
BEGIN           
 SET NOCOUNT ON    
       
 DECLARE @BankShortName as varchar(30)          
 DECLARE @mScrollNo as int    
 DECLARE @mSerialNo as int    
  SET @mSerialNo=0      
 DECLARE @GLSerialID as int    
  SET @GLSerialID=0      
 DECLARE @mCustomerAccountID as varchar(30)          
  SET @mCustomerAccountID=@AccountID           
 DECLARE @mAccountName as varchar(50)          
 DECLARE @mAccountType as char          
 DECLARE @mProductID as varchar(30)          
 DECLARE @mCurrencyID as varchar(30)        
     
 DECLARE @gAccountID as varchar(30)      
 DECLARE @gCurrencyID as varchar(30)      
       
 DECLARE @mIsLocalCurrency as char          
  SET @mIsLocalCurrency=1           
 DECLARE @mwDate as datetime          
 DECLARE @mTrxType as char          
  SET @mTrxType='D'           
 DECLARE @mGLID as varchar(30)          
 DECLARE @mForeignAmount as numeric(18,6)       
  SET @mForeignAmount=0    
 DECLARE @mExchangeRate as numeric(18,6)    
  SET @mExchangeRate=1    
 DECLARE @mDescriptionID as varchar(30)          
 DECLARE @mDescriptionIDChg1 as varchar(30)          
 DECLARE @mDescriptionIDChg2 as varchar(30)          
  SET @mDescriptionID='000'           
 DECLARE @mDescription as varchar(500)          
 DECLARE @mDescriptionCharges as varchar(500)          
 DECLARE @mDescriptionCharges2 as varchar(500)          
 DECLARE @STAN as varchar(30)      
  SET @STAN = RIGHT(@RefNo,6)      
  SET @BankShortName = 'N/A'          
 DECLARE @mRemarks as varchar(500)          
 DECLARE @mRemarksChgs as varchar(500)
 DECLARE @mRemarksChgs2 as varchar(500)          
 DECLARE @mOperatorID as varchar(30)          
  SET @mOperatorID = 'ATM-' + @ATMID          
 DECLARE @mSupervisorID as varchar(30)          
  SET @mSupervisorID = 'N/A'    
 DECLARE @mLastEOD as datetime          
 DECLARE @mCONFRETI as datetime          
 DECLARE @mCustomerBranchName as varchar(50)          
 --Checking Variables          
 DECLARE @vCount1 as bit          
  SET @vCount1 = 0    
 DECLARE @vAccountID as varchar(30)          
  SET @vAccountID = @AccountID          
 DECLARE @vClearBalance as money          
 DECLARE @AvailableBalance as money          
 DECLARE @Value as varchar(2)      
  SET @value='00'      
 DECLARE @vClearedEffects as money          
 DECLARE @vFreezeAmount as money          
 DECLARE @vMinBalance as money          
 DECLARE @limit as money          
 DECLARE @vIsClose as char          
 DECLARE @vAreCheckBooksAllowed as bit          
 DECLARE @vAllowDebitTransaction as bit          
 DECLARE @vBranchName as varchar(50)          
 DECLARE @Status as int      
    
 DECLARE @WithDrawlCharges as money          
 DECLARE @AcquiringChargesAmount as money    
  SET @WithDrawlCharges = 0          
  SET @AcquiringChargesAmount = 0    
 DECLARE @IssuerChargesAccountID as varchar(30)          
 DECLARE @ExciseDutyAccountID as varchar(30)      
 DECLARE @ExciseDutyPercentage as money      
  SET @ExciseDutyPercentage = 0      
 DECLARE @ExciseDutyAmount as money      
  SET @ExciseDutyAmount = 0      
    
 Declare @SOCID as varchar(30)    
  SET @SOCID = ''
 Declare @ATMChargeID as varchar(6)    
 Declare @AcquiringChargesAccountID as varchar(30)    
 Declare @ProductID as varchar(30)    
 Declare @NoOfCWTrxATMMonthly as decimal(24,0)    
  SET @NoOfCWTrxATMMonthly = 0    
    
 DECLARE @Super as varchar(1)          
 DECLARE @mBankCode as varchar(30)        
 DECLARE @VoucherID int      
 DECLARE @IsCredit int      
 DECLARE @GLControl nvarchar(30)      
 DECLARE @CurrencyCode as varchar(30)    
 DECLARE @LocalCurrencyID as varchar(30)    
 DECLARE @LocalCurrency as varchar(30)    
 DECLARE @HoldStatus as char(1)    
 DECLARE @HoldAmount as money    
 DECLARE @FreezeAmount as money    
 DECLARE @vCheckLimit as bit    
  SET @vCheckLimit = 0    
 DECLARE @vODLimitAllow as bit    
  SET @vODLimitAllow = 0    
 DECLARE @remAmount as numeric(18,6)    
 DECLARE @mAccountID as varchar(30)    
 DECLARE @mRefNo as varchar(50)    
 DECLARE @mHoldStatus as char(1)    
 DECLARE @sCount as int    
  SET @sCount = 0    
 DECLARE @cCount as int    
  SET @cCount = 0    
 DECLARE @mCount as int    
  SET @mCount = 0
 DECLARE @IsClearingTrx as bit    
  SET @IsClearingTrx = 0    
 DECLARE @IsGCC as bit    
  SET @IsGCC = 0    
 DECLARE @ValueDate as datetime
 DECLARE @MobileNo nvarchar(30)
 DECLARE @Email nvarchar(100)
 DECLARE @rCurrencyID nvarchar(30)
 DECLARE @ClientID nvarchar(30)
 DECLARE @NarrationID nvarchar(30)
  SET @MobileNo = ''
  SET @Email = ''
  SET @rCurrencyID = ''
  SET @ClientID = ''
  SET @NarrationID = ''
 DECLARE @AppWHTax as smallint
 DECLARE @WHTaxAmount as money
  SET @AppWHTax = 0
  SET @WHTaxAmount = 0
 DECLARE @CountryCode as nvarchar(3)
  SET @CountryCode = ''
 DECLARE @TrxDetails as varchar(50)
  SET @TrxDetails = 'Cash Withdrawal Trx'
 DECLARE @VatReferenceID as varchar(30)
  
 Select @mBankCode=OurBankID From t_GLobalVariables WHERE OurBranchID = @OurBranchID    
 SELECT @vODLimitAllow=ODLimitAllow,@BankShortName = BankShortName From t_ATM_Banks WHERE (OurBranchID = @OurBranchID AND BankIMD = @AckInstIDCode)       
 SELECT @vCheckLimit=CheckLimit,@LocalCurrencyID=LocalCurrencyID,@LocalCurrency=LocalCurrency FROM t_ATM_GlobalVariables WHERE OurBranchID = @OurBranchID    
 SELECT @rCurrencyID=ISNULL(CurrencyID,'') FROM t_Currencies WHERE OurBranchID = @OurBranchID AND CurrencyCode = @CurrCodeTran
 
 SELECT @Status = -1  
 
 if (@AuthIDResp = '')
 begin
  SET @AuthIDResp = '0'
 end

 IF (@MessageType = '0220' OR @MessageType = '0221')    
 BEGIN    
  IF (@ForwardInstID = '500000')    
  BEGIN    
   SET @IsClearingTrx = 1    
   SET @TrxDetails = 'Cash Withdrawal Trx II'
  END    
  ELSE    
  BEGIN    
   SET @IsClearingTrx = 0    
   SET @TrxDetails = 'Cash Withdrawal Trx'
  END    
 END    
 
 select @IsGCC=ISNULL(IsGCC,0), @CountryCode=ISNULL(CountryCode,'') from t_Country where OurBranchID = @OurBranchID AND ShortName = RIGHT(@NameLocation, 2)
    
 SELECT @mLastEOD=LastEOD,@mwDate=WorkingDate,@mCONFRETI=CONFRETI FROM t_Last WHERE (OurBranchID = @OurBranchID)      
 
 SET @sCount = 0    
 SET @cCount = 1    
    
 WHILE (@sCount <> @cCount)      
 BEGIN      
  exec @mScrollNo = fnc_GetRandomScrollNo    
  if not exists (SELECT ScrollNo from t_ATM_CashTransaction Where OurBranchID = @OurBranchID AND ScrollNo = @mScrollNo)    
  BEGIN    
   SET @sCount = @sCount + 1      
  END    
 END    
 
 if (@CountryCode = '784')    
 BEGIN    
  SET @ATMChargeID = '600000'    
  --SET @mDescriptionID = '801'    --old
  --SET @mDescriptionIDChg1 = '802'    --old
  --SET @mDescriptionIDChg2 = '803'    --old

  SET @mDescriptionID = '200130'    --new
  SET @mDescriptionIDChg1 = '400710'    --new
  SET @mDescriptionIDChg2 = '400020'    --new

  SET @mDescription = 'Cash Withdrawal at local ATM ' + convert(varchar(10), @mwDate, 120)
  SET @mRemarks=''

  SET @mDescriptionCharges = 'Cash Withdrawal from Local ATM Fees ATM('+@ATMID+') FROM ' + @NameLocation + '-' + convert(varchar(20), @PHXDate, 120)
  SET @mRemarksChgs=''
  
  SET @mDescriptionCharges2 = 'Value Added Tax (VAT) for transaction:  - '+Cast(@mScrollNo as varchar(30))
  SET @mRemarksChgs2=''

 END    
 else if (@IsGCC = 1)    
 BEGIN    
  SET @ATMChargeID = 'GCC'    
  
  --SET @mDescriptionID = '801'    --old
  --SET @mDescriptionIDChg1 = '802'    --old
  --SET @mDescriptionIDChg2 = '803'    --old

  SET @mDescriptionID = '200280'    --new
  SET @mDescriptionIDChg1 = '400790'    --new
  SET @mDescriptionIDChg2 = '400020'    --new

  SET @mDescription = 'Cash Withdrawal at GCC ATM ' + convert(varchar(10), @mwDate, 120)
  SET @mRemarks=''

  SET @mDescriptionCharges = 'Cash Withdrawal from GCC ATM Fees ATM('+@ATMID+') FROM ' + @NameLocation + '-' + convert(varchar(20), @PHXDate, 120)
  SET @mRemarksChgs=''
  
  SET @mDescriptionCharges2 = 'Value Added Tax (VAT) for transaction:  - '+Cast(@mScrollNo as varchar(30))
  SET @mRemarksChgs2=''

 END    
 ELSE    
 BEGIN    
  SET @ATMChargeID = '500000'    
  --SET @mDescriptionID = '804'    --old
  --SET @mDescriptionIDChg1 = '805'    --old
  --SET @mDescriptionIDChg2 = '806'    --old

  SET @mDescriptionID = '200140'        --new
  SET @mDescriptionIDChg1 = '400720'        --new
  SET @mDescriptionIDChg2 = '400020'    --new

  SET @mDescription = 'Cash Withdrawal at overseas ATM ' + convert(varchar(10), @mwDate, 120)
  SET @mRemarks=''

  SET @mDescriptionCharges = 'Cash Withdrawal from International ATM Fees ATM('+@ATMID+') FROM ' + @NameLocation + '-' + convert(varchar(20), @PHXDate, 120)
  SET @mRemarksChgs=''

  SET @mDescriptionCharges2 = 'Value Added Tax (VAT) for transaction RetRefNum:  - '+Cast(@mScrollNo as varchar(30))
  SET @mRemarksChgs2=''

 END 
 
 SET @NarrationID = @mDescriptionID
 SET @ValueDate = @mwDate
 
 IF (@ExtendedData = 'DA') -- For Declinals the account id has tokanized PAN
 begin
  SELECT @AccountID=ISNULL(AccountID,'') from t_DebitCards where OurBranchID = @OurBranchID AND DebitCardToken = @AccountID
 END
 
 SELECT @vCount1=count(*) From t_AccountBalance WHERE (OurBranchID = @OurBranchID) and (AccountID=@AccountID)          
 IF @vCount1=0          
 BEGIN       
  /** Rejection change STARTS 06/04/2021 **/
  INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
  VALUES (@OurBranchID, @AccountID, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, 
  @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @mDescription, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '02', 'Invalid Account', @TrxDetails, @AuthIDResp, @ExtendedData)
  
  SELECT @Status = 2, @AvailableBalance = 0, @vClearBalance = 0    
  SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value,'784' as CurrencyCode, @AuthIDResp as AuthIDResp,
  @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
  RETURN (1)    
  
  /** Rejection change ENDS 06/04/2021 **/
 END
 
 SELECT @mAccountName=B.Name,@mProductID=B.ProductID,@mCurrencyID=P.CurrencyID,@mAccountType='C',@Limit=ISNULL(B.Limit,0),
 @vClearBalance=ISNULL(B.ClearBalance,0),@vClearedEffects=ISNULL(B.Effects,0),@vFreezeAmount=ISNULL(B.FreezeAmount,0),    
 @vMinBalance=ISNULL(P.MinBalance,0),@vIsClose=ISNULL(A.Status,''),@vAreCheckBooksAllowed=P.AreCheckBooksAllowed,    
 @vAllowDebitTransaction=A.AllowDebitTransaction,@value=ISNULL(p.productatmcode,'00'), @CurrencyCode = ISNULL(c.CurrencyCode,''),    
 @SOCID=ISNULL(A.SOCID,''),@ProductID=A.ProductID,@NoOfCWTrxATMMonthly=B.NoOfCWTrxATMMonthly,@MobileNo=ISNULL(cust.MobileNo,''),@Email=ISNULL(cust.Email,''),@ClientID=cust.ClientID
 FROM t_AccountBalance B       
 INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND B.ProductID = A.ProductID         
 INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND A.ProductID = P.ProductID       
 INNER JOIN t_Currencies C ON A.OurBranchID = C.OurBranchID AND A.CurrencyID = C.CurrencyID    
 INNER JOIN t_Customer cust ON B.OurBranchID = cust.OurBranchID AND A.ClientID = cust.ClientID
 WHERE (A.OurBranchID = @OurBranchID) and (A.AccountID = @AccountID)            
 
 --exec @Limit = [fnc_GetLimit] @OurBranchID, @AccountID    
     
 IF @Limit > 0 AND @vCheckLimit = 1 AND @vODLimitAllow = 1    
 BEGIN    
  SET @vClearBalance=(@vClearBalance+@Limit)    
 END    
 
 
 /*****DECLINE ADVISE STARTS *****/
 
 IF (@ExtendedData = 'DA')
 begin
 
  Insert into t_ENDeclineAdvices (OurBranchID, AccountID, RetRefNum, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, 
  ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, CardAccptID, CardAccptNameLoc, 
  DescriptionID, VISATrxID, CAVV, ResponseCode)
  VALUES        
  (        
   @OurBranchID,@AccountID,@RetRefNum,@STAN,@PHXDate,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@Amount,@mForeignAmount,@mExchangeRate,@ConvRate,@MerchantType,
   @AcqCountryCode,@ForwardInstID,@ATMID,@CurrCodeTran,@CurrCodeSett,@SettlmntAmount,@ProcCode,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@CardAccptID,
   @NameLocation,@NarrationID,@VISATrxID,@CAVV,@ResponseCode    
  )
 
  SELECT @Status    = 0,       
  @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,      
  @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100      
      
  SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value, @CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp,
  @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
  RETURN 0;    
  
 end
 
 /*****DECLINE ADVISE ENDS *****/
 
 IF @mLastEOD = @mwDate or @mCONFRETI = '1/1/9999'
 BEGIN    
  /** Rejection change STARTS 06/04/2021 **/
  
  INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
  VALUES (@OurBranchID, @AccountID, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, 
  @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @mDescription, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '95', 'Day End', @TrxDetails, @AuthIDResp, @ExtendedData)
  
  SELECT @Status = 95,    
  @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,    
  @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100    
    
  SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value,@CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp,
  @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
  RETURN (1)     
  /** Rejection change ENDS 06/04/2021 **/
 END    
 
 /** If its a clearing trx then release hold -- STARTS **/    
 
 IF (@IsClearingTrx = 1)    
 BEGIN    
  SELECT @mCount=count(AccountID) FROM t_ATM_HoldTrxs WHERE (OurBranchID=@OurBranchID) AND (AccountID=@AccountID)     
  AND (RefNo IN (@OrgTrxRefNo,@OrgTrxRefNo+'|CHGS1',@OrgTrxRefNo+'|CHGS2') AND VISATrxID=@VISATrxID) AND HoldStatus IN ('H','P') group by HoldStatus    
    
  SELECT @remAmount = SUM(ISNULL(RemainingAmount,0)) FROM t_ATM_HoldTrxs WHERE (OurBranchID=@OurBranchID) AND (AccountID=@AccountID)     
  AND (RefNo = @OrgTrxRefNo AND VISATrxID=@VISATrxID) AND HoldStatus IN ('H','P') group by HoldStatus    
    
  IF @mCount = 0    
  BEGIN --NO TRX ON ACCOUNT    
   /** Rejection change STARTS 06/04/2021 **/
  
   INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
   VALUES (@OurBranchID, @AccountID, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, 
   @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @mDescription, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '21', 'No Transaction on Account', @TrxDetails, @AuthIDResp, @ExtendedData)
   
   SELECT @Status = 21,    
   @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,    
   @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100    
   SELECT @Status As Status , @vClearBalance As ClearBalance , @AvailableBalance As AvailableBalance , @value As [value],@CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp,
   @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
   RETURN (1)     
   /** Rejection change ENDS 06/04/2021 **/
  END    
    
  if (@CurrCodeSett = '784' AND @CurrCodeTran = '784') --Validate amount only if its a local currency trx..      
  BEGIN    
   IF @Amount <> @remAmount    
   BEGIN --BAD AMOUNT    
    /** Rejection change STARTS 06/04/2021 **/
  
    INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
    VALUES (@OurBranchID, @AccountID, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, 
    @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @mDescription, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '48', 'Bad Amount', @TrxDetails, @AuthIDResp, @ExtendedData)
    
    SELECT @Status = 48,    
    @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,    
    @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100    
    
    SELECT @Status As Status , @vClearBalance As ClearBalance , @AvailableBalance As AvailableBalance , @value As [value],@CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp,
	@MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
    
    RETURN (1)     
    /** Rejection change ENDS 06/04/2021 **/
   END    
  END    
  
  SET @sCount = 0    
  SET @cCount = 0
    
  Select * Into #TempCashTransaction FROM t_ATM_HoldTrxs WHERE (OurBranchID=@OurBranchID) AND (AccountID=@AccountID)     
  --AND (RefNo=@OrgTrxRefNo OR VISATrxID=@VISATrxID) AND HoldStatus IN ('H','P')    
  AND (RefNo IN (@OrgTrxRefNo,@OrgTrxRefNo+'|CHGS1',@OrgTrxRefNo+'|CHGS2') AND VISATrxID=@VISATrxID) AND HoldStatus IN ('H','P')    

  SELECT @cCount=COUNT(*) from #TempCashTransaction    
  
  WHILE (@cCount <> @sCount)    
  BEGIN    
  
   SET @sCount = @sCount + 1    
       
   --Clear Hold Trx Status    
   SELECT @mAccountID=AccountID, @mRefNo=RefNo, @mHoldStatus=HoldStatus, @ValueDate=convert(varchar(10), wDate, 120)    
   FROM (    
    SELECT AccountID, RefNo, HoldStatus, wDate, ROW_NUMBER() OVER (ORDER BY RefNo) AS RowNum    
    FROM #TempCashTransaction    
   ) AS oTable    
   WHERE oTable.RowNum = @sCount    
    
   UPDATE t_ATM_HoldTrxs SET HoldStatus = 'C', ReleaseDate = @mwDate + ' ' + convert(varchar(12), GetDate(), 114)    
  WHERE OurBranchID = @OurBranchID AND AccountID = @mAccountID AND RefNo = @mRefNo AND HoldStatus = @mHoldStatus    
        
   --Get Hold Amount    
   SELECT @HoldAmount=Amount FROM t_AccountFreeze     
   WHERE OurBranchID=@OurBranchID AND AccountID=@mAccountID AND ATMTrxHoldRef=@mRefNo AND IsFreeze = 1    

   --update account freeze to unfreeze    
   Update t_AccountFreeze SET IsFreeze = 0, UnFreezedDate = @mwDate + ' ' + convert(varchar(12), GetDate(), 114),     
   ReleaseComments = @mDescription    
   WHERE OurBranchID = @OurBranchID AND AccountID = @mAccountID AND ATMTrxHoldRef = @mRefNo    
   

   Update t_AccountBalance SET FreezeAmount = ISNULL(FreezeAmount,0) - ISNULL(@HoldAmount,0)    
   WHERE OurBranchID = @OurBranchID AND AccountID = @mAccountID    
       
   SELECT @FreezeAmount = FreezeAmount FROM t_AccountBalance WHERE OurBranchID = @OurBranchID AND AccountID = @mAccountID    
       
   IF (@FreezeAmount <= 0)    
   BEGIN    
    Update t_AccountBalance SET IsFreezed = 0    
    WHERE OurBranchID = @OurBranchID AND AccountID = @mAccountID    
   END    
    
   /* Log the Updated Balance after releasing freeze */    
    
   Insert Into BalChk     
   (    
    OurBranchID, AccountID, ProductID, [Name], LastUpdateTime, ClearBalance, Limit, Effects, Shadow, FreezeAmt, MinBalance,     
    TotalBalance, AvailableBalance    
   )    
   SELECT OurBranchID, AccountID, ProductID, [Name], @mwDate + ' ' + convert(varchar(12), GetDate(), 114), ISNULL(ClearBalance,0), ISNULL(Limit,0),     
   ISNULL(Effects,0), ISNULL(ShadowBalance,0), ISNULL(FreezeAmount,0), @vMinBalance,     
   (isnull(@Limit,0)+ISNULL(ClearBalance,0)-ISNULL(Effects,0)),    
   (isnull(@Limit,0)+ISNULL(ClearBalance,0)-ISNULL(FreezeAmount,0)-@vMinBalance)    
   FROM t_AccountBalance WHERE OurBranchID = @OurBranchID AND AccountID = @mAccountID    
    
  END    
    
  /* Get Latest Balance again.. */    
     
  SELECT @mAccountName=B.Name,@mProductID=B.ProductID,@mCurrencyID=P.CurrencyID,@mAccountType='C',@Limit=ISNULL(Limit,0),
  @vClearBalance=ISNULL(B.ClearBalance,0),@vClearedEffects=ISNULL(B.Effects,0),@vFreezeAmount=ISNULL(B.FreezeAmount,0),    
  @vMinBalance=ISNULL(P.MinBalance,0), @vIsClose=ISNULL(A.Status,''),@vAreCheckBooksAllowed=P.AreCheckBooksAllowed,    
  @vAllowDebitTransaction=A.AllowDebitTransaction,@value=ISNULL(p.productatmcode,'00'), @CurrencyCode = ISNULL(c.CurrencyCode,''),    
  @SOCID=ISNULL(A.SOCID,''),@ProductID=A.ProductID,@NoOfCWTrxATMMonthly=B.NoOfCWTrxATMMonthly    
  FROM t_AccountBalance B       
  INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND B.ProductID = A.ProductID         
  INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND A.ProductID = P.ProductID       
  INNER JOIN t_Currencies C ON A.OurBranchID = C.OurBranchID AND A.CurrencyID = C.CurrencyID    
  WHERE (A.OurBranchID = @OurBranchID) and (A.AccountID = @AccountID)          
     
  --exec @Limit = [fnc_GetLimit] @OurBranchID, @AccountID    
     
  IF @Limit > 0 AND @vCheckLimit = 1 AND @vODLimitAllow = 1    
  BEGIN    
   SET @vClearBalance=(@vClearBalance+@Limit)    
  END    
    
  /** If its a clearing trx then release hold -- ENDS **/    
 END    
 ELSE
 BEGIN
  SET @OrgTrxRefNo = @RefNo
 END
     
 SELECT @vCount1=count(*),@Super=Supervision From t_ATM_CashTransaction      
 WHERE (OurBranchID = @OurBranchID) AND (AccountID=@AccountID) AND (RefNo=@RefNo) and (AtmID=@ATMID) group by Supervision      
 
 
 IF @vCount1>0 and @Super='C'
 BEGIN       --DUP_TRAN    
  
  /** Rejection change STARTS 06/04/2021 **/
  
  INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
  VALUES (@OurBranchID, @AccountID, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, 
  @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, 
  @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @mDescription, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '10', 'Duplicate Transaction', @TrxDetails, @AuthIDResp, @ExtendedData)
  
  SELECT @Status = 10,     
  @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,    
  @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100    
      
  SELECT @Status As Status , @vClearBalance As ClearBalance , @AvailableBalance As AvailableBalance, @value As value,@CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp,
  @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
  RETURN (1)    
  
  /** Rejection change ENDS 06/04/2021 **/
  
 END
 
 IF @vCount1=1 and @Super='R'          
 BEGIN   --ORIG_ALREADY_REVERSED          
  /** Rejection change STARTS 06/04/2021 **/
  
  INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
  VALUES (@OurBranchID, @AccountID, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, 
  @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, 
  @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @mDescription, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '35', 'Orig Already Reversed', @TrxDetails, @AuthIDResp, @ExtendedData)
  
  SELECT @Status = 35,     
  @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,    
  @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100    
    
  SELECT @Status As Status , @vClearBalance As ClearBalance , @AvailableBalance As AvailableBalance , @value As value, @CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp,
  @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
  RETURN (1)    
  
  /** Rejection change ENDS 06/04/2021 **/
 END      
 
 IF (@SOCID <> '')    
 BEGIN    
  
  SELECT @WithDrawlCharges=WithDrawlCharges,@AcquiringChargesAmount=AcquiringChargesAmount,@ExciseDutyPercentage=ExciseDutyPercentage,
  @IssuerChargesAccountID=IssuerChargesAccountID,@AcquiringChargesAccountID=AcquiringChargesAccountID,@ExciseDutyAccountID=ExciseDutyAccountID    
  From t_SOCATMCharges          
  WHERE (OurBranchID = @OurBranchID AND ProductID = @ProductID AND SOCID = @SOCID AND ATMChargeID = @ATMChargeID AND NoOfFreeCW <= @NoOfCWTrxATMMonthly)    
    
  IF (@ExciseDutyPercentage > 0)
  BEGIN
   SET @ExciseDutyAmount = ROUND(@AcquiringChargesAmount * @ExciseDutyPercentage / 100, 2)    
   SET @AppWHTax = 1
   SET @WHTaxAmount = @ExciseDutyAmount
  END
 END    
 
 if (@MessageType = '0200')
 begin        
  IF @vIsClose = 'C' or @vIsClose = 'T' or @vIsClose = 'X' or @vIsClose = 'D' or @vIsClose = 'I' or @vAllowDebitTransaction = 1          
  BEGIN       
   /** Rejection change STARTS 06/04/2021 **/
  
   INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
   VALUES (@OurBranchID, @AccountID, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, 
   @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, 
   @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @mDescription, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '03', 'Account In-Active/ Closed/ Dormant/ Deceased/ Blocked', @TrxDetails, @AuthIDResp, @ExtendedData)
   
   SELECT @Status = 3,     
   @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,    
   @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100    
     
   SELECT @Status As Status , @vClearBalance As ClearBalance , @AvailableBalance As AvailableBalance , @value As value,@CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp,
   @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
   RETURN (1)
   
   /** Rejection change ENDS 06/04/2021 **/
  END 
  
  IF @Amount > (@vClearBalance-@AcquiringChargesAmount-@ExciseDutyAmount) OR       
     @Amount > (@vClearBalance-@vFreezeAmount-@vMinBalance-@AcquiringChargesAmount-@ExciseDutyAmount)    
  BEGIN      
   --LOW_BALANCE          
   
   /** Rejection change STARTS 06/04/2021 **/
  
   INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
   VALUES (@OurBranchID, @AccountID, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, 
   @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, 
   @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @mDescription, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '04', 'Low Balance', @TrxDetails, @AuthIDResp, @ExtendedData)
   
   SELECT @Status = 4,     
   @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,    
   @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100    
     
   SELECT @Status As Status , @vClearBalance As ClearBalance , @AvailableBalance As AvailableBalance, @value As value,@CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp,
   @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
   RETURN (1)           
   
   /** Rejection change ENDS 06/04/2021 **/
   
  END    
 END
 
 if (@CountryCode = '784')    
 BEGIN    
  IF (@ForwardInstID = '500000')
  BEGIN    
   SET @ATMChargeID = '500000'    
  END
  ELSE
  BEGIN
   SET @ATMChargeID = '600000'    
  END
 END
 else if (@IsGCC = 1)    
 BEGIN    
  IF (@ForwardInstID = '500000')
  BEGIN    
   SET @ATMChargeID = '500000'    
  END
  ELSE
  BEGIN
   SET @ATMChargeID = 'GCC'    
  END
 END
 ELSE    
 BEGIN    
  SET @ATMChargeID = '500000'    
 END
 
 IF NOT EXISTS (SELECT AccountID FROM t_GL WHERE (OurBranchID = @OurBranchID) AND       
 (AccountID =  (SELECT  ATMAccountID FROM t_ATM_ATMAccount WHERE OurBranchID = @OurBranchID and ATMID = @ATMChargeID)))        
 BEGIN       
  /** Rejection change STARTS 06/04/2021 **/
  
  INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
  VALUES (@OurBranchID, @AccountID, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, 
  @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, 
  @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @mDescription, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '79', 'Invalid ATM ID', @TrxDetails, @AuthIDResp, @ExtendedData)
  
  SELECT @Status = 79,     
  @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,    
  @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100    
    
  SELECT @Status As Status , @vClearBalance As ClearBalance , @AvailableBalance As AvailableBalance , @value As value, @CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp,
  @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
  RETURN (1) 
  
  /** Rejection change ENDS 06/04/2021 **/
 END      
       
 SET @mGLID = '00000000'          
 SELECT @mGLID=AccountID FROM t_GL WHERE (OurBranchID = @OurBranchID) and         
 (AccountID =  (SELECT  ATMAccountID FROM t_ATM_ATMAccount WHERE OurBranchID = @OurBranchID and ATMID = @ATMChargeID))          
     
 SELECT @vBranchName=BranchName FROM t_ATM_Branches WHERE (OurBranchID = @OurBranchID AND BranchID = @WithdrawlBranchID)    
     
 SELECT @GLControl = ISNULL(GLControl, '') from t_Products where OurBranchID = @OurBranchID and ProductID = @mProductID      
 SET @VoucherID = @mScrollNo
        
 SET @mSerialNo = @mSerialNo+1      
 SET @GLSerialID = @GLSerialID+1      
 SET @IsCredit = 0      
 
 if (@AuthIDResp = '0')
 begin
  exec @AuthIDResp = fnc_GetRandomNumber    
 end
       
 INSERT INTO t_ATM_CashTransaction       
 (      
  ScrollNo,SerialNo,RefNo,OurBranchID,CustomerBranchID,CustomerAccountID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,ValueDate,wDate,    
  PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,[Description],ATMID,Remarks,OperatorID,SupervisorID,MerchantType,STAN,    
  SettlmntAmount,ConvRate,AcqCountryCode,CurrCodeTran,CurrCodeSett,ATMStatus,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,    
  CardAccptID,CardAccptNameLoc,VISATrxID,ProcCode,CAVV,ResponseCode,ForwardInstID,ClearingRefNo
 )      
 VALUES          
 (
  @mScrollNo,@mSerialNo,@OrgTrxRefNo,@OurBranchID,@CustomerBranchID,@mCustomerAccountID,@AccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,    
  @mIsLocalCurrency,@ValueDate,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@PHXDate,@mTrxType,@Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,    
  @mDescriptionID,@mDescription,@ATMID,@mRemarks,@mOperatorID,@mSupervisorID,@MerchantType,@STAN,@SettlmntAmount,@ConvRate,@AcqCountryCode,    
  @CurrCodeTran,@CurrCodeSett,@Supervision,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@RetRefNum,@AuthIDResp,@CardAccptID,@NameLocation,    
  @VISATrxID,@ProcCode,@CAVV,@ResponseCode,@ForwardInstID,@RefNo
 )    
     
 INSERT INTO t_Transactions     
 (      
  OurBranchID,ScrollNo,SerialNo,RefNo,wDate,AccountType,DocumentType,AccountID,AccountName,ProductID,CurrencyID,ValueDate,TrxType,ChequeID,ChequeDate,Amount,    
  ForeignAmount,ExchangeRate,ProfitLoss,MeanRate,DescriptionID,[Description],BankCode,BranchCode,TrxPrinted,GLID,[Status],IsLocalCurrency,OperatorID,    
  SupervisorID,AdditionalData,IsMainTrx,DocType,GLVoucherID,Remarks,MerchantType,STAN,SettlmntAmount,ConvRate,AcqCountryCode,CurrCodeTran,CurrCodeSett,ATMStatus,    
  POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,CardAccptID,CardAccptNameLoc,VISATrxID,ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,ForwardInstID,ClearingRefNo,TClientId,TAccountid,TProductId
 )      
 VALUES       
 (      
  @OurBranchID,@mScrollNo,@mSerialNo,@OrgTrxRefNo,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@mAccountType,'ATC',@AccountID,@mAccountName,@mProductID,    
  @mCurrencyID,@ValueDate,@mTrxType,'',@mwDate,@Amount,@mForeignAmount,@mExchangeRate,0,@mExchangeRate,@mDescriptionID,@mDescription,@mBankCode,    
  @WithdrawlBranchID,0,@mGLID,@Supervision,@mIsLocalCurrency,@mOperatorID,@mSupervisorID,null,'1','ATC',@VoucherID,@mRemarks,@MerchantType,@STAN,@SettlmntAmount,    
  @ConvRate,@AcqCountryCode,@CurrCodeTran,@CurrCodeSett,@Supervision,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@RetRefNum,@AuthIDResp,    
 @CardAccptID,@NameLocation,@VISATrxID,@ChannelId,@ChannelRefID,@ProcCode,@CAVV,@ResponseCode,@ForwardInstID,@RefNo,@ClientID,@AccountID,@mProductID
 )      

 --set @mScrollNo = 0 for @GLControl before @mSerialNo
          
 INSERT INTO t_GLTransactions       
 (      
  OurBranchID,RefNo,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,ExchangeRate,IsCredit,TransactionType,    
  TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,IsMainTrx,DocType,Remarks,MerchantType,STAN,SettlmntAmount,ConvRate,    
  AcqCountryCode,CurrCodeTran,CurrCodeSett,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,CardAccptID,CardAccptNameLoc,VISATrxID,    
  ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,ForwardInstID,TClientId,TAccountid,TProductId
 )      
 VALUES      
 (      
  @OurBranchID,@OrgTrxRefNo,@GLControl,@VoucherID,@GLSerialID,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@ValueDate,@mDescriptionID,@mDescription,
  @mCurrencyID,@Amount,@mForeignAmount,@mExchangeRate,@IsCredit,'ATMCash','L',@mOperatorID,@mSupervisorID,'',0,@mSerialNo,null,'0','ATC',@mRemarks,    
  @MerchantType,@STAN,@SettlmntAmount,@ConvRate,@AcqCountryCode,@CurrCodeTran,@CurrCodeSett,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,    
  @RetRefNum,@AuthIDResp,@CardAccptID,@NameLocation,@VISATrxID,@ChannelId,@ChannelRefID,@ProcCode,@CAVV,@ResponseCode,@ForwardInstID,@ClientID,@AccountID,@mProductID
 )    
    
 IF @mTrxType = 'D'      
 BEGIN      
  SET @IsCredit = 1      
 END      
 ELSE      
 BEGIN      
  SET @IsCredit = 0      
 END      
 
 SET @GLSerialID = @GLSerialID+1      
 
 INSERT INTO t_GLTransactions       
 (      
  OurBranchID,RefNo,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,ExchangeRate,IsCredit,TransactionType,    
  TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,IsMainTrx,DocType,Remarks,MerchantType,STAN,SettlmntAmount,ConvRate,    
  AcqCountryCode,CurrCodeTran,CurrCodeSett,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,CardAccptID,CardAccptNameLoc,VISATrxID,    
  ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,ForwardInstID,TClientId,TAccountid,TProductId
 )      
 VALUES      
 (      
  @OurBranchID,@OrgTrxRefNo,@mGLID,@VoucherID,@GLSerialID,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@ValueDate,@mDescriptionID,@mDescription,
  @mCurrencyID,@Amount,@mForeignAmount,@mExchangeRate,@IsCredit,'ATMCash','L',@mOperatorID,@mSupervisorID,'',@mScrollNo,@mSerialNo,null,'0','ATC',@mRemarks,    
  @MerchantType,@STAN,@SettlmntAmount,@ConvRate,@AcqCountryCode,@CurrCodeTran,@CurrCodeSett,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,      
  @RetRefNum,@AuthIDResp,@CardAccptID,@NameLocation,@VISATrxID,@ChannelId,@ChannelRefID,@ProcCode,@CAVV,@ResponseCode,@ForwardInstID,@ClientID,@AccountID,@mProductID
 )      
       
 --Debit Switch Charges             
 IF @AcquiringChargesAmount > 0      
 BEGIN       

  SET @mSerialNo = @mSerialNo + 1      
  SET @IsCredit = 0      
  
  select @VatReferenceID = dbo.fn_GenerateText(10)
    
  INSERT INTO t_ATM_CashTransaction      
  (      
   ScrollNo,SerialNo,RefNo,OurBranchID,CustomerBranchID,CustomerAccountID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,ValueDate,wDate,    
   PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,[Description],ATMID,Remarks,OperatorID,SupervisorID,MerchantType,STAN,    
   SettlmntAmount,ConvRate,AcqCountryCode,CurrCodeTran,CurrCodeSett,ATMStatus,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,    
   CardAccptID,CardAccptNameLoc,VISATrxID,ProcCode,CAVV,ResponseCode,ForwardInstID,ClearingRefNo
  )      
  VALUES       
  (      
   @mScrollNo,@mSerialNo,@OrgTrxRefNo+'|CHGS1',@OurBranchID,@CustomerBranchID,@mCustomerAccountID,@AccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,    
   @mIsLocalCurrency,@ValueDate,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@PHXDate,@mTrxType,@Supervision,@AcquiringChargesAccountID,@AcquiringChargesAmount,    
   @mForeignAmount,@mExchangeRate,@mDescriptionIDChg1,@mDescriptionCharges,@ATMID,@mRemarksChgs,@mOperatorID,@mSupervisorID,@MerchantType,@STAN,    
   0,@ConvRate,@AcqCountryCode,@CurrCodeTran,@CurrCodeSett,@Supervision,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@RetRefNum,    
   @AuthIDResp,@CardAccptID,@NameLocation,@VISATrxID,@ProcCode+'|CW CHG',@CAVV,@ResponseCode,@ForwardInstID,@RefNo
  )      
        
  INSERT INTO t_Transactions       
  (      
   OurBranchID,ScrollNo,SerialNo,RefNo,wDate,AccountType,DocumentType,AccountID,AccountName,ProductID,CurrencyID,ValueDate,TrxType,ChequeID,ChequeDate,Amount,    
   ForeignAmount,ExchangeRate,ProfitLoss,MeanRate,DescriptionID,[Description],BankCode,BranchCode,TrxPrinted,GLID,[Status],IsLocalCurrency,OperatorID,    
   SupervisorID,AdditionalData,IsMainTrx,DocType,GLVoucherID,Remarks,MerchantType,STAN,SettlmntAmount,ConvRate,AcqCountryCode,CurrCodeTran,CurrCodeSett,ATMStatus,    
   POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,CardAccptID,CardAccptNameLoc,VISATrxID,ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,ForwardInstID,ClearingRefNo,AppWHTax,WHTaxAmount,WHTScrollNo, VATID,[Percent],TClientId,TAccountid,TProductId,VatReferenceID
  )      
  VALUES       
  (      
   @OurBranchID,@mScrollNo,@mSerialNo,@OrgTrxRefNo+'|CHGS1',@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@mAccountType,'ATC',@AccountID,@mAccountName,@mProductID,    
   @mCurrencyID,@ValueDate,@mTrxType,'',@mwDate,@AcquiringChargesAmount,@mForeignAmount,@mExchangeRate,0,@mExchangeRate,@mDescriptionIDChg1,@mDescriptionCharges,    
   @mBankCode,@WithdrawlBranchID,0,@AcquiringChargesAccountID,@Supervision,@mIsLocalCurrency,@mOperatorID,@mSupervisorID,null,'1','ATC',@VoucherID,@mRemarksChgs,    
   @MerchantType,@STAN,0,@ConvRate,@AcqCountryCode,@CurrCodeTran,@CurrCodeSett,@Supervision,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,    
   @AckInstIDCode,@RetRefNum,@AuthIDResp,@CardAccptID,@NameLocation,@VISATrxID,@ChannelId,@ChannelRefID,@ProcCode+'|CW CHG',@CAVV,@ResponseCode,@ForwardInstID,@RefNo,@AppWHTax,@WHTaxAmount,@mScrollNo, 
   (case when @ExciseDutyAmount > 0 then '003' else NULL end), 
   (case when @ExciseDutyAmount > 0 then @ExciseDutyPercentage else NULL end),
   @ClientID,@AccountID,@mProductID,
   (case when @ExciseDutyAmount > 0 then @VatReferenceID else NULL end)
  )      
  
  SET @GLSerialID = @GLSerialID+1

  --set @mScrollNo = 0 for @GLControl before @mSerialNo
           
  INSERT INTO t_GLTransactions      
  (      
   OurBranchID,RefNo,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,ExchangeRate,IsCredit,TransactionType,    
   TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,IsMainTrx,DocType,Remarks,MerchantType,STAN,SettlmntAmount,ConvRate,    
   AcqCountryCode,CurrCodeTran,CurrCodeSett,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,CardAccptID,CardAccptNameLoc,VISATrxID,    
   ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,ForwardInstID, VATID,[Percent],TClientId,TAccountid,TProductId
  )      
  VALUES      
  (      
   @OurBranchID,@OrgTrxRefNo+'|CHGS1',@GLControl,@VoucherID,@GLSerialID,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@ValueDate,@mDescriptionIDChg1,    
   @mDescriptionCharges,@mCurrencyID,@AcquiringChargesAmount,@mForeignAmount,@mExchangeRate,@IsCredit,'ATMCash','L',@mOperatorID,@mSupervisorID,'',    
   0,@mSerialNo,null,'0','ATC',@mRemarksChgs,@MerchantType,@STAN,0,@ConvRate,@AcqCountryCode,@CurrCodeTran,@CurrCodeSett,@POSEntryMode,    
   @POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@RetRefNum,@AuthIDResp,@CardAccptID,@NameLocation,@VISATrxID,@ChannelId,@ChannelRefID,@ProcCode+'|CW CHG',@CAVV,@ResponseCode,@ForwardInstID,
   (case when @ExciseDutyAmount > 0 then '003' else NULL end), 
   (case when @ExciseDutyAmount > 0 then @ExciseDutyPercentage else NULL end),
   @ClientID,@AccountID,@mProductID
  )      
          
  IF @mTrxType = 'D'      
  BEGIN      
   SET @IsCredit = 1      
  END      
  ELSE      
  BEGIN      
   SET @IsCredit = 0      
  END      
  
  SET @GLSerialID = @GLSerialID+1      
  SELECT @gAccountID=AccountID, @gCurrencyID=CurrencyID FROM t_GL WHERE (OurBranchID = @OurBranchID) AND (AccountID =  @AcquiringChargesAccountID)      
          
  INSERT INTO t_GLTransactions       
  (      
   OurBranchID,RefNo,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,ExchangeRate,IsCredit,TransactionType,    
   TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,IsMainTrx,DocType,Remarks,MerchantType,STAN,SettlmntAmount,ConvRate,    
   AcqCountryCode,CurrCodeTran,CurrCodeSett,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,CardAccptID,CardAccptNameLoc,VISATrxID,    
   ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,ForwardInstID, VATID,[Percent],TClientId,TAccountid,TProductId
  )      
  VALUES      
  (      
   @OurBranchID,@OrgTrxRefNo+'|CHGS1',@gAccountID,@VoucherID,@GLSerialID,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@ValueDate,@mDescriptionIDChg1,    
   @mDescriptionCharges,@gCurrencyID,@AcquiringChargesAmount,@mForeignAmount,@mExchangeRate,@IsCredit,'ATMCash','L',@mOperatorID,@mSupervisorID,'',    
   @mScrollNo,@mSerialNo,null,'0','ATC',@mRemarksChgs,@MerchantType,@STAN,0,@ConvRate,@AcqCountryCode,@CurrCodeTran,@CurrCodeSett,@POSEntryMode,    
   @POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@RetRefNum,@AuthIDResp,@CardAccptID,@NameLocation,@VISATrxID,@ChannelId,@ChannelRefID,@ProcCode+'|CW CHG',@CAVV,@ResponseCode,@ForwardInstID, 
   (case when @ExciseDutyAmount > 0 then '003' else NULL end), 
   (case when @ExciseDutyAmount > 0 then @ExciseDutyPercentage else NULL end),
   @ClientID,@AccountID,@mProductID
  )    
    
  IF @ExciseDutyAmount > 0      
  BEGIN       
   SET @mSerialNo = @mSerialNo + 1      
   SET @IsCredit = 0      
      
   INSERT INTO t_ATM_CashTransaction      
   (      
    ScrollNo,SerialNo,RefNo,OurBranchID,CustomerBranchID,CustomerAccountID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,ValueDate,wDate,    
    PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,[Description],ATMID,Remarks,OperatorID,SupervisorID,MerchantType,STAN,    
    SettlmntAmount,ConvRate,AcqCountryCode,CurrCodeTran,CurrCodeSett,ATMStatus,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,    
    CardAccptID,CardAccptNameLoc,VISATrxID,ProcCode,CAVV,ResponseCode,ForwardInstID,ClearingRefNo
   )      
   VALUES       
   (      
    @mScrollNo,@mSerialNo,@OrgTrxRefNo+'|CHGS2',@OurBranchID,@CustomerBranchID,@mCustomerAccountID,@AccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,    
    @mIsLocalCurrency,@ValueDate,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@PHXDate,@mTrxType,@Supervision,@ExciseDutyAccountID,@ExciseDutyAmount,    
    @mForeignAmount,@mExchangeRate,@mDescriptionIDChg2,@mDescriptionCharges2,@ATMID,@mRemarksChgs2,@mOperatorID,@mSupervisorID,@MerchantType,@STAN,    
    0,@ConvRate,@AcqCountryCode,@CurrCodeTran,@CurrCodeSett,@Supervision,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@RetRefNum,    
    @AuthIDResp,@CardAccptID,@NameLocation,@VISATrxID,@ProcCode+'|CW CHG',@CAVV,@ResponseCode,@ForwardInstID,@RefNo
   )      
         
   INSERT INTO t_Transactions       
   (      
    OurBranchID,ScrollNo,SerialNo,RefNo,wDate,AccountType,DocumentType,AccountID,AccountName,ProductID,CurrencyID,ValueDate,TrxType,ChequeID,ChequeDate,Amount,    
    ForeignAmount,ExchangeRate,ProfitLoss,MeanRate,DescriptionID,[Description],BankCode,BranchCode,TrxPrinted,GLID,[Status],IsLocalCurrency,OperatorID,    
    SupervisorID,AdditionalData,IsMainTrx,DocType,GLVoucherID,Remarks,MerchantType,STAN,SettlmntAmount,ConvRate,AcqCountryCode,CurrCodeTran,CurrCodeSett,ATMStatus,    
    POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,CardAccptID,CardAccptNameLoc,VISATrxID,ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,ForwardInstID,ClearingRefNo,TClientId,TAccountid,TProductId,VatReferenceID
   )      
   VALUES       
   (      
    @OurBranchID,@mScrollNo,@mSerialNo,@OrgTrxRefNo+'|CHGS2',@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@mAccountType,'ATC',@AccountID,@mAccountName,@mProductID,@mCurrencyID,@ValueDate,    
    @mTrxType,'',@mwDate,@ExciseDutyAmount,@mForeignAmount,@mExchangeRate,0,@mExchangeRate,@mDescriptionIDChg2,@mDescriptionCharges2,@mBankCode,    
    @WithdrawlBranchID,0,@ExciseDutyAccountID,@Supervision,@mIsLocalCurrency,@mOperatorID,@mSupervisorID,null,'1','ATC',@VoucherID,@mRemarksChgs2,@MerchantType,@STAN,0,@ConvRate,    
    @AcqCountryCode,@CurrCodeTran,@CurrCodeSett,@Supervision,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@RetRefNum,@AuthIDResp,@CardAccptID,@NameLocation,    
    @VISATrxID,@ChannelId,@ChannelRefID,@ProcCode+'|CW CHG',@CAVV,@ResponseCode,@ForwardInstID,@RefNo,@ClientID,@AccountID,@mProductID,@VatReferenceID
   )      
   
   SET @GLSerialID = @GLSerialID+1 
   
   --set @mScrollNo = 0 for @GLControl before @mSerialNo
   
   INSERT INTO t_GLTransactions      
   (      
    OurBranchID,RefNo,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,ExchangeRate,IsCredit,TransactionType,    
    TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,IsMainTrx,DocType,Remarks,MerchantType,STAN,SettlmntAmount,ConvRate,    
    AcqCountryCode,CurrCodeTran,CurrCodeSett,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,CardAccptID,CardAccptNameLoc,VISATrxID,    
    ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,ForwardInstID,TClientId,TAccountid,TProductId
   )      
   VALUES      
   (      
    @OurBranchID,@OrgTrxRefNo+'|CHGS2',@GLControl,@VoucherID,@GLSerialID,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@ValueDate,@mDescriptionIDChg2,    
    @mDescriptionCharges2,@mCurrencyID,@ExciseDutyAmount,@mForeignAmount,@mExchangeRate,@IsCredit,'ATMCash','L',@mOperatorID,@mSupervisorID,'',    
    0,@mSerialNo,null,'0','ATC',@mRemarksChgs2,@MerchantType,@STAN,0,@ConvRate,@AcqCountryCode,@CurrCodeTran,@CurrCodeSett,@POSEntryMode,    
    @POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@RetRefNum,@AuthIDResp,@CardAccptID,@NameLocation,@VISATrxID,@ChannelId,@ChannelRefID,@ProcCode+'|CW CHG',@CAVV,@ResponseCode,@ForwardInstID,@ClientID,@AccountID,@mProductID
   )      
           
   IF @mTrxType = 'D'      
   BEGIN      
    SET @IsCredit = 1      
   END      
   ELSE      
   BEGIN      
    SET @IsCredit = 0      
   END      
   SET @GLSerialID = @GLSerialID+1          
   SELECT @gAccountID=AccountID, @gCurrencyID=CurrencyID FROM t_GL WHERE (OurBranchID = @OurBranchID) AND (AccountID =  @ExciseDutyAccountID)      
           
   INSERT INTO t_GLTransactions       
   (      
    OurBranchID,RefNo,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,ExchangeRate,IsCredit,TransactionType,    
    TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,IsMainTrx,DocType,Remarks,MerchantType,STAN,SettlmntAmount,ConvRate,    
    AcqCountryCode,CurrCodeTran,CurrCodeSett,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,CardAccptID,CardAccptNameLoc,VISATrxID,    
    ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,ForwardInstID,TClientId,TAccountid,TProductId,VatReferenceID
   )      
   VALUES      
   (      
    @OurBranchID,@OrgTrxRefNo+'|CHGS2',@gAccountID,@VoucherID,@GLSerialID,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@ValueDate,@mDescriptionIDChg2,    
    @mDescriptionCharges2,@gCurrencyID,@ExciseDutyAmount,@mForeignAmount,@mExchangeRate,@IsCredit,'ATMCash','L',@mOperatorID,@mSupervisorID,'',    
    @mScrollNo,@mSerialNo,null,'0','ATC',@mRemarksChgs2,@MerchantType,@STAN,0,@ConvRate,@AcqCountryCode,@CurrCodeTran,@CurrCodeSett,@POSEntryMode,    
    @POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@RetRefNum,@AuthIDResp,@CardAccptID,@NameLocation,@VISATrxID,@ChannelId,@ChannelRefID,@ProcCode+'|CW CHG',@CAVV,@ResponseCode,@ForwardInstID,@ClientID,@AccountID,@mProductID,@VatReferenceID
   )    
  END    
    
 END    
    
 /* Log the Updated Balance after transaction */    
    
 Insert Into BalChk     
 (    
  OurBranchID, AccountID, ProductID, [Name], LastUpdateTime, ClearBalance, Limit, Effects, Shadow, FreezeAmt, MinBalance, TotalBalance, AvailableBalance    
 )    
 SELECT OurBranchID, AccountID, ProductID, [Name], @mwDate + ' ' + convert(varchar(12), GetDate(), 114), ISNULL(ClearBalance,0), ISNULL(Limit,0),     
 ISNULL(Effects,0), ISNULL(ShadowBalance,0), ISNULL(FreezeAmount,0), @vMinBalance,     
 (isnull(@Limit,0)+ISNULL(ClearBalance,0)-ISNULL(Effects,0)),    
 (isnull(@Limit,0)+ISNULL(ClearBalance,0)-ISNULL(FreezeAmount,0)-@vMinBalance)    
 FROM t_AccountBalance WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID    
    
 SELECT @mAccountName=B.Name,@mProductID=B.ProductID,@mCurrencyID=P.CurrencyID,@mAccountType='C',@Limit=ISNULL(B.Limit,0),   
 @vClearBalance=ISNULL(B.ClearBalance,0),@vClearedEffects=ISNULL(B.Effects,0),@vFreezeAmount=ISNULL(B.FreezeAmount,0),    
 @vMinBalance=ISNULL(P.MinBalance,0), @vIsClose=ISNULL(A.Status,''),@vAreCheckBooksAllowed=P.AreCheckBooksAllowed,    
 @vAllowDebitTransaction=A.AllowDebitTransaction,@value=ISNULL(p.productatmcode,'00'), @CurrencyCode = ISNULL(c.CurrencyCode,'')    
 FROM t_AccountBalance B       
 INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND B.ProductID = A.ProductID         
 INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND A.ProductID = P.ProductID       
 INNER JOIN t_Currencies C ON A.OurBranchID = C.OurBranchID AND A.CurrencyID = C.CurrencyID    
 WHERE (A.OurBranchID = @OurBranchID) and (A.AccountID = @AccountID)          
     
 --exec @Limit = [fnc_GetLimit] @OurBranchID, @AccountID    
     
 IF @Limit > 0 AND @vCheckLimit = 1 AND @vODLimitAllow = 1    
 BEGIN    
  SET @vClearBalance=(@vClearBalance+@Limit)    
 END    
    
 SELECT @Status    = 0,       
 @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,      
 @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100      
     
 SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value, @CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp,
 @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
 RETURN 0;    
END