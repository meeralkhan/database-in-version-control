﻿CREATE PROCEDURE [dbo].[spc_UpdateBODDate]  
(  
  @OurBranchID varchar(4),  
  @WorkingDate datetime  
)  
AS  
  
SET NOCOUNT ON  
  
DECLARE @mDate as VarChar(50)  
  
SELECT @mDate = Convert(varchar(10),@WorkingDate,101) + ' ' + convert(varchar(20),GetDate(),114)  
  
if exists (select LastBOD from t_Last WHERE OurBranchID = @OurBranchID)  
begin  
  Update t_Last Set LastBOD = @mDate, WorkingDate = @WorkingDate WHERE OurBranchID = @OurBranchID  
  Update t_Last Set LastBOD = @mDate, WorkingDate = @WorkingDate WHERE OurBranchID = '99'
  SELECT 'Ok' AS RetStatus  
end  
else  
begin  
  SELECT 'Error' AS RetStatus  
end