﻿CREATE   procedure [dbo].[spc_GetAccountSweeps]  
(        
   @OurBranchID nvarchar(30),        
   @ClientID nvarchar(30),        
   @AccountID nvarchar(30)=''        
)        
AS        
  
declare @tempAccTable TABLE        
(        
  RowNo int NOT NULL,        
  AccountID nvarchar(30) NOT NULL        
);        
  
declare @tempTable TABLE        
(        
  OurBranchID nvarchar(30) NOT NULL,        
  AccountID nvarchar(30) NOT NULL,        
  SerialNo decimal(24,0) NOT NULL,        
  ReferenceID  nvarchar(450) NOT NULL,        
  [Type] char(1) NOT NULL,        
  [Action] char(1) NOT NULL,        
  SIAccountID nvarchar(30) NULL,        
  Amount money,        
  CreateBy nvarchar(30) NOT NULL,        
  CreateTime datetime NOT NULL,        
  CreateTerminal nvarchar(30) NOT NULL,        
  SIAccountID2 nvarchar(30)  ,      
  comments nvarchar(max),      
  ChargesTrxRef nvarchar(100)  
);        
  
if (@AccountID = '')        
begin        
   insert into @tempAccTable        
   select ROW_NUMBER() OVER(ORDER BY AccountID ASC) AS RowNo, AccountID from t_Account        
   where OurBranchID = @OurBranchID and ClientID = @ClientID and ProductID IN (select ProductID from v_Products where OurBranchID = @OurBranchID)        
end        
else        
begin        
   insert into @tempAccTable        
   select ROW_NUMBER() OVER(ORDER BY AccountID ASC) AS RowNo, AccountID from t_Account        
   where OurBranchID = @OurBranchID and AccountID = @AccountID and ClientID = @ClientID and ProductID IN (select ProductID from v_Products where OurBranchID = @OurBranchID)        
end        
  
DECLARE @RowCnt INT = 0, @i INT = 1;        
SELECT @RowCnt = COUNT(AccountID) FROM @tempAccTable;        
  
WHILE @i <= @RowCnt        
begin        
           
   select @AccountID = AccountID from @tempAccTable where RowNo = @i;        
     
   insert into @tempTable        
   select top 1 * from t_AccountSweeps where OurBranchID = @OurBranchID and AccountID = @AccountID and Type = 'R' order by SerialNo desc        
           
   insert into @tempTable        
   select top 1 * from t_AccountSweeps where OurBranchID = @OurBranchID and AccountID = @AccountID and Type = 'S' order by SerialNo desc        
           
   insert into @tempTable        
   select top 1 * from t_AccountSweeps where OurBranchID = @OurBranchID and AccountID = @AccountID and Type = 'E' order by SerialNo desc        
     
   SET @i = @i + 1;        
end        
  
select * from @tempTable