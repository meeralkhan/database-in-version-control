﻿CREATE PROCEDURE [dbo].[sp_ATM_ValidateAccount]
	(
	@OurBranchID 	varchar(30),
	@AccountID 	varchar(30)
	)
 	
AS

	SET NOCOUNT ON
	DECLARE @Status as int	


	DECLARE @mLastEOD as datetime
	DECLARE @mwDate as datetime
	DECLARE @mCONFRETI as datetime
	DECLARE @vCount1 as bit
	DECLARE @mAccountName as varchar(100)
	DECLARE @mCurrencyID as varchar(30)
	DECLARE @vIsClose as char
	DECLARE @vAllowCreditTransaction as bit
	DECLARE @vAreCheckBooksAllowed as bit
	DECLARE @LocalCurrencyID as varchar(30)
	DECLARE @LocalCurrency as varchar(30)

	SELECT @LocalCurrencyID=LocalCurrencyID,@LocalCurrency=LocalCurrency FROM t_ATM_GlobalVariables

	SELECT @mLastEOD=LastEOD,@mwDate=WorkingDate,@mCONFRETI=CONFRETI FROM t_Last WHERE (OurBranchID = @OurBranchID)
	IF @mLastEOD = @mwDate or @mCONFRETI = '1/1/9999'
		BEGIN
			 
			SELECT @Status=9		--INVALID_DATE
			SELECT @Status  as Status
			RETURN (1)	
		END
	
	SELECT @vCount1=count(*) From t_AccountBalance
	WHERE (OurBranchID = @OurBranchID) and (AccountID=@AccountID)
	IF @vCount1=0
		BEGIN
			 
			SELECT @Status=2	--INVALID_ACCOUNT
			SELECT @Status  as Status
			RETURN (1)	
		END
	
						
	SELECT @mAccountName=B.Name,@mCurrencyID=P.CurrencyID,@vIsClose=isnull(A.Status,''),@vAreCheckBooksAllowed=P.AreCheckBooksAllowed,@vAllowCreditTransaction=isnull(S.AllowCreditTransaction,0)
	FROM t_AccountBalance B INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND B.ProductID = A.ProductID INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND A.ProductID = P.ProductID LEFT OUTER JOIN t_AccountSpecialCondition S ON B.OurBranchID = S.OurBranchID AND B.AccountID = S.AccountID
	WHERE (A.OurBranchID = @OurBranchID) and (A.AccountID = @AccountID)
	
	IF @vIsClose = 'C' or @vIsClose = 'T' or @vIsClose = 'X' or @vAllowCreditTransaction = 1
		BEGIN
			 
			SELECT @Status=3		--INACTIVE_ACCOUNT
			SELECT @Status  as Status
	
	
			RETURN (1)	
		END
	IF @vAreCheckBooksAllowed = 0
		BEGIN
			 
			SELECT @Status=3		--INVALID_PRODUCT
			SELECT @Status  as Status
			RETURN (1)	
		END
	IF @mCurrencyID <> @LocalCurrency
		BEGIN
			 
			SELECT @Status=3		--INVALID_CURRENCY
			SELECT @Status  as Status
			RETURN (1)	
		END
	
		SELECT @Status=@@ERROR

		IF @Status <> 0 
			BEGIN
				 
				RAISERROR('There Is An Error Occured While Balance Enquiry In The Local Branch',16,1) WITH SETERROR
				RETURN (5000)
			END
			
			SELECT @Status = 0 
			SELECT @Status As Status,@mAccountName As AccountName
			RETURN 0