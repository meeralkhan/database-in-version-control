﻿
CREATE PROCEDURE [dbo].[sp_ATM_GetBalDownLoad]
 @OurBranchID varchar(30)   
  
 AS  
declare @mLastEOD as datetime
declare @mwDate as datetime

SELECT @mLastEOD=convert(varchar(10), LastEOD, 101), @mwDate=convert(varchar(10), WorkingDate, 101)
FROM t_Last 


IF @mLastEOD = @mwDate
BEGIN  
	SELECT	'' as Status, t_Account.OurBranchID, AccountNumber=t_Account.AccountID, 
			AccountType=t_Products.ProductID, @mwDate as WorkingDate, CurrencyCode =t_Products.CurrencyAtmCode,
			AvailableBalance=(t_AccountBalance.ClearBalance - t_AccountBalance.FreezeAmount - t_Account.MinBalance + t_AccountBalance.limit ),  
			ActualBalance =(t_AccountBalance.ClearBalance ), 
			AccountStatus=case when isnull(t_Account.Status, '') = '' then 'A' else t_Account.Status end,
			t_AccountBalance.Freezeamount,t_AccountBalance.Limit   
	  
	 FROM t_Account INNER JOIN  
		 t_AccountBalance ON   
		 t_Account.OurBranchID = t_AccountBalance.OurBranchID AND   
		 t_Account.AccountID = t_AccountBalance.AccountID  
		 LEFT OUTER JOIN t_Products ON   
		  t_Account.OurBranchID = t_Products.OurBranchID AND   
		  t_Account.ProductID = t_Products.ProductID  
		 LEFT OUTER JOIN t_Currencies ON   
		  t_Products.OurBranchID = t_Currencies.OurBranchID AND   
		  t_Products.CurrencyID = t_Currencies.CurrencyID  
		 LEFT OUTER JOIN t_AccountSpecialCondition S ON  
		  t_Account.OurBranchID = S.OurBranchID AND  
		  t_Account.AccountID = S.AccountID   
	 WHERE t_Account.OurBranchID = @OurBranchID  AND  
	  t_Products.ProductAtmCode <> '' AND   
			 (isnull(s.AllowDebitTransaction,0) = 0 or isnull(s.AllowCreditTransaction,0) = 0) and  
	  (isnull(t_Products.AreCheckBooksAllowed,1)=1)  
	  
END
else
begin
	Select '0' as Status
	Return 0  
end