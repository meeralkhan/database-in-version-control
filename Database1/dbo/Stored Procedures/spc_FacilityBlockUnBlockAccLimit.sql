﻿
CREATE   PROCEDURE [dbo].[spc_FacilityBlockUnBlockAccLimit]
(
   @OurBranchID nvarchar(30),
   @ClientID nvarchar(30),
   @FacilityID decimal(24,0),
   @IsBlock char(1)
)
AS

SET NOCOUNT ON

BEGIN
   
   declare @tmpTable table (
      SNo decimal(24,0) IDENTITY(1,1),
	  OurBranchID nvarchar(max),
	  ClientID nvarchar(max),
	  FacilityID decimal(24,0),
	  ProductID nvarchar(max),
	  AccountID nvarchar(max),
	  CurrencyID nvarchar(max),
	  FacilityRefNo nvarchar(max),
	  AccountLimitID nvarchar(max),
	  LimitSecType nvarchar(max),
	  GroupID nvarchar(max),
	  FacilityStatus nvarchar(max),
	  FacilityProductStatus nvarchar(max),
	  AccountStatus nvarchar(max),
	  FacilityLimit money,
	  FacilityProductLimit money,
	  AccountLimit money,
	  Limit money,
	  Limit1 money
   );

   insert into @tmpTable
   SELECT * FROM v_FacilityProductAccounts WHERE OurBranchID = @OurBranchID and ClientID = @ClientID and FacilityID = @FacilityID

   declare @i int=1, @total int=0
   select @total = COUNT(OurBranchID) from @tmpTable
   declare @BID nvarchar(max), @AID nvarchar(max), @Limit money

   IF (@IsBlock = 'Y')
   BEGIN
      
	  while (@i <= @total)
	  begin
	     select @BID=OurBranchID, @AID=AccountID, @Limit=Limit from @tmpTable where SNo = @i;
		 update t_AccountBalance set Limit=0 where OurBranchID = @BID and AccountID = @AID;
		 set @i = @i+1;
	  end

   END
   ELSE
   BEGIN
      
	  while (@i <= @total)
	  begin
	     select @BID=OurBranchID, @AID=AccountID, @Limit=Limit from @tmpTable where SNo = @i;
		 update t_AccountBalance set Limit=@Limit where OurBranchID = @BID and AccountID = @AID;
		 set @i = @i+1;
	  end
	  
   END

   SELECT 'Ok' AS RetStatus

END