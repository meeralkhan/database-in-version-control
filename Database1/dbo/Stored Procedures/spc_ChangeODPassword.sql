﻿CREATE PROCEDURE [dbo].[spc_ChangeODPassword]    
(                  
  @OurBranchID varchar(30),                  
  @Username varchar(30),                  
  @OPassword varchar(max),                  
  @NPassword varchar(max),                  
  @CPassword varchar(max),            
  @WorkingDate datetime,      
  @UserTerminal varchar(30)            
)                  
AS                    
        
set nocount on             
             
BEGIN TRY                  
                   
 declare @OldPassword varchar(255)                  
                   
 if @NPassword != @CPassword                  
 begin                  
 select 'Error' AS ReturnStatus, 'PasswordDoesNotMatch' AS ReturnMessage;                  
 end                  
 else IF (select COUNT(*) FROM t_Map where OurBranchID = @OurBranchID AND OperatorID = @Username AND ISNULL(CannotChgPwd,0) = 0) > 0
 begin              
                   
 select @OldPassword = RemotePassword FROM t_Map where OurBranchID = @OurBranchID AND OperatorID = @Username;                  
                   
 if @OldPassword != @OPassword                  
 begin                  
  select 'Error' AS ReturnStatus, 'IncorrectOldPassword' AS ReturnMessage;                  
 end                  
 else                  
 begin                  
   
   UPDATE t_Map SET RemotePassword = @NPassword WHERE OurBranchID = @OurBranchID AND OperatorID = @Username;
   UPDATE t_MapDrive SET [Password] = @NPassword WHERE OurBranchID = @OurBranchID AND OperatorID = @Username
   AND FormToRun IN ('frmCashRemote', 'frmCashRemoteAccess', 'frmRemoteAccess', 'frmTransferRemote', 'frmInwardSuper');
   
   select 'Ok' AS ReturnStatus, 'PasswordChanged' AS ReturnMessage;
   
 end                  
 end                  
 else                  
 begin                  
 select 'Error' AS ReturnStatus, 'UnSufficientRights' AS ReturnMessage;                  
 end                  
                   
END TRY                  
BEGIN CATCH                  
 select 'Error' AS ReturnStatus, ERROR_NUMBER() AS ReturnErrorNo, ERROR_MESSAGE() AS ReturnMessage;                  
END CATCH