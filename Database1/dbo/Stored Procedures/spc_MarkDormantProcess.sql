﻿create PROCEDURE [dbo].[spc_MarkDormantProcess]    
(                 
 @OurBranchID as varchar(30),              
 @ProductID as varchar(30),              
 @DateFrom as datetime,              
 @Dateto  as datetime,              
 @OperatorID as Varchar(30),              
 @ComputerName as Varchar(50)              
)              
AS              
BEGIN              
              
SET NOCOUNT ON              
                 
   DECLARE @mDate as VarChar(50)                
   DECLARE @Count as int          
   DECLARE @RetStat as int            
   SET @RetStat = 0      
   SET @Count = 0                
       
 SELECT @Count = count(*) FROM t_Account a    
 INNER JOIN t_Products p ON a.OurBranchID = p.OurBranchID AND a.ProductID = p.ProductID    
 WHERE a.OurBranchID = @OurBranchID AND a.ProductID = @ProductID AND a.OpenDate <= @DateFrom AND p.DormantDays > '0'      
 AND ((ISNULL(p.IsAdvancesProduct,'No') = 'No' AND ISNULL(p.IsTermDeposit,'No') = 'No') OR (p.ProductType = 'AE'))     
 AND p.ProductType NOT IN ('A','G')     
 AND a.AccountID Not In ( Select AccountID     
        From t_Transactions     
        Where t_Transactions.OurBranchID = @OurBranchID    
        And convert(varchar(10), t_Transactions.wdate, 120) >= @DateFrom    
        And convert(varchar(10), t_Transactions.wdate, 120) <= @Dateto    
        And t_Transactions.DescriptionID Not In(select DescriptionID from t_TransactionDescriptions where OurBranchID = a.OurBranchID and ISNULL(System,0)=1)     
        AND IsNull(t_Transactions.Status,'') <> 'R')       
        AND IsNull(a.Status,'') Not IN ('C','T','X','D','I')       
    
   SELECT @mDate = Convert(varchar(10),@Dateto ,120) + ' ' + convert(varchar(20),GetDate(),114)      
    
   IF @Count > 0      
   BEGIN    
 Insert INTO t_DormantHistory              
    (OurBranchID, wDate, ProductID, AccountID, AccountName, ClearBalance, BeforMarkStatus, TerminalID, OperatorID)              
                  
    SELECT @OurBranchID, @mDate, a.ProductID, a.AccountID, a.[Name], ab.ClearBalance, IsNull(a.[Status],''), @ComputerName, @OperatorID    
 FROM t_Account a    
    INNER JOIN t_AccountBalance ab On a.OurBranchID = ab.OurBranchID AND a.AccountID = ab.AccountID     
 AND a.ProductID = @ProductID And (a.CloseDate Is Null or a.CloseDate = '1/1/1900')     
    INNER JOIN t_Products p On a.OurBranchID = p.OurBranchID And a.ProductId = p.ProductID      
 WHERE a.OurBranchID = @OurBranchID AND a.ProductID = @ProductID AND a.OpenDate <= @DateFrom AND p.DormantDays > '0'      
 AND ((ISNULL(p.IsAdvancesProduct,'No') = 'No' AND ISNULL(p.IsTermDeposit,'No') = 'No') OR (p.ProductType = 'AE'))     
 AND p.ProductType NOT IN ('A','G')     
 And a.AccountID Not In ( Select AccountID              
        From t_Transactions              
                                Where t_Transactions.OurBranchID = @OurBranchID              
                                And Convert(varchar(10),t_Transactions.wdate,120) Between @DateFrom And @DateTo              
                                And t_Transactions.DescriptionID Not In(select DescriptionID from t_TransactionDescriptions where OurBranchID = a.OurBranchID and ISNULL(System,0)=1)     
        AND IsNull(t_Transactions.Status,'') <> 'R'    
       )     
 AND IsNull(a.Status,'') Not IN ('C','T','X','D','I')    
    
 Update a     
 Set [Status] = 'T'    
 FROM t_Account a    
 INNER JOIN t_Products p ON a.OurBranchID = p.OurBranchID AND a.ProductID = p.ProductID    
 WHERE a.OurBranchID = @OurBranchID AND a.ProductID = @ProductID AND a.OpenDate <= @DateFrom AND p.DormantDays > '0'     
 AND ((ISNULL(p.IsAdvancesProduct,'No') = 'No' AND ISNULL(p.IsTermDeposit,'No') = 'No')     
 OR (p.ProductType = 'AE'))     
 AND p.ProductType NOT IN ('A','G')     
 AND a.AccountID Not IN ( Select AccountID                 
        From t_Transactions                 
        Where t_Transactions.OurBranchID = @OurBranchID    
        And Convert(varchar(10),t_Transactions.wdate,120) Between @DateFrom And @Dateto    
        And t_Transactions.DescriptionID Not In(select DescriptionID from t_TransactionDescriptions where OurBranchID = a.OurBranchID and ISNULL(System,0)=1)    
       )     
 AND IsNull(a.Status,'') Not IN ('C','T','X','D','I')      
      
 SET @RetStat = 1      
       
   END        
   ELSE      
   BEGIN      
      
SET @RetStat = 0      
       
   END      
      
   IF @RetStat = 1      
   BEGIN      
      
    SELECT @RetStat as RetStat, wDate, ProductID, AccountID FROM t_DormantHistory       
    where OurBranchID = @OurBranchID AND ProductID = @ProductID AND CONVERT(varchar(10), wDate, 120) = @Dateto      
    ORDER BY ProductID      
      
   END      
   ELSE      
   BEGIN      
      
    SELECT @RetStat as RetStat      
      
   END      
 END