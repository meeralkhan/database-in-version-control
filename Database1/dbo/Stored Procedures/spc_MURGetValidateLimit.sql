﻿CREATE PROCEDURE [dbo].[spc_MURGetValidateLimit]  
(  
 @OurBranchID varchar(30),  
 @AccountID varchar(30),  
 @ProductID  varchar(30)='',  
 @Amount  money,  
 @chkOtherAccount int = 1            
)  
AS  
  
SET NOCOUNT ON  
  
BEGIN TRY  
   
 Declare @RetValueMsg varchar(1250)  
 Declare @MeanRate money  
 Declare @CurrencyID   varchar(30)  
 Declare @IsAdvancesProduct   varchar(30)  
 Declare @FinanceProductType   varchar(30)  
 Declare @LocalCurrencyID   varchar(30)  
 Declare @ProductType varchar(3)  
 Declare @Work_Date datetime  
 Declare @mClearBalance money  
 Declare @mLimit money  
 --Declare @Limit money  
 Declare @ProductMinBalance money  
           
 declare @RemAmount money          
 declare @LinkedAccount nvarchar(30)          
 declare @LinkedAccAmount money          
           
 set @LinkedAccAmount = 0          
           
 SELECT @MeanRate = c.MeanRate, @CurrencyID=c.CurrencyID,  
 @IsAdvancesProduct = ISNULL(p.IsAdvancesProduct,'No'),  
 @FinanceProductType = ISNULL(p.FinanceProductType,''),  
 @ProductMinBalance = ISNULL(p.MinBalance,0)  
 FROM t_Products p  
 INNER JOIN t_Currencies c ON p.OurBranchID = c.OurBranchID AND p.CurrencyID = c.CurrencyID  
 INNER JOIN t_Account a ON p.OurBranchID = a.OurBranchID AND p.ProductID = a.ProductID  
 where p.OurBranchID=@OurBranchID AND a.AccountID=@AccountID  
   
 SELECT @LocalCurrencyID=LocalCurrency  
 FROM t_GlobalVariables  
 where OurBranchID = @OurBranchID  
   
 Select @Work_Date=WORKINGDATE from t_Last  
 Where OurBranchID=@OurBranchID  
   
 if @LocalCurrencyID=@CurrencyID  
 BEGIN  
  Set @MeanRate=1  
 END  
   
 if Exists(Select a.AccountID from t_Account a INNER JOIN t_AccountBalance ab ON a.OurBranchID = ab.OurBranchID AND a.AccountID = ab.AccountID where a.OurBranchID=@OurBranchID And a.AccountID=@AccountID)  
 BEGIN  
        
  --exec @Limit = fnc_GetLimit @OurBranchID, @AccountID      
    
  Select @mClearBalance=ClearBalance+Limit-FreezeAmount-@ProductMinBalance, @mLimit=Limit  
  from t_AccountBalance  
  Where OurBranchID=@OurBranchID And AccountID=@AccountID  
    
 END  
 ELSE  
 BEGIN  
    
  Set @RetValueMsg='InvalidCustomer'  
  Select @RetValueMsg RetValueMsg, '0' ClearBalance   
  return 0;  
    
 END  
   
 IF @IsAdvancesProduct = 'No'  
 BEGIN  
    
  If @mClearBalance - @Amount < 0  
  Begin  
     
   Set @RetValueMsg='TOD'  
             
   /* Account Special Conditions - Transfer Funds from Other Account */          
             
   set @RemAmount = @mClearBalance - @Amount          
             
   if @chkOtherAccount = 1          
   begin          
               
      if exists (select InSufficientFundsAccountID from t_Account where OurBranchID = @OurBranchID and           
         AccountID = @AccountID and ISNULL(LackOfFunds,0) = 1)          
      begin          
    
        select @LinkedAccount = InSufficientFundsAccountID from t_Account          
        where OurBranchID = @OurBranchID and AccountID = @AccountID and ISNULL(LackOfFunds,0) = 1          
    
        exec @LinkedAccAmount = fn_MURGetValidateLimit @OurBranchID, @LinkedAccount, @CurrencyID, @MeanRate, @LocalCurrencyID          
    
        set @RemAmount = @RemAmount + round(@LinkedAccAmount,2)          
    
      end          
  
      If @RemAmount < 0          
      begin          
    
        if exists (select InSufficientFundsAccountID2 from t_Account where OurBranchID = @OurBranchID and           
           AccountID = @AccountID and ISNULL(LackOfFunds,0) = 1)          
        begin          
      
          select @LinkedAccount = InSufficientFundsAccountID2 from t_Account          
          where OurBranchID = @OurBranchID and AccountID = @AccountID and ISNULL(LackOfFunds,0) = 1          
      
          exec @LinkedAccAmount = fn_MURGetValidateLimit @OurBranchID, @LinkedAccount, @CurrencyID, @MeanRate, @LocalCurrencyID          
      
          set @RemAmount = @RemAmount + round(@LinkedAccAmount,2)          
      
        end          
    
      end          
   end          
             
   If @RemAmount >= 0          
   begin          
     Set @RetValueMsg='OK'          
end          
             
   Select @RetValueMsg RetValueMsg, @mClearBalance ClearBalance  
   return 0;  
  End  
  Else  
  Begin  
   Set @RetValueMsg='OK'  
   Select @RetValueMsg RetValueMsg, @mClearBalance ClearBalance  
   return 0;  
  End  
 END  
 ELSE   
 BEGIN  
    
  If Not Exists( select a.AccountID from t_Adv_Account_LimitMaintain a inner join t_Account b on a.OurBranchID=b.OurBranchID AND a.AccountID = b.AccountID where a.OurBranchID = @OurBranchID AND a.AccountID=@AccountID AND isnull(AuthStatus2,'') = '')      
  
    
      
        
          
  BEGIN  
     
   If @mClearBalance - @Amount < 0  
   Begin  
      
    Set @RetValueMsg='TOD'  
              
    set @RemAmount = @mClearBalance - @Amount          
            
    if @chkOtherAccount = 1          
    begin          
               
       if exists (select InSufficientFundsAccountID from t_Account where OurBranchID = @OurBranchID and           
          AccountID = @AccountID and ISNULL(LackOfFunds,0) = 1)          
       begin          
     
         select @LinkedAccount = InSufficientFundsAccountID from t_Account          
         where OurBranchID = @OurBranchID and AccountID = @AccountID and ISNULL(LackOfFunds,0) = 1          
   
         exec @LinkedAccAmount = fn_MURGetValidateLimit @OurBranchID, @LinkedAccount, @CurrencyID, @MeanRate, @LocalCurrencyID          
   
         set @RemAmount = @RemAmount + round(@LinkedAccAmount,2)          
     
       end          
               
       If @RemAmount < 0          
       begin          
   
         if exists (select InSufficientFundsAccountID2 from t_Account where OurBranchID = @OurBranchID and           
            AccountID = @AccountID and ISNULL(LackOfFunds,0) = 1)          
         begin          
       
           select @LinkedAccount = InSufficientFundsAccountID2 from t_Account          
           where OurBranchID = @OurBranchID and AccountID = @AccountID and ISNULL(LackOfFunds,0) = 1          
     
           exec @LinkedAccAmount = fn_MURGetValidateLimit @OurBranchID, @LinkedAccount, @CurrencyID, @MeanRate, @LocalCurrencyID          
     
           set @RemAmount = @RemAmount + round(@LinkedAccAmount,2)          
       
         end          
     
       end          
    end          
            
    If @RemAmount >= 0          
    begin          
      Set @RetValueMsg='OK'          
    end          
              
    Select @RetValueMsg RetValueMsg, @mClearBalance ClearBalance  
    return 0;  
      
   End  
   Else  
   Begin  
      
    Set @RetValueMsg='OK'  
    Select @RetValueMsg RetValueMsg, @mClearBalance ClearBalance  
    return 0;  
      
   End  
  END  
  ELSE  
  BEGIN  
     
   If Exists( select a.AccountID from t_Adv_Account_LimitMaintain a inner join t_Account b on a.OurBranchID=b.OurBranchID AND a.AccountID = b.AccountID  
   where a.OurBranchID = @OurBranchID AND a.AccountID = @AccountID AND a.AuthStatus <> '' AND isnull(AUthStatus2,'') = '')  
   BEGIN  
      
    Set @RetValueMsg = 'Sorry, Account Limit Under Supervision.'  
    Select @RetValueMsg RetValueMsg, '0' ClearBalance  
    return 0;  
      
END  
   ELSE If Exists( select a.AccountID from t_Adv_Account_LimitMaintain a inner join t_Account b on a.OurBranchID=b.OurBranchID AND a.AccountID = b.AccountID  
   where a.OurBranchID=@OurBranchID AND a.AccountID=@AccountID AND ExpDate <= @Work_Date AND isnull(AuthStatus2,'') = '')  
   BEGIN  
      
    Set @RetValueMsg = 'Sorry, Account Limit Expired.'  
    Select @RetValueMsg RetValueMsg, '0' ClearBalance  
    return 0;  
      
   END             ELSE  
   BEGIN  
      
    Set @RetValueMsg='OK'  
    Select @RetValueMsg RetValueMsg, @mClearBalance ClearBalance  
    return 0;  
      
   END  
  END  
 END  
   
 Set @RetValueMsg='InvalidMessage'  
 Select @RetValueMsg RetValueMsg, '0' ClearBalance  
 return 0;  
   
END TRY  
BEGIN CATCH  
   
 Set @RetValueMsg=Error_MESSAGE()  
 Select @RetValueMsg RetValueMsg, '0' ClearBalance  
 return 0;  
   
END CATCH