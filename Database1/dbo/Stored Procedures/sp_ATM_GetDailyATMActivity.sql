﻿CREATE PROCEDURE [dbo].[sp_ATM_GetDailyATMActivity]
(
	@OurBranchID	varchar(30),
	@ATMID	varchar(30),
	@wDate	datetime
)
AS
	DECLARE @CustomerBranchID as varchar(30)

	BEGIN
		SELECT t_ATM_TransactionHistory.RefNo, 
		    t_ATM_TransactionHistory.ScrollNo, 
		    t_ATM_TransactionHistory.Supervision, 
		    t_ATM_TransactionHistory.wDate AS wDate, 
		    t_ATM_TransactionHistory.PHXDate, 
		    t_ATM_TransactionHistory.GLID, 
		    t_ATM_TransactionHistory.RefNo AS Expr1, 
		    SUBSTRING(t_ATM_TransactionHistory.RefNo, 17, 2) 
		    + '/' + SUBSTRING(t_ATM_TransactionHistory.RefNo, 19, 2) 
		    + '/' + CONVERT(varchar(4), 
		    { fn YEAR(t_ATM_TransactionHistory.PHXDate) }) AS ATMDate, 
		    SUBSTRING(t_ATM_TransactionHistory.RefNo, 21, 2) 
		    + ':' + SUBSTRING(t_ATM_TransactionHistory.RefNo, 23, 2) 
		    + ':' + SUBSTRING(t_ATM_TransactionHistory.RefNo, 25, 2) 
		    AS ATMTime, t_ATM_TransactionHistory.AccountID, 
		    t_ATM_TransactionHistory.AccountName AS AccountTitle, 
		    t_ATM_TransactionHistory.Amount, 
		    t_ATM_TransactionHistory.CustomerAccountID AS InterBrAc, 
		    t_ATM_Branches.BranchName AS 'CustomerBranchName'
		FROM t_ATM_TransactionHistory INNER JOIN
		    t_ATM_Branches ON 
		    t_ATM_TransactionHistory.CustomerBranchID = t_ATM_Branches.BranchID
		WHERE (t_ATM_TransactionHistory.OurBranchID = @OurBranchID) and (t_ATM_TransactionHistory.ATMID = @ATMID) and (t_ATM_TransactionHistory.wDate = @wDate)
		ORDER BY ScrollNo
	END