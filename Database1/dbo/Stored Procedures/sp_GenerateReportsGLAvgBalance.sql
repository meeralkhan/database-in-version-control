﻿
CREATE   PROCEDURE [dbo].[sp_GenerateReportsGLAvgBalance]
(
@sdate Varchar(50),
@edate Varchar(50),
@TName Varchar(100)				 
)

AS
BEGIN
	Set nocount on

	DECLARE @OurBranchID VARCHAR(30)	  DECLARE @cCount as decimal(24,0)     declare @sCount as decimal(24,0)		DECLARE @Counter as INT		DECLARE @LocalBalance AS MONEY
	DECLARE @ssDate datetime 	DECLARE @eeDate datetime		DECLARE @ForeignBalance AS MONEY	DECLARE @AccountID VARCHAR(30)	DECLARE @Description AS VARCHAR(100)	
    DECLARE @Flag AS BIT		declare @TabName Varchar(100)	DECLARE @SQL NVARCHAR(MAX)
	SET @TabName = @TName

	SET @SQL = 'Drop Table ' + @TabName + ' ';
	if object_id(@TabName) IS NOT NULL BEGIN EXEC sp_executesql @SQL END
	
	SET @ssDate = convert(varchar(10), @sdate, 120)
	SET @eeDate = convert(varchar(10), @edate, 120)


	SET @cCount=0     SET @sCount=0     
	SELECT @cCount=DATEDIFF(DAY,@ssDate,DATEADD(d,1,@eeDate))
      
	WHILE (@ssDate <= @eeDate)         
	BEGIN             
		if object_id(@TabName) IS NULL 
		BEGIN 
			SET @SQL = 'SELECT a.OurBranchID, a.Accountid, g.Description, g.CurrencyID, SUM(g.openingbalance + isnull( (Case g.CurrencyID When ''AED'' Then a.LCYAmount Else a.LCYAmount End),0)) LCYClosingBal,'
			SET @SQL = @SQL + 'SUM(g.ForeignOpeningBalance +  isnull( (Case g.CurrencyID When ''AED'' Then 0 Else  a.FCYAmount End),0)) as FCYClosingBal,'
			SET @SQL = @SQL + 'CASE WHEN g.AccountType = ''A'' AND a.AccountiD LIKE ''5%'' THEN ''MEMORANDUM ASSETS'' WHEN g.AccountType = ''L'' AND a.AccountiD LIKE ''6%'' THEN ''MEMORANDUM LIABILITIES'' '
			SET @SQL = @SQL + 'WHEN g.AccountType = ''A'' THEN ''ASSETS'' WHEN g.AccountType = ''E'' THEN ''EXPENDITURE'' WHEN g.AccountType = ''I'' THEN ''INCOME'' WHEN g.AccountType = ''L'' THEN ''LIABILITIES'' '
			SET @SQL = @SQL + 'ELSE ''P'' END AS AccountType, ''' + convert(varchar(10), @ssdate, 120) + ''' Date INTO ' + @TabName + ' '
			SET @SQL = @SQL + 'FROM (select OurBranchID,Accountid, sum(Case IsCredit When 0 Then -1 Else 1 End * Amount) LCYAmount, Sum(Case IsCredit When 0 Then -1 Else 1 End * ForeignAmount) FCYAmount  '
			SET @SQL = @SQL + 'from t_GLTransactions where valuedate <= ''' + convert(varchar(10), @ssdate, 120) + ''' AND t_GLTransactions.Status<>''R'' Group By OurBranchID,Accountid) as a '
			SET @SQL = @SQL + 'inner join t_GL as g on a.accountid=g.AccountID AND a.OurBranchId = g.OurBranchID GROUP BY a.OurBranchID, a.Accountid,g.Description, g.CurrencyID, g.AccountType '
			SET @SQL = @SQL + 'ORDER BY a.Accountid, a.OurBranchID;'
			EXEC sp_executesql @SQL
		END
		ELSE	
		BEGIN
			SET @SQL = 'INSERT INTO ' + @TabName + ' ' 
			SET @SQL = @SQL + 'SELECT a.OurBranchID, a.Accountid, g.Description, g.CurrencyID, SUM(g.openingbalance + isnull( (Case g.CurrencyID When ''AED'' Then a.LCYAmount Else a.LCYAmount End),0)) LCYClosingBal,'
			SET @SQL = @SQL + 'SUM(g.ForeignOpeningBalance +  isnull( (Case g.CurrencyID When ''AED'' Then 0 Else  a.FCYAmount End),0)) as FCYClosingBal,'
			SET @SQL = @SQL + 'CASE WHEN g.AccountType = ''A'' AND a.AccountiD LIKE ''5%'' THEN ''MEMORANDUM ASSETS'' WHEN g.AccountType = ''L'' AND a.AccountiD LIKE ''6%'' THEN ''MEMORANDUM LIABILITIES'' '
			SET @SQL = @SQL + 'WHEN g.AccountType = ''A'' THEN ''ASSETS'' WHEN g.AccountType = ''E'' THEN ''EXPENDITURE'' WHEN g.AccountType = ''I'' THEN ''INCOME'' WHEN g.AccountType = ''L'' THEN ''LIABILITIES'' '
			SET @SQL = @SQL + 'ELSE ''P'' END AS AccountType, ''' + convert(varchar(10), @ssdate, 120) + ''' Date '   
			SET @SQL = @SQL + 'FROM (select OurBranchID,Accountid, sum(Case IsCredit When 0 Then -1 Else 1 End * Amount) LCYAmount, Sum(Case IsCredit When 0 Then -1 Else 1 End * ForeignAmount) FCYAmount  '
			SET @SQL = @SQL + 'from t_GLTransactions where valuedate <= ''' + convert(varchar(10), @ssdate, 120) + ''' AND t_GLTransactions.Status<>''R'' Group By OurBranchID,Accountid) as a '
			SET @SQL = @SQL + 'inner join t_GL as g on a.accountid=g.AccountID AND a.OurBranchId = g.OurBranchID GROUP BY a.OurBranchID, a.Accountid,g.Description, g.CurrencyID, g.AccountType '
			SET @SQL = @SQL + 'ORDER BY a.Accountid, a.OurBranchID;'
			EXEC sp_executesql @SQL		
		END

		SET @ssDate = DATEADD(d,1,@ssDate)  

	END 

	Select 1 as a

END