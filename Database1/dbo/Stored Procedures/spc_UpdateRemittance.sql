﻿CREATE PROCEDURE [dbo].[spc_UpdateRemittance]        
(        
 @OurBranchID varchar(4),        
 @ScrollNo numeric,        
 @DateOfIssue DateTime,        
 @BatchNo int,        
 @BatchInstrumentNo int,        
 @InstrumentType char(1),        
 @WorkingDate datetime=null,        
 @StatusFlag Char(1) = 'P',  -- P = Paid, C = Cancelled        
 @PaidMode varchar(2),        
 @Supervision char(1)='C',  
 @SupervisorID varchar(30)='',  
 @NewRecord bit=0        
)        
        
AS        
    
   set nocount on    
        
   DECLARE @RetStatus as int  
   DECLARE @mDate Datetime  
     
   SELECT @mDate = Convert(varchar(10),@WorkingDate,101) + ' ' + convert(varchar(20),GetDate(),114)        
     
 Update t_LR_Issuance        
    SET Paid = 1,        
        PaidDate = @WorkingDate,        
        PaidScrollNo = @ScrollNo,          
        PaidMode = @PaidMode,         
        Supervision = @Supervision,  
        SuperviseBy = @SupervisorID,  
        SuperviseTime = @mDate  
          
    WHERE (OurBranchID = @OurBranchID) and (convert(varchar(20), wDate,101) = @DateOfIssue) and   
    (Left(InstrumentType,1) = upper(@InstrumentType))        
    and (InstrumentNo = @BatchInstrumentNo) and  (SerialNo = @BatchNo)        
        
  /*      
 Update t_LR_Advice        
    SET Paid = 1,        
        PaidDate = @WorkingDate,        
        PaidScrollNo = @ScrollNo,          
        PaidMode = @PaidMode         
    WHERE (OurBranchID = @OurBranchID) and (wDate = @DateOfIssue) and (Left(InstrumentType,1) = upper(@InstrumentType))         
   and (InstrumentNo = @BatchInstrumentNo) and  (SerialNo = @BatchNo)        
        
 Update t_LR_Advice        
    SET Paid = 1,        
        PaidDate = @WorkingDate,        
        PaidScrollNo = @ScrollNo,          
        PaidMode = @PaidMode         
    WHERE (OurBranchID = @OurBranchID) and (wDate = @DateOfIssue) and (Left(InstrumentType,1) = upper(@InstrumentType))         
   and (ControlNo = @BatchInstrumentNo) and  (SerialNo = @BatchNo) and (InstrumentNo = 0)        
  */      
        
   SELECT @RetStatus = @@ERROR        
        
   IF @RetStatus <> 0        
      RETURN(52)        
   ELSE        
      RETURN(0)