﻿CREATE PROCEDURE [dbo].[sp_ATM_AddEditPurchaseRequestTransaction]    
(    
 @OurBranchID   varchar(30),     
 @WithdrawlBranchID  varchar(30)='',     
 @POSID           varchar(30),     
 @AccountID           varchar(30),    
 @Amount        money,    
 @USDAmount       money=0,     
 @OtherCurrencyAmount money=0,    
 @USDRate       money=0,    
 @Supervision   char(1),     
 @RefNo            varchar(50),    
 @PHXDate        datetime,    
 @MerchantType      varchar(30)='',    
 @AckInstIDCode      varchar(30)='',    
 @Currency       varchar(3),    
 @NameLocation      varchar(50),    
 @GasMerchant   varchar(2)='',    
 @ProcCode             varchar(30)='',      
 @SettlmntAmount      numeric(18,6)=0,    
 @ConvRate            decimal(15,9)=0,    
 @AcqCountryCode      varchar(3)='',    
 @CurrCodeTran        varchar(3)='',    
 @CurrCodeSett        varchar(3)='',    
 @ForwardInstID    varchar(10)='',    
 @MessageType    varchar(10)='',    
 @OrgTrxRefNo    varchar(100)='',    
    
 @POSEntryMode    varchar(3)='',    
 @POSConditionCode   varchar(2)='',    
 @POSPINCaptCode    varchar(2)='',    
 @RetRefNum     varchar(15)='',    
 @CardAccptID    varchar(15)='',    
 @VISATrxID     varchar(15)='',    
 @CAVV      char(1)='',    
 @ResponseCode    varchar(2)='',    
 @ExtendedData    varchar(2)='',    
    
 /* TO BE UPDATE LATER */    
    @ChannelId     nvarchar(30)='4',    
    @ChannelRefID    nvarchar(100)='',    
 @AuthIDResp varchar(6)='',  
 @NewRecord       bit=1    
)    
AS    
 SET NOCOUNT ON    
 DECLARE @mScrollNo as int    
 DECLARE @mSerialNo as int    
  SET @mSerialNo = 0    
 DECLARE @mAccountID as varchar(30)    
 DECLARE @mAccountName as varchar(50)    
 DECLARE @mAccountType as char(1)    
 DECLARE @mProductID as varchar(6)    
 DECLARE @mCurrencyID as varchar(3)    
 DECLARE @mIsLocalCurrency as bit    
  SET @mIsLocalCurrency=1     
 DECLARE @mwDate as datetime    
 DECLARE @mTrxType as char    
 DECLARE @mGLID as varchar(30)    
  SET @mGLID = NULL    
 DECLARE @mForeignAmount as money    
  SET @mForeignAmount=0     
 DECLARE @mExchangeRate as money    
  SET @mExchangeRate=1     
 DECLARE @mDescriptionID as varchar(30)    
  SET @mDescriptionID='987'     
 DECLARE @mDescription as varchar(500)    
 DECLARE @LocalCurrencyID as varchar(3)    
 DECLARE @LocalCurrency as varchar(3)    
 DECLARE @mDescriptionCharges as varchar(500)    
 DECLARE @mDescriptionH as varchar(500)    
 DECLARE @mDescriptionHCharges as varchar(500)    
 DECLARE @Value as varchar(2)      
  SET @value='00'      
 DECLARE @STAN as varchar(30)      
  SET @STAN = RIGHT(@RefNo,6)    
  DECLARE @mRemarks as varchar(500)    
 DECLARE @mBankCode as varchar(4)    
 DECLARE @mOperatorID as varchar(30)    
  SET @mOperatorID = 'POS-' + @POSID    
 DECLARE @mSupervisorID as varchar(30)    
  SET @mSupervisorID = 'N/A'    
    
  --Checking Variables    
 DECLARE @vCount1 as bit    
 DECLARE @vCount2 as bit    
 DECLARE @vCount3 as bit    
 DECLARE @vCount4 as bit    
 DECLARE @vAccountID as varchar(30)    
 DECLARE @vClearBalance as money    
 DECLARE @vClearedEffects as money    
 DECLARE @vFreezeAmount as money    
 DECLARE @vMinBalance as money    
 DECLARE @vIsClose as char    
 DECLARE @vAreCheckBooksAllowed as bit    
 DECLARE @vAllowDebitTransaction as bit    
 DECLARE @vAccountLimit as money    
 DECLARE @vExpiryDate as datetime    
 DECLARE @vCheckLimit as bit    
  SET @vCheckLimit = 0    
 DECLARE @vODLimitAllow as bit    
  SET @vODLimitAllow = 0    
 DECLARE @vBranchName as varchar(50)    
  SET @vBranchName = 'N/A'    
 DECLARE @Status as int     
 DECLARE @mLastEOD as datetime    
 DECLARE @mCONFRETI as datetime    
 DECLARE @WithDrawlCharges as money    
 DECLARE @WithDrawlChargesPercentage as money    
  SET @WithDrawlCharges = 0    
  SET @WithDrawlChargesPercentage = 0    
 DECLARE @IssuerChargesAccountID as varchar(30)    
 DECLARE @IssuerTitle as varchar(50)    
 DECLARE @mRemarksCharges as varchar(500)    
 DECLARE @VoucherID int      
 DECLARE @IsCredit int      
 DECLARE @GLControl nvarchar(30)      
 DECLARE @CurrencyCode as varchar(30)    
  SET @CurrencyCode = ''    
    
 DECLARE @AvailableBalance as money          
 DECLARE @HoldStatus as char(1)    
 DECLARE @HoldAmount as money    
 DECLARE @FreezeAmount as money    
 DECLARE @Super as varchar(1)          
 DECLARE @remAmount as numeric(18,6)    
  SET @remAmount=0
 DECLARE @mAmount as numeric(18,6)    
  SET @mAmount=0
 DECLARE @mRefNo as varchar(50)    
 DECLARE @mHoldStatus as char(1)    
 DECLARE @sCount as int    
  SET @sCount = 0    
 DECLARE @cCount as int    
  SET @cCount = 0    
 DECLARE @mCount as int    
  SET @mCount = 0    
 DECLARE @IsClearingTrx as bit    
  SET @IsClearingTrx = 0    
 DECLARE @IsLocalCurrency as bit    
  SET @IsLocalCurrency = 0     
 DECLARE @IsGCC as bit    
  SET @IsGCC = 0    
 Declare @SOCID as varchar(30)    
  SET @SOCID = ''
 Declare @ATMChargeID as varchar(6)    
 DECLARE @ProductID as varchar(30)      
 DECLARE @ValueDate as datetime
 DECLARE @MobileNo nvarchar(30)
 DECLARE @Email nvarchar(100)
 DECLARE @rCurrencyID nvarchar(30)
 DECLARE @ClientID nvarchar(30)
 DECLARE @NarrationID nvarchar(30)
  SET @MobileNo = ''
  SET @Email = ''
  SET @rCurrencyID = ''
  SET @ClientID = ''
  SET @NarrationID = ''
 DECLARE @CountryCode as nvarchar(3)
  SET @CountryCode = ''
 DECLARE @TrxDetails as varchar(50)
  SET @TrxDetails = 'POS Trx'
 
 BEGIN    
    
  SELECT @Status = -1    
  
  if (@AuthIDResp = '')
  begin
   SET @AuthIDResp = '0'
  end
  
  SELECT @rCurrencyID=ISNULL(CurrencyID,'') FROM t_Currencies WHERE OurBranchID = @OurBranchID AND CurrencyCode = @CurrCodeTran  
  SELECT @mLastEOD=LastEOD,@mwDate=WorkingDate,@mCONFRETI=CONFRETI FROM t_Last WHERE (OurBranchID = @OurBranchID)    
  SELECT @vBranchName=Heading2, @mBankCode=OurBankID FROM t_GlobalVariables WHERE (OurBranchID = @OurBranchID)    
  SELECT @vCheckLimit=CheckLimit, @LocalCurrencyID=LocalCurrencyID,@LocalCurrency=LocalCurrency FROM t_ATM_GlobalVariables Where OurBranchID = @OurBranchID    
  SELECT @vODLimitAllow=ODLimitAllow FROM t_ATM_Banks WHERE OurBranchID = @OurBranchID AND BankIMD = @AckInstIDCode    
  
  IF (@MessageType = '0220' OR @MessageType = '0221')    
  BEGIN    
   IF (@ForwardInstID = '500000')    
   BEGIN
    SET @IsClearingTrx = 1    
	if (LEFT(@POSEntryMode,2) = '01')
    BEGIN
     SET @TrxDetails = 'ECOM Trx II'
    END
    ELSE
    BEGIN
     SET @TrxDetails = 'POS Trx II'
    END
   END
   ELSE    
   BEGIN
    SET @IsClearingTrx = 0    
	if (LEFT(@POSEntryMode,2) = '01')
    BEGIN
     SET @TrxDetails = 'ECOM Trx'
    END
    ELSE
    BEGIN
     SET @TrxDetails = 'POS Trx'
    END
   END
  END    
  
  SET @ValueDate = @mwDate
  
  IF (@ExtendedData = 'DA') -- For Declinals the account id has tokanized PAN
  begin
   SELECT @AccountID=ISNULL(AccountID,'') from t_DebitCards where OurBranchID = @OurBranchID AND DebitCardToken = @AccountID
  END
  
  SELECT @vCount1=count(*) From t_Account WHERE (OurBranchID = @OurBranchID) AND( AccountID=@AccountID)    
  IF @vCount1=0    
  BEGIN --INVALID_ACCOUNT    
  
   /** Rejection change STARTS 06/04/2021 **/
	
   INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
   VALUES (@OurBranchID, @AccountID, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @POSID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @mDescription, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '02', 'Invalid Account', @TrxDetails, @AuthIDResp, @ExtendedData)
   
   SELECT @Status=2, @AvailableBalance = 0, @vClearBalance = 0    
   SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, '00' As value,'784' as CurrencyCode, @AuthIDResp as AuthIDResp,@MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
   RETURN (1)    
   
   /** Rejection change ENDS 06/04/2021 **/
   
  END    
  
  SELECT @mAccountName=B.Name, @mProductID=B.ProductID, @mCurrencyID=P.CurrencyID,@mAccountType='C',@vClearBalance=ISNULL(B.ClearBalance,0),    
  @vClearedEffects=ISNULL(B.Effects,0),@vFreezeAmount=ISNULL(B.FreezeAmount,0),@vMinBalance=ISNULL(P.MinBalance,0),@vIsClose=ISNULL(A.Status,''),    
  @vAreCheckBooksAllowed=P.AreCheckBooksAllowed,@vAllowDebitTransaction=ISNULL(A.AllowDebitTransaction,0),@CurrencyCode = ISNULL(c.CurrencyCode,''),    
  @value=ISNULL(p.productatmcode,'00'),@SOCID=ISNULL(a.SOCID,''),@ProductID=a.ProductID,@vAccountLimit=ISNULL(B.Limit,0),@MobileNo=ISNULL(cust.MobileNo,''),@Email=ISNULL(cust.Email,''),@ClientID=cust.ClientID
  FROM t_AccountBalance B     
  INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND  B.ProductID = A.ProductID     
  INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND  A.ProductID = P.ProductID     
  INNER JOIN t_Currencies C ON A.OurBranchID = C.OurBranchID AND A.CurrencyID = C.CurrencyID    
  INNER JOIN t_Customer cust ON A.OurBranchID = cust.OurBranchID AND A.ClientID = cust.ClientID
  WHERE (A.OurBranchID = @OurBranchID) AND (A.AccountID = @AccountID)    
    
  --exec @vAccountLimit = [fnc_GetLimit] @OurBranchID, @AccountID    
    
  IF @vAccountLimit > 0 AND @vCheckLimit = 1 AND @vODLimitAllow = 1    
  BEGIN    
   SET @vClearBalance=(@vClearBalance+@vAccountLimit)    
  END 
  
  if (@LocalCurrencyID = @CurrencyCode)    
   SET @IsLocalCurrency = 1    
    
  /* No charges yet on POS Trxs */    
  select @IsGCC=ISNULL(IsGCC,0), @CountryCode=ISNULL(CountryCode,'') from t_Country where OurBranchID = @OurBranchID AND ShortName = RIGHT(@NameLocation, 2)
    
  if (@CountryCode = '784')    
  BEGIN 
   SET @ATMChargeID = '600000'    
   
   if (LEFT(@POSEntryMode,2) = '01')
   BEGIN
    set @mDescriptionID = '200120'        --new
    SET @mDescription = 'Online Card Transaction at '+@NameLocation+' MERCH ID: '+@MerchantType+' COUNTRY: '+@AcqCountryCode+' TRX DATE: '+convert(varchar(20), @PHXDate, 120)
    Set @mRemarks=''
    
    SET @mDescriptionH = 'Online Card Transaction at '+@NameLocation+' MERCH ID: '+@MerchantType+' COUNTRY: '+@AcqCountryCode+' TRX DATE: '+convert(varchar(20), @PHXDate, 120)
    Set @mRemarksCharges=''
    
    SET @mDescriptionCharges  = ''
    SET @mDescriptionHCharges  = ''            
   END
   ELSE
   BEGIN
  
    --set @mDescriptionID = '815'    --old
    set @mDescriptionID = '200110'        --new
    
    SET @mDescription = 'Card Transaction at '+@NameLocation+' MERCH ID: '+@MerchantType+' COUNTRY: '+@AcqCountryCode+' TRX DATE: '+convert(varchar(20), @PHXDate, 120)
    Set @mRemarks=''
    
    SET @mDescriptionH = 'Card Transaction at '+@NameLocation+' MERCH ID: '+@MerchantType+' COUNTRY: '+@AcqCountryCode+' TRX DATE: '+convert(varchar(20), @PHXDate, 120)
    Set @mRemarksCharges=''
    
    SET @mDescriptionCharges  = ''
    SET @mDescriptionHCharges  = ''
   END

  END    
  else IF (@IsGCC = 1)    
  BEGIN    
   SET @ATMChargeID = 'GCC'    
   
   if (LEFT(@POSEntryMode,2) = '01')
   BEGIN
    set @mDescriptionID = '200120'        --new
    SET @mDescription = 'Online Card Transaction at '+@NameLocation+' MERCH ID: '+@MerchantType+' COUNTRY: '+@AcqCountryCode+' TRX DATE: '+convert(varchar(20), @PHXDate, 120)
    Set @mRemarks=''
    
    SET @mDescriptionH = 'Online Card Transaction at '+@NameLocation+' MERCH ID: '+@MerchantType+' COUNTRY: '+@AcqCountryCode+' TRX DATE: '+convert(varchar(20), @PHXDate, 120)
    Set @mRemarksCharges=''
    
    SET @mDescriptionCharges  = ''
    SET @mDescriptionHCharges  = ''            
   END
   ELSE
   BEGIN
    --set @mDescriptionID = '815'    --old
    
    set @mDescriptionID = '200110'        --new
    
    SET @mDescription = 'Card Transaction at '+@NameLocation+' MERCH ID: '+@MerchantType+' COUNTRY: '+@AcqCountryCode+' TRX DATE: '+convert(varchar(20), @PHXDate, 120)
    Set @mRemarks=''
    
    SET @mDescriptionH = 'Card Transaction at '+@NameLocation+' MERCH ID: '+@MerchantType+' COUNTRY: '+@AcqCountryCode+' TRX DATE: '+convert(varchar(20), @PHXDate, 120)
    Set @mRemarksCharges=''
    
    SET @mDescriptionCharges  = ''
    SET @mDescriptionHCharges  = ''
   END
  END    
  ELSE    
  BEGIN    
   SET @ATMChargeID = '500000'    
   
   if (LEFT(@POSEntryMode,2) = '01')
   BEGIN
    set @mDescriptionID = '200120'        --new
    SET @mDescription = 'Online Card Transaction at '+@NameLocation+' MERCH ID: '+@MerchantType+' COUNTRY: '+@AcqCountryCode+' TRX DATE: '+convert(varchar(20), @PHXDate, 120)
    Set @mRemarks=''
    
    SET @mDescriptionH = 'Online Card Transaction at '+@NameLocation+' MERCH ID: '+@MerchantType+' COUNTRY: '+@AcqCountryCode+' TRX DATE: '+convert(varchar(20), @PHXDate, 120)
    Set @mRemarksCharges=''
    
    SET @mDescriptionCharges  = ''
    SET @mDescriptionHCharges  = ''            
   END
   ELSE
   BEGIN
    --set @mDescriptionID = '816'    --old
    
    set @mDescriptionID = '200270'        --new
    SET @mDescription = 'Card Purchase International Transaction MERCH ID: '+@MerchantType+' VAL DATE: '+convert(varchar(20), @mwDate, 120)+' AMOUNT: '+convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate)+' From : ' + @NameLocation
    Set @mRemarks=''
    SET @mDescriptionH = 'Card Purchase International Transaction MERCH ID: '+@MerchantType+' VAL DATE: '+convert(varchar(20), @mwDate, 120)+' AMOUNT: '+convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate)+' From : ' + @NameLocation
    
    Set @mRemarksCharges=''
    SET @mDescriptionCharges  = ''
    SET @mDescriptionHCharges  = ''
   END
  END    
    
  SET @NarrationID = @mDescriptionID
  
  /*****DECLINE ADVISE STARTS *****/
  
  IF (@ExtendedData = 'DA')
  begin
  
   Insert into t_ENDeclineAdvices (OurBranchID, AccountID, RetRefNum, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode,
   ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, CardAccptID, CardAccptNameLoc,
   DescriptionID, VISATrxID, CAVV, ResponseCode)
   VALUES        
   (        
    @OurBranchID,@AccountID,@RetRefNum,@STAN,@PHXDate,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@Amount,@mForeignAmount,@mExchangeRate,@ConvRate,@MerchantType,
	@AcqCountryCode,@ForwardInstID,@POSID,@CurrCodeTran,@CurrCodeSett,@SettlmntAmount,@ProcCode,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@CardAccptID,
	@NameLocation,@NarrationID,@VISATrxID,@CAVV,@ResponseCode    
   )
  
   SELECT @Status    = 0,       
   @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,      
   @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100      
       
   SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value, @CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp,@MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
   RETURN 0;    
   
  end
  
  /*****DECLINE ADVISE ENDS *****/
  
  
  IF @mLastEOD = @mwDate or @mCONFRETI = '1/1/9999'    
  BEGIN --INVALID_DATE    
   
   /** Rejection change STARTS 06/04/2021 **/
	
   INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
   VALUES (@OurBranchID, @AccountID, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @POSID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @mDescription, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '95', 'Day End', @TrxDetails, @AuthIDResp, @ExtendedData)
   
   SELECT @Status = 95,    
   @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,    
   @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100    
      
   SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value,@CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp,@MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
   RETURN (1)    
   
   /** Rejection change ENDS 06/04/2021 **/
   
  END    
  
  IF (@SOCID <> '') 
  BEGIN    
   SELECT @WithDrawlCharges=POSRetailTransactionCharges,@IssuerChargesAccountID=AcquiringChargesAccountID    
   From t_SOCATMCharges          
   WHERE (OurBranchID = @OurBranchID AND ProductID = @ProductID AND SOCID = @SOCID AND ATMChargeID = @ATMChargeID)    
  END
      
  /* No charges yet on POS Trxs set resets the value to 0 */    
    
  --SET @WithDrawlCharges = 0    
  --SET @WithDrawlChargesPercentage = 0    
    
  /** If its a clearing trx then release hold -- STARTS **/    
    
  IF (@IsClearingTrx = 1)    
  BEGIN    
   SELECT @mCount=count(*) FROM t_ATM_HoldTrxs WHERE (OurBranchID=@OurBranchID)     
   AND (AccountID=@AccountID) AND HoldStatus IN ('H','P') AND     
   (RefNo IN (@OrgTrxRefNo,@OrgTrxRefNo+'|CHGS1',@OrgTrxRefNo+'|CHGS2') OR VISATrxID=@VISATrxID) group by HoldStatus    
    
   SELECT @remAmount = SUM(ISNULL(Amount,0)), @mAmount = SUM(ISNULL(Amount,0))
   --, @Amount = SUM(ISNULL(Amount,0)) 
   FROM t_ATM_HoldTrxs WHERE (OurBranchID=@OurBranchID) AND (AccountID=@AccountID)     
   AND (RefNo = @OrgTrxRefNo OR VISATrxID=@VISATrxID) AND HoldStatus IN ('H','P') group by HoldStatus    
       
   IF @mCount = 0    
   BEGIN --NO TRX ON ACCOUNT    
    
	/** Rejection change STARTS 06/04/2021 **/
	 
    INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
    VALUES (@OurBranchID, @AccountID, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @POSID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @mDescription, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '21', 'No Transaction on Account', @TrxDetails, @AuthIDResp, @ExtendedData)
    
    SELECT @Status = 21,    
    @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,    
    @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100    
    SELECT @Status As Status , @vClearBalance As ClearBalance , @AvailableBalance As AvailableBalance , @value As [value],@CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp,@MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
    RETURN (1)     
    
    /** Rejection change ENDS 06/04/2021 **/
   
   END   
   
   -- validate amounts with percent and max amounts STARTS
   
   declare @MaxAmount as money
   declare @Percent as decimal(22,2)
   declare @MarginAmount as money
    SET @MaxAmount = 0
    SET @Percent = 0
    set @MarginAmount = 0
   
   select @Percent=[Percent], @MaxAmount=MaxAmount FROM [t_POSBaseIMargin]
   WHERE OurBranchID = @OurBranchID AND SerialNo = 1
   
   SET @MarginAmount = ((@mAmount * @Percent) / 100)
   SET @MarginAmount = @MarginAmount + @mAmount
   SET @MaxAmount = @mAmount + @MaxAmount

   if (@Amount > @MaxAmount)
   BEGIN
    
	INSERT INTO t_RejectedBaseII (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID)
	VALUES (@OurBranchID, @AccountID, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @POSID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @mDescription, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID)
	
	
    SELECT @Status = 48,
    @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,    
    @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100    
         
    SELECT @Status As Status , @vClearBalance As ClearBalance , @AvailableBalance As AvailableBalance , @value As [value],@CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp,@MobileNo as MobileNo, @Email as Email,@rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
        
    RETURN (1)
   END
   ELSE if ((@Amount > @MarginAmount) AND (@Amount > @MaxAmount))
   BEGIN
   
	INSERT INTO t_RejectedBaseII (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID)
	VALUES (@OurBranchID, @AccountID, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @POSID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @mDescription, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID)
	
	
    SELECT @Status = 48,
    @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,    
    @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100    
         
    SELECT @Status As Status , @vClearBalance As ClearBalance , @AvailableBalance As AvailableBalance , @value As [value],@CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp,@MobileNo as MobileNo, @Email as Email,@rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
        
    RETURN (1)     
   END
   
   -- validate amounts with percent and max amounts ENDS
   
   
   Select * Into #TempPurchaseRequestTransaction FROM t_ATM_HoldTrxs WHERE (OurBranchID=@OurBranchID) AND (AccountID=@AccountID)     
   AND (RefNo IN (@OrgTrxRefNo,@OrgTrxRefNo+'|CHGS1',@OrgTrxRefNo+'|CHGS2') OR VISATrxID=@VISATrxID) AND HoldStatus IN ('H','P')    
    
   SELECT @cCount=COUNT(*) from #TempPurchaseRequestTransaction    
  
   WHILE (@cCount <> @sCount)    
   BEGIN    
    SET @sCount = @sCount + 1    
       
    --Clear Hold Trx Status    
    SELECT @mAccountID=AccountID, @mRefNo=RefNo, @mHoldStatus=HoldStatus, @ValueDate=convert(varchar(10), wDate, 120)    
    FROM (    
     SELECT AccountID, RefNo, HoldStatus, wDate, ROW_NUMBER() OVER (ORDER BY RefNo) AS RowNum    
     FROM #TempPurchaseRequestTransaction    
    ) AS oTable    
    WHERE oTable.RowNum = @sCount    
    
    UPDATE t_ATM_HoldTrxs SET HoldStatus = 'C', ReleaseDate = @mwDate + ' ' + convert(varchar(12), GetDate(), 114)    
    WHERE OurBranchID = @OurBranchID AND AccountID = @mAccountID AND HoldStatus = @mHoldStatus AND RefNo = @mRefNo    
    
    --Get Hold Amount    
    SELECT @HoldAmount=Amount FROM t_AccountFreeze     
    WHERE OurBranchID=@OurBranchID AND AccountID=@mAccountID AND IsFreeze = 1 AND ATMTrxHoldRef = @mRefNo    
    
    --update account freeze to unfreeze    
    Update t_AccountFreeze SET IsFreeze = 0, UnFreezedDate = @mwDate + ' ' + convert(varchar(12), GetDate(), 114), ReleaseComments = @mDescription    
    WHERE OurBranchID = @OurBranchID AND AccountID = @mAccountID AND ATMTrxHoldRef = @mRefNo    
         
    Update t_AccountBalance SET FreezeAmount = ISNULL(FreezeAmount,0) - ISNULL(@HoldAmount,0)    
    WHERE OurBranchID = @OurBranchID AND AccountID = @mAccountID    
        
    SELECT @FreezeAmount = FreezeAmount FROM t_AccountBalance WHERE OurBranchID = @OurBranchID AND AccountID = @mAccountID    
        
    IF (@FreezeAmount <= 0)    
    BEGIN    
     Update t_AccountBalance SET IsFreezed = 0    
     WHERE OurBranchID = @OurBranchID AND AccountID = @mAccountID    
    END    
        
    /* Log the Updated Balance after releasing freeze */    
        
    Insert Into BalChk     
    (    
     OurBranchID, AccountID, ProductID, [Name], LastUpdateTime, ClearBalance, Limit, Effects, Shadow, FreezeAmt, MinBalance,     
     TotalBalance, AvailableBalance    
    )    
    SELECT OurBranchID, AccountID, ProductID, [Name], @mwDate + ' ' + convert(varchar(12), GetDate(), 114), ISNULL(ClearBalance,0), ISNULL(Limit,0),     
    ISNULL(Effects,0), ISNULL(ShadowBalance,0), ISNULL(FreezeAmount,0), @vMinBalance,     
    (isnull(@vAccountLimit,0)+ISNULL(ClearBalance,0)-ISNULL(Effects,0)),    
    (isnull(@vAccountLimit,0)+ISNULL(ClearBalance,0)-ISNULL(FreezeAmount,0)-@vMinBalance)    
    FROM t_AccountBalance WHERE OurBranchID = @OurBranchID AND AccountID = @mAccountID    
       
   END    
    
   /* Get Latest Balance again.. */    
      
   SELECT @mAccountName=B.Name, @mProductID=B.ProductID, @mCurrencyID=P.CurrencyID,@mAccountType='C',    
   @vClearBalance=ISNULL(B.ClearBalance,0),@vClearedEffects=ISNULL(B.Effects,0),@vFreezeAmount=ISNULL(B.FreezeAmount,0),    
   @vMinBalance=ISNULL(P.MinBalance,0),@vIsClose=ISNULL(A.Status,''),@vAreCheckBooksAllowed=P.AreCheckBooksAllowed,     
   @vAllowDebitTransaction=ISNULL(A.AllowDebitTransaction,0),@CurrencyCode = ISNULL(c.CurrencyCode,''),    
   @value=ISNULL(p.productatmcode,'00'),@vAccountLimit=ISNULL(B.Limit,0)
   FROM t_AccountBalance B     
   INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND  B.ProductID = A.ProductID     
   INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND  A.ProductID = P.ProductID     
   INNER JOIN t_Currencies C ON A.OurBranchID = C.OurBranchID AND A.CurrencyID = C.CurrencyID    
   WHERE (A.OurBranchID = @OurBranchID) AND (A.AccountID = @AccountID)    
      
   --exec @vAccountLimit = [fnc_GetLimit] @OurBranchID, @AccountID    
      
   IF @vAccountLimit > 0 AND @vCheckLimit = 1 AND @vODLimitAllow = 1    
   BEGIN    
    SET @vClearBalance=(@vClearBalance+@vAccountLimit)    
   END    
    
   /** If its a clearing trx then release hold -- ENDS **/    
  END    
  ELSE
  BEGIN
   SET @OrgTrxRefNo = @RefNo
  END
  
  if (@MessageType = '0200')
  begin      
   IF @vIsClose = 'C' or @vIsClose = 'T' or @vIsClose = 'X' or @vIsClose = 'D' or @vIsClose = 'I' or @vAllowDebitTransaction = 1          
   BEGIN --INACTIVE/CLOSE_ACCOUNT    
    
	/** Rejection change STARTS 06/04/2021 **/
   
    INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
    VALUES (@OurBranchID, @AccountID, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @POSID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @mDescription, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '03', 'Account In-Active/ Closed/ Dormant/ Deceased/ Blocked', @TrxDetails, @AuthIDResp, @ExtendedData)
	
	SELECT @Status=3,    
    @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,    
    @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100    
    
    SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value,@CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp,@MobileNo as MobileNo, @Email as Email,@rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
    RETURN (1)    
    
    /** Rejection change ENDS 06/04/2021 **/
   END
  
   IF @Amount > (@vClearBalance-@WithDrawlCharges) or     
      @Amount > (@vClearBalance-@vFreezeAmount-@vMinBalance-@WithDrawlCharges) or     
      @Amount > (@vClearBalance-@vMinBalance-@WithDrawlCharges)    
   BEGIN    
    --LOW_BALANCE    
    
	/** Rejection change STARTS 06/04/2021 **/
   
    INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
    VALUES (@OurBranchID, @AccountID, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @POSID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @mDescription, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '04', 'Low Balance', @TrxDetails, @AuthIDResp, @ExtendedData)
	
	SELECT @Status=4,    
    @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,    
    @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100    
        
    SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value, @CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp,@MobileNo as MobileNo, @Email as Email,@rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
    RETURN (1)    
    
    /** Rejection change ENDS 06/04/2021 **/
   END    
  END
    
  SELECT @vCount1=count(*),@Super=Supervision From t_ATM_TransferTransaction    
  WHERE (OurBranchID = @OurBranchID) AND( AccountID=@AccountID) AND (RefNo=@RefNo) and (AtmID=@POSID) group by Supervision      
         
  IF @vCount1>0 and @Super='C'    
  BEGIN  --DUP_TRAN    
   
   /** Rejection change STARTS 06/04/2021 **/
   
   INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
   VALUES (@OurBranchID, @AccountID, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @POSID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @mDescription, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '10', 'Duplicate Transaction', @TrxDetails, @AuthIDResp, @ExtendedData)

   SELECT @Status = 10,     
   @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,    
   @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100    
      
   SELECT @Status As Status , @vClearBalance As ClearBalance , @AvailableBalance As AvailableBalance , @value As value,@CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp,@MobileNo as MobileNo, @Email as Email,@rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
   RETURN (1)    
   
   /** Rejection change ENDS 06/04/2021 **/
  END      
    
  IF @vCount1>0 and @Super='R'          
  BEGIN   --ORIG_ALREADY_REVERSED          
   /** Rejection change STARTS 06/04/2021 **/
   
   INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
   VALUES (@OurBranchID, @AccountID, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @POSID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @mDescription, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '35', 'Orig Already Reversed', @TrxDetails, @AuthIDResp, @ExtendedData)

   SELECT @Status = 35,     
   @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,    
   @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100    
    
   SELECT @Status As Status , @vClearBalance As ClearBalance , @AvailableBalance As AvailableBalance , @value As value,@CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp,@MobileNo as MobileNo, @Email as Email,@rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
   RETURN (1)           
   
   /** Rejection change ENDS 06/04/2021 **/
  END          
  
  SET @sCount = 0    
  SET @cCount = 1    
      
  WHILE (@sCount <> @cCount)      
  BEGIN      
   exec @mScrollNo = fnc_GetRandomScrollNo    
   if not exists (SELECT ScrollNo from t_ATM_TransferTransaction Where OurBranchID = @OurBranchID AND ScrollNo = @mScrollNo)    
   BEGIN    
    SET @sCount = @sCount + 1      
   END    
  END    
    
  SELECT @GLControl = ISNULL(GLControl, '') from t_Products where OurBranchID = @OurBranchID and ProductID = @mProductID      
  SET @VoucherID = @mScrollNo
    
  if (@AuthIDResp = '0')
  begin
   exec @AuthIDResp = fnc_GetRandomNumber
  end
    
  SET @mSerialNo = @mSerialNo+1    
  SET @IsCredit = 0    
  Set @mTrxType = 'D'    
    
  INSERT INTO t_ATM_TransferTransaction    
  (    
   ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,    
   Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,[Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MerchantType,STAN,    
   SettlmntAmount,ConvRate,AcqCountryCode,CurrCodeTran,CurrCodeSett,ATMStatus,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,    
   CardAccptID,CardAccptNameLoc,VISATrxID,ProcCode,CAVV,ResponseCode,ForwardInstID,ClearingRefNo
  )      
    
  VALUES    
  (      
   @mScrollNo,@mSerialNo,@OrgTrxRefNo,@OurBranchID,@WithdrawlBranchID,@AccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,@ValueDate,    
   @mwDate + ' ' + convert(varchar(12), GetDate(), 114),@PHXDate,@mTrxType,@Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,    
   @mDescription,@POSID,@mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MerchantType,@STAN,@SettlmntAmount,@ConvRate,@AcqCountryCode,    
   @CurrCodeTran,@CurrCodeSett,@Supervision,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@RetRefNum,@AuthIDResp,@CardAccptID,@NameLocation,    
   @VISATrxID,@ProcCode,@CAVV,@ResponseCode,@ForwardInstID,@Refno
  )    
    
  INSERT INTO t_Transactions       
  (      
   OurBranchID,ScrollNo,SerialNo,RefNo,wDate,AccountType,DocumentType,AccountID,AccountName,ProductID,CurrencyID,ValueDate,TrxType,ChequeID,ChequeDate,Amount,    
   ForeignAmount,ExchangeRate,ProfitLoss,MeanRate,DescriptionID,[Description],BankCode,BranchCode,TrxPrinted,GLID,[Status],IsLocalCurrency,OperatorID,    
   SupervisorID,AdditionalData,IsMainTrx,DocType,GLVoucherID,Remarks,MerchantType,STAN,SettlmntAmount,ConvRate,AcqCountryCode,CurrCodeTran,CurrCodeSett,ATMStatus,
   POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,CardAccptID,CardAccptNameLoc,VISATrxID,ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,ForwardInstID,ClearingRefNo,TClientId,TAccountid,TProductId
  )      
  VALUES       
  (      
   @OurBranchID,@mScrollNo,@mSerialNo,@OrgTrxRefNo,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@mAccountType,'ATT',@AccountID,@mAccountName,@mProductID,    
   @mCurrencyID,@ValueDate,@mTrxType,'',@mwDate,@Amount,@mForeignAmount,@mExchangeRate,0,@mExchangeRate,@mDescriptionID,@mDescription,@mBankCode,    
   @WithdrawlBranchID,0,@mGLID,@Supervision,@mIsLocalCurrency,@mOperatorID,@mSupervisorID,null,'1','ATT',@VoucherID,@mRemarks,@MerchantType,@STAN,@SettlmntAmount,    
   @ConvRate,@AcqCountryCode,@CurrCodeTran,@CurrCodeSett,@Supervision,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@RetRefNum,@AuthIDResp,    
   @CardAccptID,@NameLocation,@VISATrxID,@ChannelId,@ChannelRefID,@ProcCode,@CAVV,@ResponseCode,@ForwardInstID,@RefNo,@ClientID,@AccountID,@mProductID
  ) 
  
  --set @mScrollNo = 0 for @GLControl before @mSerialNo
    
  INSERT INTO t_GLTransactions       
  (      
   OurBranchID,RefNo,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,ExchangeRate,IsCredit,TransactionType,    
   TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,IsMainTrx,DocType,Remarks,MerchantType,STAN,SettlmntAmount,ConvRate,    
   AcqCountryCode,CurrCodeTran,CurrCodeSett,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,CardAccptID,CardAccptNameLoc,VISATrxID,    
   ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,ForwardInstID,TClientId,TAccountid,TProductId
  )      
  VALUES      
  (      
   @OurBranchID,@OrgTrxRefNo,@GLControl,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@ValueDate,@mDescriptionID,@mDescription,    
   @mCurrencyID,@Amount,@mForeignAmount,@mExchangeRate,@IsCredit,'ATMTransfer','L',@mOperatorID,@mSupervisorID,'',0,@mSerialNo,null,'0','ATT',@mRemarks,    
   @MerchantType,@STAN,@SettlmntAmount,@ConvRate,@AcqCountryCode,@CurrCodeTran,@CurrCodeSett,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,    
   @RetRefNum,@AuthIDResp,@CardAccptID,@NameLocation,@VISATrxID,@ChannelId,@ChannelRefID,@ProcCode,@CAVV,@ResponseCode,@ForwardInstID,@ClientID,@AccountID,@mProductID
  )    
      
  --Debit Transaction Switch Charges       
    
  IF @MerchantType <> '0000'  and @WithDrawlCharges > 0    
  BEGIN    
   
   SET @mSerialNo = @mSerialNo+1    
       
   INSERT INTO t_ATM_TransferTransaction    
   (    
    ScrollNo,SerialNo,RefNo,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,    
    Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,[Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MerchantType,STAN,    
    SettlmntAmount,ConvRate,AcqCountryCode,CurrCodeTran,CurrCodeSett,ATMStatus,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,    
    CardAccptID,CardAccptNameLoc,VISATrxID,ProcCode,CAVV,ResponseCode,ForwardInstID,ClearingRefNo
   )    
   VALUES    
   (    
    @mScrollNo,@mSerialNo,@OrgTrxRefNo+'|CHGS1',@OurBranchID,@WithdrawlBranchID,@AccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,@ValueDate,    
    @mwDate + ' ' + convert(varchar(12), GetDate(), 114),@PHXDate,@mTrxType,@Supervision,@mGLID,Round(@WithDrawlCharges,2),@mForeignAmount,@mExchangeRate,    
    @mDescriptionID,@mDescriptionCharges,@POSID,@mBankCode,@mRemarksCharges,@mOperatorID,@mSupervisorID,@MerchantType,@STAN,@SettlmntAmount,    
    @ConvRate,@AcqCountryCode,@CurrCodeTran,@CurrCodeSett,@Supervision,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@RetRefNum,@AuthIDResp,    
    @CardAccptID,@NameLocation,@VISATrxID,@ProcCode,@CAVV,@ResponseCode,@ForwardInstID,@RefNo
   )    
   
   INSERT INTO t_Transactions       
   (      
    OurBranchID,ScrollNo,SerialNo,RefNo,wDate,AccountType,DocumentType,AccountID,AccountName,ProductID,CurrencyID,ValueDate,TrxType,ChequeID,ChequeDate,Amount,    
    ForeignAmount,ExchangeRate,ProfitLoss,MeanRate,DescriptionID,[Description],BankCode,BranchCode,TrxPrinted,GLID,[Status],IsLocalCurrency,OperatorID,SupervisorID,AdditionalData,
	IsMainTrx,DocType,GLVoucherID,Remarks,MerchantType,STAN,SettlmntAmount,ConvRate,AcqCountryCode,CurrCodeTran,CurrCodeSett,ATMStatus,POSEntryMode,POSConditionCode,
	POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,CardAccptID,CardAccptNameLoc,VISATrxID,ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,ForwardInstID,ClearingRefNo,TClientId,TAccountid,TProductId
   )      
   VALUES       
   (      
    @OurBranchID,@mScrollNo,@mSerialNo,@OrgTrxRefNo+'|CHGS1',@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@mAccountType,'ATT',@AccountID,@mAccountName,@mProductID,    
    @mCurrencyID,@ValueDate,@mTrxType,'',@mwDate,Round(@WithDrawlCharges,2),@mForeignAmount,@mExchangeRate,0,@mExchangeRate,@mDescriptionID,    
    @mDescriptionCharges,@mBankCode,@WithdrawlBranchID,0,@mGLID,@Supervision,@mIsLocalCurrency,@mOperatorID,@mSupervisorID,null,'1','ATT',    
    @VoucherID,@mRemarksCharges,@MerchantType,@STAN,@SettlmntAmount,@ConvRate,@AcqCountryCode,@CurrCodeTran,@CurrCodeSett,@Supervision,@POSEntryMode,    
    @POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@RetRefNum,@AuthIDResp,@CardAccptID,@NameLocation,@VISATrxID,@ChannelId,@ChannelRefID,@ProcCode,@CAVV,@ResponseCode,@ForwardInstID,@RefNo,@ClientID,@AccountID,@mProductID
   )
   
   --set @mScrollNo = 0 for @GLControl before @mSerialNo
    
   INSERT INTO t_GLTransactions       
   (      
    OurBranchID,RefNo,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,ExchangeRate,IsCredit,TransactionType,    
    TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,IsMainTrx,DocType,Remarks,MerchantType,STAN,SettlmntAmount,ConvRate,    
    AcqCountryCode,CurrCodeTran,CurrCodeSett,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,CardAccptID,CardAccptNameLoc,VISATrxID,    
    ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,ForwardInstID,TClientId,TAccountid,TProductId
   )      
   VALUES      
   (      
    @OurBranchID,@OrgTrxRefNo+'|CHGS1',@GLControl,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@ValueDate,@mDescriptionID,    
    @mDescriptionCharges,@mCurrencyID,Round(@WithDrawlCharges,2),@mForeignAmount,@mExchangeRate,@IsCredit,'ATMTransfer','L',@mOperatorID,    
    @mSupervisorID,'',0,@mSerialNo,null,'0','ATT',@mRemarksCharges,@MerchantType,@STAN,@SettlmntAmount,@ConvRate,@AcqCountryCode,@CurrCodeTran,    
    @CurrCodeSett,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@RetRefNum,@AuthIDResp,@CardAccptID,@NameLocation,@VISATrxID,@ChannelId,    
    @ChannelRefID,@ProcCode,@CAVV,@ResponseCode,@ForwardInstID,@ClientID,@AccountID,@mProductID
   )    
  END    
    
  --Credit Transaction       
  
  if (@AcqCountryCode = '784')    
  BEGIN    
   IF (@ForwardInstID = '500000')
   BEGIN    
    SET @ATMChargeID = '500000'    
   END
   ELSE
   BEGIN
    SET @ATMChargeID = '600000'    
   END
  END
  else if (@IsGCC = 1)    
  BEGIN    
   IF (@ForwardInstID = '500000')
   BEGIN    
    SET @ATMChargeID = '500000'    
   END
   ELSE
   BEGIN
    SET @ATMChargeID = 'GCC'    
   END
  END
  ELSE    
  BEGIN    
   SET @ATMChargeID = '500000'    
  END
    
  SELECT @vAccountID=AccountID, @mAccountName=[Description],@mProductID='GL', @mCurrencyID=CurrencyID,@mAccountType='G'     
  FROM t_GL WHERE (OurBranchID = @OurBranchID) AND AccountID = (SELECT ATMAccountID FROM t_ATM_ATMAccount WHERE OurBranchID = @OurBranchID and ATMID = @ATMChargeID)-- ))    
    
  Set @mSerialNo = @mSerialNo + 1    
  SET @IsCredit = 1    
  Set @mTrxType = 'C'    
    
  INSERT INTO t_ATM_TransferTransaction    
  (    
   ScrollNo,SerialNo,RefNo,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,    
   Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,[Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MerchantType,STAN,    
   SettlmntAmount,ConvRate,AcqCountryCode,CurrCodeTran,CurrCodeSett,ATMStatus,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,    
   CardAccptID,CardAccptNameLoc,VISATrxID,ProcCode,CAVV,ResponseCode,ForwardInstID,ClearingRefNo
  )    
  VALUES    
  (    
   @mScrollNo,@mSerialNo,@OrgTrxRefNo,@OurBranchID,@WithdrawlBranchID,@vAccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,@ValueDate,    
   @mwDate + ' ' + convert(varchar(12), GetDate(), 114),@PHXDate,@mTrxType,@Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,    
   @mDescriptionH,@POSID,@mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MerchantType,@STAN,@SettlmntAmount,@ConvRate,@AcqCountryCode,    
   @CurrCodeTran,@CurrCodeSett,@Supervision,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@RetRefNum,@AuthIDResp,@CardAccptID,@NameLocation,    
   @VISATrxID,@ProcCode,@CAVV,@ResponseCode,@ForwardInstID,@RefNo
  )    
    
  INSERT INTO t_GLTransactions       
  (      
   OurBranchID,RefNo,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,ExchangeRate,IsCredit,TransactionType,    
   TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,IsMainTrx,DocType,Remarks,MerchantType,STAN,SettlmntAmount,ConvRate,    
   AcqCountryCode,CurrCodeTran,CurrCodeSett,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,CardAccptID,CardAccptNameLoc,VISATrxID,    
   ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,ForwardInstID,TClientId,TAccountid,TProductId
  )      
  VALUES      
  (      
   @OurBranchID,@OrgTrxRefNo,@vAccountID,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@ValueDate,@mDescriptionID,@mDescriptionH,
   @mCurrencyID,@Amount,@mForeignAmount,@mExchangeRate,@IsCredit,'ATMTransfer','L',@mOperatorID,@mSupervisorID,'',@mScrollNo,@mSerialNo,null,'0','ATT',@mRemarks,    
   @MerchantType,@STAN,@SettlmntAmount,@ConvRate,@AcqCountryCode,@CurrCodeTran,@CurrCodeSett,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,    
   @RetRefNum,@AuthIDResp,@CardAccptID,@NameLocation,@VISATrxID,@ChannelId,@ChannelRefID,@ProcCode,@CAVV,@ResponseCode,@ForwardInstID,@ClientID,@AccountID,@mProductID
  )    
    
  --Credit Transaction Switch Charges       
  IF @MerchantType <> '0000'  and @WithDrawlCharges > 0    
  BEGIN    
   SELECT @vAccountID=AccountID, @mAccountName=Description,@mProductID='GL', @mCurrencyID=CurrencyID,@mAccountType='G' FROM t_GL     
   WHERE (OurBranchID = @OurBranchID) AND (AccountID =  @IssuerChargesAccountID)    
    
   Set @mSerialNo = @mSerialNo + 1    
   SET @IsCredit = 1    
   Set @mTrxType = 'C'    
    
   INSERT INTO t_ATM_TransferTransaction    
   (    
    ScrollNo,SerialNo,RefNo,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,    
    Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,[Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MerchantType,STAN,    
    SettlmntAmount,ConvRate,AcqCountryCode,CurrCodeTran,CurrCodeSett,ATMStatus,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,    
    CardAccptID,CardAccptNameLoc,VISATrxID,ProcCode,CAVV,ResponseCode,ForwardInstID,ClearingRefNo
   )    
   VALUES    
   (    
    @mScrollNo,@mSerialNo,@OrgTrxRefNo+'|CHGS1',@OurBranchID,@WithdrawlBranchID,@vAccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,@ValueDate,    
    @mwDate + ' ' + convert(varchar(12), GetDate(), 114),@PHXDate,@mTrxType,@Supervision,@mGLID,Round(@WithDrawlCharges,2),@mForeignAmount,@mExchangeRate,    
    @mDescriptionID,@mDescriptionHCharges,@POSID,@mBankCode,@mRemarksCharges,@mOperatorID,@mSupervisorID,@MerchantType,@STAN,@SettlmntAmount,    
    @ConvRate,@AcqCountryCode,@CurrCodeTran,@CurrCodeSett,@Supervision,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@RetRefNum,@AuthIDResp,    
    @CardAccptID,@NameLocation,@VISATrxID,@ProcCode,@CAVV,@ResponseCode,@ForwardInstID,@RefNo
   )    
    
   INSERT INTO t_GLTransactions       
   (      
    OurBranchID,RefNo,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,ExchangeRate,IsCredit,TransactionType,    
    TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,IsMainTrx,DocType,Remarks,MerchantType,STAN,SettlmntAmount,ConvRate,    
    AcqCountryCode,CurrCodeTran,CurrCodeSett,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,CardAccptID,CardAccptNameLoc,VISATrxID,    
    ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,ForwardInstID,TClientId,TAccountid,TProductId
   )      
   VALUES      
   (      
    @OurBranchID,@OrgTrxRefNo+'|CHGS1',@vAccountID,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@ValueDate,@mDescriptionID,    
    @mDescriptionHCharges,@mCurrencyID,Round(@WithDrawlCharges,2),@mForeignAmount,@mExchangeRate,@IsCredit,'ATMTransfer','L',@mOperatorID,    
    @mSupervisorID,'',@mScrollNo,@mSerialNo,null,'0','ATT',@mRemarksCharges,@MerchantType,@STAN,@SettlmntAmount,@ConvRate,@AcqCountryCode,@CurrCodeTran,    
    @CurrCodeSett,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@RetRefNum,@AuthIDResp,@CardAccptID,@NameLocation,@VISATrxID,@ChannelId,    
    @ChannelRefID,@ProcCode,@CAVV,@ResponseCode,@ForwardInstID,@ClientID,@AccountID,@mProductID
   )    
  END    
    
  /* Log the Updated Balance after transaction */    
    
  Insert Into BalChk     
  (    
   OurBranchID, AccountID, ProductID, [Name], LastUpdateTime, ClearBalance, Limit, Effects, Shadow, FreezeAmt, MinBalance, TotalBalance, AvailableBalance    
  )    
  SELECT OurBranchID, AccountID, ProductID, [Name], @mwDate + ' ' + convert(varchar(12), GetDate(), 114), ISNULL(ClearBalance,0), ISNULL(Limit,0),     
  ISNULL(Effects,0), ISNULL(ShadowBalance,0), ISNULL(FreezeAmount,0), @vMinBalance,     
  (isnull(@vAccountLimit,0)+ISNULL(ClearBalance,0)-ISNULL(Effects,0)),    
  (isnull(@vAccountLimit,0)+ISNULL(ClearBalance,0)-ISNULL(FreezeAmount,0)-@vMinBalance)    
  FROM t_AccountBalance WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID    
    
  SELECT @mAccountName=B.Name, @mProductID=B.ProductID, @mCurrencyID=P.CurrencyID,@mAccountType='C',@vAccountLimit=ISNULL(B.Limit,0),
  @vClearBalance=ISNULL(B.ClearBalance,0),@vClearedEffects=ISNULL(B.Effects,0),@vFreezeAmount=ISNULL(B.FreezeAmount,0),    
  @vMinBalance=ISNULL(P.MinBalance,0),@vIsClose=ISNULL(A.Status,''),@vAreCheckBooksAllowed=P.AreCheckBooksAllowed,     
  @vAllowDebitTransaction=ISNULL(A.AllowDebitTransaction,0),@CurrencyCode = ISNULL(c.CurrencyCode,''),    
  @value=ISNULL(p.productatmcode,'00')    
  FROM t_AccountBalance B     
  INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND  B.ProductID = A.ProductID     
  INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND  A.ProductID = P.ProductID     
  INNER JOIN t_Currencies C ON A.OurBranchID = C.OurBranchID AND A.CurrencyID = C.CurrencyID    
  WHERE (A.OurBranchID = @OurBranchID) AND (A.AccountID = @AccountID)    
      
  --exec @vAccountLimit = [fnc_GetLimit] @OurBranchID, @AccountID    
      
  IF @vAccountLimit > 0 AND @vCheckLimit = 1 AND @vODLimitAllow = 1    
  BEGIN    
   SET @vClearBalance=(@vClearBalance+@vAccountLimit)    
  END    
    
  SELECT @Status = 0 ,     
  @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,      
  @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100      
       
  SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value,@CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp,@MobileNo as MobileNo, @Email as Email,@rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
  RETURN 0     
 END