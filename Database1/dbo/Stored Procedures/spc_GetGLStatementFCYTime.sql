﻿CREATE PROCEDURE [dbo].[spc_GetGLStatementFCYTime]      
(                 
 @OurBranchID varchar(30),                 
 @AccountID varchar(30),                 
 @sDate Datetime,                 
 @eDate Datetime                 
)                 
                 
AS                 
                 
 SET NOCOUNT ON                 
                 
 DECLARE @OpeningBalance as money                 
 DECLARE @LocEqOpBal as money                 
     
 DECLARE @CurrencyID as nvarchar(30)    
     
 DECLARE @crOpBal as money                 
 DECLARE @crLocEqOpBal as money                 
 DECLARE @drOpBal as money                 
 DECLARE @drLocEqOpBal as money                 
                 
 --DECLARE @WorkingDate as datetime  
 --SELECT @WorkingDate = WorkingDate From t_Last WHERE OurBranchID = @OurBranchID  
  
 SET @OpeningBalance = 0                 
 SET @crOpBal = 0                 
 SET @drOpBal = 0                 
                 
 SET @LocEqOpBal = 0                 
 SET @crLocEqOpBal = 0                 
 SET @drLocEqOpBal = 0                 
     
 set @CurrencyID = ''    
    
 SELECT @CurrencyID = CurrencyID, @OpeningBalance = IsNull(ForeignOpeningBalance,0),@LocEqOpBal = IsNull(OpeningBalance,0)                 
 FROM t_GL                 
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                 
                 
 SELECT @crOpBal = IsNull(SUM(ForeignAmount),0), @crLocEqOpBal = IsNull(SUM(Amount),0) From t_GLTransactions                 
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                 
 AND IsCredit = '1' and [Status] <> 'R'            
 AND convert(varchar(10), Date, 101) < @sDate                 
                 
 SELECT @drOpBal = IsNull(SUM(ForeignAmount),0), @drLocEqOpBal = IsNull(SUM(Amount),0) From t_GLTransactions                 
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                 
 AND IsCredit = '0' and [Status] <> 'R'            
 AND convert(varchar(10), Date, 101) < @sDate                 
                 
 SET @OpeningBalance = @OpeningBalance + @crOpBal - @drOpBal                 
 SET @LocEqOpBal = @LocEqOpBal + @crLocEqOpBal - @drLocEqOpBal                 
     
 SELECT @OurBranchID AS OurBranchID, @AccountID AS AccountID, @CurrencyID AS CurrencyID, IsNull(@LocEqOpBal,0) as Amount, IsNull(@OpeningBalance,0) as ForeignAmount, 0 AS IsCredit,    
 @sDate as ValueDate, '01/01/1900' as [Date], '' AS ChannelRefID, '' As TrxType, 0 AS VoucherID, 0 AS SerialID, '' AS DescriptionID, 'Opening Balance' as [Description],     
 GETDATE() AS TrxTimeStamp, 'OP' As TableStr    
     
 UNION ALL                 
                 
 SELECT @OurBranchID AS OurBranchID, @AccountID AS AccountID, @CurrencyID AS CurrencyID, Amount, ForeignAmount, IsCredit,    
 ValueDate, Date as wDate, ChannelRefID, CASE IsCredit WHEN 1 THEN 'C' ELSE 'D' END As TrxType, VoucherID, SerialID,    
 DescriptionID, Description, TrxTimeStamp,    
 'H' As TableStr    
 FROM t_GLTransactions    
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID  and [Status] <> 'R'    
 AND convert(varchar(10), Date, 101) >= @sDate AND convert(varchar(10), Date, 101) <= @eDate