﻿CREATE   procedure [dbo].[spc_AccSaveSavingsProducts]
AS
truncate table t_AccSaveSavingsProducts;
truncate table t_AccSaveSavingsAccounts;

insert into t_AccSaveSavingsProducts
select p.OurBranchID, ProductID, p.Description, p.CurrencyID, c.MeanRate, case when ISNULL(c.CRRounding,0) > ISNULL(c.DBRounding,0) then c.CRRounding else c.DBRounding end AS CcyRounding,
CreditInterestStart, CreditInterestFrequency, CreditRounding, CreditInterestDays, MinCreditInterest,
TaxRate, TaxRounding, GLControl, GLInterestPayable, GLInterestPaid, GLTaxAccount, ISNULL(AccrualPostDate,'01/01/1900') AccrualPostDate,
ISNULL(AccruedBalance,0) AccruedBalance, ISNULL(PassDailyAccrualEntry, 'No') PassDailyAccrualEntry, CreditInterestProcedure, 0 AS IsProcessed, Debit321ProductID
from t_Products p
INNER JOIN t_Currencies c on p.OurBranchID = c.OurBranchID AND p.CurrencyID = c.CurrencyID
where ISNULL(IsTermDeposit,'No') = 'No' AND ISNULL(IsFreezed,0) = 0 AND ISNULL(CreditInterestProcedure,'') IN ('002','003','004','321');

insert into t_AccSaveSavingsAccounts
select a.OurBranchID, AccountID, ClientID, Name, ProductID, a.CurrencyID, c.MeanRate,
case when ISNULL(c.CRRounding,0) > ISNULL(c.DBRounding,0) then c.CRRounding else c.DBRounding end AS CcyRounding,
IBAN, Status, OpenDate, LastPayDate, Accrual, AccrualUpto, Accrual, AccrualUpto, 0 AS IsProcessed
from t_Account a
INNER JOIN t_Currencies c on a.OurBranchID = c.OurBranchID AND a.CurrencyID = c.CurrencyID
where ProductID IN (select ProductID from t_AccSaveSavingsProducts) AND a.AuthStatus = '' AND ISNULL(Status,'') NOT IN ('I','C');

select 'Ok' AS ReturnStatus