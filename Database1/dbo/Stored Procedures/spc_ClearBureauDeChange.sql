﻿CREATE PROCEDURE [dbo].[spc_ClearBureauDeChange]      
(       
 @OurBranchID varchar(30),       
 @ScrollNo numeric(18,0),       
 @SupervisorID varchar(30) = 0,    
 @AdditionalData text = ''    
)       
       
AS       
       
 SET NOCOUNT ON       
       
 DECLARE @Status char(1)       
 DECLARE @Supervision char(1),    
 @SellOrBuy char(1),    
 @CurrencyAccountID varchar(30),    
 @CashAccountID varchar(30),    
 @CommissionAccountID varchar(30),    
 @wDate datetime,    
 @DescriptionID varchar(30),    
 @Description varchar(255),    
 @CurrencyID varchar(30),    
 @Amount money,    
 @ForeignAmount money,    
 @ExchangeRate decimal(18,6),    
 @OperatorID varchar(30),    
 @NetAmount money,    
 @Commission money    
     
 SELECT @Status = 'C'      
       
 IF NOT EXISTS (SELECT Supervision FROM t_BureaDeChange WHERE OurBranchID = @OurBranchID and ScrollNo = @ScrollNo)       
 begin      
  SELECT '1' as RetStatus -- Trx. Not Found      
  RETURN      
 end      
     
 -- Get Transaction      
 SELECT @Supervision = Supervision, @SellOrBuy = SellOrBuy, @CurrencyAccountID = CurrAccountID, @CashAccountID = CashAccountID,    
 @CommissionAccountID = CommAccountID, @wDate = wDate, @DescriptionID = DescriptionID, @Description = [Description],     
 @CurrencyID = CurrencyID, @Amount = Amount, @ForeignAmount = ForeignAmount, @ExchangeRate = ExchangeRate,     
 @OperatorID = OperatorID, @NetAmount = NetAmount, @Commission = Commission, @AdditionalData = AdditionalData    
 FROM t_BureaDeChange WHERE (OurBranchID = @OurBranchID) and (ScrollNo = @ScrollNo)       
       
 IF @Supervision = 'R'       
 BEGIN       
  SELECT '2' as RetStatus -- Trx. Already Rejected       
  RETURN       
 END       
       
 IF @Supervision = 'C'       
 BEGIN       
  SELECT '3' as RetStatus -- Trx. Already Cleared      
  RETURN       
 END       
       
 -- Update Transaction File      
 UPDATE t_BureaDeChange      
 SET Supervision = @Status, SupervisorID = @SupervisorID       
 WHERE (OurBranchID = @OurBranchID) and (ScrollNo = @ScrollNo)       
     
 if @Status = 'C'    
 begin    
        
    declare @IsCredit int    
    declare @VoucherID int    
    declare @LocalCurrency varchar(30)    
        
    select @VoucherID = ISNULL(MAX(VoucherID),0) + 1 from t_GLTransactions where OurBranchID = @OurBranchID    
    select @LocalCurrency = ISNULL(LocalCurrency,'') from t_GlobalVariables where OurBranchID = @OurBranchID    
        
    if @SellOrBuy = 'B'      
      set @IsCredit = 1    
    else    
      set @IsCredit = 0    
        
    insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],      
    CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,      
    Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType, GLID)      
        
    values (@OurBranchID, @CurrencyAccountID, @VoucherID, '1', @wDate, convert(varchar(10), @wDate, 120), @DescriptionID,@Description,    
    @CurrencyID, @Amount,@ForeignAmount,@ExchangeRate, @IsCredit, 'Bureau', 'A',    
    @OperatorID, @SupervisorID, '', @ScrollNo, '1', @AdditionalData, '1', 'B', @CashAccountID)    
        
    if @Commission > 0    
    BEGIN    
           
       insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],      
       CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,      
       Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType)      
           
       values (@OurBranchID, @CommissionAccountID, @VoucherID, '2', @wDate, convert(varchar(10), @wDate, 120), @DescriptionID, @Description,    
       @LocalCurrency, @Commission, 0, 1, 1, 'Bureau', 'L', @OperatorID, @SupervisorID, '', @ScrollNo, '2', '', '0', 'B')    
           
    END    
        
    set @VoucherID = @VoucherID + 1    
        
    set @Description = @Description + ' - Scroll # ' + CONVERT(varchar(30),@ScrollNo)    
 set @Description = @Description + ' - Teller Account ID ' + @CashAccountID    
 set @Description = @Description + ' - Foreign Amount ' + CONVERT(varchar(30),@ForeignAmount)    
 set @Description = @Description + ' - Amount ' + CONVERT(varchar(30),@Amount)    
 set @Description = @Description + ' - Commission ' + CONVERT(varchar(30),@Commission)    
 set @Description = @Description + ' - Net Local Amount ' + CONVERT(varchar(30),@NetAmount)    
     
    if @SellOrBuy = 'B'      
    begin    
      set @IsCredit = 0    
      set @DescriptionID = 'BC0'    
      set @NetAmount = @Amount + @Commission    
    end    
    else    
    begin    
      set @IsCredit = 1    
      set @DescriptionID = 'BC1'    
      set @NetAmount = @Amount - @Commission    
    end    
        
    insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],      
    CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,      
    Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType)      
        
    values (@OurBranchID, @CashAccountID, @VoucherID, '1', @wDate, convert(varchar(10), @wDate, 120), @DescriptionID, @Description,    
    @LocalCurrency, @NetAmount, 0, 1, @IsCredit, 'Bureau', 'L', @OperatorID, @SupervisorID, '', @ScrollNo, '1', '', '0', 'B')    
        
 end    
     
 SELECT '0' as RetStatus       
 RETURN