﻿
CREATE PROCEDURE [dbo].[sp_DeleteATMBranchID]
(    
  @OurBranchID varchar(30),
  @BranchID varchar(30)
 )    
AS    
     
   DELETE FROM t_ATM_Branches
   WHERE  OurBranchID = @OurBranchID AND BranchID = @BranchID