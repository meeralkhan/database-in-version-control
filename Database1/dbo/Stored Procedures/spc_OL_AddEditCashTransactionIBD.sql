﻿CREATE PROCEDURE [dbo].[spc_OL_AddEditCashTransactionIBD]
(  
 @ScrollNo    int,  
 @SerialNo    int=1,  
 @CustomerAccountID   varchar(30),  
 @CustomerAccountName   varchar(100),  
 @CustomerProductID   varchar(30),  
 @CustomerCurrencyID   varchar(30),  
 @CustomerAccountType   char(1),  
 @CustomerTrxType    char(1),   
 @CustomerIsLocalCurrency  Char(1),  
 @HOAccountID    varchar(30),  
 @HOAccountName    varchar(100),  
 @HOProductID    varchar(30),  
 @HOCurrencyID    varchar(30),  
 @HOAccountType    char(1),  
 @HOIsLocalCurrency Char(1),  
 @HOTrxType    Char(1),  
 @ValueDate    datetime=null,  
 @wDate     datetime=null,  
 @ChequeID    varchar(30),  
 @ChequeDate    DateTime,  
 @Amount        money,  
 @ForeignAmount    Money,  
 @ExchangeRate    decimal(18,6),  
 @DescriptionID    varchar(30),  
 @Description    varchar(255),  
 @OperatorID    varchar(30),  
 @SupervisorID    varchar(30),  
 @SourceBranch    Varchar(30),  
 @TargetBranch    varchar(30),  
 @Remarks   varchar(1000),  
 @RefNo    varchar(30),  
 @MethodType   Varchar(2) = 'C',  
 @DocumentType   Varchar(2) = '',   
 @InstrumentType varchar(50)='',  
 @IssueDate  datetime='01/01/1900',  
 @InstrumentNo int=0,  
 @ControlNo  int=0,    
 @AppWHTax  Char(1) = 0,  
 @WHTaxAmount money = 0,  
 @WHTaxMode   Varchar(2) ='',  
 @WHTaxAccountID  varchar(30)='',          
 @AddData text=NULL          
)  
  
AS  
 
 SET NOCOUNT ON
 
 DECLARE @IsAccountRestrict as bit  
 DECLARE @TrxLimit as money  
 DECLARE @TrxUtilized as money  
 DECLARE @RestrictTrxType as Char(1)    
  
 DECLARE @ClearBalance   as money  
 DECLARE @FreezeAmount   as money  
 DECLARE @ShadowBalance   as money  
  
 DECLARE @Status   as varchar(30)
 DECLARE @twDate          as datetime  
 DECLARE @tLastEOD  as datetime  
 DECLARE @tLastBOD  as datetime  
  
 DECLARE @IsFreezed as Bit  
 DECLARE @AllowCrDr as Bit  
  
 DECLARE @mDate varchar(50)  
  
 DECLARE @totAmount as money  
 DECLARE @NetAmount as money  
 DECLARE @WHTaxDescription AS Varchar(255)  
 DECLARE @WHTaxCurrencyID AS Varchar(4)  
 
 DECLARE @LocalCurrency nvarchar(30)    
     
 SELECT @Status = '', @totAmount = @Amount, @NetAmount = @Amount  
  
 BEGIN  
  
  select @LocalCurrency = LocalCurrency from t_GlobalVariables WHERE OurBranchID = @TargetBranch      
    
  -- New change - 24 June 2017 - for Foreign Currency  
  if Upper(@LocalCurrency) <> Upper(@CustomerCurrencyID)  
  begin  
     set @totAmount = @ForeignAmount  
  end  
    
  SELECT @twDate = WorkingDate, @tLastEOD = Convert(Varchar(10), LastEOD,101) , @tLastBOD = Convert(Varchar(10), LastBOD,101)   
  FROM t_Last WHERE OurBranchID = @TargetBranch      
  
  IF @twDate = @wDate  
   BEGIN  
    IF @tLastEOD = @tLastBOD  
     BEGIN  
      SELECT 'Error' AS RetStatus, 'EODDone' As RetMessage
      RETURN(0) 
     END       
   END  
  ELSE  
   BEGIN  
    SELECT 'Error' AS RetStatus, 'WorkingDate' As RetMessage
    RETURN(0)
   END  
  
  IF @MethodType = 'LR' AND @InstrumentNo > 0               
  BEGIN  
  
    EXEC @Status = spc_OL_VerifyInstrumentIBD @TargetBranch, @InstrumentType, @IssueDate, @InstrumentNo, @ControlNo, @Amount  
     
    IF @Status <> 0   
    BEGIN  
     select 'Error' AS RetStatus, 'LRMarking' AS RetMessage, @Status AS LRRetStatus        
     RETURN(0)
    END  
      
    UPDATE t_LR_Issuance   
    SET Paid = 1, PaidDate = @wDate, PaidScrollNo = @ScrollNo, PaidMode = @DocumentType,IBCA = @ScrollNo,   
      PaidOperatorID = @OperatorID, PaidSupervisorID = @SupervisorID  
  
    WHERE OurBranchID = @TargetBranch AND InstrumentType = @InstrumentType AND InstrumentNo = @InstrumentNo AND ControlNo = @ControlNo   
      AND (wDate = @IssueDate Or DuplicateDate = @IssueDate)   
      AND Amount = @Amount AND Supervision = 'C'  
  
    SELECT @Status=@@ERROR  
  
    IF @Status <> 0   
    BEGIN  
     select 'Error' AS RetStatus, 'LRMarking' AS RetMessage, '' AS LRRetStatus        
     RETURN(0)        
    END        
            
  END  
  
 SET @Description = @Description + ' --- ' + cast(GETDATE() as varchar(50))  
 SELECT @mDate = Convert(varchar(10),@wDate ,101) + ' ' + convert(varchar(20),GetDate(),114)        
  
 SET @IsAccountRestrict = 1  
 SET @TrxLimit = 0  
 SET @TrxUtilized = 0  
 SET @RestrictTrxType = 'B'
 
 IF @CustomerProductID <> 'GL'  
 BEGIN  
   
   SELECT @IsAccountRestrict = IsAccountRestrict, @TrxLimit = CASE @CustomerTrxType WHEN 'C' THEN DayDepositBal  
          WHEN 'D' THEN DayDrawableBal   
   END   
   From t_OL_GlobalVariables WHERE OurBranchID = @TargetBranch      
      
 END  
 ELSE  
 BEGIN  
   SELECT @IsAccountRestrict = IsGLRestrict, @TrxLimit = CASE @CustomerTrxType WHEN 'C' THEN DayDepositBal  
          WHEN 'D' THEN DayDrawableBal   
   END   
   From t_OL_GlobalVariables WHERE OurBranchID = @TargetBranch      
 END  
  
 IF @IsAccountRestrict = 1  
 BEGIN  
  
   IF (SELECT Count(*) From t_OL_Accounts   
     WHERE OurBranchID = @TargetBranch AND AccountID = @CustomerAccountID AND   
       AccountType = @CustomerAccountType AND (TrxType = @CustomerTrxType OR TrxType = 'B')  ) > 0  
    BEGIN  
     SELECT @TrxLimit = CASE @CustomerTrxType WHEN 'C' THEN IsNull(DayDepositBal,0)  
               WHEN 'D' THEN IsNull(DayDrawableBal,0)  
          END   
     FROM t_OL_Accounts  
     WHERE OurBranchID = @TargetBranch AND AccountID = @CustomerAccountID AND AccountType = @CustomerAccountType  
    END  
   ELSE  
    BEGIN  
     SELECT 'Error' AS RetStatus, 'OnlineTrxNotAllowed' As RetMessage
     RETURN(0)
    END  
  END  
  
 IF LEFT(@DescriptionID,2) <> 'TR'  
  BEGIN  
    SELECT @TrxUtilized = IsNull(Sum(Amount),0)   
    FROM t_OnLineCashTransaction  
    WHERE OurBranchID = @TargetBranch AND AccountID = @CustomerAccountID   
      AND AccountType = @CustomerAccountType AND  
      TrxType = @CustomerTrxType AND Not DescriptionID Like 'TR%'  
    
    IF @TrxLimit < @TrxUtilized + @Amount  
     BEGIN  
      SELECT 'Error' AS RetStatus, 'TrxLimitExceed' As RetMessage
      RETURN(0)
     END  
   END  
  
  DECLARE @AccountClass CHAR(1)
  DECLARE @IsPosting as BIT
  
  IF  @CustomerProductID = 'GL'  
  BEGIN
     
     IF (SELECT Count(AccountID) From t_GL WHERE OurBranchID=@TargetBranch AND AccountID=@CustomerAccountID) > 0
     BEGIN
       
       SELECT @AccountClass = AccountClass, @IsPosting = IsPosting From t_GL
       WHERE OurBranchID=@TargetBranch AND AccountID=@CustomerAccountID
       
       IF UPPER(@AccountClass) <> 'P'
       BEGIN
         SELECT 'Error' AS RetStatus, 'NotAPostingAccount' As RetMessage
         RETURN(0)         
       END
       
       IF @IsPosting = 0
       BEGIN
         SELECT 'Error' AS RetStatus, 'GLPostingNotAllowed' As RetMessage
         RETURN(0)
       END
       
       IF (SELECT Count(*) From t_Products WHERE OurBranchID = @TargetBranch AND 
       (GLControl=@CustomerAccountID OR GLDebitBalance = @CustomerAccountID)) > 0
       BEGIN
         SELECT 'Error' AS RetStatus, 'GLPostingNotAllowedControlAccount' As RetMessage
         RETURN(0)
       END
       
     END
     ELSE
     BEGIN
       SELECT 'Error' AS RetStatus, 'AccountNotFound' As RetMessage
       RETURN(0)
     END
     
  END
  
  IF  @HOProductID = 'GL'  
  BEGIN
     
     IF (SELECT Count(AccountID) From t_GL WHERE OurBranchID=@TargetBranch AND AccountID=@HOAccountID) > 0
     BEGIN
       
       SELECT @AccountClass = AccountClass, @IsPosting = IsPosting From t_GL
       WHERE OurBranchID=@TargetBranch AND AccountID=@HOAccountID
       
       IF UPPER(@AccountClass) <> 'P'
       BEGIN
         SELECT 'Error' AS RetStatus, 'NotAPostingAccount' As RetMessage
         RETURN(0)
       END
       
       IF @IsPosting = 0
       BEGIN
         SELECT 'Error' AS RetStatus, 'GLPostingNotAllowed' As RetMessage
         RETURN(0)
       END
       
       IF (SELECT Count(*) From t_Products WHERE OurBranchID = @TargetBranch AND 
       (GLControl=@CustomerAccountID OR GLDebitBalance = @CustomerAccountID)) > 0
       BEGIN
         SELECT 'Error' AS RetStatus, 'GLPostingNotAllowedControlAccount' As RetMessage
      RETURN(0)
       END
       
     END
     ELSE
     BEGIN
       SELECT 'Error' AS RetStatus, 'AccountNotFound' As RetMessage
       RETURN(0)
     END    
     
  END
  
  IF @HOProductID <> 'GL'  
  BEGIN  
    
    IF (SELECT Count(AccountID) From t_Account WHERE OurBranchID=@TargetBranch AND AccountID=@HOAccountID) > 0
    BEGIN
      
      SELECT @HOProductID = ProductID, @Status = ISNULL(Status,''), @AllowCrDr = Case @CustomerTrxType
               WHEN 'C' THEN AllowCreditTransaction  
               WHEN 'D' THEN AllowDebitTransaction  
             END
      FROM t_Account
      WHERE OurBranchID=@TargetBranch AND AccountID=@HOAccountID          
      
      IF @Status = 'C'
      BEGIN
         SELECT 'Error' AS RetStatus, 'AccountClosed' As RetMessage
         RETURN(0)
      END
      
      IF @Status = 'D'
      BEGIN
         SELECT 'Error' AS RetStatus, 'AccountBlocked' As RetMessage
         RETURN(0)
      END
      
      IF @Status = 'T'
      BEGIN
         SELECT 'Error' AS RetStatus, 'AccountDormant' As RetMessage
         RETURN(0)
      END
      
      IF @Status = 'X'
      BEGIN
         SELECT 'Error' AS RetStatus, 'AccountDeceased' As RetMessage
         RETURN(0)
      END
      
      IF @AllowCrDr = 1
      BEGIN  
         SELECT 'Error' AS RetStatus, 'SPTrxNotAllowed' As RetMessage
         RETURN(0)
      END
      
      SELECT @IsFreezed=IsFreezed, @AllowCrDr = Case @CustomerTrxType
               WHEN 'C' THEN AllowCredit
               WHEN 'D' THEN AllowDebit
     END
      FROM t_Products
      WHERE OurBranchID = @TargetBranch AND ProductID = @HOProductID
      
      IF @IsFreezed = 1
      BEGIN
         SELECT 'Error' AS RetStatus, 'ProductFreezed' As RetMessage
         RETURN(0)
      END
      
      IF @AllowCrDr = 0
      BEGIN
         SELECT 'Error' AS RetStatus, 'ProductTrxNotAllowed' As RetMessage
         RETURN(0)
      END
      
    END
    ELSE
    BEGIN
      SELECT 'Error' AS RetStatus, 'InvalidCustomer' As RetMessage
      RETURN(0)
    END
  END
  
  IF  @CustomerProductID <> 'GL'  
  BEGIN  
    
    IF (SELECT Count(AccountID) From t_Account WHERE OurBranchID=@TargetBranch AND AccountID=@CustomerAccountID) > 0
    BEGIN
      
      SELECT @IsFreezed=IsFreezed, @AllowCrDr = Case @CustomerTrxType
               WHEN 'C' THEN AllowCredit
               WHEN 'D' THEN AllowDebit
             END
      FROM t_Products
      WHERE OurBranchID = @TargetBranch AND ProductID = @CustomerProductID
      
      IF @IsFreezed = 1
      BEGIN
         SELECT 'Error' AS RetStatus, 'ProductFreezed' As RetMessage
         RETURN(0)
      END
      
      IF @AllowCrDr = 0
      BEGIN
         SELECT 'Error' AS RetStatus, 'ProductTrxNotAllowed' As RetMessage              
         RETURN(0)
      END
      
      SELECT @Status = ISNULL(Status,''), @AllowCrDr = Case @CustomerTrxType
               WHEN 'C' THEN AllowCreditTransaction  
               WHEN 'D' THEN AllowDebitTransaction  
             END
      FROM t_Account
      WHERE OurBranchID=@TargetBranch AND AccountID=@CustomerAccountID
      
      IF @Status = 'C'
      BEGIN
         SELECT 'Error' AS RetStatus, 'AccountClosed' As RetMessage
         RETURN(0)
      END
      
      IF @Status = 'D'
      BEGIN
         SELECT 'Error' AS RetStatus, 'AccountBlocked' As RetMessage
         RETURN(0)
   END
      
      IF @Status = 'T'
      BEGIN
         SELECT 'Error' AS RetStatus, 'AccountDormant' As RetMessage
         RETURN(0)
      END
      
      IF @Status = 'X'
      BEGIN
         SELECT 'Error' AS RetStatus, 'AccountDeceased' As RetMessage
         RETURN(0)
      END
      
      IF @AllowCrDr = 1
      BEGIN  
         SELECT 'Error' AS RetStatus, 'SPTrxNotAllowed' As RetMessage
         RETURN(0)
      END
      
    END
    ELSE
    BEGIN  SELECT 'Error' AS RetStatus, 'InvalidCustomer' As RetMessage
      RETURN(0)
    END
    
-------------------------------------------------------     
-----------------------------------------------------    
    -- Condition verify Only customer debit transaction  
    IF @CustomerTrxType='D'  
    BEGIN  
      
      SELECT @ClearBalance=isNull(ClearBalance-FreezeAmount,0),  
       @FreezeAmount=isNull(FreezeAmount,0), @ShadowBalance=isNull(ShadowBalance,0)    
    
      FROM t_AccountBalance  
      WHERE OurBranchID=@TargetBranch AND AccountID=@CustomerAccountID
      
--------------------------------------------------------------  
-----------------W/H Tax Deduction  
      IF @AppWHTax = 1 AND LEFT(@DescriptionID,2) <> 'TR'  
      BEGIN  
        IF @WHTaxMode = 'T'  
         SET @totAmount = @Amount + @WHTaxAmount  
        ELSE  
         SET @NetAmount = @Amount - @WHTaxAmount   
          
        SELECT @WHTaxDescription = '' , @WHTaxCurrencyID = ''  
  
        SELECT   @WHTaxDescription = Description , @WHTaxCurrencyID = CurrencyID   
        From t_GL  
        WHERE OurBranchID = @TargetBranch AND AccountID = @WHTaxAccountID  
  
        IF @WHTaxAccountID = '' OR @WHTaxDescription = ''  
         BEGIN  
          SELECT 'Error' AS RetStatus, 'WHTaxNotDefine' As RetMessage
          RETURN(0)
         END  

        IF UPPER(@WHTaxCurrencyID) <> UPPER(@LocalCurrency)
        BEGIN  
          SELECT 'Error' AS RetStatus, 'WHTaxNotLocalCurrency' As RetMessage
          RETURN(0)
        END   
       END  
  
--------------------------------------------------------------  
--------------------------------------------------------------  
        
      IF @totAmount > @ClearBalance  
    
       BEGIN  
        SELECT 'Error' AS RetStatus, 'Balances' As RetMessage, @ClearBalance+@FreezeAmount as ClearBalance, 
        @FreezeAmount as FreezeAmount, @ShadowBalance as ShadowBalance
        RETURN(0)
       END  
      
      IF UPPER(@ChequeID) <> 'V'   
      BEGIN  
        -- Get Validate Cheaque No  
        EXEC @Status = spc_ValidateChequeNo @TargetBranch, @CustomerAccountID, @ChequeID, @ChequeID  
    
        IF @Status <> 0  
        BEGIN        
          SELECT 'Error' AS RetStatus, 'ChequeFails'+@Status As RetMessage            
          RETURN(0)
        END  
      
        INSERT INTO t_ChequePaid              
   (OurBranchID, AccountID, ChequeID, Date, AccountType, CreateBy, CreateTime, CreateTerminal, SuperviseBy)              
        VALUES              
        (@TargetBranch,@CustomerAccountID, @ChequeID,@wDate,'C',@OperatorID,@wDate,'',@SupervisorID)              
      
      END  
    
     END  
   END         
  
  INSERT INTO t_OnLineCashTransaction  
   (  
    ScrollNo,SerialNo,OurBranchID,AccountID,AccountName,ProductID,CurrencyID,AccountType,ValueDate,wDate,TrxType,  
    ChequeID,ChequeDate, Amount, ForeignAmount,ExchangeRate, DescriptionID,Description,Supervision,  
    OperatorID,SupervisorID,IsLocalCurrency,SourceBranch, TargetBranch,Remarks,RefNo,AdditionalData          
   )    
  VALUES  
   (  
    @ScrollNo,@SerialNo,@TargetBranch,@CustomerAccountID,@CustomerAccountName,@CustomerProductID,  
    @CustomerCurrencyID,@CustomerAccountType,@ValueDate,@mDate,@CustomerTrxType,@ChequeID,@ChequeDate,  
    @Amount, @ForeignAmount,@ExchangeRate, @DescriptionID,@Description,'C', @OperatorID,@SupervisorID,  
    @CustomerIsLocalCurrency,@SourceBranch, @TargetBranch,@Remarks,@RefNo,@AddData          
   )  
  
  SELECT @Status=@@ERROR  
  
  IF @Status <> 0   
   BEGIN  
    SELECT 'Error' AS RetStatus, 'ChkStatus' As RetMessage, @Status AS Status
    RETURN(0)
   END    
      
  declare @IsCredit int    
  declare @VoucherID int    
  declare @TransactionMethod char(1)    
  declare @CsGLControl varchar(30)    
  declare @HOGLControl varchar(30)    
  declare @DescID varchar(30)    
  declare @Desc varchar(255)    
      
  select @VoucherID = ISNULL(MAX(VoucherID),0) + 1 from t_GLTransactions where OurBranchID = @TargetBranch    
      
  if @CustomerTrxType = 'C'    
    set @IsCredit = 1    
  else    
    set @IsCredit = 0    
      
  if @CustomerIsLocalCurrency = 1      
  begin      
    set @ForeignAmount = 0      
    set @ExchangeRate = 1      
    set @TransactionMethod = 'L'      
  end      
  else      
    set @TransactionMethod = 'A'    
      
  if @CustomerAccountType = 'C'    
  begin    
         
     insert into t_Transactions (OurBranchID, ScrollNo, SerialNo, RefNo, wDate, AccountType, DocumentType, AccountID, AccountName,    
     ProductID, CurrencyID, ValueDate, TrxType, ChequeID, ChequeDate, Amount, ForeignAmount, ExchangeRate, ProfitLoss, MeanRate,    
     DescriptionID, [Description], BankCode, BranchCode, TrxPrinted, [Status], IsLocalCurrency, OperatorID, SupervisorID,    
     AdditionalData, Remarks, IsMainTrx, DocType, GLVoucherID, AppWHTax, WHTaxMode, WHTaxAmount, WHTScrollNo, SourceBranch, TargetBranch)    
         
     VALUES (@TargetBranch,@ScrollNo,@SerialNo,@Refno,@mDate,@CustomerAccountType,'OC',@CustomerAccountID,@CustomerAccountName,    
     @CustomerProductID,@CustomerCurrencyID, @ValueDate,@CustomerTrxType,@ChequeID,@ChequeDate, @Amount, @ForeignAmount,@ExchangeRate,    
     0,@ExchangeRate,@DescriptionID,@Description,'','', 0, 'C', @CustomerIsLocalCurrency, @OperatorID, @SupervisorID,    
     @AddData, @Remarks,'1','OC', @VoucherID, @AppWHTax, @WHTaxMode, @WHTaxAmount, @ScrollNo,@SourceBranch, @TargetBranch)    
         
     select @CsGLControl = ISNULL(GLControl, '') from t_Products where OurBranchID = @TargetBranch and ProductID = @CustomerProductID    
         
     if @CustomerTrxType = 'C'    
     begin    
       set @DescID = 'OL0'    
       set @Desc = 'Account ID: ' + @CustomerAccountID +', Account Name: ' + @CustomerAccountName +' , Module: Online-Cash Credit Entry...'    
           
       update t_Account set CashTotCr = CashTotCr + abs(@Amount), CashTotCrF = CashTotCrF + abs(@ForeignAmount)    
       where OurBranchID = @TargetBranch and AccountID = @CustomerAccountID    
           
     end    
     else    
     begin    
       set @DescID = 'OL1'    
       set @Desc = 'Account ID: ' + @CustomerAccountID +', Account Name: ' + @CustomerAccountName +' , Module: Online-Cash Debit Entry...'    
           
       update t_Account set CashTotDr = CashTotDr + abs(@Amount), CashTotDrF = CashTotDrF + abs(@ForeignAmount)    
       where OurBranchID = @TargetBranch and AccountID = @CustomerAccountID    
           
     end    
         
     insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],      
     CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,      
     Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType, SourceBranch, TargetBranch)      
         
     values (@TargetBranch, @CsGLControl, @VoucherID, '1', @mDate, @ValueDate, @DescID, @Desc,      
     @CustomerCurrencyID, @Amount, @ForeignAmount, @ExchangeRate, @IsCredit, 'OnLineCash', @TransactionMethod,    
     @OperatorID, @SupervisorID, '', '0', '0', '', '0', 'OC',@SourceBranch, @TargetBranch)      
         
  end    
  else    
  begin    
         
     insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],      
     CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,      
     Indicator, ScrollNo, SerialNo, AdditionalData, Remarks, IsMainTrx, DocType, SourceBranch, TargetBranch)      
         
     values (@TargetBranch, @CustomerAccountID, @VoucherID, '1', @mDate, @ValueDate, @DescriptionID, @Description,      
     @CustomerCurrencyID, @Amount, @ForeignAmount, @ExchangeRate, @IsCredit, 'OnLineCash', @TransactionMethod,    
     @OperatorID, @SupervisorID,'', @ScrollNo, @SerialNo, @AddData, @Remarks, '1', 'OC',@SourceBranch, @TargetBranch)    
         
  end    
      
  SET @SerialNo=@SerialNo+ 1  
      
  INSERT INTO t_OnLineCashTransaction  
   (  
    ScrollNo,SerialNo,OurBranchID,AccountID,AccountName,ProductID,CurrencyID,AccountType,ValueDate,wDate,TrxType,  
    ChequeID,ChequeDate, Amount, ForeignAmount,ExchangeRate, DescriptionID,Description,Supervision,  
    OperatorID,SupervisorID,IsLocalCurrency,SourceBranch, TargetBranch,Remarks,RefNo,AdditionalData 
   )    
  VALUES  
   (  
    @ScrollNo,@SerialNo,@TargetBranch,@HOAccountID,@HOAccountName,@HOProductID,  
    @HOCurrencyID,@HOAccountType,@ValueDate,@mDate,@HOTrxType,@ChequeID,@ChequeDate,  
    @NetAmount, @ForeignAmount,@ExchangeRate, @DescriptionID,@Description,'C', @OperatorID,@SupervisorID,  
    @HOIsLocalCurrency,@SourceBranch, @TargetBranch,@Remarks,@RefNo,@AddData          
   )  
  
  SELECT @Status=@@ERROR  
  
  IF @Status <> 0   
   BEGIN  
    SELECT 'Error' AS RetStatus, 'ChkStatus' As RetMessage, @Status AS Status
    RETURN(0)
   END    
      
  if @HOTrxType = 'C'    
    set @IsCredit = 1    
  else    
    set @IsCredit = 0    
      
  if @HOIsLocalCurrency = 1      
  begin      
    set @ForeignAmount = 0      
    set @ExchangeRate = 1      
    set @TransactionMethod = 'L'      
  end      
  else      
    set @TransactionMethod = 'A'    
      
  set @VoucherID = @VoucherID + 1    
      
  if @HOAccountType = 'C'    
  begin    
         
     insert into t_Transactions (OurBranchID, ScrollNo, SerialNo, RefNo, wDate, AccountType, DocumentType, AccountID, AccountName,    
     ProductID, CurrencyID, ValueDate, TrxType, ChequeID, ChequeDate, Amount, ForeignAmount, ExchangeRate, ProfitLoss, MeanRate,    
     DescriptionID, [Description], BankCode, BranchCode, TrxPrinted, [Status], IsLocalCurrency, OperatorID, SupervisorID,    
     AdditionalData, Remarks, IsMainTrx, DocType, GLVoucherID, SourceBranch, TargetBranch)    
         
     VALUES (@TargetBranch,@ScrollNo,@SerialNo,@Refno,@mDate,@HOAccountType,'OC',@HOAccountID,@HOAccountName,    
     @HOProductID,@HOCurrencyID, @ValueDate,@HOTrxType,@ChequeID,@ChequeDate, @NetAmount, @ForeignAmount,@ExchangeRate,    
     0,@ExchangeRate,@DescriptionID,@Description,'','', 0, 'C', @HOIsLocalCurrency, @OperatorID, @SupervisorID,    
     @AddData, @Remarks,'1','OC', @VoucherID, @SourceBranch, @TargetBranch)    
         
     select @HOGLControl = ISNULL(GLControl, '') from t_Products where OurBranchID = @TargetBranch and ProductID = @HOProductID    
         
     if @HOTrxType = 'C'    
     begin    
       set @DescID = 'OL0'    
       set @Desc = 'Account ID: ' + @HOAccountID +', Account Name: ' + @HOAccountName +' , Module: Online-Cash Credit Entry...'    
     end    
     else    
     begin    
       set @DescID = 'OL1'    
       set @Desc = 'Account ID: ' + @HOAccountID +', Account Name: ' + @HOAccountName +' , Module: Online-Cash Debit Entry...'    
     end    
         
     insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],      
     CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,      
     Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType, SourceBranch, TargetBranch)      
         
     values (@TargetBranch, @HOGLControl, @VoucherID, '1', @mDate, @ValueDate, @DescID, @Desc,      
     @HOCurrencyID, @NetAmount, @ForeignAmount, @ExchangeRate, @IsCredit, 'OnLineCash', @TransactionMethod,    
     @OperatorID, @SupervisorID, '', '0', '0', '', '0', 'OC', @SourceBranch, @TargetBranch)      
         
  end    
  else    
  begin    
         
     insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],      
     CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,      
     Indicator, ScrollNo, SerialNo, AdditionalData, Remarks, IsMainTrx, DocType, SourceBranch, TargetBranch)      
         
     values (@TargetBranch, @HOAccountID, @VoucherID, '1', @mDate, @ValueDate, @DescriptionID, @Description,      
     @HOCurrencyID, @NetAmount, @ForeignAmount, @ExchangeRate, @IsCredit, 'OnLineCash', @TransactionMethod,    
     @OperatorID, @SupervisorID,'', @ScrollNo, @SerialNo, @AddData, @Remarks, '1', 'OC',@SourceBranch, @TargetBranch)    
         
  end    
      
  set @Description = 'Withholding Tax deduction (Withdrawl) On Amount: ' + Convert(Varchar(18), @Amount)    
  set @Description = @Description + ' AND ChequeNo: ' + Convert(Varchar(30), @ChequeID)    
      
  ----------------------------------------------------------------------------------------------------------------------------  
--------------------------- WHTax Entry  
  IF @CustomerTrxType='D' AND @AppWHTax = 1 AND LEFT(@DescriptionID,2) <> 'TR'  
   BEGIN  
    IF @WHTaxMode = 'T'  
     BEGIN  
          
      set @VoucherID = @VoucherID + 1    
      set @SerialNo = @SerialNo+ 1    
          
      INSERT INTO t_OnLineCashTransaction  
      (  
        ScrollNo,SerialNo,OurBranchID,AccountID,AccountName,ProductID,CurrencyID,AccountType,ValueDate,wDate,TrxType,  
        ChequeID,ChequeDate, Amount, ForeignAmount,ExchangeRate, DescriptionID,Description,Supervision,  
        OperatorID,SupervisorID,IsLocalCurrency,SourceBranch, TargetBranch,Remarks,RefNo  
       )    
      VALUES  
       (  
        @ScrollNo,@SerialNo,@TargetBranch,@CustomerAccountID,@CustomerAccountName,@CustomerProductID,  
        @CustomerCurrencyID,@CustomerAccountType,@ValueDate,@mDate,@CustomerTrxType,'V', @ValueDate,  
        @WHTaxAmount, 0,1, 'WH1', @Description,'C', @OperatorID,@SupervisorID,  
        1, @SourceBranch, @TargetBranch, @Remarks + ' (Withholding Tax deduction)' , @RefNo  
       )  
      
      SELECT @Status=@@ERROR  
      
      IF @Status <> 0   
       BEGIN  
        SELECT 'Error' AS RetStatus, 'ChkStatus' As RetMessage, @Status AS Status
        RETURN(0)
       END    
           
       --WHTax Debit    
           
       if @CustomerIsLocalCurrency = 1      
       begin      
         set @ForeignAmount = 0      
         set @ExchangeRate = 1      
         set @TransactionMethod = 'L'      
       end      
       else      
         set @TransactionMethod = 'A'    
           
       insert into t_Transactions (OurBranchID, ScrollNo, SerialNo, RefNo, wDate, AccountType, DocumentType, AccountID, AccountName,    
       ProductID, CurrencyID, ValueDate, TrxType, ChequeID, ChequeDate, Amount, ForeignAmount, ExchangeRate, ProfitLoss, MeanRate,    
       DescriptionID, [Description], BankCode, BranchCode, TrxPrinted, [Status], IsLocalCurrency, OperatorID, SupervisorID,    
       AdditionalData, Remarks, IsMainTrx, DocType, GLVoucherID, SourceBranch, TargetBranch)    
           
       VALUES (@TargetBranch,@ScrollNo,@SerialNo,('WHTax-'+convert(varchar(20),@ScrollNo)),@mDate,@CustomerAccountType,'OC',@CustomerAccountID,@CustomerAccountName,    
       @CustomerProductID,@CustomerCurrencyID, @ValueDate,@CustomerTrxType,'V',@ChequeDate, @WHTaxAmount, 0, 1,    
       0, 1, 'WH1', @Description, '','', 0, 'C', @CustomerIsLocalCurrency, @OperatorID, @SupervisorID,    
       '', @Remarks, '1', 'OC', @VoucherID, @SourceBranch, @TargetBranch)    
           
       set @IsCredit = 0    
       set @DescID = 'OL1'    
       set @Desc = 'Account ID: ' + @CustomerAccountID +', Account Name: ' + @CustomerAccountName +' , Module: Online-Cash Debit Entry...'    
           
       insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],      
       CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,      
       Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType, SourceBranch, TargetBranch)      
           
       values (@TargetBranch, @CsGLControl, @VoucherID, '1', @mDate, @ValueDate, @DescID, @Desc,      
       @CustomerCurrencyID, @WHTaxAmount, 0, 1, @IsCredit, 'OnLineCash', @TransactionMethod,    
       @OperatorID, @SupervisorID, '', '0', '0', '', '0', 'OC', @SourceBranch, @TargetBranch)      
           
     END  
  
        SELECT @Description = 'Being Withholding Tax deduction (Withdrawl) On Account#: ' + @CustomerAccountID + ', Title: '   
        + @CustomerAccountName + ', Amount: ' + Convert(Varchar(18), @Amount) + ' AND ChequeNo: ' + Convert(Varchar(11), @ChequeID)  
       , @SerialNo=@SerialNo+1  
  
    INSERT INTO t_OnLineCashTransaction  
     (  
      ScrollNo,SerialNo,OurBranchID,AccountID,AccountName,ProductID,CurrencyID,AccountType,ValueDate,wDate,TrxType,  
      ChequeID,ChequeDate, Amount, ForeignAmount,ExchangeRate, DescriptionID,Description,Supervision,  
      OperatorID,SupervisorID,IsLocalCurrency,SourceBranch, TargetBranch,Remarks,RefNo  
     )    
    VALUES  
     (  
      @ScrollNo,@SerialNo,@TargetBranch,@WHTaxAccountID, @WHTaxDescription,'GL',  
      @WHTaxCurrencyID, 'G', @ValueDate,@mDate,'C', 'V',@ValueDate,  
      @WHTaxAmount, 0,1, 'WH1',@Description,'C', @OperatorID,@SupervisorID,  
      1,@SourceBranch, @TargetBranch,@Remarks + ' (Withholding Tax deduction)' ,@RefNo  
     )   
  
  
    SELECT @Status=@@ERROR  
    
    IF @Status <> 0   
     BEGIN  
      SELECT 'Error' AS RetStatus, 'ChkStatus' As RetMessage, @Status AS Status
      RETURN(0)
     END  
        
    set @VoucherID = @VoucherID + 1    
        
    insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],      
    CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,      
    Indicator, ScrollNo, SerialNo, AdditionalData, Remarks, IsMainTrx, DocType, SourceBranch, TargetBranch)      
        
    values (@TargetBranch, @WHTaxAccountID, @VoucherID, '1', @mDate, @ValueDate, 'WH1', @Description,      
    @WHTaxCurrencyID, @WHTaxAmount, 0, 1, 1, 'OnLineCash', @TransactionMethod,    
    @OperatorID, @SupervisorID, '', @ScrollNo, @SerialNo, '', @Remarks, '1', 'OC', @SourceBranch, @TargetBranch)      
        
   END  
--------------------------- WHTax Entry  
----------------------------------------------------------------------------------------------------------------------------  
  
  SELECT 'Ok' AS RetStatus, 'TrxPosted' As RetMessage
  RETURN(0)
 END