﻿CREATE procedure [dbo].[spc_RecSaveODProducts]      
AS          
truncate table t_RecSaveODProducts;        
truncate table t_RecSaveODAccounts;        
        
insert into t_RecSaveODProducts      
select p.OurBranchID, ProductID, p.Description, p.CurrencyID, DebitInterestProcedure, DebitInterestStart, DebitRounding,        
case when ISNULL(c.CRRounding,0) > ISNULL(c.DBRounding,0) then c.CRRounding else c.DBRounding end AS CcyRounding,        
DebitInterestDays, PassDailyAccrualEntry, GLInterestReceivable, GLInterestReceived, FinanceProductType, ReceivableProductID,         
CapitalizedProductID, GlobalRate, GLServiceCharges, GLSuspendedInterest, AdvRateBehaviour, GLBadDebt, GLProvisioning, GLProvisionHeld,         
ISNULL(DbAccruedBalance,0) AccruedBalance, ISNULL(DbAccrualPostDate,'01/01/1900') AccrualPostDate, 0 AS IsProcessed        
from t_Products p        
INNER JOIN t_Currencies c on p.OurBranchID = c.OurBranchID AND p.CurrencyID = c.CurrencyID        
where ISNULL(p.IsAdvancesProduct,'') = 'Yes' AND ISNULL(p.IsFreezed,0) = 0        
AND ISNULL(p.FinanceProductType, '') = 'OD' AND ISNULL(p.DebitInterestProcedure,'') <> ''        
AND ISNULL(p.CalculateAccrual, 'Yes') = 'Yes';        
        
insert into t_RecSaveODAccounts        
select a.OurBranchID, AccountID, Name, ClientID, ProductID, a.CurrencyID, c.MeanRate,        
case when ISNULL(c.CRRounding,0) > ISNULL(c.DBRounding,0) then c.CRRounding else c.DBRounding end AS CcyRounding,        
IBAN, Status, OpenDate, DbAccrual, DbAccrualUpto, DbtAccrual, 0 AS IsProcessed        
from t_Account a        
INNER JOIN t_Currencies c on a.OurBranchID = c.OurBranchID AND a.CurrencyID = c.CurrencyID        
where ProductID IN (select ProductID from t_RecSaveODProducts) AND a.AuthStatus = '' AND ISNULL(Status,'') NOT IN ('C','I');      
        
select 'Ok' AS ReturnStatus