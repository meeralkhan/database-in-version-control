﻿  
CREATE PROCEDURE [dbo].[spplus_GetATMBankIMDs] (
 @OurBranchId varchar(30),
 @BankIMD varchar(30)='',  
 @ODLimitAllow bit=0  
)  
 AS    
 if @BankIMD <> ''    
  begin    
   if exists ( SELECT BankShortName FROM t_ATM_Banks WHERE OurBranchID = @OurBranchID AND BankIMD = @BankIMD )  
    
    select *, 1 as ReturnValue from t_ATM_Banks  
    Where OurBranchID = @OurBranchID AND BankIMD = @BankIMD
      
   else    
   
    select 0 as ReturnValue    
    
  end    
    
 else    
  begin    
   select BankIMD, OurBranchID, BankShortName, BankName, ODLimitAllow From t_ATM_Banks  
  end