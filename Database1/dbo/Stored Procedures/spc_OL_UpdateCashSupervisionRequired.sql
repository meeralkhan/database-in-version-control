﻿
CREATE PROCEDURE [dbo].[spc_OL_UpdateCashSupervisionRequired]
(  
 @OurBranchID varchar(30),  
 @ScrollNo int,  
 @Supervision char(1),  
 @SupervisorID varchar(30)
)  
  
AS  
  
 set nocount on  
  
 BEGIN  
  
  UPDATE t_OnLineCashSupervisionRequired
  SET Supervision=@Supervision, SupervisorID=@SupervisorID     
  WHERE OurBranchID = @OurBranchID AND ScrollNo    = @ScrollNo  
    
  if @Supervision = 'R'   
  BEGIN  
    
    UPDATE t_OL_TransactionStatus
    SET Status=@Supervision, OperatorID=@SupervisorID     
    WHERE OurBranchID = @OurBranchID AND ScrollNo = @ScrollNo  
    
  END  
   
 END  
 
 SELECT 'Ok' AS RetStatus, 'SavedSuccesfully' AS RetMessage