﻿
CREATE PROCEDURE [dbo].[spc_AddEditCashBalanceBook] (    
 @OurBranchID varchar(30),        
 @CurrencyID varchar(30),        
 @Date DateTime,        
 @OpeningBalance money=0,        
 @Receipts money=0,        
 @NoOfReceipts int=0,        
 @Payments money=0,        
 @NoOfPayments int=0,    
 @N10000 int=0,      
 @N5000 int=0,        
 @N1000 int=0,        
 @N500 int=0,        
 @N200 int=0,        
 @N100 int=0,        
 @N50 int=0,        
 @N20 int=0,        
 @N10 int=0,        
 @N5 int=0,        
 @N2 int=0,        
 @N1 int=0,        
 @C5 int=0,        
 @C2 int=0,        
 @C1 int=0,        
 @CPoint5 int=0,        
 @CPoint25 int=0,        
 @CPoint10 int=0,    
 @TotalNotes money=0,    
 @TotalCoins money=0,    
 @ClosingBalance money=0,    
 @frmName varchar(100),    
 @CreateBy varchar(30),    
 @CreateTerminal varchar(30),    
 @CreateDate datetime    
)    
 AS        
    
set nocount on     
    
Declare @AmtN10000 money    
Declare @AmtN5000 money    
Declare @AmtN1000 money    
Declare @AmtN500 money    
Declare @AmtN200 money    
Declare @AmtN100 money    
Declare @AmtN50  money    
Declare @AmtN20  money    
Declare @AmtN10  money    
Declare @AmtN5   money    
Declare @AmtN2   money    
Declare @AmtN1   money    
    
Declare @AmtC5  money    
Declare @AmtC2  money    
Declare @AmtC1  money    
Declare @AmtCP5  money    
Declare @AmtCP25 money    
Declare @AmtCP10 money    
    
    
    
    
 Delete From t_CashDenomination        
 Where (OurBranchID = @OurBranchID) and (CurrencyID = @CurrencyID) and (wDate = @Date)        
     
 set @AmtN10000  = @N10000 * 10000    
 set @AmtN5000  = @N5000 * 5000    
 set @AmtN1000  = @N1000 * 1000    
 set @AmtN500  = @N500  * 500    
 set @AmtN200  = @N200  * 200    
 set @AmtN100  = @N100  * 100    
 set @AmtN50   = @N50  * 50    
 set @AmtN20   = @N20  * 20    
 set @AmtN10   = @N10  * 10    
 set @AmtN5   = @N5  * 5    
 set @AmtN2   = @N2  * 2    
 set @AmtN1   = @N1  * 1    
     
 set @AmtC5   = @C5  * 5    
 set @AmtC2   = @C2  * 2    
 set @AmtC1   = @C1  * 1    
 set @AmtCP5  = @CPoint5 * 0.50    
 set @AmtCP25  = @CPoint25 * 0.25    
 set @AmtCP10  = @CPoint10 * 0.10    
     
        
 INSERT INTO t_CashDenomination (    
 CreateBy,CreateTime,CreateTerminal,AuthStatus,OurBranchID,CurrencyID,wDate,OpeningBalance,Receipts,    
 Payments,NoOfReceipts,NoOfPayments,N10000,N5000,N1000,N500,N200,N100,N50,N20,N10,N5,N2,N1,    
 AmtN10000,AmtN5000,AmtN1000,AmtN500,AmtN200,AmtN100,AmtN50,AmtN20,AmtN10,AmtN5,AmtN2,AmtN1,    
 C5,C2,C1,CP5,CP25,CP10,    
 AmtC5,AmtC2,AmtC1,AmtCP5,AmtCP25,AmtCP10,    
 TotalNotes,TotalCoins,ClosingBalance,frmName    
 )    
 Values (    
 @CreateBy, @CreateDate, @CreateTerminal, '', @OurBranchID, @CurrencyID, @Date, @OpeningBalance, @Receipts,    
 @Payments, @NoOfReceipts, @NoOfPayments, @N10000, @N5000, @N1000, @N500, @N200, @N100, @N50, @N20, @N10, @N5,     
 @N2, @N1,     
 @AmtN10000,@AmtN5000,@AmtN1000,@AmtN500,@AmtN200,@AmtN100,@AmtN50,@AmtN20,@AmtN10,@AmtN5,@AmtN2,@AmtN1,    
 @C5, @C2, @C1, @CPoint5,@CPoint25,@CPoint10,     
 @AmtC5,@AmtC2,@AmtC1,@AmtCP5,@AmtCP25,@AmtCP10,    
 @TotalNotes,@TotalCoins,@ClosingBalance,@frmName    
 )    
     
 select 'Ok' as ReturnStatus, 'SavedSuccessfully' as ReturnMessage