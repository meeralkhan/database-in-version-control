﻿CREATE PROCEDURE [dbo].[sp_ATM_GetAccountBalance]
(
	@OurBranchID	Varchar(30),
	@AccountID	Varchar(30)
)

AS

	BEGIN

		SELECT ROUND((B.ClearBalance + B.Effects),2) as AccountBAlance
		FROM t_Account as A, t_AccountBalance as B, t_Products as P
		WHERE A.OurBranchID=P.OurBranchID AND A.ProductID=P.ProductID
			AND A.OurBranchID=B.OurBranchID AND A.ProductID=B.ProductID
			AND A.AccountID=B.AccountID
			AND A.OurBranchID=@OurBranchID AND A.AccountID=@AccountID

	END