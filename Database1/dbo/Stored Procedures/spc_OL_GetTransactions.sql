﻿CREATE PROCEDURE [dbo].[spc_OL_GetTransactions]      
(      
  @OurBranchID Varchar(30),      
  @Status Char(1)=''      
)      
      
AS      
      
SET NOCOUNT ON      
      
Declare @WorkingDate as DateTime      
      
Select @WorkingDate = WorkingDate From t_Last Where OurBranchId = @OurBranchId     
      
IF @Status = ''      
BEGIN      
    
  Select a.*, b.ChequeID, b.ChequeDate, b.RefNo as IBCADA, b.TargetBranch, c.BranchName As TargetBranchName      
  From t_OL_TransactionStatus a      
  INNER Join t_OnlineCashSupervisionRequired b ON a.OurBranchID = b.OurBranchID AND a.ScrollNo = b.ScrollNo      
  INNER Join t_OnlineBranches c ON a.OurBranchID = c.OurBranchID AND c.BranchID = b.TargetBranch      
  Where a.OurBranchId = @OurBranchId And convert(varchar(10), b.wDate, 101) = @WorkingDate      
  Order By a.ScrollNo      
        
END      
ELSE IF @Status = 'S'      
BEGIN      
        
  Select a.*, b.ChequeID, b.ChequeDate, b.RefNo as IBCADA, b.TargetBranch, c.BranchName As TargetBranchName      
  From t_OL_TransactionStatus a      
  INNER Join t_OnlineCashSupervisionRequired b ON a.OurBranchID = b.OurBranchID AND a.ScrollNo = b.ScrollNo      
  INNER Join t_OnlineBranches c ON a.OurBranchID = c.OurBranchID AND c.BranchID = b.TargetBranch      
  Where a.OurBranchId = @OurBranchId And a.Status = 'C' And a.Supervised = '0' And convert(varchar(10), b.wDate, 101) = @WorkingDate      
  Order By a.ScrollNo      
        
END      
ELSE IF @Status = 'C'      
BEGIN      
        
  Select a.*, b.ChequeID, b.ChequeDate, b.RefNo as IBCADA, b.TargetBranch, c.BranchName As TargetBranchName      
  From t_OL_TransactionStatus a      
  INNER Join t_OnlineCashSupervisionRequired b ON a.OurBranchID = b.OurBranchID AND a.ScrollNo = b.ScrollNo      
  INNER Join t_OnlineBranches c ON a.OurBranchID = c.OurBranchID AND c.BranchID = b.TargetBranch      
  Where a.OurBranchId = @OurBranchId And a.Status = 'C' And a.Supervised = '1' And convert(varchar(10), b.wDate, 101) = @WorkingDate      
  Order By a.ScrollNo      
        
END      
ELSE      
BEGIN      
        
  Select a.*, b.ChequeID, b.ChequeDate, b.RefNo as IBCADA, b.TargetBranch, c.BranchName As TargetBranchName      
  From t_OL_TransactionStatus a      
  INNER Join t_OnlineCashSupervisionRequired b ON a.OurBranchID = b.OurBranchID AND a.ScrollNo = b.ScrollNo      
  INNER Join t_OnlineBranches c ON a.OurBranchID = c.OurBranchID AND c.BranchID = b.TargetBranch      
  Where a.OurBranchId = @OurBranchId And a.Status = @Status And convert(varchar(10), b.wDate, 101) = @WorkingDate      
  Order By a.ScrollNo      
        
END