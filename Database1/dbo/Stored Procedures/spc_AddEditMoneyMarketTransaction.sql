﻿CREATE Procedure [dbo].[spc_AddEditMoneyMarketTransaction]      
(      
  @DealNo varchar (30)='',      
  @OurBranchID varchar (30),      
  @AccountID varchar (30)='',      
  @SecurityIssue varchar (30)='',      
  @Amount  money=0,      
  @InterestRate decimal(18,6)=0,      
  @InterestAmount money=0,      
  @DealDate datetime='1/1/1900',      
  @MaturityDate datetime='1/1/1900',      
  @BrokerCode varchar (30)='',      
  @SettlementDate datetime='1/1/1900',      
  @BrokerageAmount money=0,      
  @TransactionID money=0,      
  @ParentDealType varchar(2)='',      
  @TypeOfTransaction varchar(2)='',      
  @DealDescription varchar(100)='',      
  @Status varchar (1)='',      
  @SupervisionStatus varchar(1)='',        
  @OperatorID varchar(30)='',        
  @SupervisorID varchar(30)='',        
  @AccrualRemarks varchar (100)='',      
  @Price1 Money=0,      
  @CurrencyID varchar (30)='',      
  @FXAmount  money=0,      
  @ExchangeRate Numeric(18,6)=0,      
  @RepoInterestRate money=0,      
  @ClientID varchar(30)='N/A',      
  @IsPostedGL char(1)=0,      
  @AccrualAppliedUpto datetime='1/1/1900',      
  @CalculateAccrualUpto datetime='1/1/1900',      
  @Capital money=0,      
  @CapInt money=0,      
  @IsPostForwardDeal char(1)='0',      
  @IsWePayAccGL char(1)='',      
  @WePayToAccount varchar(30)='',      
  @WeRecvAccount varchar(30)='',      
  @OurNostroAccount varchar(30)='',      
  @TheirNostroAccount varchar(30)='',      
  @OurNostroDescription varchar(100)='',      
  @TheirNostroDescription varchar(100)='',      
  @Tenor money=0,      
  @PremiumAmount money=0,      
  @InterestPaid money=0,      
  @IsPremiumPaid char(1)='1',      
  @IsMultiSecurity char(1)='0',      
  @Price2  money=0,      
  @CouponRate decimal(18,6)=0,      
  @RND int=0,      
  @TaxRate money=0,      
  @TaxAmount money=0,      
  @IsVerify int=0,      
  @VerifierName Varchar(100)='',      
  @SBPPrice money=0,      
  @PDName Varchar(100)='',      
  @SGLInFavorOf varchar(30)='',
  @NewRecord bit = 1      
)    
      
AS      
  
SET NOCOUNT ON  
  
 IF (@NewRecord=1)        
      
  BEGIN       
   Declare @mDealDate as Varchar(50)      
   DECLARE @MaxDeal As Integer      
   DECLARE @Dealstr  As varchar(15)      
   DECLARE @MaxTrNo As Integer      
      
   set @mDealDate=Convert(varchar(11),@DealDate)+' '+Convert(varchar(11),getdate(),114)      
      
   SELECT @MaxTrNo=IsNull(max(TransactionID) + 1,1) from t_moneymarketnew      
      
   IF @TypeOFTransaction='CL' OR @TypeOFTransaction='CB'       
      
    BEGIN      
      
     SELECT @MaxDeal=Isnull(max(Convert(integer,Substring(DealNo,patindex('%/%',Substring(Replace(DealNo,'-','/'),3,len(dealno)))+3,len(dealno))))+1,1) from t_moneymarketnew      
     --WHERE TypeOfTransaction='CL' OR TypeOfTransaction='CB'       
     WHERE TypeOfTransaction NOT IN ('FL','FB','PL')      
       
     SET @Dealstr = 'CALL/' +  cast(@MaxDeal as varchar(15))      
    END      
   ELSE IF @TypeOFTransaction='CN'       
      
    BEGIN      
      
     SELECT @MaxDeal=Isnull(max(Convert(integer,Substring(DealNo,patindex('%/%',Substring(Replace(DealNo,'-','/'),3,len(dealno)))+3,len(dealno))))+1,1) from t_moneymarketnew      
     --WHERE TypeOfTransaction='CN'      
     WHERE TypeOfTransaction NOT IN ('FL','FB','PL')      
     SET @Dealstr = 'CLEAN/' +  cast(@MaxDeal as varchar(15))      
    END      
   ELSE IF  @TypeOFTransaction='CI'       
      
    BEGIN      
      
     SELECT @MaxDeal=Isnull(max(Convert(integer,Substring(DealNo,patindex('%/%',Substring(Replace(DealNo,'-','/'),3,len(dealno)))+3,len(dealno))))+1,1) from t_moneymarketnew      
     --WHERE TypeOfTransaction='CI'       
     WHERE TypeOfTransaction NOT IN ('FL','FB','PL')      
       
     SET @Dealstr = 'COI/' +  cast(@MaxDeal as varchar(15))      
    END      
   ELSE IF @TypeOFTransaction='BL'      
      
    BEGIN      
      
     SELECT @MaxDeal=Isnull(max(Convert(integer,Substring(DealNo,patindex('%/%',Substring(Replace(DealNo,'-','/'),3,len(dealno)))+3,len(dealno))))+1,1) from t_moneymarketnew      
     --WHERE TypeOfTransaction='BL'             
     WHERE TypeOfTransaction NOT IN ('FL','FB','PL')      
      
     SET @Dealstr = 'BL/' +  cast(@MaxDeal as varchar(15))      
    END      
      
   ELSE IF @TypeOFTransaction='DL' OR @TypeOFTransaction='DB'      
      
    BEGIN      
       
     SELECT @MaxDeal=Isnull(max(Convert(integer,Substring(DealNo,patindex('%/%',Substring(Replace(DealNo,'-','/'),3,len(dealno)))+3,len(dealno))))+1,1) from t_moneymarketnew      
     --WHERE TypeOfTransaction='DL' OR TypeOfTransaction='DB'      
     WHERE TypeOfTransaction NOT IN ('FL','FB','PL')      
      
     SET @Dealstr = 'SD/' +  cast(@MaxDeal as varchar(15))      
    END      
      
   ELSE IF @TypeOFTransaction='PL'      
      
    BEGIN      
      
     --SELECT @MaxDeal=IsNull(max(cast(substring(dealno,4,len(dealno)) as integer)) + 1,1) from t_moneymarketnew      
     --WHERE TypeOfTransaction='PL'       
       
     --SET @Dealstr = 'PL/' +  cast(@MaxDeal as varchar(15))      
     Select @Dealstr='PL/'+cast(IsNull(Max(Cast(Substring(DealNo,4,len(DealNo)) as integer)),1) as varchar(5)) From(      
      Select 'PL/'+cast(IsNull(Max(Cast(Substring(DealNo,4,len(DealNo)) as integer))+1,1) as varchar(5)) as DealNo From t_MoneyMarketNew Where TypeOfTransaction='PL'      
     union      
     Select 'PL/'+cast(IsNull(Max(Cast(Substring(DealNo,4,len(DealNo)) as integer))+1,1) as varchar(5)) as DealNo From t_MMSecurityMarkUnPledge       
     ) as Deal      
    END      
      
      
   ELSE IF @TypeOFTransaction='OS' OR @TypeOFTransaction='OP'      
      
    BEGIN      
      
     --SELECT @MaxDeal=IsNull(max(cast(substring(dealno,5,len(dealno)) as integer)) + 1,1) from t_moneymarketnew      
     --WHERE TypeOfTransaction='OP' OR TypeOfTransaction='OS'      
     --WHERE TypeOfTransaction NOT IN ('FL','FB','PL')      
     Select @MaxDeal=Isnull(max(Convert(integer,Substring(DealNo,patindex('%/%',Substring(Replace(DealNo,'-','/'),3,len(dealno)))+3,len(dealno))))+1,1) from t_moneymarketnew      
     WHERE TypeOfTransaction NOT IN ('FL','FB','PL')      
      
     SET @Dealstr = 'O/R-' +  cast(@MaxDeal as varchar(15))      
    END      
      
   ELSE IF @TypeOFTransaction='RE'      
      
    BEGIN      
      
     SELECT @MaxDeal=Isnull(max(Convert(integer,Substring(DealNo,patindex('%/%',Substring(Replace(DealNo,'-','/'),3,len(dealno)))+3,len(dealno))))+1,1) from t_moneymarketnew      
     --WHERE TypeOfTransaction='RE'      
     WHERE TypeOfTransaction NOT IN ('FL','FB','PL')      
       
     SET @Dealstr = 'REPO/' +  cast(@MaxDeal as varchar(15))      
    END      
      
      
   ELSE IF @TypeOFTransaction='RR'      
      
    BEGIN      
      
     SELECT @MaxDeal=Isnull(max(Convert(integer,Substring(DealNo,patindex('%/%',Substring(Replace(DealNo,'-','/'),3,len(dealno)))+3,len(dealno))))+1,1) from t_moneymarketnew      
     --WHERE TypeOfTransaction='RR'      
     WHERE TypeOfTransaction NOT IN ('FL','FB','PL')      
       
     SET @Dealstr = 'REV/' +  cast(@MaxDeal as varchar(15))      
    END      
   ELSE      
      
    BEGIN      
     SELECT @Dealstr=IsNull(max(cast(dealno as int)) + 1,1) from t_moneymarketnew      
     WHERE TypeOfTransaction='FL' OR TypeOfTransaction='FB'       
    END      
       
   set @DealNo = @Dealstr    
         
   INSERT INTO t_MoneyMarketNEW      
      
    (OurBranchID, AccountID,SecurityIssue, DealNo, Amount, InterestRate,InterestAmount, DealDate, MaturityDate,      
    BrokerCode,BrokerageAmount,SettlementDate,ParentDealType,TypeofTransaction,DealDescription,TransactionID,      
    Status,SupervisionStatus,OperatorID,SupervisorID, AccrualRemarks,Price1,CurrencyID,FXAmount,ExchangeRate,      
    ClientID,RepoInterestRate,IsPostedGL,AccrualAppliedUpto,CalculateAccrualUpto,Capital,CapInt,IsPostForwardDeal,IsWePayAccGL,WePayToAccount,
    WeRecvAccount, OurNostroAccount, TheirNostroAccount, OurNostroDescription, TheirNostroDescription,Tenor,PremiumAmount, InterestPaid,
    IsPremiumPaid, IsMultiSecurity,Price2,CouponRate,RND,TaxRate,TaxAmount,IsVerify,VerifierName,SBPPrice,PDName, SGLInFavorOf)      
       
   VALUES         
    (@OurBranchID, @AccountID, @SecurityIssue, @DealNo, @Amount, @InterestRate, @InterestAmount, @DealDate, @MaturityDate,      
    @BrokerCode, @BrokerageAmount,@SettlementDate,@ParentDealType,@TypeOfTransaction, @DealDescription,@MaxTrNo,      
    @Status, @SupervisionStatus, @OperatorID,@SupervisorID, @AccrualRemarks,@Price1,@CurrencyID,@FXAmount,@ExchangeRate,      
    @ClientID,@RepoInterestRate,@IsPostedGL,@AccrualAppliedUpto,@CalculateAccrualUpto,@Capital,@CapInt,@IsPostForwardDeal,      
    @IsWePayAccGL,@WePayToAccount, @WeRecvAccount, @OurNostroAccount, @TheirNostroAccount, @OurNostroDescription, @TheirNostroDescription,      
    @Tenor,@PremiumAmount, @InterestPaid,@IsPremiumPaid, @IsMultiSecurity,@Price2,@CouponRate,@RND,@TaxRate,@TaxAmount,@IsVerify,
    @VerifierName,@SBPPrice,@PDName, @SGLInFavorOf)      
       
      
  END      
      
 ELSE      
      
  BEGIN       
   UPDATE t_MoneyMarketNEW SET      
      
    Amount=@Amount , InterestRate=@InterestRate,InterestAmount=@InterestAmount,       
    MaturityDate=@MaturityDate,BrokerCode=@BrokerCode,BrokerageAmount=@BrokerageAmount,      
    SettlementDate=@SettlementDate,ParentDealType=@ParentDealType,TypeOfTransaction=@TypeOfTransaction,      
    DealDescription=@DealDescription,Status=@Status,SupervisionStatus=@SupervisionStatus ,      
    SupervisorID=@SupervisorID, AccountID=@AccountID, SecurityIssue=@SecurityIssue,       
    AccrualRemarks=@AccrualRemarks,Price1=@Price1,CurrencyID=@CurrencyID,FXAmount=@FXAmount,      
    ExchangeRate=@ExchangeRate , ClientID=@ClientID, RepoInterestRate=@RepoInterestRate,      
    IsPostedGL=@IsPostedGL, AccrualAppliedUpto=@AccrualAppliedUpto, CalculateAccrualUpto=@CalculateAccrualUpto,      
    Capital=@Capital,CapInt=@CapInt,IsPostForwardDeal=@IsPostForwardDeal,IsWePayAccGL =@IsWePayAccGL,WePayToAccount=@WePayToAccount,      
    WeRecvAccount=@WeRecvAccount, OurNostroAccount=@OurNostroAccount, TheirNostroAccount=@TheirNostroAccount,      
    OurNostroDescription=@OurNostroDescription, TheirNostroDescription=@TheirNostroDescription, Tenor=@Tenor,      
    PremiumAmount=@PremiumAmount, InterestPaid=@InterestPaid,IsPremiumPaid=@IsPremiumPaid, IsMultiSecurity = @IsMultiSecurity,      
    Price2=@Price2, CouponRate=@CouponRate,RND=@RND,TaxRate=@TaxRate,TaxAmount=@TaxAmount,IsVerify=@IsVerify, 
    VerifierName = VerifierName + '+' + @VerifierName, SBPPrice=@SBPPrice, PDName=@PDName, SGLInFavorOf = @SGLInFavorOf
      
   WHERE OurBranchID=@OurBranchID AND DealNo=@DealNo      
  END      
    
select 'Ok' AS RetStatus, @DealNo DealNo