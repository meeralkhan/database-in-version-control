﻿
CREATE   PROCEDURE [dbo].[spc_UnSetCorpAccDPD]
AS
SET NOCOUNT ON;
SET ANSI_WARNINGS OFF;
UPDATE h SET
h.NoOfDPD = 0
from t_Account h
inner join t_Products b on h.OurBranchID = b.OurBranchID and h.ProductID = b.ProductID
and ISNULL(b.ProductType,'') = 'AE' and ISNULL(b.SegmentType,'') = 'Corporate'
left join v_GetTodayCorpExcessOD e on h.OurBranchID = e.OurBranchID and h.AccountID = e.AccountID
where ISNULL(e.AccountID, '') <> h.AccountID;
SELECT 'Ok' AS RetStatus;