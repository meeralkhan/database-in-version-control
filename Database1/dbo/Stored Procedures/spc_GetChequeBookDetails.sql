﻿CREATE Procedure [dbo].[spc_GetChequeBookDetails]        
(        
 @OurBranchID as Varchar(30),
 @AccountID as Varchar(30),        
 @ChequeStart as Varchar(15),        
 @ChequeEnd as Varchar(15)        
)        
As        
        
 IF EXISTS(Select DateIssued From t_Cheque,t_ChequePaid         
    Where t_Cheque.OurBranchID=@OurBranchID And t_Cheque.Accountid=@AccountID         
     And t_Cheque.OurBranchID=t_ChequePaid.OurBranchID And t_Cheque.Accountid=t_ChequePaid.AccountID         
     And t_ChequePaid.ChequeID >=@ChequeStart And t_ChequePaid.ChequeID<=@ChequeEnd        
    And ChequeStart=@ChequeStart And ChequeEnd=@ChequeEnd)        
  BEGIN        
--    Select 1 as ReturnValue,cq.DateIssued,cp.ChequeID,cp.Date ,IsNull(sp.Date,'') As StopPaymentDate,IsNull(sp.Description,'') as Description        
--    from t_Cheque as cq Inner Join t_ChequePaid as cp ON cq.AccountID=cp.AccountID        
--      Left Outer Join t_StopPayments as sp ON cq.AccountID=sp.Accountid        
--      And  cq.OurBranchID=sp.OurBranchID And sp.ChequeID >=@ChequeStart And sp.ChequeID<=@ChequeEnd        
        
  Select 1 as ReturnValue,cq.DateIssued,cp.ChequeID,cp.Date as PaymentDate,t.Description,t.Amount,t.OperatorID, t.SupervisorID        
   from t_Cheque cq           
   INNER JOIN t_ChequePaid cp ON cq.OurBranchID=cp.OurBranchID AND cq.AccountID=cp.AccountID          
   LEFT JOIN t_Transactions t ON cq.OurBranchID=t.OurBranchID AND cq.AccountID=t.AccountID And cp.ChequeID=t.ChequeID          
             
   Where cq.OurBranchID=@OurBranchID        
    And cq.Accountid=@AccountID And cp.ChequeID >=@ChequeStart And cp.ChequeID<=@ChequeEnd         
    And cq.ChequeStart=@ChequeStart And cq.ChequeEnd=@ChequeEnd        
--   order by cp.ChequeID        
  /*        
   UNION ALL        
        
  Select 1 as ReturnValue,cq.DateIssued,cp.ChequeID,cp.Date as PaymentDate,t.Description,t.Amount,t.OperatorID, t.SupervisorID        
   from t_Cheque cq          
   INNER JOIN t_ChequePaid cp ON cq.OurBranchID=cp.OurBranchID AND cq.AccountID=cp.AccountID          
   INNER JOIN t_InwardClearing t ON cq.OurBranchID=t.OurBranchID AND cq.AccountID=t.AccountID And cp.ChequeID=t.ChequeID          
             
   Where cq.OurBranchID=@OurBranchID        
    And cq.Accountid=@AccountID And cp.ChequeID >=@ChequeStart And cp.ChequeID<=@ChequeEnd         
    And cq.ChequeStart=@ChequeStart And cq.ChequeEnd=@ChequeEnd        
--   order by cp.ChequeID        
        
   Union all        
  Select 1 as ReturnValue,cq.DateIssued,cp.ChequeID,cp.Date as PaymentDate,t.Description,t.Amount,t.OperatorID, t.SupervisorID        
  from t_Cheque cq          
  INNER JOIN t_ChequePaid cp ON cq.OurBranchID=cp.OurBranchID AND cq.AccountID=cp.AccountID          
  INNER JOIN t_CashTransactionModel t ON cq.OurBranchID=t.OurBranchID AND cq.AccountID=t.AccountID And cp.ChequeID=t.ChequeID        
            
  Where cq.OurBranchID=@OurBranchID        
    And cq.Accountid=@AccountID And cp.ChequeID >=@ChequeStart And cp.ChequeID<=@ChequeEnd         
    And cq.ChequeStart=@ChequeStart And cq.ChequeEnd=@ChequeEnd        
--   order by cp.ChequeID        
        
   union all        
        
  Select 1 as ReturnValue,cq.DateIssued,cp.ChequeID,cp.Date as PaymentDate,t.Description,t.Amount,t.OperatorID, t.SupervisorID        
  from t_Cheque cq          
  INNER JOIN t_ChequePaid cp ON cq.OurBranchID=cp.OurBranchID AND cq.AccountID=cp.AccountID          
  INNER JOIN t_TransferTransactionModel t ON cq.OurBranchID=t.OurBranchID AND cq.AccountID=t.AccountID And cp.ChequeID=t.ChequeID        
        
   Where cq.OurBranchID=@OurBranchID        
    And cq.Accountid=@AccountID And cp.ChequeID >=@ChequeStart And cp.ChequeID<=@ChequeEnd         
    And cq.ChequeStart=@ChequeStart And cq.ChequeEnd=@ChequeEnd        
--   order by cp.ChequeID        
        
  union all        
  Select 1 as ReturnValue,cq.DateIssued,cp.ChequeID,cp.Date as PaymentDate,t.Description,t.Amount,t.OperatorID, t.SupervisorID        
   from t_Cheque cq          
   INNER JOIN t_ChequePaid cp ON cq.AccountID=cp.AccountID And cq.AccountID=cp.AccountID          
   INNER JOIN t_OnLineCashTransaction t ON cq.OurBranchID=t.OurBranchID And cp.AccountID=t.AccountID And cp.ChequeID=t.ChequeID          
             
   Where cq.OurBranchID=@OurBranchID        
    And cq.Accountid=@AccountID And cp.ChequeID >=@ChequeStart And cp.ChequeID<=@ChequeEnd         
    And cq.ChequeStart=@ChequeStart And cq.ChequeEnd=@ChequeEnd        
   order by cp.ChequeID        
    */        
  END        
 ELSE        
  Select 0 as ReturnValue