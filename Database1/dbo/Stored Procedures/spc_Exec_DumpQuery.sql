﻿create procedure [dbo].[spc_Exec_DumpQuery]        
(        
 @SQLQuery nvarchar(max)   
)        
AS        
        
SET NOCOUNT ON    
  
exec (@SQLQuery)      
select 'Ok' as ReturnStatus, 'Done' AS ReturnMessage