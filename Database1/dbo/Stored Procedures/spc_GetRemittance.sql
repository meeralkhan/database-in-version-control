﻿CREATE PROCEDURE [dbo].[spc_GetRemittance]  
 @OurBranchID varchar(30),    
 @AccountID varchar(30)    
AS    

set nocount on

DECLARE  @BatchNo varchar(1),@ForeignAmount money,@InstrumentVariableNo varchar(1)    
Select  @ForeignAmount = 0    
Select   @BatchNo = ''    
Select   @BatchNo = ''    
    
 Select lr.OurBranchID,wDate AS DateOfIssue,  SerialNo AS BatchNo ,InstrumentNo AS BatchInstrumentNo, InstrumentAccountID As AccountID,'PKR' AS CurrencyID,Left(InstrumentType,1) As InstrumentType,  ControlNo AS InstrumentFixedNo,
 @BatchNo AS InstrumentVariableNo,Amount,  @ForeignAmount  As ForeignAmount,BeneficiaryName AS 'InFavourOf',    
 DrawnBankID AS BankID,DrawnBranchID AS BranchID,b.FullName AS BankName, br.Name AS BranchName, Stop,Duplicate,DuplicateDate,OldInstrumentNo,OverLimit AS OverLimit    
 from t_LR_Issuance lr  
   
 inner join t_Banks b ON lr.OurBranchID = b.OurBranchID AND lr.DrawnBankID = b.BankID  
   
 inner join t_Branches br ON lr.OurBranchID = br.OurBranchID AND lr.DrawnBankID = br.BankID AND lr.DrawnBranchID = br.BranchID  
   
 Where (lr.OurBranchID = @OurBranchID) and (InstrumentAccountID = @AccountID)  and (ISNULL(Paid,0) = 0)   
 and (Supervision = 'C') and (ISNULL(Erase,0) = 0) and (ISNULL(Stop,0) =0) and (ISNULL(Cancel,0) = 0)   
 and (ISNULL(Lost,0) = 0) and (InstrumentNo > 0)    
/*  
union all    
 Select  OurBranchID,wDate AS DateOfIssue,  SerialNo AS BatchNo ,InstrumentNo AS BatchInstrumentNo, InstrumentAccountID As AccountID,'PKR' AS CurrencyID,Left(InstrumentType,1) As InstrumentType, ControlNo AS InstrumentFixedNo,@BatchNo AS InstrumentVariabl
  
eNo,Amount,  @ForeignAmount  As ForeignAmount,BeneficiaryName AS 'InFavourOf',    
 IssueBankID AS BankID,IssueBranchID AS BranchID,IssueBankName AS BankName,IssueBranchName AS BranchName,Stop,Duplicate,DuplicateDate,OldInstrumentNo=0,0 AS OverLimit    
 from t_LR_Advice    
 Where (OurBranchID = @OurBranchID) and (InstrumentAccountID = @AccountID)  and (Paid = 0) and (Supervision = 'C') and (Stop =0) and (Cancel = 0) and (Lost=0) and (InstrumentNo > 0)    
union all    
 Select  OurBranchID,wDate AS DateOfIssue,  SerialNo AS BatchNo ,ControlNo AS BatchInstrumentNo, InstrumentAccountID As AccountID,'PKR' AS CurrencyID,Left(InstrumentType,1) As InstrumentType, ControlNo AS InstrumentFixedNo,@BatchNo AS InstrumentVariableNo
  
,Amount,  @ForeignAmount  As ForeignAmount,BeneficiaryName AS 'InFavourOf',    
 IssueBankID AS BankID,IssueBranchID AS BranchID,IssueBankName AS BankName,IssueBranchName AS BranchName, Stop,Duplicate,DuplicateDate,OldInstrumentNo=0,0 AS OverLimit    
 from t_LR_Advice    
 Where (OurBranchID = @OurBranchID) and (InstrumentAccountID = @AccountID)  and (Paid = 0) and (Supervision = 'C') and (Stop =0) and (Cancel = 0) and (Lost=0) and (InstrumentNo = 0)    
 and (ControlNo > 0)    
  */