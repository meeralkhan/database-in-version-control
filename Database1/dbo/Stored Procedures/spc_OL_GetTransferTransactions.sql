﻿CREATE PROCEDURE [dbo].[spc_OL_GetTransferTransactions]        
(        
  @OurBranchID Varchar(30),        
  @Status Char(1)=''        
)        
        
AS        
        
SET NOCOUNT ON        
        
Declare @WorkingDate as DateTime        
        
Select @WorkingDate = WorkingDate From t_Last where OurBranchID = @OurBranchID    
        
IF @Status = ''        
BEGIN        
          
  Select a.OurBranchID, a.ScrollNo, a.SerialNo, a.RefNo, a.AccountID, a.AccountName, a.CurrencyID, a.Amount,       
  a.ForeignAmount, a.ExchangeRate, a.Supervision Status, a.TrxType, a.IsLocalCurrency, a.OperatorID, a.ChequeID,       
  a.ChequeDate, a.RefNo as IBCADA, a.TargetBranchID TargetBranch, c.BranchName As TargetBranchName        
  From t_OnlineTransferSupervisionRequired a      
  INNER Join t_OnlineBranches c ON a.OurBranchId = c.OurBranchId And c.BranchID = a.TargetBranchID        
  Where a.OurBranchId = @OurBranchId And convert(varchar(10), a.wDate, 101) = @WorkingDate        
  Order By a.ScrollNo, a.SerialNo        
          
END        
ELSE IF @Status = 'S'        
BEGIN        
          
  Select a.OurBranchID, a.ScrollNo, a.SerialNo, a.RefNo, a.AccountID, a.AccountName, a.CurrencyID, a.Amount,       
  a.ForeignAmount, a.ExchangeRate, a.Supervision Status, a.TrxType, a.IsLocalCurrency, a.OperatorID, a.ChequeID,       
  a.ChequeDate, a.RefNo as IBCADA, a.TargetBranchID TargetBranch, c.BranchName As TargetBranchName        
  From t_OnlineTransferSupervisionRequired a      
  INNER Join t_OnlineBranches c ON a.OurBranchId = c.OurBranchId And c.BranchID = a.TargetBranchID        
  Where a.OurBranchId = @OurBranchId And a.Supervision = '*' And convert(varchar(10), a.wDate, 101) = @WorkingDate        
  Order By a.ScrollNo, a.SerialNo        
          
END        
ELSE IF @Status = 'C'        
BEGIN        
          
  Select a.OurBranchID, a.ScrollNo, a.SerialNo, a.RefNo, a.AccountID, a.AccountName, a.CurrencyID, a.Amount,       
  a.ForeignAmount, a.ExchangeRate, a.Supervision Status, a.TrxType, a.IsLocalCurrency, a.OperatorID, a.ChequeID,       
  a.ChequeDate, a.RefNo as IBCADA, a.TargetBranchID TargetBranch, c.BranchName As TargetBranchName        
  From t_OnlineTransferSupervisionRequired a      
  INNER Join t_OnlineBranches c ON a.OurBranchId = c.OurBranchId And c.BranchID = a.TargetBranchID        
  Where a.OurBranchId = @OurBranchId And a.Supervision = 'C' And convert(varchar(10), a.wDate, 101) = @WorkingDate        
  Order By a.ScrollNo, a.SerialNo        
          
END        
ELSE        
BEGIN        
          
  Select a.OurBranchID, a.ScrollNo, a.SerialNo, a.RefNo, a.AccountID, a.AccountName, a.CurrencyID, a.Amount,       
  a.ForeignAmount, a.ExchangeRate, a.Supervision Status, a.TrxType, a.IsLocalCurrency, a.OperatorID, a.ChequeID,       
  a.ChequeDate, a.RefNo as IBCADA, a.TargetBranchID TargetBranch, c.BranchName As TargetBranchName        
  From t_OnlineTransferSupervisionRequired a      
  INNER Join t_OnlineBranches c ON a.OurBranchId = c.OurBranchId And c.BranchID = a.TargetBranchID        
  Where a.OurBranchId = @OurBranchId And a.Supervision = @Status And convert(varchar(10), a.wDate, 101) = @WorkingDate        
  Order By a.ScrollNo, a.SerialNo        
          
END