﻿create   procedure [dbo].[sp_ATM_FileMgmtMsg] (
	@OurBranchID nvarchar(30),
	@CardNumber nvarchar(30),
	@DateTime datetime,
	@STAN nvarchar(30),
	@StatusType nvarchar(30),		-- SC -> Status Change, RP -> Replacement
	@PreviousStatus nvarchar(30),	-- in case of Card Status Update the value will be 2 chars NM to LT in case of card replacement the value will be OLD debit card token
	@CurrentStatus nvarchar(30),	-- in case of Card Status Update the value will be 2 chars NM to LT in case of card replacement the value will be NEW debit card token
	
	@ChannelID nvarchar(30),
	@RefNo varchar(100)         
)
AS
BEGIN

 SET NOCOUNT ON 

 DECLARE @Status as int
 DECLARE @mStatus as varchar(10)
 DECLARE @Count as int
 DECLARE @sCount as int          
  SET @sCount = 0          
 DECLARE @cCount as int          
  SET @cCount = 0          
 DECLARE @mLastEOD as datetime
 DECLARE @mCONFRETI as datetime
 DECLARE @ClearBalance as money            
 DECLARE @AvailableBalance as money          
 Declare @value as varchar(2)     
 DECLARE @CurrencyCode as varchar(30)                   
 DECLARE @mDescriptionCharges as varchar(500)            
 DECLARE @mDescriptionHCharges as varchar(500)      
 DECLARE @vCheckLimit as bit          
  SET @vCheckLimit = 0   
 DECLARE @AccountID as nvarchar(30)   
  SET @AccountID = ''
 DECLARE @CardStatus as nvarchar(10)
 DECLARE @vClearedEffects as money            
 DECLARE @vFreezeAmount as money            
 DECLARE @Limit as money            
 DECLARE @AcquiringChargesAccountID as varchar(30)            
 DECLARE @ExciseDutyAccountID as varchar(30)         
 DECLARE @mScrollNo as int            
  SET @mScrollNo = 0          
 DECLARE @VoucherID int            
 DECLARE @IsCredit int            
 DECLARE @mSerialNo as int            
  SET @mSerialNo = 0          
 DECLARE @mwDate as datetime            
 DECLARE @mTrxType as char            
 DECLARE @mAccountID as varchar(30)            
 DECLARE @mAccountName as varchar(50)            
 DECLARE @mAccountType as char            
 DECLARE @mProductID as varchar(30)            
 DECLARE @mCurrencyID as varchar(30)            
          
 DECLARE @gAccountID as varchar(30)            
 DECLARE @gAccountName as varchar(50)            
 DECLARE @gAccountType as char            
 DECLARE @gProductID as varchar(30)            
 DECLARE @gCurrencyID as varchar(30)            
          
 DECLARE @LocalCurrency as varchar(30)          
 DECLARE @mIsLocalCurrency as bit            
  SET @mIsLocalCurrency=1           
 DECLARE @vIsClose as char(1)    
 Declare @MinBalance as money    
 Declare @SOCID as varchar(30)     
  SET @SOCID = ''
 Declare @ATMChargeID as varchar(6) 
 DECLARE @ExciseDutyPercentage as money          
 DECLARE @WithdrawlCharges as money            
 DECLARE @ExciseDutyAmount as money          
            
  SET @ExciseDutyPercentage = 0          
  SET @WithdrawlCharges = 0          
  SET @ExciseDutyAmount = 0                                       
 DECLARE @mRemarksCharges as varchar(500)   
 Declare @mRemarksVATCharges as varchar(500)
 DECLARE @mForeignAmount as money            
  SET @mForeignAmount=0             
 DECLARE @mExchangeRate as money            
  SET @mExchangeRate=1     
 DECLARE @mDescriptionID as varchar(30)            
  SET @mDescriptionID='000'             
 DECLARE @mDescriptionIDChg2 as varchar(30)            
  SET @mDescriptionIDChg2='000'   
 DECLARE @mBankCode as varchar(4)            
  SET @mBankCode='1279'                           
 DECLARE @mOperatorID as varchar(30)            
  SET @mOperatorID = @ChannelID            
 DECLARE @mSupervisorID as varchar(30)            
  SET @mSupervisorID = 'N/A'   
 DECLARE @ChannelRefID as nvarchar(30)
 SET @ChannelRefID = @STAN          
 DECLARE @GLControl as varchar(30)          
 DECLARE @mProcCode as varchar(50)
 
 DECLARE @MobileNo nvarchar(30)
 DECLARE @Email nvarchar(100)
 DECLARE @rCurrencyID nvarchar(30)
 DECLARE @ClientID nvarchar(30)
 DECLARE @NarrationID nvarchar(30)
  SET @MobileNo = ''
  SET @Email = ''
  SET @rCurrencyID = 'AED'
  SET @ClientID = ''
 DECLARE @VatReferenceID as varchar(30)
 
 SELECT @Status = -1            
 
 SELECT @mLastEOD=LastEOD,@mwDate=WorkingDate,@mCONFRETI=CONFRETI FROM t_Last WHERE (OurBranchID = @OurBranchID)
 
 if (@StatusType = 'SC')
  SET @NarrationID = '3XXS'
 ELSE if (@StatusType = 'RP')
  SET @NarrationID = '3XXR'
 ELSE
  SET @NarrationID = ''

 IF @mLastEOD = @mwDate or @mCONFRETI = '1/1/9999'            
 BEGIN  --INVALID_DATE
 
  INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, 
  ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, 
  CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
  VALUES (@OurBranchID, @AccountID, @RefNo, @STAN, @DateTime, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), 0, 0, 0, 0, '', '', '', '', '', '', 0, @StatusType, '', '', 
  '', '', '', '', '', '', '', '', '', '', '95', 'Day End', '3XX Trx', '0', '')
 
  SELECT @Status=95, @AvailableBalance = 0, @ClearBalance = 0          
  SELECT @Status As Status, @ClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, '00' As value, '784' as CurrencyCode, '0' as AuthIDResp, @MobileNo as MobileNo, 
  @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
  RETURN (1)          
 END     
 
 SELECT @vCheckLimit=CheckLimit FROM t_ATM_GlobalVariables Where OurBranchID = @OurBranchID          
 
 if (@StatusType = 'SC')
 BEGIN

  SELECT @Count=Count(AccountID) FROM t_DebitCards WHERE (OurBranchID = @OurBranchID) AND (DebitCardToken = @CardNumber)           
  IF @Count=0    -- Debit Card Token Not Found
  BEGIN            
   INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, 
   ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, 
   CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
   VALUES (@OurBranchID, @AccountID, @RefNo, @STAN, @DateTime, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), 0, 0, 0, 0, '', '', '', '', '', '', 0, @StatusType, '', '', 
   '', '', '', '', '', '', '', '', '', '', '25', 'Debit Card Token Not Found.', '3XX Trx', '0', '')
  
   SELECT @Status=25, @AvailableBalance = 0, @ClearBalance = 0          
   SELECT @Status As Status, @ClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, '00' As value, '784' as CurrencyCode, '0' as AuthIDResp, @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
   RETURN (1)
  END
  
  SELECT @AccountID=AccountID, @CardStatus=Status FROM t_DebitCards WHERE (OurBranchID = @OurBranchID) AND (DebitCardToken = @CardNumber)

  IF (@CurrentStatus = 'CN')		-- Cancelled Card
   SET @mStatus = 'C'
  ELSE IF (@CurrentStatus = 'LT')	-- Lost Card
   SET @mStatus = 'L'
  ELSE IF (@CurrentStatus = 'ST')	-- Stolen Card
   SET @mStatus = 'S'
  ELSE IF (@CurrentStatus = 'DM')	-- Damage Card
   SET @mStatus = 'D'
  ELSE IF (@CurrentStatus = 'LD')	-- lost in delivery card
   SET @mStatus = 'T'
  ELSE IF (@CurrentStatus = 'WM')	-- Warm Blocked no charges
   SET @mStatus = 'W'
  ELSE IF (@CurrentStatus = 'BR')	-- Block & Replace no charges
   SET @mStatus = 'R'
  ELSE IF (@CurrentStatus = 'TB')	-- Temporary Block no charges
   SET @mStatus = 'P'
  ELSE IF (@CurrentStatus = 'NM')	-- Normal
   SET @mStatus = 'A'
   
  SET @Value = @CurrentStatus
   
 END
 ELSE if (@StatusType = 'RP')
 BEGIN
  SELECT @Count=Count(AccountID) FROM t_DebitCards WHERE (OurBranchID = @OurBranchID) AND (DebitCardToken = @PreviousStatus)
  IF @Count = 0    -- Old Debit Card Token not found
  BEGIN
   INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, 
   ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, 
   CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
   VALUES (@OurBranchID, '', @RefNo, @STAN, @DateTime, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), 0, 0, 0, 0, '', '', '', '', '', '', 0, @StatusType, '', '', '', '', '',
   '', '', '', '', '', '', '', '27', 'Old debit card token not found.', '3XX Trx', '0', '')
   
   SELECT @Status=27, @AvailableBalance = 0, @ClearBalance = 0          
   SELECT @Status As Status, @ClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, '00' As value, '784' as CurrencyCode, '0' as AuthIDResp, @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
   RETURN (1)
  END

  SELECT @Count=Count(AccountID) FROM t_DebitCards WHERE (OurBranchID = @OurBranchID) AND (DebitCardToken = @CurrentStatus)
  IF @Count > 0    -- New Debit Card Token already exists
  BEGIN
  
   INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, 
   ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, 
   CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
   VALUES (@OurBranchID, '', @RefNo, @STAN, @DateTime, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), 0, 0, 0, 0, '', '', '', '', '', '', 0, @StatusType, '', '', '', '', '',
   '', '', '', '', '', '', '', '26', 'New Debit Card Token already exists.', '3XX Trx', '0', '')
   
   SELECT @Status=26, @AvailableBalance = 0, @ClearBalance = 0          
   SELECT @Status As Status, @ClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, '00' As value, '784' as CurrencyCode, '0' as AuthIDResp, @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
   RETURN (1)
  END

  SELECT @AccountID=AccountID, @CardStatus=Status FROM t_DebitCards WHERE (OurBranchID = @OurBranchID) AND (DebitCardToken = @PreviousStatus)
 END
 ELSE
 BEGIN
 
  INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, 
  ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, 
  CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
  VALUES (@OurBranchID, '', @RefNo, @STAN, @DateTime, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), 0, 0, 0, 0, '', '', '', '', '', '', 0, @StatusType, '', '', '', '', '',
  '', '', '', '', '', '', '', '29', 'Invalid Card Status type', '3XX Trx', '0', '')
   
  SELECT @Status=29, --Invalid Card Status Type
  @AvailableBalance = 0, @ClearBalance = 0
  SELECT @Status As Status, @ClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value, @CurrencyCode as CurrencyCode, '0' as AuthIDResp, @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
  RETURN (1)
 END

 if (@StatusType = 'SC')
 BEGIN
  SELECT @ClearBalance=IsNull(ab.ClearBalance,0),@vClearedEffects=IsNull(ab.Effects,0),@mAccountName=ab.Name,@Limit=ISNULL(ab.Limit,0),@mAccountType='C',@mProductID=a.ProductID, 
  @mCurrencyID=c.CurrencyID,@vFreezeAmount=ISNULL(ab.FreezeAmount,0),@vIsClose=ISNULL(a.Status,''),
  --@value=ISNULL(p.productatmcode,'00'), 
  @MinBalance=ISNULL(p.MinBalance,0), 
  @CurrencyCode = ISNULL(c.CurrencyCode,''),@SOCID=ISNULL(a.SOCID,''), @MobileNo=ISNULL(cust.MobileNo,''),@Email=ISNULL(cust.Email,''),@ClientID=cust.ClientID
  FROM t_AccountBalance ab           
  INNER JOIN t_Account a ON ab.OurBranchID = a.OurBranchID AND ab.AccountID = a.AccountID AND ab.ProductID = a.ProductID          
  INNER JOIN t_Products p ON ab.OurBranchID = p.OurBranchID AND ab.ProductID = p.ProductID          
  INNER JOIN t_Currencies c ON ab.OurBranchID = c.OurBranchID AND p.CurrencyID = c.CurrencyID          
  INNER JOIN t_Customer cust ON ab.OurBranchID = cust.OurBranchID AND a.ClientID = cust.ClientID
  WHERE (ab.OurBranchID = @OurBranchID) AND (ab.AccountID = @AccountID)            
 END
 ELSE
 BEGIN
  SELECT @ClearBalance=IsNull(ab.ClearBalance,0),@vClearedEffects=IsNull(ab.Effects,0),@mAccountName=ab.Name,@Limit=ISNULL(ab.Limit,0),@mAccountType='C',@mProductID=a.ProductID, 
  @mCurrencyID=c.CurrencyID,@vFreezeAmount=ISNULL(ab.FreezeAmount,0),@vIsClose=ISNULL(a.Status,''),
  @value=ISNULL(p.productatmcode,'00'), 
  @MinBalance=ISNULL(p.MinBalance,0), 
  @CurrencyCode = ISNULL(c.CurrencyCode,''),@SOCID=ISNULL(a.SOCID,''), @MobileNo=ISNULL(cust.MobileNo,''),@Email=ISNULL(cust.Email,''),@ClientID=cust.ClientID
  FROM t_AccountBalance ab           
  INNER JOIN t_Account a ON ab.OurBranchID = a.OurBranchID AND ab.AccountID = a.AccountID AND ab.ProductID = a.ProductID          
  INNER JOIN t_Products p ON ab.OurBranchID = p.OurBranchID AND ab.ProductID = p.ProductID          
  INNER JOIN t_Currencies c ON ab.OurBranchID = c.OurBranchID AND p.CurrencyID = c.CurrencyID          
  INNER JOIN t_Customer cust ON ab.OurBranchID = cust.OurBranchID AND a.ClientID = cust.ClientID
  WHERE (ab.OurBranchID = @OurBranchID) AND (ab.AccountID = @AccountID)            
 END

 IF @Limit > 0 AND @vCheckLimit = 1
 BEGIN          
  SET @ClearBalance=(@ClearBalance+@Limit)          
 END

 if (@StatusType = 'RP')
 BEGIN

  SET @sCount = 0          
  SET @cCount = 1    
  
  WHILE (@sCount <> @cCount)            
  BEGIN            
   exec @mScrollNo = fnc_GetRandomScrollNo          
   if not exists (SELECT ScrollNo from t_ATM_TransferTransaction Where OurBranchID = @OurBranchID AND ScrollNo = @mScrollNo)        
   BEGIN          
    SET @sCount = @sCount + 1            
   END          
  END

  IF (@CardStatus = 'C')		-- Cancelled Card
  BEGIN
   SET @ATMChargeID = 'CN'
   --SET @mDescriptionID = '819' --old 
   --SET @mDescriptionIDChg2 = '820' -- old
   SET @mDescriptionID = '400060' --new
   SET @mDescriptionIDChg2 = '400020' --new

   --SET @mRemarksCharges='Debit Card reissuance fee - STAN: '+@STAN+' [DB CRD TKN:]'+@CardNumber
   --SET @mDescriptionCharges = 'Debit Card reissuance fee - STAN: '+@STAN+' [DB CRD TKN:]'+@CardNumber
   
   SET @mDescriptionCharges = 'Debit Card reissuance fee'
   SET @mRemarksCharges=''

   SET @mDescriptionHCharges = 'Value Added Tax (VAT) for transaction  - ' +Cast(@mScrollNo as varchar(30))
   SET @mRemarksVATCharges=''
   
   SET @mProcCode = 'CANCL CHG'
  END
  ELSE IF (@CardStatus = 'L')	-- Lost Card
  BEGIN
   SET @ATMChargeID = 'LT'
   --SET @mDescriptionID = '821'  -- old
   --SET @mDescriptionIDChg2 = '822' --old

   SET @mDescriptionID = '400060' --new
   SET @mDescriptionIDChg2 = '400020' --new

   --SET @mRemarksCharges='Debit Card reissuance fee STAN: '+@STAN+' [DB CRD TKN:]'+@CardNumber
   --SET @mDescriptionCharges = 'Debit Card reissuance fee STAN: '+@STAN+' [DB CRD TKN:]'+@CardNumber
   
   SET @mDescriptionCharges = 'Debit Card reissuance fee'
   SET @mRemarksCharges=''
   
   SET @mDescriptionHCharges = 'Value Added Tax (VAT) for transaction  - ' +Cast(@mScrollNo as varchar(30))
   SET @mRemarksVATCharges=''
   SET @mProcCode = 'LT CHG'
  END
  ELSE IF (@CardStatus = 'S')	-- Stolen Card
  BEGIN
   SET @ATMChargeID = 'ST'
   --SET @mDescriptionID = '823' --old
   --SET @mDescriptionIDChg2 = '824' --old

   SET @mDescriptionID = '400060' --new
   SET @mDescriptionIDChg2 = '400020' --new

   --SET @mRemarksCharges='Debit Card reissuance fee STAN: '+@STAN+' [DB CRD TKN:]'+@CardNumber
   --SET @mDescriptionCharges = 'Debit Card reissuance fee STAN: '+@STAN+' [DB CRD TKN:]'+@CardNumber
   
   SET @mDescriptionCharges = 'Debit Card reissuance fee'
   SET @mRemarksCharges=''

   SET @mDescriptionHCharges = 'Value Added Tax (VAT) for transaction  - ' +Cast(@mScrollNo as varchar(30))
   SET @mRemarksVATCharges=''
   SET @mProcCode = 'STLN CHG'
  END
  ELSE IF (@CardStatus = 'D')	-- Damage Card
  BEGIN
   SET @ATMChargeID = 'DM'
   --SET @mDescriptionID = '825' 
   --SET @mDescriptionIDChg2 = '826'

   SET @mDescriptionID = '400060'  --new
   SET @mDescriptionIDChg2 = '400020' --new

   --SET @mRemarksCharges='Debit Card reissuance fee STAN: '+@STAN+' [DB CRD TKN:]'+@CardNumber
   --SET @mDescriptionCharges = 'Debit Card reissuance fee STAN: '+@STAN+' [DB CRD TKN:]'+@CardNumber
   
   SET @mDescriptionCharges = 'Debit Card reissuance fee'
   SET @mRemarksCharges=''

   SET @mDescriptionHCharges = 'Value Added Tax (VAT) for transaction  - ' +Cast(@mScrollNo as varchar(30))
   SET @mRemarksVATCharges=''
   SET @mProcCode = 'DMG CHG'
  END
  ELSE IF (@CardStatus = 'T')	-- lost in delivery card
  BEGIN
   SET @ATMChargeID = 'LD'
   --SET @mDescriptionID = '827' --old
   --SET @mDescriptionIDChg2 = '828' --old

   SET @mDescriptionID = '400060' --new
   SET @mDescriptionIDChg2 = '400020' --new

   --SET @mRemarksCharges='Debit Card reissuance fee STAN: '+@STAN+' [DB CRD TKN:]'+@CardNumber
   --SET @mDescriptionCharges = 'Debit Card reissuance fee STAN: '+@STAN+' [DB CRD TKN:]'+@CardNumber
   
   SET @mDescriptionCharges = 'Debit Card reissuance fee'
   SET @mRemarksCharges=''

   SET @mDescriptionHCharges = 'Value Added Tax (VAT) for transaction  - ' +Cast(@mScrollNo as varchar(30))
   SET @mRemarksVATCharges=''
   
   SET @mProcCode = 'LT DELVRY CHG'
  END
  --ELSE IF (@CurrentStatus = 'WM')	-- Warm Blocked no charges
  --BEGIN
   --SET @ATMChargeID = @CurrentStatus
   --SET @mDescriptionID = '827' 
   --SET @mDescriptionIDChg2 = '828'
   --SET @mRemarksCharges='Being Amount of card lost in delivery chgs trx By A/c # ' + @AccountID + ' Title : ' + ISNULL(upper(@mAccountName),'')
   --SET @mRemarksVATCharges='Being Amount of lost in delivery Card VAT chgs Trx By A/c # ' + @AccountID + ' Title : ' + ISNULL(upper(@mAccountName),'')
   --SET @mDescriptionCharges = 'CARD LOST DELVRY CHG - STAN: '+@STAN+' [DB CRD TKN:]'+@CardNumber
   --SET @mDescriptionHCharges = 'CARD LOST DELVRY VAT CHG - STAN: '+@STAN+' [DB CRD TKN:]'+@CardNumber
   --SET @mProcCode = 'LT DELVRY CHG'
  --END
  ELSE IF (@CardStatus = 'A')	-- Normal
  BEGIN
   SET @ATMChargeID = 'REPL'
   --SET @mDescriptionID = '817'  --old
   --SET @mDescriptionIDChg2 = '818' --old

   SET @mDescriptionID = '400060' --new
   SET @mDescriptionIDChg2 = '400020' --new

   --SET @mDescriptionCharges = 'Debit Card reissuance fee STAN: '+@STAN+' [DB CRD TKN:]'+@CardNumber
   --SET @mRemarksCharges='Debit Card reissuance fee STAN: '+@STAN+' [DB CRD TKN:]'+@CardNumber
   
   SET @mDescriptionCharges='Debit Card reissuance fee'
   SET @mRemarksCharges=''

   SET @mDescriptionHCharges = 'Value Added Tax (VAT) for transaction  - ' +Cast(@mScrollNo as varchar(30))
   SET @mRemarksVATCharges=''
   
   SET @mProcCode = 'REP CHG'
  END
  
  --SET @NarrationID = @mDescriptionID
  
  IF @vIsClose = 'C' or @vIsClose = 'T' or @vIsClose = 'X' or @vIsClose = 'D' or @vIsClose = 'I'          
  BEGIN   --INACTIVE_ACCOUNT             
  
   INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, 
   ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, 
   CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
   VALUES (@OurBranchID, @AccountID, @RefNo, @STAN, @DateTime, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), 0, 0, 0, 0, '', '', '', '', '', '', 0, @StatusType, '', '', '', '', '', '', '', '', '', '', '', '', '03', 'Account In-Active/ Closed/ Dormant/ Deceased/ Blocked', '3XX Trx', '0', '')
   
   SELECT @Status=3,          
   @AvailableBalance = ( isnull(@ClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@MinBalance,0) )*100,          
   @ClearBalance     = ( IsNull(@ClearBalance,0)-IsNull(@vClearedEffects,0) )*100          
           
   SELECT @Status As Status, @ClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value, @CurrencyCode as CurrencyCode, '0' as AuthIDResp, @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
   RETURN (1)          
  END

  IF (@SOCID <> '')
  BEGIN          
   SELECT @WithDrawlCharges=WithDrawlCharges,@AcquiringChargesAccountID=AcquiringChargesAccountID,@ExciseDutyAccountID=ExciseDutyAccountID,          
   @ExciseDutyPercentage=ExciseDutyPercentage    
   From t_SOCATMCharges
   WHERE (OurBranchID = @OurBranchID AND ProductID = @mProductID AND SOCID = @SOCID AND ATMChargeID = @ATMChargeID)
  END   
  
  IF @WithdrawlCharges > 0          
  BEGIN
   IF (@ExciseDutyPercentage > 0)          
    SET @ExciseDutyAmount = ROUND(@WithDrawlCharges * @ExciseDutyPercentage / 100, 2)          
  
   IF @WithdrawlCharges > (@ClearBalance-@vFreezeAmount-@MinBalance-@ExciseDutyAmount)          
   BEGIN   --LOW_BALANCE            
   
    INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, 
    ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, 
    CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
    VALUES (@OurBranchID, @AccountID, @RefNo, @STAN, @DateTime, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), 0, 0, 0, 0, '', '', '', '', '', '', 0, @StatusType, '', '', 
	'', '', '', '', '', '', '', '', '', '', '04', 'Low Balance', '3XX Trx', '0', '')
   
    SELECT @Status=4,          
    @AvailableBalance = ( isnull(@ClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@MinBalance,0) )*100,          
    @ClearBalance     = ( IsNull(@ClearBalance,0)-IsNull(@vClearedEffects,0) )*100          
              
    SELECT @Status As Status, @ClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value, @CurrencyCode as CurrencyCode, '0' as AuthIDResp, @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
    RETURN (1)
   END  
  
   SET @VoucherID = @mScrollNo     
   
   select @VatReferenceID = dbo.fn_GenerateText(10)

   Set @mSerialNo = @mSerialNo + 1          
   Set @mTrxType = 'D'          
   Set @IsCredit = 0  
  
   INSERT INTO t_ATM_TransferTransaction          
   (            
    ScrollNo,SerialNo,RefNo,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,valueDate,wDate,
    PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,[Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,
    MerchantType,STAN,SettlmntAmount,ConvRate,AcqCountryCode,CurrCodeTran,CurrCodeSett,ATMStatus,POSEntryMode,POSConditionCode,POSPINCaptCode,
    AcqInstID,RetRefNum,AuthIDResp,CardAccptID,CardAccptNameLoc,VISATrxID,ProcCode,CAVV,ResponseCode,ForwardInstID    
   )              
   VALUES            
   (            
    @mScrollNo,@mSerialNo,@RefNo,@OurBranchID,'',@AccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,@mwDate,          
    @mwDate + ' ' + convert(varchar(12), GetDate(), 114),@DateTime,@mTrxType,'C',NULL,@WithdrawlCharges,@mForeignAmount,@mExchangeRate,          
    @mDescriptionID,@mDescriptionCharges,@ChannelID,@mBankCode,@mRemarksCharges,@mOperatorID,@mSupervisorID,'',@STAN,@WithdrawlCharges,@mExchangeRate,
    '784','784','784','C','','','','',@STAN,'','','','',@mProcCode,'','',''    
   )          
     
   INSERT INTO t_Transactions          
   (            
    OurBranchID,ScrollNo,SerialNo,RefNo,wDate,AccountType,DocumentType,AccountID,AccountName,ProductID,CurrencyID,ValueDate,TrxType,ChequeID,
    ChequeDate,Amount,ForeignAmount,ExchangeRate,ProfitLoss,MeanRate,DescriptionID,[Description],BankCode,BranchCode,TrxPrinted,GLID,[Status],
    IsLocalCurrency,OperatorID,SupervisorID,AdditionalData,IsMainTrx,DocType,GLVoucherID,Remarks,MerchantType,STAN,SettlmntAmount,ConvRate,
    AcqCountryCode,CurrCodeTran,CurrCodeSett,ATMStatus,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,CardAccptID,
    CardAccptNameLoc,VISATrxID,ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,ForwardInstID, VATID,[Percent],TClientId,TAccountid,TProductId,VatReferenceID
   )            
   VALUES             
   (            
    @OurBranchID,@mScrollNo,@mSerialNo,@RefNo,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@mAccountType,'ATT',@AccountID,@mAccountName,
    @mProductID,@mCurrencyID,@mwDate,@mTrxType,'',@mwDate,@WithdrawlCharges,@mForeignAmount,@mExchangeRate,0,@mExchangeRate,@mDescriptionID,
    @mDescriptionCharges,@mBankCode,'',0,NULL,'C',@mIsLocalCurrency,@mOperatorID,@mSupervisorID,null,'1','ATT',@VoucherID,@mRemarksCharges,'',@STAN,
    @WithdrawlCharges,@mExchangeRate,'784','784','784','C','','','','',@STAN,'','','','',@ChannelId,@ChannelRefID,@mProcCode,'','','',
	(case when @ExciseDutyAmount > 0 then '004' else NULL end), 
    (case when @ExciseDutyAmount > 0 then @ExciseDutyPercentage else NULL end),
    @ClientID,@AccountID,@mProductID,
	(case when @ExciseDutyAmount > 0 then @VatReferenceID else NULL end)
   )          
   
   select @GLControl = ISNULL(GLControl, '') from t_Products where OurBranchID = @OurBranchID and ProductID = @mProductID           
                 
   insert into t_GLTransactions           
   (          
    OurBranchID,RefNo,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,ExchangeRate,IsCredit,
    TransactionType,TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,IsMainTrx,DocType,Remarks,MerchantType,STAN,
    SettlmntAmount,ConvRate,AcqCountryCode,CurrCodeTran,CurrCodeSett,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,
    CardAccptID,CardAccptNameLoc,VISATrxID,ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,ForwardInstID, VATID,[Percent],TClientId,TAccountid,TProductId
   )            
   values           
   (          
    @OurBranchID,@RefNo,@GLControl,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@mwDate,@mDescriptionID,    
    @mDescriptionCharges,@mCurrencyID,@WithdrawlCharges,@mForeignAmount,@mExchangeRate,@IsCredit,'ATMTransfer','L',@mOperatorID,@mSupervisorID,'',
    @mScrollNo,@mSerialNo,null,'0','ATT',@mRemarksCharges,'',@STAN,@WithdrawlCharges,@mExchangeRate,'784','784','784','','','','',@STAN,'','','','',
    @ChannelId,@ChannelRefID,@mProcCode,'','','',
	(case when @ExciseDutyAmount > 0 then '004' else NULL end), 
    (case when @ExciseDutyAmount > 0 then @ExciseDutyPercentage else NULL end),
    @ClientID,@AccountID,@mProductID
   )
  
   -- Credit GL Charges Account          
  
   SELECT @gAccountID=AccountID, @gAccountName=[Description],@gProductID='GL', @gCurrencyID=CurrencyID,@gAccountType='G' FROM t_GL             
   WHERE (OurBranchID = @OurBranchID) AND (AccountID =  @AcquiringChargesAccountID)            
             
   SET @mSerialNo = @mSerialNo + 1            
   SET @mTrxType = 'C'            
   SET @IsCredit = 1          
  
   INSERT INTO t_ATM_TransferTransaction          
   (            
    ScrollNo,SerialNo,RefNo,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,valueDate,wDate,
    PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,[Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,
    MerchantType,STAN,SettlmntAmount,ConvRate,AcqCountryCode,CurrCodeTran,CurrCodeSett,ATMStatus,POSEntryMode,POSConditionCode,POSPINCaptCode,
    AcqInstID,RetRefNum,AuthIDResp,CardAccptID,CardAccptNameLoc,VISATrxID,ProcCode,CAVV,ResponseCode,ForwardInstID    
   )              
   VALUES            
   (            
    @mScrollNo,@mSerialNo,@RefNo,@OurBranchID,'',@gAccountID,@gAccountName,@gAccountType,@gProductID,@gCurrencyID,@mIsLocalCurrency,@mwDate,          
    @mwDate + ' ' + convert(varchar(12), GetDate(), 114),@DateTime,@mTrxType,'C',NULL,@WithdrawlCharges,@mForeignAmount,@mExchangeRate,
    @mDescriptionID,@mDescriptionCharges,'',@mBankCode,@mRemarksCharges,@mOperatorID,@mSupervisorID,'',@STAN,@WithdrawlCharges,@mExchangeRate,'784',
    '784','784','C','','','','',@STAN,'0','','','',@mProcCode,'','',''
   )          
            
   insert into t_GLTransactions           
   (          
    OurBranchID,RefNo,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,ExchangeRate,IsCredit,
    TransactionType,TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,IsMainTrx,DocType,Remarks,MerchantType,STAN,
    SettlmntAmount,ConvRate,AcqCountryCode,CurrCodeTran,CurrCodeSett,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,
    CardAccptID,CardAccptNameLoc,VISATrxID,ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,ForwardInstID, VATID,[Percent],TClientId,TAccountid,TProductId
   )            
   values     
   (          
    @OurBranchID,@RefNo,@gAccountID,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@mwDate,@mDescriptionID,@mDescriptionCharges,
    @mCurrencyID,@WithdrawlCharges,@mForeignAmount,@mExchangeRate,@IsCredit,'ATMTransfer','L',@mOperatorID, @mSupervisorID, '',@mScrollNo,@mSerialNo,
    null,'0','ATT',@mRemarksCharges,'',@STAN,@WithdrawlCharges,@mExchangeRate,'784','784','784','','','','',@STAN,'0','','','',@ChannelId,@ChannelRefID,
    @mProcCode,'','','',
	(case when @ExciseDutyAmount > 0 then '004' else NULL end), 
    (case when @ExciseDutyAmount > 0 then @ExciseDutyPercentage else NULL end),
    @ClientID,@AccountID,@mProductID
   )          
            
   Insert Into BalChk           
   (          
    OurBranchID, AccountID, ProductID, [Name], LastUpdateTime, ClearBalance, Limit, Effects, Shadow, FreezeAmt, MinBalance, TotalBalance, AvailableBalance          
   )          
   SELECT OurBranchID, AccountID, ProductID, [Name], @mwDate + ' ' + convert(varchar(12), GetDate(), 114), ISNULL(ClearBalance,0), ISNULL(Limit,0), 
   ISNULL(Effects,0),ISNULL(ShadowBalance,0), ISNULL(FreezeAmount,0), @MinBalance,           
   (isnull(@Limit,0)+ISNULL(ClearBalance,0)-ISNULL(Effects,0)),          
   (isnull(@Limit,0)+ISNULL(ClearBalance,0)-ISNULL(FreezeAmount,0)-@MinBalance)          
   FROM t_AccountBalance WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID          
   
   IF (@ExciseDutyAmount > 0)          
   BEGIN          
             
    Set @mSerialNo = @mSerialNo + 1          
    Set @mTrxType = 'D'          
    Set @IsCredit = 0          
  
    INSERT INTO t_ATM_TransferTransaction          
    (            
     ScrollNo,SerialNo,RefNo,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,valueDate,wDate,
     PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,[Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,
     MerchantType,STAN,SettlmntAmount,ConvRate,AcqCountryCode,CurrCodeTran,CurrCodeSett,ATMStatus,POSEntryMode,POSConditionCode,POSPINCaptCode,
     AcqInstID,RetRefNum,AuthIDResp,CardAccptID,CardAccptNameLoc,VISATrxID,ProcCode,CAVV,ResponseCode,ForwardInstID    
    )              
    VALUES            
    (            
     @mScrollNo,@mSerialNo,@RefNo,@OurBranchID,'',@AccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,@mwDate,          
     @mwDate + ' ' + convert(varchar(12), GetDate(), 114),@DateTime,@mTrxType,'C',NULL,@ExciseDutyAmount,@mForeignAmount,@mExchangeRate,          
     @mDescriptionIDChg2,@mDescriptionHCharges,'',@mBankCode,@mRemarksVATCharges,@mOperatorID,@mSupervisorID,'',@STAN,@WithdrawlCharges,@mExchangeRate,
     '784','784','784','C','','','','',@STAN,'0','','','',@mProcCode,'','',''
    )          
             
    INSERT INTO t_Transactions          
    (            
     OurBranchID,ScrollNo,SerialNo,RefNo,wDate,AccountType,DocumentType,AccountID,AccountName,ProductID,CurrencyID,ValueDate,TrxType,ChequeID,
     ChequeDate,Amount,ForeignAmount,ExchangeRate,ProfitLoss,MeanRate,DescriptionID,[Description],BankCode,BranchCode,TrxPrinted,GLID,[Status],
     IsLocalCurrency,OperatorID,SupervisorID,AdditionalData,IsMainTrx,DocType,GLVoucherID,Remarks,MerchantType,STAN,SettlmntAmount,ConvRate,
     AcqCountryCode,CurrCodeTran,CurrCodeSett,ATMStatus,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,CardAccptID,
     CardAccptNameLoc,VISATrxID,ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,ForwardInstID, VATID,[Percent],TClientId,TAccountid,TProductId,VatReferenceID
    )            
    VALUES             
    (            
     @OurBranchID,@mScrollNo,@mSerialNo,@RefNo,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@mAccountType,'ATT',@AccountID,@mAccountName,
     @mProductID,@mCurrencyID,@mwDate,@mTrxType,'',@mwDate,@ExciseDutyAmount,@mForeignAmount,@mExchangeRate,0,@mExchangeRate,@mDescriptionIDChg2,
     @mDescriptionHCharges,@mBankCode,'',0,NULL,'C',@mIsLocalCurrency,@mOperatorID,@mSupervisorID,null,'1','ATT',@VoucherID,@mRemarksVATCharges,'',
     @STAN,@WithdrawlCharges,@mExchangeRate,'784','784','784','C','','','','',@STAN,'0','','','',@ChannelId,@ChannelRefID,@mProcCode,'','','',
	 (case when @ExciseDutyAmount > 0 then '004' else NULL end), 
     (case when @ExciseDutyAmount > 0 then @ExciseDutyPercentage else NULL end),
     @ClientID,@AccountID,@mProductID,
	 (case when @ExciseDutyAmount > 0 then @VatReferenceID else NULL end)
    )          
             
    insert into t_GLTransactions           
    (          
     OurBranchID,RefNo,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,ExchangeRate,IsCredit,
     TransactionType,TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,IsMainTrx,DocType,Remarks,MerchantType,STAN,
     SettlmntAmount,ConvRate,AcqCountryCode,CurrCodeTran,CurrCodeSett,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,
     CardAccptID,CardAccptNameLoc,VISATrxID,ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,ForwardInstID, VATID,[Percent],TClientId,TAccountid,TProductId
    )            
    values           
    (          
     @OurBranchID,@RefNo,@GLControl,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@mwDate,@mDescriptionIDChg2,
     @mDescriptionHCharges,@mCurrencyID,@ExciseDutyAmount,@mForeignAmount,@mExchangeRate,@IsCredit,'ATMTransfer','L',@mOperatorID, @mSupervisorID, 
     '',@mScrollNo,@mSerialNo,null,'0','ATT',@mRemarksVATCharges,'',@STAN,@WithdrawlCharges,@mExchangeRate,'784','784','784','','','','',@STAN,'',
     '','','',@ChannelId, @ChannelRefID,@mProcCode,'','','',
	 (case when @ExciseDutyAmount > 0 then '004' else NULL end), 
     (case when @ExciseDutyAmount > 0 then @ExciseDutyPercentage else NULL end),
     @ClientID,@AccountID,@mProductID
    )
  
    -- Credit GL Excise Duty Account           
             
    SELECT @gAccountID=AccountID, @gAccountName=[Description],@gProductID='GL', @gCurrencyID=CurrencyID,@gAccountType='G' FROM t_GL             
    WHERE (OurBranchID = @OurBranchID) AND (AccountID =  @ExciseDutyAccountID)            
             
    SET @mSerialNo = @mSerialNo + 1            
    SET @mTrxType = 'C'            
    SET @IsCredit = 1          
             
    INSERT INTO t_ATM_TransferTransaction          
    (            
     ScrollNo,SerialNo,RefNo,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,valueDate,wDate,
     PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,[Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,
     MerchantType,STAN,SettlmntAmount,ConvRate,AcqCountryCode,CurrCodeTran,CurrCodeSett,ATMStatus,POSEntryMode,POSConditionCode,POSPINCaptCode,
     AcqInstID,RetRefNum,AuthIDResp,CardAccptID,CardAccptNameLoc,VISATrxID,ProcCode,CAVV,ResponseCode,ForwardInstID    
    )              
    VALUES            
    (            
     @mScrollNo,@mSerialNo,@RefNo,@OurBranchID,'',@gAccountID,@gAccountName,@gAccountType,@gProductID,@gCurrencyID,@mIsLocalCurrency,@mwDate,          
     @mwDate + ' ' + convert(varchar(12), GetDate(), 114),@DateTime,@mTrxType,'C',NULL,@ExciseDutyAmount,@mForeignAmount,@mExchangeRate,
     @mDescriptionIDChg2,@mDescriptionHCharges,'',@mBankCode,@mRemarksVATCharges,@mOperatorID,@mSupervisorID,'',@STAN,@WithdrawlCharges,
     @mExchangeRate,'784','784','784','C','','','','',@STAN,'0','','','',@mProcCode,'','','' 
    )          
             
    insert into t_GLTransactions           
    (          
     OurBranchID,RefNo,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,ExchangeRate,IsCredit,
     TransactionType,TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,IsMainTrx,DocType,Remarks,MerchantType,STAN,
     SettlmntAmount,ConvRate,AcqCountryCode,CurrCodeTran,CurrCodeSett,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,
     CardAccptID,CardAccptNameLoc,VISATrxID,ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,ForwardInstID, VATID,[Percent],TClientId,TAccountid,TProductId,VatReferenceID
    )            
    values           
    (          
     @OurBranchID,@RefNo,@gAccountID,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@mwDate,@mDescriptionIDChg2,
     @mDescriptionHCharges,@mCurrencyID,@ExciseDutyAmount,@mForeignAmount,@mExchangeRate,@IsCredit,'ATMTransfer','L',@mOperatorID,@mSupervisorID,
     '',@mScrollNo,@mSerialNo,null,'0','ATT',@mRemarksVATCharges,'',@STAN,@WithdrawlCharges,@mExchangeRate,'784','784','784','','','','',@STAN,
     '0','','','',@ChannelId,@ChannelRefID,@mProcCode,'','','',
	 (case when @ExciseDutyAmount > 0 then '004' else NULL end), 
     (case when @ExciseDutyAmount > 0 then @ExciseDutyPercentage else NULL end),
     @ClientID,@AccountID,@mProductID,
	 (case when @ExciseDutyAmount > 0 then @VatReferenceID else NULL end)
    )          
             
    Insert Into BalChk           
    (          
     OurBranchID, AccountID, ProductID, [Name], LastUpdateTime, ClearBalance, Limit, Effects, Shadow, FreezeAmt, MinBalance, TotalBalance, AvailableBalance          
    )          
    SELECT OurBranchID, AccountID, ProductID, [Name], @mwDate + ' ' + convert(varchar(12), GetDate(), 114), ISNULL(ClearBalance,0), ISNULL(Limit,0), ISNULL(Effects,0),           
    ISNULL(ShadowBalance,0), ISNULL(FreezeAmount,0), @MinBalance,           
    (isnull(@Limit,0)+ISNULL(ClearBalance,0)-ISNULL(Effects,0)),          
    (isnull(@Limit,0)+ISNULL(ClearBalance,0)-ISNULL(FreezeAmount,0)-@MinBalance)          
    FROM t_AccountBalance WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID          
   END      
  END
 END 

 if (@StatusType = 'SC')
 BEGIN
 
  Insert into t_DebitCardHistory (CreateBy, CreateTime, CreateTerminal, UpdateBy, UpdateTime, UpdateTerminal, AuthStatus, VerifyBy, VerifyTime, 
  VerifyTerminal, VerifyStatus, SuperviseBy, SuperviseTime, SuperviseTerminal, frmName, OurBranchID, AccountID, DebitCardToken, CardName, 
  DebitCardCategory, CardType, ExpiryDate, [Status])
  SELECT @ChannelID, getdate(), '3XX', NULL, NULL, NULL, '', NULL, NULL, NULL, VerifyStatus, NULL, NULL, NULL, frmName, OurBranchID, 
  AccountID, DebitCardToken, CardName, DebitCardCategory, CardType, ExpiryDate, Status
  FROM t_DebitCards WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND DebitCardToken = @CardNumber
  
  Update t_DebitCards set [Status] = @mStatus, UpdateBy = @ChannelID, UpdateTime = getdate(), UpdateTerminal = '3XX'
  WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND DebitCardToken = @CardNumber
  
  SELECT @ClearBalance=IsNull(ab.ClearBalance,0),@vClearedEffects=IsNull(ab.Effects,0),@mAccountName=ab.Name,@Limit=ISNULL(ab.Limit,0),    
  @mAccountType='C',@mProductID=ab.ProductID, @mCurrencyID=c.CurrencyID,@vFreezeAmount=ISNULL(ab.FreezeAmount,0),@vIsClose=ISNULL(a.Status,''),          
  --@value=ISNULL(p.productatmcode,'00'), 
  @MinBalance=ISNULL(p.MinBalance,0), @CurrencyCode = ISNULL(c.CurrencyCode,''),
  @MobileNo=ISNULL(cust.MobileNo,''),@Email=ISNULL(cust.Email,''),@ClientID=cust.ClientID
  FROM t_AccountBalance ab           
  INNER JOIN t_Account a ON ab.OurBranchID = a.OurBranchID AND ab.AccountID = a.AccountID AND ab.ProductID = a.ProductID          
  INNER JOIN t_Products p ON ab.OurBranchID = p.OurBranchID AND ab.ProductID = p.ProductID          
  INNER JOIN t_Currencies c ON ab.OurBranchID = c.OurBranchID AND p.CurrencyID = c.CurrencyID          
  INNER JOIN t_Customer cust ON ab.OurBranchID = cust.OurBranchID AND a.ClientID = cust.ClientID
  WHERE (ab.OurBranchID = @OurBranchID) AND (ab.AccountID = @AccountID)            
            
  IF @Limit > 0 AND @vCheckLimit = 1
  BEGIN          
   SET @ClearBalance=(@ClearBalance+@Limit)          
  END          

 END
 ELSE if (@StatusType = 'RP')
 BEGIN

  --Update t_DebitCards set Status = 'B', UpdateBy = @ChannelID, UpdateTime = @DateTime
  --WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND DebitCardToken = @PreviousStatus
  
  Insert into t_DebitCardHistory (CreateBy, CreateTime, CreateTerminal, UpdateBy, UpdateTime, UpdateTerminal, AuthStatus, VerifyBy, VerifyTime, 
  VerifyTerminal, VerifyStatus, SuperviseBy, SuperviseTime, SuperviseTerminal, frmName, OurBranchID, AccountID, DebitCardToken, CardName, 
  DebitCardCategory, CardType, ExpiryDate, [Status])
  SELECT @ChannelID, getdate(), '3XX', NULL, NULL, NULL, '', NULL, NULL, NULL, VerifyStatus, NULL, NULL, NULL, frmName, OurBranchID, 
  AccountID, DebitCardToken, CardName, DebitCardCategory, CardType, ExpiryDate, Status
  FROM t_DebitCards WHERE (OurBranchID = @OurBranchID) AND (DebitCardToken = @PreviousStatus)
  
  Insert into t_DebitCards (CreateBy, CreateTime, CreateTerminal, UpdateBy, UpdateTime, UpdateTerminal, AuthStatus, VerifyBy, VerifyTime, 
  VerifyTerminal, VerifyStatus, SuperviseBy, SuperviseTime, SuperviseTerminal, frmName, OurBranchID, AccountID, DebitCardToken, CardName, 
  DebitCardCategory, CardType, ExpiryDate, [Status])
  SELECT @ChannelID, getdate(), CreateTerminal, NULL, NULL, NULL, '', NULL, NULL, NULL, VerifyStatus, NULL, NULL, NULL, frmName, OurBranchID, 
  AccountID, @CurrentStatus, CardName, DebitCardCategory, CardType, ExpiryDate, 'I'
  FROM t_DebitCards WHERE (OurBranchID = @OurBranchID) AND (DebitCardToken = @PreviousStatus)
  
  SELECT @ClearBalance=IsNull(ab.ClearBalance,0),@vClearedEffects=IsNull(ab.Effects,0),@mAccountName=ab.Name,@Limit=ISNULL(ab.Limit,0),    
  @mAccountType='C',@mProductID=ab.ProductID, @mCurrencyID=c.CurrencyID,@vFreezeAmount=ISNULL(ab.FreezeAmount,0),@vIsClose=ISNULL(a.Status,''),          
  @value=ISNULL(p.productatmcode,'00'), 
  @MinBalance=ISNULL(p.MinBalance,0), @CurrencyCode = ISNULL(c.CurrencyCode,''),
  @MobileNo=ISNULL(cust.MobileNo,''),@Email=ISNULL(cust.Email,''),@ClientID=cust.ClientID
  FROM t_AccountBalance ab           
  INNER JOIN t_Account a ON ab.OurBranchID = a.OurBranchID AND ab.AccountID = a.AccountID AND ab.ProductID = a.ProductID          
  INNER JOIN t_Products p ON ab.OurBranchID = p.OurBranchID AND ab.ProductID = p.ProductID          
  INNER JOIN t_Currencies c ON ab.OurBranchID = c.OurBranchID AND p.CurrencyID = c.CurrencyID          
  INNER JOIN t_Customer cust ON ab.OurBranchID = cust.OurBranchID AND a.ClientID = cust.ClientID
  WHERE (ab.OurBranchID = @OurBranchID) AND (ab.AccountID = @AccountID)            
            
  IF @Limit > 0 AND @vCheckLimit = 1
  BEGIN          
   SET @ClearBalance=(@ClearBalance+@Limit)          
  END          
 
 END
 ELSE
 BEGIN
 
  INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, 
  ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, 
  CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
  VALUES (@OurBranchID, @AccountID, @RefNo, @STAN, @DateTime, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), 0, 0, 0, 0, '', '', '', '', '', '', 0, @StatusType, '', '', 
  '', '', '', '', '', '', '', '', '', '', '29', 'Invalid Card Status type', '3XX Trx', '0', '')
	
  SELECT @Status=29, --Invalid Card Status Type
  @AvailableBalance = ( isnull(@ClearBalance,0)-IsNull(@vClearedEffects,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@MinBalance,0) )*100,          
  @ClearBalance     = ( IsNull(@ClearBalance,0)-IsNull(@vClearedEffects,0) )*100          
           
  SELECT @Status As Status, @ClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value, @CurrencyCode as CurrencyCode, '0' as AuthIDResp, @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
  RETURN (1)
 END

 SELECT @Status    = 0,           
 @AvailableBalance = ( isnull(@ClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@MinBalance,0) )*100,          
 @ClearBalance     = ( IsNull(@ClearBalance,0)-IsNull(@vClearedEffects,0) )*100          
            
 SELECT @Status As Status,@ClearBalance As ClearBalance,@AvailableBalance as  AvailableBalance ,@Value as value, @CurrencyCode as CurrencyCode, '0' as AuthIDResp, @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
 RETURN 0             

END