﻿CREATE PROCEDURE [dbo].[spc_InwardChequeHonour]      
        
 @OurBranchID varchar(30),        
 @AccountID varchar(30),         
 @SerialNo int,        
 @SupervisorID varchar(30),
 @AddData text = ''
 AS        
         
 SET NOCOUNT ON        
       
 Declare @Status char(1),        
  @LocalCurrency varchar(30),        
  @CurrencyID varchar(30),        
  @ProductID varchar(30),        
  @ChequeID varchar(30),        
  @ValueDate datetime,        
  @wDate datetime,        
  @AccountType char(1),        
  @Amount money,        
  @ForeignAmount money,        
  @ClearBalance money,        
  @Effects money,        
  @Limit money,        
  @FreezeAmount money,        
  @MinBalance money,        
  @OperatorID varchar(30),        
  @TempAmount money,
  @CanGoDr as Bit        
 
 DECLARE @VoucherID decimal(24,0)
 DECLARE @GLID varchar(30)
 DECLARE @GLControl varchar(30)
 DECLARE @AccountName varchar(100)
 DECLARE @ChequeDate varchar(30)
 DECLARE @ExchangeRate decimal(24,6)
 DECLARE @DescriptionID varchar(30)
 DECLARE @Description varchar(100)
 DECLARE @BankCode varchar(30)
 DECLARE @BranchCode varchar(30)
 DECLARE @AppWHTax int
 DECLARE @WHTaxMode char(1)
 DECLARE @WHTaxAmount money
 DECLARE @WHTScrollNo int
 DECLARE @TrxType CHAR(1)
 DECLARE @IsLocalCurrency int
 declare @RefNo varchar(100)
 declare @Year varchar(4)
 declare @Month varchar(2)
 declare @Day varchar(2)
 
 SET @TrxType = 'D'
 
 Select @LocalCurrency = LocalCurrency From t_GlobalVariables Where OurBranchID = @OurBranchID
 
 Select @Status = Status , @ProductID = ProductID ,@CurrencyID = CurrencyID , @Amount = Amount ,  @ForeignAmount = ForeignAmount,
 @OperatorID = OperatorID , @ChequeID = ChequeID , @wDate = wDate , @ValueDate = ValueDate , @AccountType = AccountType,
 @AccountName = AccountName, @ChequeDate = ChequeDate, @ExchangeRate = ExchangeRate, @DescriptionID = DescriptionID,
 @Description = [Description], @BankCode = BankID, @BranchCode = BranchID, @AddData = AdditionalData, @AppWHTax = AppWHTax,
 @WHTaxMode = WHTaxMode, @WHTaxAmount = WHTaxAmount, @WHTScrollNo = WHTScrollNo
 From t_InwardClearing
 Where OurBranchID = @OurBranchID AND AccountID = @AccountID AND SerialNo = @SerialNo
 
 set @Year = CONVERT(varchar(4),YEAR(@wDate))
 
 if MONTH(@wDate) < 10
   set @Month = '0'+CONVERT(varchar(4),MONTH(@wDate))
 else
   set @Month = CONVERT(varchar(4),MONTH(@wDate))
 
 if DAY(@wDate) < 10
   set @Day = '0'+CONVERT(varchar(4),DAY(@wDate))
 else
   set @Day = CONVERT(varchar(4),DAY(@wDate))
 
 set @RefNo = 'ID-'+@OurBranchID+'-'+@Year+@Month+@Day
 
 IF UPPER(@LocalCurrency) = UPPER(@CurrencyID)
  Select @IsLocalCurrency = 1, @ExchangeRate = 1, @ForeignAmount = 0
 ELSE
  Select @IsLocalCurrency = 0
 
 IF isnull(@Status,'') = ''
  BEGIN
   Select '1' as RetStatus /* Transaction Already Honoured */        
   Return        
  END        
 ELSE        
  IF @Status <> '*' AND upper(@Status) <> 'E' AND upper(@Status) <> 'U'        
   BEGIN        
        Select '2' as RetStatus /* Error : Only Cheques Flagged For Insufficient Funds Can Be Hounoured */        
    Return        
   END        
  ELSE        
   BEGIN        
    
    IF @AccountType = 'C'        
           
     Select @ClearBalance = ClearBalance, @Effects = Effects, @Limit = Limit,      
       @FreezeAmount = FreezeAmount, @MinBalance = MinBalance, @CanGoDr = CanGoInDebit      
     From t_AccountBalance a      
     INNER JOIN t_Products p ON a.OurBranchID = p.OurBranchID AND a.ProductID = p.ProductID      
     Where a.OurBranchID = @OurBranchID AND a.AccountID = @AccountID        
          
    ELSE        
           
     Select @ClearBalance = Case @CurrencyID WHEN @LocalCurrency THEN Balance ELSE ForeignBalance END,        
       @Effects = 0 , @Limit = 0 , @FreezeAmount = 0 , @MinBalance = 0        
     From t_GL Where OurBranchID = @OurBranchID AND AccountID = @AccountID        
           
    IF @CurrencyID = @LocalCurrency        
     SET @TempAmount = @Amount        
    ELSE        
     SET @TempAmount = @ForeignAmount        
        
    IF (@AccountType = 'C')  AND (@TempAmount) > (@ClearBalance + @Limit - @FreezeAmount - @MinBalance)        
     BEGIN        
   IF @CanGoDr = 1        
       BEGIN        
        Select '3' as RetStatus /* Remote Pass Supervision */        
        Return        
       END        
      ELSE        
       BEGIN        
        Select '4' as RetStatus /* Trx Can not go in debit - Product Special Condition */        
        Return        
       END        
     END         
    ELSE         
     BEGIN        
     /* Update Inward Transaction File */        
         
         
      Update t_InwardClearing        
      Set Status = ''        
      Where (SerialNo = @SerialNo) and (OurBranchID = @OurBranchID) and (AccountID = @AccountID)         
           
      IF @AccountType = 'C'        
       BEGIN        
        IF @Status = 'U' OR @Status = '*' OR @Status = 'E'        
         IF @CurrencyID = @LocalCurrency        
          Update t_AccountBalance        
          Set ClearBalance = ClearBalance - @Amount ,        
           ShadowBalance = ShadowBalance + @Amount        
          Where (OurBranchID = @OurBranchID) and (AccountID = @AccountID)         
         ELSE        
          Update t_AccountBalance        
          Set ClearBalance = ClearBalance - @ForeignAmount ,        
           LocalClearBalance = LocalClearBalance - @Amount,        
           ShadowBalance = ShadowBalance + @ForeignAmount        
          Where (OurBranchID = @OurBranchID) and (AccountID = @AccountID)         
        ELSE        
         IF @CurrencyID = @LocalCurrency        
          Update t_AccountBalance        
          Set ClearBalance = ClearBalance - @Amount        
          Where (OurBranchID = @OurBranchID) and (AccountID = @AccountID)         
         ELSE        
          Update t_AccountBalance        
          Set ClearBalance = ClearBalance - @ForeignAmount ,        
           LocalClearBalance = LocalClearBalance - @Amount        
          Where (OurBranchID = @OurBranchID) and (AccountID = @AccountID)         
       END        
          
      --Local Remittance Reject    
      Update t_LR_Issuance    
      Set Supervision = 'C',    
      SuperviseBy = @SupervisorID,    
      SuperviseTime = (Convert(varchar(10),@wDate ,101) + ' ' + convert(varchar(20),GetDate(),114))    
      Where (OurBranchID=@OurBranchID) and (PaidScrollNo = @SerialNo) and PaidMode = 'IW'    
      and  (convert(varchar(20), PaidDate, 101) = convert(varchar(20), @wDate, 101))      
          
      SELECT @wDate = Convert(varchar(10),@wDate ,101) + ' ' + convert(varchar(20),GetDate(),114)    
          
      Update t_CustomerStatus        
      Set Status = ''        
      Where (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (SerialNo = @SerialNo)         
      and TransactionType = 'DI'        
           
      /* Create a New record For Superviser */        
      SELECT @wDate = Convert(varchar(10),@wDate ,101) + ' ' + convert(varchar(20),GetDate(),114)        
          
      Insert Into t_SupervisedBy        
      (OurBranchID,AccountID,[Date],ScrollNo,ChequeID,ClearBalance,Effects,Limit,TrxType,Amount,Status,        
      SupervisorID,TransactionCategory)        
      Values        
      (@OurBranchID,@AccountID,@wDate,@SerialNo,@ChequeID,@ClearBalance,@Effects,@Limit,'D',@Amount,@Status,        
      @SupervisorID,'I')        
     END        
   END        
   
  declare @ClearingGLID nvarchar(30)
  declare @IsCredit int
  declare @TransactionMethod char(1)
  
  set @ClearingGLID = ''
  
  if @IsLocalCurrency = 1
  begin
     
     set @ForeignAmount = 0
     set @ExchangeRate = 1
     set @TransactionMethod = 'L'
     
     SELECT @ClearingGLID = G.AccountID
     FROM t_GLParameters P
     INNER JOIN t_GL G ON P.OurBranchID = G.OurBranchID AND P.AccountID = G.AccountID
     WHERE P.OurBranchID = @OurBranchID AND P.SerialID = '2'
     
  end
  else
  begin
     
     set @TransactionMethod = 'A'
     
     SELECT @ClearingGLID = G.AccountID
     FROM t_CurrenciesAccount C
     INNER JOIN t_GLParameters GP ON C.OurBranchID = GP.OurBranchID AND C.HOSerialID = GP.SerialID
     INNER JOIN t_GL G ON G.OurBranchID = GP.OurBranchID AND G.AccountID = GP.AccountID
     WHERE C.OurBranchID = @OurBranchID AND C.CurrencyID = @CurrencyID
     
  end
  
  if @ClearingGLID = ''
  begin
     SELECT '5' as RetStatus
     RETURN
  end
  
  select @VoucherID = ISNULL(MAX(VoucherID),0) + 1 from t_GLTransactions where OurBranchID = @OurBranchID
  select @GLControl = ISNULL(GLControl, '') from t_Products where OurBranchID = @OurBranchID and ProductID = @ProductID
  
  if @AccountType = 'C'
  begin
     
     insert into t_Transactions (OurBranchID, ScrollNo, SerialNo, RefNo, wDate, AccountType, DocumentType, AccountID, AccountName,
     ProductID, CurrencyID, ValueDate, TrxType, ChequeID, ChequeDate, Amount, ForeignAmount, ExchangeRate, ProfitLoss, MeanRate,
     DescriptionID, [Description], BankCode, BranchCode, TrxPrinted, [Status], IsLocalCurrency, OperatorID,
     SupervisorID, AdditionalData, IsMainTrx, DocType, GLVoucherID, AppWHTax, WHTaxMode, WHTaxAmount, WHTScrollNo)
     
     VALUES (@OurBranchID,@SerialNo,'0', '', @wDate, @AccountType, 'ID', @AccountID, @AccountName,
     @ProductID, @CurrencyID,@ValueDate,@TrxType,@ChequeID,@ChequeDate, @Amount, @ForeignAmount,@ExchangeRate,0,@ExchangeRate,
     @DescriptionID,@Description,@BankCode,@BranchCode, 0, 'C', @IsLocalCurrency, @OperatorID, @SupervisorID, 
     @AddData,'1','ID', @VoucherID, @AppWHTax, @WHTaxMode, @WHTaxAmount, @WHTScrollNo)
     
     if @TrxType = 'C'
     begin
       set @IsCredit = 1
       set @DescriptionID = 'IC0'
       set @Description = 'Account ID: ' + @AccountID +', Account Name: ' + @AccountName +' , Module: Inward Clearing Credit Entry...'
     end
     else
     begin
       set @IsCredit = 0
       set @DescriptionID = 'IC1'
       set @Description = 'Account ID: ' + @AccountID +', Account Name: ' + @AccountName +' , Module: Inward Clearing Debit Entry...'
     end
     
     insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],
     CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,
     Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType)
     
     values (@OurBranchID, @GLControl, @VoucherID, '1', @wDate, @ValueDate, @DescriptionID, @Description,
     @CurrencyID, @Amount, @ForeignAmount, @ExchangeRate, @IsCredit, 'Inward', @TransactionMethod, @OperatorID, @SupervisorID,
     '', '0', '0', '', '0', 'ID')
     
  end
  else
  begin
     
     if @TrxType = 'C'
       set @IsCredit = 1
     else
       set @IsCredit = 0
     
     insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],
     CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,
     Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType)
     
     values (@OurBranchID, @AccountID, @VoucherID, '1', @wDate, @ValueDate, @DescriptionID, @Description,
     @CurrencyID, @Amount, @ForeignAmount, @ExchangeRate, @IsCredit, 'Inward', @TransactionMethod, @OperatorID, @SupervisorID,
     '', @SerialNo, '0', @AddData, '1', 'ID')
     
  end
  
  set @IsCredit = 1
  set @DescriptionID = 'ICH'
  set @Description = 'Inward Clearing Credit Entry for Total Honoured Chq(s)'
  
  if not exists (select VoucherID from t_GLTransactions where OurBranchID = @OurBranchID AND AccountID = @ClearingGLID AND IDRefNo = @RefNo)
  begin
     
     set @VoucherID = @VoucherID + 1
     
     insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],
     CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,
     Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType, IDRefNo)
     
     values (@OurBranchID, @ClearingGLID, @VoucherID, '1', @wDate, @ValueDate, @DescriptionID, @Description,
     @CurrencyID, @Amount, @ForeignAmount, @ExchangeRate, @IsCredit, 'Inward', @TransactionMethod, @OperatorID, @SupervisorID,
     '', '0', '0', '', '0', 'ID', @RefNo)
     
  end
  else
  begin
     
     update t_GLTransactions set 
     Amount = Amount + @Amount,
     ForeignAmount = ForeignAmount + @ForeignAmount,
     ExchangeRate = (ExchangeRate + @ExchangeRate)/2
     where OurBranchID = @OurBranchID AND AccountID = @ClearingGLID AND IDRefNo = @RefNo
     
     Update t_GL Set
     Balance = Balance + @Amount,
     ForeignBalance = ForeignBalance + @ForeignAmount
     where OurBranchID = @OurBranchID AND AccountID = @ClearingGLID
     
  end
  
  Select '0' as RetStatus