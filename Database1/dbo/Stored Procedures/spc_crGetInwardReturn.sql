﻿CREATE Procedure [dbo].[spc_crGetInwardReturn]     
(    
 @OurBranchID as Varchar(30)=''     
)    
As    
set nocount on    
    
If Exists(SELECT SerialNo FROM t_InwardClearing WHERE OurBranchID=@OurBranchID And isnull(Status, '') = 'R')    
    
 SELECT '1' As ReturnValue, t.SerialNo, t.AccountType, t.AccountID, t.ChequeID, t.AccountName, t.CurrencyID, t.Amount,     
 t.ForeignAmount, t.ValueDate, t.Reason, t.OperatorID, t.SupervisorID, t.Status, t.BankID, t.BranchID, b.FullName, br.Name, AdditionalData
 FROM t_InwardClearing t     
 LEFT OUTER JOIN t_branches br ON t.OurBranchID = br.OurBranchID AND t.BankID = br.BankID AND t.BranchID = br.BranchID     
 LEFT OUTER JOIN t_Banks b ON t.OurBranchID = b.OurBranchID AND t.BankID = b.BankID    
 WHERE t.OurBranchID=@OurBranchID And t.Status = 'R'     
     
Else    
    
 Select '0' As ReturnValue