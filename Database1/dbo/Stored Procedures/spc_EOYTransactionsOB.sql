﻿CREATE proc [dbo].[spc_EOYTransactionsOB]   (        
@year nvarchar(10)     
)           
as  
SET NOCOUNT ON  
drop table if exists t_EOYOPBalTransaction;  
  
select a.*, b.Balance, b.ForeignBalance,c.RetainedEarningGL,0 IsProcessed into t_EOYOPBalTransaction from              
(select OurBranchID, AccountID, CurrencyID,        
SUM(case IsCredit when 1 then Amount else Amount*-1 end) LCYTrxBalance,  
SUM(case IsCredit when 1 then ForeignAmount else ForeignAmount*-1 end) FCYTrxBalance  
from t_GLTransactions_2020 where --YEAR(ValueDate) = @year and 
Status != 'R' group by OurBranchID, AccountID, CurrencyID)  
a inner join t_GL b on a.OurBranchID = b.OurBranchID and a.AccountID = b.AccountID and b.AccountType IN ('A','L')  
inner join t_Currencies c on c.OurBranchID = a.OurBranchID and c.CurrencyID = a.CurrencyID  
WHERE A.LCYTrxBalance <> 0  
select 'Ok' as ReturnStatus, 'Done' AS ReturnMessage