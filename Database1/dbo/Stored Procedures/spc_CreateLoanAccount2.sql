﻿create procedure [dbo].[spc_CreateLoanAccount2]
(
   @OurBranchID nvarchar(30),
   @ClientID nvarchar(30),
   @ProductID nvarchar(30)
)

AS

SET NOCOUNT ON

declare @counts int
select @counts = COUNT(AccountID) from t_Account where OurBranchID = @OurBranchID and ClientID = @ClientID and ProductID = @ProductID

if @counts = 0
begin
   declare @AccountID nvarchar(30), @AccountPrefix nvarchar(30), @Name nvarchar(max), @CurrencyID nvarchar(30), @desc nvarchar(100)
   
   select @AccountPrefix = AccountPrefix, @desc = [Description], @CurrencyID = CurrencyID
   from t_Products
   where OurBranchID = @OurBranchID and ProductID = @ProductID
   
   select @Name = Name + ' - ' + @desc
   from t_Customer
   where OurBranchID = @OurBranchID and ClientID = @ClientID

   declare @noAccs nvarchar(2)
   if @counts < 9
      set @noAccs = '0' + convert(nvarchar(1), @counts+1)
   else if @counts < 99
      set @noAccs = convert(nvarchar(2), @counts+1)
   else
      set @noAccs = '99'

   select @AccountID = @OurBranchID + @AccountPrefix + @ClientID + @noAccs

   insert into t_Account
   (CreateBy, CreateTime, CreateTerminal, Auth, OurBranchID, AccountID, IBAN, ClientID, Name, ProductID, CurrencyID, Status, OpenDate, ModeOfOperation, frmName, CashTotDr, CashTotCr, CashTotDrF, CashTotCrF, ApplyTax, StatementFrequency)
   values ('System', dbo.GetWorkingDate(@OurBranchID), HOST_NAME(), 0, @OurBranchID, @AccountID, NULL, @ClientID, @Name, @ProductID, @CurrencyID, '', dbo.GetWorkingDate(@OurBranchID), 'Singly', '', 0, 0, 0, 0, 0, 'N')

   insert t_HO_Account(CreateBy, CreateTime, CreateTerminal, OurBranchID, AccountID, ClientID, SuperviseBy, SuperviseTime, SuperviseTerminal)
   values ('System', dbo.GetWorkingDate(@OurBranchID), HOST_NAME(), @OurBranchID, @AccountID, @ClientID, 'System', dbo.GetWorkingDate(@OurBranchID), HOST_NAME())

   insert into t_AccountCIF (OurBranchID, AccountID, ClientID) values (@OurBranchID, @AccountID, @ClientID)

   insert into t_AccountBalance (CreateBy,CreateTime,CreateTerminal,OurBranchID,AccountID,ProductID,[Name], ClearBalance,Effects,Limit,ShadowBalance,LocalClearBalance,LocalEffects,IsFreezed,FreezeAmount,ClearedEffects)
   values ('System', dbo.GetWorkingDate(@OurBranchID), HOST_NAME(), @OurBranchID, @AccountID, @ProductID, @Name, 0, 0, 0, 0, 0, 0, 0, 0, 0)

   insert into t_AccountFunctions(CreateBy,CreateTime,CreateTerminal,OurBranchID,AccountID,ProductID,[Function],SignatureInstructions,OtherInstructions)
   values ('System', dbo.GetWorkingDate(@OurBranchID), HOST_NAME(), @OurBranchID, @AccountID, @ProductID, '', '', '')

   insert into t_AccountOperatedBy(CreateBy,CreateTime,CreateTerminal, OurBranchID, AccountID, ColumnID, [Name], [Address],Phone, NIC,  MobileNo, Email, AuthStatus,VerifyStatus)
   values ('System', dbo.GetWorkingDate(@OurBranchID), HOST_NAME(), @OurBranchID, @AccountID, (select ISNULL(max(ColumnId),0)+1 from t_AccountOperatedBy), @Name, '', '', '', '', '','','Y')

end

select 'Ok' AS ReturnStatus