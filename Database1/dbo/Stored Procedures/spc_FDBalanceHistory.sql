﻿CREATE   PROCEDURE [dbo].[spc_FDBalanceHistory]          
AS          
SET NOCOUNT ON          
        
INSERT INTO t_FDBalanceHistory        
select c.LASTEOD wDate, a.OurBranchID, a.AccountID, a.RecieptID, a.ProductID, a.Amount, a.ExchangeRate, a.IssueDate, a.StartDate,       
a.MaturityDate, a.Rate, a.Interest, a.InterestPaid, a.InterestPaidupTo, a.Accrual, a.tAccrual, a.AccrualUpto       
from t_TDRIssuance a      
inner join t_Last c on a.OurBranchID = c.OurBranchID    
where isnull(IsClosed,0) = 0  
          
SELECT 'Ok' AS RetStatus