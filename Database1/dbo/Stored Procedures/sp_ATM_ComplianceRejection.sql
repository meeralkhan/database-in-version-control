﻿ 
 
 
create   PROCEDURE [dbo].[sp_ATM_ComplianceRejection]
(              
 @OurBranchID         varchar(30),               
 @AccountID           varchar(30)='',
 @ATMID               varchar(30)='',
 @Amount              numeric(18,6)=0,              
 @USDAmount           numeric(18,6)=0,              
 @OtherCurrencyAmount numeric(18,6)=0,              
 @USDRate             decimal(16,8)=0,              
 @RefNo               varchar(100)='',
 @PHXDate             datetime='',
 @MerchantType        varchar(30)='',              
 @AckInstIDCode       varchar(15)='',
 @NameLocation        varchar(50)='',
 @SettlmntAmount      numeric(18,6)=0,        
 @ConvRate            decimal(15,9)=0,        
 @AcqCountryCode      varchar(3)='',        
 @CurrCodeTran        varchar(3)='',        
 @CurrCodeSett        varchar(3)='',        
 @ForwardInstID    varchar(10)='',        
 @ProcCode        varchar(50)='',        
 @POSEntryMode    varchar(3)='',        
 @POSConditionCode   varchar(2)='',        
 @POSPINCaptCode   varchar(2)='',        
 @RetRefNum     varchar(15)='',        
 @CardAccptID    varchar(15)='',        
 @VISATrxID     varchar(15)='',        
 @CAVV      char(1)='',        
 @ResponseCode    varchar(2)='',        
 @ExtendedData    varchar(2)='',    
 @MessageType    varchar(10)='', 
 @IMTFRespCode   varchar(2)='', 
 @IMTFRespDescription   varchar(100)='', 
 @AuthIDResp varchar(6)='',
 @NewRecord           bit=1
)              
AS          
BEGIN         
         
 SET NOCOUNT ON        
        
 DECLARE @mwDate as datetime              
 DECLARE @mForeignAmount as numeric(18,6)           
  SET @mForeignAmount=0        
 DECLARE @mExchangeRate as numeric(18,6)        
  SET @mExchangeRate=1        
 DECLARE @mDescription as varchar(255)        
 DECLARE @mDescriptionChgs as varchar(255)        
 DECLARE @mDescriptionVAT as varchar(255)        
 DECLARE @STAN as varchar(30)        
  SET @STAN = RIGHT(@RefNo,6)          
 DECLARE @mOperatorID as varchar(30)              
 DECLARE @mSupervisorID as varchar(30)              
  SET @mSupervisorID = 'N/A'        
 DECLARE @mLastEOD as datetime              
 DECLARE @mCONFRETI as datetime              
 --Checking Variables              
 DECLARE @vCount1 as bit              
 DECLARE @vClearBalance as money              
 DECLARE @AvailableBalance as money              
 DECLARE @Value as varchar(2)          
  SET @value='00'          
 DECLARE @vClearedEffects as money              
 DECLARE @vFreezeAmount as money              
 DECLARE @vMinBalance as money              
 DECLARE @limit as money              
 DECLARE @vIsClose as char(1)        
 DECLARE @vAreCheckBooksAllowed as bit              
 DECLARE @vAllowDebitTransaction as bit              
 DECLARE @Status as int          
 DECLARE @CurrencyCode as varchar(30)        
 DECLARE @LocalCurrencyID as varchar(30)        
 DECLARE @HoldStatus as char(1)        
 DECLARE @vCheckLimit as bit        
  SET @vCheckLimit = 0        
 DECLARE @vODLimitAllow as bit        
  SET @vODLimitAllow = 0        
 DECLARE @IsLocalCurrency as bit        
  SET @IsLocalCurrency = 0         
  SET @mOperatorID = 'ATM-' + @ATMID              
 declare @ScrollNo as int        
 declare @SerialNo as int        
  SET @SerialNo = 1        
 declare @ValueDate as datetime -- Value date for credit clearing currently its set to 1 year.        
 DECLARE @mDescriptionID as varchar(30)
 DECLARE @mDescriptionIDChg1 as varchar(30)              
 DECLARE @mDescriptionIDChg2 as varchar(30)              
         
 Declare @SOCID as varchar(30)        
  SET @SOCID = ''
 Declare @ATMChargeID as varchar(6)        
 Declare @ProductID as varchar(30)        
 DECLARE @WithDrawlCharges as money              
 DECLARE @AcquiringChargesAmount as money              
  SET @WithDrawlCharges = 0              
  SET @AcquiringChargesAmount = 0        
 Declare @NoOfCWTrxATMMonthly as decimal(24,0)        
  SET @NoOfCWTrxATMMonthly = 0        
 Declare @NoOfBITrxATMMonthly as decimal(24,0)
  SET @NoOfBITrxATMMonthly = 0        
 DECLARE @ExciseDutyPercentage as money          
  SET @ExciseDutyPercentage = 0      
 DECLARE @ExciseDutyAmount as money          
  SET @ExciseDutyAmount = 0          
 DECLARE @IsGCC as bit        
  SET @IsGCC = 0        
 DECLARE @MobileNo nvarchar(30)
 DECLARE @Email nvarchar(100)
 DECLARE @rCurrencyID nvarchar(30)
 DECLARE @ClientID nvarchar(30)
 DECLARE @NarrationID nvarchar(30)
  SET @MobileNo = ''
  SET @Email = ''    
  SET @rCurrencyID = ''          
  SET @ClientID = ''          
  SET @NarrationID = ''
 DECLARE @CountryCode as nvarchar(3)
  SET @CountryCode = ''
 DECLARE @NoOfFreeCW as decimal(24,0)
  SET @NoOfFreeCW = 0
 DECLARE @TrxDetails as varchar(50)
  SET @TrxDetails = 'IMTF Declines'
  
 SELECT @Status = -1              
 SELECT @LocalCurrencyID=LocalCurrencyID,@vCheckLimit=CheckLimit FROM t_ATM_GlobalVariables Where OurBranchID = @OurBranchID        
 SELECT @vODLimitAllow=ODLimitAllow FROM t_ATM_Banks WHERE OurBranchID = @OurBranchID AND BankIMD = @AckInstIDCode        
 
 SELECT @mLastEOD=LastEOD,@mwDate=WorkingDate,@mCONFRETI=CONFRETI FROM t_Last WHERE (OurBranchID = @OurBranchID)
 
 select @IsGCC=ISNULL(IsGCC,0), @CountryCode=ISNULL(CountryCode,'') from t_Country where OurBranchID = @OurBranchID AND ShortName = RIGHT(@NameLocation, 2)
 
 
 
 IF (@MessageType = '0220' OR @MessageType = '0221')    
 BEGIN    
  IF (@ForwardInstID = '500000')    
  BEGIN    
   SET @TrxDetails = 'IMTF Declines II'
  END    
  ELSE    
  BEGIN    
   SET @TrxDetails = 'IMTF Declines'
  END    
 END    
 
 if (@CountryCode = '784')          
 BEGIN 
  SET @ATMChargeID = '600000'    
   
  if (@ProcCode = '01')  -- CW Trx    
  BEGIN    
   --SET @mDescriptionID = '801'        
   --SET @mDescriptionIDChg1 = '802'        
   --SET @mDescriptionIDChg2 = '803'    

   SET @mDescriptionID = '200130'        
   SET @mDescriptionIDChg1 = '400710'        
   SET @mDescriptionIDChg2 = '400020'    

   SET @mDescription = 'Cash Withdrawal at local ATM ' + convert(varchar(10), @mwDate, 120)
   SET @mDescriptionChgs = 'Cash Withdrawal from Local ATM Fees ATM('+@ATMID+') FROM ' + @NameLocation + '-' + convert(varchar(20), @PHXDate, 120)
   SET @mDescriptionVAT  = 'Value Added Tax (VAT) for transaction RetRefNum:  - '+@RetRefNum

  END    
  else if (@ProcCode = '00') -- POS    
  BEGIN
   if (LEFT(@POSEntryMode,2) = '01')
   BEGIN
    --set @mDescriptionID = '815'        --old
    set @mDescriptionID = '200120'        --new
    SET @mDescriptionIDChg1 = ''        
    SET @mDescriptionIDChg2 = ''    
    SET @mOperatorID = 'POS-' + @ATMID              
    
    SET @mDescription = 'Online Card Transaction at '+@NameLocation+' MERCH ID: '+@MerchantType+' COUNTRY: '+@AcqCountryCode+' TRX DATE: '+convert(varchar(20), @PHXDate, 120)
    SET @mDescriptionChgs = ''
    SET @mDescriptionVAT  = ''
   END
   ELSE
   BEGIN
    --set @mDescriptionID = '815'        --old
    set @mDescriptionID = '200110'        --new
    SET @mDescriptionIDChg1 = ''        
    SET @mDescriptionIDChg2 = ''    
    SET @mOperatorID = 'POS-' + @ATMID              
    
    SET @mDescription = 'Card Transaction at '+@NameLocation+' MERCH ID: '+@MerchantType+' COUNTRY: '+@AcqCountryCode+' TRX DATE: '+convert(varchar(20), @PHXDate, 120)
    SET @mDescriptionChgs = ''
    SET @mDescriptionVAT  = ''
   END
  END
  else if (@ProcCode = '20') -- Credit Voucher/ Merchandise Return Clearing      
  BEGIN
   --SET @mDescriptionID = '813'      --old
   set @mDescriptionID = '100240'        --new
   SET @mDescriptionIDChg1 = ''        
   SET @mDescriptionIDChg2 = ''    

   SET @mDescription = 'Credit Voucher/ Merchandise Return Transaction Local'
   SET @mDescriptionChgs = ''
   SET @mDescriptionVAT  = ''
  END
  ELSE if (@ProcCode = '23') -- Credit Transaction      
  BEGIN
   --SET @mDescriptionID = '811'    --old  
   SET @mDescriptionID = '100220'      --new
   SET @mDescriptionIDChg1 = ''        
   SET @mDescriptionIDChg2 = ''    

   SET @mDescription = 'Credit OCT Transaction Local'
   SET @mDescriptionChgs = ''
   SET @mDescriptionVAT  = ''
  END
  ELSE if (@ProcCode = '26') -- OCT Transaction      
  BEGIN
   --SET @mDescriptionID = '811'    --old  
   SET @mDescriptionID = '100220'      --new
   SET @mDescriptionIDChg1 = ''        
   SET @mDescriptionIDChg2 = ''    

   SET @mDescription = 'Credit OCT Transaction Local'
   SET @mDescriptionChgs = ''
   SET @mDescriptionVAT  = ''
  END
  ELSE if (@ProcCode = '31') -- Balance Enquiry Trx
  BEGIN
   --SET @mDescriptionID = '811'    --old  
   SET @mDescriptionID = '400730'      --new
   SET @mDescriptionIDChg1 = '400020'        
   SET @mDescriptionIDChg2 = ''    

   SET @mDescription = 'Balance Enquiry from Local ATM Fees (' + @ATMID+ ') Local Balance Enquiry Fees at ' + @NameLocation
   SET @mDescriptionChgs = 'Value Added Tax (VAT) for transaction'
   SET @mDescriptionVAT  = ''
  END
  ELSE      
  BEGIN
   --SET @mDescriptionID = '811'    --old  
   SET @mDescriptionID = NULL      --new
   SET @mDescriptionIDChg1 = NULL        
   SET @mDescriptionIDChg2 = NULL  

   SET @mDescription = NULL
   SET @mDescriptionChgs = NULL
   SET @mDescriptionVAT  = NULL
  END
 END    
 else if (@IsGCC = 1) -- 600000 is a local forwarding institution id euronet sends        
 BEGIN  
  SET @ATMChargeID = 'GCC'        
  
  if (@ProcCode = '01')    
  BEGIN    
   --SET @mDescriptionID = '801'        --old
   --SET @mDescriptionIDChg1 = '802'        --old
   --SET @mDescriptionIDChg2 = '803'        --old

   SET @mDescriptionID = '200280'        --new
   SET @mDescriptionIDChg1 = '400790'        --new
   SET @mDescriptionIDChg2 = '400020'    --new
   
   SET @mDescription = 'Cash Withdrawal at GCC ATM ' + convert(varchar(10), @mwDate, 120)
   SET @mDescriptionChgs = 'Cash Withdrawal from GCC ATM Fees ATM('+@ATMID+') FROM ' + @NameLocation + '-' + convert(varchar(20), @PHXDate, 120)
   SET @mDescriptionVAT  = 'Value Added Tax (VAT) for transaction RetRefNum:  - '+@RetRefNum

  END    
  else if (@ProcCode = '00')    
  BEGIN
   if (LEFT(@POSEntryMode,2) = '01')
   BEGIN
    --set @mDescriptionID = '815'        --old
    set @mDescriptionID = '200120'        --new
    SET @mDescriptionIDChg1 = ''        
    SET @mDescriptionIDChg2 = ''    
    SET @mOperatorID = 'POS-' + @ATMID              
    
    SET @mDescription = 'Online Card Transaction at '+@NameLocation+' MERCH ID: '+@MerchantType+' COUNTRY: '+@AcqCountryCode+' TRX DATE: '+convert(varchar(20), @PHXDate, 120)
    SET @mDescriptionChgs = ''
    SET @mDescriptionVAT  = ''
   END
   ELSE
   BEGIN
    --set @mDescriptionID = '815'        --old
    set @mDescriptionID = '200110'        --new
    SET @mDescriptionIDChg1 = ''        
    SET @mDescriptionIDChg2 = ''    
    SET @mOperatorID = 'POS-' + @ATMID              
    
    SET @mDescription = 'Card Transaction at '+@NameLocation+' MERCH ID: '+@MerchantType+' COUNTRY: '+@AcqCountryCode+' TRX DATE: '+convert(varchar(20), @PHXDate, 120)
    SET @mDescriptionChgs = ''
    SET @mDescriptionVAT  = ''
   END
  END
  else if (@ProcCode = '20') -- Credit Voucher/ Merchandise Return Clearing      
  BEGIN
   --SET @mDescriptionID = '813'      --old
   set @mDescriptionID = '100240'        --new
   SET @mDescriptionIDChg1 = ''        
   SET @mDescriptionIDChg2 = ''    

   SET @mDescription = 'Credit Voucher/ Merchandise Return Transaction Local'
   SET @mDescriptionChgs = ''
   SET @mDescriptionVAT  = ''
  END
  ELSE if (@ProcCode = '23') -- Credit Transaction      
  BEGIN
   --SET @mDescriptionID = '811'    --old  
   SET @mDescriptionID = '100220'      --new
   SET @mDescriptionIDChg1 = ''        
   SET @mDescriptionIDChg2 = ''    

   SET @mDescription = 'Credit OCT Transaction Local'
   SET @mDescriptionChgs = ''
   SET @mDescriptionVAT  = ''
  END
  ELSE if (@ProcCode = '26') -- OCT Transaction      
  BEGIN
   --SET @mDescriptionID = '811'    --old  
   SET @mDescriptionID = '100220'      --new
   SET @mDescriptionIDChg1 = ''        
   SET @mDescriptionIDChg2 = ''    

   SET @mDescription = 'Credit OCT Transaction Local'
   SET @mDescriptionChgs = ''
   SET @mDescriptionVAT  = ''
  END
  ELSE if (@ProcCode = '31') -- Balance Enquiry Trx
  BEGIN
   --SET @mDescriptionID = '811'    --old  
   SET @mDescriptionID = '400800'      --new
   SET @mDescriptionIDChg1 = '400020'        
   SET @mDescriptionIDChg2 = ''    

   SET @mDescription = 'Balance Enquiry from GCC ATM Fees (' + @ATMID+ ') GCC Balance Enquiry Fees at ' + @NameLocation
   SET @mDescriptionChgs = 'Value Added Tax (VAT) for transaction'
   SET @mDescriptionVAT  = ''
  END
  ELSE      
  BEGIN
   --SET @mDescriptionID = '811'    --old  
   SET @mDescriptionID = NULL      --new
   SET @mDescriptionIDChg1 = NULL
   SET @mDescriptionIDChg2 = NULL  

   SET @mDescription = NULL
   SET @mDescriptionChgs = NULL
   SET @mDescriptionVAT  = NULL     
  END
 END    
 ELSE        
 BEGIN    
  SET @ATMChargeID = '500000'        
  if (@ProcCode = '01')    
  BEGIN    
   --SET @mDescriptionID = '804'        --old
   --SET @mDescriptionIDChg1 = '805'    --old    
   --SET @mDescriptionIDChg2 = '806'    --old

   SET @mDescriptionID = '200140'        --new
   SET @mDescriptionIDChg1 = '400710'        --new
   SET @mDescriptionIDChg2 = '400020'    --new

   SET @mDescription = 'Cash Withdrawal at overseas ATM ' + convert(varchar(10), @mwDate, 120)
   SET @mDescriptionChgs = 'Cash Withdrawal from International ATM Fees ATM('+@ATMID+') FROM ' + @NameLocation + '-' + convert(varchar(20), @PHXDate, 120)
   SET @mDescriptionVAT  = 'Value Added Tax (VAT) for transaction RetRefNum:  - '+Cast(@RetRefNum as varchar(30))
  END    
  else if (@ProcCode = '00')    
  BEGIN
   if (LEFT(@POSEntryMode,2) = '01')
   BEGIN
    --set @mDescriptionID = '815'        --old
    set @mDescriptionID = '200120'        --new
    SET @mDescriptionIDChg1 = ''        
    SET @mDescriptionIDChg2 = ''    
    SET @mOperatorID = 'POS-' + @ATMID              
    
    SET @mDescription = 'Online Card Transaction at '+@NameLocation+' MERCH ID: '+@MerchantType+' COUNTRY: '+@AcqCountryCode+' TRX DATE: '+convert(varchar(20), @PHXDate, 120)
    SET @mDescriptionChgs = ''
    SET @mDescriptionVAT  = ''
   END
   ELSE
   BEGIN
    --set @mDescriptionID = '816'        --old
    set @mDescriptionID = '200270'        --new
    SET @mDescriptionIDChg1 = ''        
    SET @mDescriptionIDChg2 = ''    
    SET @mOperatorID = 'POS-' + @ATMID              
    
    SET @mDescription = 'Card Purchase International Transaction MERCH ID: '+@MerchantType+' VAL DATE: '+convert(varchar(20), @mwDate, 120)+' AMOUNT: '+convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate)+' From : ' + @NameLocation
    SET @mDescriptionChgs = ''
    SET @mDescriptionVAT  = ''
   END
  END
  else if (@ProcCode = '20') -- Credit Voucher/ Merchandise Return Clearing      
  BEGIN
   --SET @mDescriptionID = '814'      --old
   SET @mDescriptionID = '100250'      --new
   SET @mDescriptionIDChg1 = ''        
   SET @mDescriptionIDChg2 = ''    

   SET @mDescription = 'Credit Voucher/ Merchandise Return Transaction International'
   SET @mDescriptionChgs = ''
   SET @mDescriptionVAT  = ''
  END
  ELSE if (@ProcCode = '23') -- Credit Transaction      
  BEGIN
   --SET @mDescriptionID = '812'      --old
   SET @mDescriptionID = '100230'      --new
   SET @mDescriptionIDChg1 = ''        
   SET @mDescriptionIDChg2 = ''    

   SET @mDescription = 'Credit OCT Transaction International'
   SET @mDescriptionChgs = ''
   SET @mDescriptionVAT  = ''
  END
  ELSE if (@ProcCode = '26') -- OCT Transaction      
  BEGIN
   --SET @mDescriptionID = '812'      --old
   SET @mDescriptionID = '100230'      --new
   SET @mDescriptionIDChg1 = ''        
   SET @mDescriptionIDChg2 = ''    

   SET @mDescription = 'Credit OCT Transaction International'
   SET @mDescriptionChgs = ''
   SET @mDescriptionVAT  = ''
  END
  ELSE if (@ProcCode = '31') -- Balance Enquiry Trx
  BEGIN
   --SET @mDescriptionID = '811'    --old  
   SET @mDescriptionID = '400740'      --new
   SET @mDescriptionIDChg1 = '400020'        
   SET @mDescriptionIDChg2 = ''    

   SET @mDescription = 'Balance Enquiry from International ATM Fees (' + @ATMID+ ') International Balance Enquiry Fees at ' + @NameLocation
   SET @mDescriptionChgs = 'Value Added Tax (VAT) for transaction'
   SET @mDescriptionVAT  = ''
  END
  ELSE      
  BEGIN
   --SET @mDescriptionID = '812'      --old
   SET @mDescriptionID = NULL      --new
   SET @mDescriptionIDChg1 = NULL
   SET @mDescriptionIDChg2 = NULL  

   SET @mDescription = NULL
   SET @mDescriptionChgs = NULL
   SET @mDescriptionVAT  = NULL     
  END
 END   
 
 SET @NarrationID=@mDescriptionID
 
 if (@AuthIDResp = '')
 begin
  SET @AuthIDResp = '0'
 end
 
 SELECT @vCount1=count(*) From t_AccountBalance WHERE (OurBranchID = @OurBranchID) and (AccountID=@AccountID)          
 IF @vCount1=0          
 BEGIN       
  /** Rejection change STARTS 06/04/2021 **/

  INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, AuthIDResp, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, ExtendedData)
  VALUES (@OurBranchID, @AccountID, @RefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, 
  @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, 
  @AckInstIDCode, @RetRefNum, @AuthIDResp, @CardAccptID, @NameLocation, '', @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '02', 'Invalid Account', @TrxDetails, @ExtendedData)
  
  SELECT @Status = 2, @AvailableBalance = 0, @vClearBalance = 0    
  SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value,'784' as CurrencyCode, @AuthIDResp as AuthIDResp,@MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
  RETURN (1)    
  
  /** Rejection change ENDS 06/04/2021 **/
 END
 
 SELECT @rCurrencyID=ISNULL(CurrencyID,'') FROM t_Currencies WHERE OurBranchID = @OurBranchID AND CurrencyCode = @CurrCodeTran
 
 SELECT @vClearBalance=ISNULL(B.ClearBalance,0),@vClearedEffects=ISNULL(B.Effects,0),@vFreezeAmount=ISNULL(B.FreezeAmount,0),@vMinBalance=ISNULL(P.MinBalance,0),
 @vIsClose=ISNULL(A.Status,''),@vAreCheckBooksAllowed=P.AreCheckBooksAllowed,@vAllowDebitTransaction=A.AllowDebitTransaction,@value=ISNULL(p.productatmcode,'00'),
 @CurrencyCode = ISNULL(c.CurrencyCode,''),@SOCID=ISNULL(A.SOCID,''),@ProductID=A.ProductID,@NoOfCWTrxATMMonthly=ISNULL(B.NoOfCWTrxATMMonthly,0),
 @NoOfBITrxATMMonthly=ISNULL(B.NoOfBITrxATMMonthly,0),@Limit=ISNULL(B.Limit,0),@MobileNo=ISNULL(cust.MobileNo,''),@Email=ISNULL(cust.Email,''),@ClientID=cust.ClientID
 FROM t_AccountBalance B           
 INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND B.ProductID = A.ProductID             
 INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND A.ProductID = P.ProductID           
 INNER JOIN t_Currencies C ON A.OurBranchID = C.OurBranchID AND A.CurrencyID = C.CurrencyID        
 INNER JOIN t_Customer cust ON A.OurBranchID = cust.OurBranchID AND A.ClientID = cust.ClientID  
 WHERE (A.OurBranchID = @OurBranchID) and (A.AccountID = @AccountID)              
  
 --exec @Limit = [fnc_GetLimit] @OurBranchID, @AccountID        
         
 IF @Limit > 0 AND @vCheckLimit = 1 AND @vODLimitAllow = 1        
 BEGIN        
  SET @vClearBalance=(@vClearBalance+@Limit)        
 END        
        
 if (@LocalCurrencyID = @CurrencyCode)        
  SET @IsLocalCurrency = 1        

 if (@AuthIDResp = '0')
 BEGIN
  exec @AuthIDResp = fnc_GetRandomNumber        
 END
    
 INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, AuthIDResp, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, ExtendedData)
 VALUES (@OurBranchID, @AccountID, @RefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, 
 @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, 
 @AckInstIDCode, @RetRefNum, @AuthIDResp, @CardAccptID, @NameLocation, @mDescription, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, @IMTFRespCode, @IMTFRespDescription, @TrxDetails, @ExtendedData)
 
 SELECT @Status    = CAST(@IMTFRespCode as int),
 @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,        
 @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100        
          
 SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value,@CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp,@MobileNo as MobileNo, @Email as Email,@rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
 RETURN 0;        
END