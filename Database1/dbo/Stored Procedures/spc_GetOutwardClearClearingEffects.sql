﻿CREATE PROCEDURE [dbo].[spc_GetOutwardClearClearingEffects]
(
  @OurBranchID varchar(30),
  @AccountID varchar(30),
  @AccountType Char(1)='C'
)

AS

SET NOCOUNT ON

SELECT OurBranchID, RefNo, ScrollNo, SerialNo, ValueDate, AccountID, ChequeID, BankID,
Amount, ForeignAmount, Status, IsLocalCurrency, AccountType, ChequeDate, OperatorID, SupervisorID

FROM t_Clearing a
Where a.OurBranchID = @OurBranchID and a.AccountID = @AccountID and Status NOT IN ('C','R')

Order By ValueDate Desc