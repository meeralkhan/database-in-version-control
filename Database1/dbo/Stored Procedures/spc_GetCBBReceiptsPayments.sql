﻿CREATE procedure [dbo].[spc_GetCBBReceiptsPayments]    
(    
 @OurBranchID varchar(30),    
 @CurrencyID varchar(30)    
)    
AS    
    
SET NOCOUNT ON    
    
BEGIN          
      
  declare @LocalCurrency varchar(10)      
      
  declare @Receipts money, @TotalReceipts int, @Payments money, @TotalPayments int    
      
  Select @LocalCurrency=LocalCurrency from t_GlobalVariables WHERE OurBranchID = @OurBranchID   
        
  if @CurrencyID = @LocalCurrency      
  Begin      
        
    SELECT @Receipts = ISNULL(SUM(Amount), 0), @TotalReceipts = SUM(Counts) FROM (    
          
      SELECT ISNULL(SUM(Amount),0) Amount, COUNT(*) Counts FROM t_CashTransactionModel    
      WHERE OurBranchID = @OurBranchID AND TrxType ='C' AND CurrencyID = @CurrencyID AND Supervision = 'C'    
          
      UNION ALL    
          
      Select isnull(sum(NetAmount),0) Amount, COUNT(*) Counts From t_BureaDeChange    
      Where OurBranchID = @ourBranchID and SellOrBuy = 'B' and Supervision ='C'    
          
    ) AS SumCash    
        
    SELECT @Payments = ISNULL(SUM(Amount), 0), @TotalPayments = SUM(Counts) FROM (    
          
      SELECT ISNULL(SUM(Amount),0) Amount, COUNT(*) Counts FROM t_CashTransactionModel    
      WHERE (OurBranchID = @OurBranchID AND TrxType ='D' AND CurrencyID = @CurrencyID AND Supervision = 'C')    
      OR (OurBranchID = @OurBranchID AND TrxType ='D' AND IsLocalCurrency = 1 AND Supervision = 'C')    
          
      UNION ALL    
          
      Select isnull(sum(NetAmount),0) Amount, COUNT(*) Counts From t_BureaDeChange    
      Where OurBranchID = @ourBranchID and SellOrBuy = 'S' and Supervision ='C'    
          
    ) AS SumCash    
        
    select @Receipts Receipts, @TotalReceipts TotalReceipts, @Payments Payments, @TotalPayments TotalPayments    
        
  end      
  ELSE      
  BEGIN      
        
    SELECT @Receipts = ISNULL(SUM(Amount), 0), @TotalReceipts = SUM(Counts) FROM (    
          
      SELECT ISNULL(SUM(ForeignAmount),0) Amount, COUNT(*) Counts FROM t_CashTransactionModel    
      WHERE OurBranchID = @OurBranchID AND TrxType ='C' AND CurrencyID = @CurrencyID AND Supervision = 'C' AND IsLocalCurrency = 0    
          
      UNION ALL    
          
      Select isnull(sum(ForeignAmount),0) Amount, COUNT(*) Counts From t_BureaDeChange    
      Where OurBranchID = @ourBranchID and SellOrBuy = 'B' and Supervision ='C'    
          
    ) AS SumCash    
        
    SELECT @Payments = ISNULL(SUM(Amount), 0), @TotalPayments = SUM(Counts) FROM (    
          
      SELECT ISNULL(SUM(ForeignAmount),0) Amount, COUNT(*) Counts FROM t_CashTransactionModel    
      WHERE OurBranchID = @OurBranchID AND TrxType ='D' AND CurrencyID = @CurrencyID     
      AND IsLocalCurrency = 0 AND Supervision = 'C'    
          
      UNION ALL    
          
      Select isnull(sum(ForeignAmount),0) Amount, COUNT(*) Counts From t_BureaDeChange    
      Where OurBranchID = @ourBranchID AND CurrencyID = @CurrencyID and SellOrBuy = 'S' and Supervision ='C'    
          
    ) AS SumCash    
        
    select @Receipts Receipts, @TotalReceipts TotalReceipts, @Payments Payments, @TotalPayments TotalPayments    
        
  END      
END