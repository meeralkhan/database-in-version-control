﻿  
CREATE PROCEDURE [dbo].[spc_GetMaturityDueDateReport]    
(            
 @OurBranchID varchar(4)='',            
 @ToDate datetime,            
 @FromDate datetime,            
 @ReportType varchar(50)=''            
)            
  AS            
  set nocount on         
        
 IF (@ReportType = 'Maturity Due Date')             
  BEGIN            
   SELECT * from vc_DepositIssuance            
   WHERE OurBranchID=@OurBranchID AND             
   (MaturityDate BETWEEN @FromDate AND @ToDate)  AND ((CloseDate>=@FromDate AND CloseDate<=@ToDate) OR CloseDate Is Null)            
   ORDER BY Period, MaturityDate            
  END            
            
 ELSE            
  BEGIN            
   SELECT * from vc_DepositIssuance            
   WHERE OurBranchID=@OurBranchID AND             
   (MaturityDate BETWEEN '01/01/1800' AND @ToDate)             
   ORDER BY  Period, MaturityDate            
  END