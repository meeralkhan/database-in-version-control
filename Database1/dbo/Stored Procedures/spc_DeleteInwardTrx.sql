﻿CREATE PROCEDURE [dbo].[spc_DeleteInwardTrx]  
(  
 @OurBranchID varchar(30),    
 @SerialNo int,    
 @AccountID varchar(30),    
 @ChequeID varchar(30)='',    
 @Amount money=0,    
 @ForeignAmount money=0,    
 @CurrencyID varchar(30)='',    
 @LocalCurrency varchar(30)='',  
 @AccountType char(1)='C',    
 @Status char(1)='',    
 @TransactionType char(1)='',    
 @wDate datetime='',    
 @ClearBalance money=0,    
 @Effects money=0,    
 @Limit money=0,    
 @SupervisorID varchar(30)='',  
 @NewRecord bit=0    
)  
AS    
     
 SET NOCOUNT ON  
   
 Select  @AccountID = AccountID,@wDate=wDate, @ChequeID = ChequeID , @CurrencyID = CurrencyID ,   
 @Amount=Amount, @ForeignAmount=ForeignAmount,    
 @TransactionType=TransactionType, @AccountType = AccountType , @Status = Status     
 From   t_InwardClearing    
 Where SerialNo = @SerialNo AND OurBranchID = @OurBranchID  
   
 if @Status = ''  
 begin  
    select 'Error' AS ReturnStatus, 'TrxAlreadyHonoured' AS ReturnMessage  
    return  
 end  
 else if @Status = 'R'  
 begin  
    select 'Error' AS ReturnStatus, 'TrxAlreadyRejected' AS ReturnMessage  
    return  
 end  
   
 Select @ClearBalance=ClearBalance,@Effects = Effects,@Limit = Limit     
 From t_AccountBalance    
 Where OurBranchID = @OurBranchID and AccountID = @AccountID    
     
 Select @LocalCurrency = LocalCurrency From t_GlobalVariables Where OurBranchID = @OurBranchID    
    
 -- Delete Entry From Transaction Table    
 Delete From t_InwardClearing    
 Where SerialNo = @SerialNo AND OurBranchID = @OurBranchID and AccountID = @AccountID  
   
 -- Delete Entry From ChequePaid Table    
 Delete From t_ChequePaid     
 Where OurBranchID = @OurBranchID and AccountID = @AccountID and ChequeID = @ChequeID  
   
 --         Update Balance Table    
 IF @AccountType = 'C'     
 BEGIN    
  IF @Status = ''    
   BEGIN    
    IF @LocalCurrency = @CurrencyID    
     Update t_AccountBalance    
     Set  ClearBalance = (ClearBalance + @Amount)    
     Where (OurBranchID = @OurBranchID) and (AccountID = @AccountID)    
    ELSE    
     Update t_AccountBalance    
     Set  ClearBalance = (ClearBalance + @ForeignAmount),    
     LocalClearBalance = LocalClearBalance + @Amount    
     Where (OurBranchID = @OurBranchID) and (AccountID = @AccountID)    
   END    
    
  ELSE IF @Status = 'U' OR @Status = '*' OR @Status = 'E'    
   BEGIN    
    IF @LocalCurrency = @CurrencyID    
     Update t_AccountBalance    
     Set  ShadowBalance = (ShadowBalance + @Amount)    
     Where (OurBranchID = @OurBranchID) and (AccountID = @AccountID)    
    ELSE    
     Update t_AccountBalance    
     Set  ShadowBalance = (ShadowBalance + @ForeignAmount)    
     Where (OurBranchID = @OurBranchID) and (AccountID = @AccountID)    
   END    
  ELSE    
   BEGIN    
    IF @LocalCurrency = @CurrencyID    
     Update t_AccountBalance    
     Set  ShadowBalance = (ShadowBalance + @Amount)    
     Where (OurBranchID = @OurBranchID) and (AccountID = @AccountID)    
    ELSE    
     Update t_AccountBalance    
     Set  ShadowBalance = (ShadowBalance + @ForeignAmount)    
     Where (OurBranchID = @OurBranchID) and (AccountID = @AccountID)    
    
   END    
END    
    
 Delete From t_CustomerStatus    
 Where (OurBranchID = @OurbranchID) and (AccountID = @AccountID) and (SerialNo = @SerialNo) AND TransactionType = 'DI'    
 
 if @NewRecord = 0
 begin
    
    /* Create a New record For Delete Exception */  
    Insert Into t_SupervisedBy    
    (OurBranchID,AccountID,[Date],ScrollNo,ChequeID,ClearBalance,Effects,Limit,TrxType,Amount,Status,SupervisorID,TransactionCategory)    
    Values    
    (@OurBranchID,@AccountID,@wDate,@SerialNo,'Deleted',@ClearBalance,@Effects,@Limit,'D',@Amount,@Status,@SupervisorID,'I')    
 
 end
 
 IF @AccountType = 'G'     
  BEGIN    
 --Local Remittance Unpaid Mark    
   Update t_LR_Issuance    
   Set Paid = 0,    
   PaidDate = '1/1/1900',    
PaidScrollNo = 0,    
Supervision = 'C',    
PaidMode = 'NA'    
   Where (OurBranchID=@OurBranchID) and (InstrumentAccountID = @AccountID)      
   and  (PaidScrollNo = @SerialNo) and  (convert(varchar(10),PaidDate,101) =convert(varchar(10), @wDate,101))       
   and (PaidMode = 'IW')    
     
 --Local Remittance Unpaid Mark  
 /*  
   Update t_LR_Advice    
   Set Paid = 0,  
   PaidDate = '1/1/1900',  
   PaidScrollNo = 0,  
   PaidMode = 'NA'  
   Where (OurBranchID=@OurBranchID) and (InstrumentAccountID = @AccountID)  
   and (PaidScrollNo = @SerialNo) and (PaidMode = 'IW')  
   and (convert(varchar(10),PaidDate,101) = convert(varchar(10), @wDate,101))  
 */  
     
  END    
    
  select 'Ok' AS ReturnStatus, @SerialNo AS SerialNo