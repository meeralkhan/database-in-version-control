﻿CREATE PROCEDURE [dbo].[spc_GetOldVouchers]        
(          
 @OurBranchID  varchar(30),          
 @ScrollNo  money,          
 @wDate         Datetime,          
 @DocumentType  varchar(32)='',          
 @TransactionType varchar(32)=''          
)          
          
AS          
   SET NOCOUNT ON          
          
   DECLARE @LocalCurrency as Varchar(30)             
   DECLARE @OpeningBalance as money          
          
   DECLARE @crOpBal as money          
          
          
   DECLARE @drOpBal as money          
   DECLARE @tDate as Varchar(15)          
   DECLARE @WorkingDate as datetime          
          
  IF @DocumentType ='T' OR @TransactionType = 'T'          
  BEGIN          
        
    SELECT ScrollNo, SerialNo, AccountID, AccountName, AccountType, ProductID, ValueDate, wDate, TrxTimeStamp,    
    Amount,ForeignAmount, CurrencyID, ChequeID, ChequeDate, TrxType, DescriptionID, Description,    
    ExchangeRate,OperatorID,SupervisorID    
    FROM (    
          
      SELECT ScrollNo, SerialNo, AccountID, AccountName, AccountType, ProductID, ValueDate, wDate, TrxTimeStamp,           
      Amount,ForeignAmount, CurrencyID, ChequeID, ChequeDate, TrxType, DescriptionID, Description,           
      ExchangeRate,OperatorID,SupervisorID           
      FROM t_Transactions          
      WHERE OurBranchID = @OurBranchID              
      AND convert(varchar(10) , wDate,101) = @wDate AND Scrollno = @ScrollNo          
      AND Documenttype = @TransactionType      
          
      UNION ALL          
          
      SELECT ScrollNo, SerialNo, T.AccountID, G.Description AccountName,'G' AccountType,'GL' ProductID,           
      ValueDate,Date wDate, TrxTimeStamp, Amount,ForeignAmount, G.CurrencyID,'V' ChequeID,Date ChequeDate,           
      CASE IsCredit WHEN 1 THEN 'C' ELSE 'D' END TrxType, DescriptionID, T.Description,           
      ExchangeRate,OperatorID,SupervisorID           
      FROM t_GLtransactions T , t_GL G          
      WHERE T.OurBranchID = G.OurBranchID AND T.AccountID = G.AccountID           
      AND T.OurBranchID = @OurBranchID              
      AND convert(varchar(10) , Date,101) = @wDate AND Scrollno = @ScrollNo          
      AND TransactionType = @DocumentType -- 'Transfer'           
        
    ) a ORDER BY ScrollNo, SerialNo    
        
 END          
         
         
   ELSE          
          
 BEGIN          
-- This Condition Is New Update 29-11-2004          
-- Display Indvidual Clearing Trx. instead of batch          
---- Start New Immd          
  IF  @DocumentType ='OC'          
  BEGIN          
        
    SELECT ScrollNo, SerialNo, AccountID, AccountName, AccountType, ProductID, ValueDate, wDate, TrxTimeStamp,    
    Amount, ForeignAmount, CurrencyID, ChequeID, ChequeDate, TrxType, DescriptionID, Description,    
    ExchangeRate, OperatorID, SupervisorID    
    FROM (    
           
       SELECT T.ScrollNo, C.SerialNo, C.AccountID, T.AccountName, T.AccountType, T.ProductID, T.ValueDate, T.wDate, TrxTimeStamp,           
       C.Amount,C.ForeignAmount, T.CurrencyID, C.ChequeID, C.ChequeDate, T.TrxType, T.DescriptionID,           
       Case C.Status WHEN 'R' THEN 'Clearing Cheque Is Return' WHEN 'C' THEN 'Clearing Cheque Is Cleared' END Description,           
       T.ExchangeRate,T.OperatorID,T.SupervisorID           
       FROM t_Clearing C, t_Transactions T          
       WHERE C.OurBranchID = T.OurBRanchID AND C.AccountID = T.AccountID AND           
       C.ValueDate = T.ValueDate AND C.ScrollNo = T.ScrollNo AND           
       C.OurBranchID =  @OurBranchID          
       AND convert(varchar(10) , T.wDate,101) = @wDate AND T.Scrollno =@ScrollNo          
       AND T.Documenttype = @TransactionType          
           
       UNION ALL          
           
       SELECT ScrollNo, SerialNo, T.AccountID, G.Description AccountName,'G' AccountType,'GL' ProductID,           
       ValueDate,Date wDate, TrxTimeStamp, Amount,ForeignAmount, G.CurrencyID,'V' ChequeID,Date ChequeDate,           
       CASE IsCredit WHEN 1 THEN 'C' ELSE 'D' END TrxType, DescriptionID, T.Description,                  ExchangeRate,OperatorID,SupervisorID           
       FROM t_GLtransactions T , t_GL G          
       WHERE T.OurBranchID = G.OurBranchID AND T.AccountID = G.AccountID           
       AND T.OurBranchID = @OurBranchID              
       AND convert(varchar(10) , Date,101) = @wDate AND Scrollno = @ScrollNo          
       AND TransactionType = @DocumentType -- 'Transfer'           
           
    ) a ORDER BY ScrollNo, SerialNo    
        
   END        
           
  ELSE          
--End New amendment          
  BEGIN          
        
    SELECT ScrollNo, SerialNo, AccountID, AccountName, AccountType, ProductID, ValueDate, wDate, TrxTimeStamp,    
    Amount,ForeignAmount, CurrencyID, ChequeID, ChequeDate, TrxType, DescriptionID, Description,    
    ExchangeRate,OperatorID,SupervisorID    
    FROM (    
          
      SELECT ScrollNo, SerialNo, AccountID, AccountName, AccountType, ProductID, ValueDate, wDate, TrxTimeStamp,           
      Amount,ForeignAmount, CurrencyID, ChequeID, ChequeDate, TrxType, DescriptionID, Description,    
      ExchangeRate,OperatorID,SupervisorID           
      FROM t_Transactions          
      WHERE OurBranchID = @OurBranchID              
      AND convert(varchar(10) , wDate,101) = @wDate AND Scrollno = @ScrollNo          
      AND Documenttype = @TransactionType          
          
      UNION ALL          
          
      SELECT ScrollNo, SerialNo, T.AccountID, G.Description AccountName,'G' AccountType,'GL' ProductID,           
      ValueDate,Date wDate, TrxTimeStamp, Amount,ForeignAmount, G.CurrencyID,'V' ChequeID,Date ChequeDate,           
      CASE IsCredit WHEN 1 THEN 'C' ELSE 'D' END TrxType, DescriptionID, T.Description,           
      ExchangeRate,OperatorID,SupervisorID           
      FROM t_GLtransactions T , t_GL G          
      WHERE T.OurBranchID = G.OurBranchID AND T.AccountID = G.AccountID           
      AND T.OurBranchID = @OurBranchID              
      AND convert(varchar(10) , Date,101) = @wDate AND Scrollno = @ScrollNo          
      AND TransactionType = @DocumentType -- 'Transfer'           
        
    ) a ORDER BY ScrollNo, SerialNo    
        
   END           
 END