﻿CREATE PROCEDURE [dbo].[sp_ATM_GetGlobalVariables] (
 @OurBranchID varchar(30)
)
 AS
  BEGIN

   DECLARE @AppDomain varchar(max)

   IF NOT EXISTS ( SELECT [name] FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[t_ATM_GlobalVariables]')
                   AND [name] = 'AppDomain' )
   BEGIN
     ALTER TABLE t_ATM_GlobalVariables add [AppDomain] varchar(max) NOT NULL DEFAULT ('')
   end

   SELECT @AppDomain=[AppDomain]
   FROM t_ATM_GlobalVariables
   WHERE OurBranchID = @OurBranchID

   IF (@AppDomain = '')
   BEGIN
     Update t_ATM_GlobalVariables SET [AppDomain] = '(0,-@=_Nmk*.+)7*.06-7Te^qo1-/20.0))06gdJ20815314622073792820232144827880037891069370'
   END

   SELECT Version, UBPS_73_Allowed, TitleFetch_62, [3PFT_40], IBFT_48, CashDeposit_15, CreditCardBill_22,
   LocalCurrencyID, [AppDomain]
   FROM t_ATM_GlobalVariables
   WHERE OurBranchID = @OurBranchID

  END