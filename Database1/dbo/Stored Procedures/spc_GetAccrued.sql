﻿
CREATE Procedure [dbo].[spc_GetAccrued]  
(  
  @OurBranchID nvarchar(30),  
  @AccountID   nvarchar(30),  
  @ReceiptID   nvarchar(30)  
)  
  
As  

SET NOCOUNT ON

Begin  
    
  SELECT a.*, b.Name, b.CurrencyID FROM t_DepositAccrualSummary a  
  INNER JOIN t_Account b ON a.OurBranchID = b.OurBranchID AND a.AccountID = b.AccountID  
  WHERE a.OurBranchID = @OurBranchID AND a.AccountID = @AccountID AND a.ReceiptID = @ReceiptID  
    
End