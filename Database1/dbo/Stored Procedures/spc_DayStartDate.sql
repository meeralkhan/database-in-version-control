﻿CREATE PROCEDURE [dbo].[spc_DayStartDate]     
(     
 @OurBranchID varchar(30),  
 @InputDate As datetime    
)     
    
/*     
 1) If Day Start is successful than @canStart will be set to 1     
 2) @inputDate is the date that will be input to this parameter     
*/     
    
AS    
    
SET NOCOUNT ON    
    
 DECLARE     
 @LastEOD As Datetime, @LastBOD As Datetime, @NextDate As Datetime,     
 @CanStart As Char(1) , @ErrMessage As Varchar(100) , @WorkingDate As Datetime     
     
 /*Convert string date into DateTime type*/     
 Set @workingDate = Convert(DateTime , @inputDate)     
     
 /*Check's Whether the input is holiday.If TRUE then @canStart=0.*/     
 If @workingDate IN (Select [Date] From t_Holidays Where OurBranchID = @OurBranchID and [Date]=@workingDate)     
 Begin     
 Set @canStart = 0     
 Set @errMessage = 'Can Not Perform Day Start, Today Marked Holiday...'     
 Select @canStart As CanStart, @errMessage as ErrMessage     
 Return     
 End     
     
     
 /*Store LASTEOD and LASTBOD fields into corresponding variables */     
 Select @lastEOD = Convert(varchar(10) , LASTEOD ,101), @lastBOD =Convert(varchar(10) , LASTBOD,101) From t_Last Where OurBranchID = @OurBranchID  
     
 If (@WorkingDate <= Convert(varchar(10), @lastEOD,101)) --@LastEOD)     
 Begin     
 Set @canStart = 0     
 Set @errMessage = 'Can Not Perform Day Start, Woking Date Is Not Compatible...'     
 Select @canStart As CanStart, @errMessage as ErrMessage     
 Return     
 End     
     
     
 /*If lastEOD is not equal to LASTBOD than @canStart=0*/     
 If Convert(varchar(10), @lastEOD,101) <> Convert(varchar(10) ,@lastBOD ,101)     
 Begin     
 Set @canStart = 0     
 Set @errMessage = 'Day Start Already Done OR Day End Process Not Perform Yet, So Can Not Perform Day Start...'     
 Select @canStart As CanStart, @errMessage as ErrMessage     
 Return     
 End     
     
     
 /*WorkingDate i.e the day when new DAY STRT starts should be greater than LASTEOD.If it is less then LASTEOD than stop*/     
     
     
 Set @nextDate = Convert(varchar(10), @lastEOD,101)     
     
 /*Add one Day to @nextDate so that it does not point to LASTEOD*/     
 Set @nextDate = DateAdd(day , 1 , @nextDate)     
     
 /*Increment @nextDate and Check's if it is a holiday.If it is a holiday than increments to next day*/     
 While @nextDate IN (Select [Date] From t_Holidays Where OurBranchID = @OurBranchID and [Date]=@nextDate)     
 Begin     
 Set @nextDate = DateAdd(day , 1 , @nextDate)     
 End     
     
 /*If WorkingDate is set to Date that is not to equal to calculated Date but valid than Display Warning*/     
 If @WorkingDate > @nextDate     
 Begin     
 Set @canStart = 2     
 Set @errMessage='Warning: ' + Char(13) + Char(13) + 'Your Date Should Be >>> ' + Convert(Varchar(12),@nextDate)     
 Select @canStart As CanStart, @errMessage as ErrMessage--, @nextDate As NextDate, @workingDate As WorkingDate     
 Return     
 End     
 Else /*Otherwise successful .Set @errMessage equal to NULL */     
 Begin     
 Set @canStart = 1     
 Set @errMessage = 'NULL'     
 Select @canStart As CanStart, @errMessage as ErrMessage     
 Return     
 End