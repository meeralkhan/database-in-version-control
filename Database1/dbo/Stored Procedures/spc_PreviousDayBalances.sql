﻿CREATE PROCEDURE [dbo].[spc_PreviousDayBalances]
(
  @OurBranchID varchar(30)
)
AS

SET NOCOUNT ON

Declare @Wdate datetime
Select @Wdate = WorkingDate from t_Last where OurBranchID = @OurBranchID

delete from t_PreviousDayBalances where OurBranchID = @OurBranchID

Insert Into t_PreviousDayBalances (WorkingDate,OurBranchID,AccountID,ProductID,[Name],ClearBalance,Effects,LocalClearBalance,LocalEffects)
Select @WDate,OurBranchID,AccountID,ProductID,[Name],ClearBalance,Effects,LocalClearBalance,LocalEffects From t_AccountBalance where OurBranchID = @OurBranchID

select 'Ok' AS RetStatus