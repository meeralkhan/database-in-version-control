﻿CREATE PROCEDURE [dbo].[spc_OL_AddEditTransferTransactionIBD]    
(      
 @ScrollNo    int,      
 @SerialNo    int,      
 @OurBranchID varchar(30),      
 @IBCANo      int,       
 @BranchID    varchar(30),      
 @AccountID   varchar(30),      
 @AccountName varchar(100),      
 @ProductID   varchar(30),      
 @CurrencyID  varchar(30),      
 @AccountType char(1),      
 @TrxType     char(1),       
 @IsLocalCurrency   Char(1),      
 @ValueDate   datetime,      
 @wDate       datetime,      
 @ChequeID    varchar(30),      
 @ChequeDate  DateTime,      
 @Amount      money,      
 @ForeignAmount  Money,      
 @ExchangeRate   Money,      
 @DescriptionID  varchar(30),      
 @Description    varchar(255),      
 @OperatorID     varchar(30),      
 @SupervisorID   varchar(30)='',      
 @SourceBranchID Varchar(30)='',      
 @TargetBranchID varchar(30)='',      
 @RefNo   varchar(30),      
 --@S_BranchAccountID   varchar(30),      
 --@S_BranchAccountName  varchar(100)='',      
 --@T_BranchAccountID   varchar(30),      
 --@T_BranchAccountName  varchar(100)='',      
 --@HOAccountID   varchar(30),      
 --@HOAccountName   varchar(100)='',       
 @Remarks   varchar(255)='ONLINE TRANSFER',      
 --@Supervision char(1),      
                 
 @MethodType Varchar(2)='',      
 @DocumentType Varchar(2)='',      
 @InstrumentType varchar(50)='',      
 @IssueDate  datetime='01/01/1900',      
 @InstrumentNo int=0,      
 @ControlNo  int=0,      
 @Beneficiary varchar(255)='',      
 @DrawnBank  varchar(100)='',      
 @DrawnBranch varchar(100)='',        
 --@BankID  varchar(4)='',    
                 
 @WHTaxSerial int = 0,            
 @AppWHTax  Char(1) = 0,            
 @WHTaxAmount money = 0,            
 @WHTaxMode   Varchar(2) ='',            
 @WHTaxAccountID  varchar(30)='',    
 @IsCheckValidate int=0,    
 @IsHOAccount int=0,                  
 @AddData text=NULL                  
)      
      
AS      
      
 DECLARE @ClearBalance   as money      
 DECLARE @FreezeAmount   as money      
 DECLARE @Limit          as Money       
 DECLARE @ShadowBalance  as money            
 DECLARE @totAmount      as money            
 DECLARE @NetAmount      as money            
 DECLARE @WHTaxName      as varchar(255)    
 DECLARE @WHTaxDescription as varchar(255)    
     
 DECLARE @WHTaxCurrencyID as varchar(30)    
 DECLARE @LocalCurrency as varchar(30)    
     
 DECLARE @IsAccountRestrict as bit            
 DECLARE @TrxLimit as money            
 DECLARE @TrxUtilized as money            
 DECLARE @RestrictTrxType as Char(1)              
     
 DECLARE @Status       as char(2)    
 --DECLARE @HOTrxType    as Char(1)      
 DECLARE @twDate       as datetime       
 DECLARE @tLastEOD     as datetime      
 DECLARE @tLastBOD     as datetime       
 DECLARE @mDate varchar(50)                  
     
 DECLARE @AppWHT int = 0, @WHTAmt money = 0, @WHTMode Varchar(1) = '', @WHTScrollNo int = 0    
 if @SerialNo = 1 and @AppWHTax = 1    
 begin    
    select @AppWHT = @AppWHTax, @WHTAmt = @WHTaxAmount, @WHTMode = @WHTaxMode, @WHTScrollNo = @ScrollNo    
 end    
     
 --IF @TrxType='D'      
 --    SELECT @HOTrxType='C'      
 --ELSE       
 --  SELECT @HOTrxType='D'      
      
 /*DECLARE @HOProductID   varchar(6)='GL',      
      @HOCurrencyID   varchar(4)='PKR',      
        @HOAccountType   char(1)='C',      
      @HOTrxType   Char(1)='',      
      @HOIsLocalCurrency   Char(1)='1',      
*/      
      
 SET NOCOUNT ON      
     
  SELECT @OurBranchID = OurBranchID, @LocalCurrency = LocalCurrency From t_GlobalVariables WHERE OurBranchID = @BranchID              
      
  SELECT @twDate = WorkingDate, @tLastEOD = LastEOD , @tLastBOD = LastBOD FROM t_Last WHERE OurBranchID = @BranchID              
      
  IF @twDate = @wDate      
   BEGIN      
    IF @tLastEOD = @tLastBOD      
     BEGIN      
      SELECT 'Error' AS RetStatus, 'EODDone' As RetMessage          
      RETURN (0)      
     END           
   END      
  ELSE      
   BEGIN      
    SELECT 'Error' AS RetStatus, 'WorkingDate' As RetMessage          
    RETURN (0)        END      
      
 BEGIN      
  SELECT @OurbranchID = @BranchID,@IBCANo=@RefNo       
  SELECT @RefNo = @ScrollNo      
  SELECT @ScrollNo=@IBCANo      
      
  SET @IsAccountRestrict = 1            
  SET @TrxLimit = 0            
  SET @TrxUtilized = 0            
 SET @RestrictTrxType = 'B'          
  SET @mDate = convert(varchar(10),@wDate,101) + ' ' + convert(varchar(20), getdate(),114)    
      
  IF @IsCheckValidate = 1 AND @IsHOAccount = 0    
  BEGIN    
        
    IF @ProductID <> 'GL'            
    BEGIN            
          
      SELECT @IsAccountRestrict = IsAccountRestrict, @TrxLimit = CASE @TrxType WHEN 'C' THEN DayDepositBal    
      WHEN 'D' THEN DayDrawableBal END From t_OL_GlobalVariables WHERE OurBranchID = @BranchID              
          
    END            
    ELSE            
    BEGIN            
          
      SELECT @IsAccountRestrict = IsGLRestrict, @TrxLimit = CASE @TrxType WHEN 'C' THEN DayDepositBal     
      WHEN 'D' THEN DayDrawableBal END From t_OL_GlobalVariables WHERE OurBranchID = @BranchID              
          
    END            
        
    IF @IsAccountRestrict = 1            
    BEGIN            
              
      IF (SELECT Count(*) From t_OL_Accounts             
      WHERE OurBranchID = @BranchID AND AccountID = @AccountID AND AccountType = @AccountType     
      AND (TrxType = @TrxType OR TrxType = 'B')  ) > 0            
      BEGIN            
            
        SELECT @TrxLimit = CASE @TrxType WHEN 'C' THEN IsNull(DayDepositBal,0)            
               WHEN 'D' THEN IsNull(DayDrawableBal,0)            
          END             
        FROM t_OL_Accounts            
        WHERE OurBranchID = @BranchID AND AccountID = @AccountID AND AccountType = @AccountType            
      END            
      ELSE            
      BEGIN            
        SELECT 'Error' AS RetStatus, 'OnlineTrxNotAllowed' As RetMessage          
        RETURN(0)          
      END            
    END            
      
    --IF LEFT(@DescriptionID,2) <> 'TR'            
    --BEGIN            
      SELECT @TrxUtilized = IsNull(Sum(Amount),0)             
      FROM t_OnLineCashTransaction            
      WHERE OurBranchID = @BranchID AND AccountID = @AccountID             
        AND AccountType = @AccountType AND            
        TrxType = @TrxType AND Not DescriptionID Like 'TR%'     
                
      IF @TrxLimit < @TrxUtilized + @Amount            
       BEGIN            
        SELECT 'Error' AS RetStatus, 'TrxLimitExceed' As RetMessage          
        RETURN(0)          
       END            
    --END            
        
  END    
            
  DECLARE @AccountClass CHAR(1)          
  DECLARE @IsPosting as BIT          
      
  IF @ProductID = 'GL'            
  BEGIN          
               
     IF (SELECT Count(AccountID) From t_GL WHERE OurBranchID=@BranchID AND AccountID=@AccountID) > 0          
     BEGIN          
                 
       SELECT @AccountClass = AccountClass, @IsPosting = IsPosting From t_GL          
   WHERE OurBranchID=@BranchID AND AccountID=@AccountID          
                 
       IF UPPER(@AccountClass) <> 'P'          
       BEGIN          
         SELECT 'Error' AS RetStatus, 'NotAPostingAccount' As RetMessage          
         RETURN(0)          
       END          
                 
       IF @IsPosting = 0               
       BEGIN          
         SELECT 'Error' AS RetStatus, 'GLPostingNotAllowed' As RetMessage          
         RETURN(0)          
       END          
                 
       IF (SELECT Count(*) From t_Products WHERE OurBranchID = @BranchID AND           
       (GLControl=@AccountID OR GLDebitBalance = @AccountID)) > 0          
       BEGIN          
         SELECT 'Error' AS RetStatus, 'GLPostingNotAllowedControlAccount' As RetMessage          
         RETURN(0)          
       END          
                 
     END          
     ELSE          
     BEGIN          
       SELECT 'Error' AS RetStatus, 'AccountNotFound' As RetMessage          
       RETURN(0)      
     END          
               
  END          
      
  DECLARE @IsFreezed as Bit    
  DECLARE @AllowCrDr as Bit    
      
  IF @ProductID <> 'GL'            
 BEGIN            
              
    IF (SELECT Count(AccountID) From t_Account WHERE OurBranchID=@BranchID AND AccountID=@AccountID) > 0          
    BEGIN          
                
      SELECT @ProductID = ProductID, @Status = ISNULL(Status,''), @AllowCrDr = Case @TrxType          
               WHEN 'C' THEN AllowCreditTransaction            
               WHEN 'D' THEN AllowDebitTransaction            
             END          
      FROM t_Account          
 WHERE OurBranchID=@BranchID AND AccountID=@AccountID    
                
      IF @Status = 'C'          
      BEGIN          
         SELECT 'Error' AS RetStatus, 'AccountClosed' As RetMessage          
         RETURN(0)          
      END          
                
      IF @Status = 'D'          
      BEGIN          
         SELECT 'Error' AS RetStatus, 'AccountBlocked' As RetMessage          
         RETURN(0)          
      END          
                
      IF @Status = 'T'          
      BEGIN SELECT 'Error' AS RetStatus, 'AccountDormant' As RetMessage          
         RETURN(0)          
      END          
                
      IF @Status = 'X'          
      BEGIN                  
         SELECT 'Error' AS RetStatus, 'AccountDeceased' As RetMessage          
         RETURN(0)          
      END          
                
      IF @AllowCrDr = 1          
      BEGIN            
         SELECT 'Error' AS RetStatus, 'SPTrxNotAllowed' As RetMessage          
         RETURN(0)          
      END          
          
      SELECT @IsFreezed=IsFreezed, @AllowCrDr = Case @TrxType                   
               WHEN 'C' THEN AllowCredit          
               WHEN 'D' THEN AllowDebit          
             END          
      FROM t_Products          
      WHERE OurBranchID = @BranchID AND ProductID = @ProductID    
          
      IF @IsFreezed = 1          
      BEGIN          
         SELECT 'Error' AS RetStatus, 'ProductFreezed' As RetMessage          
         RETURN(0)          
      END          
                
      IF @AllowCrDr = 0          
      BEGIN          
         SELECT 'Error' AS RetStatus, 'ProductTrxNotAllowed' As RetMessage          
         RETURN(0)          
      END          
          
    END          
    ELSE          
    BEGIN          
      SELECT 'Error' AS RetStatus, 'InvalidCustomer' As RetMessage          
      RETURN(0)          
    END          
  END    
      
-- Condition verify Only customer debit transaction      
--   IF @TrxType='D' AND @ProductID <> 'GL'      
--    BEGIN      
--     SELECT @ClearBalance=isNull(ClearBalance-FreezeAmount,0),      
--   @LImit = Isnull(limit,0),      
--      @FreezeAmount=isNull(FreezeAmount,0)       
--       
-- FROM t_AccountBalance      
--     WHERE OurBranchID=@OurBranchID      
--     AND AccountID=@AccountID      
--       AND ProductID=@ProductID      
--          
--     IF @Amount > @ClearBalance       
--       
--     IF @amount > ( @limit+@ClearBalance)      
--       
--               
--      BEGIN      
--       SET NOCOUNT OFF      
--       SELECT @Status=5      
--       SELECT @Status as Status ,@ClearBalance+@FreezeAmount as ClearBalance , @FreezeAmount as FreezeAmount      
--       RETURN (1)       
--      END      
--     --IF UPPER(@ChequeID) <> 'V'       
--      --BEGIN      
--       -- Get Validate Cheaque No      
--       --EXEC @Status = sp_ValidateChequeNo @OurBranchID, @AccountID, @ChequeID, @ChequeID,'C'      
--       
--       --IF @Status <> 0      
--        --BEGIN            
--         --SELECT @Status as Status      
--         --SET NOCOUNT OFF      
--         --RETURN(1)    
--        --END      
--      --END      
--       
--    END      
      
  IF @TrxType='D' AND @MethodType = 'LR' AND @InstrumentNo > 0    
  BEGIN                
    
    EXEC @Status = spc_OL_VerifyInstrumentIBD @TargetBranchID, @InstrumentType, @IssueDate, @InstrumentNo, @ControlNo, @Amount                
    
    IF @Status <> 0                
    BEGIN                
     select 'Error' AS RetStatus, 'LRMarking' AS RetMessage, @Status AS LRRetStatus                
     RETURN(0)                
    END                
    
    UPDATE t_LR_Issuance           
    SET Paid = 1, PaidDate = @wDate, PaidScrollNo = @ScrollNo, PaidMode = @DocumentType,IBCA = @ScrollNo,           
      PaidOperatorID = @OperatorID, PaidSupervisorID = @SupervisorID          
          
    WHERE OurBranchID = @BranchID AND InstrumentType = @InstrumentType AND InstrumentNo = @InstrumentNo AND ControlNo = @ControlNo           
      AND (wDate = @IssueDate Or DuplicateDate = @IssueDate)               AND Amount = @Amount AND Supervision = 'C'          
          
   SELECT @Status=@@ERROR          
          
    IF @Status <> 0           
    BEGIN          
     select 'Error' AS RetStatus, 'LRMarking' AS RetMessage, '' AS LRRetStatus                
     RETURN(0)                
    END                
    
  END                
            
  declare @IsCredit int          
  declare @VoucherID int          
  declare @TransactionMethod char(1)          
  declare @GLControl varchar(30)          
  declare @DescID varchar(30)          
  declare @Desc varchar(255)          
            
  select @VoucherID = ISNULL(MAX(VoucherID),0) + 1 from t_GLTransactions where OurBranchID = @OurBranchID          
  select @GLControl = ISNULL(GLControl, '') from t_Products where OurBranchID = @OurBranchID and ProductID = @ProductID          
            
  if @IsLocalCurrency = 1            
  begin            
    set @ForeignAmount = 0            
    set @ExchangeRate = 1            
    set @TransactionMethod = 'L'            
  end            
  else            
    set @TransactionMethod = 'A'          
            
  if @TrxType = 'C'          
  begin          
              
    set @IsCredit = 1          
              
    update t_Account set CashTotCr = CashTotCr + abs(@Amount), CashTotCrF = CashTotCrF + abs(@ForeignAmount)          
    WHERE OurBranchID=@BranchID AND AccountID=@AccountID          
              
  end          
  else          
  begin          
              
    set @IsCredit = 0          
              
    update t_Account set CashTotDr = CashTotDr + abs(@Amount), CashTotDrF = CashTotDrF + abs(@ForeignAmount)          
    WHERE OurBranchID=@BranchID AND AccountID=@AccountID          
              
  end          
            
  IF @TrxType='D' AND @ProductID <> 'GL' AND @IsHOAccount = 0    
  BEGIN            
                
      SELECT @ClearBalance=isNull(ClearBalance-FreezeAmount,0),            
       @FreezeAmount=isNull(FreezeAmount,0), @ShadowBalance=isNull(ShadowBalance,0)              
              
      FROM t_AccountBalance            
      WHERE OurBranchID=@BranchID AND AccountID=@AccountID          
                
      ----------------- W/H Tax Deduction            
          
      IF @CurrencyID = @LocalCurrency    
        SET @totAmount = @Amount    
      ELSE    
        SET @totAmount = @ForeignAmount    
          
      IF @AppWHTax = 1 --AND LEFT(@DescriptionID,2) <> 'TR'            
      BEGIN            
            
        SET @totAmount = @Amount + @WHTaxAmount            
    
        SELECT @WHTaxName = '' , @WHTaxCurrencyID = ''            
            
        SELECT @WHTaxName = Description , @WHTaxCurrencyID = CurrencyID             
        From t_GL            
        WHERE OurBranchID = @BranchID AND AccountID = @WHTaxAccountID            
            
        IF @WHTaxAccountID = '' OR @WHTaxName = ''            
         BEGIN            
          SELECT 'Error' AS RetStatus, 'WHTaxNotDefine' As RetMessage          
          RETURN(0)          
         END            
            
        IF UPPER(@WHTaxCurrencyID) <> UPPER(@LocalCurrency)          
        BEGIN            
          SELECT 'Error' AS RetStatus, 'WHTaxNotLocalCurrency' As RetMessage          
          RETURN(0)          
        END             
   
      END    
          
      --------------------------------------------------------------    
           
      IF @totAmount > @ClearBalance            
              
       BEGIN            
        SELECT 'Error' AS RetStatus, 'Balances' As RetMessage, @ClearBalance+@FreezeAmount as ClearBalance,           
        @FreezeAmount as FreezeAmount, @ShadowBalance as ShadowBalance          
        RETURN(0)          
       END            
          
      IF @AppWHTax = 1 --AND LEFT(@DescriptionID,2) <> 'TR'            
      BEGIN    
            
        SELECT @WHTaxDescription = 'Withholding Tax deduction (Withdrawl) On Amount: ' + Convert(Varchar(18), @Amount) +     
        ' AND ChequeNo: ' + Convert(Varchar(30), @ChequeID)    
            
        INSERT INTO t_OnLineCashTransaction            
        (            
         ScrollNo,SerialNo,OurBranchID,AccountID,AccountName,ProductID,CurrencyID,AccountType,ValueDate,wDate,TrxType,            
         ChequeID,ChequeDate, Amount, ForeignAmount,ExchangeRate, DescriptionID,Description,Supervision,            
         OperatorID,SupervisorID,IsLocalCurrency,SourceBranch, TargetBranch,Remarks,RefNo            
        )              
        VALUES            
        (            
         @ScrollNo,@WHTaxSerial,@OurBranchID,@AccountID,@AccountName,@ProductID,            
         @CurrencyID,@AccountType,@ValueDate,@mDate,'D','V',@ValueDate,    
         @WHTaxAmount, 0,1, 'WH1', @WHTaxDescription,'C', @OperatorID,@SupervisorID,            
         1, @SourceBranchID, @TargetBranchID, @Remarks + ' (Withholding Tax deduction)' , @RefNo            
        )    
            
        SELECT @Status=@@ERROR    
            
        IF @Status <> 0    
        BEGIN    
          SELECT 'Error' AS RetStatus, 'ChkStatus' As RetMessage, @Status AS Status    
          RETURN(0)    
        END    
                  
        insert into t_Transactions (OurBranchID, ScrollNo, SerialNo, RefNo, wDate, AccountType, DocumentType, AccountID, AccountName,          
        ProductID, CurrencyID, ValueDate, TrxType, ChequeID, ChequeDate, Amount, ForeignAmount, ExchangeRate, ProfitLoss, MeanRate,          
        DescriptionID, [Description], BankCode, BranchCode, TrxPrinted, [Status], IsLocalCurrency, OperatorID, SupervisorID,          
        AdditionalData, Remarks, IsMainTrx, DocType, GLVoucherID, SourceBranch, TargetBranch)          
                  
        VALUES (@OurBranchID,@ScrollNo,@WHTaxSerial,('WHT-'+CONVERT(varchar(30),@ScrollNo)),@mDate,@AccountType,'OT',@AccountID,@AccountName,          
        @ProductID,@CurrencyID, @ValueDate, @TrxType, 'V',@ChequeDate, @WHTaxAmount, 0, 1, 0, 1,          
        'WH1',@WHTaxDescription,'','', 0, 'C', 1, @OperatorID, @SupervisorID,          
        '', @Remarks, '1', 'OT', @VoucherID, @SourceBranchID, @TargetBranchID)          
                  
        set @DescID = 'OT1'          
        set @Desc = 'Account ID: ' + @AccountID +', Account Name: ' + @AccountName +' , Module: Online-Transfer Debit Entry...'          
                  
        insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],            
        CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,            
        Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType, SourceBranch, TargetBranch)            
                  
        values (@OurBranchID, @GLControl, @VoucherID, '1', @mDate, @ValueDate, @DescID, @Desc,          
        @CurrencyID, @WHTaxAmount, 0, 1, 0, 'OnLineTransfer', @TransactionMethod,          
        @OperatorID, @SupervisorID, '', '0', '0', '', '0', 'OT', @SourceBranchID, @TargetBranchID)          
                  
        SELECT @WHTaxDescription = 'Being Withholding Tax deduction (Withdrawl) On Account#: ' + @AccountID + ', Title: ' +    
        @AccountName + ', Amount: ' + Convert(Varchar(18),@Amount) + ' AND ChequeNo: ' + Convert(Varchar(11),@ChequeID),    
        @WHTaxSerial=@WHTaxSerial+1    
            
        INSERT INTO t_OnLineCashTransaction            
        (            
         ScrollNo,SerialNo,OurBranchID,AccountID,AccountName,ProductID,CurrencyID,AccountType,ValueDate,wDate,TrxType,            
ChequeID,ChequeDate, Amount, ForeignAmount,ExchangeRate, DescriptionID,Description,Supervision,            
         OperatorID,SupervisorID,IsLocalCurrency,SourceBranch, TargetBranch,Remarks,RefNo            
        )              
        VALUES            
        (            
         @ScrollNo,@WHTaxSerial,@OurBranchID,@WHTaxAccountID,@WHTaxName,'GL',            
         @WHTaxCurrencyID,'G',@ValueDate,@mDate,'C','V',@ValueDate,            
         @WHTaxAmount, 0,1, 'WH1', @WHTaxDescription,'C', @OperatorID,@SupervisorID,            
         1, @SourceBranchID, @TargetBranchID, @Remarks + ' (Withholding Tax deduction)' , @RefNo                   
        )    
                  
        SELECT @Status=@@ERROR    
            
        IF @Status <> 0    
        BEGIN    
          SELECT 'Error' AS RetStatus, 'ChkStatus' As RetMessage, @Status AS Status    
          RETURN(0)    
        END          
                  
        insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],            
        CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,            
        Indicator, ScrollNo, SerialNo, AdditionalData, Remarks, IsMainTrx, DocType, SourceBranch, TargetBranch)            
                  
        values (@OurBranchID, @WHTaxAccountID, @VoucherID, '2', @mDate, @ValueDate, 'WH1', @WHTaxDescription,          
        @WHTaxCurrencyID, @WHTaxAmount, 0, 1, 1, 'OnLineTransfer', @TransactionMethod,          
        @OperatorID, @SupervisorID, '', @ScrollNo, @WHTaxSerial, '', @Remarks, '1', 'OT', @SourceBranchID, @TargetBranchID)          
                  
      END    
          
      IF UPPER(@ChequeID) <> 'V'             
      BEGIN            
        -- Get Validate Cheque No            
        EXEC @Status = spc_ValidateChequeNo @BranchID, @AccountID, @ChequeID, @ChequeID    
            
        IF @Status <> 0            
        BEGIN                  
          SELECT 'Error' AS RetStatus, 'ChequeFails'+@Status As RetMessage      
          RETURN(0)          
        END            
              
        INSERT INTO t_ChequePaid        
        (OurBranchID, AccountID, ChequeID, Date, AccountType, CreateBy, CreateTime, CreateTerminal, SuperviseBy)        
        VALUES        
        (@BranchID,@AccountID, @ChequeID,@wDate,'C',@OperatorID,@mDate,'',@SupervisorID)        
                
      END            
          
  END    
      
  INSERT INTO t_OnLineCashTransaction      
   (      
    ScrollNo,SerialNo,OurBranchID,AccountID,AccountName,ProductID,CurrencyID,AccountType,ValueDate,wDate,TrxType,      
    ChequeID,ChequeDate, Amount, ForeignAmount,ExchangeRate, DescriptionID,Description,Supervision,      
    OperatorID,SupervisorID,IsLocalCurrency,SourceBranch, TargetBranch,RefNo, Remarks, AdditionalData,    
    AppWHTax, WHTaxAmount, WHTaxMode, WHTScrollNo    
   )        
  VALUES      
   (      
    @ScrollNo,@SerialNo,@OurBranchID,@AccountID,@AccountName,@ProductID, @CurrencyID,@AccountType,    
    @ValueDate,@mDate,@TrxType,@ChequeID,@ChequeDate, @Amount, @ForeignAmount,@ExchangeRate, @DescriptionID,    
    @Description,'C', @OperatorID,@SupervisorID, @IsLocalCurrency,@SourceBranchID,    
    @TargetBranchID,@RefNo, @Remarks, @AddData, @AppWHT, @WHTAmt, @WHTMode, @WHTScrollNo    
   )      
      
  SELECT @Status=@@ERROR      
      
  IF @Status <> 0       
  BEGIN      
        
    SELECT 'Error' AS RetStatus, 'ChkStatus' As RetMessage, @Status AS Status    
    RETURN(0)    
            
  END          
      
  select @VoucherID = ISNULL(MAX(VoucherID),0) + 1 from t_GLTransactions where OurBranchID = @OurBranchID          
            
  if @AccountType = 'C'          
  begin          
               
     insert into t_Transactions (OurBranchID, ScrollNo, SerialNo, RefNo, wDate, AccountType, DocumentType, AccountID, AccountName,          
     ProductID, CurrencyID, ValueDate, TrxType, ChequeID, ChequeDate, Amount, ForeignAmount, ExchangeRate, ProfitLoss, MeanRate,          
     DescriptionID, [Description], BankCode, BranchCode, TrxPrinted, [Status], IsLocalCurrency, OperatorID, SupervisorID,          
     AdditionalData, Remarks, IsMainTrx, DocType, GLVoucherID, SourceBranch, TargetBranch, AppWHTax, WHTaxAmount, WHTaxMode, WHTScrollNo)          
               
     VALUES (@OurBranchID,@ScrollNo,@SerialNo,@Refno,@mDate,@AccountType,'OT',@AccountID,@AccountName,          
     @ProductID,@CurrencyID, @ValueDate,@TrxType,@ChequeID,@ChequeDate, @Amount, @ForeignAmount,@ExchangeRate,          
     0,@ExchangeRate,@DescriptionID,@Description,'','', 0, 'C', @IsLocalCurrency, @OperatorID, @SupervisorID,          
     @AddData, @Remarks,'1','OT', @VoucherID, @SourceBranchID, @TargetBranchID, @AppWHT, @WHTAmt, @WHTMode, @WHTScrollNo)          
               
     if @TrxType = 'C'          
     begin          
      set @DescID = 'OT0'          
      set @Desc = 'Account ID: ' + @AccountID +', Account Name: ' + @AccountName +' , Module: Online-Transfer Credit Entry...'          
     end          
     else          
     begin          
      set @DescID = 'OT1'          
      set @Desc = 'Account ID: ' + @AccountID +', Account Name: ' + @AccountName +' , Module: Online-Transfer Debit Entry...'          
     end          
               
     insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],            
     CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,            
     Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType, SourceBranch, TargetBranch)            
               
     values (@OurBranchID, @GLControl, @VoucherID, '1', @mDate, @ValueDate, @DescID, @Desc,            
     @CurrencyID, @Amount, @ForeignAmount, @ExchangeRate, @IsCredit, 'OnLineTransfer', @TransactionMethod,          
     @OperatorID, @SupervisorID, '', '0', '0', '', '0', 'OT', @SourceBranchID, @TargetBranchID)            
               
  end          
  else          
  begin          
               
     insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],            
     CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,            
     Indicator, ScrollNo, SerialNo, AdditionalData, Remarks, IsMainTrx, DocType, SourceBranch, TargetBranch)            
               
     values (@OurBranchID, @AccountID, @VoucherID, '1', @mDate, @ValueDate, @DescriptionID, @Description,            
     @CurrencyID, @Amount, @ForeignAmount, @ExchangeRate, @IsCredit, 'OnLineTransfer', @TransactionMethod,          
     @OperatorID, @SupervisorID,'', @ScrollNo, @SerialNo, @AddData, @Remarks, '1', 'OT', @SourceBranchID, @TargetBranchID)          
               
  end          
            
/*  INSERT INTO t_OnLineCashTransaction      
   (      
    ScrollNo,SerialNo,OurBranchID,AccountID,AccountName,ProductID,CurrencyID,AccountType,ValueDate,wDate,TrxType,      
    ChequeID,ChequeDate, Amount, ForeignAmount,ExchangeRate, DescriptionID,Description,Supervision,      
    OperatorID,SupervisorID,IsLocalCurrency,SourceBranch, TargetBranch,RefNo, Remarks      
   )        
  VALUES      
   (      
    @ScrollNo,@SerialNo+1,@OurBranchID,@HOAccountID,@HOAccountName,'GL',      
    'PKR','G',@ValueDate,@mDate,@HOTrxType,@ChequeID,@ChequeDate,      
    @Amount, @ForeignAmount,@ExchangeRate, @DescriptionID,@Description,'C', @OperatorID,@SupervisorID,      
    1,@SourceBranchID, @TargetBranchID,@RefNo,@Remarks      
   )      
      
  SELECT @Status=@@ERROR      
      
  IF @Status <> 0       
   BEGIN   
    SET NOCOUNT OFF          
    RAISERROR('There Is An Error Occured While Creating Transaction In The Remote Branch',16,1) WITH SETERROR      
    RETURN (5000)      
   END      
        
*/      
--Naeem      
      
  SELECT 'Ok' AS RetStatus, 'TrxPosted' As RetMessage          
  RETURN(0)    
            
 END