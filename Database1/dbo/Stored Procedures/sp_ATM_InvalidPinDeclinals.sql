﻿CREATE   PROCEDURE [dbo].[sp_ATM_InvalidPinDeclinals]  
(      
 @OurBranchID            varchar(30),       
 @ATMID                  varchar(30),       
 @AccountID           varchar(30),          
 @RefNo                  varchar(50),      
 @PHXDate                datetime,      
 @MerchantType           varchar(30)='',      
 @AckInstIDCode          varchar(30)='',      
 @ProcCode                varchar(50)='',    
 @AcqCountryCode         varchar(3)='',    
 @MessageType       varchar(10)='',    
 @POSEntryMode    varchar(3)='',    
 @POSConditionCode   varchar(2)='',    
 @POSPINCaptCode   varchar(2)='',    
 @RetRefNum     varchar(15)='',    
 @CardAccptID    varchar(15)='',    
 @ResponseCode    varchar(2)='',    
 @ExtendedData    varchar(2)='',      
 @NewRecord              bit=1      
)
AS    
BEGIN    
    
 SET NOCOUNT ON      

 -------------------------------------
 DECLARE @AccountID2 as varchar(30) = @AccountID 
 DECLARE @OrgTrxRefNo as varchar(100) = ''  
 DECLARE @Amount as money = 0  
 DECLARE @mForeignAmount as money = 0  
 DECLARE @mExchangeRate as money = 1
 DECLARE @ConvRate as money = 0
 DECLARE @ForwardInstID as varchar(10)=''
 DECLARE @CurrCodeTran as varchar(3)=''
 DECLARE @CurrCodeSett as varchar(3)=''
 DECLARE @SettlmntAmount as money = 0  
 DECLARE @NameLocation as varchar(50) = ''
 DECLARE @VISATrxID as varchar(15)=''
 DECLARE @CAVV as char(1)=''
 DECLARE @AuthIDResp as varchar(6) = ''

 DECLARE @NarrationID nvarchar(30) = ''
 DECLARE @mDescriptionID as varchar(30) = '000'     
  DECLARE @mDescription as varchar(30) = ''     

 DECLARE @mAccountName as varchar(50)          
 DECLARE @mAccountType as char          
 DECLARE @mProductID as varchar(30)          
 DECLARE @mCurrencyID as varchar(30)        
 -------------------------------------------


    
 DECLARE @mIsLocalCurrency as bit      
  SET @mIsLocalCurrency=1       
 DECLARE @mwDate as datetime      
 DECLARE @mTrxType as char      
 DECLARE @STAN as varchar(30)    
  SET @STAN = RIGHT(@RefNo,6)    

     
 --Checking Variables    
 DECLARE @vCount1 as int      
  SET @vCount1 = 0  
 DECLARE @vClearBalance as money      
 DECLARE @vClearedEffects as money      
 DECLARE @vFreezeAmount as money      
 DECLARE @vMinBalance as money      
 DECLARE @vIsClose as char      
 DECLARE @vAreCheckBooksAllowed as bit      
 DECLARE @vAllowCreditTransaction as bit      
 DECLARE @Status as int    
 DECLARE @mLastEOD as datetime      
 DECLARE @mCONFRETI as datetime      
 DECLARE @AccountDigit as smallint     
  SET @AccountDigit= 16    
 DECLARE @LocalCurrencyID as varchar(3)    
 DECLARE @LocalCurrency as varchar(3)    
 DECLARE @CurrencyCode as varchar(30)    
  SET @CurrencyCode = ''    
    
 DECLARE @vAccountLimit as money    
 DECLARE @vCheckLimit as bit    
  SET @vCheckLimit = 0    
 DECLARE @vODLimitAllow as bit    
  SET @vODLimitAllow = 0    
 DECLARE @AvailableBalance as money          
 DECLARE @Value as varchar(2)      
  SET @value=@ResponseCode     
 DECLARE @IsLocalCurrency as bit    
  SET @IsLocalCurrency = 0     
 DECLARE @MobileNo nvarchar(30)  
 DECLARE @Email nvarchar(100)  
 DECLARE @rCurrencyID nvarchar(30)  
 DECLARE @ClientID nvarchar(30)  
  SET @MobileNo = ''  
  SET @Email = ''  
  SET @rCurrencyID = ''  
  SET @ClientID = ''  
  SET @NarrationID = ''  
 DECLARE @TrxDetails as varchar(50)  
  SET @TrxDetails = 'PIN Declinals'  
    
 SELECT @Status = -1      
   
 SELECT @LocalCurrencyID=LocalCurrencyID,@LocalCurrency=LocalCurrency,@vCheckLimit=CheckLimit FROM t_ATM_GlobalVariables WHERE OurBranchID = @OurBranchID    
 SELECT @mLastEOD=LastEOD,@mwDate=WorkingDate,@mCONFRETI=CONFRETI FROM t_Last WHERE (OurBranchID = @OurBranchID)    

 If (@ProcCode = 'PV')
	Set @NarrationID = 'PVXXXX'
Else If (@ProcCode = 'RP')
	Set @NarrationID = 'RPXXXX'

   
----------------------------
 IF (@ProcCode != 'PV' And @ProcCode !='RP') 
 BEGIN --Field Error      
  /**Starts Rejection if Not PV or RP **/  
    
  INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)  
  VALUES (@OurBranchID, @AccountID2, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @mDescription, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '09', 'Field Error', @TrxDetails, '0', @ExtendedData)  
    
  SELECT @Status=9, @AvailableBalance = 0, @vClearBalance = 0    
  SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @Value As value, '784' as CurrencyCode, '0' as AuthIDResp, @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID  
  RETURN (1)     
    
  /**Ends Rejection change if Not PV or RP **/  
    
 END

 SELECT @AccountID=ISNULL(AccountID,'') from t_DebitCards where OurBranchID = @OurBranchID AND DebitCardToken = @AccountID
 
 SET @AccountID2 = @AccountID
 
 SELECT @vCount1=count(*) From t_Account WHERE (OurBranchID = @OurBranchID) AND (AccountID = @AccountID)    
 IF @vCount1=0    
 BEGIN  --INVALID_Account    
  /** Rejection change STARTS **/  
     
  INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp)  
  VALUES (@OurBranchID, @AccountID2, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @mDescription, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '02', 'Invalid Account', @TrxDetails, '0')  
    
  SELECT @Status = 2, @AvailableBalance = 0, @vClearBalance = 0    
  SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, '00' As value, '784' as CurrencyCode, '0' as AuthIDResp, @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID  
  RETURN (1)    
    
  /** Rejection change ENDS **/  
 END     
   
 SELECT @mAccountName=B.Name,@mProductID=B.ProductID,@mCurrencyID=P.CurrencyID,@mAccountType='C',@vClearBalance=ISNULL(B.ClearBalance,0),    
 @vClearedEffects=ISNULL(B.Effects,0),@vFreezeAmount=ISNULL(B.FreezeAmount,0),@vMinBalance=ISNULL(P.MinBalance,0),@vIsClose=ISNULL(A.Status,''),    
 @vAreCheckBooksAllowed=P.AreCheckBooksAllowed,@vAllowCreditTransaction=ISNULL(A.AllowCreditTransaction,'0'),@CurrencyCode = ISNULL(c.CurrencyCode,''),    
 --@value=ISNULL(p.productatmcode,'00'),
 @vAccountLimit=ISNULL(B.Limit,0),@MobileNo=ISNULL(cust.MobileNo,''),@Email=ISNULL(cust.Email,''),@ClientID=cust.ClientID  
 FROM t_AccountBalance B     
 INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND B.ProductID = A.ProductID     
 INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND A.ProductID = P.ProductID     
 INNER JOIN t_Currencies C ON A.OurBranchID = C.OurBranchID AND A.CurrencyID = C.CurrencyID    
 INNER JOIN t_Customer cust ON A.OurBranchID = cust.OurBranchID AND A.ClientID = cust.ClientID  
 WHERE (A.OurBranchID = @OurBranchID) AND (A.AccountID = @AccountID2)    
    
 IF @vAccountLimit > 0 AND @vCheckLimit = 1 AND @vODLimitAllow = 1    
 BEGIN    
  SET @vClearBalance=(@vClearBalance+@vAccountLimit)    
 END    
    
 if (@LocalCurrencyID = @CurrencyCode)    
 BEGIN    
  SET @IsLocalCurrency = 1    
 END    
   
  INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)  
  VALUES (@OurBranchID, @AccountID, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @mDescription, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, @ResponseCode, @TrxDetails, @TrxDetails, '0', @ExtendedData)  
    
 SELECT @Status = '00',     
 @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,    
 @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100    
       
 SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value, @CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp, @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID  
 RETURN 0       
     
END