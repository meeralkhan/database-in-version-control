﻿CREATE Procedure [dbo].[spc_LR_GetGLListPostingCash]
(  
 @OurBranchID varchar(30),  
 @InstrumentType varchar(50)  
)  
AS
BEGIN

	Set NOCOUNT ON

	BEGIN TRY
   
		if @InstrumentType = 'PO'
		Begin  
			Select 'Ok' as ReturnStatus, a.PaymentOrder As AccountID, b.[Description] As AccountName, 
			@InstrumentType As InstrumentType, a.AuthStatus as LRSusStatus, b.AuthStatus as GLStatus
			From t_LR_SuspenseAccountsForCash a
			INNER JOIN t_GL b on a.OurBranchID = b.OurBranchID AND a.PaymentOrder=b.AccountID
			where a.OurBranchID = @OurBranchID
		End
		else if @InstrumentType = 'BC'
		Begin  
			Select 'Ok' as ReturnStatus, a.BankersCheque As AccountID, b.[Description] As AccountName, 
			@InstrumentType As InstrumentType, a.AuthStatus as LRSusStatus, b.AuthStatus as GLStatus  
			From t_LR_SuspenseAccountsForCash a
			INNER JOIN t_GL b on a.OurBranchID = b.OurBranchID AND a.BankersCheque=b.AccountID
			where a.OurBranchID = @OurBranchID
		End
		Else if @InstrumentType = 'PS'
		Begin  
			Select 'Ok' as ReturnStatus, a.PaySlip As AccountID, b.[Description] As AccountName, 
			@InstrumentType As InstrumentType, a.AuthStatus as LRSusStatus, b.AuthStatus as GLStatus  
			From t_LR_SuspenseAccountsForCash a
			INNER JOIN t_GL b on a.OurBranchID = b.OurBranchID AND a.PaySlip=b.AccountID
			where a.OurBranchID = @OurBranchID
		End  
		Else if @InstrumentType = 'DD'
		Begin  
			Select 'Ok' as ReturnStatus, a.DemandDraft As AccountID, b.[Description] As AccountName,
			@InstrumentType As InstrumentType, a.AuthStatus as LRSusStatus, b.AuthStatus as GLStatus  
			From t_LR_SuspenseAccountsForCash a
			INNER JOIN t_GL b on a.OurBranchID = b.OurBranchID AND a.DemandDraft=b.AccountID
			where a.OurBranchID = @OurBranchID
		End  
		Else if @InstrumentType = 'TT'
		Begin  
			Select 'Ok' as ReturnStatus, a.TeleTransfer As AccountID, b.[Description] As AccountName, 
			@InstrumentType As InstrumentType, a.AuthStatus as LRSusStatus, b.AuthStatus as GLStatus  
			From t_LR_SuspenseAccountsForCash a
			INNER JOIN t_GL b on a.OurBranchID = b.OurBranchID AND a.TeleTransfer=b.AccountID
			where a.OurBranchID = @OurBranchID
		End  

	END TRY

	BEGIN CATCH
		select 'Error' as ReturnStatus, ERROR_MESSAGE() as ReturnMessage
	END CATCH
END