﻿CREATE   PROCEDURE [dbo].[spc_Forgot_Password]        
(         
 @OurBranchID varchar(30),         
 @Username varchar(30),         
 @NPassword varchar(max),         
 @CPassword varchar(max),        
 @UserTerminal varchar(30)        
)         
AS         
    
set nocount on         
    
BEGIN TRY         
         
 if @NPassword != @CPassword         
 begin         
   select 'Error' AS ReturnStatus, 'PasswordDoesNotMatch' AS ReturnMessage;         
 end         
 ELSE IF (select COUNT(*) FROM t_Map where OperatorID = @Username AND CannotChgPwd = '0') > 0         
 begin         
           
   IF EXISTS (SELECT [Password] FROM tbl_UserPasswords WHERE Username = @Username AND         
   @NPassword IN (SELECT TOP 11 [Password] FROM tbl_UserPasswords ORDER BY CreateTime DESC))        
   BEGIN        
           
     select 'Error' AS ReturnStatus, 'PasswordAlreadyUsed' AS ReturnMessage;        
           
   END        
   ELSE        
   BEGIN        
             
     UPDATE t_Map SET [Password] = @NPassword WHERE OperatorID = @Username and OurBranchID = @OurBranchID;        
             
     INSERT INTO tbl_UserPasswords (OurBranchID,Username, [Password], CreateTime, CreateTeminal)        
     VALUES (@OurBranchID,@Username, @NPassword, GETDATE(), @UserTerminal);        
             
     delete from tbl_UserTokens WHERE OurBranchID = @OurBranchID AND Username = @Username;        
             
     select 'Ok' AS ReturnStatus, 'PasswordChanged' AS ReturnMessage;        
             
   END        
 end         
 else         
 begin         
   select 'Error' AS ReturnStatus, 'UnSufficientRights' AS ReturnMessage;         
 end         
         
END TRY         
BEGIN CATCH         
   select 'Error' AS ReturnStatus, ERROR_NUMBER() AS ReturnErrorNo, ERROR_MESSAGE() AS ReturnMessage;         
END CATCH