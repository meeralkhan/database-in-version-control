﻿create procedure [dbo].[spc_rptGLDailyMovement] (
	@OurBranchID nvarchar(30),
	@ToDate datetime
)
AS
BEGIN

	SELECT GL.AccountID, GL.Description, GL.CurrencyID as GLCurrency, GL.AccountType,
	OpeningBalanceLocal = 
	(
		SELECT SUM(Amount) FROM t_GLTransactions WHERE OurBranchID = @OurBranchID AND AccountID = GL.AccountID 
		AND (Date < convert(datetime, @ToDate)) AND ISNULL(Status,'') <> 'R' AND IsCredit = 1
	) - 
	(
		SELECT SUM(Amount) FROM t_GLTransactions WHERE OurBranchID = @OurBranchID AND AccountID = GL.AccountID 
		AND (Date < convert(datetime, @ToDate)) AND ISNULL(Status,'') <> 'R' AND IsCredit = 0
	),
	GL.Balance,
	OpeningBalanceForeign = 
	(
		SELECT SUM(ForeignAmount) FROM t_GLTransactions WHERE OurBranchID = @OurBranchID AND AccountID = GL.AccountID 
		AND (Date < convert(datetime, @ToDate)) AND ISNULL(Status,'') <> 'R' AND IsCredit = 1
	) - 
	(
		SELECT SUM(ForeignAmount) FROM t_GLTransactions WHERE OurBranchID = @OurBranchID AND AccountID = GL.AccountID 
		AND (Date < convert(datetime, @ToDate)) AND ISNULL(Status,'') <> 'R' AND IsCredit = 0
	),
	GL.ForeignBalance,
	DebitTotal = 
	(	
		SELECT SUM(Balance) FROM t_GL WHERE OurBranchID = GL.OurBranchID AND Balance <> 0 AND AccountID IN
		(	SELECT DISTINCT(AccountID) from t_GLTransactions
			WHERE OurBranchID = GL.OurBranchID AND [Date] >= CONVERT(DATETIME, @ToDate) AND ISNULL(Status, '') <> 'R' AND Balance < 0
		)
	),
	CreditTotal = 
	(	
		SELECT SUM(Balance) FROM t_GL WHERE OurBranchID = GL.OurBranchID AND Balance <> 0 AND AccountID IN
		(	SELECT DISTINCT(AccountID) from t_GLTransactions
			WHERE OurBranchID = GL.OurBranchID AND [Date] >= CONVERT(DATETIME, @ToDate) AND ISNULL(Status, '') <> 'R' AND Balance > 0
		)
	)
	
	FROM t_GL GL
	where GL.OurBranchID = @OurBranchID AND GL.Balance <> 0 AND GL.AccountID IN
		(	SELECT DISTINCT(AccountID) from t_GLTransactions
			WHERE OurBranchID = @OurBranchID AND [Date] >= CONVERT(DATETIME, @ToDate) AND ISNULL(Status, '') <> 'R')
	ORDER BY AccountType, AccountID
	
END