﻿CREATE PROCEDURE [dbo].[spc_DeleteTransferTrx]        
(        
 @OurBranchID varchar(30),        
 @AccountID varchar(30),        
 @SupervisorID varchar(30),        
 @ScrollNo numeric(10),        
 @SerialNo money,        
 @AddData text = ''        
)        
        
AS        
   SET NOCOUNT ON        
         
   DECLARE @WHTaxApply int              
   DECLARE @WHTaxScrollNo int              
         
   DECLARE @vDate datetime         
   DECLARE @wDate datetime         
   DECLARE @mDate Varchar(32)        
   DECLARE @ChequeID varchar(30)        
   DECLARE @ClearBalance money        
   DECLARE @Effects money        
   DECLARE @Limit money        
   DECLARE @TrxType char(1)        
   DECLARE @Amount money        
   DECLARE @ForeignAmount money        
   DECLARE @IsLocalCurrency bit        
   DECLARE @Status char(1)        
   DECLARE @AccountType char(1)        
   DECLARE @Supervision char(1)        
   DECLARE @RefNo varchar(100)          
   DECLARE @AccountStatus char(1)        
           
   DECLARE @GLVoucherID decimal(24,0)        
   DECLARE @GLID varchar(30)        
   DECLARE @tGLID varchar(30)        
   DECLARE @TrxPrinted int        
   DECLARE @ProductID varchar(30)        
   DECLARE @GLControl varchar(30)        
   DECLARE @AccountName varchar(100)        
   DECLARE @CurrencyID varchar(30)        
   DECLARE @OperatorID varchar(30)        
   DECLARE @ChequeDate varchar(30)        
   DECLARE @DescriptionID varchar(30)        
   DECLARE @Description varchar(100)        
   DECLARE @BankCode varchar(30)        
   DECLARE @BranchCode varchar(30)        
   DECLARE @ExchangeRate decimal(24,6)        
   DECLARE @tDate Varchar(10)        
   DECLARE @AuthStatus char(1)        
   DECLARE @ChannelId varchar(30)      
   DECLARE @VirtualAccountID varchar(30)      
   DECLARE @VirtualAccountTitle varchar(100)      
           
   SELECT @Status = 'R',@AuthStatus='',@GLVoucherID=0        
           
   /* Get Transfer Transaction */        
   SELECT @wDate=wDate, @vDate=ValueDate,@ChequeID= ChequeID,@AccountType=AccountType,@TrxType=TrxType, @Amount = isnull(Amount,0),        
   @ForeignAmount = ForeignAmount,@IsLocalCurrency=IsLocalCurrency,@Supervision = Supervision,@RefNo = RefNo,@AccountName=AccountName,        
   @WHTaxApply = ISNULL(AppWHTax,0), @WHTaxScrollNo = ISNULL(WHTaxScrollNo,0),@ProductID=ProductID,@CurrencyID = CurrencyID,        
   @ChequeDate=ChequeDate,@ExchangeRate=ExchangeRate,@DescriptionID=DescriptionID,@Description=Description,@TrxPrinted=TrxPrinted,        
   @BankCode=BankCode,@BranchCode=BranchCode,@AddData=AdditionalData,@GLID=GLID,@OperatorID=OperatorID,      
   @ChannelId=ChannelId,@VirtualAccountID=VirtualAccountID,@VirtualAccountTitle=VirtualAccountTitle      
   FROM t_TransferTransactionModel        
   WHERE (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (ScrollNo = @ScrollNo) and (SerialNo = @SerialNo)        
           
   set @tGLID = @GLID        
   if @tGLID = ''        
      set @tGLID = null        
           
 IF LEFT(@RefNo, 3) = 'ADV'        
 BEGIN        
   SELECT 'ADV' as RetStatus -- Advances Trx. cannot be Rejected from this Procedure        
   RETURN;        
 END        
               
 IF LEFT(@RefNo, 2) = 'FD'        
 BEGIN        
   SELECT 'TDR' as RetStatus -- Fixed Deposit Trx. cannot be Rejected from this Procedure        
   RETURN;        
 END        
               
 IF LEFT(@RefNo, 3) = 'OR1'        
 BEGIN        
   SELECT 'OWR' as RetStatus -- Advances Trx. cannot be Rejected from this Procedure        
   RETURN;        
 END        
               
 IF LEFT(@RefNo, 3) = 'OWR'        
 BEGIN        
   SELECT 'OWR' as RetStatus -- Advances Trx. cannot be Rejected from this Procedure        
   RETURN;        
 END        
               
 IF LEFT(@RefNo, 3) = 'IR1'        
 BEGIN        
   SELECT 'IWR' as RetStatus -- Advances Trx. cannot be Rejected from this Procedure        
   RETURN;        
 END        
           
 IF LEFT(@RefNo, 3) = 'IWR'        
 BEGIN        
   SELECT 'IWR' as RetStatus -- Advances Trx. cannot be Rejected from this Procedure        
   RETURN;        
 END        
           
 IF LEFT(@RefNo, 3) = 'OWC'        
 BEGIN        
   SELECT 'OWC' as RetStatus -- Advances Trx. cannot be Rejected from this Procedure        
   RETURN;        
 END        
         
 if @TrxType = 'D' AND @WHTaxApply = 1 AND @WHTaxScrollNo <> 0 AND @AccountType = 'C'              
  begin  declare @WHTaxSupervisionStatus char(1)              
        
  Select @WHTaxSupervisionStatus=Supervision from t_TransferTransactionModel               
  WHERE (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (ScrollNo = @WHTaxScrollNo) and (SerialNo = @SerialNo)              
        
  if @WHTaxSupervisionStatus <> 'R'              
  BEGIN              
   declare @CrGLAccount varchar(30)              
           
   exec spc_DeleteTransferTrx @OurBranchID, @AccountID, @SupervisorID, @WHTaxScrollNo, 1              
         
   select @CrGLAccount=AccountID from t_TransferTransactionModel              
   WHERE (OurBranchID = @OurBranchID) and (ScrollNo = @WHTaxScrollNo) and (SerialNo = 2)              
         
   exec spc_DeleteTransferTrx @OurBranchID, @CrGLAccount, @SupervisorID, @WHTaxScrollNo, 2              
  END              
  end              
               
 SET @tDate = Convert(varchar(10),@wDate,101)        
        
   IF @Supervision = 'R'         
      BEGIN         
 SELECT '3' as RetStatus -- Trx. Already Rejected        
 RETURN           
      END        
 -- PRINT  '1-' + @SUPERVISION        
 IF @Supervision = '#'         
  BEGIN            
   Set @AuthStatus='#'         
   SET @SUPERVISION = (SELECT  ModuleStatus               
   FROM t_Authorization_RejectedTrx        
      WHERE OurBranchID = @OurBranchID and ScrollNo = @ScrollNo AND SerialNo = @SerialNo        
     AND ModuleType = 'T' AND Status = '#'        
     AND convert(varchar(10), wDate,101) = @tDate )        
   -- -- PRINT  '2-' + @SUPERVISION        
  END        
               
        
 /* Initialize */        
 SELECT @AccountStatus = ''         
 IF @AccountType = 'C'        
  BEGIN        
   SELECT @AccountStatus = IsNull(Status,'') FROM t_Account        
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID        
           
   IF Upper(Ltrim(RTrim(@AccountStatus))) = 'C'           
 BEGIN        
  SELECT '4' as RetStatus -- Closed Account Can Not Rejected...        
  RETURN        
 END           
  END        
        
   SELECT @mDate = Convert(varchar(10),@wDate ,101) + ' ' + convert(varchar(20),GetDate(),114)        
           
   IF @AccountType = 'C'        
      BEGIN        
 /* Get Current ClearBalance, Effective Balance and Limit */         
      SELECT @ClearBalance=ClearBalance,@Effects=Effects,@limit=IsNull(Limit ,0)        
 FROM t_AccountBalance        
 WHERE (OurBranchID = @OurBranchID) AND (AccountID = @AccountID)          
--         
        
 IF @Supervision = '*' OR @Supervision = 'P'        
  BEGIN        
   -- PRINT  '2 * AND p -' + @SUPERVISION         
    IF @TrxType = 'D'         
    BEGIN        
       IF @IsLocalCurrency='0'         
 BEGIN         
    UPDATE t_AccountBalance        
    SET ShadowBalance = ShadowBalance + @ForeignAmount        
    WHERE OurBranchID = @OurBranchID and AccountID = @AccountID        
 END        
       ELSE IF @IsLocalCurrency='1'         
 BEGIN         
     -- PRINT  '3 * AND p -' + cast (@Amount as varchar(32))        
    UPDATE t_AccountBalance        
    SET ShadowBalance = ShadowBalance + @Amount        
    WHERE OurBranchID = @OurBranchID and AccountID = @AccountID        
 END        
--print @Supervision        
--return        
    END        
    END        
        
        
        
 ELSE IF @Supervision = 'C'         
  BEGIN        
   -- PRINT  '4 c -' + cast (@Amount as varchar(32))         
   IF @TrxType = 'C'        
   BEGIN        
    IF @AuthStatus=''         
    BEGIN        
IF @IsLocalCurrency='0' AND @vDate > @wDate        
BEGIN         
UPDATE t_AccountBalance               
     SET Effects = Effects - @ForeignAmount,        
  LocalEffects = LocalEffects - @Amount        
     WHERE OurBranchID = @OurBranchID and AccountID = @AccountID        
      END        
          
ELSE IF @IsLocalCurrency='0' AND @vDate <= @wDate        
 BEGIN         
 UPDATE t_AccountBalance        
    SET ClearBalance = ClearBalance - @ForeignAmount,        
  LocalClearBalance = LocalClearBalance - @Amount        
    WHERE OurBranchID = @OurBranchID and AccountID = @AccountID        
 END        
          
ELSE IF @IsLocalCurrency='1' AND @vDate > @wDate        
 BEGIN         
    UPDATE t_AccountBalance        
 SET Effects = Effects - @Amount        
    WHERE OurBranchID = @OurBranchID and AccountID = @AccountID        
 END        
          
ELSE IF @IsLocalCurrency='1' AND @vDate <= @wDate        
 BEGIN         
    UPDATE t_AccountBalance        
    SET ClearBalance = ClearBalance - @Amount        
    WHERE OurBranchID = @OurBranchID and AccountID = @AccountID        
      END        
    END        
  END        
        
     ELSE --IF @TrxType = 'D'        
  BEGIN        
   -- PRINT  '5  -' + cast (@Amount as varchar(32))        
     IF @IsLocalCurrency='0'        
BEGIN         
    -- PRINT  '6  -' + cast (@Amount as varchar(32))        
 UPDATE t_AccountBalance        
 SET ClearBalance = ClearBalance + @ForeignAmount,        
     LocalClearBalance = LocalClearBalance + @Amount        
 WHERE OurBranchID = @OurBranchID and AccountID = @AccountID        
END              
         
     ELSE IF @IsLocalCurrency='1'        
BEGIN         
    -- PRINT  '7 - BEFORE UPDATE' + cast (@Amount as varchar(32))        
    --SELECT ClearBalance FROM T_Accountbalance WHERE OurBranchID = @OurBranchID and AccountID = @AccountID            
 UPDATE t_AccountBalance        
SET ClearBalance = ClearBalance + @Amount        
 WHERE OurBranchID = @OurBranchID and AccountID = @AccountID        
    -- PRINT  '7 - AFTER UPDATE' + cast (@Amount as varchar(32))        
    --SELECT ClearBalance FROM T_Accountbalance WHERE OurBranchID = @OurBranchID and AccountID = @AccountID        
        
END        
  END -- TRX TYPE END        
  END -- SUPERVISION END          
      END -- A/C TYPE END        
   ELSE        
      BEGIN        
  -- PRINT  '8 -' + cast (@Amount as varchar(32))        
 SELECT @ClearBalance=0,@Effects=0,@limit=0        
      END        
        
--return        
        
   /*UpDate Transaction File Status*/        
   UPDATE t_TransferTransactionModel        
   SET Supervision = @Status, SupervisorID = @SupervisorID        
   WHERE (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (ScrollNo = @ScrollNo) and (SerialNo = @SerialNo)        
            
   IF @vDate > @wDate AND @TrxType = 'C'             
      BEGIN        
 DELETE From t_Clearing        
 WHERE (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (ScrollNo = @ScrollNo)  and (SerialNo = @SerialNo)  and Status <> 'C'        
      END        
         
   /* Create a New record For Superviser */        
   IF @SupervisorID <> ''        
      BEGIN        
 --IF @IsLocalCurrency = 0         
  --SELECT @Amount = @ForeignAmount        
        
 INSERT INTO t_SupervisedBy        
     (OurBranchID,AccountID,[Date],ScrollNo,ChequeID,ClearBalance,Effects,Limit,TrxType,Amount,[Status],SupervisorID,      
  TransactionCategory)        
 VALUES        
     (@OurBranchID,@AccountID,@mDate,@ScrollNo,@ChequeID,@ClearBalance,@Effects,@Limit,@TrxType,@Amount,@Status,      
  @SupervisorID,'T')        
        
      END          
          
   /* Update Customer Status */        
   Update t_CustomerStatus        
   Set Status = @Status           
   Where (OurBranchID=@OurBranchID) and (AccountID = @AccountID)         
  and (ScrollNo = @ScrollNo) and (SerialNo = @SerialNo) and  (Left(TransactionType,1)='T')        
        
   /*@Update OtherTransaction Table For Transfer*/        
        
/** rameez starts **        
        
   Update t_OtherTransactionStatus         
   Set Status = @Status        
   Where (OurBranchID=@OurBranchID) and (AccountID = @AccountID) and (ScrollNo = @ScrollNo)         
 and (SerialNo = @SerialNo) and  (Left(TransactionType,1)='T')          
 Update t_LRSRemit        
 Set Status=@Status        
 Where (OurBranchID = @OurBranchID) and (Date = @wDate)  and (RefNo = (Select RefNo From t_TransferTransactionmodel          
  Where (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (ScrollNo = @ScrollNo)  and (SerialNo = @SerialNo)))             
        
** rameez ends **/        
        
/*        
  Update t_MFLoanAccounts        
  Set IsPosted = 0,ScrollNo = 0        
  Where (OurBranchID = @OurBranchID) and (SanctionDate = @Date)  and (ScrollNo = @ScrollNo)        
        
  Update t_MFLoanPayments        
  Set IsPosted = 0,ScrollNo = 0,OperatorID = '',PostDate = Null        
  Where (OurBranchID = @OurBranchID) and (PostDate = @Date)  and (ScrollNo = @ScrollNo)         
*/        
             
 --Local Remittance Reject            
             
 if exists( select * from t_LR_Issuance where OurBranchID = @OurBranchID and ScrollNo = @ScrollNo AND            
 TranSerialNo = @SerialNo and convert(varchar(10), wDate, 101) = @tDate and Supervision = 'R' )            
 begin            
   SELECT 'LRR' as RetStatus -- LR Trx. Already Rejected            
   RETURN            
 end            
             
 if exists( select * from t_LR_Issuance where OurBranchID = @OurBranchID and ScrollNo = @ScrollNo AND            
 TranSerialNo = @SerialNo and convert(varchar(10), wDate, 101) = @tDate and Cancel = 1 )            
 begin            
               
   select top 1 'LRC' as RetStatus, CancelDate, CancellScrollNo from t_LR_Issuance where OurBranchID = @OurBranchID            
   and ScrollNo = @ScrollNo and convert(varchar(10), wDate, 101) = @tDate and Cancel = 1 -- LR Trx. Cancelled            
               
   RETURN            
 end            
             
 if exists( select * from t_LR_Issuance where OurBranchID = @OurBranchID and ScrollNo = @ScrollNo AND            
 TranSerialNo = @SerialNo and convert(varchar(10), wDate, 101) = @tDate and Paid = 1 )            
 begin            
               
   select top 1 'LRP' as RetStatus, PaidDate, PaidMode, PaidScrollNo from t_LR_Issuance where OurBranchID = @OurBranchID            
   and ScrollNo = @ScrollNo and convert(varchar(10), wDate, 101) = @tDate and Paid = 1 -- LR Trx. Paid            
               
   RETURN            
 end            
        
 Update t_LR_Issuance            
 Set Supervision = @Status, SuperviseBy = @SupervisorID, SuperviseTime = @mDate        
 Where (OurBranchID=@OurBranchID) and (ScrollNo = @ScrollNo) and (convert(varchar(10), wDate, 101) = @tDate)             
         
 Update t_LR_Issuance        
 Set Supervision = 'C', Paid = 0, PaidDate = '1/1/1900', PaidMode = ''        
 Where (OurBranchID=@OurBranchID) and (PaidScrollNo = @ScrollNo) and PaidMode = 'TR'        
 and (convert(varchar(20), wDate, 101) = convert(varchar(20), @tDate, 101))        
               
 Update t_LR_Issuance        
 Set Supervision = 'C', Cancel = 0, CancelDate = '1/1/1900'              
 Where (OurBranchID=@OurBranchID) and (CancellScrollNo = @ScrollNo) and Cancel = '1'          
 and (convert(varchar(20), CancelDate, 101) = convert(varchar(20), @tDate, 101))        
         
             
/** rameez starts **        
            
--Local Remittance Advice Reject        
  Update t_LR_Advice        
  Set Supervision = @Status        
  Where (OurBranchID=@OurBranchID) and  (ScrollNo = @ScrollNo) and  (wDate = @wDate)        
        
--Local Remittance Advice Unpaid Mark         
  Update t_LR_Advice         
  Set Paid = 0,        
  PaidDate = '1/1/1900',        
  PaidScrollNo = 0,              
  PaidMode = 'NA'             
  Where (OurBranchID=@OurBranchID) and  (PaidScrollNo = @ScrollNo) and  (PaidDate = @wDate) and (PaidMode = 'TR')        
        
 ----FOREIGN REMITTANCE------------------        
 ------------------------------------------------------------------        
 ---INLAND OUTWARD LODGEMENT ENTRY REJECTED        
 IF @REFNO='OBC-I'        
  BEGIN        
   UPDATE t_FrInlandOutward        
   SET STATUS='X'         
   WHERE (Transferno=CONVERT(VARCHAR(10),CONVERT(NUMERIC,@SCROLLNO))) AND TRDate=@WDATE        
         
   UPDATE t_FrInlandOutward        
   SET STATUS='X'         
   WHERE (ReTransferNo=CONVERT(VARCHAR(10),CONVERT(NUMERIC,@SCROLLNO))) AND ReTRDate=@WDATE        
  END        
         
 ---INLAND INWARD LODGEMENT ENTRY REJECTED         
 ELSE IF @REFNO='IBC-I'        
  BEGIN        
   UPDATE t_FrInlandInward        
   SET STATUS='X'         
   WHERE (Transferno=CONVERT(VARCHAR(10),CONVERT(NUMERIC,@SCROLLNO))) AND TRDate=@WDATE        
         
   UPDATE t_FrInlandInward        
   SET STATUS='X'         
   WHERE (ReTransferNo=CONVERT(VARCHAR(10),CONVERT(NUMERIC,@SCROLLNO))) AND ReTRDate=@WDATE        
  END        
         
 ---FOREIGN OUTWARD LODGEMENT ENTRY REJECTED        
 ELSE IF left(@REFNO,3)='FDD'         
  BEGIN        
   print @SCROLLNO         
   print  @REFNO        
   UPDATE t_frremitt        
   SET STATUS='E'               
   WHERE  REFNO=@REFNO and transferscrollno=@scrollno        
         
  END        
         
 ---FOREIGN OUTWARD LODGEMENT ENTRY REJECTED        
 ELSE IF left(@REFNO,3)='FTT'         
  BEGIN        
   UPDATE t_frremitt        
   SET STATUS='E'         
   WHERE  REFNO=@REFNO and transferscrollno=@scrollno        
 --  UPDATE t_FrOutlandOutward        
 --  SET STATUS='X'         
 --  WHERE (ReTransferNo=CONVERT(VARCHAR(10),CONVERT(NUMERIC,@SCROLLNO))) AND ReTRDate=@WDATE        
  END        
         
         
 ELSE IF @REFNO='FOB=I'        
  BEGIN        
   UPDATE t_FrOutlandOutward        
   SET STATUS='X'         
   WHERE (Transferno=CONVERT(VARCHAR(10),CONVERT(NUMERIC,@SCROLLNO))) AND TRDate=@WDATE        
         
   UPDATE t_FrOutlandOutward        
   SET STATUS='X'         
   WHERE (ReTransferNo=CONVERT(VARCHAR(10),CONVERT(NUMERIC,@SCROLLNO))) AND ReTRDate=@WDATE        
  END        
         
 ---FOREIGN INWARD LODGEMENT ENTRY REJECTED        
 ELSE IF @REFNO='FIB=I'        
  BEGIN        
   UPDATE t_FrOutlandInward        
   SET STATUS='X'         
   WHERE (Transferno=CONVERT(VARCHAR(10),CONVERT(NUMERIC,@SCROLLNO))) AND TRDate=@WDATE        
         
   UPDATE t_FrOutlandInward        
   SET STATUS='X'         
   WHERE (ReTransferNo=CONVERT(VARCHAR(10),CONVERT(NUMERIC,@SCROLLNO))) AND ReTRDate=@WDATE        
  END        
 ---        
 ---INLAND OUTWARD REALIZE/RETURN ENTRY REJECTED        
 ELSE IF @REFNO='OB-REL' OR @REFNO='OB-RET'        
  BEGIN        
   UPDATE t_FrInlandOutward        
   SET STATUS='C',RealizeTRDate='01/01/1900',IsRealize='0',RealizeDate='01/01/1900'         
   WHERE (RealizeTRNO=CONVERT(VARCHAR(10),CONVERT(NUMERIC,@SCROLLNO))) AND RealizeTRDate=@WDATE        
        
   UPDATE t_FrInlandOutward        
   SET STATUS='C',RealizeTRDate='01/01/1900',IsRealize='0',RealizeDate='01/01/1900'        
   WHERE (RealizeTRNO='*' + CONVERT(VARCHAR(10),CONVERT(NUMERIC,@SCROLLNO))) AND RealizeTRNO=@WDATE        
  END        
  ---INWARD INWARD REALIZE/RETURN ENTRY REJECTED        
 ELSE IF @REFNO='IB-REL' OR @REFNO='IB-RET'        
  BEGIN        
   UPDATE t_FrInlandInward        
   SET STATUS='C',RealizeTRDate='01/01/1900',IsRealize='0',RealizeDate='01/01/1900'        
   WHERE (RealizeTRNO=CONVERT(VARCHAR(10),CONVERT(NUMERIC,@SCROLLNO))) AND RealizeTRNO=@WDATE        
         
   UPDATE t_FrInlandInward        
   SET STATUS='C',RealizeTRDate='01/01/1900',IsRealize='0',RealizeDate='01/01/1900'        
   WHERE (RealizeTRNO='*' + CONVERT(VARCHAR(10),CONVERT(NUMERIC,@SCROLLNO))) AND RealizeTRNO=@WDATE        
  END        
         
 ---FOREIGN OUTWARD REALIZE/RETURN ENTRY REJECTED        
 ELSE IF @REFNO='FOB=REL' OR @REFNO='FOB=RET'        
  BEGIN        
   UPDATE t_FrOutlandOutward        
   SET STATUS='C',RealizeTRDate='01/01/1900',IsRealize='0',RealizeDate='01/01/1900'             
   WHERE (RealizeTRNO=CONVERT(VARCHAR(10),CONVERT(NUMERIC,@SCROLLNO))) AND RealizeTRNO=@WDATE        
        
   UPDATE t_FrOutlandOutward        
   SET STATUS='C',RealizeTRDate='01/01/1900',IsRealize='0',RealizeDate='01/01/1900'        
   WHERE (RealizeTRNO='*' + CONVERT(VARCHAR(10),CONVERT(NUMERIC,@SCROLLNO))) AND RealizeTRNO=@WDATE        
  END        
         
 ---FOREIGN INWARD REALIZE/RETURN ENTRY REJECTED        
 ELSE IF @REFNO='FIB=REL' OR @REFNO='FIB=RET'        
  BEGIN        
   UPDATE t_FrOutlandInward        
   SET STATUS='C',RealizeTRDate='01/01/1900',IsRealize='0',RealizeDate='01/01/1900'        
   WHERE (RealizeTRNO= CONVERT(VARCHAR(10),CONVERT(NUMERIC,@SCROLLNO))) AND RealizeTRNO=@WDATE        
         
   UPDATE t_FrOutlandInward        
   SET STATUS='C',RealizeTRDate='01/01/1900',IsRealize='0',RealizeDate='01/01/1900'        
   WHERE (RealizeTRNO='*' + CONVERT(VARCHAR(10),CONVERT(NUMERIC,@SCROLLNO))) AND RealizeTRNO=@WDATE        
  END        
         
 --------------------------------------------------------------------------------------------        
         
         
   if  @Refno like 'FD%'        
   Begin           
    Delete from t_Deposits where scrollno=@Scrollno and        
    OurBranchid =@OurBranchid and Issuedate=@wdate        
          
       if (select count(*) from t_interestmovement where         
    scrollno=@Scrollno and        
    OurBranchid =@OurBranchid and InterestTransactionOn=@wdate) > 0        
        
 Begin        
          
  update a set a.Interestpaidupto =a.MInterestpaidupto,        
   a.InterestPaid=a.InterestPaid -b.InterestAmount,        
   a.TaxAmount =a.TaxAmount - b.TaxAmount,        
   a.Zakat=a.Zakat - b.ZakatAmt,        
a.CloseDate = '01-01-1900',        
IsClosed=0          
   from        
   t_deposits a ,t_interestmovement b where        
   a.OurBranchid=b.OurBranchid and        
   a.Accountid=b.Accountid and        
   a.Productid=b.Productid and        
   a.Receiptid=b.Receiptid and        
   b.Scrollno=@Scrollno and        
   b.OurBranchid=@OurBranchid and        
   b.InterestTransactionOn=@wdate        
        
     Delete from t_interestmovement where scrollno=@Scrollno and        
     OurBranchid =@OurBranchid and InterestTransactionOn=@wdate        
          
        
    end        
         
   end               
         
  ** rameez ends **/         
        
   IF @TrxType='D'        
      BEGIN        
 IF UPPER(LTRIM(RTRIM(@ChequeID))) <> 'V' AND LTRIM(RTRIM(@ChequeID)) <> ''         
  BEGIN        
     DELETE FROM t_ChequePaid        
     WHERE (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (ChequeID = @ChequeID)        
  END        
      END        
        
  /** rameez starts **        
 UPDATE t_Authorization_RejectedTrx        
 SET Status = 'C', SupervisorID = @SupervisorID, SupDate = @mDate, SupTerminalID = Host_Name()        
 WHERE OurBranchID = @OurBranchID AND ScrollNO = @ScrollNO AND ModuleType = 'T' AND Status = '#' AND SerialNo = @SerialNo        
  ** rameez ends **/        
           
   IF @Supervision = 'C'        
   BEGIN        
             
     IF @AccountType = 'C'        
     begin        
          
update t_Transactions set [Status] = 'R' where OurBranchID = @OurBranchID and AccountID = @AccountID        
and DocType = 'T' and CONVERT(varchar(10),wDate,101)= @tDate and ScrollNo = @ScrollNo and SerialNo = @SerialNo        
          
select @GLVoucherID = isnull(GLVoucherID,0) from t_Transactions where OurBranchID = @OurBranchID and AccountID = @AccountID            
and DocType = 'T' and CONVERT(varchar(10),wDate,101)= @tDate and ScrollNo = @ScrollNo and SerialNo = @SerialNo            
          
select @GLControl = ISNULL(GLControl, '') from t_Products where OurBranchID = @OurBranchID and ProductID = @ProductID            
          
end        
     else        
     begin        
          
select @GLControl = @AccountID        
          
select @GLVoucherID = isnull(VoucherID,0) from t_GLTransactions where OurBranchID = @OurBranchID and AccountID = @AccountID        
and DocType = 'T' and CONVERT(varchar(10),[Date],101)= @tDate and ScrollNo = @ScrollNo and SerialNo = @SerialNo        
          
     end        
   END        
           
   if @GLVoucherID = 0        
   begin        
             
     if @AccountType = 'C'        
     begin        
          
insert into t_RejectedTrx (OurBranchID, ScrollNo, SerialNo, RefNo, wDate, AccountType, DocumentType, AccountID,       
AccountName, ProductID, CurrencyID, ValueDate, TrxType, ChequeID, ChequeDate, Amount, ForeignAmount, ExchangeRate,       
ProfitLoss, MeanRate, DescriptionID, [Description], BankCode, BranchCode, TrxPrinted, GLID, [Status], IsLocalCurrency,       
OperatorID, SupervisorID, AdditionalData, IsMainTrx, DocType, AppWHTax, WHTaxMode, WHTaxAmount, WHTScrollNo,       
ChannelId, VirtualAccountID, VirtualAccountTitle)        
          
VALUES (@OurBranchID,@ScrollNo,@SerialNo,@RefNo,@wDate,@AccountType,'T',@AccountID,@AccountName,@ProductID,@CurrencyID,      
@vDate,@TrxType,@ChequeID,@ChequeDate, @Amount, @ForeignAmount,@ExchangeRate,0,@ExchangeRate,@DescriptionID,@Description,      
@BankCode,@BranchCode, @TrxPrinted, @tGLID, 'R', @IsLocalCurrency, @OperatorID, @SupervisorID, @AddData,'1','T',       
@WHTaxApply, 'T', '0', @WHTaxScrollNo,@ChannelId,@VirtualAccountID,@VirtualAccountTitle)        
          
     end        
     else        
     begin        
          
DECLARE @IsCredit int        
DECLARE @TransactionMethod char(1)        
          
if @TrxType = 'C'        
 set @IsCredit = 1        
else        
 set @IsCredit = 0        
          
if @IsLocalCurrency = '1'        
begin        
             
 set @ForeignAmount = 0        
     set @ExchangeRate = 1        
 set @TransactionMethod = 'L'        
             
end        
else        
 set @TransactionMethod = 'A'        
             
insert into t_RejectedGLTrx        
(OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description], CurrencyID,        
Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,         
Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType, ChannelId, VirtualAccountID, VirtualAccountTitle)        
          
VALUES (@OurBranchID, @AccountID, '1', '1', @wDate, @vDate, @DescriptionID, @Description, @CurrencyID, @Amount,       
@ForeignAmount, @ExchangeRate, @IsCredit, 'Transfer', @TransactionMethod, @OperatorID, @SupervisorID, '', @ScrollNo,       
@SerialNo, @AddData, '1', 'T', @ChannelId,@VirtualAccountID,@VirtualAccountTitle)        
          
     end        
             
   end        
   else        
   begin        
             
     update t_GLTransactions set [Status] = 'R'        
     where OurBranchID = @OurBranchID and DocType = 'T'            
     and CONVERT(varchar(10),[Date],101) = @tDate  and VoucherID = @GLVoucherID        
             
     if @IsLocalCurrency = '1'        
     begin        
          
  if @TrxType = 'C'        
  begin        
             
   Update t_GL Set Balance = Balance - @Amount Where OurBranchID = @OurBranchID and AccountID = @GLControl        
             
  end        
  else        
  begin        
             
   Update t_GL Set Balance = Balance + @Amount Where OurBranchID = @OurBranchID and AccountID = @GLControl        
              
  end        
          
     end        
     else        
     begin        
          
  if @TrxType = 'C'        
  begin        
             
   Update t_GL Set Balance = Balance - @Amount, ForeignBalance = ForeignBalance - @ForeignAmount        
   Where OurBranchID = @OurBranchID and AccountID = @GLControl        
        
  end        
  else        
  begin        
             
   Update t_GL Set Balance = Balance + @Amount, ForeignBalance = ForeignBalance + @ForeignAmount        
   Where OurBranchID = @OurBranchID and AccountID = @GLControl        
             
  end        
          
     end        
             
   end        
           
   SELECT '0' as RetStatus        
   RETURN        
              
        
        
  /** rameez starts **        
         
 update t_Deposits set Minterestpaidupto='01-01-1900'        
 where Minterestpaidupto is null        
               
 ** rameez ends **/