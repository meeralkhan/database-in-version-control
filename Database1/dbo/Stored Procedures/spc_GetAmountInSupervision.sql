﻿CREATE PROCEDURE [dbo].[spc_GetAmountInSupervision]      
(      
 @OurBranchID varchar(30),      
 @AccountID varchar(30)      
)      
  
AS      
  
SET NOCOUNT ON      
  
 DECLARE @TotalCredit money, @TotalDebit money,      
  @ReturnValue as char(1)      
  set @ReturnValue='0'      
      
 IF (SELECT COUNT(AccountID)  FROM t_CustomerStatus      
  WHERE (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (status = '*' OR status = 'U' or status = 'P' ) ) > 0       
  BEGIN      
   set @ReturnValue='1'      
   SELECT @TotalCredit=SUM(amount)       
   FROM t_CustomerStatus      
   WHERE (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (status = '*' OR status = 'U' ) AND (amount > 0)      
      
   SELECT @TotalDebit=abs(SUM(amount) )      
   FROM t_CustomerStatus      
   WHERE (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (status = '*' OR status = 'U' or status = 'P') AND (amount < 0)      
  END      
      
      
 SELECT @ReturnValue as ReturnValue, IsNull(@TotalDebit,0) as TotalDebit,IsNull( @TotalCredit,0) as TotalCredit