﻿CREATE Procedure [dbo].[spc_GetMMLimitUtilized]    
(    
  @OurBranchID varchar(30),      
  @PartyID varchar(30),    
  @SettlementDate datetime,      
  @MaturityDate datetime,    
  @DealType varchar(2)    
)    
  
As    
   
 IF @DealType='CL' OR @DealType='CN' OR @DealType='CI'    
 BEGIN    
    
    SELECT 'Ok' AS ReturnStatus, isnull(sum(Amount),0)  as Amount    
    FROM t_MoneyMarketNew    
    WHERE OurBranchID = @OurBranchID AND ClientID = @PartyID AND TypeOfTransaction In ('CL','CN','CI')     
     AND SettlementDate< @MaturityDate AND MaturityDate > @SettlementDate AND Status <> 'R'      
    
 END    
 ELSE    
 BEGIN    
    
    SELECT 'Ok' AS ReturnStatus, isnull(sum(Amount),0)  as Amount    
    FROM t_MoneyMarketNew    
    WHERE OurBranchID = @OurBranchID AND AccountID = @PartyID AND TypeOfTransaction=@DealType     
     AND SettlementDate< @MaturityDate AND MaturityDate > @SettlementDate AND Status <> 'R'      
    
 END