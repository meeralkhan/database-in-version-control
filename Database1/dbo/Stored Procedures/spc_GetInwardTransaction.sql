﻿CREATE PROCEDURE [dbo].[spc_GetInwardTransaction]          
          
 @OurBranchID varchar(30),
 @SerialNo int=0 ,              
 @AccountID varchar(30) =''              
AS              
 SET NOCOUNT ON              
 Declare @BankID varchar(30),              
  @BranchID varchar(30),              
  @BankName varchar(50),              
  @BranchName varchar(50),              
  @AccountType varchar(1),              
  @LocalCurrency varchar(30),
  @Status varchar(1)              
              
 SET @AccountType = ''              
              
 Select @Status = Status From t_InwardClearing              
 where SerialNo = @SerialNo AND OurBranchID = @OurBranchID AND AccountID = @AccountID          
     
 Select @BankID = BankID , @BranchID = BranchID From t_InwardClearing               
 where SerialNo = @SerialNo AND OurBranchID = @OurBranchID and AccountID = @AccountID          
           
 select @LocalCurrency = LocalCurrency from t_GlobalVariables Where OurBranchID = @OurBranchID          
           
 Select @BankName = FullName from t_Banks              
 Where OurBranchID = @OurBranchID AND BankID = @BankID              
              
 Select @BranchName = Name from t_Branches              
 Where OurBranchID  = @OurBranchID AND BankID = @BankID AND BranchID = @BranchID          
           
 IF @AccountID <> ''              
 BEGIN              
  Select @AccountType = AccountType From t_InwardClearing              
  where SerialNo = @SerialNo AND OurBranchID = @OurBranchID AND AccountID = @AccountID          
            
  IF @AccountType = 'C'              
             
   Select '1' as RetStatus, i.wDate, i.ValueDate, i.BranchID, isnull(@BranchName,'') as BranchName,          
   i.BankID, @BankName as BankName, i.AccountType, i.AccountID, i.AccountName, i.ProductID,           
   p.Description as ProductName, i.CurrencyID, c.Description as CurrencyName, i.ChequeID, i.ChequeDate,          
   Amount, ForeignAmount, ExchangeRate, Payee, DescriptionID, i.Description, Reminder, i.Status, AdditionalData,          
   ClearBalance, Effects, ab.ShadowBalance, Limit, ISNULL(p.MinBalance,0) ProductMinBalance, FreezeAmount BlockedAmount,         
   p.CanGoInDebit AS CanGoCrDr, ab.IsFreezed        
           
   FROM t_InwardClearing i          
   INNER JOIN  t_AccountBalance ab ON i.OurBranchID = ab.OurBranchID AND i.AccountID = ab.AccountID          
   INNER JOIN t_Currencies c ON i.OurBranchID = c.OurBranchID AND i.CurrencyID = c.CurrencyID          
   INNER JOIN t_Products p ON i.OurBranchID = p.OurBranchID AND i.ProductID = p.ProductID          
             
   WHERE i.SerialNo = @SerialNo AND i.OurBranchID = @OurBranchID AND i.AccountID = @AccountID          
             
  ELSE              
             
   Select '1' as RetStatus, i.wDate, ValueDate, BranchID, isnull(@BranchName,'') as BranchName, BankID,          
   @BankName as BankName, i.AccountType, i.AccountID, AccountName, i.ProductID, 'General Ledger' as ProductName,          
   i.CurrencyID, c.Description as CurrencyName, ChequeID, ChequeDate, Amount, ForeignAmount, ExchangeRate,          
   Payee, DescriptionID, i.Description, '' as Reminder, i.Status, AdditionalData,          
   CASE g.CurrencyID WHEN @LocalCurrency THEN Balance ELSE ForeignBalance END AS ClearBalance,          
   '0' Effects, '0' ShadowBalance, '0' Limit, '0' ProductMinBalance, '0' BlockedAmount, '0' CanGoCrDr, '0' IsFreezed        
           
   FROM t_InwardClearing i          
   INNER JOIN t_GL g ON i.OurBranchID = g.OurBranchID AND i.AccountID = g.AccountID          
   INNER JOIN t_Currencies c ON i.OurBranchID = c.OurBranchID AND i.CurrencyID = c.CurrencyID          
             
   WHERE i.SerialNo = @SerialNo AND i.OurBranchID = @OurBranchID AND i.AccountID = @AccountID          
             
 END              
 ELSE              
  IF @serialno =0              
   Select * from t_InwardClearing              
   Where OurBranchID = @OurBranchID          
  else              
   Select * from t_InwardClearing              
   Where SerialNo = @SerialNo AND OurBranchID = @OurBranchID