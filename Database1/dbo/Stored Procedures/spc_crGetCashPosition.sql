﻿CREATE PROCEDURE [dbo].[spc_crGetCashPosition]            
 @OurBranchID varchar(30),              
 @HOAccountID varchar(30),              
 @CurrencyID varchar(4),              
 @IsLocalCurrency Bit=1,              
 @ReceivedFromBanks Money=0,              
 @CReceivedFromBanks Money=0,              
 @PaidToBanks Money=0,              
 @CPaidToBanks Money=0,              
 @ReceivedFromClients Money=0,              
 @CReceivedFromClients Money=0,              
 @PaidToClients Money=0,              
 @CPaidToClients Money=0,              
 @GLID  Varchar(15) = ''              
AS            
            
SET NOCOUNT ON            
            
 Select @ReceivedFromBanks=0, @CReceivedFromBanks=0, @PaidToBanks=0, @CPaidToBanks=0, @ReceivedFromClients=0,     
        @CReceivedFromClients=0, @PaidToClients=0, @CPaidToClients=0    
              
 IF @GLID <> ''              
  BEGIN              
      
   IF @IsLocalCurrency=1              
   BEGIN              
     
    --------------       
    SELECT  @ReceivedFromBanks=ISNULL(SUM(Amount),0)     
    FROM t_CashTransactionModel  
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID and TrxType ='C'     
    AND CurrencyID = @CurrencyID AND Supervision = 'C'    
      
    SELECT  @ReceivedFromBanks = @ReceivedFromBanks + ISNULL(SUM(Amount),0)     
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID and TrxType ='C'     
    AND CurrencyID = @CurrencyID AND Supervision = 'C'    
    --------------  
      
    SELECT @CReceivedFromBanks=COUNT(*)     
    FROM t_CashTransactionModel  
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID AND TrxType ='C' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C' AND GLID = @GLID    
      
    SELECT @CReceivedFromBanks = @CReceivedFromBanks + COUNT(*)     
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID AND TrxType ='C' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C' AND GLID = @GLID    
    --------------  
            
    SELECT  @PaidToBanks=ISNULL(SUM(Amount),0)     
    FROM t_CashTransactionModel   
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID and TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C' AND GLID = @GLID              
      
    SELECT  @PaidToBanks=@PaidToBanks + ISNULL(SUM(Amount),0)     
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID and TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C' AND GLID = @GLID              
    ---------------  
        
    SELECT @CPaidToBanks=COUNT(*)     
    FROM t_CashTransactionModel   
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID AND TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C' AND GLID = @GLID    
      
    SELECT @CPaidToBanks=@CPaidToBanks + COUNT(*)     
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID AND TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C' AND GLID = @GLID    
    ---------------  
        
    SELECT  @ReceivedFromClients=ISNULL(SUM(Amount),0)     
    FROM t_CashTransactionModel   
    WHERE OurBranchID = @OurBranchID AND AccountID <> @HOAccountID and TrxType ='C' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C' AND GLID = @GLID              
      
    SELECT  @ReceivedFromClients=@ReceivedFromClients + ISNULL(SUM(Amount),0)   
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND AccountID <> @HOAccountID and TrxType ='C' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C' AND GLID = @GLID              
    ---------------  
        
    SELECT @CReceivedFromClients=COUNT(*)     
    FROM t_CashTransactionModel   
    WHERE OurBranchID = @OurBranchID AND AccountID <> @HOAccountID AND TrxType ='C' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C' AND GLID = @GLID    
      
    SELECT @CReceivedFromClients=@CReceivedFromClients + COUNT(*)     
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND AccountID <> @HOAccountID AND TrxType ='C' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C' AND GLID = @GLID    
    ---------------  
      
    SELECT  @PaidToClients=ISNULL(SUM(Amount),0)     
    FROM t_CashTransactionModel   
    WHERE OurBranchID = @OurBranchID AND ((AccountID <> @HOAccountID and TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C') OR (AccountID <> @HOAccountID and TrxType ='D' AND Supervision = 'C'     
    AND left(Refno, 9) = 'LOCALCASH' )) AND GLID = @GLID     
      
    SELECT  @PaidToClients=@PaidToClients + ISNULL(SUM(Amount),0)     
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND ((AccountID <> @HOAccountID and TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C') OR (AccountID <> @HOAccountID and TrxType ='D' AND Supervision = 'C'     
    AND left(Refno, 9) = 'LOCALCASH' )) AND GLID = @GLID     
    ---------------  
    SELECT @CPaidToClients=COUNT(*)     
    FROM t_CashTransactionModel   
    WHERE OurBranchID = @OurBranchID AND ((AccountID <> @HOAccountID AND TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C') OR (AccountID <> @HOAccountID and TrxType ='D' AND Supervision = 'C' AND     
    left(Refno, 9) = 'LOCALCASH' )) AND GLID = @GLID    
      
    SELECT @CPaidToClients=@CPaidToClients + COUNT(*)     
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND ((AccountID <> @HOAccountID AND TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C') OR (AccountID <> @HOAccountID and TrxType ='D' AND Supervision = 'C' AND     
    left(Refno, 9) = 'LOCALCASH' )) AND GLID = @GLID    
        
   END              
   else              
   BEGIN              
    -----------------------  
    SELECT  @ReceivedFromBanks=ISNULL(SUM(ForeignAmount),0)     
    FROM t_CashTransactionModel   
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID and TrxType ='C' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C' AND GLID = @GLID     
      
    SELECT  @ReceivedFromBanks=@ReceivedFromBanks + ISNULL(SUM(ForeignAmount),0)     
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID and TrxType ='C' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C' AND GLID = @GLID     
    -----------------------  
    SELECT @CReceivedFromBanks=COUNT(*)     
    FROM t_CashTransactionModel   
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID AND TrxType ='C' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C' AND GLID = @GLID              
      
    SELECT @CReceivedFromBanks=@CReceivedFromBanks + COUNT(*)     
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID AND TrxType ='C' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C' AND GLID = @GLID              
    ------------------------  
    SELECT @PaidToBanks=ISNULL(SUM(ForeignAmount),0)     
    FROM t_CashTransactionModel   
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID and TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C' AND GLID = @GLID     
      
    SELECT @PaidToBanks=@PaidToBanks + ISNULL(SUM(ForeignAmount),0)     
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID and TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C' AND GLID = @GLID     
    ------------------------  
      
    SELECT @CPaidToBanks=COUNT(*)     
    FROM t_CashTransactionModel   
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID AND TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C' AND GLID = @GLID              
      
    SELECT @CPaidToBanks=@CPaidToBanks+COUNT(*)     
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID AND TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C' AND GLID = @GLID              
    ------------------------  
    SELECT @ReceivedFromClients=ISNULL(SUM(ForeignAmount),0)     
    FROM t_CashTransactionModel   
    WHERE OurBranchID = @OurBranchID AND AccountID <> @HOAccountID and TrxType ='C' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C' AND GLID = @GLID    
      
    SELECT @ReceivedFromClients=@ReceivedFromClients + ISNULL(SUM(ForeignAmount),0)     
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND AccountID <> @HOAccountID and TrxType ='C' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C' AND GLID = @GLID    
    -------------------------  
        
    SELECT @CReceivedFromClients=COUNT(*)     
    FROM t_CashTransactionModel   
    WHERE OurBranchID = @OurBranchID AND AccountID <> @HOAccountID AND TrxType ='C' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C' AND GLID = @GLID    
      
    SELECT @CReceivedFromClients=@CReceivedFromClients + COUNT(*)     
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND AccountID <> @HOAccountID AND TrxType ='C' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C' AND GLID = @GLID      
    --------------------------  
    SELECT @PaidToClients=ISNULL(SUM(ForeignAmount),0)     
    FROM t_CashTransactionModel   
    WHERE OurBranchID = @OurBranchID AND AccountID <> @HOAccountID and TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C' AND left(Refno, 9) <> 'LOCALCASH' AND GLID = @GLID    
      
    SELECT @PaidToClients=@PaidToClients + ISNULL(SUM(ForeignAmount),0)     
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND AccountID <> @HOAccountID and TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C' AND left(Refno, 9) <> 'LOCALCASH' AND GLID = @GLID    
    ---------------------------  
              
    SELECT @CPaidToClients=COUNT(*)     
    FROM t_CashTransactionModel   
    WHERE OurBranchID = @OurBranchID AND AccountID <> @HOAccountID AND TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C' AND left(Refno, 9) <> 'LOCALCASH' AND GLID = @GLID    
      
    SELECT @CPaidToClients=@CPaidToClients + COUNT(*)     
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND AccountID <> @HOAccountID AND TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C' AND left(Refno, 9) <> 'LOCALCASH' AND GLID = @GLID    
    ---------------------------  
   END              
              
  END              
 ELSE              
  BEGIN              
   IF @IsLocalCurrency=1              
   BEGIN              
    ----------------------------  
    SELECT  @ReceivedFromBanks=ISNULL(SUM(Amount),0)     
    FROM t_CashTransactionModel   
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID and TrxType ='C' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C'    
      
    SELECT @ReceivedFromBanks= @ReceivedFromBanks + ISNULL(SUM(Amount),0)     
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID and TrxType ='C' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C'    
    ---------------------------  
    SELECT @CReceivedFromBanks=COUNT(*)     
    FROM t_CashTransactionModel   
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID AND TrxType ='C' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C'    
      
    SELECT @CReceivedFromBanks= @CReceivedFromBanks + COUNT(*)     
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID AND TrxType ='C' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C'    
    ---------------------------  
    SELECT  @PaidToBanks=ISNULL(SUM(Amount),0)     
    FROM t_CashTransactionModel   
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID and TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C'              
      
    SELECT  @PaidToBanks=@PaidToBanks + ISNULL(SUM(Amount),0)     
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID and TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C'              
    ----------------------------  
        
    SELECT @CPaidToBanks=COUNT(*)     
    FROM t_CashTransactionModel   
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID AND TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C'    
      
    SELECT @CPaidToBanks= @CPaidToBanks + COUNT(*)     
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID AND TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C'    
    ---------------------------  
        
    SELECT  @ReceivedFromClients=ISNULL(SUM(Amount),0)     
    FROM t_CashTransactionModel   
    WHERE OurBranchID = @OurBranchID AND AccountID <> @HOAccountID and TrxType ='C' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C'              
      
    SELECT  @ReceivedFromClients= @ReceivedFromClients + ISNULL(SUM(Amount),0)     
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND AccountID <> @HOAccountID and TrxType ='C' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C'              
    ----------------------------  
        
    SELECT @CReceivedFromClients=COUNT(*)     
    FROM t_CashTransactionModel   
    WHERE OurBranchID = @OurBranchID AND AccountID <> @HOAccountID AND TrxType ='C' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C'    
      
    SELECT @CReceivedFromClients= @CReceivedFromClients + COUNT(*)     
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND AccountID <> @HOAccountID AND TrxType ='C' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C'    
    ----------------------------  
        
    SELECT @PaidToClients=ISNULL(SUM(Amount),0)     
    FROM t_CashTransactionModel   
    WHERE OurBranchID = @OurBranchID AND (AccountID <> @HOAccountID and TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C') OR (AccountID <> @HOAccountID and TrxType ='D' AND Supervision = 'C'      
    AND left(Refno, 9) = 'LOCALCASH' )    
      
    SELECT @PaidToClients= @PaidToClients + ISNULL(SUM(Amount),0)     
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND (AccountID <> @HOAccountID and TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C') OR (AccountID <> @HOAccountID and TrxType ='D' AND Supervision = 'C'      
    AND left(Refno, 9) = 'LOCALCASH' )    
    -----------------------------  
        
    SELECT @CPaidToClients=COUNT(*)     
    FROM t_CashTransactionModel   
    WHERE OurBranchID = @OurBranchID AND (AccountID <> @HOAccountID AND TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C') OR (AccountID <> @HOAccountID and TrxType ='D' AND Supervision = 'C'     
    AND left(Refno, 9) = 'LOCALCASH' )    
      
    SELECT @CPaidToClients=@CPaidToClients + COUNT(*)     
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND (AccountID <> @HOAccountID AND TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C') OR (AccountID <> @HOAccountID and TrxType ='D' AND Supervision = 'C'     
    AND left(Refno, 9) = 'LOCALCASH' )    
    ------------------------------  
   END              
   else              
   BEGIN              
    ---------------------------  
    SELECT  @ReceivedFromBanks=ISNULL(SUM(ForeignAmount),0)     
    FROM t_CashTransactionModel   
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID and TrxType ='C' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C'              
      
    SELECT  @ReceivedFromBanks= @ReceivedFromBanks + ISNULL(SUM(ForeignAmount),0)     
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID and TrxType ='C' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C'              
    ----------------------------  
        
    SELECT @CReceivedFromBanks=COUNT(*)     
    FROM t_CashTransactionModel   
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID AND TrxType ='C' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C'                        
      
    SELECT @CReceivedFromBanks= @CReceivedFromBanks + COUNT(*)     
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID AND TrxType ='C' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C'                   
    -----------------------------  
        
    SELECT  @PaidToBanks=ISNULL(SUM(ForeignAmount),0)     
    FROM t_CashTransactionModel   
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID and TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C'    
      
    SELECT  @PaidToBanks=@PaidToBanks + ISNULL(SUM(ForeignAmount),0)     
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID and TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C'    
    ------------------------------  
        
    SELECT @CPaidToBanks=COUNT(*)     
    FROM t_CashTransactionModel   
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID AND TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C'              
      
    SELECT @CPaidToBanks=@CPaidToBanks+COUNT(*)     
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND AccountID = @HOAccountID AND TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C'  
    ------------------------------  
    SELECT  @ReceivedFromClients=ISNULL(SUM(ForeignAmount),0)     
    FROM t_CashTransactionModel   
    WHERE OurBranchID = @OurBranchID AND AccountID <> @HOAccountID and TrxType ='C' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C'              
      
    SELECT  @ReceivedFromClients= @ReceivedFromClients + ISNULL(SUM(ForeignAmount),0)     
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND AccountID <> @HOAccountID and TrxType ='C' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C'              
    -----------------------------  
    SELECT @CReceivedFromClients=COUNT(*)     
    FROM t_CashTransactionModel   
    WHERE OurBranchID = @OurBranchID AND AccountID <> @HOAccountID AND TrxType ='C' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C'              
      
    SELECT @CReceivedFromClients= @CReceivedFromClients + COUNT(*)     
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND AccountID <> @HOAccountID AND TrxType ='C' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C'              
    -------------------------------  
    SELECT  @PaidToClients=ISNULL(SUM(ForeignAmount),0)     
    FROM t_CashTransactionModel   
    WHERE OurBranchID = @OurBranchID AND AccountID <> @HOAccountID and TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C' AND left(Refno, 9) <> 'LOCALCASH'     
      
    SELECT  @PaidToClients=@PaidToClients + ISNULL(SUM(ForeignAmount),0)     
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND AccountID <> @HOAccountID and TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C' AND left(Refno, 9) <> 'LOCALCASH'     
    ----------------------------------  
    SELECT @CPaidToClients=COUNT(*)     
    FROM t_CashTransactionModel   
    WHERE OurBranchID = @OurBranchID AND AccountID <> @HOAccountID AND TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C' AND left(Refno, 9) <> 'LOCALCASH'     
      
    SELECT @CPaidToClients= @CPaidToClients + COUNT(*)     
    FROM t_OnLineLocalCashTransaction  
    WHERE OurBranchID = @OurBranchID AND AccountID <> @HOAccountID AND TrxType ='D' AND CurrencyID = @CurrencyID     
    AND Supervision = 'C' AND left(Refno, 9) <> 'LOCALCASH'     
    ----------------------------------  
   END              
  END              
              
--  if @ReceivedFromBanks IS NULL              
--   Select @ReceivedFromBanks=0              
              
--  if @CReceivedFromBanks IS NULL              
--   Select @CReceivedFromBanks=0              
--               
--  if @PaidToBanks IS NULL              
--   Select @PaidToBanks=0              
--               
--  if @CPaidToBanks IS NULL              
--   Select @CPaidToBanks=0              
--               
--  if @ReceivedFromClients IS NULL              
--   Select @ReceivedFromClients=0              
--               
--  if @CReceivedFromClients IS NULL              
--   Select @CReceivedFromClients=0              
--               
--  if @PaidToClients IS NULL              
--   Select @PaidToClients=0              
--               
--  if @CPaidToClients IS NULL              
--   Select @CPaidToClients=0              
              
              
 SELECT @ReceivedFromBanks AS ReceivedFromBanks, @CReceivedFromBanks as CReceivedFromBanks, @PaidToBanks as PaidToBanks,              
   @CPaidToBanks as CPaidToBanks, @ReceivedFromClients as ReceivedFromClients, @CReceivedFromClients as CReceivedFromClients,              
   @PaidToClients as PaidToClients, @CPaidToClients as CPaidToClients