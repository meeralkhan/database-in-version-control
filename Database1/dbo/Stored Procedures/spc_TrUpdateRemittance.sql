﻿CREATE PROCEDURE [dbo].[spc_TrUpdateRemittance]          
(          
 @OurBranchID varchar(4),          
 @ScrollNo numeric,          
 @DateOfIssue DateTime,          
 @BatchNo int,          
 @BatchInstrumentNo int,          
 @InstrumentType char(1),          
 @WorkingDate datetime,          
 @StatusFlag Char(1) = 'P',  -- P = Paid, C = Cancelled    
 @Supervision char(1) = 'C',    
 @PaidMode varchar(2)      
)          
          
AS      
      
SET NOCOUNT ON      
      
DECLARE @RetStatus as int      
      
      
Update t_LR_Issuance          
    SET Paid = 1,          
        PaidDate = @WorkingDate,          
        PaidScrollNo = @ScrollNo,            
        PaidMode = @PaidMode,    
        Supervision = @Supervision    
          
    WHERE (OurBranchID = @OurBranchID)       
      and (convert(varchar(20), wDate, 101) = @DateOfIssue)       
      and (Left(InstrumentType,1) = upper(@InstrumentType))      
      and (InstrumentNo = @BatchInstrumentNo)       
      and (SerialNo = @BatchNo)          
          
  /*        
 Update t_LR_Advice          
    SET Paid = 1,          
        PaidDate = @WorkingDate,          
        PaidScrollNo = @ScrollNo,            
        PaidMode = @PaidMode           
    WHERE (OurBranchID = @OurBranchID) and (wDate = @DateOfIssue) and (Left(InstrumentType,1) = upper(@InstrumentType))           
   and (InstrumentNo = @BatchInstrumentNo) and  (SerialNo = @BatchNo)          
          
 Update t_LR_Advice          
    SET Paid = 1,          
        PaidDate = @WorkingDate,          
        PaidScrollNo = @ScrollNo,            
        PaidMode = @PaidMode           
    WHERE (OurBranchID = @OurBranchID) and (wDate = @DateOfIssue) and (Left(InstrumentType,1) = upper(@InstrumentType))           
   and (ControlNo = @BatchInstrumentNo) and  (SerialNo = @BatchNo) and (InstrumentNo = 0)          
  */      
        
  SELECT 'Ok' AS ReturnStatus