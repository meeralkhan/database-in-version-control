﻿CREATE PROCEDURE [dbo].[spc_GetProfitPayAccounts]        
(        
 @OurBranchID nvarchar(30),         
 @AccountID nvarchar(30)='',         
 @ReceiptID nvarchar(30)=''         
)         
         
AS         
         
SET NOCOUNT ON         
         
BEGIN         
         
 DECLARE @Names VARCHAR(MAX)         
 DECLARE @SQL NVARCHAR(MAX)         
 DECLARE @Par NVARCHAR(MAX)         
         
 SELECT @Names = COALESCE(@Names + ', t.', '') + name FROM sys.columns         
 where object_id = object_id('t_TDRIssuance') and name         
 not in ('CreateBy','CreateTime','CreateTerminal','UpdateBy','UpdateTime','UpdateTerminal','frmName',         
 'VerifyBy','VerifyTime','VerifyTerminal','VerifyStatus','SuperviseBy','SuperviseTime','SuperviseTerminal',         
 'OurBranchID','AccountID','ProductID','CurrencyID','RecieptID','Amount')         
         
 SET @SQL = 'SELECT a.OurBranchID, a.AccountID, a.Name, a.ProductID, a.CurrencyID, t.RecieptID, t.Amount, '         
 SET @SQL = @SQL + ' p.GLInterestPayable, p.GLInterestPaid, p.IsAdvanceProfitPay, p.GLServiceCharges, '         
 SET @SQL = @SQL + ' p.CreditRounding, p.TaxRounding, p.CreditInterestDays, p.RateType, t.' + @Names         
 SET @SQL = @SQL + ' FROM t_TDRIssuance t'         
 SET @SQL = @SQL + ' INNER JOIN t_Account a ON t.OurBranchID = a.OurBranchID AND t.AccountID = a.AccountID'         
 SET @SQL = @SQL + ' INNER JOIN t_Products p ON t.OurBranchID = p.OurBranchID AND t.ProductID = p.ProductID'         
 SET @SQL = @SQL + ' WHERE t.OurBranchID = @OurBranchID AND ISNULL(p.IsTermDeposit,''No'') = ''Yes'''         
    
 SET @Par = N'@OurBranchID nvarchar(30)'         
         
 IF @AccountID <> '' AND @ReceiptID <> ''         
 BEGIN         
          
  SET @SQL = @SQL + ' AND t.RecieptID = @ReceiptID AND t.AccountID = @AccountID'         
  SET @Par = N'@OurBranchID nvarchar(30), @ReceiptID nvarchar(30), @AccountID nvarchar(30)'         
          
  EXEC sp_executesql @SQL, @Par, @OurBranchID, @ReceiptID, @AccountID         
          
 END         
 ELSE         
 BEGIN         
          
  EXEC sp_executesql @SQL, @Par, @OurBranchID         
          
 END         
         
END