﻿
CREATE PROCEDURE [dbo].[sp_DeleteATMID]
(    
  @OurBranchID varchar(30),
  @ATMID varchar(30)
 )    
AS    
     
   DELETE FROM t_ATM_ATMAccount
   WHERE  OurBranchID = @OurBranchID AND ATMID = @ATMID