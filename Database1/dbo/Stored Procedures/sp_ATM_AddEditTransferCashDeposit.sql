﻿CREATE PROCEDURE [dbo].[sp_ATM_AddEditTransferCashDeposit]  
(  
 @OurBranchID       varchar(30),   
 @WithdrawlBranchID      varchar(30),   
 @ATMID       varchar(30),   
 @AccountID           varchar(30),  
 @Amount        money,  
 @USDAmount       money=0,  
 @OtherCurrencyAmount      money=0,  
 @USDRate           money=0,  
 @Supervision       char(1),   
 @RefNo        varchar(50),  
 @PHXDate        datetime,  
 @MerchantType      varchar(30),  
 @OurIMD       varchar(30),  
 @AckInstIDCode      varchar(30),  
 @Currency       varchar(30),  
 @NameLocation      varchar(100),  
 @MCC        varchar(30),  
 @PHXDateTime        varchar(14),  
  
 @NewRecord       bit=1  
)  
  
AS  
 DECLARE @BankShortName as varchar(30)  
  
 DECLARE @mScrollNo as int  
 DECLARE @mSerialNo as smallint  
 DECLARE @mAccountID as varchar(30)  
 DECLARE @mAccountName as varchar(100)  
 DECLARE @mAccountType as char  
 DECLARE @mProductID as varchar(30)  
 DECLARE @mCurrencyID as varchar(30)  
 DECLARE @mIsLocalCurrency as bit  
  SET @mIsLocalCurrency=1   
 DECLARE @mwDate as datetime  
 DECLARE @mTrxType as char  
 DECLARE @mGLID as varchar(30)  
  SET @mGLID = ''   
 DECLARE @mForeignAmount as money  
  SET @mForeignAmount=0   
 DECLARE @mExchangeRate as money  
  SET @mExchangeRate=1   
 DECLARE @mDescriptionID as varchar(30)  
  SET @mDescriptionID='987'   
 DECLARE @mDescription as varchar(255)  
 DECLARE @mDescriptionTax as varchar(255)  
 DECLARE @LocalCurrencyID as varchar(30)  
 DECLARE @LocalCurrency as varchar(30)  
  BEGIN   
   SELECT @LocalCurrencyID=LocalCurrencyID,@LocalCurrency=LocalCurrency FROM t_ATM_GlobalVariables  WHere OurBranchID = @OurBranchID
   SET @BankShortName = 'N/A'  
   SELECT @BankShortName = BankShortName From t_ATM_Banks  
   WHERE (BankIMD = @OurIMD)  
   
   IF @MerchantType <> '0000'   
    IF @Currency = @LocalCurrencyID  
     BEGIN  
      SET @mDescription=Right(@RefNo,6) + '-ATM(' + @ATMID + ') IBK-CSH DEPOSIT AT ' + @NameLocation   
      SET @mDescription = @mDescription + '-' + @PHXDateTime  
      SET @mDescriptionTax=Right(@RefNo,6) + '-ATM(' + @ATMID + ') IBK-CSH DEPOSIT-TAX AT ' + @NameLocation   
      SET @mDescriptionTax = @mDescriptionTax + '-' + @PHXDateTime  
     END  
    ELSE   
     IF @Currency = '840'  
      BEGIN  
       SET @mDescription=Right(@RefNo,6) + '-ATM(' + @ATMID + ') IBK-CSH DEPOSIT ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') AT ' + @NameLocation   
       SET @mDescription = @mDescription + '-' + @PHXDateTime  
       SET @mDescriptionTax=Right(@RefNo,6) + '-ATM(' + @ATMID + ') IBK-CSH DEPOSIT-TAX ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') AT ' + @NameLocation   
       SET @mDescriptionTax = @mDescriptionTax + '-' + @PHXDateTime  
      END  
     ELSE   
      BEGIN  
       SET @mDescription=Right(@RefNo,6) + '-ATM(' + @ATMID + ') IBK-CSH DEPOSIT (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) AT ' + @NameLocation   
       SET @mDescription = @mDescription + '-' + @PHXDateTime  
       SET @mDescriptionTax=Right(@RefNo,6) + '-ATM(' + @ATMID + ') IBK-CSH DEPOSIT-TAX (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) AT ' + @NameLocation   
       SET @mDescriptionTax = @mDescriptionTax + '-' + @PHXDateTime  
      END  
   ELSE   
    BEGIN  
     SET @mDescription=Right(@RefNo,6) + '-ATM(' + @ATMID + ') IB-CSH DEPOSIT AT BK.' + @BankShortName  + ' (BR.' + @WithdrawlBranchID + ')'   
     SET @mDescription = @mDescription + '-' + @PHXDateTime  
     SET @mDescriptionTax=Right(@RefNo,6) + '-ATM(' + @ATMID + ') IB-CSH DEPOSIT-TAX AT BK.' + @BankShortName  + ' (BR.' + @WithdrawlBranchID + ')'   
     SET @mDescriptionTax = @mDescriptionTax + '-' + @PHXDateTime  
    END  
  END   
 DECLARE @mDescriptionCharges as varchar(255)  
  BEGIN   
   IF @MerchantType <> '0000'   
    IF @Currency = @LocalCurrencyID  
     BEGIN  
      SET @mDescriptionCharges=Right(@RefNo,6) + '-ATM(' + @ATMID + ') IBK-CSH DEPOSIT-CHG AT ' + @NameLocation   
      SET @mDescriptionCharges = @mDescriptionCharges + '-' + @PHXDateTime  
     END  
    ELSE   
     IF @Currency = '840'  
      BEGIN  
       SET @mDescriptionCharges=Right(@RefNo,6) + '-ATM(' + @ATMID + ') IBK-CSH DEPOSIT-CHG ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') AT ' + @NameLocation   
       SET @mDescriptionCharges = @mDescriptionCharges + '-' + @PHXDateTime  
      END  
     ELSE   
      BEGIN  
       SET @mDescriptionCharges=Right(@RefNo,6) + '-ATM(' + @ATMID + ') IBK-CSH DEPOSIT-CHG (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) AT ' + @NameLocation   
       SET @mDescriptionCharges = @mDescriptionCharges + '-' + @PHXDateTime  
      END  
  END   
 DECLARE @mDescriptionH as varchar(255)  
  BEGIN   
   IF @MerchantType <> '0000'   
--    SET @mDescriptionH='ATM(' + @ATMID + ') IBK-CSH WDRL AT BK.' + @BankShortName   + ' (BR.' + @WithdrawlBranchID + ') ' + ' From : A/c # ' + @AccountID  
    IF @Currency = @LocalCurrencyID  
     BEGIN  
      SET @mDescriptionH=Right(@RefNo,6) + '-ATM(' + @ATMID + ') IBK-CSH DEPOSIT AT ' +  + @NameLocation + ' From : A/c # ' + @AccountID  
      SET @mDescriptionH = @mDescriptionH + '-' + @PHXDateTime  
     END  
    ELSE   
     IF @Currency = '840'  
      BEGIN  
       SET @mDescriptionH=Right(@RefNo,6) + '-ATM(' + @ATMID + ') IBK-CSH DEPOSIT ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') AT ' +  + @NameLocation + ' From : A/c # ' + @AccountID  
       SET @mDescriptionH = @mDescriptionH + '-' + @PHXDateTime  
      END  
     ELSE   
      BEGIN  
       SET @mDescriptionH=Right(@RefNo,6) + '-ATM(' + @ATMID + ') IBK-CSH DEPOSIT (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) AT ' +  + @NameLocation
 + ' From : A/c # ' + @AccountID  
       SET @mDescriptionH = @mDescriptionH + '-' + @PHXDateTime  
      END  
   ELSE   
    BEGIN  
     SET @mDescriptionH=Right(@RefNo,6) + '-ATM(' + @ATMID + ') IB-CSH DEPOSIT AT BK.' + @BankShortName   + ' (BR.' + @WithdrawlBranchID + ') ' + ' From : A/c # ' + @AccountID  
     SET @mDescriptionH = @mDescriptionH + '-' + @PHXDateTime  
    END  
  END   
 DECLARE @mDescriptionHCharges as varchar(255)  
 DECLARE @mDescriptionHChargesExcise as varchar(255)  
  BEGIN   
   IF @MerchantType <> '0000'   
--    SET @mDescriptionHCharges='ATM(' + @ATMID + ') IBK-CSH WDRL-CHG AT BK.' + @BankShortName   + ' (BR.' + @WithdrawlBranchID + ') ' + ' From : A/c # ' + @AccountID  
    IF @Currency = @LocalCurrencyID  
     BEGIN  
      SET @mDescriptionHCharges=Right(@RefNo,6) + '-ATM(' + @ATMID + ') IBK-CSH DEPOSIT-CHG AT ' + @NameLocation + ' From : A/c # ' + @AccountID  
      SET @mDescriptionHCharges = @mDescriptionHCharges + '-' + @PHXDateTime  
      SET @mDescriptionHChargesExcise=Right(@RefNo,6) + '-ATM(' + @ATMID + ') IBK-CSH DEPOSIT-FED AT ' + @NameLocation + ' From : A/c # ' + @AccountID  
      SET @mDescriptionHChargesExcise = @mDescriptionHChargesExcise + '-' + @PHXDateTime  
     END  
    ELSE   
     IF @Currency = '840'  
      BEGIN  
       SET @mDescriptionHCharges=Right(@RefNo,6) + '-ATM(' + @ATMID + ') IBK-CSH DEPOSIT-CHG ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') AT ' + @NameLocation + ' From : A/c # ' + @AccountID  
       SET @mDescriptionHCharges = @mDescriptionHCharges + '-' + @PHXDateTime  
       SET @mDescriptionHChargesExcise=Right(@RefNo,6) + '-ATM(' + @ATMID + ') IBK-CSH DEPOSIT-FED ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') AT ' + @NameLocation + ' From : A/c # ' + @AccountID  
       SET @mDescriptionHChargesExcise = @mDescriptionHChargesExcise + '-' + @PHXDateTime  
      END  
     ELSE   
      BEGIN  
       SET @mDescriptionHCharges=Right(@RefNo,6) + '-ATM(' + @ATMID + ') IBK-CSH DEPOSIT-CHG (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) AT ' + @NameLocation + ' From : A/c # ' + @AccountID  
       SET @mDescriptionHCharges = @mDescriptionHCharges + '-' + @PHXDateTime  
       SET @mDescriptionHChargesExcise=Right(@RefNo,6) + '-ATM(' + @ATMID + ') IBK-CSH DEPOSIT-FED (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) AT ' +
 @NameLocation + ' From : A/c # ' + @AccountID  
       SET @mDescriptionHChargesExcise = @mDescriptionHChargesExcise + '-' + @PHXDateTime  
      END  
  END   
 DECLARE @mRemarks as varchar(200)  
 DECLARE @mBankCode as varchar(30)  
  SET @mBankCode='093'   
 DECLARE @mOperatorID as varchar(30)  
  SET @mOperatorID = 'ATM-' + @ATMID  
 DECLARE @mSupervisorID as varchar(30)  
  SET @mSupervisorID = @OurIMD   
  
--Checking Variables  
 DECLARE @vCount1 as bit  
 DECLARE @vCount2 as bit  
 DECLARE @vCount3 as bit  
 DECLARE @vCount4 as bit  
 DECLARE @vAccountID as varchar(30)  
 DECLARE @vClearBalance as money  
 DECLARE @vClearedEffects as money  
 DECLARE @vFreezeAmount as money  
 DECLARE @vMinBalance as money  
 DECLARE @vIsClose as char  
 DECLARE @vAreCheckBooksAllowed as bit  
 DECLARE @vAllowCreditTransaction as bit  
 DECLARE @vTaxAmount as money  
  SET @vTaxAmount = 0   DECLARE @vAccountLimit as money  
 DECLARE @vCashAmt as money  
 DECLARE @vExpiryDate as datetime  
 DECLARE @vCheckLimit as bit  
  SET @vCheckLimit = 0  
 DECLARE @vODLimitAllow as bit  
  SET @vODLimitAllow = 0  
 DECLARE @vTaxAccountID as varchar(30)  
  SET @vTaxAccountID = ''  
 DECLARE @vTaxDeduction as money  
  SET @vTaxDeduction = 0  
 DECLARE @vTaxLimit as money  
  SET @vTaxLimit = 0  
  
 DECLARE @vBranchName as varchar(100)  
 DECLARE @Status as int   
  
 DECLARE @mLastEOD as datetime  
-- DECLARE @mWorkingDate as datetime  
 DECLARE @mCONFRETI as datetime  
 DECLARE @mWithdrawlBranchName as varchar(100)  
  
 DECLARE @WithDrawlCharges as money  
 DECLARE @WithDrawlChargesPercentage as money  
 DECLARE @IssuerAccountID as varchar(30)  
 DECLARE @IssuerAccountTitle as varchar(100)  
 DECLARE @IssuerChargesAccountID as varchar(30)  
 DECLARE @IssuerTitle as varchar(100)  
 DECLARE @mRemarksCharges as varchar(200)  
 DECLARE @mRemarksTax as varchar(200)  
  
 DECLARE @WRDLCHGExciseDuty as money --samad  
 DECLARE @ExciseDutyAccountID as varchar(30) --samad  
  
 SET NOCOUNT ON  
  
 SELECT @Status = -1  
---------------------  
  
 BEGIN  
  
   SELECT @mLastEOD=LastEOD,@mwDate=WorkingDate,@mCONFRETI=CONFRETI FROM t_Last WHERE (OurBranchID = @OurBranchID)  
   IF @mLastEOD = @mwDate or @mCONFRETI = '1/1/9999'  
    BEGIN  
        
     SELECT @Status=9  --INVALID_DATE  
     SELECT @Status  as Status  
     RETURN (1)   
    END  
  
   SELECT @mScrollNo=(ISNULL(MAX(ScrollNo),0)+1) FROM t_ATM_TransferTransaction WHERE (OurBranchID = @OurBranchID) and (wDate = @mwDate)   
  
------------------------  
------------------------  
--Debit Transaction     
  
  
   SELECT @vCount1=count(*) From t_Account  
   WHERE (OurBranchID = @OurBranchID) AND( AccountID=@AccountID)  
   IF @vCount1=0  
    BEGIN  
        
     SELECT @Status=2  --INVALID_ACCOUNT  
     SELECT @Status  as Status  
     RETURN (1)   
    END  
  
   SET @WithDrawlCharges = 0  
   SET @WithDrawlChargesPercentage = 0     SET @WRDLCHGExciseDuty = 0 --samad  
  
    BEGIN  
     IF @Currency = @LocalCurrencyID   
      BEGIN  
       SELECT @IssuerAccountID=IssuerAccountID,@IssuerAccountTitle=IssuerAccountTitle,  
         @IssuerChargesAccountID=IssuerChargesAccountID,@IssuerTitle=IssuerTitle  
       From t_ATM_SwitchCharges  
       WHERE (MerchantType = @MerchantType)  
       IF @WithDrawlCharges < ((@WithDrawlChargesPercentage/100)*@Amount)  
        BEGIN  
         SET @WithDrawlCharges = ((@WithDrawlChargesPercentage/100)*@Amount)  
        END  
      END  
     ELSE  
      BEGIN  
       SELECT @IssuerAccountID=IssuerAccountID,@IssuerAccountTitle=IssuerAccountTitle,  
         @IssuerChargesAccountID=IssuerChargesAccountID,@IssuerTitle=IssuerTitle  
       From t_ATM_SwitchCharges  
       WHERE (MerchantType = @MerchantType)  
       IF @WithDrawlCharges < ((@WithDrawlChargesPercentage/100)*@Amount)  
        BEGIN  
         SET @WithDrawlCharges = ((@WithDrawlChargesPercentage/100)*@Amount)  
        END  
      END  
    END  
  
  
   SELECT @mAccountName=B.Name, @mProductID=B.ProductID, @mCurrencyID=P.CurrencyID,@mAccountType='C',@vClearBalance=B.ClearBalance,@vClearedEffects=B.ClearedEffects,@vFreezeAmount=B.FreezeAmount,@vMinBalance=A.MinBalance,@vIsClose=A.Status, @vAreCheckBooksAllowed=P.AreCheckBooksAllowed, @vAllowCreditTransaction=S.AllowCreditTransaction, @vAccountLimit=B.Limit, @vCashAmt=B.CashAmt  
   FROM t_AccountBalance B INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND  B.ProductID = A.ProductID INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND  A.ProductID = P.ProductID LEFT OUTER JOIN t_AccountSpecialCondition S ON B.OurBranchID = S.OurBranchID AND B.AccountID = S.AccountID  
   WHERE  (A.OurBranchID = @OurBranchID) AND (A.AccountID = @AccountID)  
  
   IF @vIsClose = 'C' or @vIsClose = 'T' or @vIsClose = 'X' or @vAllowCreditTransaction = 1  
    BEGIN  
        
     SELECT @Status=3  --INACTIVE/CLOSE_ACCOUNT  
     SELECT @Status  as Status  
     RETURN (1)   
    END  
  
   IF @mCurrencyID <> @LocalCurrency  
    BEGIN  
        
     SELECT @Status=3  --INVALID_CURRENCY  
     SELECT @Status  as Status  
     RETURN (1)   
    END  
  
   Set @mSerialNo = 1  
   Set @mTrxType = 'C'  
   SET @vBranchName = 'N/A'  
   SELECT @vBranchName=Heading2 FROM t_GlobalVariables WHERE (OurBranchID = @OurBranchID)  
  
   SELECT @vCount4=count(*) From t_ATM_Branches  
   WHERE BranchID = @WithdrawlBranchID  
  
   IF @MerchantType <> '0000'   
    BEGIN  
     IF @Currency = @LocalCurrencyID  
      BEGIN  
       Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Transaction From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
       Set @mRemarksTax='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Tax Transaction From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
       Set @mRemarksCharges='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Charges Transaction From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
      END  
     ELSE   
      IF @Currency = '840'  
       BEGIN  
        Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
        Set @mRemarksTax='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Tax Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + 
upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
        Set @mRemarksCharges='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Charges Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Titl
e : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
  
       END  
      ELSE   
       BEGIN  
        Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) From : '
 + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
        Set @mRemarksTax='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Tax Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) F
rom : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
        Set @mRemarksCharges='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Charges Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate)
 + ')) From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
       END  
    END  
   ELSE  
    IF @vCount4 > 0   
     BEGIN  
      SET  @mWithdrawlBranchName = 'N/A'  
      SELECT @mWithdrawlBranchName=BranchName FROM t_ATM_Branches  
      WHERE BranchID = @WithdrawlBranchID  
      Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Branch Cash Withdrawl Transaction From : BK.' + @BankShortName +  ' (' + upper(@mWithdrawlBranchName) + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
      Set @mRemarksTax='Being Amount of ATM (' + @ATMID+ ') Inter Branch Cash Withdrawl Tax Transaction From : BK.' + @BankShortName +  ' (' + upper(@mWithdrawlBranchName) + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
     END  
    --IF @vCount4 = 0   
    ELSE  
     BEGIN  
      Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Branch Cash Withdrawl Transaction From : BK.' + @BankShortName +  ' (BR.' +  @WithdrawlBranchID + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
      Set @mRemarksTax='Being Amount of ATM (' + @ATMID+ ') Inter Branch Cash Withdrawl Tax Transaction From : BK.' + @BankShortName +  ' (BR.' +  @WithdrawlBranchID + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
     END  
  
  INSERT INTO t_ATM_TransferTransaction  
   (  
    ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,  
    valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,  
    BankCode,Remarks,OperatorID,SupervisorID,MCC,MerchantType  
   )    
  
  VALUES  
  
   (  
  
    @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@AccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,  
    @mwDate,@mwDate,@PHXDate,@mTrxType,@Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescription,@ATMID,  
    @mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MCC,@MerchantType  
   )  
  
  
    SELECT @Status=@@ERROR  
  
    IF @Status <> 0   
  
     BEGIN  
         
      RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR  
  
      RETURN (5000)  
     END  
  
--Credit Transaction     
  
  SELECT @vAccountID=AccountID, @mAccountName=Description,@mProductID='GL', @mCurrencyID=CurrencyID,@mAccountType='G' FROM t_GL   
  WHERE (OurBranchID = @OurBranchID) AND (AccountID =  @IssuerAccountID)  
       Set @mSerialNo = 2  
  Set @mTrxType = 'D'  
  
  INSERT INTO t_ATM_TransferTransaction  
   (  
    ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,  
    valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,  
    BankCode,Remarks,OperatorID,SupervisorID,MCC,MerchantType  
   )    
  
  VALUES  
   (  
    @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@vAccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,  
    @mwDate,@mwDate,@PHXDate,@mTrxType,@Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescriptionH,@ATMID,  
    @mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MCC,@MerchantType  
   )  
  
    SELECT @Status=@@ERROR  
  
    IF @Status <> 0   
     BEGIN  
         
      RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR  
      RETURN (5000)  
     END  
  
 BEGIN  
  SELECT @Status = 0 , @vClearBalance = ((IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0))+IsNull(@Amount,0)-isNull(@WithDrawlCharges,0))*100  
  SELECT @Status As Status , Round(@vClearBalance,0) As ClearBalance  
 END   
  
  RETURN 0  
  
  
     
 END