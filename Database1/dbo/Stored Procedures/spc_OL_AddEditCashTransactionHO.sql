﻿CREATE PROCEDURE [dbo].[spc_OL_AddEditCashTransactionHO]
( 
 @ScrollNo int, 
 @SerialNo int=1, 
 @HOBranchID varchar(30), 
 @sBranchAccountID varchar(30), 
 @sBranchAccountName varchar(100), 
 @sBranchProductID varchar(30), 
 @sBranchCurrencyID varchar(30), 
 @sBranchAccountType char(1), 
 @sBranchTrxType char(1), 
 @sBranchIsLocalCurrency Char(1), 
 @tBranchAccountID varchar(30), 
 @tBranchAccountName varchar(100), 
 @tBranchProductID varchar(30), 
 @tBranchCurrencyID varchar(30), 
 @tBranchAccountType char(1), 
 @tBranchTrxType Char(1), 
 @tBranchIsLocalCurrency Char(1), 
 @ValueDate datetime, 
 @wDate datetime, 
 @ChequeID varchar(30), 
 @ChequeDate DateTime, 
 @Amount money, 
 @ForeignAmount Money, 
 @ExchangeRate decimal(18,6), 
 @DescriptionID varchar(30), 
 @Description varchar(255), 
 @OperatorID varchar(30), 
 @SupervisorID varchar(30), 
 @SourceBranch Varchar(30), 
 @TargetBranch varchar(30), 
 @Remarks varchar(1000), 
 @RefNo varchar(30),      
 @AddData text=NULL      
)
 
AS 
  
 DECLARE @IsFreezed as Bit  
 DECLARE @AllowCrDr as Bit  
 DECLARE @AccountClass as char(1)
 DECLARE @IsPosting as Bit  
 
 DECLARE @BranchID as Varchar(4) 
 DECLARE @BranchStatus as Varchar(1) 
 DECLARE @Status as char(1)
 DECLARE @twDate as datetime 
 DECLARE @tLastEOD as datetime 
 DECLARE @tLastBOD as datetime 
 DECLARE @LocalCurrency as varchar(30)
 DECLARE @mDate varchar(50) 
 
 SET NOCOUNT ON 
 
 SELECT @Status = '' 
 
 BEGIN 
   
   select @LocalCurrency = LocalCurrency from t_GlobalVariables WHERE OurBranchID=@HOBranchID    
   
   SELECT @twDate = WorkingDate, @tLastEOD =Convert(Varchar(10), LastEOD, 101) , 
   @tLastBOD =Convert(VArchar(10), LastBOD,101) FROM t_Last WHERE OurBranchID=@HOBranchID    
       
   IF @twDate = @wDate 
   BEGIN 
     IF @tLastEOD = @tLastBOD 
     BEGIN 
       SELECT 'Error' AS RetStatus, 'EODDone' As RetMessage
       RETURN(0)
     END 
   END 
   ELSE 
   BEGIN 
     SELECT 'Error' AS RetStatus, 'WorkingDate' As RetMessage
     RETURN(0)
   END 
   
   SELECT @mDate = Convert(varchar(10),@wDate ,101) + ' ' + convert(varchar(20),GetDate(),114) 
   SET @Description = @Description + ' --- ' + cast(GETDATE() as varchar(50)) 
   
   if @sBranchProductID = 'GL'
   begin
     
     IF (SELECT Count(AccountID) From t_GL WHERE OurBranchID=@HOBranchID AND AccountID=@sBranchAccountID) > 0
     begin
       
       SELECT @AccountClass = AccountClass, @IsPosting = IsPosting From t_GL
       WHERE OurBranchID=@HOBranchID AND AccountID=@sBranchAccountID
       
       IF UPPER(@AccountClass) <> 'P'
       BEGIN
 SELECT 'Error' AS RetStatus, 'NotAPostingAccount' As RetMessage
 RETURN(0)
       END
       
       IF @IsPosting = 0
       BEGIN
 SELECT 'Error' AS RetStatus, 'GLPostingNotAllowed' As RetMessage
 RETURN(0)
       END
       
       IF (SELECT Count(*) From t_Products WHERE OurBranchID = @HOBranchID AND 
       (GLControl=@sBranchAccountID OR GLDebitBalance = @sBranchAccountID)) > 0
       BEGIN
 SELECT 'Error' AS RetStatus, 'GLPostingNotAllowedControlAccount' As RetMessage
 RETURN(0)
       END
       
     end
     ELSE
     BEGIN
       SELECT 'Error' AS RetStatus, 'AccountNotFound' As RetMessage
       RETURN(0)
    END
     
   end
   else
   begin
     
     IF (SELECT Count(AccountID) From t_Account WHERE OurBranchID=@HOBranchID AND AccountID=@sBranchAccountID) > 0
     BEGIN
       
  SELECT @sBranchProductID = ProductID, @Status = ISNULL(Status,''), @AllowCrDr = Case @sBranchTrxType
       WHEN 'C' THEN AllowCreditTransaction  
       WHEN 'D' THEN AllowDebitTransaction  
     END
       FROM t_Account
       WHERE OurBranchID=@HOBranchID AND AccountID=@sBranchAccountID
       
       IF @Status = 'C'
       BEGIN
 SELECT 'Error' AS RetStatus, 'AccountClosed' As RetMessage
 RETURN(0)
       END
       
       IF @Status = 'D'
       BEGIN
 SELECT 'Error' AS RetStatus, 'AccountBlocked' As RetMessage
 RETURN(0)
       END
       
       IF @Status = 'T'
       BEGIN
 SELECT 'Error' AS RetStatus, 'AccountDormant' As RetMessage
 RETURN(0)
       END
       
       IF @Status = 'X'
       BEGIN
 SELECT 'Error' AS RetStatus, 'AccountDeceased' As RetMessage
 RETURN(0)
       END
       
       IF @AllowCrDr = 1
       BEGIN  
 SELECT 'Error' AS RetStatus, 'SPTrxNotAllowed' As RetMessage
 RETURN(0)
       END
       
       SELECT @IsFreezed=IsFreezed, @AllowCrDr = Case @sBranchTrxType
       WHEN 'C' THEN AllowCredit
       WHEN 'D' THEN AllowDebit
     END
       FROM t_Products
       WHERE OurBranchID = @HOBranchID AND ProductID = @sBranchProductID
       
       IF @IsFreezed = 1
       BEGIN
 SELECT 'Error' AS RetStatus, 'ProductFreezed' As RetMessage
 RETURN(0)
       END
       
       IF @AllowCrDr = 0
       BEGIN
 SELECT 'Error' AS RetStatus, 'ProductTrxNotAllowed' As RetMessage
 RETURN(0)
       END
       
     END
     ELSE
     BEGIN
       SELECT 'Error' AS RetStatus, 'AccountNotFound' As RetMessage
       RETURN(0)
     END
    
   end
   
   if @tBranchProductID = 'GL'
   begin
     
     IF (SELECT Count(AccountID) From t_GL WHERE OurBranchID=@HOBranchID AND AccountID=@tBranchAccountID) > 0
     begin
       
       SELECT @AccountClass = AccountClass, @IsPosting = IsPosting From t_GL
       WHERE OurBranchID=@HOBranchID AND AccountID=@tBranchAccountID
       
       IF UPPER(@AccountClass) <> 'P'
       BEGIN
 SELECT 'Error' AS RetStatus, 'NotAPostingAccount' As RetMessage
 RETURN(0)
       END
       
       IF @IsPosting = 0
       BEGIN
 SELECT 'Error' AS RetStatus, 'GLPostingNotAllowed' As RetMessage
 RETURN(0)
       END
       
       IF (SELECT Count(*) From t_Products WHERE OurBranchID = @HOBranchID AND 
       (GLControl=@tBranchAccountID OR GLDebitBalance = @tBranchAccountID)) > 0
       BEGIN
 SELECT 'Error' AS RetStatus, 'GLPostingNotAllowedControlAccount' As RetMessage
 RETURN(0)
       END
       
     end
     ELSE
     BEGIN
       SELECT 'Error' AS RetStatus, 'AccountNotFound' As RetMessage
       RETURN(0)
     END
     
   end
   else
   begin
     
     IF (SELECT Count(AccountID) From t_Account WHERE OurBranchID=@HOBranchID AND AccountID=@tBranchAccountID) > 0
     BEGIN
       
       SELECT @tBranchProductID = ProductID, @Status = ISNULL(Status,''), @AllowCrDr = Case @tBranchTrxType
       WHEN 'C' THEN AllowCreditTransaction  
       WHEN 'D' THEN AllowDebitTransaction  
     END
       FROM t_Account
       WHERE OurBranchID=@HOBranchID AND AccountID=@tBranchAccountID
       
       IF @Status = 'C'
       BEGIN
 SELECT 'Error' AS RetStatus, 'AccountClosed' As RetMessage
 RETURN(0)
       END
       
       IF @Status = 'D'
       BEGIN
 SELECT 'Error' AS RetStatus, 'AccountBlocked' As RetMessage
 RETURN(0)
       END
       
       IF @Status = 'T'
       BEGIN
 SELECT 'Error' AS RetStatus, 'AccountDormant' As RetMessage
 RETURN(0)
       END
       
       IF @Status = 'X'
       BEGIN
 SELECT 'Error' AS RetStatus, 'AccountDeceased' As RetMessage
 RETURN(0)
      END
       
       IF @AllowCrDr = 1
       BEGIN  
 SELECT 'Error' AS RetStatus, 'SPTrxNotAllowed' As RetMessage
 RETURN(0)
       END
       
       SELECT @IsFreezed=IsFreezed, @AllowCrDr = Case @tBranchTrxType
       WHEN 'C' THEN AllowCredit
       WHEN 'D' THEN AllowDebit
     END
       FROM t_Products
       WHERE OurBranchID = @HOBranchID AND ProductID = @tBranchProductID
       
       IF @IsFreezed = 1
       BEGIN
 SELECT 'Error' AS RetStatus, 'ProductFreezed' As RetMessage
 RETURN(0)
       END
       
       IF @AllowCrDr = 0
       BEGIN
 SELECT 'Error' AS RetStatus, 'ProductTrxNotAllowed' As RetMessage
 RETURN(0)
       END
       
     END
     ELSE
     BEGIN
       SELECT 'Error' AS RetStatus, 'AccountNotFound' As RetMessage
       RETURN(0)
     END
    
   end
   
   INSERT INTO t_OnLineCashTransaction 
   ( 
    ScrollNo,SerialNo,OurBranchID,AccountID,AccountName,ProductID,CurrencyID,AccountType,ValueDate,wDate,TrxType, 
    ChequeID,ChequeDate, Amount, ForeignAmount,ExchangeRate, DescriptionID,Description,Supervision, 
    OperatorID,SupervisorID,IsLocalCurrency,SourceBranch, TargetBranch,Remarks,RefNo,AdditionalData      
   ) 
   VALUES 
   ( 
    @ScrollNo,@SerialNo,@HOBranchID,@sBranchAccountID,@sBranchAccountName,@sBranchProductID, 
    @sBranchCurrencyID,@sBranchAccountType,@ValueDate,@mDate,@sBranchTrxType,@ChequeID,@ChequeDate, 
    @Amount, @ForeignAmount,@ExchangeRate, @DescriptionID,@Description,'C', @OperatorID,@SupervisorID, 
    @sBranchIsLocalCurrency,@SourceBranch, @TargetBranch,@Remarks,@RefNo,@AddData      
   ) 
   
   SELECT @Status=@@ERROR  
   
   IF @Status <> 0   
   BEGIN  
     SELECT 'Error' AS RetStatus, 'ChkStatus' As RetMessage, @Status AS Status
     RETURN(0)
   END    
     
   declare @IsCredit int  
   declare @VoucherID int  
   declare @TransactionMethod char(1)  
   declare @GLControl varchar(30)  
   declare @DescID varchar(30)  
   declare @Desc varchar(255)  
     
   select @VoucherID = ISNULL(MAX(VoucherID),0) + 1 from t_GLTransactions where OurBranchID = @HOBranchID  
     
   if @sBranchTrxType = 'C'  
     set @IsCredit = 1  
   else  
     set @IsCredit = 0  
     
   if @sBranchIsLocalCurrency = 1    
   begin    
     set @ForeignAmount = 0    
     set @ExchangeRate = 1    
     set @TransactionMethod = 'L'    
   end    
   else    
     set @TransactionMethod = 'A'  
     
   if @sBranchAccountType = 'C'  
   begin  

      insert into t_Transactions (OurBranchID, ScrollNo, SerialNo, RefNo, wDate, AccountType, DocumentType, AccountID, AccountName,  
      ProductID, CurrencyID, ValueDate, TrxType, ChequeID, ChequeDate, Amount, ForeignAmount, ExchangeRate, ProfitLoss, MeanRate,  
      DescriptionID, [Description], BankCode, BranchCode, TrxPrinted, [Status], IsLocalCurrency, OperatorID, SupervisorID,  
      AdditionalData, Remarks, IsMainTrx, DocType, GLVoucherID, SourceBranch, TargetBranch)  

      VALUES (@HOBranchID,@ScrollNo,@SerialNo,@Refno,@mDate,@sBranchAccountType,'OC',@sBranchAccountID,@sBranchAccountName,  
      @sBranchProductID,@sBranchCurrencyID, @ValueDate,@sBranchTrxType,@ChequeID,@ChequeDate, @Amount, @ForeignAmount,@ExchangeRate,  
      0,@ExchangeRate,@DescriptionID,@Description,'','', 0, 'C', @sBranchIsLocalCurrency, @OperatorID, @SupervisorID,  
      @AddData, @Remarks,'1','OC', @VoucherID,@SourceBranch, @TargetBranch)  

      select @GLControl = ISNULL(GLControl, '') from t_Products where OurBranchID = @HOBranchID and ProductID = @sBranchProductID  

      if @sBranchTrxType = 'C'  
      begin  
       set @DescID = 'OL0'  
       set @Desc = 'Account ID: ' + @sBranchAccountID +', Account Name: ' + @sBranchAccountName +' , Module: Online-Cash Credit Entry...'  
      end  
      else  
      begin  
       set @DescID = 'OL1'  
       set @Desc = 'Account ID: ' + @sBranchAccountID +', Account Name: ' + @sBranchAccountName +' , Module: Online-Cash Debit Entry...'  
      end  

      insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],    
      CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,    
      Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType, SourceBranch, TargetBranch)    

      values (@HOBranchID, @GLControl, @VoucherID, '1', @mDate, @ValueDate, @DescID, @Desc,    
      @sBranchCurrencyID, @Amount, @ForeignAmount, @ExchangeRate, @IsCredit, 'OnLineCash', @TransactionMethod,  
      @OperatorID, @SupervisorID, '', '0', '0', '', '0', 'OC', @SourceBranch, @TargetBranch)    

   end  
   else  
   begin  

      insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],    
      CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,    
      Indicator, ScrollNo, SerialNo, AdditionalData, Remarks, IsMainTrx, DocType, SourceBranch, TargetBranch)    

      values (@HOBranchID, @sBranchAccountID, @VoucherID, '1', @mDate, @ValueDate, @DescriptionID, @Description,    
      @sBranchCurrencyID, @Amount, @ForeignAmount, @ExchangeRate, @IsCredit, 'OnLineCash', @TransactionMethod,  
      @OperatorID, @SupervisorID,'', @ScrollNo, @SerialNo, @AddData, @Remarks, '1', 'OC',@SourceBranch, @TargetBranch)  

   end  
     
   set @SerialNo = @SerialNo+1  
   set @VoucherID = @VoucherID + 1  
     
   INSERT INTO t_OnLineCashTransaction 
   ( 
    ScrollNo,SerialNo,OurBranchID,AccountID,AccountName,ProductID,CurrencyID,AccountType,ValueDate,wDate,TrxType, 
    ChequeID,ChequeDate, Amount, ForeignAmount,ExchangeRate, DescriptionID,Description,Supervision, 
    OperatorID,SupervisorID,IsLocalCurrency,SourceBranch, TargetBranch,Remarks,RefNo,AdditionalData      
   ) 
   VALUES 
   ( 
    @ScrollNo,@SerialNo,@HOBranchID,@tBranchAccountID,@tBranchAccountName,@tBranchProductID, 
    @tBranchCurrencyID,@tBranchAccountType,@ValueDate,@mDate,@tBranchTrxType,@ChequeID,@ChequeDate, 
    @Amount, @ForeignAmount,@ExchangeRate, @DescriptionID,@Description,'C', @OperatorID,@SupervisorID, 
    @tBranchIsLocalCurrency ,@SourceBranch, @TargetBranch,@Remarks,@RefNo,@AddData      
   ) 
   
   SELECT @Status=@@ERROR  
   
   IF @Status <> 0   
   BEGIN  
     SELECT 'Error' AS RetStatus, 'ChkStatus' As RetMessage, @Status AS Status
     RETURN(0)
   END  
     
   if @tBranchTrxType = 'C'  
     set @IsCredit = 1  
   else  
     set @IsCredit = 0  
     
   if @tBranchIsLocalCurrency = 1    
   begin    
     set @ForeignAmount = 0    
     set @ExchangeRate = 1    
     set @TransactionMethod = 'L'    
   end    
   else    
     set @TransactionMethod = 'A'  
     
   if @tBranchAccountType = 'C'  
   begin  

      insert into t_Transactions (OurBranchID, ScrollNo, SerialNo, RefNo, wDate, AccountType, DocumentType, AccountID, AccountName,  
      ProductID, CurrencyID, ValueDate, TrxType, ChequeID, ChequeDate, Amount, ForeignAmount, ExchangeRate, ProfitLoss, MeanRate,  
      DescriptionID, [Description], BankCode, BranchCode, TrxPrinted, [Status], IsLocalCurrency, OperatorID, SupervisorID,  
      AdditionalData, Remarks, IsMainTrx, DocType, GLVoucherID, SourceBranch, TargetBranch)  

      VALUES (@HOBranchID,@ScrollNo,@SerialNo,@Refno,@mDate,@tBranchAccountType,'OC',@tBranchAccountID,@tBranchAccountName,  
      @tBranchProductID,@tBranchCurrencyID, @ValueDate,@tBranchTrxType,@ChequeID,@ChequeDate, @Amount, @ForeignAmount,@ExchangeRate,  
      0,@ExchangeRate,@DescriptionID,@Description,'','', 0, 'C', @tBranchIsLocalCurrency, @OperatorID, @SupervisorID,  
      @AddData, @Remarks,'1','OC', @VoucherID, @SourceBranch, @TargetBranch)  

      select @GLControl = ISNULL(GLControl, '') from t_Products where OurBranchID = @HOBranchID and ProductID = @tBranchProductID  

      if @tBranchTrxType = 'C'  
      begin  
       set @DescID = 'OL0'  
       set @Desc = 'Account ID: ' + @tBranchAccountID +', Account Name: ' + @tBranchAccountName +' , Module: Online-Cash Credit Entry...'  
      end  
      else  
      begin  
       set @DescID = 'OL1'  
       set @Desc = 'Account ID: ' + @tBranchAccountID +', Account Name: ' + @tBranchAccountName +' , Module: Online-Cash Debit Entry...'  
      end  

      insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],    
      CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,    
      Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType, SourceBranch, TargetBranch)    

      values (@HOBranchID, @GLControl, @VoucherID, '1', @mDate, @ValueDate, @DescID, @Desc,    
      @tBranchCurrencyID, @Amount, @ForeignAmount, @ExchangeRate, @IsCredit, 'OnLineCash', @TransactionMethod,  
      @OperatorID, @SupervisorID, '', '0', '0', '', '0', 'OC', @SourceBranch, @TargetBranch)    

   end  
   else  
   begin  

      insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],    
      CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,    
      Indicator, ScrollNo, SerialNo, AdditionalData, Remarks, IsMainTrx, DocType, SourceBranch, TargetBranch)    

      values (@HOBranchID, @tBranchAccountID, @VoucherID, '1', @mDate, @ValueDate, @DescriptionID, @Description,    
      @tBranchCurrencyID, @Amount, @ForeignAmount, @ExchangeRate, @IsCredit, 'OnLineCash', @TransactionMethod,  
      @OperatorID, @SupervisorID,'', @ScrollNo, @SerialNo, @AddData, @Remarks, '1', 'OC',@SourceBranch, @TargetBranch)  

   end  
     
   SELECT 'Ok' AS RetStatus, 'TrxPosted' As RetMessage
   RETURN(0)
   
 END