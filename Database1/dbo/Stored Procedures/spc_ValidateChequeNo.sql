﻿CREATE Procedure [dbo].[spc_ValidateChequeNo]      
 (      
  @OurBranchID varchar(30),      
  @AccountID varchar(30),      
  @ChequeStart varchar(11),      
  @ChequeEnd varchar(11)    
 )      
As      
  
SET NOCOUNT ON  
  
 DECLARE @ChequeSeriesCount int      
 DECLARE @ChequeSeriesPresent bit, @ChequeSeriesAlreadyStopped bit, @AllChequeAreStopped bit, @AreAnyChequeCleared bit      
 DECLARE @AuthorizationStatus int      
 DECLARE @AuthorizationStopPaymentCount int      
      
 SELECT @AuthorizationStatus = 0, @AuthorizationStopPaymentCount = 0      
      
--First check whether the above mentioned cheques were isssued       
 SELECT @ChequeSeriesCount = COUNT(*)      
 FROM t_Cheque      
 WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID    
             AND (      
  CAST(@ChequeStart AS money) BETWEEN (CAST (ChequeStart as money))       
  AND (CAST (ChequeEnd as money))      
      )      
             AND (      
  CAST(@ChequeEnd AS money) BETWEEN (CAST(ChequeStart as money))      
  AND (CAST (ChequeEnd as money))      
      )      
      
 IF @ChequeSeriesCount > 0      
  BEGIN --Cheques were issued BEGIN      
   SELECT @ChequeSeriesPresent=1      
      
--Check wether any of the Cheque Is already Cleared      
   SELECT @ChequeSeriesCount = COUNT(*)      
   FROM t_ChequePaid      
   WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID       
               AND (       
    CAST(ChequeID AS money) BETWEEN (CAST(@ChequeStart AS money))       
    AND (CAST(@ChequeEnd AS money))      
         )      
         
   IF @ChequeSeriesCount = 0       
    BEGIN --No Cheques were Cleared BEGIN      
     SELECT @AreAnyChequeCleared = 0      
      
--Check wether the ChequeSeries Is already been Stopped      
     SELECT @ChequeSeriesCount=COUNT(*)      
     FROM t_StopPayments      
     WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID       
                 AND (      
      CAST(ChequeID AS money) BETWEEN (CAST(@ChequeStart AS money))       
      AND (CAST(@ChequeEnd AS money))      
           )      
        
     IF @ChequeSeriesCount = 0       
      BEGIN --ChequeSeries was NOT stopped BEGIN      
      
       SELECT @ChequeSeriesAlreadyStopped = 0      
       SELECT @AllChequeAreStopped = 0      
      
      END --ChequeSeries was NOT stopped BEGIN      
      
      
     ELSE      
      BEGIN         
--Check wether all Cheques were stopped            
       SELECT @ChequeSeriesAlreadyStopped = 1      
       IF (@ChequeSeriesCount = (CAST(@ChequeEnd AS money) - CAST(@ChequeStart AS money) +1))      
        SELECT @AllChequeAreStopped = 1      
       ELSE      
        SELECT @AllChequeAreStopped = 0      
      
      END      
      
    END --No Cheques were Cleared END      
   ELSE           
    BEGIN --Cheques were Cleared BEGIN      
     SELECT @AreAnyChequeCleared = 1      
    END  --Cheques were Cleared END      
      
  END --Cheques were issued END      
      
 ELSE      
--Display the result if the Cheque are not issued.       
  BEGIN --Cheques were not issued BEGIN      
   SELECT @ChequeSeriesPresent = 0      
   SELECT @ChequeSeriesAlreadyStopped = 0      
   SELECT @AreAnyChequeCleared = 0      
   SELECT @AllChequeAreStopped = 0       
  END --Cheques were not issued END      
      
 IF @ChequeSeriesPresent = 0      
  BEGIN      
   RETURN(2)      
  END      
      
 IF @AreAnyChequeCleared = 1      
  BEGIN      
   RETURN(3)      
  END      
      
 IF @ChequeSeriesAlreadyStopped = 1      
  BEGIN      
   RETURN(4)      
  END      
      
  SELECT @AuthorizationStopPaymentCount = COUNT(*)      
 FROM t_StopPayment    
 WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND AuthStatus <> ''    
             AND (      
  CAST(@ChequeStart AS money) BETWEEN (CAST (ChequeStart as money))       
  AND (CAST (ChequeEnd as money))      
      )      
             AND (      
  CAST(@ChequeEnd AS money) BETWEEN (CAST(ChequeStart as money))      
  AND (CAST (ChequeEnd as money))      
      )    
      
   IF @AuthorizationStopPaymentCount > 0       
    RETURN (5)      
  
      
 RETURN 0