﻿  
CREATE procedure [dbo].[spc_GetCOD] (  
 @OurBranchID varchar(30),  
 @ProductID varchar(30)=''  
)  
AS    
BEGIN  
  
 SET NOCOUNT ON  
  
 if @ProductID <> ''    
    
  select * from vc_TOD   
  where productid=@productid and OurBranchID=@OurBranchID   
  and accountid in (  
 select accountid from t_Adv_Account_LimitMaintain   
 where isnull(isBlocked,'0')=0 and isnull(iscancelled,'0')=0 and OurBranchID=@OurBranchID  
  )  
    
 else    
   
  select * from vc_TOD   
  where productid='CAPKR' and OurBranchID=@OurBranchID   
  and accountid in (  
 select accountid from t_Adv_Account_LimitMaintain   
 where isnull(isBlocked,'0')=0 and isnull(iscancelled,'0')=0 and OurBranchID=@OurBranchID  
  )     
END