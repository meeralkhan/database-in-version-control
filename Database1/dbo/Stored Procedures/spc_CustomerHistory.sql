﻿create   PROCEDURE  [dbo].[spc_CustomerHistory]   (            
@ClientID nvarchar(15)            
)            
AS              
SET NOCOUNT ON              
Declare @SerialNO int = 1;            
select @SerialNO = ISNULL(max(SerialID),0)+1 from t_Customer_History where ClientID = @ClientID            
INSERT INTO t_Customer_History (SerialID, wDate, CreateBy, CreateTime, CreateTerminal, UpdateBy, UpdateTime, UpdateTerminal, AuthStatus, SuperviseBy, SuperviseTime, SuperviseTerminal, OurBranchID, ClientID,             
Name, CategoryId, Address, Phone1, Phone2, MobileNo, Fax, Email, NIC, DOB, NicExpirydate, NTN, CIBCode, RelationShipCode, frmName, VerifyBy, VerifyTime, VerifyTerminal, VerifyStatus,            
CID, NameOfFather, NameOfMother, Gender, MaritalStatus, POBCountry, POBCity, OldClientID, FZType, DOI, TradeLicenseNo, CompanyType, GroupID, ParentClientID, GroupCode, CloseDate,             
RiskProfile, FirstName, MiddleName, LastName, FATCARelevantInd, Nationality, IDIssueCountry, PassportNumber, PassportExpiry, PassportIssueCountry, PassportGender, OnboardingCountry,             
Profession, EmployerName, IncomeBracket, KYCReviewDate, DualCitizenship, CitizenCountry2, PEP, BlackList, CRSRelevantInd, AreYouAuSCitizenInd, AreYouAUSTaxResidentInd,            
WereYouBornInTheUSInd, CountryUSOrUAEInd, TinInd, TaxCountry1Ind, TaxCountry2Ind, TaxCountry3Ind, TaxCountry4Ind, TaxCountry5Ind, EntityNameCorp, YearofestablishmentCorp,             
RegisteredAddressCorp, ContactName1Corp, ContactID1Corp, Address1Corp, PhoneNumberCorp, AlternatePhonenumberCorp, EmailIDCorp, ContactName2Corp, ContactID2Corp, Address2Corp,             
PhoneNumber2Corp, AlternatePhonenumber2Corp, EmailID2Corp, BusinessActivity, IndustryCorp, PresenceinCISCountryCorp, DealingwithCISCountryCorp, TradeLicenseNumberCorp,             
CountryofIncorporationCorp, TradeLicenseIssuedateCorp, TradeLicenseExpirydateCorp, TradeLicenseIssuanceAuthorityCorp, TRNNoCorp, PEPCorp, BlackListCorp, KYCReviewDateCorp,             
FATCARelevantCorp, CRSRelevantCorp, USAEntityCorp, FFICorp, GIINNoCorp, SponsorNameCorp, SponsorGIIN, StockExchangeCorp, TradingSymbolCorp, USAORUAECorp, TINAvailableCorp,             
TaxCountry1Corp, TaxCountry2Corp, TaxCountry3Corp, TaxCountry4Corp, TaxCountry5Corp, ReasonACorp, ReasonBCorp, FATCANoReason, ReasonBind, HNI, CountryCors, StateCors, CityCors,             
CountryCurr, StateCurr, CityCurr, AddressHomeCountry, CountryHome, StateHome, CityHome, ResidentNonResident, AddressCors, AddressCurr, Tinno1Ind, Tinno2Ind, Tinno3Ind, Tinno4Ind,             
Tinno5Ind, TinNo1Corp, TinNo2Corp, TinNo3Corp, TinNo4Corp, TinNo5Corp, EntityTypeCorp, IDTypeCorp, SICCODESCORP, FFICategoryCorp, NFFECorp, GIINNACorp, IDType2, CountryOfResidence,             
CustomerPromoCode, RefererPromoCode, MDMID, RegAddCountryCorp, RegAddStateCorp, RegAddCityCorp, Add1CountryCorp, Add1StateCorp, Add1CityCorp, Add2CountryCorp, Add2StateCorp, Add2CityCorp,            
CompanyWebsite, CompCashIntensive, PubLimCompCorp, VATRegistered, RManager, GroupCodeType, DeathDate, IsVIP, JurEmiratesID, JurTypeID, JurAuthorityID, POBox, JurOther, Status, LicenseType,            
CountryID, MOAMOU, SigningAuthority, POAResidenceCountry, POAAddress,            
POACountry, POAState, POACity, AuditedFinancials, Assets, Revenue, Profits, BusinessTurnOver,             
CompanyCapital, ExpectedDebitTurnOver, ExpectedMaxDebitTurnOver, ExpectedCreditTurnOver, ExpectedMaxCreditTurnOver, CompanyAddress, ExpectedCashVolume, ExpectedCashValue,             
ChequesVolume, ChequesValue, SalaryPaymentVolume, SalaryPaymentValue, IntraBankRemittancesVolume, IntraBankRemittancesValue, InternationalRemittancesVolume, InternationalRemittancesValue,            
DomesticRemittanceVolume, DomesticRemittanceValues, ExpectedCashDebitVolume, ExpectedCashDebitValue, ChequeDebitVolume, ChequeDebitValue, SalaryDVolume, SalaryDValue, IntraDVolume,            
IntraDValue, InternationalDVolume, InternationalDValue, DomesticDRemittanceValues, DomesticDRemittanceVolume, CreditTradeFinanceVolume, CreditTradeFinanceValue, TradeFinanceVolumeD,             
TradeFinanceValueD, NoOfStaff, CompanyActivites, BusinessNatureRelation, RelationalCompanyStructure, CompanyGeoGraphicCourage, UBOBackground, ProductAndServicesOffered,            
IsDisclosureActivity, DisclosureDetails, IsSanctionCountry, SanctionRemarks, IsOfficeInSactionCountry, SanctionCountries, BusinessNatureSanctionCountry, IsProductOrigninatedCountry,             
OriginatedProductCountry, OriginatedCountryRemarks, IsExportGoods, ExportGoodsCountry, ExportGoodsRemarks, isRecievedTransFromSanctionCountry, TransactionSanctionCountry,            
TransactionSanctionRemarks, CorporateStructureChart, VisitConducted, SanctionAssestment, PepAssessment, AdverseMedia, AccountOpeningPurpose          
,          
IsBankRelatedParty,ChamberCommerceNo,Nationality1,DualCitizen,DOB1,POB1,gender1,ResidenceCountry1,Nationality2,DualCitizen2,DOB2,POB2,gender2,ResidenceCountry2,          
POAIDType,POAIDNum,POAIDExpiryDate,SigningAuthority2,POAResidenceCountry2,POAAddress2,POACountry2,POAState2,POACity2,POAName2,POANationality2,POAPlaceOfBirth2,          
POAGender2,POADateofBirth2,POAIDType2,POAIDNum2,POAIDExpiryDate2,AuditCompany,AuditCompanyName,PayrollBank          
)            
select @SerialNO, ISNULL(UpdateTime,CreateTime), CreateBy, CreateTime, CreateTerminal, UpdateBy, UpdateTime, UpdateTerminal, AuthStatus, SuperviseBy, SuperviseTime, SuperviseTerminal, OurBranchID, ClientID,             
Name, CategoryId, Address, Phone1, Phone2, MobileNo, Fax, Email, NIC, DOB, NicExpirydate, NTN, CIBCode, RelationShipCode, frmName, VerifyBy, VerifyTime, VerifyTerminal, VerifyStatus,            
CID, NameOfFather, NameOfMother, Gender, MaritalStatus, POBCountry, POBCity, OldClientID, FZType, DOI, TradeLicenseNo, CompanyType, GroupID, ParentClientID, GroupCode, CloseDate,             
RiskProfile, FirstName, MiddleName, LastName, FATCARelevantInd, Nationality, IDIssueCountry, PassportNumber, PassportExpiry, PassportIssueCountry, PassportGender, OnboardingCountry,             
Profession, EmployerName, IncomeBracket, KYCReviewDate, DualCitizenship, CitizenCountry2, PEP, BlackList, CRSRelevantInd, AreYouAuSCitizenInd, AreYouAUSTaxResidentInd,            
WereYouBornInTheUSInd, CountryUSOrUAEInd, TinInd, TaxCountry1Ind, TaxCountry2Ind, TaxCountry3Ind, TaxCountry4Ind, TaxCountry5Ind, EntityNameCorp, YearofestablishmentCorp,             
RegisteredAddressCorp, ContactName1Corp, ContactID1Corp, Address1Corp, PhoneNumberCorp, AlternatePhonenumberCorp, EmailIDCorp, ContactName2Corp, ContactID2Corp, Address2Corp,             
PhoneNumber2Corp, AlternatePhonenumber2Corp, EmailID2Corp, BusinessActivity, IndustryCorp, PresenceinCISCountryCorp, DealingwithCISCountryCorp, TradeLicenseNumberCorp,             
CountryofIncorporationCorp, TradeLicenseIssuedateCorp, TradeLicenseExpirydateCorp, TradeLicenseIssuanceAuthorityCorp, TRNNoCorp, PEPCorp, BlackListCorp, KYCReviewDateCorp,             
FATCARelevantCorp, CRSRelevantCorp, USAEntityCorp, FFICorp, GIINNoCorp, SponsorNameCorp, SponsorGIIN, StockExchangeCorp, TradingSymbolCorp, USAORUAECorp, TINAvailableCorp,             
TaxCountry1Corp, TaxCountry2Corp, TaxCountry3Corp, TaxCountry4Corp, TaxCountry5Corp, ReasonACorp, ReasonBCorp, FATCANoReason, ReasonBind, HNI, CountryCors, StateCors, CityCors,             
CountryCurr, StateCurr, CityCurr, AddressHomeCountry, CountryHome, StateHome, CityHome, ResidentNonResident, AddressCors, AddressCurr, Tinno1Ind, Tinno2Ind, Tinno3Ind, Tinno4Ind,             
Tinno5Ind, TinNo1Corp, TinNo2Corp, TinNo3Corp, TinNo4Corp, TinNo5Corp, EntityTypeCorp, IDTypeCorp, SICCODESCORP, FFICategoryCorp, NFFECorp, GIINNACorp, IDType2, CountryOfResidence,             
CustomerPromoCode, RefererPromoCode, MDMID, RegAddCountryCorp, RegAddStateCorp, RegAddCityCorp, Add1CountryCorp, Add1StateCorp, Add1CityCorp, Add2CountryCorp, Add2StateCorp, Add2CityCorp,            
CompanyWebsite, CompCashIntensive, PubLimCompCorp, VATRegistered, RManager, GroupCodeType, DeathDate, IsVIP, JurEmiratesID, JurTypeID, JurAuthorityID, POBox, JurOther, Status, LicenseType,            
CountryID, MOAMOU, SigningAuthority, POAResidenceCountry, POAAddress,            
POACountry, POAState, POACity, AuditedFinancials, Assets, Revenue, Profits, BusinessTurnOver,             
CompanyCapital, ExpectedDebitTurnOver, ExpectedMaxDebitTurnOver, ExpectedCreditTurnOver, ExpectedMaxCreditTurnOver, CompanyAddress, ExpectedCashVolume, ExpectedCashValue,             
ChequesVolume, ChequesValue, SalaryPaymentVolume, SalaryPaymentValue, IntraBankRemittancesVolume, IntraBankRemittancesValue, InternationalRemittancesVolume, InternationalRemittancesValue,            
DomesticRemittanceVolume, DomesticRemittanceValues, ExpectedCashDebitVolume, ExpectedCashDebitValue, ChequeDebitVolume, ChequeDebitValue, SalaryDVolume, SalaryDValue, IntraDVolume,            
IntraDValue, InternationalDVolume, InternationalDValue, DomesticDRemittanceValues, DomesticDRemittanceVolume, CreditTradeFinanceVolume, CreditTradeFinanceValue, TradeFinanceVolumeD,             
TradeFinanceValueD, NoOfStaff, CompanyActivites, BusinessNatureRelation, RelationalCompanyStructure, CompanyGeoGraphicCourage, UBOBackground, ProductAndServicesOffered,            
IsDisclosureActivity, DisclosureDetails, IsSanctionCountry, SanctionRemarks, IsOfficeInSactionCountry, SanctionCountries, BusinessNatureSanctionCountry, IsProductOrigninatedCountry,             
OriginatedProductCountry, OriginatedCountryRemarks, IsExportGoods, ExportGoodsCountry, ExportGoodsRemarks, isRecievedTransFromSanctionCountry, TransactionSanctionCountry,            
TransactionSanctionRemarks, CorporateStructureChart, VisitConducted, SanctionAssestment, PepAssessment, AdverseMedia, AccountOpeningPurpose,          
IsBankRelatedParty,ChamberCommerceNo,Nationality1,DualCitizen,DOB1,POB1,gender1,ResidenceCountry1,Nationality2,DualCitizen2,DOB2,POB2,gender2,ResidenceCountry2,          
POAIDType,POAIDNum,POAIDExpiryDate,SigningAuthority2,POAResidenceCountry2,POAAddress2,POACountry2,POAState2,POACity2,POAName2,POANationality2,POAPlaceOfBirth2,          
POAGender2,POADateofBirth2,POAIDType2,POAIDNum2,POAIDExpiryDate2,AuditCompany,AuditCompanyName,PayrollBank          
from t_Customer
where ClientID=@ClientID