﻿CREATE PROCEDURE [dbo].[spc_OL_GetHOAccBalance]    
 (    
  @HOBranchID Varchar(30),  
  @HOAccountID Varchar(30),  
  @AccountType char(1) = 'G',  
  @IsLocalCurrency char(1) = 'Y'  
 )    
AS    
    
 DECLARE @crBal money    
 DECLARE @crFBal money    
 DECLARE @drBal money    
 DECLARE @drFBal money    
 DECLARE @Balance money     
 DECLARE @NetBalance money    
 DECLARE @NetFBalance money    
    
 DECLARE @crOrignate Integer    
 DECLARE @drOrignate Integer    
 DECLARE @crRespond Integer    
 DECLARE @drRespond Integer    
    
 BEGIN    
    
  SELECT @crBal=IsNull(Sum(Amount),0),@crFBal=IsNull(Sum(ForeignAmount),0),@crOrignate=Count(*)  
  FROM t_OnLineLocalCashTransaction WHERE OurBranchID = @HOBranchID AND AccountID = @HOAccountID AND TrxType='C'
  
  SELECT @drBal=IsNull(Sum(Amount),0),@drFBal=IsNull(Sum(ForeignAmount),0),@drOrignate=Count(*)  
  FROM t_OnLineLocalCashTransaction WHERE OurBranchID = @HOBranchID AND AccountID = @HOAccountID AND TrxType='D'
  
  SET @NetBalance  = @crBal  - @drBal  
  SET @NetFBalance = @crFBal - @drFBal  
    
  SELECT @crBal=IsNull(Sum(Amount),0),@crFBal=IsNull(Sum(ForeignAmount),0),@crRespond=Count(*)  
  FROM t_OnLineCashTransaction WHERE OurBranchID = @HOBranchID AND AccountID = @HOAccountID AND TrxType='C'  
    
  SELECT @drBal=IsNull(Sum(Amount),0),@drFBal=IsNull(Sum(ForeignAmount),0),@drRespond=Count(*)  
  FROM t_OnLineCashTransaction WHERE OurBranchID = @HOBranchID AND AccountID = @HOAccountID AND TrxType='D'    
    
  SET @NetBalance  = @NetBalance  + @crBal  - @drBal  
  SET @NetFBalance = @NetFBalance + @crFBal - @drFBal  
    
  --SELECT @crBal=IsNull(Sum(Amount),0),@crOrignate=@crOrignate+Count(*) FROM t_OL_LocalTransferTransaction    
  --WHERE TrxType='C' AND AccountID = @HOAccountID    
    
  --SELECT @drBal=IsNull(Sum(Amount),0),@drOrignate=@drOrignate+Count(*) FROM t_OL_LocalTransferTransaction    
  --WHERE TrxType='D'  AND AccountID = @HOAccountID    
    
  --SET @NetBalance =@NetBalance + @crBal - @drBal    
    
  if @AccountType = 'G'  
    if @IsLocalCurrency = 'Y'  
      SELECT @Balance =IsNull(Balance,0) FROM t_GL WHERE OurBranchID = @HOBranchID AND AccountID = @HOAccountID  
    else  
      SELECT @Balance =IsNull(ForeignBalance,0) FROM t_GL WHERE OurBranchID = @HOBranchID AND AccountID = @HOAccountID  
  else  
    SELECT @Balance =IsNull(ClearBalance,0) FROM t_AccountBalance WHERE OurBranchID = @HOBranchID AND AccountID = @HOAccountID  
    
  SET @NetBalance = @NetBalance + @Balance    
  SET @NetFBalance = @NetFBalance + @Balance    
    
  if @IsLocalCurrency = 'Y'  
    SET @NetFBalance = 0  
    
  SELECT IsNull(@NetBalance,0) As Balance,IsNull(@NetFBalance,0) As FBalance,  
  @crOrignate as crOrignate, @drOrignate as drOrignate,  
  @crRespond as crRespond, @drRespond as drRespond,  
  (@crOrignate + @drOrignate + @crRespond + @drRespond) as TotlTrans  
    
 END