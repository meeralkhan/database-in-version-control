﻿CREATE PROCEDURE [dbo].[spc_InwardVerify]    
 (            
 @OurBranchID  varchar(30)='',            
 @AccountID    varchar(30)='',            
 @TrxType      varchar(1)='D',            
 @Amount       money=0,            
 @AccountType  char(1)='G',            
 @Supervision  char(1)='',            
 @PostingLimit money=0,            
 @OperatorID   varchar(30)='',            
 @LocEq        money=0            
 )            
            
 AS            
           
 SET NOCOUNT ON          
           
 DECLARE @Status as varchar(5)            
 DECLARE @ProductID as VarChar(6)            
 DECLARE @ClearBalance as Money            
 DECLARE @Effects as Money            
 DECLARE @ShadowBalance as Money            
 DECLARE @Limit as Money            
            
 DECLARE @BlockedAmount as Money            
 DECLARE @MinAmount as Money            
 DECLARE @VpOD as Money            
            
 DECLARE @IsFreezed as Bit            
 DECLARE @CanGoCrDr as Bit            
 DECLARE @AllowCrDr as Bit            
 DECLARE @EveryTrxGoesInSupervision as Bit            
            
 DECLARE @ByPassSupervision as Bit            
 DECLARE @IsProductFreezed as Bit            
            
 SET @ByPassSupervision = 0            
 SET @EveryTrxGoesInSupervision = 0            
            
 SET @Status = ''            
            
 IF @Supervision='N'            
  SET @ByPassSupervision = 1            
            
 SET @Supervision=''            
------------------------------------------------------------------------------                    
 IF @AccountType ='C' AND @TrxType='D'            
 BEGIN            
/******************************* Checking A/c Status *************************************/            
    
  exec @Limit = fnc_GetLimit @OurBranchID, @AccountID  
  
  SELECT @Status = IsNull(a.Status,''), @ClearBalance = ab.ClearBalance, @Effects = ab.Effects,            
   @ShadowBalance = ab.ShadowBalance,            
   @BlockedAmount = CASE ISNULL(ab.IsFreezed,0) WHEN 1 THEN ISNULL(ab.FreezeAmount,0) WHEN 0 THEN 0 END,            
   @IsFreezed = ab.IsFreezed, @ProductID = a.ProductID            
  From t_Account a          
  INNER JOIN t_AccountBalance ab ON a.OurBranchID = ab.OurBranchID AND a.AccountID = ab.AccountID          
  WHERE a.OurBranchID = @OurBranchID AND a.AccountID = @AccountID          
            
--print @IsFreezed                            
--print @Status            
  IF IsNull(@Status,'')<>''            
  BEGIN            
   IF @Status = 'C'            
    RETURN(15)--A/c is Closed            
   ELSE IF @Status ='D'            
    RETURN(16) -- A/c Is blocked Long time            
   ELSE IF @Status ='T'            
    RETURN(17)   -- A/c Mark Dormant            
   ELSE IF @Status ='X'            
    RETURN(18)   -- A/c Mark Decessed            
   ELSE IF @Status ='I'            
    RETURN(24)   -- A/c Mark In-Active    
  END            
            
  SELECT @IsProductFreezed=IsFreezed,            
   @AllowCrDr = Case @TrxType    WHEN 'C' THEN AllowCredit            
   WHEN 'D' THEN AllowDebit      END ,            
   @CanGoCrDr = Case @TrxType    WHEN 'C' THEN CanGoInCredit            
   WHEN 'D' THEN CanGoInDebit    END ,            
   @MinAmount =IsNull( MinBalance,0)            
  From t_Products            
  WHERE   OurBranchID = @OurBranchID AND ProductID = @ProductID            
            
  IF @IsProductFreezed = 1            
   RETURN(19) -- Product Is Freezed            
            
  IF @AllowCrDr = 0            
   RETURN(21) -- In Product Condition Dr/Cr Trx Not Allowed            
            
  SELECT @AllowCrDr = Case @TrxType WHEN 'C' THEN AllowCreditTransaction WHEN 'D' THEN AllowDebitTransaction END,          
  @EveryTrxGoesInSupervision = Case @TrxType WHEN 'C' THEN CreditNeedsSupervision WHEN 'D' THEN DebitNeedsSupervision END          
  From t_Account          
  WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID          
            
  IF @AllowCrDr = 1            
    RETURN(20) -- In Account Special Condition Dr/Cr Transaction Not Allowed            
            
/************************   END Checking A/c Status ************************************/            
            
/********************* Checking Supervision Status *****************************/            
          
            --SET @VpOD = (@Amount * -1) - (@Limit - ( @ClearBalance + @Effects ))            
  SET @VpOD = @Limit            
            
  IF @IsFreezed = 1            
   SELECT @MinAmount = @MinAmount + @BlockedAmount            
            
--print 'IsFreezed ' + cast ( @IsFreezed as char(1))            
            
  IF @IsFreezed = 1 AND (@ClearBalance+ @VpOD ) < (@MinAmount + @Amount)            
  BEGIN            
     IF @CanGoCrDr = 0            
      BEGIN            
                     IF UPPER(@OperatorID)<> 'COMPUTER'            
                        RETURN(22) -- A/c Is Freezed Cannot Go In Debit            
      END            
     ELSE            
                       SET @Supervision = '*'            
            
  END            
  ELSE            
  BEGIN            
   IF @Amount <= @ClearBalance + @VpOD - @MinAmount            
   --BEGIN            
    SET @Supervision = ''            
    --IF UPPER(@OperatorID)<> 'COMPUTER'            
    -- RETURN(23) -- This Trx Cannot Go In Debit            
   --END            
   ELSE            
   BEGIN            
            IF @Amount <= @ClearBalance + @VpOD +@Effects - @MinAmount            
    SET @Supervision = 'E' --Effects not cleared            
            ELSE            
                                         SET @Supervision = '*' --Not enough balance            
   END            
  END            
 END            
-----------------------------------------------------------------------------------            
            
--print 'Supervision :' + @Supervision            
 IF @ByPassSupervision = 1            
  SET @Supervision = 'C'            
 ELSE            
  BEGIN            
   IF @Supervision = ''            
   BEGIN            
    IF @LocEq > @PostingLimit            
     SET @Supervision = 'U'              
   END            
  END            
-------------------------------------------------------------------------------------                    
            
/************************ End Checking Supervision Status **************************/            
            
 IF @Supervision = ''            
  IF @EveryTrxGoesInSupervision = 1            
   RETURN (98)  -- Every Trx Goes In Supervision            
  ELSE            
   RETURN (97)            
            
 ELSE IF @Supervision = 'U' -- Supervision Requried            
  RETURN (98)            
 ELSE IF @Supervision = '*' -- Remote Pass            
  RETURN (99)            
 ELSE IF @Supervision = 'E' -- Effect Not cleared            
  RETURN (96)            
            
 IF @EveryTrxGoesInSupervision = 1            
  RETURN (98)  -- Every Trx Goes In Supervision 