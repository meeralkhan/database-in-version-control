﻿
CREATE PROCEDURE [dbo].[sp_DeleteATMUtilityCharges]
(    
  @OurBranchID varchar(30),
  @CompanyNumber varchar(30)
 )    
AS    
     
   DELETE FROM t_ATM_UtilityCharges
   WHERE  OurBranchID = @OurBranchID AND CompanyNumber = @CompanyNumber