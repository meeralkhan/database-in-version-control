﻿
CREATE   PROCEDURE [dbo].[spc_CustomerBalanceHistory]
AS
SET NOCOUNT ON

INSERT INTO t_AccountBalanceHistory (wDate, OurBranchID, AccountID, ProductID, AccountName, ClearBalance, LocalClearBalance, Effects, LocalEffects, Limit, OverdraftClassID, NoOfDPD)
select b.LASTEOD AS wDate, a.OurBranchID, a.AccountID, a.ProductID, a.Name, ClearBalance, LocalClearBalance, Effects, LocalEffects, Limit, ISNULL(c.OverdraftClassID,''), ISNULL(c.NoOfDPD,0)
from t_PrevAccountBalances a
inner join t_Last b on a.OurBranchID = b.OurBranchID
inner join t_Account c on a.OurBranchID = c.OurBranchID and a.AccountID = c.AccountID

SELECT 'Ok' AS RetStatus