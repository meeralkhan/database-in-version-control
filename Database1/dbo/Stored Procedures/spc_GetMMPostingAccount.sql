﻿CREATE PROCEDURE [dbo].[spc_GetMMPostingAccount]
(
  @OurBranchID  varchar(30),
  @PartyID  varchar(30),
  @CurrencyID varchar(30)
)
AS

DECLARE @IsGL char(1)

SELECT @IsGL=IsGLAccount FROM t_MMPartyGLInterFace Where OurBranchID=@OurBranchID AND PartyID=@PartyID AND CurrencyID=@CurrencyID

IF @IsGL='1'
BEGIN
  SELECT * FROM v_GetMMPartyGLInterFace Where OurBranchID=@OurBranchID AND PartyID=@PartyID AND CurrencyID=@CurrencyID
END
ELSE
BEGIN
  SELECT * FROM v_GetMMPartyGLInterFaceCust Where OurBranchID=@OurBranchID AND PartyID=@PartyID AND CurrencyID=@CurrencyID
END