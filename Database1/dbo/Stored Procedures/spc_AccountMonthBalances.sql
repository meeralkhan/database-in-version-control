﻿CREATE PROCEDURE [dbo].[spc_AccountMonthBalances]          
(          
 @OurBranchID varchar(30),          
 @Month datetime          
)          
          
AS          
          
SET NOCOUNT ON          
        
UPDATE t_AccountBalance set      
NoOfCrTrxMonthly = 0, NoOfDrTrxMonthly = 0, AmountOfCrTrxMonthly = 0, AmountOfDrTrxMonthly = 0,      
NoOfCWTrxATMMonthly = 0, NoOfBITrxATMMonthly = 0, AmountOfCWTrxATMMonthly = 0, AmountOfBITrxATMMonthly = 0      
Where OurBranchID = @OurBranchID          
        
Insert Into t_AccountMonthBalances          
([Month],OurBranchID,AccountID,ProductID,[Name],ClearBalance,Effects,LocalClearBalance,LocalEffects)          
Select @Month,OurBranchID,AccountID,ProductID,[Name],ClearBalance,Effects,LocalClearBalance,LocalEffects          
From t_AccountBalance          
Where OurBranchID = @OurBranchID          
          
Update t_Last Set EOMUPDATE = @Month Where OurBranchID = @OurBranchID          
          
SELECT 'Ok' AS RetStatus