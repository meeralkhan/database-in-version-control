﻿CREATE PROCEDURE [dbo].[spc_GeneralLedgerBalanceHistory]
AS
SET NOCOUNT ON

INSERT INTO t_GLBalanceHistory (OurBranchID, wDate, AccountID, Description, CurrencyID, AccountType, ForeignBalance, Balance)
SELECT a.OurBranchID, b.LASTEOD AS wDate, AccountID, Description, CurrencyID, AccountType, ForeignBalance, Balance from t_PrevGLBalances a inner join t_Last b on a.OurBranchID = b.OurBranchID and a.AccountClass = 'P'

SELECT 'Ok' AS RetStatus