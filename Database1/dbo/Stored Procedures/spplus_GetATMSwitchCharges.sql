﻿  
CREATE PROCEDURE [dbo].[spplus_GetATMSwitchCharges] (
 @OurBranchID varchar(30),
 @MerchantType varchar(30)=''
)  
 AS    
 if @MerchantType <> ''    
  begin    
   if exists ( SELECT MerchantType FROM t_ATM_SwitchCharges WHERE OurBranchID = @OurBranchID AND MerchantType = @MerchantType )
    
    SELECT MerchantType, WithdrawlCharges, AcquiringChargesAmount, BalanceEnquiryCharges, AcqBICharges, MiniStatmentCharges, IssuerAccountID, IssuerAccountTitle, AcquiringAccountID, AcquiringAccountTitle, IssuerChargesAccountID, IssuerTitle, AcquiringChargesAccountID, AcquiringTitle, WRDLCHGExciseDuty, BALENQCHGExciseDuty, [3PFTAccountID], 
	[3PFTAccountTitle]=ISNULL((SELECT [Description] from t_GL WHERE OurBranchID = @OurBranchID AND AccountID = [3PFTAccountID]),''), IBFTAccountID, 
	IBFTAccountTitle=ISNULL((SELECT [Description] from t_GL WHERE OurBranchID = @OurBranchID AND AccountID = IBFTAccountID),''),
	IBFTCharges, ExciseDutyAccountID,
	ExciseDutyAccountTitle=ISNULL((SELECT [Description] from t_GL WHERE OurBranchID = @OurBranchID AND AccountID = ExciseDutyAccountID),''),
	ExciseDutyPercentage, OurBranchID, FTCharges, 1 as ReturnValue 
	from t_ATM_SwitchCharges
	Where OurBranchID = @OurBranchID AND MerchantType = @MerchantType
      
   else    
   
    select 0 as ReturnValue    
    
  end    
    
 else    
  begin    
   select MerchantType, WithdrawlCharges, AcquiringChargesAmount, BalanceEnquiryCharges, AcqBICharges, MiniStatmentCharges, IBFTCharges, ExciseDutyPercentage, FTCharges
   FROM t_ATM_SwitchCharges WHERE OurBranchID = @OurBranchID 
   
  end