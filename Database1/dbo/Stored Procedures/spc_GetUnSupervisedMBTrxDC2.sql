﻿CREATE   PROCEDURE [dbo].[spc_GetUnSupervisedMBTrxDC2]      
  
AS      
      
set nocount on      
      
BEGIN TRY       
        
  SELECT distinct 'Ok' AS ReturnStatus, ScrollNo FROM t_TransferTransactionModel WHERE OurBranchID IN (select OurBranchID from t_Last) AND Supervision NOT IN ('C','R')      
        
END TRY       
BEGIN CATCH       
        
  select 'Error' AS ReturnStatus, ERROR_NUMBER() AS ReturnErrorNo, ERROR_MESSAGE() AS ReturnMessage;       
        
END CATCH