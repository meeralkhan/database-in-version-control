﻿create procedure [dbo].[spc_GetDisbRAccounts]  
(  
   @OurBranchID nvarchar(30),  
   @AccountID nvarchar(30)  
)  
  
AS  
  
SET NOCOUNT ON  
  
select * from t_Account  
where OurBranchID = @OurBranchID  
and ClientID IN (select ClientID from t_Account where OurBranchID = @OurBranchID and AccountID = @AccountID)  
and ProductID IN (select ReceivableProductID from t_Products where OurBranchID = @OurBranchID and ProductID IN (select ProductID from t_Account where OurBranchID = @OurBranchID and AccountID = @AccountID))