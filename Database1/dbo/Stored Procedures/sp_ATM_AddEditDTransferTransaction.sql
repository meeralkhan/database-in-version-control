﻿CREATE PROCEDURE [dbo].[sp_ATM_AddEditDTransferTransaction]
(  
 @OurBranchID            varchar(30),   
 @WithdrawlBranchID      varchar(30)='',   
 @CustomerBranchID       varchar(30),  
 @CustomerBranchID2      varchar(30),  
 @ATMID                  varchar(30),   
 @AccountID              varchar(30),  
 @AccountID2             varchar(30),  
 @Amount                 money,  
 @USDAmount              money=0,  
 @OtherCurrencyAmount    money=0,  
 @USDRate                money=0,  
 @Supervision            char(1),   
 @RefNo          varchar(50),  
 @PHXDate        datetime,  
 @MerchantType      varchar(30),  
 @OurIMD       varchar(30),  
 @AckInstIDCode      varchar(30),  
 @Currency       varchar(3),  
 @NameLocation      varchar(100),  
 @MCC        varchar(30),
 @TrxDesc        varchar(30)='',
 @NewRecord       bit=1  
)    
AS
BEGIN    

 --Fund Transfer Debit Transaction Procedure 
 --The FT Debit Advice will be sent by the controller to the Debit Customer Branch
 
 DECLARE @BankShortName as varchar(100)  
  SET @BankShortName = 'N/A'
 DECLARE @mScrollNo as int  
 DECLARE @mSerialNo as smallint  
  SET @mSerialNo = 0
 DECLARE @mAccountID as varchar(30)  
 DECLARE @mAccountName as varchar(100)  
 DECLARE @mAccountType as char  
 DECLARE @mProductID as varchar(30)  
 DECLARE @mCurrencyID as varchar(30)  
 DECLARE @mIsLocalCurrency as bit  
  SET @mIsLocalCurrency=1   
 DECLARE @mwDate as datetime  
 DECLARE @mTrxType as char  
 DECLARE @STAN as varchar(30)
 DECLARE @mGLID as varchar(30)  
  SET @mGLID = ''   
 DECLARE @mForeignAmount as money  
  SET @mForeignAmount=0   
 DECLARE @mExchangeRate as money  
  SET @mExchangeRate=1   
 DECLARE @mDescriptionID as varchar(30)  
  SET @mDescriptionID='000'   
 DECLARE @mDescription as varchar(255)  
 DECLARE @mDescriptionH as varchar(255)  
 DECLARE @mRemarks as varchar(200)  
 DECLARE @mBankCode as varchar(30)  
  SET @mBankCode='1279'   
 DECLARE @mOperatorID as varchar(30)  
  SET @mOperatorID = 'ATM-' + @ATMID  
 DECLARE @mSupervisorID as varchar(30)  
  SET @mSupervisorID = @OurIMD   
--Checking Variables  
 DECLARE @vCount1 as int  
 DECLARE @vCount2 as bit  
 DECLARE @vCount3 as bit  
 DECLARE @vCount4 as bit  
 DECLARE @vAccountID as varchar(30)  
 DECLARE @vClearBalance as money  
 DECLARE @vClearBalance2 as money  
 DECLARE @vClearedEffects as money  
 DECLARE @vFreezeAmount as money  
 DECLARE @vMinBalance as money  
 DECLARE @Limit as money  
 DECLARE @vIsClose as char  
 DECLARE @vAreCheckBooksAllowed as bit  
 DECLARE @vAllowDebitTransaction as bit  
 DECLARE @vBranchName as varchar(50)  
 DECLARE @Status as int   
 DECLARE @mLastEOD as datetime  
-- DECLARE @mWorkingDate as datetime  
 DECLARE @mCONFRETI as datetime  
 DECLARE @mWithdrawlBranchName as varchar(100) 
 DECLARE @IssuerAccountID as varchar(30)  
 DECLARE @IssuerAccountTitle as varchar(100)  
 DECLARE @AcquiringAccountTitle as varchar(100)  
 DECLARE @IssuerChargesAccountID as varchar(30)  
 DECLARE @IssuerTitle as varchar(100)  
 DECLARE @mRemarksCharges as varchar(200)  
 DECLARE @ExciseDutyAccountID as varchar(30)  
 DECLARE @ExciseDutyPercentage as money  
 DECLARE @ExciseDutyAmount as money  
 DECLARE @FEDAccID as varchar(30)
 DECLARE @FEDAccTitle as varchar(100)
 DECLARE @FTCharges as money
  SET @ExciseDutyPercentage = 0
  SET @ExciseDutyAmount = 0
  SET @FTCharges = 0
 
 DECLARE @VoucherID int
 DECLARE @IsCredit int
 DECLARE @GLControl nvarchar(30)
 
 SELECT @BankShortName = BankShortName From t_ATM_Banks WHERE (OurBranchID = @OurBranchID AND BankIMD = @OurIMD)
 
 SET @mDescription='IB(' + @ATMID + ') FUND TRF AT BK.' + @BankShortName  + ' (BR.' + @WithdrawlBranchID + ') TO : A/c # ' + @AccountID2 
 SET @mDescriptionH='ATM (' + @ATMID + ') FUND TRF AT BK.' + @BankShortName   + ' (BR.' + @CustomerBranchID + ') From : A/c # ' + @AccountID
  
 SET NOCOUNT ON  
 SELECT @Status = -1  

 SELECT @mLastEOD=LastEOD,@mwDate=WorkingDate,@mCONFRETI=CONFRETI FROM t_Last WHERE (OurBranchID = @OurBranchID)  
 
 IF @mLastEOD = @mwDate or @mCONFRETI = '1/1/9999'  
 BEGIN  
     
  SELECT @Status=9  --INVALID_DATE  
  SELECT @Status  as Status  
  RETURN (1)   
 END  
  
 SELECT @mScrollNo=(ISNULL(MAX(ScrollNo),0)+1) FROM t_ATM_TransferTransaction WHERE (OurBranchID = @OurBranchID) and 
 (convert(varchar(10), wDate, 120) = convert(varchar(10), @mwDate, 120))
 
 SELECT @vCount1=count(*) From t_Account WHERE (OurBranchID = @OurBranchID) AND( AccountID in (@AccountID))  
 
 IF  @vCount1=0  
 BEGIN  
     
  SELECT @Status=2  --INVALID_ACCOUNT  
  SELECT @Status  as Status  
  RETURN (1)   
 END  
  
 SELECT @mAccountName=B.Name, @mProductID=B.ProductID, @mCurrencyID=P.CurrencyID, @mAccountType='C', 
 @vClearBalance=B.ClearBalance, @vClearedEffects=B.ClearedEffects, @vFreezeAmount=B.FreezeAmount, @vMinBalance=P.MinBalance, 
 @Limit=isnull(b.Limit,0), @vIsClose=A.[Status], @vAreCheckBooksAllowed=P.AreCheckBooksAllowed,
 @vAllowDebitTransaction=isnull(A.AllowDebitTransaction,0)    
 FROM t_AccountBalance B 
 INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND B.ProductID = A.ProductID 
 INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND  A.ProductID = P.ProductID 
 WHERE (A.OurBranchID = @OurBranchID) AND (A.AccountID = @AccountID) 
 
 SET @vClearBalance2=@vClearBalance  
 
 SELECT @IssuerAccountID=IssuerAccountID,@IssuerAccountTitle=IssuerAccountTitle,
 @IssuerChargesAccountID=IssuerChargesAccountID,@IssuerTitle=IssuerTitle,@ExciseDutyAccountID= ExciseDutyAccountID,
 @ExciseDutyPercentage=ExciseDutyPercentage,@FTCharges=FTCharges
 From t_ATM_SwitchCharges WHERE (OurBranchID = @OurBranchID AND MerchantType = @MerchantType)
 
 IF @vIsClose = 'C' or @vIsClose = 'T' or @vIsClose = 'X' or @vIsClose = 'D' or @vAllowDebitTransaction = 1
 BEGIN  
     
  SELECT @Status=3  --INACTIVE/CLOSE_ACCOUNT  
  SELECT @Status  as Status  
  RETURN (1)   
 END  
 
 IF @Amount >(@vClearBalance-@vClearedEffects+@Limit)-@FTCharges OR 
    @Amount >((@vClearBalance-@vClearedEffects+@Limit)-@vFreezeAmount-@vMinBalance-@FTCharges) OR
    @Amount >((@vClearBalance-@vClearedEffects+@Limit)-@vMinBalance-@FTCharges) 
 BEGIN
     
  SELECT @Status=4  --LOW_BALANCE  
  SELECT @Status  as Status  
  RETURN (1)   
 END  
 
 IF @vAreCheckBooksAllowed = 0   
 BEGIN  
     
  SELECT @Status=3  --INVALID_PRODUCT  
  SELECT @Status  as Status  
  RETURN (1)   
 END  
 
 IF @mCurrencyID <> 'PKR'  
 BEGIN  
     
  SELECT @Status=3  --INVALID_CURRENCY  
  SELECT @Status  as Status  
  RETURN (1)   
 END  
    
 -------------------Debit Transaction---------------------
 
 Set @mSerialNo = @mSerialNo + 1
 Set @mTrxType = 'D'  
 SET @IsCredit = 0
 SET @STAN = RIGHT(@RefNo,6)
 
 SELECT @vBranchName=Heading2 FROM t_GlobalVariables WHERE (OurBranchID = @OurBranchID)  
 SELECT @mWithdrawlBranchName=BranchName FROM t_ATM_Branches WHERE OurBranchID = @OurBranchID AND BranchID = @WithdrawlBranchID
 
 SET @mRemarks='Being Amount of IB (' + @ATMID+ ') Inter Branch Fund Transfer Transaction From : BK.' + @BankShortName +  ' (BR.' +  @WithdrawlBranchID + ') By  A/c # ' + @AccountID +  ' At : '  + upper(@vBranchName)  
 
 INSERT INTO t_ATM_TransferTransaction  
 (  
  ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
  IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
  [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
 )
 VALUES  
 (    
  @mScrollNo,@mSerialNo,@RefNo,@OurBranchID,@WithdrawlBranchID,@AccountID,@mAccountName,@mAccountType,@mProductID,
  @mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,
  @Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescription+' STAN: '+@STAN,@ATMID,
  @mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MCC,@TrxDesc,@MerchantType,@STAN
 )  

 select @GLControl = ISNULL(GLControl, '') from t_Products where OurBranchID = @OurBranchID and ProductID = @mProductID
 select @VoucherID = ISNULL(MAX(VoucherID),0) + 1 from t_GLTransactions where OurBranchID = @OurBranchID
    
 INSERT INTO t_Transactions 
 ( 
  OurBranchID,ScrollNo,SerialNo,RefNo,wDate,AccountType,DocumentType,AccountID,AccountName,ProductID,CurrencyID,ValueDate,
  TrxType,ChequeID,ChequeDate,Amount,ForeignAmount,ExchangeRate,ProfitLoss,MeanRate,DescriptionID,[Description],BankCode,
  BranchCode,TrxPrinted,GLID,[Status],IsLocalCurrency,OperatorID,SupervisorID,AdditionalData,IsMainTrx,DocType,GLVoucherID,
  Remarks,TrxDesc,MerchantType,STAN 
 ) 
          
 VALUES 
 ( 
  @OurBranchID,@mScrollNo,@mSerialNo,@RefNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@mAccountType,'AT',
  @AccountID,@mAccountName,@mProductID,@mCurrencyID,@mwDate,@mTrxType,'',@mwDate,@Amount,@mForeignAmount,@mExchangeRate,0,
  @mExchangeRate,@mDescriptionID,@mDescription+' STAN: '+@STAN,@mBankCode,@WithdrawlBranchID,0,null,@Supervision,
  @mIsLocalCurrency,@mOperatorID,@mSupervisorID,null,'1','AT',@VoucherID,@mRemarks,@TrxDesc,@MerchantType,@STAN 
 )
    
 INSERT INTO t_GLTransactions 
 ( 
  OurBranchID,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,
  ExchangeRate,IsCredit,TransactionType,TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,
  IsMainTrx,DocType,Remarks,TrxDesc,MerchantType,STAN 
 )
 
 VALUES
 ( 
  @OurBranchID,@GLControl,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@mwDate,@mDescriptionID,
  @mDescription+' STAN: '+@STAN,@mCurrencyID,@Amount,@mForeignAmount,@mExchangeRate,@IsCredit,'ATMTransfer','L',@mOperatorID,
  @mSupervisorID,'',@mScrollNo,@mSerialNo,null,'0','AT',@mRemarks,@TrxDesc,@MerchantType,@STAN 
 )

 SELECT @Status=@@ERROR  
 
 IF @Status <> 0    
 BEGIN  
     
  RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR   
  RETURN (5000)  
 END  
 
 --Credit Transaction     
 
 SET @vIsClose = ''  
 SET @vAreCheckBooksAllowed = ''  
 SET @vAllowDebitTransaction = ''  
 Set @mSerialNo = @mSerialNo + 1
 Set @mTrxType = 'C' 
 SET @IsCredit = 1
 
 SET @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Branch Fund Transfer Transaction From : BK.' + @BankShortName +  ' (' + upper(@mWithdrawlBranchName) + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
 
 INSERT INTO t_ATM_TransferTransaction  
 (  
  ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
  IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
  [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
 )
 VALUES
 (
 @mScrollNo,@mSerialNo,@RefNo,@OurBranchID,@WithdrawlBranchID,@IssuerAccountID,@IssuerAccountTitle,'G','GL','PKR',
 @mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,@Supervision,@mGLID,
 @Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescriptionH+' STAN: '+@STAN,@ATMID,@mBankCode,@mRemarks,
 @mOperatorID,@mSupervisorID,@MCC,@TrxDesc,@MerchantType,@STAN
 )
 
 INSERT INTO t_GLTransactions 
 (
  OurBranchID,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,
  ExchangeRate,IsCredit,TransactionType,TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,
  IsMainTrx,DocType,Remarks,TrxDesc,MerchantType,STAN
 )
 
 VALUES 
 (
  @OurBranchID,@IssuerAccountID,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@mwDate,
  @mDescriptionID,@mDescriptionH+' STAN: '+@STAN,@mCurrencyID,@Amount,@mForeignAmount,@mExchangeRate,@IsCredit,'ATMTransfer',
  'L',@mOperatorID,@mSupervisorID,'',@mScrollNo,@mSerialNo,null,'1','AT',@mRemarks,@TrxDesc,@MerchantType,@STAN 
 )
 
 SELECT @Status=@@ERROR
 
 IF @Status <> 0
 BEGIN
     
  RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR  
  RETURN (5000)  
 END  

 -----Charges DEBIT Transaction-----
 
 if (@FTCharges > 0)
 BEGIN
  
  SET @ExciseDutyAmount = (@FTCharges * @ExciseDutyPercentage) / 116
 
  Set @mSerialNo = @mSerialNo + 1
  SET @mTrxType = 'D'  
  SET @mAccountType='C'
  SET @mCurrencyID='PKR'
  SET @IsCredit = 0
  
  SET @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Branch Fund Transfer Charges Transaction From : BK.' + @BankShortName +  ' (BR.' +  @WithdrawlBranchID + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
  
  INSERT INTO t_ATM_TransferTransaction
  (
   ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
   IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
   [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
  )
  VALUES
  (
   @mScrollNo,@mSerialNo,@RefNo,@OurBranchID,@WithdrawlBranchID,@AccountID,@mAccountName,@mAccountType,@mProductID,
   @mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,
   @Supervision,@mGLID,@FTCharges,0,1,@mDescriptionID,@mDescriptionH+' STAN: '+@STAN,@ATMID,@mBankCode,@mRemarks,
   @mOperatorID,@mSupervisorID,@MCC,@TrxDesc+'|IBR-FT CHG',@MerchantType,@STAN
  )
  
  INSERT INTO t_Transactions 
  ( 
   OurBranchID,ScrollNo,SerialNo,RefNo,wDate,AccountType,DocumentType,AccountID,AccountName,ProductID,CurrencyID,ValueDate,
   TrxType,ChequeID,ChequeDate,Amount,ForeignAmount,ExchangeRate,ProfitLoss,MeanRate,DescriptionID,[Description],BankCode,
   BranchCode,TrxPrinted,GLID,[Status],IsLocalCurrency,OperatorID,SupervisorID,AdditionalData,IsMainTrx,DocType,GLVoucherID,
   Remarks,TrxDesc,MerchantType,STAN 
  ) 
          
  VALUES 
  ( 
   @OurBranchID,@mScrollNo,@mSerialNo,@RefNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@mAccountType,'AT',
   @AccountID,@mAccountName,@mProductID,@mCurrencyID,@mwDate,@mTrxType,'',@mwDate,@FTCharges,@mForeignAmount,@mExchangeRate,0,
   @mExchangeRate,@mDescriptionID,@mDescriptionH+' STAN: '+@STAN,@mBankCode,@WithdrawlBranchID,0,null,@Supervision,
   @mIsLocalCurrency,@mOperatorID,@mSupervisorID,null,'1','AT',@VoucherID,@mRemarks,@TrxDesc+'|IBR-FT CHG',@MerchantType,@STAN 
  )
    
  INSERT INTO t_GLTransactions 
  ( 
   OurBranchID,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,
   ExchangeRate,IsCredit,TransactionType,TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,
   IsMainTrx,DocType,Remarks,TrxDesc,MerchantType,STAN 
  )
 
  VALUES
  ( 
   @OurBranchID,@GLControl,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@mwDate,@mDescriptionID,
   @mDescriptionH+' STAN: '+@STAN,@mCurrencyID,@FTCharges,@mForeignAmount,@mExchangeRate,@IsCredit,'ATMTransfer','L',
   @mOperatorID,@mSupervisorID,'',@mScrollNo,@mSerialNo,null,'0','AT',@mRemarks,@TrxDesc+'|IBR-FT CHG',@MerchantType,@STAN 
  )
   
  SELECT @Status=@@ERROR
   
  IF @Status <> 0
  BEGIN
      
   RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR  
   RETURN (5000)  
  END
   
  --Charges Credit Transaction
   
  Set @mSerialNo = @mSerialNo + 1
  SET @mTrxType = 'C'  
  SET @mAccountType='G'
  SET @mProductid ='GL'  
  SET @mCurrencyID='PKR'
  SET @IsCredit = 1
  
  SET @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Branch Fund Transfer Charges Transaction From : BK.' + @BankShortName +  ' (BR.' +  @WithdrawlBranchID + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  

  INSERT INTO t_ATM_TransferTransaction 
  (  
   ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
   IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
   [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
  )
  VALUES 
  (
   @mScrollNo,@mSerialNo,@RefNo,@OurBranchID,@WithdrawlBranchID,@IssuerChargesAccountID,@IssuerTitle,@mAccountType,
   @mProductID,@mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,
   @mTrxType,@Supervision,@mGLID,(@FTCharges-@ExciseDutyAmount),0,1,@mDescriptionID,@mDescriptionH+' STAN: '+@STAN,@ATMID,
   @mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MCC,@TrxDesc+'|IBR-FT CHG',@MerchantType,@STAN
  )
  
  INSERT INTO t_GLTransactions 
  (
   OurBranchID,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,
   ExchangeRate,IsCredit,TransactionType,TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,
   IsMainTrx,DocType,Remarks,TrxDesc,MerchantType,STAN 
  )
  
  VALUES
  (
   @OurBranchID,@IssuerChargesAccountID,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),
   @mwDate,@mDescriptionID,@mDescriptionH+' STAN: '+@STAN,@mCurrencyID,(@FTCharges-@ExciseDutyAmount),@mForeignAmount,
   @mExchangeRate,@IsCredit,'ATMTransfer','L',@mOperatorID,@mSupervisorID,'',@mScrollNo,@mSerialNo,null,'1','AT',
   @mRemarks,@TrxDesc+'|IBR-FT CHG',@MerchantType,@STAN 
  )
  
  SELECT @Status=@@ERROR
  
  IF @Status <> 0
  BEGIN
      
   RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR  
   RETURN (5000)  
  END
  
  ----FED Charges Credit Transaction----
  
  if (@ExciseDutyPercentage > 0)
  BEGIN
  
   SELECT @FEDAccID = AccountID, @FEDAccTitle = [Description] FROM t_GL WHERE OurBranchID = @OurBranchID AND AccountID = @ExciseDutyAccountID
  
   Set @mSerialNo = @mSerialNo + 1
   SET @mTrxType = 'C'
   SET @mAccountType='G'
   SET @mProductid ='GL'
   SET @mCurrencyID='PKR'
   SET @IsCredit = 1
  
   SET @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Branch Fund Transfer FED Charges From : BK.' + @BankShortName +  ' (BR.' +  @WithdrawlBranchID + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)
  
   INSERT INTO t_ATM_TransferTransaction 
   (  
    ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
    IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
    [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
   )
   VALUES
   (
    @mScrollNo,@mSerialNo,@RefNo,@OurBranchID,@WithdrawlBranchID,@FEDAccID,@FEDAccTitle,@mAccountType,@mProductID,
    @mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,
    @Supervision,@mGLID,@ExciseDutyAmount,0,1,@mDescriptionID,@mDescriptionH+' STAN: '+@STAN,@ATMID,@mBankCode,@mRemarks,
    @mOperatorID,@mSupervisorID,@MCC,@TrxDesc+'|IBR-FT FED CHG',@MerchantType,@STAN
   )
   
   INSERT INTO t_GLTransactions 
   (
    OurBranchID,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,
    ExchangeRate,IsCredit,TransactionType,TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,
    IsMainTrx,DocType,Remarks,TrxDesc,MerchantType,STAN 
   )
  
   VALUES
   (
    @OurBranchID,@FEDAccID,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@mwDate,
    @mDescriptionID,@mDescriptionH+' STAN: '+@STAN,@mCurrencyID,@ExciseDutyAmount,@mForeignAmount,@mExchangeRate,@IsCredit,
    'ATMTransfer','L',@mOperatorID,@mSupervisorID,'',@mScrollNo,@mSerialNo,null,'1','AT',@mRemarks,@TrxDesc+'|IBR-FT FED CHG',
    @MerchantType,@STAN 
   )
  
   SELECT @Status=@@ERROR
  
   IF @Status <> 0
   BEGIN
       
    RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR  
    RETURN (5000)  
   END
  END
  
  SELECT @Status = 0, 
  @vClearBalance2 = ((IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0))-IsNull(@Amount,0)-isNull(@FTCharges,0))*100
  
  SELECT @Status As Status , @vClearBalance2 As ClearBalance  
  RETURN 0   
  
     
 END
END