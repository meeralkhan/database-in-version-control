﻿CREATE    procedure [dbo].[spc_DC_Initialize_Tables]     
(     
 @ProgramID varchar(30),     
 @Table varchar(100)     
)     
AS     
     
SET NOCOUNT ON     
     
DECLARE @SQLQuery varchar(5000)     
     
if @ProgramID = 'GL'     
begin     
     
 set @SQLQuery = 'SELECT * INTO ' + @Table + ' FROM t_GL '     
 exec (@SQLQuery)     
      
 TRUNCATE TABLE t_PrevGLBalances;     
     
 INSERT INTO t_PrevGLBalances     
 SELECT * FROM vc_GLBalances;     
     
 select 'Ok' as ReturnStatus, 'Done' AS ReturnMessage     
     
end     
else if @ProgramID = 'CUSTOMER'     
begin     
     
 set @SQLQuery = 'SELECT a.*,CurrencyID,AllowCreditTransaction,AllowDebitTransaction,Status,ClientID INTO ' + @Table + ' FROM t_AccountBalance a  
inner join t_Account b on a.OurBranchID = b.OurBranchID and a.AccountID = b.AccountID'     
 exec (@SQLQuery)     
      
 TRUNCATE TABLE t_PrevAccountBalances;     
     
 INSERT INTO t_PrevAccountBalances     
 SELECT * FROM vc_AccountBalances;     
     
 select 'Ok' as ReturnStatus, 'Done' AS ReturnMessage     
     
end     
else if @ProgramID = 'CASH'     
begin     
     
 set @SQLQuery = 'SELECT * INTO ' + @Table + ' FROM t_CashTransactionModel '     
     
 exec (@SQLQuery)     
     
 DELETE FROM t_CashTransactionModel      
     
 select 'Ok' as ReturnStatus, 'Done' AS ReturnMessage     
     
end     
else if @ProgramID = 'OLCASH'     
begin     
     
 set @SQLQuery = 'SELECT * INTO ' + @Table + ' FROM t_OnLineCashTransaction '     
     
 exec (@SQLQuery)     
     
 DELETE FROM t_OnLineCashTransaction      
      
 select 'Ok' as ReturnStatus, 'Done' AS ReturnMessage     
     
end     
else if @ProgramID = 'TRANSFER'     
begin     
     
 set @SQLQuery = 'SELECT * INTO ' + @Table + ' FROM t_TransferTransactionModel '     
     
 exec (@SQLQuery)     
     
 DELETE FROM t_TransferTransactionModel      
     
 select 'Ok' as ReturnStatus, 'Done' AS ReturnMessage     
     
end     
else if @ProgramID = 'ATMCASH'     
begin     
     
 set @SQLQuery = 'SELECT * INTO ' + @Table + ' FROM t_ATM_CashTransaction '     
     
 exec (@SQLQuery)     
     
 DELETE FROM t_ATM_CashTransaction      
     
 select 'Ok' as ReturnStatus, 'Done' AS ReturnMessage     
     
end     
else if @ProgramID = 'ATMTRANSFER'     
begin     
     
 set @SQLQuery = 'SELECT * INTO ' + @Table + ' FROM t_ATM_TransferTransaction '     
     
 exec (@SQLQuery)     
     
 DELETE FROM t_ATM_TransferTransaction      
     
 select 'Ok' as ReturnStatus, 'Done' AS ReturnMessage     
     
end     
else if @ProgramID = 'INWARD'     
begin     
     
 set @SQLQuery = 'SELECT * INTO ' + @Table + ' FROM t_InwardClearing '     
     
 exec (@SQLQuery)     
     
 DELETE FROM t_InwardClearing      
     
 select 'Ok' as ReturnStatus, 'Done' AS ReturnMessage     
     
end     
else if @ProgramID = 'BUREAU'     
begin     
     
 set @SQLQuery = 'SELECT * INTO ' + @Table + ' FROM t_BureaDeChange '     
     
 exec (@SQLQuery)     
     
 DELETE FROM t_BureaDeChange      
     
 select 'Ok' as ReturnStatus, 'Done' AS ReturnMessage     
     
end     
else if @ProgramID = 'OUTWARD'     
begin     
      
 set @SQLQuery = 'SELECT * INTO ' + @Table + ' FROM t_OutwardClearing '     
 exec (@SQLQuery)     
        
 DELETE FROM t_OutwardClearing      
     
 select 'Ok' as ReturnStatus, 'Done' AS ReturnMessage     
     
end     
else if @ProgramID = 'DEBITCARD'     
begin     
     
 set @SQLQuery = 'SELECT * INTO ' + @Table + ' FROM t_DebitCards '     
     
 exec (@SQLQuery)     
     
 DELETE FROM t_TransferTransactionModel      
     
 select 'Ok' as ReturnStatus, 'Done' AS ReturnMessage     
     
end    
else     
begin     
     
 select 'Error' as ReturnStatus, 'Invalid Program ID, Pls! Contact Proper Person...' AS ReturnMessage     
     
end