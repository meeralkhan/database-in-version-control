﻿CREATE   PROCEDURE [dbo].[spc_GetCBSData]    
(               
 @OurBranchID varchar(30),    
 @sDate as datetime    
)               
AS               
BEGIN    
               
 SET NOCOUNT ON               
               
 DECLARE @OpeningBalance as money               
 DECLARE @ClosingBalance as money               
 DECLARE @crOpBal as money               
 DECLARE @drOpBal as money               
 DECLARE @cCount as smallint    
 DECLARE @sCount as smallint      
 DECLARE @WorkingDate as datetime               
 DECLARE @ATMID as varchar(30)    
 DECLARE @Description as varchar(100)    
 DECLARE @ATMAccountID as varchar(30)    
 DECLARE @mATMAccountID as varchar(30)    
 DECLARE @TableName as varchar(100)    
 Declare @SQLQuery as nvarchar(max)          
 Declare @ParamDefinition AS nVarchar(MAX)          
 
 SET @mATMAccountID = ''
 SET @TableName = 't_CBSData'    
    
 if object_id('t_CBSData') IS NULL    
 BEGIN    
  Create Table t_CBSData (    
   OurBranchID varchar(30) NOT NULL,    
   AccountID varchar(30) NOT NULL,    
   TerminalName varchar(30) NOT NULL,    
   Description varchar(100) NOT NULL,    
   Transaction_Particular varchar(255) NOT NULL,    
   ValueDate Datetime NOT NULL,    
   TrxDate Datetime NOT NULL,    
   TrxIdentifier varchar(100) NULL,    
   TerminalID varchar(30) NULL,    
   Customer_Account_Date Datetime NOT NULL,    
   TrxTime varchar(30) NULL,    
   STAN varchar(30) NULL,    
   AuthIDResp varchar(30) NULL,    
   RetRefNum varchar(30) NULL,    
   PAN varchar(30) NULL,    
   TrxType char(1) NOT NULL,    
   Amount money NOT NULL,    
   Balance money NOT NULL,    
   CardAccptID varchar(30) NULL    
  )    
 END    
 ELSE    
 BEGIN    
  delete from t_CBSData    
 END    
    
 SET @ClosingBalance = 0    
 SET @OpeningBalance = 0               
 SET @crOpBal = 0               
 SET @drOpBal = 0               
 SET @cCount = 0               
 SET @sCount = 0               
     
 SELECT @cCount=COUNT(*) from t_ATM_ATMAccount where OurBranchID = @OurBranchID 
    
 WHILE (@cCount <> @sCount)          
 BEGIN          
  SET @sCount = @sCount + 1          
    
  SELECT @ATMID=ATMID, @Description=Description, @ATMAccountID=ATMAccountID    
  FROM (    
   SELECT ATMID, Description, ATMAccountID, ROW_NUMBER() OVER (ORDER BY ATMID) AS RowNum       
   FROM t_ATM_ATMAccount where OurBranchID = @OurBranchID    
  ) AS oTable          
  WHERE oTable.RowNum = @sCount          
  
  if (@ATMAccountID <> @mATMAccountID)
  BEGIN
   select top 1 @OpeningBalance=Balance from t_GLBalanceHistory where OurBranchID = @OurBranchID AND AccountID = @ATMAccountID    
   and convert(varchar(10), wDate, 120) <= @sDate - 1
   order by wDate desc    
               
   SELECT @crOpBal = IsNull(SUM(Amount),0) From t_GLTransactions               
   WHERE OurBranchID = @OurBranchID AND AccountID = @ATMAccountID    
   AND IsCredit = '1' and [Status] <> 'R' AND convert(varchar(10), [Date], 120) = @sDate
    
   SELECT @drOpBal = IsNull(SUM(Amount),0) From t_GLTransactions               
   WHERE OurBranchID = @OurBranchID AND AccountID = @ATMAccountID    
   AND IsCredit = '0' and [Status] <> 'R' AND convert(varchar(10), [Date], 120) = @sDate
      
   SET @ClosingBalance = @OpeningBalance + @crOpBal - @drOpbal    
  END


  if (@ATMAccountID <> @mATMAccountID)
  BEGIN
   Insert Into t_CBSData (OurBranchID, AccountID, TerminalName, [Description], Transaction_Particular, ValueDate, TrxDate, TrxIdentifier, TerminalID, Customer_Account_Date, TrxTime, STAN, 
   AuthIDResp, RetRefNum, PAN, TrxType, Amount, Balance, CardAccptID)
   SELECT @OurBranchID, @ATMAccountID, @ATMID, @Description, 'Opening Balance', @sDate, @sDate, '', '', @sDate, convert(varchar(10), @sDate, 108), '', '', '', '', '', 
   IsNull(@OpeningBalance,0), IsNull(@OpeningBalance,0), ''
  END

  if (@ATMAccountID <> @mATMAccountID)  
  BEGIN  
   Insert Into t_CBSData (OurBranchID, AccountID, TerminalName, [Description], Transaction_Particular, ValueDate, TrxDate, TrxIdentifier, TerminalID, Customer_Account_Date, TrxTime, STAN, 
   AuthIDResp, RetRefNum, PAN, TrxType, Amount, Balance, CardAccptID)
   SELECT OurBranchID, AccountID, @ATMID, @Description, [Description], ValueDate, [Date], 
   ( SELECT top 1 RefNo from t_Transactions where OurBranchID = @OurBranchID AND ScrollNo = t_GLTransactions.ScrollNo AND Len(RefNo) = 32 ), 
   ltrim(substring(OperatorID, 5, 20)), [Date], convert(varchar(10), [Date], 108), STAN, AuthIDResp, RetRefNum,
   ( SELECT top 1 left(RefNo,16) from t_Transactions where OurBranchID = @OurBranchID AND ScrollNo = t_GLTransactions.ScrollNo AND Len(RefNo) = 32 ),
   CASE IsCredit 
    WHEN 1 THEN 'C' ELSE 'D' END As TrxType,
   Amount, 
   Balance = case 
    when IsCredit = 1 then @OpeningBalance + Amount
    when IsCredit = 0 then @OpeningBalance - Amount
   end, CardAccptID
   FROM t_GLTransactions           
   WHERE OurBranchID = @OurBranchID AND AccountID = @ATMAccountID and [Status] <> 'R' 
   --AND CurrCodeTran = '784' AND CurrCodeSett = '784'
   --AND RIGHT(CardAccptNameLoc, 2) IN (SELECT ShortName from t_Country where OurBranchID = t_GLTransactions.OurBranchID AND IsGCC = 0 AND CountryCode <> '784')
   AND convert(varchar(10), Date, 120) >= @sDate AND convert(varchar(10) , Date,120) <= @sDate 
  --END
  END
  if (@ATMAccountID <> @mATMAccountID)
  BEGIN
   Insert Into t_CBSData (OurBranchID, AccountID, TerminalName, [Description], Transaction_Particular, ValueDate, TrxDate, TrxIdentifier, TerminalID, Customer_Account_Date, TrxTime, STAN, 
   AuthIDResp, RetRefNum, PAN, TrxType, Amount, Balance, CardAccptID)
   SELECT @OurBranchID, @ATMAccountID, @ATMID, @Description, 'Closing Balance', @sDate, @sDate, '', '', @sDate, convert(varchar(10), @sDate, 108), '', '', '', '', '', IsNull(@ClosingBalance,0), 
   IsNull(@ClosingBalance,0), ''
  END

  SET @mATMAccountID = @ATMAccountID
      
 END    
END