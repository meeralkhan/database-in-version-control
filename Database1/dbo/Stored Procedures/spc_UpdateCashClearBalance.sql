﻿CREATE PROCEDURE [dbo].[spc_UpdateCashClearBalance]
(              
 @OurBranchID varchar(30),            
 @AccountID varchar(30),            
 @SupervisorID varchar(30),            
 @ScrollNo numeric(24,0),          
 @NewRecord bit = 0,
 @AddData text=''
)            
            
AS            
   SET NOCOUNT ON            
            
   DECLARE @vDate datetime             
   DECLARE @mDate Varchar(32)            
   DECLARE @wDate datetime
   DECLARE @ChequeID varchar(30)            
   DECLARE @ClearBalance money            
   DECLARE @Effects money            
   DECLARE @Limit money            
   DECLARE @TrxType char(1)            
   DECLARE @Amount money            
   DECLARE @ForeignAmount money            
   DECLARE @IsLocalCurrency bit            
   DECLARE @Status char(1)            
   DECLARE @AccountType char(1)             
   DECLARE @VoucherID decimal(24,0)
   DECLARE @GLID varchar(30)
   DECLARE @ProductID varchar(30)
   DECLARE @GLControl varchar(30)
   DECLARE @RefNo varchar(100)
   DECLARE @AccountName varchar(100)
   DECLARE @CurrencyID varchar(30)
   DECLARE @ValueDate datetime
   DECLARE @ChequeDate varchar(30)
   DECLARE @ExchangeRate decimal(24,6)
   DECLARE @DescriptionID varchar(30)
   DECLARE @Description varchar(100)
   DECLARE @BankCode varchar(30)
   DECLARE @BranchCode varchar(30)
   DECLARE @OperatorID varchar(30)
   DECLARE @UtilityNumber varchar(100)
   DECLARE @AppWHTax int
   DECLARE @WHTaxMode char(1)
   DECLARE @WHTaxAmount money
   
   /* Get Cash Transaction */
   SELECT @vDate=ValueDate,@wDate=wDate,@RefNo=RefNo,@ChequeID= ChequeID,@AccountType=AccountType,@TrxType=TrxType,@Amount = Amount,
   @ForeignAmount = ForeignAmount, @ProductID = ProductID,@IsLocalCurrency=IsLocalCurrency,@Status = Supervision, @GLID = GlID,
   @AccountName=AccountName,@CurrencyID=CurrencyID,@ValueDate=ValueDate,@ChequeDate=ChequeDate,@ExchangeRate=ExchangeRate,
   @DescriptionID=DescriptionID,@Description=Description,@BankCode=BankCode,@BranchCode=BranchCode,@OperatorID=OperatorID,
   @UtilityNumber=UtilityNumber,@AddData=AdditionalData,@AppWHTax=AppWHTax,@WHTaxMode=WHTaxMode,@WHTaxAmount=WHTaxAmount
   FROM t_CashTransactionModel
   WHERE (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (ScrollNo = @ScrollNo)
   
   SELECT @mDate = Convert(varchar(10),@vDate ,101) + ' ' + convert(varchar(20),GetDate(),114)
   
   IF @AccountType = 'C'            
      BEGIN            
		 /* Get Current ClearBalance, Effective Balance and Limit */             
         SELECT @ClearBalance=ClearBalance,@Effects=Effects,@limit=Limit             
         FROM t_AccountBalance            
         WHERE (OurBranchID = @OurBranchID) AND (AccountID = @AccountID)
      END            
   ELSE            
      SELECT @ClearBalance=0,@Effects=0,@limit=0
      
   IF @Status = '*'            
      BEGIN            
         SELECT @Status='C'            
            
         /*UpDate Transaction File Status*/            
         UPDATE t_CashTransactionModel            
         SET Supervision = @Status, SupervisorID = @SupervisorID            
         WHERE (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (ScrollNo = @ScrollNo)            
             
         /* Create a New record For Superviser */            
         INSERT INTO t_SupervisedBy            
               (OurBranchID,AccountID,[Date],ScrollNo,ChequeID,ClearBalance,Effects,Limit,TrxType,Amount,Status,SupervisorID,TransactionCategory)            
         VALUES            
               (@OurBranchID,@AccountID,@mDate,@ScrollNo,@ChequeID,@ClearBalance,@Effects,@Limit,@TrxType,@Amount,@Status,@SupervisorID,'C')            
              
         IF @AccountType = 'C'            
            
            BEGIN /* Update Shadow Balance */            
               IF @TrxType='C' AND @IsLocalCurrency = 1            
                  Update t_AccountBalance            
                  Set --ShadowBalance = ShadowBalance - @Amount,
                  ClearBalance = ClearBalance + @Amount            
                  Where  OurBranchID = @OurBranchID and AccountID = @AccountID            
            
               ELSE IF @TrxType='C' AND @IsLocalCurrency = 0            
                  Update t_AccountBalance            
                  Set --ShadowBalance = ShadowBalance - @ForeignAmount,            
                        ClearBalance = ClearBalance + @ForeignAmount,
                        LocalClearBalance = LocalClearBalance + @Amount            
                  Where OurBranchID = @OurBranchID and AccountID = @AccountID            
                  
               ELSE IF @TrxType='D' AND @IsLocalCurrency = 1            
                  Update t_AccountBalance            
                  Set ShadowBalance = ShadowBalance + @Amount            
                     ,ClearBalance = ClearBalance - @Amount            
                  Where OurBranchID = @OurBranchID and AccountID = @AccountID            
   
               ELSE IF @TrxType='D' AND @IsLocalCurrency = 0            
                  Update t_AccountBalance            
                  Set ShadowBalance = ShadowBalance + @ForeignAmount            
                     ,ClearBalance = ClearBalance - @ForeignAmount,
                     LocalClearBalance = LocalClearBalance - @Amount          
                  Where OurBranchID = @OurBranchID and AccountID = @AccountID            
            
            END             
              
         /* Update Customer Status */            
       Update t_CustomerStatus            
         Set Status = @Status            
         Where (OurBranchID=@OurBranchID) and (AccountID = @AccountID)             
                  and (ScrollNo = @ScrollNo) and  (Left(TransactionType,1)='C')            
            
         /*@Update OtherTransaction Table For Cash*/            
         /*          
         Update t_OtherTransactionStatus            
         Set Status = @Status            
         Where (OurBranchID=@OurBranchID) and (AccountID = @AccountID) and (ScrollNo = @ScrollNo)             
               and  (Left(TransactionType,1)='C')  /*and (Left(RefNo,2) = 'FD')*/            
         */          
-----------------------------------------            
-----------------------------------------            
        
--Local Remittance Reject            
 Update t_LR_Issuance
 Set Supervision = @Status,      
 SuperviseBy = @SupervisorID,      
 SuperviseTime = @mDate      
 Where (OurBranchID=@OurBranchID) and (PaidScrollNo = @ScrollNo) and PaidMode = 'CH'    
 and  (convert(varchar(20), PaidDate, 101) = convert(varchar(20), @vDate, 101))      
    
-- and  (ScrollNo = @ScrollNo)      
-- and (convert(varchar(20), wDate, 101) = convert(varchar(20), @vDate, 101))       
            
/*        
--Local Remittance Advice Reject            
 Update t_LR_Advice            
 Set Supervision = @Status            
 Where (OurBranchID=@OurBranchID) and  (ScrollNo = @ScrollNo) and  (wDate = @vDate)            
-------------------------------------------------------------            
*/        
         
         select @VoucherID = ISNULL(MAX(VoucherID),0) + 1 from t_GLTransactions where OurBranchID = @OurBranchID
         
         declare @IsCredit int
         declare @TransactionMethod char(1)
         
         if @AccountType = 'C'
         begin
            
            insert into t_Transactions (OurBranchID, ScrollNo, SerialNo, RefNo, wDate, AccountType, DocumentType, AccountID, AccountName,
            ProductID, CurrencyID, ValueDate, TrxType, ChequeID, ChequeDate, Amount, ForeignAmount, ExchangeRate, ProfitLoss, MeanRate,
            DescriptionID, [Description], BankCode, BranchCode, TrxPrinted, GLID, [Status], IsLocalCurrency, OperatorID,
            SupervisorID, UtilityNumber, AdditionalData, IsMainTrx, DocType, GLVoucherID, AppWHTax, WHTaxMode, WHTaxAmount)
            
            VALUES (@OurBranchID,@ScrollNo,'0',@RefNo,@wDate,@AccountType,'C',@AccountID,@AccountName,
            @ProductID,@CurrencyID,@ValueDate,@TrxType,@ChequeID,@ChequeDate, @Amount, @ForeignAmount,@ExchangeRate,0,@ExchangeRate,
            @DescriptionID,@Description,@BankCode,@BranchCode, 0, @GLID, 'C', @IsLocalCurrency, @OperatorID, @SupervisorID, 
            @UtilityNumber,@AddData,'1','C', @VoucherID, @AppWHTax, @WHTaxMode, @WHTaxAmount)
            
            if @TrxType = 'C'
            begin
              set @IsCredit = 1
              set @DescriptionID = 'CA0'
              set @Description = 'Account ID: ' + @AccountID +', Account Name: ' + @AccountName +' , Module: Cash Credit Entry...'
            end
            else
            begin
              set @IsCredit = 0
              set @DescriptionID = 'CA1'
              set @Description = 'Account ID: ' + @AccountID +', Account Name: ' + @AccountName +' , Module: Cash Debit Entry...'
            end
            
            SELECT @GLControl = ISNULL(GLControl, '') from t_Products where OurBranchID = @OurBranchID and ProductID = @ProductID
            
            if @IsLocalCurrency = 1
            begin
              set @ForeignAmount = 0
              set @ExchangeRate = 1
              set @TransactionMethod = 'L'
            end
            else
            begin
              set @TransactionMethod = 'A'
            end
            
            insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],
            CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,
            Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType)
            
            values (@OurBranchID, @GLControl, @VoucherID, '1', @wDate, @ValueDate, @DescriptionID, @Description,
            @CurrencyID, @Amount, @ForeignAmount, @ExchangeRate, @IsCredit, 'Cash', @TransactionMethod, @OperatorID, @SupervisorID,
            '', '0', '0', @AddData, '0', 'C')
            
            if @TrxType = 'C'
            begin
              
              set @IsCredit = 0
              if (left(@RefNo,9) = 'LOCALCASH')
              begin
                set @DescriptionID = 'CA3'
                set @TransactionMethod = 'L'
              end
              else
              begin
                set @DescriptionID = 'CA4'
              end
              
            end
            else
            begin
              set @IsCredit = 1
              set @DescriptionID = 'CA2'
            end
            
            insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],
            CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,
            Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType)
            
            values (@OurBranchID, @GLID, @VoucherID, '2', @wDate, @ValueDate, @DescriptionID, @Description,
            @CurrencyID, @Amount, @ForeignAmount, @ExchangeRate, @IsCredit, 'Cash', @TransactionMethod, @OperatorID, @SupervisorID,
            '', '0', '0', @AddData, '0', 'C')
            
         end
         else
         begin
            
            if @TrxType = 'C'
              set @IsCredit = 1
            else
              set @IsCredit = 0
            
            if @IsLocalCurrency = 1
            begin
              set @ForeignAmount = 0
              set @ExchangeRate = 1
              set @TransactionMethod = 'L'
            end
            else
            begin
              set @TransactionMethod = 'A'
            end
            
            insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],
            CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,
            Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType, GLID)
            
            values (@OurBranchID, @AccountID, @VoucherID, '1', @wDate, @ValueDate, @DescriptionID, @Description,
            @CurrencyID, @Amount, @ForeignAmount, @ExchangeRate, @IsCredit, 'Cash', @TransactionMethod, @OperatorID, @SupervisorID,
            '', @ScrollNo, '0', @AddData, '1', 'C', @GLID)
            
            if @TrxType = 'C'
            begin
              
              set @IsCredit = 0
              set @Description = 'Account ID: ' + @AccountID +', Account Name: ' + @AccountName +' , Module: Cash Debit Entry...'
              if (left(@RefNo,9) = 'LOCALCASH')
              begin
                set @DescriptionID = 'CA3'
                set @TransactionMethod = 'L'
              end
              else
              begin
                set @DescriptionID = 'CA4'
              end
              
            end
            else
            begin
              set @IsCredit = 1
              set @DescriptionID = 'CA2'
              set @Description = 'Account ID: ' + @AccountID +', Account Name: ' + @AccountName +' , Module: Cash Debit Entry...'
            end
            
            insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],
            CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,
            Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType)
            
            values (@OurBranchID, @GLID, @VoucherID, '2', @wDate, @ValueDate, @DescriptionID, @Description,
            @CurrencyID, @Amount, @ForeignAmount, @ExchangeRate, @IsCredit, 'Cash', @TransactionMethod, @OperatorID, @SupervisorID,
            '', '0', '0', @AddData, '0', 'C')
            
         end
         
         SELECT '0' as RetStatus
         RETURN            
      END            
            
   ELSE            
      BEGIN            
         IF @Status = 'C'            
            BEGIN            
               SELECT '2' as RetStatus            
               RETURN            
            END            
            
         IF @Status = 'R'            
            BEGIN            
               SELECT '3' as RetStatus            
               RETURN            
         END            
            
         SELECT '4' as RetStatus            
         RETURN            
      END