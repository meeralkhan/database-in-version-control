﻿create procedure [dbo].[spc_FacilitiesByCIF]
(
   @OurBranchID nvarchar(30),
   @ClientID nvarchar(30)
)

AS

SET NOCOUNT ON

select ReferenceNo AS FacilityId, Status
from v_Facilities
where OurBranchID = @OurBranchID
AND ClientID = @ClientID
and ((Status = 'E') or ((Status = '' and Status2 = '') or (Status = 'C' and Status2 = 'A') or (Status = 'B' and Status2 = 'A')))