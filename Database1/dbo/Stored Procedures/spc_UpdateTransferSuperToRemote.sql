﻿CREATE PROCEDURE [dbo].[spc_UpdateTransferSuperToRemote]  
(              
 @OurBranchID varchar(30),              
 @AccountID varchar(30),              
 @SupervisorID varchar(30),              
 @ScrollNo money,              
 @SerialNo money            
)              
              
AS              
   SET NOCOUNT ON              
              
   DECLARE @vDate datetime               
   DECLARE @wDate datetime               
   DECLARE @mDate Varchar(32)              
   DECLARE @ChequeID varchar(11)              
   DECLARE @ClearBalance money              
   DECLARE @Effects money              
   DECLARE @Limit money              
   DECLARE @TrxType char(1)              
   DECLARE @Amount money              
   DECLARE @ForeignAmount money              
   DECLARE @IsLocalCurrency bit              
   DECLARE @Status char(1)              
   DECLARE @AccountType char(1)               
              
 /* Get Transfer Transaction */              
   SELECT @wDate=wDate, @vDate=ValueDate,@ChequeID= ChequeID,@AccountType=AccountType,@TrxType=TrxType              
         ,@Amount = Amount, @ForeignAmount = ForeignAmount,              
         @IsLocalCurrency=IsLocalCurrency,@Status = Supervision                
   FROM t_TransferTransactionModel              
   WHERE (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (ScrollNo = @ScrollNo) and (SerialNo = @SerialNo)               
              
   SELECT @mDate = Convert(varchar(10),@wDate ,101) + ' ' + convert(varchar(20),GetDate(),114)              
              
   IF @AccountType = 'C'              
      BEGIN              
    /* Get Current ClearBalance, Effective Balance and Limit */               
         SELECT @ClearBalance=ClearBalance,@Effects=Effects,@limit=Limit               
         FROM t_AccountBalance              
         WHERE (OurBranchID = @OurBranchID) AND (AccountID = @AccountID)               
      END              
   ELSE              
      SELECT @ClearBalance=0,@Effects=0,@limit=0              
              
              
   IF @Status = '*'              
      BEGIN              
         SELECT @Status='C'              
              
         /*UpDate Transaction File Status*/              
         UPDATE t_TransferTransactionModel              
         SET Supervision = 'P'            
         WHERE (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (ScrollNo = @ScrollNo)  and (SerialNo = @SerialNo)               
               
         /* Create a New record For Superviser */              
         INSERT INTO t_SupervisedBy              
               (OurBranchID,AccountID,[Date],ScrollNo,ChequeID,ClearBalance,Effects,Limit,TrxType,Amount,Status,SupervisorID,TransactionCategory)              
         VALUES              
               (@OurBranchID,@AccountID,@mDate,@ScrollNo,@ChequeID,@ClearBalance,@Effects,@Limit,@TrxType,@Amount,@Status,@SupervisorID,'T')              
                
         /* Update Customer Status */              
         Update t_CustomerStatus              
         Set Status = 'P', IsOverDraft = '1'        
         Where (OurBranchID=@OurBranchID) and (AccountID = @AccountID)               
                  and (ScrollNo = @ScrollNo) and  (Left(TransactionType,1)='T')  and (SerialNo = @SerialNo)               
              
         /** rameez starts **            
                     
         /*@Update OtherTransaction Table For Transfer*/              
         Update t_OtherTransactionStatus              
         Set Status = @Status              
         Where (OurBranchID=@OurBranchID) and (AccountID = @AccountID) and (ScrollNo = @ScrollNo)               
               and  (Left(TransactionType,1)='T')  and (SerialNo = @SerialNo)               
                     
         ** rameez endss **/            
             
         --Local Remittance Reject                    
         Update t_LR_Issuance Set        
         Supervision = 'P',        
         SuperviseBy = @SupervisorID,        
         SuperviseTime = @mDate       
         Where (OurBranchID=@OurBranchID) and  (PaidScrollNo = @ScrollNo)        
         and (CONVERT(varchar(20), wDate, 101) = @vDate) AND PaidMode = 'TR'    
           
         Update t_LR_Issuance Set        
         Supervision = 'P',        
         SuperviseBy = @SupervisorID,        
         SuperviseTime = @mDate        
         Where (OurBranchID=@OurBranchID) and  (CancellScrollNo = @ScrollNo) AND Cancel = '1'  
         and CONVERT(varchar(20), CancelDate, 101) = convert(varchar(20), @vDate, 101)  
             
         SELECT '0' as RetStatus              
         RETURN              
      END              
   ELSE              
      BEGIN              
         IF @Status = 'C'              
            BEGIN              
               SELECT '2' as RetStatus              
               RETURN              
            END              
              
         IF @Status = 'R'              
            BEGIN              
               SELECT '3' as RetStatus              
               RETURN              
         END              
              
         SELECT '4' as RetStatus              
         RETURN              
      END