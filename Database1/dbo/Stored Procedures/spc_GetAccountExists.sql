﻿CREATE   PROCEDURE [dbo].[spc_GetAccountExists]          
(                
 @OurBranchID  varchar(10),                
 @AccountID varchar(30),                
 @AccountType varchar(1)=''                
)                
                
AS                
                
   SET NOCOUNT ON                
                
    DECLARE @LocalCurrency as Varchar(4)                   
    DECLARE @StatementFrequency as Char(1)                
    DECLARE @IsControlAccount as int                
                  
    SELECT @StatementFrequency = 'N', @IsControlAccount = 0              
                  
    SELECT @LocalCurrency =UPPER(LocalCurrency) From t_GlobalVariables where OurBranchID = @OurBranchID          
                  
    IF @AccountType = 'C'          
      BEGIN                  
         IF (Select Count(AccountID) From t_AccountBalance WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID) > 0           
            BEGIN              
               SELECT @StatementFrequency = IsNull(StatementFrequency,'N') From t_Account           
               WHERE  OurBranchID = @OurBranchID AND AccountID = @AccountID              
                             
               SELECT '1' as RetStatus, A.Name, A.ProductID, AB.CurrencyID, A.ClearBalance, A.Effects, AB.OpenDate,               
               CASE P.CurrencyID WHEN @LocalCurrency THEN 1 ELSE 0 END as IsLocalCurrency,              
               @StatementFrequency StatementFrequency, @IsControlAccount IsControlAccount              
                             
               FROM t_AccountBalance as A              
                             
               INNER JOIN t_Account as AB ON A.OurBranchID = AB.OurBranchID AND A.AccountID = AB.AccountID              
                           
               INNER JOIN t_Products as P ON A.OurBranchID = P.OurBranchID AND A.ProductID = P.ProductID              
                             
               WHERE A.OurBranchID = @OurBranchID AND A.AccountID = @AccountID              
                             
            END                
         ELSE                
            BEGIN                
                SELECT '0' AS RetStatus,'' as Name,0 as IsLocalCurrency     
       --Account Does Not Exists              
               RETURN(0)                
            END                
      END                
    ELSE              
      BEGIN              
         IF (Select Count(AccountID) From t_GL WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID) > 0                 
            BEGIN              
                             
               SELECT @IsControlAccount = Count(*) From t_Products              
               WHERE OurBranchID = @OurBranchID AND GlControl = @AccountID              
                             
               SELECT '1' as RetStatus, Description as Name,'GL' as  ProductID, CurrencyID, GETDATE() AS OpenDate,               
               CASE CurrencyID WHEN @LocalCurrency THEN Balance ELSE ForeignBalance END as ClearBalance,               
               0 as Effects, CASE CurrencyID WHEN @LocalCurrency THEN 1 ELSE 0 END as IsLocalCurrency,               
               @StatementFrequency StatementFrequency, @IsControlAccount IsControlAccount              
                             
               FROM t_GL              
                             
               WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID              
            END                
         ELSE                
            BEGIN                
               SELECT '0' AS RetStatus,'' as Name,0 as IsLocalCurrency     
          
       --Account Does Not Exists                
               RETURN(0)                
            END                
      END