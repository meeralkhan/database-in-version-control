﻿CREATE PROCEDURE [dbo].[spc_GetTDRProfitCalc]    
(    
  @OurBranchID varchar(30),    
  @RecieptID varchar(30)    
)    
    
AS    
    
SET NOCOUNT ON    
    
BEGIN    
      
  DECLARE @StartDate DATETIME    
  DECLARE @WorkingDate DATETIME    
  DECLARE @ProductID varchar(30)    
  DECLARE @Amount money    
      
  SELECT @WorkingDate = WorkingDate FROM t_Last  WHERE OurBranchID = @OurBranchID  
      
  select @StartDate = ISNULL(InterestPaidUpto,StartDate),    
  @ProductID = ProductID, @Amount = Amount    
  from t_TDRIssuance where OurBranchID = @OurBranchID AND RecieptID = @RecieptID    
      
  DECLARE @TheDate DateTime    
  DECLARE @Rate Money    
  DECLARE @TablesToDelete CURSOR    
      
  SET @TablesToDelete = CURSOR FOR SELECT TheDate FROM fn_ExplodeDates(@StartDate, @WorkingDate)    
  OPEN @TablesToDelete    
      
  FETCH NEXT    
  FROM @TablesToDelete INTO @TheDate    
      
  TRUNCATE TABLE t_TestTDR    
      
  WHILE @@FETCH_STATUS = 0    
  BEGIN    
        
    set @Rate = '-'    
        
    select @Rate = ISNULL(MonthRate,'-') from t_DepositRates    
    where OurBranchID = @OurBranchID and ProductID = @ProductID and @Amount BETWEEN MinAmount AND MaxAmount    
    and @TheDate >= EffectiveDate    
        
    IF @Rate = '-'    
      select @Rate = Rate from t_TDRIssuance where OurBranchID = @OurBranchID and ProductID = @ProductID and RecieptID = @RecieptID    
        
    insert into t_TestTDR values (@TheDate, @Rate)    
        
    FETCH NEXT    
    FROM @TablesToDelete INTO @TheDate    
  END    
      
  CLOSE @TablesToDelete    
  DEALLOCATE @TablesToDelete    
      
  SELECT 'Ok' RetStatus, * FROM t_TestTDR    
      
END