﻿CREATE PROCEDURE [dbo].[spc_RptGetStatementAccounts]                    
@OurBranchID varchar(30),
@AccountFrom varchar(30),
@AccountTo varchar(30),                      
@AccountType varchar(2),
@DropZero bit                    
AS                      
DECLARE @LocalCurrency varchar(6)                    
DECLARE @BankID varchar(4)                      
 SELECT @LocalCurrency=LocalCurrency, @BankID=OurBankID FROM t_GlobalVariables WHERE OurBranchID = @OurBranchID    
 IF @AccountType = 'C'                      
  BEGIN                         
   IF @DropZero = 1                         
    SELECT t_Account.AccountID, t_Account.Name AS AccountName, t_Account.Address,           
    t_Products.CurrencyID, t_Currencies.Description AS CurrencyName,           
    t_Products.Description AS ProductName,t_Account.OpenDate,                          
    IsNULL(t_Account.StatementFrequency,'N') StatementFrequency , 'C' AccountType                         
    FROM t_Account                        
    INNER JOIN t_AccountBalance ON t_Account.OurBranchID = t_AccountBalance.OurBranchID AND           
    t_Account.AccountID = t_AccountBalance.AccountID                        
    INNER JOIN t_Products ON t_AccountBalance.OurBranchID = t_Products.OurBranchID           
    AND t_AccountBalance.ProductID = t_Products.ProductID                        
    INNER JOIN t_Currencies ON t_Products.OurBranchID = t_Currencies.OurBranchID AND           
    t_Products.CurrencyID = t_Currencies.CurrencyID                         
    WHERE ( t_Account.AccountID BETWEEN @AccountFrom AND @AccountTo)                           
    AND t_Account.OurBranchID = @OurBranchID AND (ClearBalance + Effects) <> 0  ORDER BY  t_Account.AccountID                      
   ELSE           
       SELECT t_Account.AccountID, t_Account.Name AS AccountName, t_Account.Address,           
       t_Products.CurrencyID, t_Currencies.Description AS CurrencyName,           
       t_Products.Description AS ProductName,t_Account.OpenDate,           
       IsNULL(t_Account.StatementFrequency,'N') StatementFrequency , 'C' AccountType                         
       FROM t_Account           
       INNER JOIN t_AccountBalance ON t_Account.OurBranchID = t_AccountBalance.OurBranchID AND                              
       t_Account.AccountID = t_AccountBalance.AccountID           
       INNER JOIN t_Products ON t_AccountBalance.OurBranchID = t_Products.OurBranchID AND           
       t_AccountBalance.ProductID = t_Products.ProductID           
       INNER JOIN t_Currencies ON t_Products.OurBranchID = t_Currencies.OurBranchID AND                              
       t_Products.CurrencyID = t_Currencies.CurrencyID                         
       WHERE (t_Account.AccountID BETWEEN @AccountFrom AND @AccountTo)                           
       AND t_Account.OurBranchID = @OurBranchID ORDER BY t_Account.AccountID                         
    END                       
 ELSE                       
 BEGIN            
  IF @DropZero = 1                         
   SELECT t_GL.AccountID,t_GL.Description AS AccountName, t_branches.Name AS Address,           
   t_Currencies.CurrencyID,t_Currencies.Description AS CurrencyName,  'GL' AS ProductName,'' as OpenDate ,           
   'N' StatementFrequency  , 'G' AccountType                         
   FROM t_GL           
   INNER JOIN t_Currencies ON t_GL.OurBranchID = t_Currencies.OurBranchID AND           
   t_GL.CurrencyID = t_Currencies.CurrencyID           
   INNER JOIN t_branches ON t_Currencies.OurBranchID = t_branches.OurBranchID AND     
   t_Currencies.OurBranchID = t_branches.BranchID     
   WHERE (AccountID BETWEEN @AccountFrom AND @AccountTo) AND t_GL.OurBranchID = @OurBranchID AND           
   BankID =  @BankID AND Balance <> 0           
   ORDER BY AccountID                        
  ELSE                         
   SELECT t_GL.AccountID,t_GL.Description AS AccountName, t_branches.Name AS Address,           
   t_Currencies.CurrencyID, t_Currencies.Description AS CurrencyName,  'GL' AS ProductName,'' as OpenDate,           
   'N' StatementFrequency  , 'G' AccountType                         
   FROM t_GL           
   INNER JOIN t_Currencies ON t_GL.OurBranchID = t_Currencies.OurBranchID AND                             
   t_GL.CurrencyID = t_Currencies.CurrencyID           
   INNER JOIN t_branches ON t_Currencies.OurBranchID = t_branches.OurBranchID AND     
   t_Currencies.OurBranchID = t_branches.BranchID     
   WHERE (AccountID BETWEEN @AccountFrom AND @AccountTo)                 
   AND t_GL.OurBranchID = @OurBranchID AND BankID =  @BankID ORDER BY AccountID                       
  END