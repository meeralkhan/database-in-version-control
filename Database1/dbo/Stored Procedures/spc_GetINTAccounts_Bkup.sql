﻿    
CREATE PROCEDURE [dbo].[spc_GetINTAccounts_Bkup]  
(        
 @OurBranchID varchar(30),        
 @ProductID varchar(30)='',        
 @FromAccount varchar(30)='',        
 @ToAccount varchar(30)=''        
)      
AS      
BEGIN      
 
 set nocount on
 
 IF @FromAccount<>'' AND @ToAccount<>''      
  BEGIN        
   IF @ProductID <> ''      
    BEGIN        
     SELECT a.OurBranchID, a.AccountID, a.ProductID, p.[Description] as ProductDesc, a.ClientID, a.Name,     
     a.CurrencyID, a.OpenDate, a.CloseDate      
     FROM t_ACCOUNT a    
     INNER JOIN t_Products p on a.OurBranchID=p.OurBranchID AND a.ProductID=p.ProductID    
     WHERE a.OurBranchID=@OurBranchID and a.ProductID=@ProductID      
     AND isnull(a.[Status],'')=''      
     AND cast(a.AccountID as bigint) BETWEEN @FromAccount AND @ToAccount        
    END        
  END        
        
 ELSE        
        
  BEGIN        
   IF @ProductID <> ''          
    BEGIN        
     SELECT a.OurBranchID, a.AccountID, a.ProductID, p.[Description] as ProductDesc, a.ClientID, a.Name,     
     a.CurrencyID, a.OpenDate, a.CloseDate      
     FROM t_ACCOUNT a    
     INNER JOIN t_Products p on a.OurBranchID=p.OurBranchID AND a.ProductID=p.ProductID    
     WHERE a.OurBranchID=@OurBranchID and a.ProductID=@ProductID      
     AND isnull(a.[Status],'')=''      
    END        
  END        
END