﻿  
CREATE PROCEDURE [dbo].[spc_GetTotalCashTeller]      
 @ourBranchID varchar(4),        
 @OperatorID varchar(8)='',        
 @CurrencyID varchar(4) =''        
AS        
    
SET NOCOUNT ON    
    
--declare local variable        
DECLARE @totalLCredit money,@totalLDebit  money, @totalLCreditCount int, @totalLDebitCount int        
DECLARE @totalULCredit money, @totalULDebit  money, @totalULCreditCount int, @totalULDebitCount int        
DECLARE @totalRLCredit money, @totalRLDebit  money, @totalRLCreditCount int, @totalRLDebitCount int        
        
DECLARE @totalFCredit money, @totalFDebit  money, @totalFCreditCount int, @totalFDebitCount int        
DECLARE @totalUFCredit money, @totalUFDebit  money, @totalUFCreditCount int, @totalUFDebitCount int        
DECLARE @totalRFCredit money, @totalRFDebit  money, @totalRFCreditcount int, @totalRFDebitcount int        
        
        
 if @OperatorID = ''        
 BEGIN        
  if @CurrencyID =''        
  BEGIN        
     
   Select @totalLCredit = sum(Amount), @totalLCreditCount = sum(Counts) From (  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and IsLocalCurrency=1 and trxtype='C' and supervision ='C'  
    UNION ALL  
    Select isnull(sum(NetAmount),0) Amount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and SellOrBuy = 'B' and Supervision ='C'  
   ) AS SumCash  
     
   Select @totalLDebit = sum(Amount), @totalLDebitCount = sum(Counts) From (  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and IsLocalCurrency=1 and trxtype='D' and supervision ='C'  
    UNION ALL  
    Select isnull(sum(NetAmount),0) Amount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and SellOrBuy = 'S' and Supervision ='C'  
   ) AS SumCash  
     
   Select @totalULCredit = sum(Amount), @totalULCreditCount = sum(Counts) From (  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and IsLocalCurrency=1 and trxtype='C' and supervision in ('P','*')  
    UNION ALL  
    Select isnull(sum(NetAmount),0) Amount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and SellOrBuy = 'B' and Supervision ='*'  
   ) AS SumCash  
     
   Select @totalULDebit = sum(Amount), @totalULDebitCount = sum(Counts) From (  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and IsLocalCurrency=1 and trxtype='D' and supervision in ('P','*')  
    UNION ALL  
    Select isnull(sum(NetAmount),0) Amount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and SellOrBuy = 'S' and Supervision ='*'  
   ) AS SumCash  
     
   Select @totalRLCredit = sum(Amount), @totalRLCreditCount = sum(Counts) From (  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and IsLocalCurrency=1 and trxtype='C' and supervision = 'R'  
    UNION ALL  
    Select isnull(sum(NetAmount),0) Amount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and SellOrBuy = 'B' and Supervision ='R'  
   ) AS SumCash  
     
   Select @totalRLDebit = sum(Amount), @totalRLDebitCount = sum(Counts) From (  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and IsLocalCurrency=1 and trxtype='D' and supervision = 'R'  
    UNION ALL  
    Select isnull(sum(NetAmount),0) Amount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and SellOrBuy = 'S' and Supervision ='R'  
   ) AS SumCash  
     
  END        
  else        
  BEGIN        
     
   Select @totalLCredit = sum(Amount), @totalLCreditCount = sum(Counts) From (  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and trxtype='C' and supervision ='C'  
    UNION ALL  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and SellOrBuy = 'S' and Supervision ='C'  
   ) AS SumCash  
     
   Select @totalLDebit = sum(Amount), @totalLDebitCount = sum(Counts) From (  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and trxtype='D' and supervision ='C'  
    UNION ALL  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and SellOrBuy = 'B' and Supervision ='C'  
   ) AS SumCash  
     
   Select @totalULCredit = sum(Amount), @totalULCreditCount = sum(Counts) From (  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and trxtype='C' and supervision in ('P','*')  
    UNION ALL  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and SellOrBuy = 'S' and Supervision ='*'  
   ) AS SumCash  
     
   Select @totalULDebit = sum(Amount), @totalULDebitCount = sum(Counts) From (  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and trxtype='D' and supervision in ('P','*')  
    UNION ALL  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and SellOrBuy = 'B' and Supervision ='*'  
   ) AS SumCash  
     
   Select @totalRLCredit = sum(Amount), @totalRLCreditCount = sum(Counts) From (  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and trxtype='C' and supervision = 'R'  
    UNION ALL  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and SellOrBuy = 'S' and Supervision ='R'  
   ) AS SumCash  
     
   Select @totalRLDebit = sum(Amount), @totalRLDebitCount = sum(Counts) From (  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and trxtype='D' and supervision = 'R'  
    UNION ALL  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and SellOrBuy = 'B' and Supervision ='R'  
   ) AS SumCash  
     
   Select @totalFCredit = sum(ForeignAmount), @totalFCreditCount = sum(Counts) From (  
    Select isnull(sum(ForeignAmount),0) ForeignAmount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and trxtype='C' and supervision ='C' and upper(left(refno,9))<>'LOCALCASH'  
    UNION ALL  
    Select isnull(sum(ForeignAmount),0) ForeignAmount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and SellOrBuy = 'S' and Supervision ='C'  
   ) AS SumCash  
     
   Select @totalFDebit = sum(ForeignAmount), @totalFDebitCount = sum(Counts) From (  
    Select isnull(sum(ForeignAmount),0) ForeignAmount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and trxtype='D' and supervision ='C' and upper(left(refno,9))<>'LOCALCASH'  
    UNION ALL  
    Select isnull(sum(ForeignAmount),0) ForeignAmount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and SellOrBuy = 'B' and Supervision ='C'  
   ) AS SumCash  
     
   Select @totalUFCredit = sum(ForeignAmount), @totalUFCreditCount = sum(Counts) From (  
    Select isnull(sum(ForeignAmount),0) ForeignAmount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and trxtype='C' and supervision in ('P','*') and upper(left(refno,9))<>'LOCALCASH'  
    UNION ALL  
    Select isnull(sum(ForeignAmount),0) ForeignAmount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and SellOrBuy = 'S' and Supervision ='*'  
   ) AS SumCash  
     
   Select @totalUFDebit = sum(ForeignAmount), @totalUFDebitCount = sum(Counts) From (  
    Select isnull(sum(ForeignAmount),0) ForeignAmount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and trxtype='D' and supervision in ('P','*') and upper(left(refno,9))<>'LOCALCASH'  
    UNION ALL  
    Select isnull(sum(ForeignAmount),0) ForeignAmount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and SellOrBuy = 'B' and Supervision ='*'  
   ) AS SumCash  
     
   Select @totalRFCredit = sum(ForeignAmount), @totalRFCreditCount = sum(Counts) From (  
    Select isnull(sum(ForeignAmount),0) ForeignAmount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and trxtype='C' and supervision = 'R' and upper(left(refno,9))<>'LOCALCASH'  
    UNION ALL  
    Select isnull(sum(ForeignAmount),0) ForeignAmount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and SellOrBuy = 'S' and Supervision ='R'  
   ) AS SumCash  
     
   Select @totalRFDebit = sum(ForeignAmount), @totalRFDebitcount = sum(Counts) From (  
    Select isnull(sum(ForeignAmount),0) ForeignAmount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and trxtype='D' and supervision = 'R' and upper(left(refno,9))<>'LOCALCASH'  
    UNION ALL  
    Select isnull(sum(ForeignAmount),0) ForeignAmount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and SellOrBuy = 'B' and Supervision ='R'  
   ) AS SumCash  
     
  END        
 END        
 else        
 BEGIN        
  if @CurrencyID =''        
  BEGIN         
     
   Select @totalLCredit = sum(Amount), @totalLCreditCount = sum(Counts) From (  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and IsLocalCurrency=1 and trxtype='C' and supervision ='C'  
    UNION ALL  
    Select isnull(sum(NetAmount),0) Amount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and SellOrBuy = 'B' and Supervision ='C'  
   ) AS SumCash  
     
   Select @totalLDebit = sum(Amount), @totalLDebitCount = sum(Counts) From (  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and IsLocalCurrency=1 and trxtype='D' and supervision ='C'  
    UNION ALL  
    Select isnull(sum(NetAmount),0) Amount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and SellOrBuy = 'S' and Supervision ='C'  
   ) AS SumCash  
     
   Select @totalULCredit = sum(Amount), @totalULCreditCount = sum(Counts) From (  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and IsLocalCurrency=1 and trxtype='C' and supervision in ('P','*')  
    UNION ALL  
    Select isnull(sum(NetAmount),0) Amount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and SellOrBuy = 'B' and Supervision ='*'  
   ) AS SumCash  
     
   Select @totalULDebit = sum(Amount), @totalULDebitCount = sum(Counts) From (  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and IsLocalCurrency=1 and trxtype='D' and supervision in ('P','*')  
    UNION ALL  
    Select isnull(sum(NetAmount),0) Amount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and SellOrBuy = 'S' and Supervision ='*'  
   ) AS SumCash  
     
   Select @totalRLCredit = sum(Amount), @totalRLCreditCount = sum(Counts) From (  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and IsLocalCurrency=1 and trxtype='C' and supervision = 'R'  
    UNION ALL  
    Select isnull(sum(NetAmount),0) Amount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and SellOrBuy = 'B' and Supervision ='R'  
   ) AS SumCash  
     
   Select @totalRLDebit = sum(Amount), @totalRLDebitCount = sum(Counts) From (  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and IsLocalCurrency=1 and trxtype='D' and supervision = 'R'  
    UNION ALL  
    Select isnull(sum(NetAmount),0) Amount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and SellOrBuy = 'S' and Supervision ='R'  
   ) AS SumCash  
     
  END        
  else        
  BEGIN        
     
   Select @totalLCredit = sum(Amount), @totalLCreditCount = sum(Counts) From (  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and trxtype='C' and supervision ='C' and OperatorID = @OperatorID  
    UNION ALL  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and SellOrBuy = 'S' and Supervision ='C' and OperatorID = @OperatorID  
   ) AS SumCash  
     
   Select @totalLDebit = sum(Amount), @totalLDebitCount = sum(Counts) From (  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and trxtype='D' and supervision ='C' and OperatorID = @OperatorID  
    UNION ALL  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and SellOrBuy = 'B' and Supervision ='C' and OperatorID = @OperatorID  
   ) AS SumCash  
     
   Select @totalULCredit = sum(Amount), @totalULCreditCount = sum(Counts) From (  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and trxtype='C' and supervision in ('P','*') and OperatorID = @OperatorID  
    UNION ALL  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and SellOrBuy = 'S' and Supervision ='*' and OperatorID = @OperatorID  
   ) AS SumCash  
     
   Select @totalULDebit = sum(Amount), @totalULDebitCount = sum(Counts) From (  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and trxtype='D' and supervision in ('P','*') and OperatorID = @OperatorID  
    UNION ALL  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and SellOrBuy = 'B' and Supervision ='*' and OperatorID = @OperatorID  
   ) AS SumCash  
     
   Select @totalRLCredit = sum(Amount), @totalRLCreditCount = sum(Counts) From (  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and trxtype='C' and supervision = 'R' and OperatorID = @OperatorID  
    UNION ALL  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and SellOrBuy = 'S' and Supervision ='R' and OperatorID = @OperatorID  
   ) AS SumCash  
     
   Select @totalRLDebit = sum(Amount), @totalRLDebitCount = sum(Counts) From (  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and trxtype='D' and supervision = 'R' and OperatorID = @OperatorID  
    UNION ALL  
    Select isnull(sum(Amount),0) Amount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and SellOrBuy = 'B' and Supervision ='R' and OperatorID = @OperatorID  
   ) AS SumCash  
     
   Select @totalFCredit = sum(ForeignAmount), @totalFCreditCount = sum(Counts) From (  
    Select isnull(sum(ForeignAmount),0) ForeignAmount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and trxtype='C' and supervision ='C' and upper(left(refno,9))<>'LOCALCASH' and OperatorID = @OperatorID  
    UNION ALL  
    Select isnull(sum(ForeignAmount),0) ForeignAmount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and SellOrBuy = 'S' and Supervision ='C' and OperatorID = @OperatorID  
   ) AS SumCash  
     
   Select @totalFDebit = sum(ForeignAmount), @totalFDebitCount = sum(Counts) From (  
    Select isnull(sum(ForeignAmount),0) ForeignAmount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and trxtype='D' and supervision ='C' and upper(left(refno,9))<>'LOCALCASH' and OperatorID = @OperatorID  
    UNION ALL  
    Select isnull(sum(ForeignAmount),0) ForeignAmount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and SellOrBuy = 'B' and Supervision ='C' and OperatorID = @OperatorID  
   ) AS SumCash  
     
   Select @totalUFCredit = sum(ForeignAmount), @totalUFCreditCount = sum(Counts) From (  
    Select isnull(sum(ForeignAmount),0) ForeignAmount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and trxtype='C' and supervision in ('P','*') and upper(left(refno,9))<>'LOCALCASH' and OperatorID = @OperatorID  
    UNION ALL  
    Select isnull(sum(ForeignAmount),0) ForeignAmount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and SellOrBuy = 'S' and Supervision ='*' and OperatorID = @OperatorID  
   ) AS SumCash  
     
   Select @totalUFDebit = sum(ForeignAmount), @totalUFDebitCount = sum(Counts) From (  
    Select isnull(sum(ForeignAmount),0) ForeignAmount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and trxtype='D' and supervision in ('P','*') and upper(left(refno,9))<>'LOCALCASH' and OperatorID = @OperatorID  
    UNION ALL  
    Select isnull(sum(ForeignAmount),0) ForeignAmount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and SellOrBuy = 'B' and Supervision ='*' and OperatorID = @OperatorID  
   ) AS SumCash  
     
   Select @totalRFCredit = sum(ForeignAmount), @totalRFCreditCount = sum(Counts) From (  
    Select isnull(sum(ForeignAmount),0) ForeignAmount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and trxtype='C' and supervision = 'R' and upper(left(refno,9))<>'LOCALCASH' and OperatorID = @OperatorID  
    UNION ALL  
    Select isnull(sum(ForeignAmount),0) ForeignAmount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and SellOrBuy = 'S' and Supervision ='R' and OperatorID = @OperatorID  
   ) AS SumCash  
     
   Select @totalRFDebit = sum(ForeignAmount), @totalRFDebitcount = sum(Counts) From (  
    Select isnull(sum(ForeignAmount),0) ForeignAmount, count(*) Counts From t_CashTransactionModel  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and trxtype='D' and supervision = 'R' and upper(left(refno,9))<>'LOCALCASH' and OperatorID = @OperatorID  
    UNION ALL  
    Select isnull(sum(ForeignAmount),0) ForeignAmount, count(*) Counts From t_BureaDeChange  
    Where OurBranchID = @ourBranchID and currencyid=@CurrencyID and SellOrBuy = 'B' and Supervision ='R' and OperatorID = @OperatorID  
   ) AS SumCash  
     
  END        
 END        
        
select  
isnull(@totalLCredit,0) as TotalLocalCredit, isnull(@totalLDebit,0) as TotalLocalDebit,  
isnull(@totalLCreditCount,0) + isnull(@totalLDebitCount,0) as LocalCountCleared,  
  
isnull(@totalULCredit,0) as TotalUnsLocalCredit , isnull(@totalULDebit,0) as TotalUnsLocalDebit,  
isnull(@totalULCreditCount,0) + isnull(@totalULDebitCount,0) as LocalCountUnsupervisd,  
  
isnull(@totalRLCredit,0) as TotalRejLocalCredit , isnull(@totalRLDebit,0) as TotalRejLocalDebit,  
isnull(@totalRLCreditCount,0) + isnull(@totalRLDebitCount,0) as LocalCountRejected,  
  
isnull(@totalFCredit,0) as TotalForeignCredit , isnull(@totalFDebit,0) as TotalForeignDebit,  
isnull(@totalFCreditCount,0) + isnull(@totalFDebitCount,0) as ForeignCountCleared,  
  
isnull(@totalUFCredit,0) as TotalUnsForeignCredit , isnull(@totalUFDebit,0) as TotalUnsForeignDebit,  
isnull(@totalUFCreditCount,0) + isnull(@totalUFDebitCount,0) as ForeignCountUnsupervisd,  
  
isnull(@totalRFCredit,0) as TotalRejForeignCredit , isnull(@totalRFDebit,0) as TotalRejForeignDebit,  
isnull(@totalRFCreditcount,0) + isnull(@totalRFDebitcount,0) as ForeignCountRejected