﻿CREATE PROCEDURE [dbo].[sp_ATM_crGetInterBranchStatement]
(
	@OurBranchID	varchar(30),
	@PHXDate	datetime,
	@CashTable1	varchar(30),
	@CashTable2	varchar(30),
	@CashTable3	varchar(30)
)
AS
	BEGIN
		SELECT '1' AS CategoryID, 'Originating' AS CategoryName, 
		    PHXDate, RefNo, Amount, TrxType, Remarks, 
		    Supervision
		FROM CashTable1
		WHERE (OurBranchID = @OurBranchID) and (OurBranchID <> CustomerBranchID) and (Supervision = 'C')
		and (PHXDate = @PHXDate)
UNION ALL
		SELECT '2' AS CategoryID,'Responding' AS CategoryName, 
		    PHXDate, RefNo, Amount, TrxType, Remarks, 
		    Supervision
		FROM t_ATM_TransferTransaction
		WHERE (OurBranchID = @OurBranchID) and (TrxType = 'C') and (Supervision = 'C')
		and (PHXDate = @PHXDate)
	END