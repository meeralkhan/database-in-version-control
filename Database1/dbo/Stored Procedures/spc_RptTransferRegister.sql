﻿CREATE Procedure [dbo].[spc_RptTransferRegister]  
(         
 @OurBranchID varchar(4) ,        
 @CurrencyID   varchar(4) = 'All',        
 @OperatorID varchar(8)='All',        
 @ScrollNoFrom Int = 0 ,        
 @ScrollNoTo     Int = 999999        
)        
        
AS        
    
SET NOCOUNT ON    
        
IF @OperatorID = 'All'        
            BEGIN        
                SELECT t_TransferTransactionModel.ScrollNo, t_TransferTransactionModel.OurBranchID, t_TransferTransactionModel.AccountID,         
                    t_TransferTransactionModel.AccountName,  t_TransferTransactionModel.ProductID,  t_TransferTransactionModel.CurrencyID,         
                    t_TransferTransactionModel.TrxType, t_TransferTransactionModel.ChequeID, t_TransferTransactionModel.Amount,         
                    t_TransferTransactionModel.ForeignAmount, t_TransferTransactionModel.ExchangeRate, t_TransferTransactionModel.GlID,         
       t_TransferTransactionModel.Supervision,  t_TransferTransactionModel.IsLocalCurrency, t_TransferTransactionModel.SerialNo,        
                    t_TransferTransactionModel.OperatorID, t_TransferTransactionModel.wDate,t_TransferTransactionModel.Description as Narration,        
        
                    t_TransferTransactionModel1.ScrollNo AS ScrollNoDB,  t_TransferTransactionModel1.OurBranchID AS OurBranchIDDB,         
                    t_TransferTransactionModel1.AccountID AS AccountIDDB,  t_TransferTransactionModel1.AccountName AS AccountNameDB,        
                    t_TransferTransactionModel1.ProductID AS ProductIDDB, t_TransferTransactionModel1.CurrencyID AS CurrencyIDDB,         
                    t_TransferTransactionModel1.TrxType AS TrxTypeDB, t_TransferTransactionModel1.ChequeID AS ChequeIDDB,         
                    t_TransferTransactionModel1.Amount AS AmountDB, t_TransferTransactionModel1.ForeignAmount AS ForeignAmountDB,        
                    t_TransferTransactionModel1.ExchangeRate AS ExchangeRateDB, t_TransferTransactionModel1.GlID AS GlIDDB,         
                    t_TransferTransactionModel1.Supervision AS SupervisionDB,  t_TransferTransactionModel1.IsLocalCurrency AS IsLocalCurrencyDB,        
                    t_TransferTransactionModel1.SerialNo AS SerialNoDB,t_TransferTransactionModel1.OperatorID AS OperatorIDDB,        
        t_TransferTransactionModel1.wDate AS wDateDB,t_TransferTransactionModel1.Description as NarrationDB,t_TransferTransactionModel.SupervisorID,        
  t_TransferTransactionModel1.SupervisorID as SupervisorIDDB        
        
                    FROM t_TransferTransactionModel INNER JOIN        
                        t_TransferTransactionModel t_TransferTransactionModel1 ON         
                        (t_TransferTransactionModel.OurBranchID = t_TransferTransactionModel1.OurBranchID) AND         
                        (t_TransferTransactionModel.ScrollNo = t_TransferTransactionModel1.ScrollNo)         
  --AND  (t_TransferTransactionModel.SerialNo = t_TransferTransactionModel1.Serialno)           
                    WHERE (t_TransferTransactionModel.OurBranchID = @OurBranchID) AND         
--                        (t_TransferTransactionModel.CurrencyID = @CurrencyID) AND        
                        (t_TransferTransactionModel.ScrollNo BETWEEN @ScrollNoFrom AND  @ScrollNoTo) AND        
                        (t_TransferTransactionModel.TrxType = 'C') AND         
                        (t_TransferTransactionModel1.TrxType = 'D') --AND (t_TransferTransactionModel.Supervision <> 'R')        
                    ORDER BY t_TransferTransactionModel.CurrencyID, t_TransferTransactionModel.ScrollNo, t_TransferTransactionModel.SerialNo        
            END        
        
ELSE        
        
            BEGIN        
                SELECT t_TransferTransactionModel.ScrollNo,  t_TransferTransactionModel.OurBranchID, t_TransferTransactionModel.AccountID,         
                    t_TransferTransactionModel.AccountName,  t_TransferTransactionModel.ProductID,  t_TransferTransactionModel.CurrencyID,         
                    t_TransferTransactionModel.TrxType, t_TransferTransactionModel.ChequeID, t_TransferTransactionModel.Amount,         
                    t_TransferTransactionModel.ForeignAmount, t_TransferTransactionModel.ExchangeRate, t_TransferTransactionModel.GlID,         
                    t_TransferTransactionModel.Supervision, t_TransferTransactionModel.IsLocalCurrency, t_TransferTransactionModel.SerialNo,         
  t_TransferTransactionModel.OperatorID, t_TransferTransactionModel.wDate,t_TransferTransactionModel.Description as Narration,        
                    t_TransferTransactionModel1.ScrollNo AS ScrollNoDB,  t_TransferTransactionModel1.OurBranchID AS OurBranchIDDB,         
                    t_TransferTransactionModel1.AccountID AS AccountIDDB,  t_TransferTransactionModel1.AccountName AS AccountNameDB,        
                    t_TransferTransactionModel1.ProductID AS ProductIDDB,  t_TransferTransactionModel1.CurrencyID AS CurrencyIDDB,         
                    t_TransferTransactionModel1.TrxType AS TrxTypeDB,  t_TransferTransactionModel1.ChequeID AS ChequeIDDB,         
                    t_TransferTransactionModel1.Amount AS AmountDB,  t_TransferTransactionModel1.ForeignAmount AS ForeignAmountDB,        
                    t_TransferTransactionModel1.ExchangeRate AS ExchangeRateDB, t_TransferTransactionModel1.GlID AS GlIDDB,         
                    t_TransferTransactionModel1.Supervision AS SupervisionDB,  t_TransferTransactionModel1.IsLocalCurrency AS IsLocalCurrencyDB,        
                    t_TransferTransactionModel1.SerialNo AS SerialNoDB, t_TransferTransactionModel1.OperatorID AS OperatorIDDB,        
        t_TransferTransactionModel1.wDate AS wDateDB,t_TransferTransactionModel1.Description as NarrationDB,t_TransferTransactionModel.SupervisorID,        
   t_TransferTransactionModel1.SupervisorID as SupervisorIDDB        
                    FROM t_TransferTransactionModel INNER JOIN        
                        t_TransferTransactionModel t_TransferTransactionModel1 ON         
                        (t_TransferTransactionModel.OurBranchID = t_TransferTransactionModel1.OurBranchID) AND         
                        (t_TransferTransactionModel.ScrollNo = t_TransferTransactionModel1.ScrollNo)         
  --AND (t_TransferTransactionModel.SerialNo = t_TransferTransactionModel1.Serialno)           
                    WHERE (t_TransferTransactionModel.OurBranchID = @OurBranchID) AND         
                        --(t_TransferTransactionModel.CurrencyID = @CurrencyID) AND        
                        (t_TransferTransactionModel.OperatorID = @OperatorID) AND        
            (t_TransferTransactionModel.ScrollNo BETWEEN @ScrollNoFrom AND  @ScrollNoTo) AND        
                        (t_TransferTransactionModel.TrxType = 'C') AND         
                        (t_TransferTransactionModel1.TrxType = 'D') --AND (t_TransferTransactionModel.Supervision <> 'R')        
                    ORDER BY t_TransferTransactionModel.CurrencyID, t_TransferTransactionModel.ScrollNo, t_TransferTransactionModel.SerialNo        
            END