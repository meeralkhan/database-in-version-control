﻿CREATE PROCEDURE [dbo].[sp_ATM_AddEditIBFTDebit]
(  
 @OurBranchID            varchar(30),   
 @WithdrawlBranchID      varchar(30)='',   
 @CustomerBranchID       varchar(30),  
 @CustomerBranchID2      varchar(30),  
 @ATMID                  varchar(30),   
 @AccountID              varchar(30),  
 @AccountID2             varchar(30),  
 @Amount                 money,  
 @USDAmount              money=0,  
 @OtherCurrencyAmount    money=0,  
 @USDRate                money=0,  
 @Supervision            char(1),   
 @RefNo                  varchar(36),  
 @PHXDate                datetime,  
 @MerchantType           varchar(30),  
 @OurIMD                 varchar(30),  
 @AckInstIDCode          varchar(30),  
 @Currency               varchar(3),  
 @NameLocation           varchar(40),  
 @MCC                    varchar(30),
 @TrxDesc                varchar(30)='',
 @NewRecord              bit=1
) 
AS
BEGIN

 DECLARE @BankShortName as varchar(30)
  SET @BankShortName = 'N/A'  
 DECLARE @mScrollNo as int  
 DECLARE @mSerialNo as smallint  
  SET @mSerialNo = 0
 DECLARE @mAccountName as varchar(50)  
 DECLARE @mAccountType as char  
 DECLARE @mProductID as varchar(30)  
 DECLARE @mCurrencyID as varchar(30)  
 DECLARE @mIsLocalCurrency as bit  
  SET @mIsLocalCurrency=1   
 DECLARE @mwDate as datetime  
 DECLARE @mTrxType as char  
 DECLARE @STAN as varchar(30)
 DECLARE @mGLID as varchar(30)  
  SET @mGLID = ''   
 DECLARE @mForeignAmount as money  
  SET @mForeignAmount=0   
 DECLARE @mExchangeRate as money  
  SET @mExchangeRate=1   
 DECLARE @mDescriptionID as varchar(3)  
  SET @mDescriptionID='000'   
 DECLARE @mDescription as varchar(255)  
 DECLARE @mDescriptionH as varchar(255)  
 DECLARE @mDescriptionCharges as varchar(255)  
 DECLARE @mRemarks as varchar(200)  
 DECLARE @mBankCode as varchar(30)  
  SET @mBankCode='1279'   
 DECLARE @mOperatorID as varchar(30)  
  SET @mOperatorID = 'ATM-' + @ATMID  
 DECLARE @mSupervisorID as varchar(30)  
  SET @mSupervisorID = @OurIMD
 --Checking Variables  
 DECLARE @vCount1 as int  
 DECLARE @vCount4 as bit  
 DECLARE @vClearBalance as money  
 DECLARE @vClearBalance2 as money  
 DECLARE @vClearedEffects as money  
 DECLARE @vFreezeAmount as money  
 DECLARE @vMinBalance as money  
 DECLARE @limit as money  
 DECLARE @vIsClose as char  
 DECLARE @vAreCheckBooksAllowed as bit  
 DECLARE @vAllowDebitTransaction as bit  
 DECLARE @vBranchName as varchar(50)  
  SET @vBranchName = 'N/A'  
 DECLARE @Status as int 
 DECLARE @mLastEOD as datetime  
 DECLARE @mCONFRETI as datetime  
 DECLARE @mWithdrawlBranchName as varchar(50)
 DECLARE @IssuerAccountID as varchar(30)  
 DECLARE @IssuerAccountTitle as varchar(50)  
 DECLARE @ExciseDutyAccountID as varchar(30)  
 DECLARE @ExciseDutyPercentage as money  
 DECLARE @ExciseDutyAmount as money
 DECLARE @TranAmount as money
 DECLARE @FEDAccID as varchar(30)
 DECLARE @FEDAccTitle as varchar(50)
 DECLARE @IBFTAccountID as varchar(30)  
 DECLARE @IBFTCharges as money  
 DECLARE @IBFTAccID as varchar(30)
 DECLARE @IBFTAccTitle as varchar(50)
 DECLARE @3PFTAccountID as varchar(30)  
 DECLARE @3PFTCharges as money  
 DECLARE @3PFTAccID as varchar(30)
 DECLARE @3PFTAccTitle as varchar(50)
  SET @ExciseDutyPercentage = 0
  SET @ExciseDutyAmount = 0
  SET @TranAmount = 0
  SET @IBFTCharges = 0
  SET @3PFTCharges = 0
  
 DECLARE @VoucherID int
 DECLARE @IsCredit int
 DECLARE @GLControl nvarchar(30)
  
 SELECT @BankShortName = BankShortName From t_ATM_Banks WHERE (OurBranchID = @OurBranchID AND BankIMD = @OurIMD)
 
 IF @MerchantType <> '0000'
 BEGIN
  IF @Currency = '586'
  BEGIN
   SET @mDescription='INT.BK. FUND TRF. AT BK.' + @BankShortName  + ' (BR.' + @CustomerBranchID + ') TO : A/c # ' + @AccountID2
   SET @mDescriptionH='INT.BK. FUND TRF. AT BK.' + @BankShortName   + ' (BR.' + @CustomerBranchID + ') From : A/c # ' + @AccountID  
   
  END
  ELSE
  BEGIN
   IF @Currency = '840'
   BEGIN
    SET @mDescription='INT.BK. FUND TRF. ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') AT ' + @NameLocation
	SET @mDescriptionH='INT.BK. FUND TRF. ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') AT ' +  + @NameLocation + ' From : A/c # ' + @AccountID
	
   END
   ELSE   
   BEGIN  
    SET @mDescription='INT.BK. FUND TRF.(' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) AT ' + @NameLocation   
	SET @mDescriptionH='INT.BK. FUND TRF. (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) AT ' +  + @NameLocation + ' From : A/c # ' + @AccountID  
	
   END  
  END
 END
 ELSE   
 BEGIN
  SET @mDescription='IB(' + @ATMID + ') FUND TRF AT BK.' + @BankShortName  + ' (BR.' + @WithdrawlBranchID + ') TO : A/c # ' + @AccountID2
  SET @mDescriptionH='ATM (' + @ATMID + ') FUND TRF AT BK.' + @BankShortName   + ' (BR.' + @CustomerBranchID + ') From : A/c # ' + @AccountID  
  
 END
  
 SET NOCOUNT ON
 SELECT @Status = -1  
 
 SELECT @mLastEOD=LastEOD,@mwDate=WorkingDate,@mCONFRETI=CONFRETI FROM t_Last WHERE (OurBranchID = @OurBranchID)
 IF @mLastEOD = @mwDate or @mCONFRETI = '1/1/9999'  
 BEGIN  
     
  SELECT @Status=9  --INVALID_DATE  
  SELECT @Status  as Status  
  RETURN (1)   
 END
 
 SELECT @mScrollNo=(ISNULL(MAX(ScrollNo),0)+1) FROM t_ATM_TransferTransaction WHERE (OurBranchID = @OurBranchID) 
 AND (convert(varchar(10), wDate, 120) = convert(varchar(10), @mwDate, 120))
 
 SELECT @vCount1=count(*) FROM t_Account WHERE (OurBranchID = @OurBranchID) AND (AccountID = @AccountID)
 
 IF  @vCount1=0
 BEGIN
     
  SELECT @Status=2  --INVALID_ACCOUNT  
  SELECT @Status  as Status  
  RETURN (1)   
 END
 
 SELECT @mAccountName=B.Name,@mProductID=B.ProductID,@mCurrencyID=P.CurrencyID,@mAccountType='C',@vClearBalance=B.ClearBalance,
 @vClearedEffects=B.ClearedEffects,@vFreezeAmount=B.FreezeAmount,@vMinBalance=P.MinBalance,@limit=isnull(b.limit,0),
 @vIsClose=A.[Status],@vAreCheckBooksAllowed=P.AreCheckBooksAllowed,@vAllowDebitTransaction=isnull(A.AllowDebitTransaction,0)
 FROM t_AccountBalance B 
 INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND B.ProductID = A.ProductID 
 INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND A.ProductID = P.ProductID 
 WHERE (A.OurBranchID = @OurBranchID) AND (A.AccountID = @AccountID)
 
 SET @vClearBalance2=@vClearBalance
 
 SELECT @IssuerAccountID=IssuerAccountID,@IssuerAccountTitle=IssuerAccountTitle,@IBFTAccountID=IBFTAccountID,
 @IBFTCharges=IBFTCharges,@3PFTAccountID=[3PFTAccountID],@ExciseDutyAccountID=ExciseDutyAccountID,
 @ExciseDutyPercentage=ExciseDutyPercentage
 From t_ATM_SwitchCharges 
 WHERE (OurBranchID = @OurBranchID AND MerchantType = @MerchantType)
 
 SELECT @3PFTCharges=ISNULL(Charges,0) 
 FROM t_ATM_OneLinkChargesSlab 
 WHERE OurBranchID = @OurBranchID AND @Amount BETWEEN AmountFrom AND AmountTo
 
 IF @vIsClose = 'C' or @vIsClose = 'T' or @vIsClose = 'X' or @vIsClose = 'D' or @vAllowDebitTransaction = 1
 BEGIN
     
  SELECT @Status=3  --INACTIVE/CLOSE_ACCOUNT  
  SELECT @Status  as Status  
  RETURN (1)   
 END  
 
 IF @Amount > (@vClearBalance-@vClearedEffects+@limit)-@IBFTCharges-@3PFTCharges OR 
    @Amount > ((@vClearBalance-@vClearedEffects+@limit)-@vFreezeAmount-@vMinBalance-@IBFTCharges-@3PFTCharges) OR 
	@Amount > ((@vClearBalance-@vClearedEffects+@limit)-@vMinBalance-@IBFTCharges-@3PFTCharges)
 BEGIN
     
  SELECT @Status=4  --LOW_BALANCE  
  SELECT @Status  as Status  
  RETURN (1)   
 END  
 
 IF @vAreCheckBooksAllowed = 0 
 BEGIN
   
  SELECT @Status=3  --INVALID_PRODUCT  
  SELECT @Status  as Status  
  RETURN (1)   
 END
 
 IF @mCurrencyID <> 'PKR'
 BEGIN
     
  SELECT @Status=3  --INVALID_CURRENCY  
  SELECT @Status  as Status  
  RETURN (1)   
 END
 
 ------------------------------------------------
 --Debit Transaction
 
 SET @mSerialNo = @mSerialNo + 1
 SET @mTrxType = 'D'
 SET @IsCredit = 0
 
 SELECT @vBranchName=Heading2 FROM t_GlobalVariables WHERE (OurBranchID = @OurBranchID)
 SELECT @vCount4=count(*) From t_ATM_Branches WHERE OurBranchID = @OurBranchID AND BranchID = @WithdrawlBranchID
 
 IF @MerchantType <> '0000'   
 BEGIN
  IF @Currency = '586'
  BEGIN
   SET @mRemarks='Being Amount of INT.BK. Fund TRF Transaction From : ' + @CustomerBranchID + ' By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)
   
  END
  ELSE
  BEGIN
   IF @Currency = '840'  
   BEGIN
    SET @mRemarks='Being Amount of INT.BK. Fund TRF Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)   
   END
   ELSE
   BEGIN  
    SET @mRemarks='Being Amount of INT.BK. Fund TRF Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) From : ' + @NameLocation + ' By  A/c # ' + @AccountID +  ' At : '  + upper(@vBranchName)
   END  
  END  
 END
 ELSE  
 BEGIN
  IF @vCount4 > 0 
  BEGIN
   SET  @mWithdrawlBranchName = 'N/A'  
   SELECT @mWithdrawlBranchName=BranchName FROM t_ATM_Branches WHERE OurBranchID = @OurBranchID AND BranchID = @WithdrawlBranchID
   SET @mRemarks='Being Amount of INT.BK. Fund TRF Transaction From : BK.' + @BankShortName +  ' (' + upper(@mWithdrawlBranchName) + ') By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)
  END
  ELSE
  BEGIN
   SET @mRemarks='Being Amount of (' + @ATMID+ ') INT.BK. Fund TRF Transaction From : BK.' + @BankShortName +  ' (BR.' +  @WithdrawlBranchID + ') By  A/c # ' + @AccountID +  ' At : '  + upper(@vBranchName)
  END
 END
 
 SET @STAN = RIGHT(@RefNo,6)
 
 IF @MerchantType <> '0000'
 BEGIN
  INSERT INTO t_ATM_TransferTransaction
  (
   ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
   IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
   [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
  )
  VALUES
  (
   @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@AccountID,@mAccountName,@mAccountType,@mProductID,
   @mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,
   @Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescription+' STAN: '+@STAN,@ATMID, 
   @mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MCC,@TrxDesc,@MerchantType,@STAN
  )
 END
 ELSE
 BEGIN
  INSERT INTO t_ATM_TransferTransaction
  (
   ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
   IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
   [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
  )
  VALUES
  (
   @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@AccountID,@mAccountName,@mAccountType,@mProductID,
   @mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,
   @Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescription+' STAN: '+@STAN,@ATMID,
   @mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MCC,@TrxDesc,@MerchantType,@STAN
  )
 END
 
 select @GLControl = ISNULL(GLControl, '') from t_Products where OurBranchID = @OurBranchID and ProductID = @mProductID
 select @VoucherID = ISNULL(MAX(VoucherID),0) + 1 from t_GLTransactions where OurBranchID = @OurBranchID
    
 INSERT INTO t_Transactions 
 ( 
  OurBranchID,ScrollNo,SerialNo,RefNo,wDate,AccountType,DocumentType,AccountID,AccountName,ProductID,CurrencyID,ValueDate,
  TrxType,ChequeID,ChequeDate,Amount,ForeignAmount,ExchangeRate,ProfitLoss,MeanRate,DescriptionID,[Description],BankCode,
  BranchCode,TrxPrinted,GLID,[Status],IsLocalCurrency,OperatorID,SupervisorID,AdditionalData,IsMainTrx,DocType,GLVoucherID,
  Remarks,TrxDesc,MerchantType,STAN 
 ) 
          
 VALUES 
 ( 
  @OurBranchID,@mScrollNo,@mSerialNo,@RefNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@mAccountType,'AT',
  @AccountID,@mAccountName,@mProductID,@mCurrencyID,@mwDate,@mTrxType,'',@mwDate,@Amount,@mForeignAmount,@mExchangeRate,0,
  @mExchangeRate,@mDescriptionID,@mDescription+' STAN: '+@STAN,@mBankCode,@WithdrawlBranchID,0,null,@Supervision,
  @mIsLocalCurrency,@mOperatorID,@mSupervisorID,null,'1','AT',@VoucherID,@mRemarks,@TrxDesc,@MerchantType,@STAN 
 )
    
 INSERT INTO t_GLTransactions
 ( 
  OurBranchID,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,
  ExchangeRate,IsCredit,TransactionType,TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,
  IsMainTrx,DocType,Remarks,TrxDesc,MerchantType,STAN 
 )
 
 VALUES
 ( 
  @OurBranchID,@GLControl,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@mwDate,@mDescriptionID,
  @mDescription+' STAN: '+@STAN,@mCurrencyID,@Amount,@mForeignAmount,@mExchangeRate,@IsCredit,'ATMTransfer','L',@mOperatorID,
  @mSupervisorID,'',@mScrollNo,@mSerialNo,null,'0','AT',@mRemarks,@TrxDesc,@MerchantType,@STAN 
 )
 
 SELECT @Status=@@ERROR
 
 IF @Status <> 0
 BEGIN
   
  RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR
  RETURN (5000)
 END
 
 --Credit Transaction
 SET @vIsClose = ''  
 SET @vAreCheckBooksAllowed = ''  
 SET @vAllowDebitTransaction = ''
 
 SET @mSerialNo = @mSerialNo + 1
 SET @mTrxType = 'C'  
 SET @IsCredit = 1
 
 IF @MerchantType <> '0000'
 BEGIN
  IF @Currency = '586'
  BEGIN
   SET @mRemarks='Being Amount of INT.BK. Fund TRF Transaction From : ' + @CustomerBranchID + ' By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)
  END      
  ELSE   
  BEGIN
   IF @Currency = '840'
   BEGIN
    SET @mRemarks='Being Amount of INT.BK. Fund TRF Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)  
   END
   ELSE
   BEGIN  
    SET @mRemarks='Being Amount of INT.BK. Fund TRF Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) From : ' + @NameLocation + ' By  A/c # ' + @AccountID +  ' At : '  + upper(@vBranchName)
   END
  END
 END
 ELSE  
 BEGIN
  IF @vCount4 > 0
  BEGIN
   SET  @mWithdrawlBranchName = 'N/A'
   SELECT @mWithdrawlBranchName=BranchName FROM t_ATM_Branches  
   WHERE OurBranchID = @OurBranchID AND BranchID = @WithdrawlBranchID  
   SET @mRemarks='Being Amount of ATM (' + @ATMID+ ') INT.BK. Fund Transfer Transaction From : BK.' + @BankShortName +  ' (' + upper(@mWithdrawlBranchName) + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
  END
  ELSE
  BEGIN  
   SET @mRemarks='Being Amount of ATM (' + @ATMID+ ') INT.BK. Fund Transfer Transaction From : BK.' + @BankShortName +  ' (BR.' +  @WithdrawlBranchID + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
  END
 END
 
 IF @MerchantType <> '0000'
 BEGIN
  INSERT INTO t_ATM_TransferTransaction
  (
   ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
   IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
   [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
  )
  VALUES 
  (  
   @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@IssuerAccountID,@IssuerAccountTitle,'G','GL','PKR',
   @mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,@Supervision,@mGLID,
   @Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescriptionH+' STAN: '+@STAN,@ATMID,@mBankCode,@mRemarks,
   @mOperatorID,@mSupervisorID,@MCC,@TrxDesc,@MerchantType,@STAN
  )
 END
 ELSE
 BEGIN
  INSERT INTO t_ATM_TransferTransaction
  (
   ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
   IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
   [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
  )
  VALUES
  (
   @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@IssuerAccountID,@IssuerAccountTitle,'G','GL','PKR',
   @mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,@Supervision,@mGLID,
   @Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescriptionH+' STAN: '+@STAN,@ATMID,@mBankCode,@mRemarks,
   @mOperatorID,@mSupervisorID,@MCC,@TrxDesc,@MerchantType,@STAN
  )
 END
 
 INSERT INTO t_GLTransactions
 ( 
  OurBranchID,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,
  ExchangeRate,IsCredit,TransactionType,TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,
  IsMainTrx,DocType,Remarks,TrxDesc,MerchantType,STAN 
 )
 
 VALUES
 ( 
  @OurBranchID,@IssuerAccountID,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@mwDate,
  @mDescriptionID,@mDescriptionH+' STAN: '+@STAN,@mCurrencyID,@Amount,@mForeignAmount,@mExchangeRate,@IsCredit,'ATMTransfer',
  'L',@mOperatorID,@mSupervisorID,'',@mScrollNo,@mSerialNo,null,'1','AT',@mRemarks,@TrxDesc,@MerchantType,@STAN 
 )
 
 SELECT @Status=@@ERROR  
 
 IF @Status <> 0
 BEGIN
     
  RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR
  RETURN (5000)
 END
 
 --Charges DEBIT Transaction--
 
 IF (@IBFTCharges > 0 OR @3PFTCharges > 0)
 BEGIN
 
  IF @IBFTCharges > 0
  BEGIN
  
   --SET @mSerialNo = 3
   SET @mSerialNo = @mSerialNo + 1
   SET @mTrxType = 'D'
   SET @mAccountType='C'
   SET @mCurrencyID='PKR'
   SET @IsCredit = 0
  
   IF @MerchantType <> '0000'
   BEGIN  
    IF @Currency = '586'  
    BEGIN  
     SET @mRemarks='Being Amount of INT.BK. Fund TRF Charges Transaction From : ' + @CustomerBranchID + ' By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)  
	 SET @mDescriptionCharges='ATM(' + @ATMID + ') INT.BK. Fund TRF-CHG A/c # ' + @AccountID + ' OF BK.' + @BankShortName
    END      
    ELSE   
    BEGIN
     IF @Currency = '840'  
	 BEGIN  
	  SET @mRemarks='Being Amount of INT.BK. Fund TRF Charges Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)  
	  SET @mDescriptionCharges='ATM(' + @ATMID + ') INT.BK. Fund TRF-CHG A/c # ' + @AccountID + ' From : ' + @NameLocation
	 END  
	 ELSE   
	 BEGIN  
	  SET @mRemarks='Being Amount of INT.BK. Fund TRF Charges Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) From : ' + @NameLocation + ' By  A/c # ' + @AccountID +  ' At : '  + upper(@vBranchName)
	  SET @mDescriptionCharges='ATM(' + @ATMID + ') INT.BK. Fund TRF-CHG A/c # ' + @AccountID + ' OF BK.' + @BankShortName
	 END
    END
   END
   ELSE
   BEGIN
    IF @vCount4 > 0
    BEGIN
     SET  @mWithdrawlBranchName = 'N/A'  
	 SELECT @mWithdrawlBranchName=BranchName FROM t_ATM_Branches WHERE OurBranchID = @OurBranchID AND BranchID = @WithdrawlBranchID
	 SET @mRemarks='Being Amount of ATM (' + @ATMID+ ') INT.BK. Fund Transfer Charges Transaction From : BK.' + @BankShortName +  ' (' + upper(@mWithdrawlBranchName) + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
	 SET @mDescriptionCharges='ATM(' + @ATMID + ') INT.BK. Fund TRF-CHG A/c # ' + @AccountID + ' OF BK.' + @BankShortName
    END  
    ELSE  
    BEGIN
     SET @mRemarks='Being Amount of ATM (' + @ATMID+ ') INT.BK. Fund Transfer Charges Transaction From : BK.' + @BankShortName +  ' (BR.' +  @WithdrawlBranchID + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)
	 SET @mDescriptionCharges='ATM(' + @ATMID + ') INT.BK. Fund TRF-CHG A/c # ' + @AccountID + ' OF BK.' + @BankShortName
    END
   END
  
   IF @MerchantType <> '0000'
   BEGIN
    INSERT INTO t_ATM_TransferTransaction 
    (
     ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
     IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
     [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
    )
    VALUES 
    (
     @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@AccountID,@mAccountName,@mAccountType,@mProductID,
     @mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,
     @Supervision,@mGLID,@IBFTCharges,0,1,@mDescriptionID,@mDescriptionCharges+' STAN: '+@STAN,@ATMID,@mBankCode,@mRemarks,
     @mOperatorID,@mSupervisorID,@MCC,@TrxDesc+'|IBK-FT CHG',@MerchantType,@STAN
    )  
   END
   ELSE  
   BEGIN
    INSERT INTO t_ATM_TransferTransaction 
    (  
     ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
     IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
     [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
    )
    VALUES
    (
     @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@AccountID,@mAccountName,@mAccountType,@mProductID,
     @mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,
     @Supervision,@mGLID,@IBFTCharges,0,1,@mDescriptionID,@mDescriptionCharges+' STAN: '+@STAN,@ATMID,@mBankCode,@mRemarks,
     @mOperatorID,@mSupervisorID,@MCC,@TrxDesc+'|IBK-FT CHG',@MerchantType,@STAN
    )
   END
   
   INSERT INTO t_Transactions 
   ( 
    OurBranchID,ScrollNo,SerialNo,RefNo,wDate,AccountType,DocumentType,AccountID,AccountName,ProductID,CurrencyID,ValueDate,
    TrxType,ChequeID,ChequeDate,Amount,ForeignAmount,ExchangeRate,ProfitLoss,MeanRate,DescriptionID,[Description],BankCode,
    BranchCode,TrxPrinted,GLID,[Status],IsLocalCurrency,OperatorID,SupervisorID,AdditionalData,IsMainTrx,DocType,GLVoucherID,
    Remarks,TrxDesc,MerchantType,STAN 
   ) 
   VALUES 
   ( 
    @OurBranchID,@mScrollNo,@mSerialNo,@RefNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@mAccountType,'AT',
    @AccountID,@mAccountName,@mProductID,@mCurrencyID,@mwDate,@mTrxType,'',@mwDate,@IBFTCharges,0,1,0,1,@mDescriptionID,
    @mDescriptionCharges+' STAN: '+@STAN,@mBankCode,@WithdrawlBranchID,0,null,@Supervision,@mIsLocalCurrency,@mOperatorID,
    @mSupervisorID,null,'1','AT',@VoucherID,@mRemarks,@TrxDesc+'|IBK-FT CHG',@MerchantType,@STAN 
   )
    
   INSERT INTO t_GLTransactions
   ( 
    OurBranchID,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,
    ExchangeRate,IsCredit,TransactionType,TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,
    IsMainTrx,DocType,Remarks,TrxDesc,MerchantType,STAN 
   )
   VALUES
   ( 
    @OurBranchID,@GLControl,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@mwDate,@mDescriptionID,
    @mDescriptionCharges+' STAN: '+@STAN,@mCurrencyID,@IBFTCharges,0,1,@IsCredit,'ATMTransfer','L',@mOperatorID,@mSupervisorID,
    '',@mScrollNo,@mSerialNo,null,'0','AT',@mRemarks,@TrxDesc+'|IBK-FT CHG',@MerchantType,@STAN 
   )
   
   SELECT @Status=@@ERROR
  
   IF @Status <> 0
   BEGIN
     
    RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR
    RETURN (5000)
   END
  END
  
  IF (@3PFTCharges > 0)
  BEGIN
   
   --SET @mSerialNo = 4
   SET @mSerialNo = @mSerialNo + 1
   SET @mTrxType = 'D'
   SET @mAccountType='C'
   SET @mCurrencyID='PKR'
   SET @IsCredit = 0
  
   IF @MerchantType <> '0000'
   BEGIN  
    IF @Currency = '586'  
    BEGIN  
     SET @mRemarks='Being Amount of INT.BK.TRF 1-Link Charges Transaction From : ' + @CustomerBranchID + ' By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)  
	 SET @mDescriptionCharges='ATM(' + @ATMID + ') INT.BK. Fund TRF-1-Link CHG A/c # ' + @AccountID + ' OF BK.' + @BankShortName
    END      
    ELSE   
    BEGIN
     IF @Currency = '840'  
	 BEGIN  
	  SET @mRemarks='Being Amount of INT.BK.TRF 1-Link Charges Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)  
	  SET @mDescriptionCharges='ATM(' + @ATMID + ') INT.BK. Fund TRF-1-Link CHG A/c # ' + @AccountID + ' From : ' + @NameLocation
	 END  
	 ELSE   
	 BEGIN  
	  SET @mRemarks='Being Amount of INT.BK.TRF 1-Link Charges Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) From : ' + @NameLocation + ' By  A/c # ' + @AccountID +  ' At : '  + upper(@vBranchName)
	  SET @mDescriptionCharges='ATM(' + @ATMID + ') INT.BK. Fund TRF-1-Link CHG A/c # ' + @AccountID + ' From : ' + @NameLocation
	 END
    END
   END
   ELSE
   BEGIN
    IF @vCount4 > 0
    BEGIN
     SET  @mWithdrawlBranchName = 'N/A'  
	 SELECT @mWithdrawlBranchName=BranchName FROM t_ATM_Branches WHERE OurBranchID = @OurBranchID AND BranchID = @WithdrawlBranchID
	
	 SET @mRemarks='Being Amount of ATM (' + @ATMID+ ') INT.BK.TRF 1-Link Charges Transaction From : BK.' + @BankShortName +  ' (' + upper(@mWithdrawlBranchName) + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
	 SET @mDescriptionCharges='ATM(' + @ATMID + ') INT.BK. Fund TRF-1-Link CHG A/c # ' + @AccountID + ' OF BK.' + @BankShortName
    END  
    ELSE  
    BEGIN
     SET @mRemarks='Being Amount of ATM (' + @ATMID+ ') INT.BK.TRF 1-Link Charges Transaction From : BK.' + @BankShortName +  ' (BR.' +  @WithdrawlBranchID + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)
	 SET @mDescriptionCharges='ATM(' + @ATMID + ') INT.BK. Fund TRF-1-Link CHG A/c # ' + @AccountID + ' OF BK.' + @BankShortName
    END
   END
   
   IF @MerchantType <> '0000'
   BEGIN
    INSERT INTO t_ATM_TransferTransaction 
    (
     ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
     IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
     [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
    )
    VALUES 
    (
     @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@AccountID,@mAccountName,@mAccountType,@mProductID,
     @mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,
     @Supervision,@mGLID,@3PFTCharges,0,1,@mDescriptionID,@mDescriptionCharges+' STAN: '+@STAN,@ATMID,@mBankCode,@mRemarks,
     @mOperatorID,@mSupervisorID,@MCC,@TrxDesc+'|IBK-FT 1LNK CHG',@MerchantType,@STAN
    )  
   END
   ELSE  
   BEGIN
    INSERT INTO t_ATM_TransferTransaction 
    (  
     ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
     IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
     [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
    )
    VALUES
    (
     @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@AccountID,@mAccountName,@mAccountType,@mProductID,
     @mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,
     @Supervision,@mGLID,@3PFTCharges,0,1,@mDescriptionID,@mDescriptionCharges+' STAN: '+@STAN,@ATMID,@mBankCode,@mRemarks,
     @mOperatorID,@mSupervisorID,@MCC,@TrxDesc+'|IBK-FT 1LNK CHG',@MerchantType,@STAN
    )
   END
   
   INSERT INTO t_Transactions 
   ( 
    OurBranchID,ScrollNo,SerialNo,RefNo,wDate,AccountType,DocumentType,AccountID,AccountName,ProductID,CurrencyID,ValueDate,
    TrxType,ChequeID,ChequeDate,Amount,ForeignAmount,ExchangeRate,ProfitLoss,MeanRate,DescriptionID,[Description],BankCode,
    BranchCode,TrxPrinted,GLID,[Status],IsLocalCurrency,OperatorID,SupervisorID,AdditionalData,IsMainTrx,DocType,GLVoucherID,
    Remarks,TrxDesc,MerchantType,STAN 
   ) 
   VALUES 
   ( 
    @OurBranchID,@mScrollNo,@mSerialNo,@RefNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@mAccountType,'AT',
    @AccountID,@mAccountName,@mProductID,@mCurrencyID,@mwDate,@mTrxType,'',@mwDate,@3PFTCharges,0,1,0,1,@mDescriptionID,
    @mDescriptionCharges+' STAN: '+@STAN,@mBankCode,@WithdrawlBranchID,0,null,@Supervision,@mIsLocalCurrency,@mOperatorID,
    @mSupervisorID,null,'1','AT',@VoucherID,@mRemarks,@TrxDesc+'|IBK-FT 1LNK CHG',@MerchantType,@STAN 
   )
    
   INSERT INTO t_GLTransactions
   ( 
    OurBranchID,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,
    ExchangeRate,IsCredit,TransactionType,TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,
    IsMainTrx,DocType,Remarks,TrxDesc,MerchantType,STAN 
   )
   VALUES
   ( 
    @OurBranchID,@GLControl,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@mwDate,@mDescriptionID,
    @mDescriptionCharges+' STAN: '+@STAN,@mCurrencyID,@3PFTCharges,0,1,@IsCredit,'ATMTransfer','L',@mOperatorID,@mSupervisorID,
    '',@mScrollNo,@mSerialNo,null,'0','AT',@mRemarks,@TrxDesc+'|IBK-FT 1LNK CHG',@MerchantType,@STAN 
   )
   
   SELECT @Status=@@ERROR
  
   IF @Status <> 0
   BEGIN
     
    RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR
    RETURN (5000)
   END
  END
  
  ----Charges Credit Transaction----
  
  if (@IBFTCharges > 0)
  BEGIN
  
   if (@ExciseDutyPercentage > 0)
   BEGIN
    SET @ExciseDutyAmount = (@IBFTCharges * @ExciseDutyPercentage) / 116
	SET @TranAmount = (@IBFTCharges-@ExciseDutyAmount)
   END
   ELSE
   BEGIN
    SET @ExciseDutyAmount = 0
	SET @TranAmount = @IBFTCharges
   END
   
   --SET @mSerialNo = 5
   SET @mSerialNo = @mSerialNo + 1
   SET @mTrxType = 'C'  
   SET @mAccountType='G'  
   SET @mProductid ='GL'  
   SET @mCurrencyID='PKR'
   SET @IsCredit = 1
   
   IF @MerchantType <> '0000'
   BEGIN  
    IF @Currency = '586'
    BEGIN
     SET @mRemarks='Being Amount of INT.BK. Fund TRF Charges Transaction From : ' + @CustomerBranchID + ' By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)  
	 SET @mDescriptionCharges='ATM(' + @ATMID + ') INT.BK. Fund TRF-CHG A/c # ' + @AccountID + ' OF BK.' + @BankShortName
    END
    ELSE
    BEGIN
     IF @Currency = '840'
	 BEGIN
	  SET @mRemarks='Being Amount of INT.BK. Fund TRF Charges Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)
	  SET @mDescriptionCharges='ATM(' + @ATMID + ') INT.BK. Fund TRF-CHG A/c # ' + @AccountID + ' OF BK.' + @BankShortName
	 END
	 ELSE
	 BEGIN
	  SET @mRemarks='Being Amount of INT.BK. Fund TRF Charges Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) From : ' + @NameLocation + ' By  A/c # ' + @AccountID +  ' At : '  + upper(@vBranchName)
	  SET @mDescriptionCharges='ATM(' + @ATMID + ') INT.BK. Fund TRF-CHG A/c # ' + @AccountID + ' OF BK.' + @BankShortName
	 END
    END
   END
   ELSE  
   BEGIN
    IF @vCount4 > 0
    BEGIN
     SET  @mWithdrawlBranchName = 'N/A'
	 SELECT @mWithdrawlBranchName=BranchName FROM t_ATM_Branches WHERE OurBranchID = @OurBranchID AND BranchID = @WithdrawlBranchID  
	
	 SET @mRemarks='Being Amount of ATM (' + @ATMID+ ') INT.BK. Fund Transfer Charges Transaction From : BK.' + @BankShortName +  ' (' + upper(@mWithdrawlBranchName) + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)
	 SET @mDescriptionCharges='ATM(' + @ATMID + ') INT.BK. Fund TRF-CHG A/c # ' + @AccountID + ' OF BK.' + @BankShortName
    END
    ELSE
    BEGIN
     SET @mRemarks='Being Amount of ATM (' + @ATMID+ ') INT.BK. Fund Transfer Charges Transaction From : BK.' + @BankShortName +  ' (BR.' +  @WithdrawlBranchID + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
	 SET @mDescriptionCharges='ATM(' + @ATMID + ') INT.BK. Fund TRF-CHG A/c # ' + @AccountID + ' OF BK.' + @BankShortName
    END
   END
   
   SELECT @IBFTAccID = AccountID, @IBFTAccTitle = [Description] 
   FROM t_GL WHERE OurBranchID = @OurBranchID AND AccountID = @IBFTAccountID
   
   IF @MerchantType <> '0000'   
   BEGIN
    INSERT INTO t_ATM_TransferTransaction
    (
     ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
     IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
     [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
    )
    VALUES
    (
     @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@IBFTAccID,@IBFTAccTitle,@mAccountType,@mProductID,
     @mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,
     @Supervision,@mGLID,@TranAmount,0,1,@mDescriptionID,@mDescriptionCharges+' STAN: '+@STAN,@ATMID,@mBankCode,@mRemarks,
     @mOperatorID,@mSupervisorID,@MCC,@TrxDesc+'|IBK-FT CHG',@MerchantType,@STAN
    )
   END
   ELSE
   BEGIN
    INSERT INTO t_ATM_TransferTransaction 
    (
     ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
     IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
     [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
    )
    VALUES
    (
     @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@IBFTAccID,@IBFTAccTitle,@mAccountType,@mProductID,
     @mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,
     @Supervision,@mGLID,@TranAmount,0,1,@mDescriptionID,@mDescriptionCharges+' STAN: '+@STAN,@ATMID,@mBankCode,@mRemarks,
     @mOperatorID,@mSupervisorID,@MCC,@TrxDesc+'|IBK-FT CHG',@MerchantType,@STAN
    )
   END
   
   INSERT INTO t_GLTransactions
   ( 
    OurBranchID,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,
    ExchangeRate,IsCredit,TransactionType,TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,
    IsMainTrx,DocType,Remarks,TrxDesc,MerchantType,STAN 
   )
   VALUES
   ( 
    @OurBranchID,@IBFTAccID,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@mwDate,@mDescriptionID,
    @mDescriptionCharges+' STAN: '+@STAN,@mCurrencyID,@TranAmount,0,1,@IsCredit,'ATMTransfer','L',@mOperatorID,@mSupervisorID,
    '',@mScrollNo,@mSerialNo,null,'1','AT',@mRemarks,@TrxDesc+'|IBK-FT CHG',@MerchantType,@STAN 
   )
  
   SELECT @Status=@@ERROR
  
   IF @Status <> 0
   BEGIN
       
    RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR  
    RETURN (5000)  
   END
  END
  
  if (@ExciseDutyPercentage > 0)
  BEGIN
  
   SET @ExciseDutyAmount = (@IBFTCharges * @ExciseDutyPercentage) / 116
  
   --SET @mSerialNo = 6
   SET @mSerialNo = @mSerialNo + 1
   SET @mTrxType = 'C'  
   SET @mAccountType='G'  
   SET @mProductid ='GL'  
   SET @mCurrencyID='PKR'
   SET @IsCredit = 1
  
   IF @MerchantType <> '0000'
   BEGIN  
    IF @Currency = '586'
    BEGIN
     SET @mRemarks='Being Amount of INT.BK. Fund TRF FED Charges Transaction From : ' + @CustomerBranchID + ' By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)  
	 SET @mDescriptionCharges='ATM(' + @ATMID + ') INT.BK. Fund TRF-FED.CHG A/c # ' + @AccountID + ' OF BK.' + @BankShortName
    END
    ELSE
    BEGIN
     IF @Currency = '840'
	 BEGIN
	  SET @mRemarks='Being Amount of INT.BK. Fund TRF FED Charges Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)
	  SET @mDescriptionCharges='ATM(' + @ATMID + ') INT.BK. Fund TRF-FED.CHG A/c # ' + @AccountID + ' OF BK.' + @BankShortName
	 END
	 ELSE
	 BEGIN
	  SET @mRemarks='Being Amount of INT.BK. Fund TRF FED Charges Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) From : ' + @NameLocation + ' By  A/c # ' + @AccountID +  ' At : '  + upper(@vBranchName)
	  SET @mDescriptionCharges='ATM(' + @ATMID + ') INT.BK. Fund TRF-FED.CHG A/c # ' + @AccountID + ' OF BK.' + @BankShortName
	 END
    END
   END
   ELSE  
   BEGIN
    IF @vCount4 > 0
    BEGIN
     SET  @mWithdrawlBranchName = 'N/A'
	 SELECT @mWithdrawlBranchName=BranchName FROM t_ATM_Branches WHERE OurBranchID = @OurBranchID AND BranchID = @WithdrawlBranchID  
	
	 SET @mRemarks='Being Amount of ATM (' + @ATMID+ ') INT.BK. Fund Transfer FED Charges Transaction From : BK.' + @BankShortName +  ' (' + upper(@mWithdrawlBranchName) + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)
	 SET @mDescriptionCharges='ATM(' + @ATMID + ') INT.BK. Fund TRF-FED.CHG A/c # ' + @AccountID + ' OF BK.' + @BankShortName
    END
    ELSE
    BEGIN
     SET @mRemarks='Being Amount of ATM (' + @ATMID+ ') INT.BK. Fund Transfer FED Charges Transaction From : BK.' + @BankShortName +  ' (BR.' +  @WithdrawlBranchID + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
	 SET @mDescriptionCharges='ATM(' + @ATMID + ') INT.BK. Fund TRF-FED.CHG A/c # ' + @AccountID + ' OF BK.' + @BankShortName
    END
   END
   
   SELECT @FEDAccID = AccountID, @FEDAccTitle = [Description] FROM t_GL WHERE OurBranchID = @OurBranchID AND AccountID = @ExciseDutyAccountID
   
   IF @MerchantType <> '0000'   
   BEGIN
    INSERT INTO t_ATM_TransferTransaction
    (
     ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
     IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
     [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
    )
    VALUES
    (
     @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@FEDAccID,@FEDAccTitle,@mAccountType,@mProductID,
     @mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,
     @Supervision,@mGLID,@ExciseDutyAmount,0,1,@mDescriptionID,@mDescriptionCharges+' STAN: '+@STAN,@ATMID,@mBankCode,
     @mRemarks,@mOperatorID,@mSupervisorID,@MCC,@TrxDesc+'|IBK-FT FED CHG',@MerchantType,@STAN
    )
   END
   ELSE
   BEGIN
    INSERT INTO t_ATM_TransferTransaction 
    (
     ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
     IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
     [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
    )
    VALUES
    (
     @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@FEDAccID,@FEDAccTitle,@mAccountType,@mProductID,
     @mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,
     @Supervision,@mGLID,@ExciseDutyAmount,0,1,@mDescriptionID,@mDescriptionCharges+' STAN: '+@STAN,@ATMID,@mBankCode,
     @mRemarks,@mOperatorID,@mSupervisorID,@MCC,@TrxDesc+'|IBK-FT FED CHG',@MerchantType,@STAN
    )
   END
   
   INSERT INTO t_GLTransactions
   ( 
    OurBranchID,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,
    ExchangeRate,IsCredit,TransactionType,TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,
    IsMainTrx,DocType,Remarks,TrxDesc,MerchantType,STAN 
   )
   VALUES
   ( 
    @OurBranchID,@FEDAccID,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@mwDate,@mDescriptionID,
    @mDescriptionCharges+' STAN: '+@STAN,@mCurrencyID,@ExciseDutyAmount,0,1,@IsCredit,'ATMTransfer','L',@mOperatorID,
    @mSupervisorID,'',@mScrollNo,@mSerialNo,null,'1','AT',@mRemarks,@TrxDesc+'|IBK-FT FED CHG',@MerchantType,@STAN 
   )
  
   SELECT @Status=@@ERROR
  
   IF @Status <> 0
   BEGIN
       
    RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR  
    RETURN (5000)  
   END
  END
  
  if (@3PFTCharges > 0)
  BEGIN
   --SET @mSerialNo = 7
   SET @mSerialNo = @mSerialNo + 1
   SET @mTrxType = 'C'  
   SET @mAccountType='G'  
   SET @mProductid ='GL'  
   SET @mCurrencyID='PKR'
   SET @IsCredit = 1
  
   IF @MerchantType <> '0000'
   BEGIN  
    IF @Currency = '586'
    BEGIN
     SET @mRemarks='Being Amount of INT.BK. Fund TRF 1-Link Charges Transaction From : ' + @CustomerBranchID + ' By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)  
	 SET @mDescriptionCharges='ATM(' + @ATMID + ') INT.BK. Fund TRF-1-Link CHG A/c # ' + @AccountID + ' OF BK.' + @BankShortName
    END
    ELSE
    BEGIN
     IF @Currency = '840'
	 BEGIN
	  SET @mRemarks='Being Amount of INT.BK. Fund TRF 1-Link Charges Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)
	  SET @mDescriptionCharges='ATM(' + @ATMID + ') INT.BK. Fund TRF-1-Link CHG A/c # ' + @AccountID + ' OF BK.' + @BankShortName
	 END
	 ELSE
	 BEGIN
	  SET @mRemarks='Being Amount of INT.BK. Fund TRF 1-Link Charges Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) From : ' + @NameLocation + ' By  A/c # ' + @AccountID +  ' At : '  + upper(@vBranchName)
	  SET @mDescriptionCharges='ATM(' + @ATMID + ') INT.BK. Fund TRF-1-Link CHG A/c # ' + @AccountID + ' OF BK.' + @BankShortName
	 END
    END
   END
   ELSE  
   BEGIN
    IF @vCount4 > 0
    BEGIN
     SET  @mWithdrawlBranchName = 'N/A'
	 SELECT @mWithdrawlBranchName=BranchName FROM t_ATM_Branches WHERE OurBranchID = @OurBranchID AND BranchID = @WithdrawlBranchID  
	
	 SET @mRemarks='Being Amount of ATM (' + @ATMID+ ') INT.BK. Fund Transfer 1-Link Charges Transaction From : BK.' + @BankShortName +  ' (' + upper(@mWithdrawlBranchName) + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)
	 SET @mDescriptionCharges='ATM(' + @ATMID + ') INT.BK. Fund TRF-1-Link CHG A/c # ' + @AccountID + ' OF BK.' + @BankShortName
    END
    ELSE
    BEGIN
     SET @mRemarks='Being Amount of ATM (' + @ATMID+ ') INT.BK. Fund Transfer 1-Link Charges Transaction From : BK.' + @BankShortName +  ' (BR.' +  @WithdrawlBranchID + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
	 SET @mDescriptionCharges='ATM(' + @ATMID + ') INT.BK. Fund TRF-1-Link CHG A/c # ' + @AccountID + ' OF BK.' + @BankShortName
    END
   END
   
   SELECT @3PFTAccID = AccountID, @3PFTAccTitle = [Description] FROM t_GL WHERE OurBranchID = @OurBranchID AND AccountID = @3PFTAccountID
   
   IF @MerchantType <> '0000'
   BEGIN
    INSERT INTO t_ATM_TransferTransaction
    (
     ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
     IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
     [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
    )
    VALUES
    (
     @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@3PFTAccID,@3PFTAccTitle,@mAccountType,@mProductID,
     @mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,
     @Supervision,@mGLID,@3PFTCharges,0,1,@mDescriptionID,@mDescriptionCharges+' STAN: '+@STAN,@ATMID,@mBankCode,@mRemarks,
     @mOperatorID,@mSupervisorID,@MCC,@TrxDesc+'|IBK-FT 1LNK CHG',@MerchantType,@STAN
    )
   END
   ELSE
   BEGIN
    INSERT INTO t_ATM_TransferTransaction 
    (
     ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
     IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
     [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
    )
    VALUES
    (
     @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@3PFTAccID,@3PFTAccTitle,@mAccountType,@mProductID,
     @mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,
     @Supervision,@mGLID,@3PFTCharges,0,1,@mDescriptionID,@mDescriptionCharges+' STAN: '+@STAN,@ATMID,@mBankCode,@mRemarks,
     @mOperatorID,@mSupervisorID,@MCC,@TrxDesc+'|IBK-FT 1LNK CHG',@MerchantType,@STAN
    )
   END
   
   INSERT INTO t_GLTransactions
   ( 
    OurBranchID,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,
    ExchangeRate,IsCredit,TransactionType,TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,
    IsMainTrx,DocType,Remarks,TrxDesc,MerchantType,STAN 
   )
   VALUES
   ( 
    @OurBranchID,@3PFTAccID,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@mwDate,@mDescriptionID,
    @mDescriptionCharges+' STAN: '+@STAN,@mCurrencyID,@3PFTCharges,0,1,@IsCredit,'ATMTransfer','L',@mOperatorID,
    @mSupervisorID,'',@mScrollNo,@mSerialNo,null,'1','AT',@mRemarks,@TrxDesc+'|IBK-FT 1LNK CHG',@MerchantType,@STAN 
   )
  
   SELECT @Status=@@ERROR
  
   IF @Status <> 0
   BEGIN
       
    RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR  
    RETURN (5000)  
   END
  END
  
  SELECT @Status = 0, 
  @vClearBalance2 = ((IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0))-IsNull(@Amount,0)-ISNULL(@IBFTCharges,0)-ISNULL(@3PFTCharges,0))*100
  SELECT @Status As Status , @vClearBalance2 As ClearBalance
  RETURN 0
 
   
 END
END