﻿CREATE PROCEDURE [dbo].[spc_GetLimitOtherAccount]   
(   
 @OurBranchID varchar(30),   
 @AccountID varchar(30),   
 @CurrencyID varchar(30),   
 @MeanRate decimal(24,6),   
 @LocalCurrencyID varchar(30),  
 @Balance money=0  
)   
AS   
   
SET NOCOUNT ON   
  
BEGIN   
   
 exec @Balance = fn_MURGetValidateLimit @OurBranchID, @AccountID, @CurrencyID, @MeanRate, @LocalCurrencyID   
   
 select @Balance ClearBalance   
   
END