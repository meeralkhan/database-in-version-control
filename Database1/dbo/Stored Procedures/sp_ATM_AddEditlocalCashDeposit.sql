﻿CREATE PROCEDURE [dbo].[sp_ATM_AddEditlocalCashDeposit]  
(  
 @OurBranchID       varchar(30),   
 @CustomerBranchID      varchar(30),   
 @ATMID       varchar(30),   
 @AccountID           varchar(30),  
 @Amount        money=0,  
 @USDAmount       money=0,  
 @OtherCurrencyAmount      money=0,  
 @USDRate           money=0,  
 @Supervision       char(1),   
 @RefNo        varchar(50),  
 @PHXDate       datetime,   
 @MerchantType                  varchar(30),  
 @OurIMD       varchar(30),   
 @AckInstIDCode      varchar(30),  
 @Currency       varchar(30),  
 @NameLocation      varchar(100),  
 @MCC        varchar(30),  
 @PHXDateTime        varchar(14),  
 @CreditCardNumber      varchar(30)='',  
  
 @NewRecord       bit=1  
)  
AS  
  
 DECLARE @BankShortName as varchar(30)  
  
 DECLARE @mScrollNo as money  
 DECLARE @mSerialNo as money  
  SET @mSerialNo=1   
 DECLARE @mCustomerAccountID as varchar(30)  
  SET @mCustomerAccountID=@AccountID   
 DECLARE @mAccountName as varchar(100)  
 DECLARE @mAccountType as char  
 DECLARE @mProductID as varchar(30)  
 DECLARE @mCurrencyID as varchar(30)  
 DECLARE @mIsLocalCurrency as char  
  SET @mIsLocalCurrency=1   
 DECLARE @mwDate as datetime  
 DECLARE @mTrxType as char  
  SET @mTrxType='C'   
 DECLARE @mGLID as varchar(30)  
 DECLARE @mForeignAmount as money  
  SET @mForeignAmount=0   
 DECLARE @mExchangeRate as money  
  SET @mExchangeRate=1   
 DECLARE @mDescriptionID as varchar(30)  
  SET @mDescriptionID='987'   
 DECLARE @mDescription as varchar(255)  
 DECLARE @mDescriptionCharges as varchar(255)  
 DECLARE @mDescriptionTax as varchar(255)  
 DECLARE @LocalCurrencyID as varchar(30)  
 DECLARE @LocalCurrency as varchar(30)  
  BEGIN   
   SELECT @LocalCurrencyID=LocalCurrencyID,@LocalCurrency=LocalCurrency FROM t_ATM_GlobalVariables  
   SET @BankShortName = 'N/A'  
   SELECT @BankShortName = BankShortName From t_ATM_Banks  
   WHERE (BankIMD = @AckInstIDCode)  
   
   IF @OurIMD = @AckInstIDCode   
    BEGIN   
     SET @mDescription=Right(@RefNo,6) + '-ATM(' + @ATMID + ') CSH DEPOSIT : A/c # ' + @AccountID + ' OF BK.' + @BankShortName + '(BR.' + @CustomerBranchID + ')'   
     SET @mDescription = @mDescription + '-' + @PHXDateTime  
     SET @mDescriptionTax=Right(@RefNo,6) + '-ATM(' + @ATMID + ') CSH DEPOSIT-TAX : A/c # ' + @AccountID + ' OF BK.' + @BankShortName + '(BR.' + @CustomerBranchID + ')'   
     SET @mDescriptionTax = @mDescriptionTax + '-' + @PHXDateTime  
    END   
   ELSE   
    BEGIN   
     SET @mDescription=Right(@RefNo,6) + '-ATM(' + @ATMID + ') CSH DEPOSIT : A/c # ' + @AccountID + ' OF BK.' + @BankShortName + '(BR.' + @CustomerBranchID + ')'   
     SET @mDescription = @mDescription + '-' + @PHXDateTime  
     SET @mDescriptionCharges=Right(@RefNo,6) + '-ATM(' + @ATMID + ') CSH DEPOSIT-CHG : A/c # ' + @AccountID + ' OF BK.' + @BankShortName + '(BR.' + @CustomerBranchID + ')'   
     SET @mDescriptionCharges = @mDescriptionCharges + '-' + @PHXDateTime  
     SET @mDescriptionTax=Right(@RefNo,6) + '-ATM(' + @ATMID + ') CSH DEPOSIT-TAX : A/c # ' + @AccountID + ' OF BK.' + @BankShortName + '(BR.' + @CustomerBranchID + ')'   
     SET @mDescriptionTax = @mDescriptionTax + '-' + @PHXDateTime  
    END   
  END   
 DECLARE @mRemarks as varchar(200)  
 DECLARE @mOperatorID as varchar(30)  
  SET @mOperatorID = 'ATM-' + @ATMID  
 DECLARE @mSupervisorID as varchar(30)  
  SET @mSupervisorID = @AckInstIDCode  
  
 DECLARE @mLastEOD as datetime  
 DECLARE @mCONFRETI as datetime  
 DECLARE @mCustomerBranchName as varchar(100)  
  
--Checking Variables  
 DECLARE @vCount1 as bit  
 DECLARE @vCount2 as bit  
 DECLARE @vCount3 as bit  
 DECLARE @vCount4 as bit  
 DECLARE @vAccountID as varchar(30)  
  SET @vAccountID = @AccountID  
 DECLARE @vClearBalance as money  
 DECLARE @vClearedEffects as money  
 DECLARE @vFreezeAmount as money  
 DECLARE @vMinBalance as money  
 DECLARE @vIsClose as char  
 DECLARE @vAreCheckBooksAllowed as bit  
 DECLARE @vAllowCreditTransaction as bit  
 DECLARE @vTaxAmount as money  
  SET @vTaxAmount = 0  
  
 DECLARE @vAccountLimit as money  
 DECLARE @vCashAmt as money  
 DECLARE @vExpiryDate as datetime  
 DECLARE @vCheckLimit as bit  
  SET @vCheckLimit = 0  
 DECLARE @vODLimitAllow as bit  
  SET @vODLimitAllow = 0  
 DECLARE @vTaxAccountID as varchar(30)  
  SET @vTaxAccountID = ''  
 DECLARE @vTaxDeduction as money  
  SET @vTaxDeduction = 0  
 DECLARE @vTaxLimit as money  
  SET @vTaxLimit = 0  
  
 DECLARE @vBranchName as varchar(100)  
 DECLARE @Status as int   
  
 DECLARE @WithDrawlCharges as money  
 DECLARE @WithDrawlChargesPercentage as money  
 DECLARE @AcquiringAccountID as varchar(30)  
 DECLARE @AcquiringAccountTitle as varchar(100)  
 DECLARE @AcquiringChargesAccountID as varchar(30)  
 DECLARE @AcquiringTitle as varchar(100)  
 DECLARE @CCAccountID as varchar(30)  
 DECLARE @CCTitle as varchar(100)  
  
 SET NOCOUNT ON  
  
 SELECT @Status = -1  
  
 BEGIN  
  
   SELECT @mLastEOD=LastEOD,@mwDate=WorkingDate,@mCONFRETI=CONFRETI FROM t_Last WHERE (OurBranchID = @OurBranchID)  
   IF @mLastEOD = @mwDate or @mCONFRETI = '1/1/9999'  
    BEGIN  
        
     SELECT @Status=9  --INVALID_DATE  
     SELECT @Status  as Status  
     RETURN (1)   
    END  
  
   IF (@OurBranchID <> @CustomerBranchID and @MerchantType='0000') or (@MerchantType <> '0000') --@OurIMD = @AckInstIDCode) OR (@OurIMD <> @AckInstIDCode)  
    BEGIN  
     IF @MerchantType = '0000'--@OurIMD = @AckInstIDCode  
      BEGIN  
       SET @WithDrawlCharges = 0  
       SET @WithDrawlChargesPercentage = 0  
       SELECT @AcquiringAccountID=IssuerAccountID,@AcquiringAccountTitle=AcquiringAccountTitle,  
         @AcquiringChargesAccountID=AcquiringChargesAccountID,@AcquiringTitle=AcquiringTitle    
       From t_ATM_SwitchCharges  
       WHERE (MerchantType = @MerchantType)  
  
       SET @mDescription=Right(@RefNo,6) + '-ATM(' + @ATMID + ') CSH DEPOSIT : A/c # ' + @AccountID + ' OF BK.' + @BankShortName + '(BR.'+ @CustomerBranchID + ')'   
       SET @mDescription = @mDescription + '-' + @PHXDateTime  
       SELECT @AccountID=AccountID, @mAccountName=Description,@mProductID='GL', @mCurrencyID=CurrencyID,@mAccountType='G' FROM t_GL   
       WHERE (OurBranchID = @OurBranchID) and (AccountID =  @AcquiringAccountID)  --(SELECT HOAccountID FROM t_ATM_HOAccount))  
      END  
    END  
  
  
   ELSE  
    BEGIN  
     SELECT @vCount1=count(*) From t_AccountBalance  
     WHERE (OurBranchID = @OurBranchID) and (AccountID=@AccountID)  
     IF @vCount1=0  
      BEGIN  
          
       SELECT @Status=2 --INVALID_ACCOUNT  
       SELECT @Status  as Status  
       RETURN (1)   
      END  
  
       
     SELECT @mAccountName=B.Name, @mProductID=B.ProductID, @mCurrencyID=P.CurrencyID,@mAccountType='C',@vClearBalance=B.ClearBalance,@vClearedEffects=B.ClearedEffects,@vFreezeAmount=B.FreezeAmount,@vMinBalance=A.MinBalance,@vIsClose=A.Status, @vAreCheckBooksAllowed=P.AreCheckBooksAllowed, @vAllowCreditTransaction=S.AllowCreditTransaction, @vAccountLimit=B.Limit, @vCashAmt=B.CashAmt  
     FROM t_AccountBalance B INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND B.ProductID = A.ProductID INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND A.ProductID = P.ProductID LEFT OUTER JOIN t_AccountSpecialCondition S ON B.OurBranchID = S.OurBranchID AND B.AccountID = S.AccountID  
     WHERE (A.OurBranchID = @OurBranchID) and (A.AccountID = @AccountID)  
  
     IF @vIsClose = 'C' or @vIsClose = 'T' or @vIsClose = 'X' or @vAllowCreditTransaction = 1  
      BEGIN  
          
       SELECT @Status=3  --INACTIVE_ACCOUNT  
       SELECT @Status  as Status  
  
  
       RETURN (1)   
      END  
     IF @mCurrencyID <> @LocalCurrency  
      BEGIN  
          
       SELECT @Status=3  --INVALID_CURRENCY  
       SELECT @Status  as Status  
       RETURN (1)   
      END  
    END  
  
  
   SELECT @mScrollNo=(ISNULL(MAX(ScrollNo),0)+1) FROM t_ATM_CashTransaction WHERE (OurBranchID = @OurBranchID) and (wDate = @mwDate)  
  
   SET @mGLID = '00000000'  
   SELECT @mGLID=AccountID FROM t_GL WHERE (OurBranchID = @OurBranchID) and (AccountID =  (SELECT  CDMAccountID FROM t_ATM_ATMAccount WHERE OurBranchID = @OurBranchID and ATMID = @ATMID))  
  
   SET  @vBranchName = 'N/A'  
   SELECT @vBranchName=Heading2 FROM t_GlobalVariables  WHERE (OurBranchID = @OurBranchID)  
  
  
   SELECT @vCount4=count(*) From t_ATM_Branches  
   WHERE BranchID = @CustomerBranchID  
  
   IF @OurIMD <> @AckInstIDCode   
    BEGIN  
       SET @mRemarks='Being Amount of ATM Cash Withdrawl Transaction From : ' + upper(@vBranchName) + '  By A/c # ' + @vAccountID + ' At : ' + @BankShortName + '(' + @CustomerBranchID+ ')'  
    END  
   ELSE  
    IF @vCount4 > 0   
     BEGIN  
      SELECT @mCustomerBranchName=BranchName FROM t_ATM_Branches  
      WHERE BranchID = @CustomerBranchID  
      IF @CustomerBranchID <> @OurBranchID   
      BEGIN  
         SET @mRemarks='Being Amount of ATM Cash Withdrawl Transaction From : ' + upper(@vBranchName) + '  By A/c # ' + @vAccountID + ' At : ' + @BankShortName + '(' +  upper(@mCustomerBranchName) + ')'  
      END  
      ELSE  
      BEGIN  
         SET @mRemarks='Being Amount of ATM Cash Withdrawl Transaction From : ' + upper(@vBranchName) + '  By A/c # ' + @vAccountID + ' At : ' + @BankShortName + '(' +  upper(@mCustomerBranchName) + ')'  
      END  
     END  
   --IF @vCount4 = 0   
      ELSE  
     BEGIN  
      IF @CustomerBranchID <> @OurBranchID  
      BEGIN   
       SET @mRemarks='Being Amount of ATM Cash Withdrawl Transaction From : ' + upper(@vBranchName) + '  By A/c # ' + @vAccountID + ' At : '  + @BankShortName + '(' + @CustomerBranchID+ ')'  
      END   
      ELSE  
      BEGIN   
       SET @mRemarks='Being Amount of ATM Cash Withdrawl Transaction From : ' + upper(@vBranchName) + '  By A/c # ' + @vAccountID + ' At : '  + @BankShortName + '(' + @CustomerBranchID+ ')'  
      END   
     END  
  
   Set @mSerialNo = 1  
  
  
  
  INSERT INTO t_ATM_CashTransaction  
   (  
     ScrollNo,SerialNo,RefNo,OurBranchID,CustomerBranchID,CustomerAccountID,AccountID,AccountName,AccountType,ProductID,  
     CurrencyID,IsLocalCurrency,ValueDate,wDate,PHXDate,TrxType,Supervision,GLID,  
     Amount,ForeignAmount,ExchangeRate,DescriptionID,Description, ATMID,Remarks,OperatorID,SupervisorID,MCC,MerchantType,CreditCardNumber  
   )    
  
  VALUES  
  
   (  
    @mScrollNo,@mSerialNo,@RefNo,@OurBranchID,@CustomerBranchID,@mCustomerAccountID,@AccountID,@mAccountName,@mAccountType,@mProductID,  
    @mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate,@PHXDate,@mTrxType,@Supervision,@mGLID,  
    @Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescription,@ATMID,@mRemarks,@mOperatorID,@mSupervisorID,@MCC,@MerchantType,@CreditCardNumber  
   )  
  
    SELECT @Status=@@ERROR  
  
    IF @Status <> 0   
     BEGIN  
         
      RAISERROR('There Is An Error Occured While Creating Transaction In The Source Branch',16,1) WITH SETERROR  
      RETURN (5000)  
     END  
    
    
 BEGIN  
--  SELECT @Status = 0 , @vClearBalance = ((Isnull(@vClearBalance,0)-IsNull(@vClearedEffects,0))+isnull(@Amount,0))*100  
  SELECT @Status = 0 , @vClearBalance = ((Isnull(@vClearBalance,0)-IsNull(@vClearedEffects,0))+IsNull(@Amount,0)-isNull(@WithDrawlCharges,0))*100  
  SELECT @Status As Status , Round(@vClearBalance,0) As ClearBalance  
 END   
  
  RETURN 0  
  
  
     
 END