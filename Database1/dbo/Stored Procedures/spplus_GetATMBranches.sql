﻿  
CREATE PROCEDURE [dbo].[spplus_GetATMBranches] (
 @OurBranchID varchar(30),
 @BranchID varchar(30)=''
)  
 AS    
 if @BranchID <> ''    
  begin    
   if exists ( SELECT BranchID FROM t_ATM_Branches WHERE OurBranchID = @OurBranchID AND BranchID = @BranchID )
    
    select *, 1 as ReturnValue from t_ATM_Branches
    Where OurBranchID = @OurBranchID AND BranchID = @BranchID
      
   else    
   
    select 0 as ReturnValue    
    
  end    
    
 else    
  begin    
   select BranchID, OurBranchID, BranchName From t_ATM_Branches
  end