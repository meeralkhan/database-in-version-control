﻿CREATE PROCEDURE [dbo].[sp_ATM_AddEditTransactionHistory] 
AS
	SET NOCOUNT ON

	DECLARE @Status as int	
	DECLARE @Count as  int
	DECLARE @Count2 as  int
	
	Select @Count = Count(*)  from t_ATM_CashTransaction

	if @Count > 0
	BEGIN
		INSERT INTO t_ATM_TransactionHistory (Category,ScrollNo,SerialNo,RefNo,OurBranchID,CustomerBranchID,WithdrawlBranchID,ATMID,CustomerAccountID,
							AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,
							ValueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,
							ExchangeRate,DescriptionID,Description,BankCode,Remarks,OperatorID,SupervisorID,MCC,MerchantType)

			 (SELECT 'O',ScrollNo,SerialNo,RefNo,OurBranchID,CustomerBranchID,'',ATMID,CustomerAccountID,
				    AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,
				    ValueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,
				    ExchangeRate,DescriptionID,Description,'093',Remarks,OperatorID,SupervisorID,MCC,MerchantType
			 FROM t_ATM_CashTransaction )
	END

	SELECT @Status=@@ERROR

	IF @Status <> 0 
		BEGIN
			 
			RAISERROR('There Is An Error Occured While Balance Enquiry In The Local Branch',16,1) WITH SETERROR
			RETURN (5000)
		END
		
	Select @Count2 = Count(*)  from t_ATM_TransferTransaction

	if @Count2 > 0
	BEGIN
		INSERT INTO t_ATM_TransactionHistory (Category,ScrollNo,SerialNo,RefNo,OurBranchID,CustomerBranchID,WithdrawlBranchID,ATMID,CustomerAccountID,
							AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,
							ValueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,
							ExchangeRate,DescriptionID,Description,BankCode,Remarks,OperatorID,SupervisorID,MCC,MerchantType)

			 (SELECT 'R',ScrollNo,SerialNo,Refno,OurBranchID,'',WithdrawlBranchID,ATMID,'',AccountID,
				   AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,ValueDate,
				   wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
                                                        Description,BankCode,Remarks,OperatorID,SupervisorID,MCC,MerchantType
			 FROM t_ATM_TransferTransaction )

	END
	SELECT @Status=@@ERROR

	IF @Status <> 0 
		BEGIN
			 
			RAISERROR('There Is An Error Occured While Balance Enquiry In The Local Branch',16,1) WITH SETERROR
			RETURN (5000)
		END

		SELECT @Status = 0 
		SELECT @Status As Status
		RETURN 0