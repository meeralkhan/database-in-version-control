﻿CREATE PROCEDURE [dbo].[spc_AddEditCashTransaction]
(
 @OurBranchID varchar(30),
 @AccountID varchar(30),
 @AccountName varchar(100)='',
 @ProductID varchar(30)='GL',
 @CurrencyID varchar(30),
 @AccountType Char(1),
                       
 @wDate datetime,
 @ValueDate datetime=@wDate,
                       
 @TrxType Char(1)='',
 @ChequeID varchar(30)='V',
 @ChequeDate DateTime=@wDate,

 @Amount money,
 @ForeignAmount Money,
 @ExchangeRate Decimal(18,6)=1,

 @DescriptionID varchar(30)='',
 @Description varchar(255)='',

 @BankCode varchar(30),
 @BranchCode varchar(30),
                       
 @AddData text='',                      
                       
 @GlID varchar(30)='',
 @Supervision char(1)='',
 @IsSupervision bit=0,

 @OperatorID varchar(30),
 @SupervisorID varchar(30)='',

-- @RemotePass bit=0,
-- @ClearBalance money=0,
-- @OutDp Money=0,
-- @PassBy varchar(8)='',
 @RemoteDescription varchar(100)='',
-- @EffectiveBalance money=0,
 @Status char(1)='',

 @IsLocalCurrency int=1,
-- @DebitPostingLimit money=0,
 @PostingLimit money=0,
 @IsOverDraft bit=1,

 @TrxPrinted bit=0,
 @ProfitOrLoss bit=0,

 @TrxMode char(1)='C',
 @RefNo varchar(100)='',      

 @AppWHTax char(1) = 0,
 @WHTaxAmount money = 0,
 @WHTaxMode char(1) = '',
 @WHTaxScrollNo Numeric(18,0) = 0,


 @AppChg char(1) = 0,
 @ChgAmount money = 0,
 @ChgMode char(1) = '',
 @ChgScrollNo Numeric(18,0) = 0,

 @DateOfIssue datetime ='01/01/1900',
 @BatchNo int=0,
 @BatchInstrumentNo int = 0,
 @InstrumentType Varchar(20) = '',
 @PaidMode Char(2) = 'CH',
 @UtilityNumber varchar(20)='',
 
 @NewRecord bit = 1
)
AS
   SET NOCOUNT ON
   DECLARE @ScrollNo as Numeric(18,0)
   DECLARE @RetStatus as int

   DECLARE @Amt as Money
   DECLARE @IsDebit as Bit
   DECLARE @mDate as Varchar(50)
   DECLARE @LocEq as Money

   BEGIN
--   print @AccountType
/*
      IF @AccountType = 'C'
         BEGIN
            IF (SELECT Count(AccountID) FROM t_AccountBalance 
               WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID) = 0
               BEGIN
                SELECT 50 as Status --Customer A/c Not Found
                  RETURN(1)
               END
         END                 
      ELSE
         BEGIN
             IF (SELECT Count(AccountID) FROM t_GL 
               WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID) = 0
                  BEGIN
                     SELECT 51 as Status ----GL A/c Not Found
                  RETURN(1)
                  END
         END
  */       
         IF @IsLocalCurrency = 0
         SELECT @Amt = @ForeignAmount, @LocEq = @Amount
         ELSE
            SELECT @Amt = @Amount, @LocEq = @Amount 


         IF @TrxType = 'C'  SELECT @IsDebit = 0
         ELSE
            SELECT @IsDebit = 1

            
         EXEC @RetStatus = spc_AmountVerify @OurBranchID, @AccountID, @TrxType, @Amt, @AccountType, @Supervision, 
            @PostingLimit, @SupervisorID, @LocEq
      

      IF @RetStatus = '97'
         SET @Supervision = 'C'
   ELSE IF @RetStatus = '98'
         SET @Supervision = '*'
      ELSE IF @RetStatus = '99'
         SET @Supervision = 'P'
      ELSE
         BEGIN
            SELECT @RetStatus as Status, 'V' as Supervision                       
            RETURN(1)
         END 

--return

      SELECT @ScrollNo = IsNull(Max(ScrollNo),0) + 1
      FROM t_CashTransactionModel
      WHERE OurBranchID = @OurBranchID

      SELECT @mDate = Convert(varchar(10),@wDate ,101) + ' ' + convert(varchar(20),GetDate(),114)


 INSERT INTO t_CashTransactionModel
  (ScrollNo,OurBranchID,AccountID,AccountName,ProductID,CurrencyID,AccountType,ValueDate,wDate,TrxType,ChequeID,ChequeDate, Amount, ForeignAmount,ExchangeRate,
DescriptionID,Description,BankCode,BranchCode,TrxPrinted,ProfitorLoss,Glid,Issupervision,Supervision,RemoteDescription,OperatorID,SupervisorID,IsLocalCurrency,Refno
  , AppWHTax, WHTaxMode, WHTaxAmount, WHTaxScrollNo,AdditionalData,UtilityNumber,AppChg,ChgAmount,ChgMode,ChgScrollNo)
 VALUES 
  (@ScrollNo,@OurBranchID,@AccountID,@AccountName,@ProductID,@CurrencyID,@AccountType,@ValueDate,@mDate,@TrxType,@ChequeID,@ChequeDate, @Amount, @ForeignAmount,@ExchangeRate,
  @DescriptionID,@Description,@BankCode,@BranchCode,@TrxPrinted,@ProfitorLoss,@Glid, @IsSupervision, @Supervision, @RemoteDescription,@OperatorID,@SupervisorID,@IsLocalCurrency,@Refno
  , @AppWHTax, @WHTaxMode, @WHTaxAmount,@WHTaxScrollNo,@AddData,@UtilityNumber,@AppChg,@ChgAmount,@ChgMode,@ChgScrollNo)


 IF @Refno =  '' OR left(@Refno,9)  = 'LOCALCASH' OR left(UPPER(@Refno),5)  = 'WHTAX' OR left(UPPER(@Refno),5)  = 'LRPAY'
     BEGIN
         IF  @Supervision <> 'M'
             BEGIN
                     IF @TrxType = 'D' AND @ChequeID <> 'V' AND  @ChequeID <> ''
                      BEGIN
   -- Get Validate Cheaque No
   EXEC @RetStatus  = spc_ValidateChequeNo @OurBranchID, @AccountID, @ChequeID, @ChequeID                  

          IF @RetStatus  <> 0
      BEGIN      
          SELECT @RetStatus  as Status,'C' as Supervision        
          RETURN(1)
       END

    INSERT INTO t_ChequePaid
        (OurBranchID, AccountID,ChequeID,Date,AccountType,CreateBy,CreateTime,CreateTerminal,SuperviseBy)                      
    VALUES
        (@OurBranchID,@AccountID, @ChequeID,@ValueDate,@AccountType,@OperatorID,@wDate,'',@SupervisorID)                      
END
    END   
               
            IF @Supervision = '*' OR @Supervision = 'P' 
                  BEGIN
                     IF @Supervision = 'P' 
SELECT @IsOverDraft = 1
                     ELSE
SELECT @IsOverDraft = 0

                IF @TrxType = 'D'
BEGIN
 IF @AccountType = 'G'
       BEGIN
         INSERT INTO t_CustomerStatus
              (ScrollNo,OurBranchID,AccountID,GLID,Amount,Rate,Status,AccountName,TransactionType,IsOverDraft,OperatorID)
         VALUES
              (@ScrollNo,@OurBranchID,@AccountID,@GLID,@Amt*-1,@ExchangeRate,@Supervision,@AccountName,'CG',@IsOverDraft,@OperatorID)
                    END
    ELSE
      BEGIN
        INSERT INTO t_CustomerStatus
               (ScrollNo,OurBranchID,AccountID,GLID,Amount,Rate,Status,AccountName,TransactionType,IsOverDraft,OperatorID)
         VALUES
               (@ScrollNo,@OurBranchID,@AccountID,@GLID,@Amt*-1,@ExchangeRate,@Supervision,@AccountName,'C',@IsOverDraft,@OperatorID)
      END
END
                     ELSE
BEGIN
   IF @AccountType = 'G'
      BEGIN
         INSERT INTO t_CustomerStatus
               (ScrollNo,OurBranchID,AccountID,GLID,Amount,Rate,Status,AccountName,TransactionType,IsOverDraft,OperatorID)
         VALUES
               (@ScrollNo,@OurBranchID,@AccountID,@GLID,@Amt,@ExchangeRate,@Supervision,@AccountName,'CG',@IsOverDraft,@OperatorID)
      END
              ELSE
      BEGIN
     INSERT INTO t_CustomerStatus
               (ScrollNo,OurBranchID,AccountID,GLID,Amount,Rate,Status,AccountName,TransactionType,IsOverDraft,OperatorID)
         VALUES
             (@ScrollNo,@OurBranchID,@AccountID,@GLID,@Amt,@ExchangeRate,@Supervision,@AccountName,'C',@IsOverDraft,@OperatorID)
      END
END
                  END
    END      

--print 'Supervision :' + @Supervision

-- Only Debit Trx In Shadow either its supervision or remote pass
-- so Credit Trx Comment its discussed 

--      IF @Supervision <> 'P' AND @AccountType = 'C'
      IF @AccountType = 'C'
         BEGIN
--print 'OK'
            EXEC @RetStatus  = spc_CashUpdateBalance @OurBranchID, @AccountID, @Amount,
         @ForeignAmount, @IsDebit,@Supervision, @IsLocalCurrency,0
--print 'RetStatus :' + cast ( @RetStatus as varchar(3))

            IF @RetStatus  <> 0
               BEGIN      
                  SELECT @RetStatus as Status,'B' as Supervision         
                  RETURN(1)                 
               END
         END

  IF left(UPPER(@Refno),5)  = 'LRPAY'
   BEGIN
             EXEC @RetStatus  = spc_UpdateRemittance @OurBranchID, @ScrollNo, @DateOfIssue,
         @BatchNo, @BatchInstrumentNo,@InstrumentType, @wDate, 'P', @PaidMode,             
         @Supervision, @SupervisorID            

             IF @RetStatus  <> 0
                 BEGIN      
                    SELECT @RetStatus as Status,'L' as Supervision         
                    RETURN(1)
                 END

   END
                
      if @Supervision = 'C'    
      begin    
            
        declare @IsCredit int    
        declare @VoucherID int    
        declare @TransactionMethod char(1)    
            
        if @AccountType = 'C'    
        begin    
               
           select @VoucherID = ISNULL(MAX(VoucherID),0) + 1 from t_GLTransactions where OurBranchID = @OurBranchID    
               
           insert into t_Transactions (OurBranchID, ScrollNo, SerialNo, RefNo, wDate, AccountType, DocumentType, AccountID, AccountName,    
           ProductID, CurrencyID, ValueDate, TrxType, ChequeID, ChequeDate, Amount, ForeignAmount, ExchangeRate, ProfitLoss, MeanRate,    
           DescriptionID, [Description], BankCode, BranchCode, TrxPrinted, GLID, [Status], IsLocalCurrency, OperatorID,    
           SupervisorID, UtilityNumber, AdditionalData, IsMainTrx, DocType, GLVoucherID, AppWHTax, WHTaxMode, WHTaxAmount)    
               
           VALUES (@OurBranchID,@ScrollNo,'0',@Refno,@mDate,@AccountType,'C',@AccountID,@AccountName,    
           @ProductID,@CurrencyID,@ValueDate,@TrxType,@ChequeID,@ChequeDate, @Amount, @ForeignAmount,@ExchangeRate,@ProfitorLoss,@ExchangeRate,    
           @DescriptionID,@Description,@BankCode,@BranchCode, 0, @Glid, @Supervision, @IsLocalCurrency, @OperatorID, @SupervisorID,     
           @UtilityNumber,@AddData,'1','C', @VoucherID, @AppWHTax, @WHTaxMode, @WHTaxAmount)    
               
           declare @GLControl nvarchar(30)    
               
           if @TrxType = 'C'    
           begin    
             set @IsCredit = 1    
             set @DescriptionID = 'CA0'    
             set @Description = 'Account ID: ' + @AccountID +', Account Name: ' + @AccountName +' , Module: Cash Credit Entry...'    
           end    
           else    
           begin    
             set @IsCredit = 0    
             set @DescriptionID = 'CA1'    
             set @Description = 'Account ID: ' + @AccountID +', Account Name: ' + @AccountName +' , Module: Cash Debit Entry...'    
           end    
               
           select @GLControl = ISNULL(GLControl, '') from t_Products where OurBranchID = @OurBranchID and ProductID = @ProductID    
               
           if @IsLocalCurrency = 1    
           begin    
             set @ForeignAmount = 0    
             set @ExchangeRate = 1    
             set @TransactionMethod = 'L'    
           end    
           else    
           begin    
             set @TransactionMethod = 'A'    
           end    
               
           insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],    
           CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,    
           Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType)    
               
           values (@OurBranchID, @GLControl, @VoucherID, '1', @mDate, @ValueDate, @DescriptionID, @Description,    
           @CurrencyID, @Amount, @ForeignAmount, @ExchangeRate, @IsCredit, 'Cash', @TransactionMethod, @OperatorID, @SupervisorID,    
           '', '0', '0', @AddData, '0', 'C')    
               
           if @TrxType = 'C'    
           begin    
                 
             set @IsCredit = 0    
             if (left(@RefNo,9) = 'LOCALCASH')    
             begin    
               set @DescriptionID = 'CA3'    
               set @TransactionMethod = 'L'    
             end    
             else    
             begin    
               set @DescriptionID = 'CA4'    
             end    
                 
           end    
           else    
           begin    
             set @IsCredit = 1    
             set @DescriptionID = 'CA2'    
           end    
               
           insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],    
           CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,    
           Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType)    
               
           values (@OurBranchID, @Glid, @VoucherID, '2', @mDate, @ValueDate, @DescriptionID, @Description,    
           @CurrencyID, @Amount, @ForeignAmount, @ExchangeRate, @IsCredit, 'Cash', @TransactionMethod, @OperatorID, @SupervisorID,    
           '', '0', '0', @AddData, '0', 'C')    
               
        end    
        if @AccountType = 'G'    
        begin    
               
           select @VoucherID = ISNULL(MAX(VoucherID),0) + 1 from t_GLTransactions where OurBranchID = @OurBranchID    
               
           if @TrxType = 'C'    
             set @IsCredit = 1    
           else    
             set @IsCredit = 0    
               
           if @IsLocalCurrency = 1    
           begin    
             set @ForeignAmount = 0    
             set @ExchangeRate = 1    
             set @TransactionMethod = 'L'    
           end    
           else    
           begin    
             set @TransactionMethod = 'A'    
           end    
               
           insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],    
           CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,    
           Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType, GLID)    
               
           values (@OurBranchID, @AccountID, @VoucherID, '1', @mDate, @ValueDate, @DescriptionID, @Description,    
           @CurrencyID, @Amount, @ForeignAmount, @ExchangeRate, @IsCredit, 'Cash', @TransactionMethod, @OperatorID, @SupervisorID,    
           '', @ScrollNo, '0', @AddData, '1', 'C', @GLID)    
               
           if @TrxType = 'C'    
           begin    
                 
             set @IsCredit = 0    
             set @Description = 'Account ID: ' + @AccountID +', Account Name: ' + @AccountName +' , Module: Cash Debit Entry...'    
             if (left(@RefNo,9) = 'LOCALCASH')    
             begin    
               set @DescriptionID = 'CA3'    
               set @TransactionMethod = 'L'    
             end    
             else    
             begin    
               set @DescriptionID = 'CA4'    
             end    
                 
           end    
           else    
           begin    
             set @IsCredit = 1    
             set @DescriptionID = 'CA2'    
             set @Description = 'Account ID: ' + @AccountID +', Account Name: ' + @AccountName +' , Module: Cash Debit Entry...'    
           end    
               
           insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],    
           CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,    
           Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType)    
               
           values (@OurBranchID, @Glid, @VoucherID, '2', @mDate, @ValueDate, @DescriptionID, @Description,    
           @CurrencyID, @Amount, @ForeignAmount, @ExchangeRate, @IsCredit, 'Cash', @TransactionMethod, @OperatorID, @SupervisorID,    
           '', '0', '0', @AddData, '0', 'C')    
               
        end    
      end    
          
      SELECT '0' as Status,@Supervision as Supervision , @ScrollNo as ScrollNo    
      RETURN(0)    
          
   END