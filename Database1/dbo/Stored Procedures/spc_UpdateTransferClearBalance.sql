﻿CREATE    PROCEDURE [dbo].[spc_UpdateTransferClearBalance]          
(           
 @OurBranchID varchar(30),           
 @AccountID varchar(30),           
 @SupervisorID varchar(30),           
 @Refno varchar(100)='',           
 @ScrollNo money,           
 @SerialNo money,           
 @AddData text = ''           
)           
           
AS           
 SET NOCOUNT ON           
           
 DECLARE @vDate datetime           
 DECLARE @wDate datetime           
 DECLARE @mDate Varchar(32)           
 DECLARE @ChequeID varchar(11)           
 DECLARE @ClearBalance money           
 DECLARE @Effects money           
 DECLARE @Limit money           
 DECLARE @TrxType char(1)           
 DECLARE @Amount money           
 DECLARE @ForeignAmount money           
 DECLARE @IsLocalCurrency bit           
 DECLARE @Status char(1)           
 DECLARE @AccountType char(1)           
           
 DECLARE @VoucherID decimal(24,0)           
 DECLARE @GLID varchar(30)           
 DECLARE @tGLID varchar(30)           
 DECLARE @TrxPrinted int           
 DECLARE @ProductID varchar(30)           
 DECLARE @GLControl varchar(30)           
 DECLARE @AccountName varchar(100)           
 DECLARE @CurrencyID varchar(30)           
 DECLARE @OperatorID varchar(30)           
 DECLARE @ChequeDate varchar(30)           
 DECLARE @DescriptionID varchar(30)           
 DECLARE @Description varchar(100)           
 DECLARE @BankCode varchar(30)           
 DECLARE @BranchCode varchar(30)           
 DECLARE @ExchangeRate decimal(24,6)           
 DECLARE @AppWHTax int           
 DECLARE @WHTScrollNo int           
 Declare @ChannelRefID varchar(100)        
 Declare @VatID nvarchar(30)      
 Declare @Percent money      
 Declare @PrevYearEntry int      
 Declare @GLVendorID nvarchar(30)    
 Declare @memo2 nvarchar(250)    
 Declare @TClientId nvarchar(30)      
 Declare @TAccountid nvarchar(30)      
 Declare @TProductId nvarchar(30)      
 Declare @CostCenterID nvarchar(30)      
 Declare @VendorID nvarchar(30)      
 Declare @CostTypeID nvarchar(30)           
 Declare @CustomerLifecycleID nvarchar(30)      
 Declare @PostExpenseID nvarchar(30)      
 Declare @VatReferenceID nvarchar(30)      
           
 /* Get Transfer Transaction */           
 SELECT @wDate=wDate, @vDate=ValueDate,@ChequeID= ChequeID,@AccountType=AccountType,@TrxType=TrxType,@AccountName=AccountName,@Amount = Amount,           
 @ForeignAmount = ForeignAmount,@IsLocalCurrency=IsLocalCurrency,@Status = Supervision,@ProductID=ProductID,@CurrencyID = CurrencyID,           
 @ChequeDate=ChequeDate,@ExchangeRate=ExchangeRate,@DescriptionID=DescriptionID,@Description=Description,@TrxPrinted=TrxPrinted,           
 @BankCode=BankCode,@BranchCode=BranchCode,@AddData=AdditionalData,@GLID=GLID,@OperatorID=OperatorID,@AppWHTax=AppWHTax,@WHTScrollNo=WHTaxScrollNo,@ChannelRefID = ChannelRefID,@VatID = VatID, @Percent = [Percent] , @PrevYearEntry = PrevYearEntry      
 ,@GLVendorID = GLVendorID, @memo2 = memo2 ,@TClientId=TClientId ,@TAccountid = TAccountid  ,@TProductId = TProductId,@CostCenterID=CostCenterID,@VendorID=VendorID,
 @CostTypeID=CostTypeID,@CustomerLifecycleID=CustomerLifecycleID,@PostExpenseID=PostExpenseID,@VatReferenceID=VatReferenceID
 FROM t_TransferTransactionModel           
 WHERE (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (ScrollNo = @ScrollNo) and (SerialNo = @SerialNo)           
           
 set @tGLID = @GLID           
 if @tGLID = ''           
 set @tGLID = NULL           
           
 SELECT @mDate = Convert(varchar(10),@wDate ,101) + ' ' + convert(varchar(20),GetDate(),114)           
           
 IF @AccountType = 'C'           
 BEGIN           
 /* Get Current ClearBalance, Effective Balance and Limit */           
 SELECT @ClearBalance=ClearBalance,@Effects=Effects,@limit=Limit           
 FROM t_AccountBalance           
 WHERE (OurBranchID = @OurBranchID) AND (AccountID = @AccountID)           
 END           
 ELSE           
 SELECT @ClearBalance=0,@Effects=0,@limit=0           
           
           
 IF @Status = '*'           
 BEGIN           
 SELECT @Status='C'           
           
 declare @TrxRestriction char(1),           
 @CrDailyCount decimal(24,0), @DrDailyCount decimal(24,0), @CrDailyThreshold decimal(21,3), @DrDailyThreshold decimal(21,3),           
 @CrMonthlyCount decimal(24,0), @DrMonthlyCount decimal(24,0), @CrMonthlyThreshold decimal(21,3), @DrMonthlyThreshold decimal(21,3),           
 @cCrDailyCount decimal(24,0), @cDrDailyCount decimal(24,0), @cCrDailyThreshold decimal(21,3), @cDrDailyThreshold decimal(21,3),           
 @cCrMonthlyCount decimal(24,0), @cDrMonthlyCount decimal(24,0), @cCrMonthlyThreshold decimal(21,3), @cDrMonthlyThreshold decimal(21,3)           
           
 select @TrxRestriction = ISNULL(TrxRestrictions, 'N'),           
 @CrDailyCount = ISNULL(NoOfCrTrx, 0), @DrDailyCount = ISNULL(NoOfDrTrx, 0), @CrDailyThreshold = ISNULL(AmountOfCrTrx, 0), @DrDailyThreshold = ISNULL(AmountOfDrTrx, 0),           
 @CrMonthlyCount = ISNULL(NoOfCrTrxMonthly, 0), @DrMonthlyCount = ISNULL(NoOfDrTrxMonthly, 0), @CrMonthlyThreshold = ISNULL(AmountOfCrTrxMonthly, 0), @DrMonthlyThreshold = ISNULL(AmountOfDrTrxMonthly, 0)           
 from t_Products where OurBranchID = @OurBranchID and ProductID = @ProductID           
           
 IF @TrxRestriction = 'Y'           
 BEGIN           
           
 select           
 @cCrDailyCount = NoOfCrTrx, @cDrDailyCount = NoOfDrTrx, @cCrDailyThreshold = AmountOfCrTrx, @cDrDailyThreshold = AmountOfDrTrx,           
 @cCrMonthlyCount = NoOfCrTrxMonthly, @cDrMonthlyCount = NoOfDrTrxMonthly, @cCrMonthlyThreshold = AmountOfCrTrxMonthly, @cDrMonthlyThreshold = AmountOfDrTrxMonthly           
 from t_AccountBalance           
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID           
           
 if @TrxType = 'C'           
 BEGIN           
           
 if (@cCrDailyCount + 1) > @CrDailyCount           
 begin           
 SELECT '31' as RetStatus           
 RETURN(1)           
 end           
 if (@cCrDailyThreshold + @Amount) > @CrDailyThreshold           
 begin           
 SELECT '32' as RetStatus           
 RETURN(1)           
 end           
 if (@cCrMonthlyCount + 1) > @CrMonthlyCount           
 begin           
 SELECT '33' as RetStatus           
 RETURN(1)           
 end           
 if (@cCrMonthlyThreshold + @Amount) > @CrMonthlyThreshold           
 begin           
 SELECT '34' as RetStatus           
 RETURN(1)           
 end           
           
 END           
 ELSE           
 BEGIN           
           
 if (@cDrDailyCount + 1) > @DrDailyCount           
 begin           
 SELECT '35' as RetStatus           
 RETURN(1)           
 end           
 if (@cDrDailyThreshold + @Amount) > @DrDailyThreshold           
 begin           
 SELECT '36' as RetStatus           
 RETURN(1)           
 end           
 if (@cDrMonthlyCount + 1) > @DrMonthlyCount           
 begin           
 SELECT '37' as RetStatus           
 RETURN(1)           
 end           
 if (@cDrMonthlyThreshold + @Amount) > @DrMonthlyThreshold           
 begin           
 SELECT '38' as RetStatus           
 RETURN(1)           
 end           
           
 END           
 END           
           
 if @TrxType = 'C'           
 BEGIN           
 UPDATE t_AccountBalance SET           
 NoOfCrTrx = NoOfCrTrx+1, AmountOfCrTrx = AmountOfCrTrx+@Amount,           
 NoOfCrTrxMonthly = NoOfCrTrxMonthly+1, AmountOfCrTrxMonthly = AmountOfCrTrxMonthly+@Amount           
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID           
 END           
 ELSE           
 BEGIN           
 UPDATE t_AccountBalance SET           
 NoOfDrTrx = NoOfDrTrx+1, AmountOfDrTrx = AmountOfDrTrx+@Amount,           
 NoOfDrTrxMonthly = NoOfDrTrxMonthly+1, AmountOfDrTrxMonthly = AmountOfDrTrxMonthly+@Amount           
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID           
 END           
           
 /*UpDate Transaction File Status*/           
 UPDATE t_TransferTransactionModel           
 SET Supervision = @Status, SupervisorID = @SupervisorID           
 WHERE (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (ScrollNo = @ScrollNo) and (SerialNo = @SerialNo)           
           
 /* Create a New record For Superviser */           
 INSERT INTO t_SupervisedBy           
 (OurBranchID,AccountID,[Date],ScrollNo,SerialNo,ChequeID,ClearBalance,Effects,Limit,TrxType,Amount,Status,SupervisorID,TransactionCategory)           
 VALUES           
 (@OurBranchID,@AccountID,@mDate,@ScrollNo,@SerialNo,@ChequeID,@ClearBalance,@Effects,@Limit,@TrxType,@Amount,@Status,@SupervisorID,'T')           
           
 IF @AccountType = 'C'           
           
 BEGIN /* Update Shadow Balance */           
 IF @TrxType='C'           
 BEGIN           
 IF @IsLocalCurrency = 1 AND @vDate > @wDate           
 Update t_AccountBalance           
 Set --ShadowBalance = ShadowBalance - @Amount,           
 Effects = Effects + @Amount           
 Where OurBranchID = @OurBranchID and AccountID = @AccountID           
           
 ELSE IF @IsLocalCurrency = 1 AND @vDate <= @wDate      
 Update t_AccountBalance           
 Set --ShadowBalance = ShadowBalance - @Amount,           
 ClearBalance = ClearBalance + @Amount           
 Where OurBranchID = @OurBranchID and AccountID = @AccountID           
           
 ELSE IF @IsLocalCurrency = 0 AND @vDate > @wDate           
 Update t_AccountBalance           
 Set --ShadowBalance = ShadowBalance - @ForeignAmount,           
 Effects = Effects + @ForeignAmount,           
 LocalEffects = LocalEffects + @Amount           
 Where OurBranchID = @OurBranchID and AccountID = @AccountID           
           
 ELSE IF @IsLocalCurrency = 0 AND @vDate <= @wDate           
 Update t_AccountBalance           
 Set --ShadowBalance = ShadowBalance - @ForeignAmount,           
 ClearBalance = ClearBalance + @ForeignAmount,           
 LocalClearBalance = LocalClearBalance + @Amount           
 Where OurBranchID = @OurBranchID and AccountID = @AccountID           
 END           
 ELSE IF @TrxType='D'           
 BEGIN           
 IF @IsLocalCurrency = 1           
           
 Update t_AccountBalance           
 Set ShadowBalance = ShadowBalance + @Amount           
 ,ClearBalance = ClearBalance - @Amount           
 Where OurBranchID = @OurBranchID and AccountID = @AccountID           
           
 ELSE IF @IsLocalCurrency = 0           
 Update t_AccountBalance           
 Set ShadowBalance = ShadowBalance + @ForeignAmount           
 ,ClearBalance = ClearBalance - @ForeignAmount,           
 LocalClearBalance = LocalClearBalance - @Amount           
 Where OurBranchID = @OurBranchID and AccountID = @AccountID           
 END           
 END           
           
 /* Update Customer Status */           
 Update t_CustomerStatus           
 Set Status = @Status           
 Where (OurBranchID=@OurBranchID) and (AccountID = @AccountID)           
 and (ScrollNo = @ScrollNo) and (Left(TransactionType,1)='T') and (SerialNo = @SerialNo)           
           
 /** rameez starts **           
           
 /*@Update OtherTransaction Table For Transfer*/           
 Update t_OtherTransactionStatus           
 Set Status = @Status           
 Where (OurBranchID=@OurBranchID) and (AccountID = @AccountID) and (ScrollNo = @ScrollNo)           
 and (Left(TransactionType,1)='T') and (SerialNo = @SerialNo)           
           
----------------------------------------- ** rameez endss **/           
           
--Local Remittance Reject           
           
 if ((select COUNT(*) from t_TransferTransactionModel           
 Where (OurBranchID=@OurBranchID) and (ScrollNo = @ScrollNo) and Supervision <> 'C') = 0)           
 begin           
           
 Update t_LR_Issuance           
 Set Supervision = @Status, SuperviseBy = @SupervisorID, SuperviseTime = @mDate           
 Where (OurBranchID=@OurBranchID) and (ScrollNo = @ScrollNo)           
 and (convert(varchar(10), wDate, 101) = convert(varchar(10), @wDate, 101))           
           
 Update t_LR_Issuance           
 Set Supervision = @Status, SuperviseBy = @SupervisorID, SuperviseTime = @mDate           
 Where (OurBranchID=@OurBranchID) and (CancellScrollNo = @ScrollNo) AND Cancel = '1'           
 and (convert(varchar(10), CancelDate, 101) = convert(varchar(10), @wDate, 101))           
           
 end           
           
 if ((select COUNT(*) from t_TransferTransactionModel Where (OurBranchID=@OurBranchID)           
 and UPPER(LEFT(RefNo,5)) = 'LRPAY' and (ScrollNo = @ScrollNo) and Supervision <> 'C') = 0)           
 begin           
           
 Update t_LR_Issuance           
 Set Supervision = @Status, SuperviseBy = @SupervisorID, SuperviseTime = @mDate           
 Where (OurBranchID=@OurBranchID) and (PaidScrollNo = @ScrollNo) and PaidMode = 'TR'           
 and (convert(varchar(20), PaidDate, 101) = convert(varchar(20), @wDate, 101))           
           
 end           
           
/** rameez starts **           
           
--Local Remittance Advice Reject           
 Update t_LR_Advice           
 Set Supervision = @Status           
 Where (OurBranchID=@OurBranchID) and (ScrollNo = @ScrollNo) and (wDate = @wDate)           
--------------------------------------------           
-----------------           
           
** rameez endss **/           
           
           
---INLAND OUTWARD LODGEMENT ENTRY REJECTED           
           
           
           
-----------------           
           
 select @VoucherID = @ScrollNo --ISNULL(MAX(VoucherID),0) + 1 from t_GLTransactions where OurBranchID = @OurBranchID           
           
 declare @IsCredit int           
 declare @TransactionMethod char(1)           
           
 if @AccountType = 'C'           
 begin           
           
 insert into t_Transactions (OurBranchID, ScrollNo, SerialNo, RefNo, wDate, AccountType, DocumentType, AccountID, AccountName,           
 ProductID, CurrencyID, ValueDate, TrxType, ChequeID, ChequeDate, Amount, ForeignAmount, ExchangeRate, ProfitLoss, MeanRate,           
 DescriptionID, [Description], BankCode, BranchCode, TrxPrinted, GLID, [Status], IsLocalCurrency, OperatorID,           
 SupervisorID, AdditionalData, IsMainTrx, DocType, GLVoucherID, AppWHTax, WHTaxMode, WHTaxAmount, WHTScrollNo,VatID, [Percent], PrevYearEntry,GLVendorID,memo2)           
           
 VALUES (@OurBranchID,@ScrollNo,@SerialNo,@RefNo,@wDate,@AccountType,'T',@AccountID,@AccountName,           
 @ProductID,@CurrencyID,@vDate,@TrxType,@ChequeID,@ChequeDate, @Amount, @ForeignAmount,@ExchangeRate,0,@ExchangeRate,           
 @DescriptionID,@Description,@BankCode,@BranchCode, @TrxPrinted, @tGLID, 'C', @IsLocalCurrency, @OperatorID, @SupervisorID,           
 @AddData,'1','T', @VoucherID, @AppWHTax, 'T', '0', @WHTScrollNo,@VatID, @Percent, @PrevYearEntry,@GLVendorID,@memo2)           
           
 if @TrxType = 'C'           
 begin           
 set @IsCredit = 1           
 set @DescriptionID = 'TR0'           
 set @Description = 'Account ID: ' + @AccountID +', Account Name: ' + @AccountName +', Module: Transfer Credit Entry...'           
 end           
 else           
 begin           
 set @IsCredit = 0           
 set @DescriptionID = 'TR1'           
 set @Description = 'Account ID: ' + @AccountID +', Account Name: ' + @AccountName +' , Module: Transfer Debit Entry...'           
 end           
           
 SELECT @GLControl = ISNULL(GLControl, '') from t_Products where OurBranchID = @OurBranchID and ProductID = @ProductID           
           
 if @IsLocalCurrency = 1           
 begin           
 set @ForeignAmount = 0           
 set @ExchangeRate = 1           
 set @TransactionMethod = 'L'           
 end           
 else           
 begin           
 set @TransactionMethod = 'A'           
 end           
           
 insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],           
 CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,           
 Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType,ChannelRefID,VatID, [Percent], PrevYearEntry,GLVendorID,memo2,TClientId,TAccountid,TProductId,CostCenterID,VendorID,
 CostTypeID,CustomerLifecycleID,PostExpenseID,VatReferenceID)           
           
 values (@OurBranchID, @GLControl, @VoucherID, '1', @wDate, @vDate, @DescriptionID, @Description,           
 @CurrencyID, @Amount, @ForeignAmount, @ExchangeRate, @IsCredit, 'Transfer', @TransactionMethod, @OperatorID, @SupervisorID,           
 '', '0', '0', @AddData, '0', 'T',@ChannelRefID,@VatID, @Percent, @PrevYearEntry,@GLVendorID,@memo2,@TClientId,@TAccountid,@TProductId,@CostCenterID,@VendorID,
 @CostTypeID,@CustomerLifecycleID,@PostExpenseID,@VatReferenceID)           
           
 end           
 else           
 begin           
           
 if @TrxType = 'C'           
 set @IsCredit = 1           
 else           
 set @IsCredit = 0           
           
 if @IsLocalCurrency = 1           
 begin           
 set @ForeignAmount = 0           
 set @ExchangeRate = 1           
 set @TransactionMethod = 'L'           
 end           
 else           
 begin           
 set @TransactionMethod = 'A'           
 end           
           
 insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],           
 CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,           
 Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType,ChannelRefID,VatID, [Percent], PrevYearEntry,GLVendorID,memo2,TClientId,TAccountid,TProductId,
 CostCenterID,VendorID,CostTypeID,CustomerLifecycleID,PostExpenseID,VatReferenceID)           
           
 values (@OurBranchID, @AccountID, @VoucherID, '1', @wDate, @vDate, @DescriptionID, @Description,           
 @CurrencyID, @Amount, @ForeignAmount, @ExchangeRate, @IsCredit, 'Transfer', @TransactionMethod, @OperatorID, @SupervisorID,           
 '', @ScrollNo, @SerialNo, @AddData, '1', 'T',@ChannelRefID,@VatID, @Percent, @PrevYearEntry,@GLVendorID,@memo2,@TClientId,@TAccountid,@TProductId,@CostCenterID,@VendorID,
 @CostTypeID,@CustomerLifecycleID,@PostExpenseID,@VatReferenceID)           
           
 end           
           
 SELECT '0' as RetStatus           
 RETURN           
 END           
           
/** rameez starts **           
           
IF @REFNO='OBC-I'           
 BEGIN           
 UPDATE t_FrInlandOutward           
 SET STATUS='C'           
 WHERE (Transferno=CONVERT(VARCHAR(10),CONVERT(NUMERIC,@SCROLLNO))) AND TRDate=@WDATE           
           
           
           
 UPDATE t_FrInlandOutward           
 SET STATUS='C'           
 WHERE (ReTransferNo=CONVERT(VARCHAR(10),CONVERT(NUMERIC,@SCROLLNO))) AND ReTRDate=@WDATE           
 END           
           
---INLAND INWARD LODGEMENT ENTRY REJECTED           
ELSE IF @REFNO='IBC-I'           
 BEGIN           
 UPDATE t_FrInlandInward           
 SET STATUS='C'           
 WHERE (Transferno=CONVERT(VARCHAR(10),CONVERT(NUMERIC,@SCROLLNO))) AND TRDate=@WDATE           
           
 UPDATE t_FrInlandInward           
 SET STATUS='C'           
 WHERE (ReTransferNo=CONVERT(VARCHAR(10),CONVERT(NUMERIC,@SCROLLNO))) AND ReTRDate=@WDATE           
 END           
           
---FOREIGN OUTWARD LODGEMENT ENTRY REJECTED           
ELSE IF @REFNO='FOB=I'           
 BEGIN           
 UPDATE t_FrOutlandOutward           
 SET STATUS='C'           
 WHERE (Transferno=CONVERT(VARCHAR(10),CONVERT(NUMERIC,@SCROLLNO))) AND TRDate=@WDATE           
           
 UPDATE t_FrOutlandOutward           
 SET STATUS='C'           
 WHERE (ReTransferNo=CONVERT(VARCHAR(10),CONVERT(NUMERIC,@SCROLLNO))) AND ReTRDate=@WDATE           
 END           
           
---FOREIGN INWARD LODGEMENT ENTRY REJECTED           
ELSE IF @REFNO='FIB=I'           
 BEGIN           
 UPDATE t_FrOutlandInward           
 SET STATUS='C'           
 WHERE (Transferno=CONVERT(VARCHAR(10),CONVERT(NUMERIC,@SCROLLNO))) AND TRDate=@WDATE           
           
 UPDATE t_FrOutlandInward           
 SET STATUS='C'           
 WHERE (ReTransferNo=CONVERT(VARCHAR(10),CONVERT(NUMERIC,@SCROLLNO))) AND ReTRDate=@WDATE           
 END           
---           
** rameez ends **/           
           
 ELSE           
 BEGIN           
 IF @Status = 'C'           
 BEGIN           
 SELECT '2' as RetStatus           
 RETURN           
 END           
           
 IF @Status = 'R'           
 BEGIN           
 SELECT '3' as RetStatus           
 RETURN           
 END           
           
 SELECT '4' as RetStatus           
 RETURN           
 END