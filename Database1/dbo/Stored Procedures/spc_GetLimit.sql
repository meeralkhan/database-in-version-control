﻿CREATE PROCEDURE [dbo].[spc_GetLimit]          
(          
  @OurBranchID nvarchar(30),          
  @AccountID nvarchar(30)          
)          
AS          
          
SET NOCOUNT ON          
          
declare @LimitType nvarchar(10)          
          
select @LimitType = LimitType from t_GlobalVariables where OurBranchID = @OurBranchID AND ParameterID = 1;          
          
if (@LimitType = 'Account')          
begin          
   select 'Ok' AS ReturnStatus, Limit from t_AccountBalance where OurBranchID = @OurBranchID AND AccountID = @AccountID;          
   return;          
end          
          
declare @ClientID nvarchar(30)          
declare @ProductID nvarchar(30)          
          
select @ClientID = ClientID, @ProductID = ProductID from t_Account where OurBranchID = @OurBranchID AND AccountID = @AccountID;          
          
declare @FID decimal(24,0)          
declare @GID nvarchar(30)          
declare @PID nvarchar(30)          
declare @LType nvarchar(30)          
declare @DP money          
declare @tDP money          
declare @obt money          
declare @accts nvarchar(max)          
          
declare @Limit money          
declare @Util money          
set @Limit = 0          
set @Util = 0          
set @accts = ''          
        
CREATE TABLE #Facilities        
(        
   FacilityId INT,        
   SerialNo DECIMAL(24,0),        
   GroupID NVARCHAR(30),        
   ProductID NVARCHAR(30),        
   LimitType NVARCHAR(30),        
   FacilityDP MONEY        
)        
        
INSERT INTO #Facilities        
select ROW_NUMBER() OVER(ORDER BY l.SerialNo), l.SerialNo, l.GroupID, p.ProductID, p.LimitType,    
case when ISNULL(l.FacilityDP,0) >= ISNULL(p.Limit,0) then ISNULL(p.Limit,0) else ISNULL(l.FacilityDP,0) end    
from v_Facilities l          
inner join t_FacilityProduct p on l.OurBranchID = p.OurBranchID and l.ClientID = p.ClientID and l.SerialNo = p.FacilityID and p.Status = ''          
where l.OurBranchID = @OurBranchID          
AND l.ClientID = @ClientID          
and ((l.Status = '' and Status2 = '') or (l.Status = 'C' and Status2 = 'A') or (l.Status = 'B' and Status2 = 'A'));        
        
DECLARE @TotalRows INT = (SELECT COUNT(*) FROM #Facilities), @i INT=1        
        
WHILE @i<=@TotalRows        
BEGIN        
           
   SELECT @FID = SerialNo, @GID = GroupID, @PID = ProductID, @LType = LimitType, @DP = FacilityDP from #Facilities WHERE FacilityId=@i        
        
   if exists (select SerialID from t_LinkFacilityAccount where OurBranchID = @OurBranchID AND ClientID = @ClientID AND FacilityID = @FID AND AccountID = @AccountID)          
   begin          
   if @PID = @ProductID          
   begin          
      select @tDP = ISNULL(Limit,0) from t_LinkFacilityAccount where OurBranchID = @OurBranchID AND ClientID = @ClientID AND FacilityID = @FID AND AccountID = @AccountID          
      if @DP >= @tDP          
        set @Limit = @Limit + @tDP;          
      else          
        set @Limit = @Limit + @DP;          
   end          
             
   select @accts = @accts + ',' + @AccountID          
             
   end          
   else          
   begin          
      if @PID = @ProductID          
   begin          
      if exists (select SerialID from t_LinkFacilityAccount where OurBranchID = @OurBranchID AND ClientID = @ClientID AND FacilityID = @FID AND AccountID <> @AccountID)          
      begin          
         select @tDP = ISNULL(Limit,0) from t_LinkFacilityAccount where OurBranchID = @OurBranchID AND ClientID = @ClientID AND FacilityID = @FID AND AccountID <> @AccountID          
      end          
      else          
      begin          
         select @tDP = 0          
      end          
                
      if @DP >= @tDP          
        set @Limit = @Limit + @DP - @tDP;          
      else          
        set @Limit = @Limit + 0;          
   end          
          
   select @accts = @accts + ',' + COALESCE(@accts, ',', '') +','+ ISNULL(ab.AccountID,',') from t_AccountBalance ab          
   inner join t_Account a on ab.OurBranchID = a.OurBranchID and ab.AccountID = a.AccountID          
   where ab.OurBranchID = @OurBranchID AND a.ClientID = @ClientID AND ab.ProductID = @PID          
   AND ab.AccountID NOT IN (select AccountID from t_LinkFacilityAccount where OurBranchID = @OurBranchID AND ClientID = @ClientID AND FacilityID = @FID)          
          
   end          
           
   SET @i=@i+1        
        
END          
        
select @Util = ISNULL(SUM(case when ClearBalance >= 0 then 0 else ClearBalance end),0) from t_AccountBalance          
where OurBranchID = @OurBranchID          
and AccountID != @AccountID          
and AccountID IN (select [Name] from Split(@accts,','))          
      
if @Limit + @Util < 0      
   select 'Ok' AS ReturnStatus, @Limit AS DP, @Util AS Utilized, 0.00 AS OutstandingDP      
else      
   select 'Ok' AS ReturnStatus, @Limit AS DP, @Util AS Utilized, @Limit+@Util AS OutstandingDP