﻿CREATE PROCEDURE [dbo].[sp_ATM_AddEditBillPayment]
(
 @OurBranchID		   varchar(30),	
 @WithdrawlBranchID	   varchar(30)='',	
 @CustomerBranchID	   varchar(30),
 @CustomerBranchID2	   varchar(30)='',
 @ATMID		           varchar(30),	
 @AccountID 	   	   varchar(30),
 @AccountID2 	   	   varchar(30)='',
 @Amount 		       money,
 @USDAmount		       money=0,
 @OtherCurrencyAmount  money=0,
 @USDRate	    	   money=0,
 @Supervision		   char(1),	
 @RefNo 		       varchar(36),
 @PHXDate 		       datetime,
 @MerchantType	       varchar(30),
 @OurIMD		       varchar(30),
 @AckInstIDCode	       varchar(30),
 @Currency		       varchar(3),
 @NameLocation	       varchar(40),
 @MCC			       varchar(30),
 @CompanyNumber        varchar(30) ,
 @CreditCardNumber     varchar(50)='',
 @TrxDesc              varchar(30)='',
 @NewRecord		       bit=1
)
AS
BEGIN
 DECLARE @BankShortName as varchar(30)
 DECLARE @mScrollNo as int
 DECLARE @mSerialNo as smallint
  SET @mSerialNo = 0
 DECLARE @mAccountID as varchar(30)
 DECLARE @mAccountName as varchar(50)
 DECLARE @mAccountType as char
 DECLARE @mProductID as varchar(30)
 DECLARE @mCurrencyID as varchar(30)
 DECLARE @STAN as varchar(30)
 DECLARE @mIsLocalCurrency as bit
  SET @mIsLocalCurrency=1	
 DECLARE @mwDate as datetime
 DECLARE @mTrxType as char
 DECLARE @mGLID as varchar(30)
  SET @mGLID = ''	
 DECLARE @mForeignAmount as money
  SET @mForeignAmount=0	
 DECLARE @mExchangeRate as money
  SET @mExchangeRate=1	
 DECLARE @mDescriptionID as varchar(3)
  SET @mDescriptionID='000'	
 DECLARE @mDescription as varchar(255)
 DECLARE @mDescriptionH as varchar(255)
  SET @BankShortName = 'N/A'
  
 SELECT @BankShortName = BankShortName From t_ATM_Banks WHERE (OurBranchID = @OurBranchID AND BankIMD = @OurIMD)
 
 IF @MerchantType <> '0000' 
 BEGIN
  IF @Currency = '586'
  BEGIN
   SET @mDescription='INT.BK. BILL PAID AT BK.' + @BankShortName  + ' (BR.' + @CustomerBranchID + ')' -- TO : A/c # ' + @AccountID2 
   SET @mDescriptionH='INT.BK. BILL PAID AT BK.' + @BankShortName   + ' (BR.' + @CustomerBranchID + ') From : A/c # ' + @AccountID
  END
  ELSE
  BEGIN
   IF @Currency = '840'
   BEGIN
    SET @mDescription='INT.BK. BILL PAID ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') AT ' + @NameLocation
	SET @mDescriptionH='INT.BK. BILL PAID ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') AT ' +  + @NameLocation + ' From : A/c # ' + @AccountID
   END
   ELSE 
   BEGIN
    SET @mDescription='INT.BK. BILL PAID(' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) AT ' + @NameLocation 
	SET @mDescriptionH='INT.BK. BILL PAID (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) AT ' +  + @NameLocation + ' From : A/c # ' + @AccountID
   END
  END
 END
 ELSE 
 BEGIN
  SET @mDescription='IB(' + @ATMID + ') BILL PAID AT BK.' + @BankShortName  + ' (BR.' + @WithdrawlBranchID + '))'
  SET @mDescriptionH='ATM (' + @ATMID + ') BILL PAID AT BK.' + @BankShortName   + ' (BR.' + @CustomerBranchID + ') From : A/c # ' + @AccountID
 END
 
 DECLARE @mRemarks as varchar(200)
 DECLARE @mBankCode as varchar(30)
  SET @mBankCode='1279'	
 DECLARE @mOperatorID as varchar(30)
  SET @mOperatorID = 'ATM-' + @ATMID
 DECLARE @mSupervisorID as varchar(30)
  SET @mSupervisorID = @OurIMD	
 
 --Checking Variables
 DECLARE @vCount1 as int
 DECLARE @vCount2 as bit
 DECLARE @vCount3 as bit
 DECLARE @vCount4 as bit
 DECLARE @vAccountID as varchar(30)
 DECLARE @vClearBalance as money
 DECLARE @vClearBalance2 as money
 DECLARE @vClearedEffects as money
 DECLARE @vFreezeAmount as money
 DECLARE @vMinBalance as money
 DECLARE @limit as money
 DECLARE @vIsClose as char
 DECLARE @vAreCheckBooksAllowed as bit
 DECLARE @vAllowDebitTransaction as bit
 DECLARE @vBranchName as varchar(50)
  SET @vBranchName = 'N/A'
 DECLARE @Status as int	
 DECLARE @mLastEOD as datetime
 -- DECLARE @mWorkingDate as datetime
 DECLARE @mCONFRETI as datetime
 DECLARE @mWithdrawlBranchName as varchar(50)
 DECLARE @WithDrawlCharges as money
  SET @WithDrawlCharges = 0
 DECLARE @WithDrawlChargesPercentage as money
  SET @WithDrawlChargesPercentage = 0
 DECLARE @IssuerAccountID as varchar(30)
 DECLARE @IssuerAccountTitle as varchar(50)
 DECLARE @AcquiringAccountID as varchar(30)
 DECLARE @AcquiringAccountTitle as varchar(50)
 DECLARE @IssuerChargesAccountID as varchar(30)
 DECLARE @IssuerTitle as varchar(50)
 DECLARE @AcquiringChargesAccountID as varchar(30)
 DECLARE @AcquiringTitle as varchar(50)
 DECLARE @ExcDutyAccID as varchar(30)
 DECLARE @ExcDutyAccTitle as varchar(50)
 DECLARE @ExciseDutyAccountID as varchar(30)
 DECLARE @ExciseDutyPercentage as money
  SET @ExciseDutyPercentage = 0
 DECLARE @ExciseDutyAmount as money
  SET @ExciseDutyAmount = 0
 DECLARE @OneLinkCharges as money
  SET @OneLinkCharges = 0
 DECLARE @ChargeType as Char(1)
  SET @ChargeType = ''
 DECLARE @mRemarksCharges as varchar(200)
 DECLARE @AvailableBalance as money
 DECLARE @Value as varchar(1)
 DECLARE @Super as varchar(1)
 
 DECLARE @VoucherID int
 DECLARE @IsCredit int
 DECLARE @GLControl nvarchar(30)
 
 SET NOCOUNT ON
 
 SELECT @Status = -1
 --------------------
 
 SELECT @mLastEOD=LastEOD,@mwDate=WorkingDate,@mCONFRETI=CONFRETI FROM t_Last WHERE (OurBranchID = @OurBranchID)
 IF @mLastEOD = @mwDate or @mCONFRETI = '1/1/9999'
 BEGIN
  SELECT @Status=9		--INVALID_DATE
  SELECT @Status  as Status
  RETURN (1)	
 END
 
 SELECT @vCount1=count(*),@Super=Supervision From t_ATM_TransferTransaction 
 WHERE (OurBranchID = @OurBranchID) AND ( AccountID=@AccountID) AND (Refno=@Refno) and (AtmID=@ATMID) group by Supervision
 
 IF @vCount1=1 and @Super='C'
 BEGIN
  SELECT @Status=10		--DUP_TRAN
  SELECT @Status  as Status
  RETURN (1)	
 END
 
 IF @vCount1=1 and @Super='R'
 BEGIN
  SELECT @Status=35		--ORIG_ALREADY_REVERSED
  SELECT @Status  as Status
  RETURN (1)	
 END
 
 SELECT @mScrollNo=(ISNULL(MAX(ScrollNo),0)+1) FROM t_ATM_TransferTransaction 
 WHERE (OurBranchID = @OurBranchID) and (convert(varchar(10), wDate, 120) = convert(varchar(10), @mwDate, 120))

 -------------------Debit Transaction-----------------
 
 SELECT @vCount1=count(*) From t_Account WHERE (OurBranchID = @OurBranchID) AND (AccountID in (@AccountID))
 
 IF  @vCount1=0
 BEGIN
  SELECT @Status=2		--INVALID_ACCOUNT
  SELECT @Status  as Status
  RETURN (1)	
 END
 
 SELECT @mAccountName=B.Name, @mProductID=B.ProductID, @mCurrencyID=P.CurrencyID,@mAccountType='C',
 @vClearBalance=B.ClearBalance,@vClearedEffects=B.ClearedEffects,@vFreezeAmount=B.FreezeAmount,@vMinBalance=P.MinBalance,
 @limit=isnull(b.limit,0),@vIsClose=A.[Status], @vAreCheckBooksAllowed=P.AreCheckBooksAllowed, 
 @vAllowDebitTransaction=isnull(A.AllowDebitTransaction,0)
 FROM t_AccountBalance B 
 INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND  B.ProductID = A.ProductID 
 INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND  A.ProductID = P.ProductID 
 WHERE (A.OurBranchID = @OurBranchID) AND (A.AccountID = @AccountID)
 
 SET @vClearBalance2=@vClearBalance
 
 IF @vIsClose = 'C' or @vIsClose = 'T' or @vIsClose = 'X' or @vIsClose = 'D' or @vAllowDebitTransaction = 1
 BEGIN
  SELECT @Status=3		--INACTIVE/CLOSE_ACCOUNT
  SELECT @Status  as Status
  RETURN (1)	
 END
 
 IF @vAreCheckBooksAllowed = 0
 BEGIN
  SELECT @Status=3		--INVALID_PRODUCT
  SELECT @Status  as Status
  RETURN (1)	
 END
 
 IF @mCurrencyID <> 'PKR'
 BEGIN
  SELECT @Status=3		--INVALID_CURRENCY
  SELECT @Status  as Status
  RETURN (1)	
 END
 
 SELECT @IssuerAccountID=IssuerAccountID,@IssuerAccountTitle=IssuerAccountTitle,@AcquiringAccountID=AcquiringAccountID,
 @AcquiringAccountTitle=AcquiringAccountTitle,@IssuerChargesAccountID=IssuerChargesAccountID,@IssuerTitle=IssuerTitle,
 @AcquiringChargesAccountID=AcquiringChargesAccountID,@AcquiringTitle=AcquiringTitle,@ExciseDutyAccountID=ExciseDutyAccountID,
 @ExciseDutyPercentage=ExciseDutyPercentage,@WithDrawlCharges=WithDrawalCharges,
 @WithDrawlChargesPercentage=WithDrawlChargesPercentage,@ChargeType=ChargeType
 From t_ATM_UtilityCharges WHERE (OurBranchID = @OurBranchID AND CompanyNumber = @CompanyNumber)
 
 --IssuerChargesAccountID   dr - due to 1-link HO Account
 --AcquiringChargesAccountID	cr	- branch income account
 
 IF (@ChargeType = 'F') --Fixed
 BEGIN
  SET @ExciseDutyAmount = ROUND((@WithDrawlCharges * @ExciseDutyPercentage) / 116, 2)
 END
 ELSE --Percentage
 BEGIN
  SET @WithDrawlCharges = ROUND((@WithDrawlChargesPercentage * @Amount) / 100,2)
  SET @ExciseDutyAmount = ROUND((@WithDrawlCharges * @ExciseDutyPercentage) / 116, 2)
 END
 
 IF @Amount > (@vClearBalance-@vClearedEffects+@limit) OR @Amount > ((@vClearBalance-@vClearedEffects+@limit)-@vFreezeAmount-@vMinBalance) OR 
	@Amount > ((@vClearBalance-@vClearedEffects+@limit)-@vMinBalance)
 BEGIN
  SELECT @Status=4		--LOW_BALANCE
  SELECT @Status  as Status
  RETURN (1)	
 END
 
 SET @mSerialNo = @mSerialNo + 1
 SET @mTrxType = 'D'
 SET @STAN = RIGHT(@RefNo,6)
 SET @IsCredit = 0
 
 SELECT @vBranchName=Heading2 FROM t_GlobalVariables WHERE (OurBranchID = @OurBranchID)
 
 SELECT @vCount4=count(*) From t_ATM_Branches WHERE OurBranchID = @OurBranchID AND BranchID = @WithdrawlBranchID
 
 IF @MerchantType <> '0000' 
 BEGIN
  IF @Currency = '586'
  BEGIN
   SET @mRemarks='Being Amount of INT.BK. Bill Payment Transaction From : ' + @CustomerBranchID + ' By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)
  END
  ELSE 
  BEGIN
   IF @Currency = '840'
   BEGIN
    SET @mRemarks='Being Amount of INT.BK. Bill Payment Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)
   END
   ELSE 
   BEGIN
    SET @mRemarks='Being Amount of INT.BK. Bill Payment Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) From : ' + @NameLocation + ' By  A/c # ' + @AccountID +  ' At : '  + upper(@vBranchName)
   END 
  END
 END
 ELSE
 BEGIN
  IF @vCount4 > 0 
  BEGIN
   SET  @mWithdrawlBranchName = 'N/A'
   SELECT @mWithdrawlBranchName=BranchName FROM t_ATM_Branches 
   WHERE OurBranchID = @OurBranchID AND BranchID = @WithdrawlBranchID
   SET @mRemarks='Being Amount of Bill Payment Transaction From : BK.' + @BankShortName +  ' (' + upper(@mWithdrawlBranchName) + ') By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)
  END
  ELSE
  BEGIN
   SET @mRemarks='Being Amount of ATM(' + @ATMID+ ') Bill Payment Transaction From : BK.' + @BankShortName +  ' (BR.' +  @WithdrawlBranchID + ') By  A/c # ' + @AccountID +  ' At : '  + upper(@vBranchName)
  END
 END
 
 IF @MerchantType <> '0000' 
 BEGIN
 
  INSERT INTO t_ATM_TransferTransaction 
  (
   ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
   IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
   [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,CompanyNumber,CreditCardNumber,STAN
  )
  VALUES
  (
   @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@AccountID,@mAccountName,@mAccountType,@mProductID,
   @mCurrencyID,@mIsLocalCurrency,@PHXDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,
   @Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,
   @mDescription+' STAN: '+@STAN+' CompanyNumber: '+@CompanyNumber,@ATMID,@mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,
   @MCC,@TrxDesc,@MerchantType,@CompanyNumber,@CreditCardNumber,@STAN
  )
 END
 ELSE
 BEGIN
  INSERT INTO t_ATM_TransferTransaction 
  (
   ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
   IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
   [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,CompanyNumber,CreditCardNumber,STAN
  )
  VALUES
  (
   @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@AccountID,@mAccountName,@mAccountType,@mProductID,
   @mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,
   @Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,
   @mDescription+' STAN: '+@STAN+' CompanyNumber: '+@CompanyNumber,@ATMID,@mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,
   @MCC,@TrxDesc,@MerchantType,@CompanyNumber,@CreditCardNumber,@STAN
  )
 END
 
 select @GLControl = ISNULL(GLControl, '') from t_Products where OurBranchID = @OurBranchID and ProductID = @mProductID
 select @VoucherID = ISNULL(MAX(VoucherID),0) + 1 from t_GLTransactions where OurBranchID = @OurBranchID
    
 INSERT INTO t_Transactions 
 ( 
  OurBranchID,ScrollNo,SerialNo,RefNo,wDate,AccountType,DocumentType,AccountID,AccountName,ProductID,CurrencyID,ValueDate,
  TrxType,ChequeID,ChequeDate,Amount,ForeignAmount,ExchangeRate,ProfitLoss,MeanRate,DescriptionID,[Description],BankCode,
  BranchCode,TrxPrinted,GLID,[Status],IsLocalCurrency,OperatorID,SupervisorID,AdditionalData,IsMainTrx,DocType,GLVoucherID,
  Remarks,TrxDesc,MerchantType,STAN,CompanyNumber
 )
 VALUES 
 ( 
  @OurBranchID,@mScrollNo,@mSerialNo,@RefNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@mAccountType,'AT',
  @AccountID,@mAccountName,@mProductID,@mCurrencyID,@mwDate,@mTrxType,'',@mwDate,@Amount,@mForeignAmount,@mExchangeRate,0,
  @mExchangeRate,@mDescriptionID,@mDescription+' STAN: '+@STAN+' CompanyNumber: '+@CompanyNumber,@mBankCode,@WithdrawlBranchID,
  0,null,'C',@mIsLocalCurrency,@mOperatorID,@mSupervisorID,null,'1','AT',@VoucherID,@mRemarks,@TrxDesc,@MerchantType,@STAN,
  @CompanyNumber 
 )
    
 INSERT INTO t_GLTransactions 
 (
  OurBranchID,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,
  ExchangeRate,IsCredit,TransactionType,TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,
  IsMainTrx,DocType,Remarks,TrxDesc,MerchantType,STAN,CompanyNumber
 )
 values 
 ( 
  @OurBranchID,@GLControl,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@mwDate,@mDescriptionID,
  @mDescription+' STAN: '+@STAN+' CompanyNumber: '+@CompanyNumber,@mCurrencyID,@Amount,@mForeignAmount,@mExchangeRate,
  @IsCredit,'ATMTransfer','L',@mOperatorID,@mSupervisorID,'',@mScrollNo,@mSerialNo,null,'0','AT',@mRemarks,@TrxDesc,
  @MerchantType,@STAN,@CompanyNumber
 )
  
 SELECT @Status=@@ERROR
  
 IF @Status <> 0 
 BEGIN
  RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR
  RETURN (5000)
 END
 
 SET @mSerialNo = @mSerialNo + 1
 SET @mTrxType = 'D'
 SET @mAccountType = 'G'
 SET @mProductID = 'GL'
 SET @mCurrencyID = 'PKR'
 SET @IsCredit = 0
 
 IF @MerchantType <> '0000' 
 BEGIN
  IF @Currency = '586'
  BEGIN
   SET @mRemarks='Being Amount of INT.BK. Bill Payment 1-Link Transaction From : ' + @CustomerBranchID + ' By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)
  END
  ELSE 
  BEGIN
   IF @Currency = '840'
   BEGIN
    SET @mRemarks='Being Amount of INT.BK. Bill Payment 1-Link Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)
   END
   ELSE 
   BEGIN
    SET @mRemarks='Being Amount of INT.BK. Bill Payment 1-Link Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) From : ' + @NameLocation + ' By  A/c # ' + @AccountID +  ' At : '  + upper(@vBranchName)
   END 
  END
 END
 ELSE
 BEGIN
  IF @vCount4 > 0 
  BEGIN
   SET @mRemarks='Being Amount of Bill Payment 1-Link Transaction From : BK.' + @BankShortName +  ' (' + upper(@mWithdrawlBranchName) + ') By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)
  END
  ELSE
  BEGIN
   SET @mRemarks='Being Amount of ATM(' + @ATMID+ ') Bill Payment 1-Link Transaction From : BK.' + @BankShortName +  ' (BR.' +  @WithdrawlBranchID + ') By  A/c # ' + @AccountID +  ' At : '  + upper(@vBranchName)
  END
 END
 
 IF @MerchantType <> '0000' 
 BEGIN
 
  INSERT INTO t_ATM_TransferTransaction 
  (
   ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
   IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
   [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,CompanyNumber,CreditCardNumber,STAN
  )
  VALUES
  (
   @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@IssuerChargesAccountID,@IssuerTitle,@mAccountType,
   @mProductID,@mCurrencyID,@mIsLocalCurrency,@PHXDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,
   @mTrxType,@Supervision,@mGLID,@WithDrawlCharges,@mForeignAmount,@mExchangeRate,@mDescriptionID,
   @mDescription+' STAN: '+@STAN+' CompanyNumber: '+@CompanyNumber,@ATMID,@mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,
   @MCC,@TrxDesc+'|BILPAY 1LINK TRX',@MerchantType,@CompanyNumber,@CreditCardNumber,@STAN
  )
 END
 ELSE
 BEGIN
  INSERT INTO t_ATM_TransferTransaction 
  (
   ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
   IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
   [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,CompanyNumber,CreditCardNumber,STAN
  )
  VALUES
  (
   @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@IssuerChargesAccountID,@IssuerTitle,@mAccountType,
   @mProductID,@mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,
   @mTrxType,@Supervision,@mGLID,@WithDrawlCharges,@mForeignAmount,@mExchangeRate,@mDescriptionID,
   @mDescription+' STAN: '+@STAN+' CompanyNumber: '+@CompanyNumber,@ATMID,@mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,
   @MCC,@TrxDesc+'|BILPAY 1LINK TRX',@MerchantType,@CompanyNumber,@CreditCardNumber,@STAN
  )
 END
 
 INSERT INTO t_GLTransactions 
 (
  OurBranchID,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,
  ExchangeRate,IsCredit,TransactionType,TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,
  IsMainTrx,DocType,Remarks,TrxDesc,MerchantType,STAN,CompanyNumber
 )
 values 
 ( 
  @OurBranchID,@IssuerChargesAccountID,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@mwDate,
  @mDescriptionID,@mDescription+' STAN: '+@STAN+' CompanyNumber: '+@CompanyNumber,@mCurrencyID,@WithDrawlCharges,
  @mForeignAmount,@mExchangeRate,@IsCredit,'ATMTransfer','L',@mOperatorID,@mSupervisorID,'',@mScrollNo,@mSerialNo,null,'1',
  'AT',@mRemarks,@TrxDesc+'|BILPAY 1LINK TRX',@MerchantType,@STAN,@CompanyNumber
 )
 
 SELECT @Status=@@ERROR
  
 IF @Status <> 0 
 BEGIN
  RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR
  RETURN (5000)
 END
 
 ----------Credit Transaction------------------------------Riz
 
 SELECT @vCount1=count(*) From t_GL
 WHERE (OurBranchID = @OurBranchID) AND( AccountID in (@IssuerAccountID))
  
 IF  @vCount1=0
 BEGIN
  SELECT @Status=2		--INVALID_ACCOUNT
  SELECT @Status  as Status
  RETURN (1)	
 END
 
 -------------------------------End Riz
 
 SET @mSerialNo = @mSerialNo + 1
 Set @mTrxType = 'C'
 Set @mAccountType='G'
 SET @mProductid ='GL'
 SET @mCurrencyID='PKR'
 SET @IsCredit = 1
 
 IF @MerchantType <> '0000'
 BEGIN
  IF @Currency = '586'
  BEGIN
   SET @mRemarks='Being Amount of INT.BK. Bill Payment Transaction From : ' + @CustomerBranchID + ' By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)
  END
  ELSE 
  BEGIN
   IF @Currency = '840'
   BEGIN
    SET @mRemarks='Being Amount of INT.BK. Bill Payment Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)
   END
   ELSE 
   BEGIN
    SET @mRemarks='Being Amount of INT.BK. Bill Payment Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) From : ' + @NameLocation + ' By  A/c # ' + @AccountID +  ' At : '  + upper(@vBranchName)
   END
  END
 END
 ELSE
 BEGIN
  IF @vCount4 > 0 
  BEGIN
   SET  @mWithdrawlBranchName = 'N/A'
   SELECT @mWithdrawlBranchName=BranchName FROM t_ATM_Branches WHERE OurBranchID = @OurBranchID AND BranchID = @WithdrawlBranchID
   SET @mRemarks='Being Amount of ATM (' + @ATMID+ ') Bill Payment Transaction From : BK.' + @BankShortName +  ' (' + upper(@mWithdrawlBranchName) + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)
  END
  ELSE
  BEGIN
   SET @mRemarks='Being Amount of ATM (' + @ATMID+ ') Bill Payment Transaction From : BK.' + @BankShortName +  ' (BR.' +  @WithdrawlBranchID + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)
  END
 END
 
 IF @MerchantType <> '0000' 
 BEGIN
  INSERT INTO t_ATM_TransferTransaction
  (
   ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
   IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
   [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,CompanyNumber,CreditCardNumber,STAN
  )
  VALUES 
  (
   @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@IssuerAccountID,@IssuerAccountTitle,@mAccountType,
   @mProductID,@mCurrencyID,@mIsLocalCurrency,@PHXDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,
   @mTrxType,@Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,
   @mDescriptionH+' STAN: '+@STAN+' CompanyNumber: '+@CompanyNumber,@ATMID,@mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,
   @MCC,@TrxDesc,@MerchantType,@CompanyNumber,@CreditCardNumber,@STAN
  )
 END
 ELSE
 BEGIN
  INSERT INTO t_ATM_TransferTransaction 
  (
   ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
   IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
   [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,CompanyNumber,CreditCardNumber,STAN
  )
  VALUES 
  (
   @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@IssuerAccountID,@IssuerAccountTitle,@mAccountType,
   @mProductID,@mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,
   @Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,
   @mDescriptionH+' STAN: '+@STAN+' CompanyNumber: '+@CompanyNumber,@ATMID,@mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,
   @MCC,@TrxDesc,@MerchantType,@CompanyNumber,@CreditCardNumber,@STAN
  )
 END
 
 INSERT INTO t_GLTransactions 
 (
  OurBranchID,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,
  ExchangeRate,IsCredit,TransactionType,TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,
  IsMainTrx,DocType,Remarks,TrxDesc,MerchantType,STAN,CompanyNumber
 )
 values 
 ( 
  @OurBranchID,@IssuerAccountID,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@mwDate,
  @mDescriptionID,@mDescriptionH+' STAN: '+@STAN+' CompanyNumber: '+@CompanyNumber,@mCurrencyID,@Amount,@mForeignAmount,
  @mExchangeRate,@IsCredit,'ATMTransfer','L',@mOperatorID,@mSupervisorID,'',@mScrollNo,@mSerialNo,null,'1','AT',@mRemarks,
  @TrxDesc,@MerchantType,@STAN,@CompanyNumber
 )
 
 SELECT @Status=@@ERROR
 IF @Status <> 0 
 BEGIN
  RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR
  RETURN (5000)
 END
 
 IF (@WithDrawlCharges > 0) 
 BEGIN
 
  SET @mSerialNo = @mSerialNo + 1
  SET @mRemarks='Being Amount of Bill Payment Charges Transaction From : ' + @CustomerBranchID + ' By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)
  
  INSERT INTO t_ATM_TransferTransaction 
  (
   ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
   IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
   [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,CompanyNumber,CreditCardNumber,STAN
  )
  VALUES
  (
   @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@AcquiringChargesAccountID,@AcquiringTitle,@mAccountType,
   @mProductID,@mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,
   @Supervision,@mGLID,(@WithDrawlCharges-@ExciseDutyAmount),@mForeignAmount,@mExchangeRate,@mDescriptionID,
   @mDescription+' STAN: '+@STAN+' CompanyNumber: '+@CompanyNumber,@ATMID,@mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,
   @MCC,@TrxDesc+'|BILPAY CHG',@MerchantType,@CompanyNumber,@CreditCardNumber,@STAN
  )
  
  INSERT INTO t_GLTransactions 
  (
   OurBranchID,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,
   ExchangeRate,IsCredit,TransactionType,TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,
   IsMainTrx,DocType,Remarks,TrxDesc,MerchantType,STAN,CompanyNumber
  )
  values 
  ( 
   @OurBranchID,@AcquiringChargesAccountID,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@mwDate,
   @mDescriptionID,@mDescription+' STAN: '+@STAN+' CompanyNumber: '+@CompanyNumber,@mCurrencyID,
   (@WithDrawlCharges-@ExciseDutyAmount),@mForeignAmount,@mExchangeRate,@IsCredit,'ATMTransfer','L',@mOperatorID,@mSupervisorID,
   '',@mScrollNo,@mSerialNo,null,'1','AT',@mRemarks,@TrxDesc+'|BILPAY CHG',@MerchantType,@STAN,@CompanyNumber
  )
  
  SELECT @Status=@@ERROR
  
  IF @Status <> 0 
  BEGIN
   RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR
   RETURN (5000)
  END
  
  IF (@ExciseDutyPercentage > 0) 
  BEGIN
  
   SET @mSerialNo = @mSerialNo + 1
   SET @mRemarks='Being Amount of Bill Payment FED Transaction From : ' + @CustomerBranchID + ' By  A/c # ' + @AccountID  + ' At : '  + upper(@vBranchName)
   
   SELECT @ExcDutyAccID = AccountID, @ExcDutyAccTitle = Description FROM t_GL 
   WHERE OurBranchID = @OurBranchID AND AccountID = @ExciseDutyAccountID
   
   INSERT INTO t_ATM_TransferTransaction 
   (
    ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,
    IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,
    [Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,CompanyNumber,CreditCardNumber,STAN
   )
   VALUES
   (
    @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@ExcDutyAccID,@ExcDutyAccTitle,@mAccountType,@mProductID,
    @mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,
    @Supervision,@mGLID,@ExciseDutyAmount,@mForeignAmount,@mExchangeRate,@mDescriptionID,
    @mDescription+' STAN: '+@STAN+' CompanyNumber: '+@CompanyNumber,@ATMID,@mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,
    @MCC,@TrxDesc+'|BILPAY FED',@MerchantType,@CompanyNumber,@CreditCardNumber,@STAN
   )
   
   INSERT INTO t_GLTransactions 
   (
    OurBranchID,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,
    ExchangeRate,IsCredit,TransactionType,TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,
    IsMainTrx,DocType,Remarks,TrxDesc,MerchantType,STAN,CompanyNumber
   )
   values 
   ( 
    @OurBranchID,@ExcDutyAccID,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@mwDate,
    @mDescriptionID,@mDescription+' STAN: '+@STAN+' CompanyNumber: '+@CompanyNumber,@mCurrencyID,
    @ExciseDutyAmount,@mForeignAmount,@mExchangeRate,@IsCredit,'ATMTransfer','L',@mOperatorID,@mSupervisorID,'',@mScrollNo,
    @mSerialNo,null,'1','AT',@mRemarks,@TrxDesc+'|BILPAY FED',@MerchantType,@STAN,@CompanyNumber
   )
   
   SELECT @Status=@@ERROR
   
   IF @Status <> 0 
   BEGIN
    RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR
    RETURN (5000)
   END
  END
 END
 
 SELECT @Status    = 0, 
 @AvailableBalance = (isnull(@limit,0)+(isnull(@vClearBalance,0))-(isnull(@Amount,0)))*100, 
 @vClearBalance    = ((IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0))-IsNull(@Amount,0))*100, @Value = '3'
 SELECT @Status As Status , @vClearBalance As ClearBalance , @AvailableBalance As AvailableBalance , @value As value
 RETURN 0
 
END