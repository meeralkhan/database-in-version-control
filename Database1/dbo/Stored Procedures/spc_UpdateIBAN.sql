﻿create procedure [dbo].[spc_UpdateIBAN]
AS
truncate table ChkAccount;

insert into ChkAccount
select a.OurBranchID, a.AccountID, a.Name, a.ClientID, a.ProductID, a.CurrencyID, a.IBAN AS OldIBAN, '', a.Status, ab.ClearBalance, ab.Effects, ab.Limit, ab.ShadowBalance, ab.LocalClearBalance, ab.LocalEffects, ab.IsFreezed, ab.FreezeAmount, 0 AS IsProcessed
from t_Account a
inner join t_AccountBalance ab on a.OurBranchID = ab.OurBranchID and a.AccountID = ab.AccountID
inner join t_Products p on a.OurBranchID = p.OurBranchID and a.ProductID = p.ProductID
where p.ProductID IN ('NONCHQ','TDRCOL') OR (p.ProductType IN ('L','AE') AND ISNULL(p.IsTermDeposit,'No') = 'No')

select 'Ok' AS ReturnStatus;