﻿CREATE PROCEDURE [dbo].[spc_GetOutStandingCheques]    
(    
  @OurBranchID VARCHAR(30),    
  @AccountID VARCHAR(30),    
  @ChequeStart VARCHAR(15),    
  @ChequeEnd VARCHAR(15)    
)    
AS    
    
SET NOCOUNT ON    
    
SELECT ChequeID,Description,DATE,CreateBy,SuperviseBy    
FROM t_StopPayments    
WHERE OURBRANCHID = @OURBRANCHID AND ACCOUNTID = @ACCOUNTID    
AND cast(ChequeID as money) >= cast(@ChequeStart as money) And cast(ChequeID as money) <= cast(@ChequeEnd as money)