﻿CREATE PROCEDURE [dbo].[sp_ATM_AddEditQuasiCashTransaction]  
(  
 @OurBranchID       varchar(30),   
 @WithdrawlBranchID      varchar(30),   
 @QSIID       varchar(30),   
 @AccountID           varchar(30),  
 @Amount        money,  
 @USDAmount        money,  
 @OtherCurrencyAmount      money=0,  
 @USDRate       money=0,  
 @Supervision       char(1),   
 @RefNo        varchar(50),  
 @PHXDate        datetime,  
 @MerchantType      varchar(30),  
 @OurIMD       varchar(30),  
 @AckInstIDCode      varchar(30),  
 @Currency       varchar(30),  
 @NameLocation      varchar(100),  
 @MCC        varchar(30),  
 @PHXDateTime        varchar(14),  
  
 @NewRecord       bit=1  
)  
AS  
-- DECLARE @BankShortName as varchar(6)  
  
 DECLARE @mScrollNo as int  
 DECLARE @mSerialNo as smallint  
 DECLARE @mAccountID as varchar(30)  
 DECLARE @mAccountName as varchar(100)  
 DECLARE @mAccountType as char  
 DECLARE @mProductID as varchar(30)  
 DECLARE @mCurrencyID as varchar(30)  
 DECLARE @mIsLocalCurrency as bit  
  SET @mIsLocalCurrency=1   
 DECLARE @mwDate as datetime  
 DECLARE @mTrxType as char  
 DECLARE @mGLID as varchar(30)  
  SET @mGLID = ''   
 DECLARE @mForeignAmount as money  
  SET @mForeignAmount=0   
 DECLARE @mExchangeRate as money  
  SET @mExchangeRate=1   
 DECLARE @mDescriptionID as varchar(30)  
  SET @mDescriptionID='987'   
 DECLARE @mDescription as varchar(100)  
 DECLARE @LocalCurrencyID as varchar(30)  
 DECLARE @LocalCurrency as varchar(30)  
--  BEGIN   
--   SET @BankShortName = 'N/A'  
--   SELECT @BankShortName = BankShortName From t_ATM_Banks  
--   WHERE (BankIMD = @OurIMD)  
--   IF @MerchantType <> '0000'   
--    SET @mDescription='QSI(' + @QSIID + ') QUASI CASH AT BK.' + @BankShortName  + ' (BR.' + @WithdrawlBranchID + ')'   
   SELECT @LocalCurrencyID=LocalCurrencyID,@LocalCurrency=LocalCurrency FROM t_ATM_GlobalVariables  
   
    IF @Currency = @LocalCurrencyID  
     BEGIN  
      SET @mDescription=Right(@RefNo,6) + '-QSI(' + @QSIID + ') QUASI CASH AT ' + @NameLocation   
      SET @mDescription = @mDescription + '-' + @PHXDateTime  
     END  
    ELSE   
     IF @Currency = '840'  
      BEGIN  
       SET @mDescription=Right(@RefNo,6) + '-QSI(' + @QSIID + ') QUASI CASH ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') AT ' + @NameLocation   
       SET @mDescription = @mDescription + '-' + @PHXDateTime  
      END  
     ELSE   
      BEGIN  
       SET @mDescription=Right(@RefNo,6) + '-QSI(' + @QSIID + ') QUASI CASH (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) AT ' + @NameLocation   
       SET @mDescription = @mDescription + '-' + @PHXDateTime  
      END  
--   ELSE   
--    SET @mDescription='QSI(' + @QSIID + ') IB-CSH WDRL AT BK.' + @BankShortName  + ' (BR.' + @WithdrawlBranchID + ')'   
--  END   
 DECLARE @mDescriptionCharges as varchar(100)  
--  BEGIN   
--   IF @MerchantType <> '0000'   
--    SET @mDescriptionCharges='QSI(' + @QSIID + ') QUASI CASH-CHG AT BK.' + @BankShortName  + ' (BR.' + @WithdrawlBranchID + ')'   
    IF @Currency = @LocalCurrencyID  
     BEGIN  
      SET @mDescriptionCharges=Right(@RefNo,6) + '-QSI(' + @QSIID + ') QUASI CASH-CHG AT ' + @NameLocation   
      SET @mDescriptionCharges = @mDescriptionCharges + '-' + @PHXDateTime  
     END  
    ELSE   
     IF @Currency = '840'  
      BEGIN  
       SET @mDescriptionCharges=Right(@RefNo,6) + '-QSI(' + @QSIID + ') QUASI CASH-CHG ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') AT ' + @NameLocation   
       SET @mDescriptionCharges = @mDescriptionCharges + '-' + @PHXDateTime  
      END  
     ELSE   
      BEGIN  
       SET @mDescriptionCharges=Right(@RefNo,6) + '-QSI(' + @QSIID + ') QUASI CASH-CHG (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) AT ' + @NameLocation   
       SET @mDescriptionCharges = @mDescriptionCharges + '-' + @PHXDateTime  
      END  
--  END   
 DECLARE @mDescriptionH as varchar(100)  
--  BEGIN   
--   IF @MerchantType <> '0000'   
--    SET @mDescriptionH='QSI(' + @QSIID + ') QUASI CASH AT BK.' + @BankShortName   + ' (BR.' + @WithdrawlBranchID + ') ' + ' From : A/c # ' + @AccountID  
    IF @Currency = @LocalCurrencyID  
     BEGIN  
      SET @mDescriptionH=Right(@RefNo,6) + '-QSI(' + @QSIID + ') QUASI CASH AT ' +  + @NameLocation + ' From : A/c # ' + @AccountID  
      SET @mDescriptionH = @mDescriptionH + '-' + @PHXDateTime  
     END  
    ELSE   
  
  
     IF @Currency = '840'  
      BEGIN  
       SET @mDescriptionH=Right(@RefNo,6) + '-QSI(' + @QSIID + ') QUASI CASH ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') AT ' +  + @NameLocation + ' From : A/c # ' + @AccountID  
       SET @mDescriptionH = @mDescriptionH + '-' + @PHXDateTime  
      END  
     ELSE   
      BEGIN  
       SET @mDescriptionH=Right(@RefNo,6) + '-QSI(' + @QSIID + ') QUASI CASH (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) AT ' +  + @NameLocation + ' From : A/c # ' + @AccountID  
       SET @mDescriptionH = @mDescriptionH + '-' + @PHXDateTime  
      END  
--   ELSE   
--    SET @mDescriptionH='QSI(' + @QSIID + ') IB-CSH WDRL AT BK.' + @BankShortName   + ' (BR.' + @WithdrawlBranchID + ') ' + ' From : A/c # ' + @AccountID  
--  END   
 DECLARE @mDescriptionHCharges as varchar(100)  
--  BEGIN   
--   IF @MerchantType <> '0000'   
--    SET @mDescriptionHCharges='QSI(' + @QSIID + ') QUASI CASH-CHG AT BK.' + @BankShortName   + ' (BR.' + @WithdrawlBranchID + ') ' + ' From : A/c # ' + @AccountID  
    IF @Currency = @LocalCurrencyID  
     BEGIN  
      SET @mDescriptionHCharges=Right(@RefNo,6) + '-QSI(' + @QSIID + ') QUASI CASH-CHG AT ' + @NameLocation + ' From : A/c # ' + @AccountID  
      SET @mDescriptionHCharges = @mDescriptionHCharges + '-' + @PHXDateTime  
     END  
    ELSE   
     IF @Currency = '840'  
      BEGIN  
       SET @mDescriptionHCharges=Right(@RefNo,6) + '-QSI(' + @QSIID + ') QUASI CASH-CHG ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') AT ' + @NameLocation + ' From : A/c # ' + @AccountID  
       SET @mDescriptionHCharges = @mDescriptionHCharges + '-' + @PHXDateTime  
      END  
     ELSE   
      BEGIN  
       SET @mDescriptionHCharges=Right(@RefNo,6) + '-QSI(' + @QSIID + ') QUASI CASH-CHG (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) AT ' + @NameLocation + ' From : A/c # ' + @AccountID  
       SET @mDescriptionHCharges = @mDescriptionHCharges + '-' + @PHXDateTime  
      END  
--  END   
 DECLARE @mRemarks as varchar(200)  
 DECLARE @mBankCode as varchar(30)  
  SET @mBankCode='093'   
 DECLARE @mOperatorID as varchar(30)  
  SET @mOperatorID = 'QSI-' + @QSIID  
 DECLARE @mSupervisorID as varchar(30)  
  SET @mSupervisorID = @OurIMD   
  
--Checking Variables  
 DECLARE @vCount1 as bit  
 DECLARE @vCount2 as bit  
 DECLARE @vCount3 as bit  
 DECLARE @vCount4 as bit  
 DECLARE @vAccountID as varchar(30)  
 DECLARE @vClearBalance as money  
 DECLARE @vClearedEffects as money  
 DECLARE @vFreezeAmount as money  
 DECLARE @vMinBalance as money  
 DECLARE @vIsClose as char  
 DECLARE @vAreCheckBooksAllowed as bit  
 DECLARE @vAllowDebitTransaction as bit  
  
 DECLARE @vAccountLimit as money  
 DECLARE @vExpiryDate as datetime  
 DECLARE @vCheckLimit as bit  
  SET @vCheckLimit = 0  
 DECLARE @vODLimitAllow as bit  
  SET @vODLimitAllow = 0  
  
 DECLARE @vBranchName as varchar(100)  
 DECLARE @Status as int   
  
 DECLARE @mLastEOD as datetime  
-- DECLARE @mWorkingDate as datetime  
 DECLARE @mCONFRETI as datetime  
-- DECLARE @mWithdrawlBranchName as varchar(50)  
  
 DECLARE @WithDrawlCharges as money  
 DECLARE @WithDrawlChargesPercentage as money  
 DECLARE @IssuerAccountID as varchar(30)  
 DECLARE @IssuerAccountTitle as varchar(100)  
 DECLARE @IssuerChargesAccountID as varchar(30)  
 DECLARE @IssuerTitle as varchar(100)  
 DECLARE @mRemarksCharges as varchar(200)  
  
 SET NOCOUNT ON  
  
 SELECT @Status = -1  
---------------------  
  
 BEGIN  
  
   SELECT @mLastEOD=LastEOD,@mwDate=WorkingDate,@mCONFRETI=CONFRETI FROM t_Last WHERE (OurBranchID = @OurBranchID)  
   IF @mLastEOD = @mwDate or @mCONFRETI = '1/1/9999'  
    BEGIN  
        
     SELECT @Status=9  --INVALID_DATE  
     SELECT @Status  as Status  
     RETURN (1)   
    END  
  
   SELECT @mScrollNo=(ISNULL(MAX(ScrollNo),0)+1) FROM t_ATM_TransferTransaction WHERE (OurBranchID = @OurBranchID) and (wDate = @mwDate)   
  
------------------------  
------------------------  
--Debit Transaction     
  
  
   SELECT @vCount1=count(*) From t_Account  
   WHERE (OurBranchID = @OurBranchID) AND( AccountID=@AccountID)  
   IF @vCount1=0  
  
    BEGIN  
        
     SELECT @Status=2  --INVALID_ACCOUNT  
     SELECT @Status  as Status  
     RETURN (1)   
    END  
  
   SET @WithDrawlCharges = 0  
   SET @WithDrawlChargesPercentage = 0  
--   IF @MCC = '' --Check  
--    BEGIN  
--    IF @Currency = @LocalCurrencyID   
--     BEGIN  
--      SELECT @WithDrawlCharges=QuasiCashCharges,@WithDrawlChargesPercentage=QuasiCashChargesPercentage,  
--        @IssuerAccountID=IssuerAccountID,@IssuerAccountTitle=IssuerAccountTitle,  
--        @IssuerChargesAccountID=IssuerChargesAccountID,@IssuerTitle=IssuerTitle    
--      From t_ATM_SwitchCharges  
--      WHERE (MerchantType = @MerchantType)  
--      IF @WithDrawlCharges < ((@WithDrawlChargesPercentage/100)*@Amount)  
--       BEGIN  
--        SET @WithDrawlCharges = ((@WithDrawlChargesPercentage/100)*@Amount)  
--       END  
--     END  
--    ELSE  
--     BEGIN  
--      SELECT @WithDrawlCharges=ForeignQuasiCashCharges,@WithDrawlChargesPercentage=ForeignQuasiCashChargesPercentage,  
--        @IssuerAccountID=IssuerAccountID,@IssuerAccountTitle=IssuerAccountTitle,  
--        @IssuerChargesAccountID=IssuerChargesAccountID,@IssuerTitle=IssuerTitle    
--      From t_ATM_SwitchCharges  
--      WHERE (MerchantType = @MerchantType)  
--      IF @WithDrawlCharges < ((@WithDrawlChargesPercentage/100)*@Amount)  
--       BEGIN  
--        SET @WithDrawlCharges = ((@WithDrawlChargesPercentage/100)*@Amount)  
--       END  
--     END  
--    END  
  
   SELECT Top 1 @vCheckLimit=CheckLimit FROM t_ATM_GlobalVariables  
   SELECT Top 1 @vODLimitAllow=ODLimitAllow FROM t_ATM_Banks WHERE BankIMD = left(@RefNo,6)  
  
   SELECT @mAccountName=B.Name, @mProductID=B.ProductID, @mCurrencyID=P.CurrencyID,@mAccountType='C',@vClearBalance=B.ClearBalance,@vClearedEffects=B.ClearedEffects,@vFreezeAmount=B.FreezeAmount,@vMinBalance=A.MinBalance,@vIsClose=A.Status, @vAreCheckBooksAllowed=P.AreCheckBooksAllowed, @vAllowDebitTransaction=S.AllowDebitTransaction, @vAccountLimit=B.Limit  
   FROM t_AccountBalance B INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND  B.ProductID = A.ProductID INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND  A.ProductID = P.ProductID LEFT OUTER JOIN t_AccountSpecialCondition S ON B.OurBranchID = S.OurBranchID AND B.AccountID = S.AccountID  
   WHERE  (A.OurBranchID = @OurBranchID) AND (A.AccountID = @AccountID)  
   IF @vAccountLimit > 0 AND @vCheckLimit = 1 AND @vODLimitAllow = 1  
    BEGIN  
     SELECT @vExpiryDate=ExpiryDate FROm t_AccountLimits WHERE (OurBranchID = @OurBranchID) and (AccountID = @AccountID)  
     IF @vExpiryDate > @mwDate  
      BEGIN  
       SET @vClearBalance=(@vClearBalance+@vAccountLimit)  
      END  
    END  
   IF @vIsClose = 'C' or @vIsClose = 'T' or @vIsClose = 'X' or @vAllowDebitTransaction = 1  
    BEGIN  
        
     SELECT @Status=3  --INACTIVE/CLOSE_ACCOUNT  
     SELECT @Status  as Status  
     RETURN (1)   
    END  
   IF @Amount >(@vClearBalance-@vClearedEffects)-@WithDrawlCharges or @Amount >((@vClearBalance-@vClearedEffects)-@vFreezeAmount-@vMinBalance-@WithDrawlCharges) or @Amount >((@vClearBalance-@vClearedEffects)-@vMinBalance-@WithDrawlCharges)  
    BEGIN  
        
     SELECT @Status=4  --LOW_BALANCE  
     SELECT @Status  as Status  
     RETURN (1)   
    END  
   IF @vAreCheckBooksAllowed = 0  
    BEGIN  
        
     SELECT @Status=3  --INVALID_PRODUCT  
     SELECT @Status  as Status  
     RETURN (1)   
    END  
   IF @mCurrencyID <> @LocalCurrency  
    BEGIN  
        
     SELECT @Status=3  --INVALID_CURRENCY  
     SELECT @Status  as Status  
     RETURN (1)   
    END  
  
   Set @mSerialNo = 1  
   Set @mTrxType = 'D'  
   SET @vBranchName = 'N/A'  
   SELECT @vBranchName=Heading2 FROM t_GlobalVariables WHERE (OurBranchID = @OurBranchID)  
  
--   SELECT @vCount4=count(*) From t_ATM_Branches  
--   WHERE BranchID = @WithdrawlBranchID  
  
--   IF @MerchantType <> '0000'   
--    BEGIN  
--     Set @mRemarks='Being Amount of QSI (' + @QSIID+ ') Purchase Request Transaction From : BK.' + @BankShortName + ' (BR.' + @WithdrawlBranchID + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
--     Set @mRemarksCharges='Being Amount of QSI (' + @QSIID+ ') Purchase Request Charges Transaction From : BK.' + @BankShortName + ' (BR.' + @WithdrawlBranchID + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
     IF @Currency = @LocalCurrencyID  
      BEGIN  
       Set @mRemarks='Being Amount of QSI (' + @QSIID+ ') Quasi Cash Transaction From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
       Set @mRemarksCharges='Being Amount of QSI (' + @QSIID+ ') Quasi Cash Charges Transaction From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
      END  
     ELSE   
      IF @Currency = '840'  
       BEGIN  
        Set @mRemarks='Being Amount of QSI (' + @QSIID+ ') Quasi Cash Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
        Set @mRemarksCharges='Being Amount of QSI (' + @QSIID+ ') Quasi Cash Charges Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
       END  
      ELSE   
       BEGIN  
        Set @mRemarks='Being Amount of QSI (' + @QSIID+ ') Quasi Cash Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
        Set @mRemarksCharges='Being Amount of QSI (' + @QSIID+ ') Quasi Cash Charges Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
       END  
--     END  
--   ELSE  
--    IF @vCount4 > 0   
--     BEGIN  
--      SET  @mWithdrawlBranchName = 'N/A'  
--      SELECT @mWithdrawlBranchName=BranchName FROM t_ATM_Branches  
--      WHERE BranchID = @WithdrawlBranchID  
--      Set @mRemarks='Being Amount of QSI (' + @QSIID+ ') Inter Branch Cash Withdrawl Transaction From : BK.' + @BankShortName +  ' (' + upper(@mWithdrawlBranchName) + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
--     END  
    --IF @vCount4 = 0   
--    ELSE  
--     BEGIN  
--      Set @mRemarks='Being Amount of QSI (' + @QSIID+ ') Inter Branch Cash Withdrawl Transaction From : BK.' + @BankShortName +  ' (BR.' +  @WithdrawlBranchID + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
--     END  
  
  INSERT INTO t_ATM_TransferTransaction  
   (  
    ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,  
     valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,  
    BankCode,Remarks,OperatorID,SupervisorID,MCC,MerchantType  
   )    
  
  VALUES  
  
   (  @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@AccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,  
    @mwDate,@mwDate,@PHXDate,@mTrxType,@Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescription,@QSIID,  
    @mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MCC,@MerchantType  
   )  
     SELECT @Status=@@ERROR  
  
    IF @Status <> 0   
     BEGIN  
         
      RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR       RETURN (5000)  
     END  
--Debit Transaction Switch Charges     
IF @MerchantType <> '0000'  and @WithDrawlCharges > 0  
 BEGIN  
   Set @mSerialNo = 3  
  
  INSERT INTO t_ATM_TransferTransaction  
   (  
    ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,  
     valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,  
    BankCode,Remarks,OperatorID,SupervisorID,MCC,MerchantType  
   )    
  
  VALUES  
  
   (  
    @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@AccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,  
  
    @mwDate,@mwDate,@PHXDate,@mTrxType,@Supervision,@mGLID,@WithDrawlCharges,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescriptionCharges,@QSIID,  
    @mBankCode,@mRemarksCharges,@mOperatorID,@mSupervisorID,@MCC,@MerchantType  
   )  
     SELECT @Status=@@ERROR  
  
    IF @Status <> 0   
     BEGIN  
         
      RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR       RETURN (5000)  
     END  
 END  
--Credit Transaction     
  
  SELECT @vAccountID=AccountID, @mAccountName=Description,@mProductID='GL', @mCurrencyID=CurrencyID,@mAccountType='G' FROM t_GL   
  WHERE (OurBranchID = @OurBranchID) AND (AccountID =  @IssuerAccountID)  
  
  Set @mSerialNo = 2  
  Set @mTrxType = 'C'  
  
  INSERT INTO t_ATM_TransferTransaction  
   (  
    ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,  
    valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,  
    BankCode,Remarks,OperatorID,SupervisorID,MCC,MerchantType  
   )    
  
  VALUES  
  
   (  
    @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@vAccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,  
    @mwDate,@mwDate,@PHXDate,@mTrxType,@Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescriptionH,@QSIID,  
    @mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MCC,@MerchantType  
   )  
  
    SELECT @Status=@@ERROR  
  
    IF @Status <> 0   
     BEGIN  
         
      RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR  
      RETURN (5000)  
     END  
  
--Credit Transaction Switch Charges     
IF @MerchantType <> '0000'  and @WithDrawlCharges > 0  
 BEGIN  
  
  SELECT @vAccountID=AccountID, @mAccountName=Description,@mProductID='GL', @mCurrencyID=CurrencyID,@mAccountType='G' FROM t_GL   
  WHERE (OurBranchID = @OurBranchID) AND (AccountID =  @IssuerChargesAccountID)  
  
  Set @mSerialNo = 4  
  
  INSERT INTO t_ATM_TransferTransaction  
   (  
    ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,  
    valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,  
    BankCode,Remarks,OperatorID,SupervisorID,MCC,MerchantType  
   )    
  
  VALUES  
  
   (  
    @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@vAccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,  
    @mwDate,@mwDate,@PHXDate,@mTrxType,@Supervision,@mGLID,@WithDrawlCharges,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescriptionHCharges,@QSIID,  
    @mBankCode,@mRemarksCharges,@mOperatorID,@mSupervisorID,@MCC,@MerchantType  
   )  
  
    SELECT @Status=@@ERROR  
  
    IF @Status <> 0   
     BEGIN  
         
      RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR  
   
      RETURN (5000)  
     END  
 END  
  
 IF @vAccountLimit > 0 AND @vCheckLimit = 1 AND @vODLimitAllow = 1  AND @vExpiryDate > @mwDate  
  BEGIN  
   SELECT @Status = 0 , @vClearBalance = ((IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0)-@vAccountLimit)-IsNull(@Amount,0)-isNull(@WithDrawlCharges,0))*100  
   SELECT @Status As Status , @vClearBalance As ClearBalance  
  END   
 ELSE  
  BEGIN  
   SELECT @Status = 0 , @vClearBalance = ((IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0))-IsNull(@Amount,0)-isNull(@WithDrawlCharges,0))*100  
   SELECT @Status As Status , @vClearBalance As ClearBalance  
  END   
  
  RETURN 0   
     
 END