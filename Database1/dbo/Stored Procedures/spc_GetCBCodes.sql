﻿CREATE PROCEDURE [dbo].[spc_GetCBCodes]    
(    
 @OurBranchID varchar(30),    
 @AccountID varchar(30)    
)    
AS    
    
SET NOCOUNT ON    
    
declare @CategoryId nvarchar(30)    
declare @CBType nvarchar(3)    
declare @CBDescription nvarchar(30)    
declare @Resident nvarchar(3)    
declare @NIC nvarchar(30)    
declare @CountryID nvarchar(30)    
declare @PassportNumber nvarchar(30)    
declare @TradeLicenseNo nvarchar(30)    
declare @EconoActCode nvarchar(30)    
    
set @CBType = ''    
set @CBDescription = ''    
set @NIC = ''    
set @CountryID = ''    
set @PassportNumber = ''    
set @TradeLicenseNo = ''    
set @EconoActCode = ''    
    
select @CategoryId = ISNULL(c.CategoryId,''), @Resident = ISNULL(c.ResidentNonResident,''), @NIC = ISNULL(c.NIC,''),    
@CountryID = ISNULL(ct.ShortName,''), @PassportNumber = ISNULL(c.PassportNumber,''), @TradeLicenseNo = ISNULL(c.TradeLicenseNumberCorp,''),    
@EconoActCode = case ISNULL(c.CategoryId,'') when 'Individual' then '1100' else ISNULL(s.EconoActCode,'1200') end    
from t_Account a    
inner join t_Customer c on a.ClientID = c.ClientID    
left join t_Country ct on a.OurBranchID = ct.OurBranchID and c.PassportIssueCountry = ct.CountryID      
left join t_SICCode s on c.OurBranchID = s.OurBranchID and ISNULL(c.SICCODESCORP,'') = ISNULL(s.SICID,'')    
where a.OurBranchID = @OurBranchID and a.AccountID = @AccountID    
    
if @CategoryId = 'Individual' and @Resident = 'Y'    
begin    
   set @CBType = 'IRE'    
   set @CBDescription = 'Individual Resident'    
   set @CountryID = ''    
   set @PassportNumber = ''    
   set @TradeLicenseNo = ''    
end    
else if @CategoryId = 'Individual' and @Resident = 'N'    
begin    
   set @CBType = 'INR'    
   set @CBDescription = 'Individual Non Resident'    
   set @NIC = ''    
   set @TradeLicenseNo = ''    
end    
else if @CategoryId IN ('Corporate','FIS','NOSTRO','KYCLite')
begin    
   set @CBType = 'JUR'    
   set @CBDescription = 'Juridical'    
   set @NIC = ''    
   set @CountryID = ''    
   set @PassportNumber = ''    
end    
    
select @AccountID AS AccountID, @CBType AS CBType, @CBDescription AS CBDescription, @NIC AS EmiratesIDNo, @CountryID AS CountryID, @PassportNumber AS PassportNo, @TradeLicenseNo AS TradeLicenseNo, @EconoActCode AS EconoActCode