﻿CREATE PROCEDURE [dbo].[spc_GetLogin]    
(            
  @OurBranchID varchar(30),            
  @LoginID varchar(30),          
  @CheckBODEOD varchar(1) = 'Y'          
)            
AS            
      
SET NOCOUNT ON      
      
BEGIN TRY            
             
 DECLARE @LastEOD Datetime              
 DECLARE @LastBOD Datetime              
 DECLARE @WorkingDate Datetime              
               
 IF Exists (SELECT OperatorID From t_Map WHERE OurBranchID = @OurBranchID AND OperatorID = @LoginID AND [AuthStatus] = '')              
 BEGIN              
             
   IF @CheckBODEOD = 'Y'          
   begin          
  SELECT @LastEOD = LastEOD, @LastBOD = LastBOD, @WorkingDate = WorkingDate FROM t_Last WHERE OurBranchID = @OurBranchID   
   end          
   else          
   begin          
  SELECT @LastEOD = GETDATE(), @LastBOD = GETDATE(), @WorkingDate = GETDATE()          
   end          
             
   SELECT  'Ok' as ReturnStatus, @LastEOD AS LastEOD, @LastBOD AS LastBOD, @WorkingDate AS WorkingDate,            
   * FROM t_Map WHERE OurBranchID = @OurBranchID AND OperatorID = @LoginID AND [AuthStatus] = '';            
   --FullName, Password, LoginDate, IsLoggedIn, TimeLoggedIn, TrustLevel, GreetingDate,               
   --GreetingMessage, Chgpwdnext, CannotChgPwd, NormalShutDown, ACDisable, ComputerName, LastPasswordChg, Day, RemindDays,               
   --IsPasswordExp FROM t_Map WHERE OurBranchID = @OurBranchID AND OperatorID=@LoginID AND [Authorization]=0 ;              
 END                
 ELSE IF Exists (SELECT OperatorID From t_Map WHERE OurBranchID = @OurBranchID AND OperatorID=@LoginID )               
 BEGIN              
   select 'Error' AS ReturnStatus, 'Under Authorization.' AS ReturnMessage;              
 end              
 ELSE                
 BEGIN              
   select 'Error' AS ReturnStatus, 'Invalid Username.' AS ReturnMessage;              
 end              
end try               
begin catch              
 select 'Error' AS ReturnStatus, 'General SQL Exception.' AS ReturnMessage;              
end catch