﻿CREATE PROCEDURE [dbo].[spc_InwardReturnCheque]          
            
 @SerialNo int,            
 @OurBranchID varchar(30),            
 @AccountID varchar(30),            
 @Amount money,            
 @ChequeID varchar(30)='',            
 @Reason varchar(255)='',            
 @Status char(1)='',            
 @CurrencyID varchar(30)='',            
 @ForeignAmount money=0,            
 @OperatorID varchar(30)='',            
 @NewRecord bit=1,  
 @IsApplyCharges bit=0,            
 @ChargesScrollNo int = 0,
 @ChargeAmt money = 0,
 @AddData text = ''  
 AS            
            
 SET NOCOUNT ON            
-- Update Inward Transaction File             
            
 DECLARE @LocalCurrency varchar(30),            
  @AccountName varchar(100),            
  @ProductID varchar(30),            
  @AccountType Char(1),            
  @ValueDate datetime,            
  @wDate  datetime,            
  @BankCode varchar(30),            
  @BranchCode varchar(30),            
  @GLVoucherID int,  
  @GLControl varchar(30),  
  @TrxType char(1),  
  @ChequeDate datetime,  
  @ExchangeRate decimal(24,6),  
  @DescriptionID varchar(30),  
  @Description varchar(100),  
  @IsLocalCurrency int,  
  @SupervisorID varchar(30),  
  @RefNo varchar(100),  
  @Year varchar(4),  
  @Month varchar(2),  
  @Day varchar(2),  
  @IsCredit int,  
  @TransactionMethod char(1),  
  @AppWHTax int,  
  @WHTaxAmount money,  
  @WHTaxMode char(1),  
  @WHTScrollNo int  
    
 SELECT @GLVoucherID=0,@ProductID = '', @AccountType = '', @AccountName = '', @LocalCurrency = '', @ValueDate = '01/01/1900',
 @TrxType = 'D', @GLControl = '', @wDate = '01/01/1900', @BankCode = '', @BranchCOde = '', @IsLocalCurrency=1
 
 SELECT @LocalCurrency = LocalCurrency            
 FROM t_GlobalVariables            
 Where OurBranchID = @OurBranchID          
   
 IF @NewRecord = 1            
  BEGIN            
            
   Select @AccountType= AccountType, @Status = Status,@CurrencyID=CurrencyID, @ForeignAmount=ForeignAmount, @wDate = wDate,  
   @ValueDate = ValueDate , @AccountName = AccountName , @BankCode = BankID, @BranchCode = BranchID, @ProductID = ProductID,  
   @SupervisorID=SupervisorID,@ChequeDate = ChequeDate, @ExchangeRate = ExchangeRate, @AddData = AdditionalData,  
   @DescriptionID=DescriptionID, @Description = [Description], @AppWHTax=AppWHTax, @WHTScrollNo=WHTScrollNo,  
   @WHTaxAmount = WHTaxAmount, @WHTaxMode = WHTaxMode  
   From t_InwardClearing  
   Where SerialNo = @SerialNo AND OurBranchID = @OurBranchID and AccountID = @AccountID and ChequeID = @ChequeID  
     
   if UPPER(@CurrencyID) = UPPER(@LocalCurrency)  
   begin  
      set @IsLocalCurrency = 1  
      set @ExchangeRate = 1  
   end  
   else  
   begin  
      set @IsLocalCurrency=0  
   end  
     
   set @Year = CONVERT(varchar(4),YEAR(@wDate))  
     
   if MONTH(@wDate) < 10  
     set @Month = '0'+CONVERT(varchar(4),MONTH(@wDate))  
   else  
     set @Month = CONVERT(varchar(4),MONTH(@wDate))  
     
   if DAY(@wDate) < 10  
     set @Day = '0'+CONVERT(varchar(4),DAY(@wDate))  
   else  
     set @Day = CONVERT(varchar(4),DAY(@wDate))  
     
   set @RefNo = 'ID-'+@OurBranchID+'-'+@Year+@Month+@Day  
     
   select @GLControl = ISNULL(GLControl, '') from t_Products where OurBranchID = @OurBranchID and ProductID = @ProductID  
     
   /* Update Balance File */            
   IF @Status <> 'R'            
    BEGIN            
     IF @AccountType = 'C'            
      BEGIN            
       IF @Status = ''               
        BEGIN            
           IF  @CurrencyID = @LocalCurrency            
          BEGIN            
            Update t_AccountBalance            
           Set ClearBalance = (ClearBalance + @Amount)            
           Where OurBranchID = @OurBranchID and AccountID = @AccountID            
          END            
         ELSE            
          BEGIN            
            Update t_AccountBalance            
           Set ClearBalance = (ClearBalance + @ForeignAmount),            
            LocalClearBalance = (LocalClearBalance + @Amount)            
           Where OurBranchID = @OurBranchID and AccountID = @AccountID            
          END            
        END            
              
       ELSE --IF @Status = 'U' OR @Status = '*' OR @Status = 'E'            
        BEGIN              
            IF  @CurrencyID = @LocalCurrency            
           BEGIN            
             Update t_AccountBalance            
            Set ShadowBalance = (ShadowBalance + @Amount)            
            Where OurBranchID = @OurBranchID and AccountID = @AccountID            
           END            
          ELSE            
           BEGIN            
             Update t_AccountBalance            
            Set ShadowBalance = (ShadowBalance + @ForeignAmount)            
            Where OurBranchID = @OurBranchID and AccountID = @AccountID            
           END            
        END            
              
  -- Delete From Cheque Paid Table            
            
       Delete From  t_ChequePaid            
       Where  OurBranchID = @OurBranchID and AccountID = @AccountID and ChequeID = @ChequeID            
      END            
            
     IF @IsApplyCharges = 0            
      Update t_InwardClearing            
      Set Status = 'R', Reason = @Reason             
        Where (SerialNo = @SerialNo) and (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (ChequeID = @ChequeID)            
                 
     Update t_CustomerStatus            
     Set Status = 'R'            
     Where OurBranchID = @OurbranchID and AccountID = @AccountID and SerialNo = @SerialNo and TransactionType = 'DI'          
               
               
     IF @AccountType = 'G'            
      BEGIN            
     --Local Remittance Unpaid Mark            
       Update t_LR_Issuance            
       Set Paid = 0,          
       PaidDate = '1/1/1900',          
       PaidScrollNo = 0,          
       Supervision = 'C' ,          
       PaidMode = 'NA'          
       Where OurBranchID=@OurBranchID and InstrumentAccountID = @AccountID and PaidScrollNo = @SerialNo           
       and convert(varchar(10),PaidDate,101) = convert(varchar(10), @wDate,101) and PaidMode = 'IW'          
                 
       /*          
     --Local Remittance Unpaid Mark            
       Update t_LR_Advice            
       Set Paid = 0,          
       PaidDate = '1/1/1900',          
       PaidScrollNo = 0,          
       PaidMode = 'NA'          
       Where OurBranchID=@OurBranchID and InstrumentAccountID = @AccountID and PaidScrollNo = @SerialNo          
       and convert(varchar(10),PaidDate,101) = convert(varchar(10), @wDate,101) and PaidMode = 'IW'          
       */          
                 
      END            
            
    END            
            
   IF @IsApplyCharges = 1 AND @AccountType = 'C'
   BEGIN
     
     Update t_InwardClearing
     Set Status = 'R', Reason = @Reason, ChargesAmount = @ChargeAmt, ChargesScrollNo = @ChargesScrollNo, IsApplyCharges = 1
     Where (SerialNo = @SerialNo) and (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (ChequeID = @ChequeID)
     
   END
   
   declare @WHTaxSupervisionStatus char(1)  
   declare @CrGLAccount varchar(15)  
     
   set @WHTaxSupervisionStatus = ''  
     
   IF @Status <> 'R'  
   begin  
        
      IF @Status = ''  
      begin  
           
         if @AccountType = 'C'  
         begin  
              
            Select @WHTaxSupervisionStatus = Supervision from t_TransferTransactionModel  
            WHERE (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (ScrollNo = @WHTScrollNo) and (SerialNo = '1')  
              
            update t_Transactions set [Status] = 'R'  
            where OurBranchID = @OurBranchID and AccountID = @AccountID and DocumentType = 'ID'  
            and CONVERT(varchar(10),wDate,101)= convert(varchar(10),@wDate,101) and ScrollNo = @SerialNo  
              
            select @GLVoucherID = isnull(GLVoucherID,0) from t_Transactions  
            where OurBranchID = @OurBranchID and AccountID = @AccountID and DocumentType = 'ID'  
            and CONVERT(varchar(10),wDate,101)= convert(varchar(10),@wDate,101) and ScrollNo = @SerialNo  
              
         end  
         else  
         begin  
              
            select @GLControl = @AccountID  
              
            select @GLVoucherID = isnull(VoucherID,0) from t_GLTransactions  
            where OurBranchID = @OurBranchID and AccountID = @AccountID and DocType = 'ID'  
            and CONVERT(varchar(10),[Date],101)= convert(varchar(10),@wDate,101) and ScrollNo = @SerialNo  
              
         end  
           
      end  
        
      if @GLVoucherID = 0  
      begin  
           
         if @AccountType = 'C'  
         begin  
              
            insert into t_RejectedTrx (OurBranchID, ScrollNo, SerialNo, RefNo, wDate, AccountType, DocumentType, AccountID, AccountName,  
            ProductID, CurrencyID, ValueDate, TrxType, ChequeID, ChequeDate, Amount, ForeignAmount, ExchangeRate, ProfitLoss, MeanRate,  
            DescriptionID, [Description], BankCode, BranchCode, TrxPrinted, [Status], IsLocalCurrency, OperatorID,  
            SupervisorID, AdditionalData, IsMainTrx, DocType, AppWHTax, WHTaxAmount, WHTaxMode, WHTScrollNo)  
              
            VALUES (@OurBranchID,@SerialNo,'0','',@wDate,@AccountType,'ID',@AccountID,@AccountName,  
            @ProductID,@CurrencyID,@ValueDate,@TrxType,@ChequeID,@ChequeDate, @Amount, @ForeignAmount,@ExchangeRate,0,@ExchangeRate,  
            @DescriptionID,@Description,@BankCode,@BranchCode, 0, 'R', @IsLocalCurrency, @OperatorID, @SupervisorID,   
            @AddData,'1','ID', @AppWHTax, @WHTaxAmount, @WHTaxMode, @WHTScrollNo)  
              
         end  
         else  
         begin  
              
            if @TrxType = 'C'  
               set @IsCredit = 1  
            else  
               set @IsCredit = 0  
              
            if @IsLocalCurrency = '1'  
            begin  
                 
               set @ForeignAmount = 0  
               set @ExchangeRate = 1  
               set @TransactionMethod = 'L'  
                 
            end  
            else  
               set @TransactionMethod = 'A'  
              
            insert into t_RejectedGLTrx  
            (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description], CurrencyID,  
            Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,   
            Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType)  
              
            VALUES (@OurBranchID, @AccountID, '1', '1', @wDate, @ValueDate, @DescriptionID, @Description, @CurrencyID, @Amount, @ForeignAmount,  
            @ExchangeRate, @IsCredit, 'Inward', @TransactionMethod, @OperatorID, @SupervisorID, '', @SerialNo, '0', @AddData, '1', 'ID')  
              
         end  
           
      end  
      else  
      begin  
           
         update t_GLTransactions set [Status] = 'R'  
         where OurBranchID = @OurBranchID and VoucherID = @GLVoucherID and DocType = 'ID'  
         and CONVERT(varchar(10),[Date],101) = CONVERT(varchar(10),@wDate,101)  
           
         declare @ClearingGLID nvarchar(30)  
         set @ClearingGLID = ''  
           
         if @IsLocalCurrency = 1  
         begin  
              
            set @ForeignAmount = 0  
            set @ExchangeRate = 1  
            set @TransactionMethod = 'L'  
            
            SELECT @ClearingGLID = G.AccountID FROM t_GLParameters P  
            INNER JOIN t_GL G ON P.OurBranchID = G.OurBranchID AND P.AccountID = G.AccountID  
            WHERE P.OurBranchID = @OurBranchID AND P.SerialID = '2'  
              
         end  
         else  
         begin  
              
            set @TransactionMethod = 'A'  
              
            SELECT @ClearingGLID = G.AccountID  
            FROM t_CurrenciesAccount C  
            INNER JOIN t_GLParameters GP ON C.OurBranchID = GP.OurBranchID AND C.HOSerialID = GP.SerialID  
            INNER JOIN t_GL G ON G.OurBranchID = GP.OurBranchID AND G.AccountID = GP.AccountID  
            WHERE C.OurBranchID = @OurBranchID AND C.CurrencyID = @CurrencyID  
              
         end  
           
         if @ClearingGLID = ''  
         begin  
            SELECT '7' as [Status]  
            RETURN(0)  
         end  
           
         if @IsLocalCurrency = '1'  
         begin  
              
            if @TrxType = 'C'  
            begin  
                 
               Update t_GL Set Balance = Balance - @Amount Where OurBranchID = @OurBranchID and AccountID = @GLControl  
               Update t_GL Set Balance = Balance + @Amount Where OurBranchID = @OurBranchID and AccountID = @ClearingGLID  
                 
               update t_GLTransactions set Amount = Amount + @Amount
               where OurBranchID = @OurBranchID AND AccountID = @ClearingGLID AND IDRefNo = @RefNo  
                 
            end  
            else  
            begin  
                 
               Update t_GL Set Balance = Balance + @Amount Where OurBranchID = @OurBranchID and AccountID = @GLControl  
               Update t_GL Set Balance = Balance - @Amount Where OurBranchID = @OurBranchID and AccountID = @ClearingGLID  
                 
               update t_GLTransactions set Amount = Amount - @Amount
               where OurBranchID = @OurBranchID AND AccountID = @ClearingGLID AND IDRefNo = @RefNo  
                 
            end  
              
         end  
         else  
         begin  
              
            if @TrxType = 'C'  
            begin  
                 
               Update t_GL Set Balance = Balance - @Amount, ForeignBalance = ForeignBalance - @ForeignAmount  
               Where OurBranchID = @OurBranchID and AccountID = @GLControl  
                 
               Update t_GL Set Balance = Balance + @Amount, ForeignBalance = ForeignBalance + @ForeignAmount  
               Where OurBranchID = @OurBranchID and AccountID = @ClearingGLID  
                 
               update t_GLTransactions set
               Amount = Amount + @Amount,
               ForeignAmount = ForeignAmount + @ForeignAmount,
               ExchangeRate = (ExchangeRate + @ExchangeRate)/2
               where OurBranchID = @OurBranchID AND AccountID = @ClearingGLID AND IDRefNo = @RefNo  
                 
            end  
            else  
            begin  
                 
               Update t_GL Set Balance = Balance + @Amount, ForeignBalance = ForeignBalance + @ForeignAmount  
               Where OurBranchID = @OurBranchID and AccountID = @GLControl  
                 
               Update t_GL Set Balance = Balance - @Amount, ForeignBalance = ForeignBalance - @ForeignAmount  
               Where OurBranchID = @OurBranchID and AccountID = @ClearingGLID  
                 
               update t_GLTransactions set
               Amount = Amount - @Amount,
               ForeignAmount = ForeignAmount - @ForeignAmount,
               ExchangeRate = (ExchangeRate + @ExchangeRate)/2
               where OurBranchID = @OurBranchID AND AccountID = @ClearingGLID AND IDRefNo = @RefNo
               
            end  
              
         end  
           
      end  
   end  
     
 END  
   
 Select '0' as ReturnStatus, @ChargesScrollNo as ScrollNo, @ChargeAmt as ChargesAmount  
   
 if @WHTaxSupervisionStatus <> 'R'  
 BEGIN  
      
    exec spc_DeleteTransferTrx @OurBranchID, @AccountID, @SupervisorID, @WHTScrollNo, 1  
      
    select @CrGLAccount=AccountID from t_TransferTransactionModel  
    WHERE (OurBranchID = @OurBranchID) and (ScrollNo = @WHTScrollNo) and (SerialNo = 2)  
      
    exec spc_DeleteTransferTrx @OurBranchID, @CrGLAccount, @SupervisorID, @WHTScrollNo, 2  
      
 END