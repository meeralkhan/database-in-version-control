﻿CREATE Procedure [dbo].[spc_BODEODStatus]      
(      
 @OurBranchID nvarchar(30)      
)      
AS      
  
SET NOCOUNT ON  
  
declare @wdate datetime      
declare @EOD datetime      
declare @BOD datetime      
      
Begin Try      
       
 if ((select COUNT(OurBranchID) from t_last where OurBranchID = @OurBranchID) > 0)      
 BEGIN      
  select @wdate = cast(floor(cast(workingdate as float)) as datetime), @EOD = cast(floor(cast(lastEod as float)) as datetime) , @BOD = cast(floor(cast(LASTBOD as float)) as datetime) from t_last where OurBranchID = @OurBranchID      
        
  If @WDATE = @EOD      
  Begin      
   select 'EODDone' As ReturnStatus, @wdate AS wDate, 'Eod Done No Posting or Updates Allowed...Except ATM like Channels'      
   Return      
  End      
      
  else if @WDATE = @BOD      
  Begin      
   Select 'OKPosting' As ReturnStatus, @wdate AS wDate, 'Ok for Posting' AS ReturnMessage      
   Return      
  End      
      
  else     
  Begin      
   Select 'Error' As ReturnStatus, @wdate AS wDate, 'Invalid Working Date' AS ReturnMessage      
   Return      
  End      
 END      
 else      
 begin      
  Select 'Error' As ReturnStatus, @wdate AS wDate,  'No Record Found...' AS ReturnMessage      
 end      
      
End Try      
Begin Catch      
       
 Select 'Error' As ReturnStatus, @wdate AS wDate,  ERROR_MESSAGE() AS ReturnMessage      
       
End Catch