﻿CREATE   PROCEDURE [dbo].[spc_GetMetaDataField]                  
(                                  
 @ClientID nvarchar(30) = '',
 @AccountID nvarchar(30) = '',
 @ProductID nvarchar(30) = ''
)

AS                  

SET NOCOUNT ON        

   select a.AccountID,a.Name AccountName,c.ClientID,c.Name ClientName,b.ProductID,b.Description ProductName from t_Account a
   inner join t_Customer c on c.ClientID = a.ClientID and a.OurBranchID = c.OurBranchID
   inner join t_Products b on a.ProductID = b.ProductID and a.OurBranchID = b.OurBranchID
   where a.AccountID = @AccountID and a.ProductID = @ProductID and a.ClientID = @ClientID