﻿CREATE PROCEDURE [dbo].[spc_RejectBureauDeChange]  
(   
 @OurBranchID varchar(30),   
 @ScrollNo numeric(18,0),   
 @SupervisorID varchar(30) = 0,
 @AdditionalData text = ''
)   
   
AS   
   
 SET NOCOUNT ON   
   
 DECLARE @Status char(1)   
 DECLARE @Supervision char(1),
 @SellOrBuy char(1),
 @CurrencyAccountID varchar(30),
 @CashAccountID varchar(30),
 @CommissionAccountID varchar(30),
 @wDate datetime,
 @DescriptionID varchar(30),
 @Description varchar(255),
 @CurrencyID varchar(30),
 @Amount money,
 @ForeignAmount money,
 @ExchangeRate decimal(18,6),
 @OperatorID varchar(30),
 @NetAmount money,
 @Commission money
 
 SELECT @Status = 'R'  
   
 IF NOT EXISTS (SELECT Supervision FROM t_BureaDeChange WHERE OurBranchID = @OurBranchID and ScrollNo = @ScrollNo)   
 begin  
  SELECT '1' as RetStatus -- Trx. Not Found  
  RETURN  
 end  
   
 -- Get Transaction  
 SELECT @Supervision = Supervision, @SellOrBuy = SellOrBuy, @CurrencyAccountID = CurrAccountID, @CashAccountID = CashAccountID,
 @CommissionAccountID = CommAccountID, @wDate = wDate, @DescriptionID = DescriptionID, @Description = [Description], 
 @CurrencyID = CurrencyID, @Amount = Amount, @ForeignAmount = ForeignAmount, @ExchangeRate = ExchangeRate, 
 @OperatorID = OperatorID, @NetAmount = NetAmount, @Commission = Commission, @AdditionalData = AdditionalData
 FROM t_BureaDeChange WHERE (OurBranchID = @OurBranchID) and (ScrollNo = @ScrollNo)   
   
 IF @Supervision = 'R'   
 BEGIN   
  SELECT '2' as RetStatus -- Trx. Already Rejected   
  RETURN   
 END   
   
 -- Update Transaction File  
 UPDATE t_BureaDeChange  
 SET Supervision = @Status, SupervisorID = @SupervisorID   
 WHERE (OurBranchID = @OurBranchID) and (ScrollNo = @ScrollNo)   
 
 declare @GLVoucherID1 int, @GLVoucherID2 int
 
 IF @Supervision <> 'C'
 BEGIN
    
    declare @IsCredit int
    if @SellOrBuy = 'B'
       set @IsCredit = 1
    else
       set @IsCredit = 0
    
    insert into t_RejectedGLTrx
    (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description], CurrencyID,
    Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID, 
    Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType, GLID)
    
    VALUES (@OurBranchID, @CurrencyAccountID, '1', '1', @wDate, @wDate, @DescriptionID, @Description, @CurrencyID, @Amount, @ForeignAmount,
    @ExchangeRate, @IsCredit, 'Bureau', 'A', @OperatorID, @SupervisorID, '', @ScrollNo, '0', @AdditionalData, '1', 'B', @CashAccountID) 
    
 END
 ELSE
 BEGIN
    
    select @GLVoucherID1 = isnull(VoucherID,0) from t_GLTransactions
    where OurBranchID = @OurBranchID and AccountID = @CurrencyAccountID
    and ScrollNo = @ScrollNo and IsMainTrx = '1' and DocType = 'B'
    and CONVERT(varchar(10),[Date],101) = CONVERT(varchar(10),@wDate,101)
    
    select @GLVoucherID2 = isnull(VoucherID,0) from t_GLTransactions
    where OurBranchID = @OurBranchID and AccountID = @CashAccountID
    and ScrollNo = @ScrollNo and IsMainTrx = '0' and DocType = 'B'
    and CONVERT(varchar(10),[Date],101) = CONVERT(varchar(10),@wDate,101)
    
    update t_GLTransactions set [Status] = 'R'
    where OurBranchID = @OurBranchID and VoucherID = @GLVoucherID1
    and DocType = 'B' and CONVERT(varchar(10),[Date],101) = CONVERT(varchar(10),@wDate,101)
    
    update t_GLTransactions set [Status] = 'R'
    where OurBranchID = @OurBranchID and VoucherID = @GLVoucherID2
    and DocType = 'B' and CONVERT(varchar(10),[Date],101) = CONVERT(varchar(10),@wDate,101)
    
    if @SellOrBuy = 'B'
    begin
       
       set @NetAmount = @Amount + @Commission
       
       Update t_GL Set Balance = Balance - @Amount, ForeignBalance = ForeignBalance - @ForeignAmount
       Where OurBranchID = @OurBranchID and AccountID = @CurrencyAccountID
       
       Update t_GL Set Balance = Balance + @NetAmount Where OurBranchID = @OurBranchID and AccountID = @CashAccountID
       
    end
    else
    begin
       
       set @NetAmount = @Amount - @Commission
       
       Update t_GL Set Balance = Balance + @Amount, ForeignBalance = ForeignBalance + @ForeignAmount
       Where OurBranchID = @OurBranchID and AccountID = @CurrencyAccountID
       
       Update t_GL Set Balance = Balance - @NetAmount Where OurBranchID = @OurBranchID and AccountID = @CashAccountID
       
    end
    
    if @Commission > 0
    begin
       
       Update t_GL Set Balance = Balance - @Commission Where OurBranchID = @OurBranchID and AccountID = @CommissionAccountID
       
    end
    
 END
 
 SELECT '0' as RetStatus   
 RETURN