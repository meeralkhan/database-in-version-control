﻿CREATE PROCEDURE [dbo].[spc_GetCB950OpeningBalance]
(
 @OurBranchID varchar(30),
 @AccountID varchar(30),
 @sDate Datetime
)

AS

 SET NOCOUNT ON
 
 DECLARE @sdDate Datetime, @eDate Datetime

 set @sdDate = CONVERT(VARCHAR(10),@sDate,101);
 set @sDate = @sdDate
 set @eDate = @sDate

 DECLARE @BaseCurrency as NVarchar(30) 
 DECLARE @CurrencyID as NVarchar(30) 
 DECLARE @OpeningBalance as money
 DECLARE @crOpBal as money
 DECLARE @drOpBal as money
 DECLARE @tDate as Varchar(15)
 DECLARE @TrxType as varchar(1)
 
 SELECT @tDate = cast(Month(@sDate) as Varchar(2)) + '/01/' + cast(Year(@sDate) as Varchar(4)), @OpeningBalance = 0, @crOpBal = 0, @drOpBal = 0

 SELECT @BaseCurrency = LocalCurrency FROM t_GlobalVariables WHERE OurBranchID = @OurBranchID
 SELECT @CurrencyID = CurrencyID FROM t_Account WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID

 SELECT @OpeningBalance = IsNull(ClearBalance,0) FROM t_AccountMonthBalances WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND Month = @tDate

 if @CurrencyID = @BaseCurrency
 begin
    SELECT @crOpBal = IsNull(SUM(Amount),0) From t_Transactions WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID
    AND TrxType = 'C' AND AccountType = 'C' AND [Status] <> 'R' AND wDate >= @tDate AND wDate < @sDate
 end
 else
 begin
    SELECT @crOpBal = IsNull(SUM(ForeignAmount),0) From t_Transactions WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID
    AND TrxType = 'C' AND AccountType = 'C' AND [Status] <> 'R' AND wDate >= @tDate AND wDate < @sDate
 end
 
 if @CurrencyID = @BaseCurrency
 begin
    SELECT @drOpBal = IsNull(SUM(Amount),0) From t_Transactions WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID
    AND TrxType = 'D' AND AccountType = 'C' AND [Status] <> 'R' AND wDate >= @tDate AND wDate < @sDate
 end
 else
 begin
    SELECT @drOpBal = IsNull(SUM(ForeignAmount),0) From t_Transactions WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID
    AND TrxType = 'D' AND AccountType = 'C' AND [Status] <> 'R' AND wDate >= @tDate AND wDate < @sDate
 end

 SET @OpeningBalance = @OpeningBalance + @crOpBal - @drOpbal 

 set @TrxType = 'C'
 if @OpeningBalance < 0
    set @TrxType = 'D'

 set @OpeningBalance = ABS(@OpeningBalance)

 SELECT @OurBranchID AS OurBranchID, @AccountID AS AccountID, SUBSTRING(CONVERT(VARCHAR(8),@sDate,112),3,8) AS OpeningBalanceDate, @TrxType as OpeningBalanceMark, ISNULL(@CurrencyID,'') as OpeningBalanceCcy, IsNull(@OpeningBalance,0) as OpeningBalance