﻿create PROCEDURE [dbo].[spc_GetCustomerStatementTime]          
(          
 @OurBranchID varchar(30),          
 @AccountID varchar(30),          
 @sDate Datetime,          
 @eDate Datetime          
)          
          
AS          
BEGIN          
         
 SET NOCOUNT ON          
          
 DECLARE @CurrencyID nvarchar(30)        
 DECLARE @LocalCurrency as Varchar(4)                                                     
   DECLARE @OpeningBalance as money                                                  
                                                  
   DECLARE @crOpBal as money                                                  
                                                  
                                                  
   DECLARE @drOpBal as money                                                  
   DECLARE @tDate as Varchar(15)                                                  
   DECLARE @WorkingDate as datetime                                                  
                                                  
   SELECT @WorkingDate = WorkingDate From t_Last                                                  
   WHERE OurBranchID = @OurBranchID                                                  
                                 
   if @eDate > @WorkingDate                              
   begin                              
      set @eDate = @WorkingDate                              
   end                              
                                 
   SET @tDate = cast(Month(@sDate) as Varchar(2)) + '/01/' + cast(Year(@sDate)  as Varchar(4))                                                  
                                                  
 SET @OpeningBalance = 0                                                  
 SET @crOpBal = 0                                                  
 SET @drOpBal = 0                                                  
        
   SELECT @CurrencyID = CurrencyID                                                
   FROM t_Account                                                 
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID    

 

   SELECT @OpeningBalance = IsNull(ClearBalance,0)                                                 
   FROM t_AccountMonthBalances                                                  
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                                                   
   AND Month = @tDate                                                  
                                                 
   SELECT @crOpBal = IsNull(SUM(Amount),0) From t_Transactions                                                  
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                                                   
         AND TrxType = 'C' AND AccountType = 'C' AND [Status] <> 'R'                                                  
    --     AND convert(varchar(10) , wDate,101) >= @tDate AND convert(varchar(10) , wDate,101) < @sDate                                                  
  AND wDate >= @tDate AND wDate < @sDate                                                  
                                                  
   SELECT @drOpBal = IsNull(SUM(Amount),0) From t_Transactions                                                  
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                                                   
         AND TrxType = 'D' AND AccountType = 'C' AND [Status] <> 'R'                                                  
         --AND convert(varchar(10) , wDate,101) >= @tDate AND convert(varchar(10) , wDate,101) < @sDate                                                  
  AND wDate >= @tDate AND wDate < @sDate                                                  
                                                
   SET @OpeningBalance = @OpeningBalance + @crOpBal - @drOpbal                                   
                   
 select OurBranchID, AccountID, CurrencyID, Amount, ForeignAmount, IsCredit, ValueDate, [Date], ChannelRefID, TrxType, VoucherID, SerialID, DescriptionID, Description, TrxTimeStamp, TableStr from (          
           
 SELECT @OurBranchID AS OurBranchID, @AccountID AS AccountID, @CurrencyID AS CurrencyID, IsNull(@OpeningBalance,0) as Amount, 0 as ForeignAmount, '0' AS IsCredit, @sDate as ValueDate, '01/01/1900' as [Date],        
 '' AS ChannelRefID, '' AS TrxType, 0 as VoucherID, 0 as SerialID, 'OP' AS DescriptionID, 'Opening Balance' as Description, '01/01/1900' as TrxTimeStamp, 'OP' As TableStr        
        
 UNION ALL        
        
 SELECT OurBranchID, AccountID, CurrencyID, Amount, 0 as ForeignAmount, case TrxType when 'C' then '1' else '0' end AS IsCredit, ValueDate, wDate AS [Date], ISNULL(ChannelRefID,''), TrxType,        
 ScrollNo as VoucherID, SerialNo as SerialID, DescriptionID, Description, TrxTimeStamp, 'H' As TableStr        
 FROM t_Transactions        
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID        
 AND AccountType = 'C' AND [Status] <> 'R'        
 AND convert(varchar(10), wDate,101) >= @sDate AND convert(varchar(10) , wDate,101) <= @eDate        
        
 ) a order by TrxTimeStamp        
           
END