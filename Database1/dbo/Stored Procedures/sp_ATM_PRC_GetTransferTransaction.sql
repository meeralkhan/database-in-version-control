﻿CREATE PROCEDURE [dbo].[sp_ATM_PRC_GetTransferTransaction]
	@OurBranchID varchar(30)
 	
AS
	if Exists (Select AccountID From t_ATM_TransferTransaction Where Supervision  in ('*','P'))
		Select  'Error'
	Else
		BEGIN
			Select a.*,b.GLControl From t_ATM_TransferTransaction a,t_Products b
			Where a.ProductID = b.ProductID 
			Order By a.ScrollNo,a.SerialNo
		END