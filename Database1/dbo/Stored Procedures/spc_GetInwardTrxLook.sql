﻿CREATE PROCEDURE [dbo].[spc_GetInwardTrxLook]    
(      
      @OurBranchID       Varchar(30),      
      @OperatorID        Varchar(30)='',      
      @AccountID         Varchar(30)='',      
      @BankID           Varchar(30) = '',    
      @Status            Char(1)='U',    
      @ScrollNoS         Numeric(18,0)=0,      
      @ScrollNoE         Numeric(18,0)=999999999999,      
      @AmountS           Numeric(18,0)=0,      
      @AmountE           Numeric(18,0) = 9999999999999,      
      @ChequeIDS            Varchar(11)='00000000000',      
      @ChequeIDE            Varchar(11)='v' --'99999999999'      
)      
      
AS      
    
SET NOCOUNT ON     
    
if @Status = 'B'    
 set @Status = ''    
    
-- 1-            --Get Un-Supervised Trx.       
   IF @BankID = '' AND @Status ='H'      
      BEGIN      
            IF (SELECT count(SerialNo) FROM t_InwardClearing      
                WHERE OurBranchID = @OurBranchID       
                           AND SerialNo >= @ScrollNoS AND SerialNo <= @ScrollNoE      
                           AND Amount >= @AmountS AND Amount <= @AmountE              
                           AND ChequeID >= @ChequeIDS AND ChequeID <= @ChequeIDE      
                           AND Not Status IN ('','R') AND OperatorID Like @OperatorID + '%'       
         AND AccountID Like @AccountID + '%' ) > 0      
               BEGIN      
     SELECT  '1' as RetStatus,SerialNo As ScrollNo,BankID, AccountID, AccountName, AccountType,ChequeID,      
       CurrencyID,ChequeDate, Amount, ForeignAmount, Status, OperatorID, SupervisorID,      
      Reason,TransactionType as TrxType,IsApplyCharges,ChargesScrollno      
     FROM t_InwardClearing      
                    WHERE OurBranchID = @OurBranchID       
                           AND SerialNo >= @ScrollNoS AND SerialNo <= @ScrollNoE      
                           AND Amount >= @AmountS AND Amount <= @AmountE              
                           AND ChequeID >= @ChequeIDS AND ChequeID <= @ChequeIDE      
                           AND Not Status IN ('','R')  AND OperatorID Like @OperatorID + '%'       
         AND AccountID Like @AccountID + '%'      
               END      
            ELSE      
               BEGIN      
                  SELECT '0' AS RetStatus      
                  RETURN      
               END      
      END      
      
-- 2- All Transaction      
   ELSE IF @BankID = '' AND @Status ='A'      
      BEGIN      
            IF (SELECT count(SerialNo) FROM t_InwardClearing      
                WHERE OurBranchID = @OurBranchID       
                           AND SerialNo >= @ScrollNoS AND SerialNo <= @ScrollNoE      
                           AND Amount >= @AmountS AND Amount <= @AmountE              
                           AND ChequeID >= @ChequeIDS AND ChequeID <= @ChequeIDE      
        AND OperatorID Like @OperatorID + '%'       
         AND AccountID Like @AccountID + '%') > 0      
               BEGIN      
     SELECT  '1' as RetStatus,SerialNo As ScrollNo,BankID, AccountID, AccountName, AccountType,ChequeID,      
       CurrencyID,ChequeDate, Amount, ForeignAmount, Status, OperatorID, SupervisorID,      
      Reason,TransactionType as TrxType,IsApplyCharges,ChargesScrollno      
     FROM t_InwardClearing      
                    WHERE OurBranchID = @OurBranchID       
                           AND SerialNo >= @ScrollNoS AND SerialNo <= @ScrollNoE      
                           AND Amount >= @AmountS AND Amount <= @AmountE              
                           AND ChequeID >= @ChequeIDS AND ChequeID <= @ChequeIDE      
         AND OperatorID Like @OperatorID + '%'       
         AND AccountID Like @AccountID + '%'      
               END      
            ELSE      
               BEGIN      
                  SELECT '0' AS RetStatus      
                  RETURN      
               END      
      END      
      
   ELSE IF @BankID = ''      
      BEGIN      
            IF (SELECT count(SerialNo) FROM t_InwardClearing      
                WHERE OurBranchID = @OurBranchID       
                           AND SerialNo >= @ScrollNoS AND SerialNo <= @ScrollNoE      
                           AND Amount >= @AmountS AND Amount <= @AmountE              
                           AND ChequeID >= @ChequeIDS AND ChequeID <= @ChequeIDE      
                           AND Status = @Status  AND OperatorID Like @OperatorID + '%'       
         AND AccountID Like @AccountID + '%') > 0      
               BEGIN      
     SELECT  '1' as RetStatus,SerialNo As ScrollNo,BankID, AccountID, AccountName, AccountType,ChequeID,      
       CurrencyID,ChequeDate, Amount, ForeignAmount, Status, OperatorID, SupervisorID,      
      Reason,TransactionType as TrxType,IsApplyCharges,ChargesScrollno      
     FROM t_InwardClearing      
                    WHERE OurBranchID = @OurBranchID       
                           AND SerialNo >= @ScrollNoS AND SerialNo <= @ScrollNoE      
                           AND Amount >= @AmountS AND Amount <= @AmountE              
                           AND ChequeID >= @ChequeIDS AND ChequeID <= @ChequeIDE      
                           AND Status = @Status  AND OperatorID Like @OperatorID + '%'       
         AND AccountID Like @AccountID + '%'      
               END      
            ELSE      
               BEGIN      
                  SELECT '0' AS RetStatus      
                  RETURN      
               END      
      END      
      
      
   ELSE IF @Status ='H'      
      BEGIN      
            IF (SELECT count(SerialNo) FROM t_InwardClearing      
                WHERE OurBranchID = @OurBranchID       
                           AND SerialNo >= @ScrollNoS AND SerialNo <= @ScrollNoE      
                           AND Amount >= @AmountS AND Amount <= @AmountE              
                           AND ChequeID >= @ChequeIDS AND ChequeID <= @ChequeIDE      
                           AND Not Status IN ('','R') AND OperatorID Like @OperatorID + '%'       
         AND AccountID Like @AccountID + '%' AND BankID = @BankID ) > 0      
               BEGIN      
     SELECT  '1' as RetStatus,SerialNo As ScrollNo,BankID, AccountID, AccountName, AccountType,ChequeID,      
       CurrencyID,ChequeDate, Amount, ForeignAmount, Status, OperatorID, SupervisorID,      
      Reason,TransactionType as TrxType,IsApplyCharges,ChargesScrollno      
     FROM t_InwardClearing      
                    WHERE OurBranchID = @OurBranchID       
                           AND SerialNo >= @ScrollNoS AND SerialNo <= @ScrollNoE      
                           AND Amount >= @AmountS AND Amount <= @AmountE              
                           AND ChequeID >= @ChequeIDS AND ChequeID <= @ChequeIDE      
                           AND Not Status IN ('','R')  AND OperatorID Like @OperatorID + '%'       
         AND AccountID Like @AccountID + '%' AND BankID = @BankID      
               END      
            ELSE      
               BEGIN      
                  SELECT '0' AS RetStatus      
                  RETURN      
               END      
      END      
      
      
   ELSE IF @Status ='A'      
      BEGIN      
            IF (SELECT count(SerialNo) FROM t_InwardClearing      
                WHERE OurBranchID = @OurBranchID       
                           AND SerialNo >= @ScrollNoS AND SerialNo <= @ScrollNoE      
                           AND Amount >= @AmountS AND Amount <= @AmountE              
                           AND ChequeID >= @ChequeIDS AND ChequeID <= @ChequeIDE      
        AND OperatorID Like @OperatorID + '%'       
         AND AccountID Like @AccountID + '%'  AND BankID = @BankID) > 0      
               BEGIN      
     SELECT  '1' as RetStatus,SerialNo As ScrollNo,BankID, AccountID, AccountName, AccountType,ChequeID,      
       CurrencyID,ChequeDate, Amount, ForeignAmount, Status, OperatorID, SupervisorID,      
      Reason,TransactionType as TrxType,IsApplyCharges,ChargesScrollno      
     FROM t_InwardClearing      
                    WHERE OurBranchID = @OurBranchID       
                           AND SerialNo >= @ScrollNoS AND SerialNo <= @ScrollNoE      
                           AND Amount >= @AmountS AND Amount <= @AmountE              
                           AND ChequeID >= @ChequeIDS AND ChequeID <= @ChequeIDE      
         AND OperatorID Like @OperatorID + '%'       
        AND AccountID Like @AccountID + '%'  AND BankID = @BankID      
               END      
            ELSE      
               BEGIN      
                  SELECT '0' AS RetStatus      
                  RETURN      
               END      
      END      
      
   ELSE      
      BEGIN      
            IF (SELECT count(SerialNo) FROM t_InwardClearing      
                WHERE OurBranchID = @OurBranchID       
                           AND SerialNo >= @ScrollNoS AND SerialNo <= @ScrollNoE      
                           AND Amount >= @AmountS AND Amount <= @AmountE              
                           AND ChequeID >= @ChequeIDS AND ChequeID <= @ChequeIDE      
                           AND Status = @Status  AND OperatorID Like @OperatorID + '%'       
         AND AccountID Like @AccountID + '%'  AND BankID = @BankID) > 0      
               BEGIN      
     SELECT  '1' as RetStatus,SerialNo As ScrollNo,BankID, AccountID, AccountName, AccountType,ChequeID,      
       CurrencyID,ChequeDate, Amount, ForeignAmount, Status, OperatorID, SupervisorID,      
      Reason,TransactionType as TrxType,IsApplyCharges,ChargesScrollno      
     FROM t_InwardClearing      
                    WHERE OurBranchID = @OurBranchID       
                           AND SerialNo >= @ScrollNoS AND SerialNo <= @ScrollNoE      
                           AND Amount >= @AmountS AND Amount <= @AmountE              
                           AND ChequeID >= @ChequeIDS AND ChequeID <= @ChequeIDE      
                           AND Status = @Status  AND OperatorID Like @OperatorID + '%'       
         AND AccountID Like @AccountID + '%'  AND BankID = @BankID      
               END      
            ELSE      
               BEGIN      
                  SELECT '0' AS RetStatus      
                  RETURN      
               END      
      END