﻿CREATE PROCEDURE [dbo].[spc_AccountMonthBalances_All]
(
   @Month datetime
)
AS
SET NOCOUNT ON

UPDATE t_AccountBalance set
NoOfCrTrxMonthly = 0, NoOfDrTrxMonthly = 0, AmountOfCrTrxMonthly = 0, AmountOfDrTrxMonthly = 0,
NoOfCWTrxATMMonthly = 0, NoOfBITrxATMMonthly = 0, AmountOfCWTrxATMMonthly = 0, AmountOfBITrxATMMonthly = 0
where (NoOfCrTrxMonthly>0 OR NoOfDrTrxMonthly>0 OR AmountOfCrTrxMonthly>0 OR AmountOfDrTrxMonthly>0 OR
NoOfCWTrxATMMonthly>0 OR NoOfBITrxATMMonthly>0 OR AmountOfCWTrxATMMonthly>0 OR AmountOfBITrxATMMonthly>0)

Insert Into t_AccountMonthBalances
([Month],OurBranchID,AccountID,ProductID,[Name],ClearBalance,Effects,LocalClearBalance,LocalEffects)
Select @Month,OurBranchID,AccountID,ProductID,[Name],ClearBalance,Effects,LocalClearBalance,LocalEffects
From t_PrevAccountBalances

Update t_Last Set EOMUPDATE = @Month

SELECT 'Ok' AS RetStatus;