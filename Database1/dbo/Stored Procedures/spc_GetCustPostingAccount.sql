﻿CREATE PROCEDURE [dbo].[spc_GetCustPostingAccount]   
(   
   @OurBranchID varchar(30),   
   @AccountID varchar(30),   
   @TrxType varchar(5)='',   
   @OperatorID varchar(30)='',   
   @ModuleType varchar(2)='', --L-> Limit, --D-> Disbursement   
   @RestrictedIDs varchar(1)='0' -- 1-> On, 0-> Off   
)   
   
AS   
  
SET NOCOUNT ON   
   
DECLARE @Status as VarChar(3)   
DECLARE @AuthorizationStatus as Char(2)   
DECLARE @ProductID as Varchar(30)   
DECLARE @AccountName as Varchar(75)   
DECLARE @Reminder as Varchar(100)   
DECLARE @ProductName as Varchar(50)   
DECLARE @CurrencyID as Varchar(30)   
DECLARE @CurrencyName as Varchar(50)   
DECLARE @ClearBalance as Money   
DECLARE @Effects as Money   
DECLARE @ShadowBalance as Money   
DECLARE @Limit as Money   
DECLARE @BlockedAmount as Money   
DECLARE @MeanRate as decimal(18,6)   
DECLARE @AccountClearingDays as Money   
DECLARE @ProductMinBalance AS MONEY   
   
DECLARE @IsFreezed as Bit   
DECLARE @AllowCrDr as Bit   
DECLARE @CanGoCrDr as Bit   
DECLARE @GLID as Varchar(30)   
DECLARE @frmName as Varchar(100)   
   
DECLARE @LocalCurrency as Varchar(30)   
DECLARE @EveryTrxGoesInSupervision as Bit   
DECLARE @AllProductsAllowInFO as Bit   
DECLARE @IsPostAllCustomerAccounts as Bit   
DECLARE @IsUtilityAccount as bit   
   
DECLARE @ClientID as NVarchar(30)   
DECLARE @CNICNo as NVarchar(30)   
DECLARE @NTNNo as NVarchar(30)   
   
select @ClientID = ClientID from t_Account where OurBranchID = @OurBranchID and AccountID = @AccountID   
  
if @RestrictedIDs = '1'   
begin   
   
   select @CNICNo = ISNULL(NIC,''), @NTNNo = ISNULL(NTN,'') from t_Customer where OurBranchID = @OurBranchID and ClientID = @ClientID   
     
   if (select COUNT(OurBranchID) from t_RestrictedIDs where [Type] = 'NTN' and IDNo = @NTNNo) > 0   
   begin   
      SELECT '14' AS RetStatus, 'NTN' AS IDType, @NTNNo AS IDNo --Restricted ID's   
      RETURN(0)   
   end   
     
   if exists (select TOP 1 IDNo from t_RestrictedIDs where [Type] = 'CNIC' and (IDNo = @CNICNo OR IDNo IN (select ISNULL(NIC,'')   
   from t_AccountOperatedBy where OurBranchID = @OurBranchID and AccountID = @AccountID)))   
   begin   
      SELECT '14' AS RetStatus, 'CNIC' AS IDType, IDNo from t_RestrictedIDs where [Type] = 'CNIC' and (IDNo = @CNICNo OR IDNo IN (select ISNULL(NIC,'')   
      from t_AccountOperatedBy where OurBranchID = @OurBranchID and AccountID = @AccountID))   
      RETURN(0)   
   end   
  
end   
   
SELECT @LocalCurrency =UPPER(LocalCurrency),@AllProductsAllowInFO=isnull(AllProductsAllowInFO,'0'),   
@IsPostAllCustomerAccounts=isnull(IsPostAllCustomerAccounts,'0') From t_GlobalVariables WHERE OurBranchID = @OurBranchID   
   
SET @AccountClearingDays = 0   
SET @EveryTrxGoesInSupervision = 0   
SET @AllProductsAllowInFO = 1   
   
IF @IsPostAllCustomerAccounts = 1   
   if (@ModuleType <> 'L' AND @ModuleType <> 'D')   
   BEGIN   
      IF (Select Count(*) From t_UserAccountAccess   
      WHERE OurBranchID = @OurBranchID AND UserID = @OperatorID AND AccountType = 'C'   
      AND AccountID = (SELECT ProductID From t_Account WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID ) -- @ProductID   
      AND (TrxType = @TrxType OR TrxType = 'B') AND AuthStatus = '') <= 0   
      BEGIN   
  
         SELECT '13' AS RetStatus, @ClientID AS ClientID, 0.00 as ClearBalance, 0.00 as Effects, 0.00 as ShadowBalance, 0.00 as Limit, '' as AccountName, '' as ProductID,   
         '' as ProductName, '' as CurrencyID, '' as CurrencyName, '' as Reminder, 0.00 as BlockedAmount, 0.00 as TotalBalance, 0 as CanGoCrDr, '' as GLID,   
         0.00 as MeanRate, '' as Status, 0.00 as AccountClearingDays, '' as AuthorizationStatus, 0 as EveryTrxGoesInSupervision, 0.00 AS ProductMinBalance, 0 as IsUtilityAccount --User Account Rights Not Define   
         RETURN(0)   
      END   
  
   END   
     
   IF (Select Count(AccountID) From t_AccountBalance WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID) > 0   
   BEGIN   
      ------------------------------------AUTHORIZATION-------------------   
  SELECT @Status=IsNull([Auth],'0'), @AccountName=Name, @ProductID=ProductID, @AuthorizationStatus = IsNull([AuthStatus],'')   
      FROM t_Account   
      WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID  
  
      IF IsNull(@Status,'0') <> '0'   
      BEGIN   
         IF @Status <> '8' AND @Status <> '9' AND @Status <> '11'   
         --SET @AuthorizationStatus = IsNull([Authorization],'0')   
         --ELSE   
         BEGIN   
            SELECT 'A' + @Status AS RetStatus, @ClientID AS ClientID, 0.00 as ClearBalance, 0.00 as Effects, 0.00 as ShadowBalance, 0.00 as Limit, '' as AccountName, '' as ProductID,   
            '' as ProductName, '' as CurrencyID, '' as CurrencyName, '' as Reminder, 0.00 as BlockedAmount, 0.00 as TotalBalance, 0 as CanGoCrDr, '' as GLID,   
            0.00 as MeanRate, '' as Status, 0.00 as AccountClearingDays, '' as AuthorizationStatus, 0 as EveryTrxGoesInSupervision, 0.00 AS ProductMinBalance, 0 as IsUtilityAccount --Ret. Status   
            RETURN(1)   
         END   
      END   
      --------------------------------------------------------------------------------   
   
      SELECT @Status=IsNull(Status,''), @AccountName=Name, @ProductID=ProductID   
      FROM t_Account   
      WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID   
  
      IF IsNull(@Status,'') <> ''   
      BEGIN   
         IF @TrxType = 'C' AND @Status = 'T'   
         BEGIN   
            SELECT @Status='T'   
         END   
         ELSE IF @Status = 'X'   
         BEGIN   
            SELECT @Status='X'   
         END   
         ELSE IF @Status = 'D'   
         BEGIN   
            SELECT @Status='D'   
         END   
         ELSE IF @ModuleType = 'ID'   
         BEGIN   
            SELECT @Status = @Status   
         END    
         ELSE   
         BEGIN   
            SELECT @Status AS RetStatus, @ClientID AS ClientID, 0.00 as ClearBalance, 0.00 as Effects, 0.00 as ShadowBalance, 0.00 as Limit, '' as AccountName, '' as ProductID,   
            '' as ProductName, '' as CurrencyID, '' as CurrencyName, '' as Reminder, 0.00 as BlockedAmount, 0.00 as TotalBalance, 0 as CanGoCrDr, '' as GLID,   
            0.00 as MeanRate, '' as Status, 0.00 as AccountClearingDays, '' as AuthorizationStatus, 0 as EveryTrxGoesInSupervision, 0.00 AS ProductMinBalance, 0 as IsUtilityAccount --Ret. Status   
            RETURN(1)   
         END   
      END  
  
      IF @AllProductsAllowInFO = 0   
      BEGIN   
         IF (SELECT Count(*) FROM t_FOAllowProducts WHERE OurBranchID = @OurBranchID AND ProductID = @ProductID)=0   
         BEGIN   
            SELECT '10' AS RetStatus, @ClientID AS ClientID, 0.00 as ClearBalance, 0.00 as Effects, 0.00 as ShadowBalance, 0.00 as Limit, '' as AccountName, '' as ProductID,   
            '' as ProductName, '' as CurrencyID, '' as CurrencyName, '' as Reminder, 0.00 as BlockedAmount, 0.00 as TotalBalance, 0 as CanGoCrDr, '' as GLID,   
            0.00 as MeanRate, '' as Status, 0.00 as AccountClearingDays, '' as AuthorizationStatus, 0 as EveryTrxGoesInSupervision, 0.00 AS ProductMinBalance, 0 as IsUtilityAccount --Ret. Status   
            RETURN(1)   
         END   
      END   
  
      --exec @Limit = fnc_GetLimit @OurBranchID, @AccountID   
        
      SELECT @Reminder=Reminder, @ClearBalance=ClearBalance,   
   @Effects=Effects, @ShadowBalance=ShadowBalance, @Limit=Limit,  
      @BlockedAmount = CASE IsFreezed WHEN 1 THEN FreezeAmount WHEN 0 THEN 0 END   
      FROM t_AccountBalance   
      WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID   
         
      SELECT @CurrencyID=Upper(CurrencyID) ,@ProductName=Description,  
   @IsFreezed=IsFreezed, @ProductMinBalance = ISNULL(MinBalance,0),   
      @AllowCrDr = Case @TrxType WHEN 'C' THEN AllowCredit WHEN 'D' THEN AllowDebit END,   
      @CanGoCrDr = Case @TrxType WHEN 'C' THEN CanGoInCredit WHEN 'D' THEN CanGoInDebit END,   
      @IsUtilityAccount=IsUtilityAccount   
      FROM t_Products   
      WHERE OurBranchID = @OurBranchID AND ProductID = @ProductID  
  
      IF @ModuleType = 'C'   
      BEGIN   
         IF @TrxType = 'L'   
         BEGIN   
            IF @LocalCurrency = @CurrencyID   
            BEGIN   
               SELECT '6' AS RetStatus, @ClientID AS ClientID, 0.00 as ClearBalance, 0.00 as Effects, 0.00 as ShadowBalance, 0.00 as Limit, '' as AccountName, '' as ProductID,   
               '' as ProductName, '' as CurrencyID, '' as CurrencyName, '' as Reminder, 0.00 as BlockedAmount, 0.00 as TotalBalance, 0 as CanGoCrDr, '' as GLID,   
               0.00 as MeanRate, '' as Status, 0.00 as AccountClearingDays, '' as AuthorizationStatus, 0 as EveryTrxGoesInSupervision, 0.00 AS ProductMinBalance, 0 as IsUtilityAccount --Local Debit Not Allowed In Loacl Currency   
               RETURN(0)   
            END  
  
            SELECT @GLID=IsNull(GLID,'') FROM t_CashierMultiCurrency   
            WHERE OurBranchID = @OurBranchID AND OperatorID = @OperatorID AND CurrencyID = @LocalCurrency   
         END   
         ELSE   
         BEGIN   
            SELECT @GLID=IsNull(GLID,'') FROM t_CashierMultiCurrency   
            WHERE OurBranchID = @OurBranchID AND OperatorID = @OperatorID AND CurrencyID = @CurrencyID   
         END   
  
         IF IsNull(@GLID,'')=''   
         BEGIN   
            SELECT '5' AS RetStatus, @ClientID AS ClientID, 0.00 as ClearBalance, 0.00 as Effects, 0.00 as ShadowBalance, 0.00 as Limit, '' as AccountName, '' as ProductID,   
            '' as ProductName, '' as CurrencyID, '' as CurrencyName, '' as Reminder, 0.00 as BlockedAmount, 0.00 as TotalBalance, 0 as CanGoCrDr, '' as GLID,   
            0.00 as MeanRate, '' as Status, 0.00 as AccountClearingDays, '' as AuthorizationStatus, 0 as EveryTrxGoesInSupervision, 0.00 AS ProductMinBalance, 0 as IsUtilityAccount --Cashier Not Autorize For this Type of Trx. or Till A/c Not Define   
            RETURN(1)   
         END  
      END  
  
      IF @IsFreezed = 1 AND @ModuleType <> 'ID'   
      BEGIN   
         SELECT '2' AS RetStatus, @ClientID AS ClientID, 0.00 as ClearBalance, 0.00 as Effects, 0.00 as ShadowBalance, 0.00 as Limit, '' as AccountName, '' as ProductID,   
         '' as ProductName, '' as CurrencyID, '' as CurrencyName, '' as Reminder, 0.00 as BlockedAmount, 0.00 as TotalBalance, 0 as CanGoCrDr, '' as GLID,   
         0.00 as MeanRate, '' as Status, 0.00 as AccountClearingDays, '' as AuthorizationStatus, 0 as EveryTrxGoesInSupervision, 0.00 AS ProductMinBalance, 0 as IsUtilityAccount -- 2 - Product Is Freezed   
         RETURN(0)   
      END   
      ELSE IF @IsFreezed = 1 AND @ModuleType = 'ID'   
         SELECT @Status='I'   
  
      IF (Select Count(AccountID) From t_Account WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID) > 0   
      BEGIN   
         SELECT @AllowCrDr = Case @TrxType WHEN 'C' THEN AllowCreditTransaction WHEN 'D' THEN AllowDebitTransaction END,   
   @AccountClearingDays = IsNull(ClearingTransactionDays,0),   
   @EveryTrxGoesInSupervision = Case @TrxType WHEN 'C' THEN CreditNeedsSupervision WHEN 'D' THEN DebitNeedsSupervision END   
         FROM t_Account   
         WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID   
  
         IF @AllowCrDr = 1 AND UPPER(@OperatorID) != 'COMPUTER'
         BEGIN   
            SELECT '3' AS RetStatus, @ClientID AS ClientID, 0.00 as ClearBalance, 0.00 as Effects, 0.00 as ShadowBalance, 0.00 as Limit, '' as AccountName, '' as ProductID,   
            '' as ProductName, '' as CurrencyID, '' as CurrencyName, '' as Reminder, 0.00 as BlockedAmount, 0.00 as TotalBalance, 0 as CanGoCrDr, '' as GLID,   
            0.00 as MeanRate, '' as Status, 0.00 as AccountClearingDays, '' as AuthorizationStatus, 0 as EveryTrxGoesInSupervision, 0.00 AS ProductMinBalance, 0 as IsUtilityAccount -- 3 - In Account Special Condition Dr/Cr Transaction Not Allowed   
            RETURN(0)   
         END   
      END   
   ELSE   
      BEGIN   
         IF @AllowCrDr = 0   
         BEGIN   
            SELECT '4' AS RetStatus, @ClientID AS ClientID, 0.00 as ClearBalance, 0.00 as Effects, 0.00 as ShadowBalance, 0.00 as Limit, '' as AccountName, '' as ProductID,   
            '' as ProductName, '' as CurrencyID, '' as CurrencyName, '' as Reminder, 0.00 as BlockedAmount, 0.00 as TotalBalance, 0 as CanGoCrDr, '' as GLID,   
            0.00 as MeanRate, '' as Status, 0.00 as AccountClearingDays, '' as AuthorizationStatus, 0 as EveryTrxGoesInSupervision, 0.00 AS ProductMinBalance, 0 as IsUtilityAccount -- 4 - In Product Condition Dr/Cr Trx Not Allowed   
            RETURN(0)   
         END   
      END   
  
      SELECT @CurrencyName = Description, @MeanRate = MeanRate FROM t_Currencies   
      WHERE OurBranchID=@OurBranchID AND CurrencyID=@CurrencyID   
  
      SELECT '1' as RetStatus, @ClientID AS ClientID , @ClearBalance as ClearBalance, @Effects as Effects,   
      @ShadowBalance as ShadowBalance, @Limit as Limit,@AccountName as AccountName,   
      @ProductID as ProductID, @ProductName as ProductName, @CurrencyID as CurrencyID,   
      @CurrencyName as CurrencyName, @Reminder as Reminder, @BlockedAmount as BlockedAmount,   
      @ClearBalance + @Effects + @Limit as TotalBalance, @CanGoCrDr as CanGoCrDr,   
      @GLID as GLID, @MeanRate as MeanRate, @Status as Status, @AccountClearingDays as AccountClearingDays,   
      @AuthorizationStatus as AuthorizationStatus, @EveryTrxGoesInSupervision as EveryTrxGoesInSupervision,   
      @ProductMinBalance AS ProductMinBalance, @IsUtilityAccount as IsUtilityAccount   
   END   
   ELSE   
   BEGIN   
      SELECT '0' AS RetStatus, @ClientID AS ClientID, 0.00 as ClearBalance, 0.00 as Effects, 0.00 as ShadowBalance, 0.00 as Limit, '' as AccountName, '' as ProductID,   
      '' as ProductName, '' as CurrencyID, '' as CurrencyName, '' as Reminder, 0.00 as BlockedAmount, 0.00 as TotalBalance, 0 as CanGoCrDr, '' as GLID,   
      0.00 as MeanRate, '' as Status, 0.00 as AccountClearingDays, '' as AuthorizationStatus, 0 as EveryTrxGoesInSupervision, 0.00 AS ProductMinBalance, 0 as IsUtilityAccount --Account Does Not Exists   
      RETURN(0)   
   END   
  
SET QUOTED_IDENTIFIER OFF