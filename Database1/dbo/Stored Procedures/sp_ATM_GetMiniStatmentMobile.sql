﻿CREATE PROCEDURE [dbo].[sp_ATM_GetMiniStatmentMobile]
(
	@OurBranchID 	varchar(30),
	@AccountID 	varchar(30)
)

AS

	Declare @Count int
	Declare @Count2 int
	Declare @DRCR varchar(2)
	Declare @a  nvarchar(4000)
	Declare @RetVal  int

	SELECT @Count = Count(*) from v_ATM_GetTodayTransactions
	WHERE (OurBranchID = @OurBranchID) AND (AccountID = @AccountID)

	if @Count is null
	SELECT @Count = 1

	IF @Count =0
		BEGIN

		Select  @a = '  SELECT TOP 1  ValueDate, Amount,  case trxtype when ''D'' then ''-'' when ''C'' then '' '' end as DRCR,'
		Select @a= @a + '  CASE DocumentType ' 
		Select @a= @a + '     WHEN ''C'' THEN '
		Select @a= @a + ' 	Case TrxType '
		Select @a= @a + ' 		When ''D'' THEN ''CASH DEBIT'
		Select @a= @a + ' 		When ''C'' THEN ''CASH CREDIT' 
		Select @a= @a + ' 	END'
		Select @a= @a + '     WHEN ''T'' THEN '
		Select @a= @a + ' 	Case TrxType '
		Select @a= @a + ' 		When ''D''THEN ''DEBIT TRANSFER'
		Select @a= @a + ' 		When ''C''THEN ''CREDIT TRANSFER'
		Select @a= @a + ' 	END '
		Select @a= @a + '     WHEN '''' THEN '
		Select @a= @a + ' 	Case TrxType '
		Select @a= @a + ' 		When ''D''THEN ''DEBIT'
		Select @a= @a + ' 		When ''C''THEN ''CREDIT'
		Select @a= @a + ' 	END '
		Select @a= @a + '     WHEN ''ID'' THEN ''INWARD CLEARING'
		Select @a= @a + '     WHEN ''OC'' THEN ''OUTWARD CLEARING'
		Select @a= @a + '      END AS TrxType'
		Select @a= @a + '  FROM t_Transactions WHERE OurBranchID = ' + '''' + @OurBranchID + '''' + ' AND AccountID = ' + '''' + @AccountID + '''' + ' ORDER BY ValueDate desc'

		END
	ELSE
		BEGIN
			SELECT TOP 1 ValueDate, Amount,TrxType as DRCR, DocumentType as TrxType from v_ATM_GetTodayTransactions
			WHERE (OurBranchID = @OurBranchID) AND (AccountID = @AccountID)
		END

	EXEC (@a)


	IF @RetVal <> 0  
		BEGIN
			RAISERROR ('Error Occured',16,1) WITH SETERROR
			RETURN (1)
		END	

	ELSE
		BEGIN
			RETURN (0)
		END