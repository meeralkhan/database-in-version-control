﻿	CREATE PROCEDURE [dbo].[spc_GetFDReports]  
	(        
	 @OurBranchID varchar(30)='',        
	 @FromDate datetime,        
	 @ToDate datetime,        
	 @ReportType varchar(50)        
	)        
	AS        
	set nocount on            
			
	BEGIN TRY        
			
	 IF (@ReportType = 'Issuance')        
	  BEGIN        
	   SELECT 'Ok' as RetStat, * from vc_GetDeposits        
	   WHERE OurBranchID = @OurBranchID AND convert(varchar(10), StartDate, 120) BETWEEN @FromDate AND @ToDate        
	   ORDER BY Period, StartDate        
	  END        
	 ELSE IF (@ReportType = 'Maturity')        
	  BEGIN        
	   SELECT 'Ok' as RetStat, * from vc_GetDeposits        
	   WHERE OurBranchID = @OurBranchID AND convert(varchar(10), MaturityDate, 120) BETWEEN @FromDate AND @ToDate        
	   ORDER BY Period, MaturityDate        
	  END        
	 ELSE IF (@ReportType = 'OutStanding')        
	  BEGIN        
	   SELECT 'Ok' as RetStat, * from vc_GetDeposits        
	   WHERE OurBranchID = @OurBranchID AND convert(varchar(10), StartDate, 120) <= @ToDate AND ISNULL(IsClosed,0) = 0        
	   AND (ISNULL(CloseDate, '1900-01-01') = '1900-01-01' OR CloseDate IS NULL OR CloseDate > @ToDate)        
	   ORDER BY Period, StartDate          
	  END        
	 ELSE IF (@ReportType = 'Closed')        
	  BEGIN        
	   SELECT 'Ok' as RetStat, * from vc_GetDeposits        
	   WHERE OurBranchID = @OurBranchID AND convert(varchar(10), CloseDate, 120) BETWEEN @FromDate AND @ToDate       
	   AND ISNULL(IsClosed,0) = 1 AND CloseDate IS NOT NULL      
	   ORDER BY Period, StartDate          
	  END        
	 ELSE IF (@ReportType = 'Principal Payment')    
	  BEGIN        
	   SELECT 'Ok' as RetStat, * from vc_GetDeposits        
	   WHERE OurBranchID = @OurBranchID AND convert(varchar(10), CloseDate, 120) BETWEEN @FromDate AND @ToDate       
	   AND ISNULL(IsClosed,0) = 1 AND CloseDate IS NOT NULL      
	   --AND IsPrincipalPaid = 1     
	   ORDER BY Period, StartDate          
	  END        
	 ELSE        
	 BEGIN        
	   raiserror('Invalid Report Type', 15, 1);        
	 END        
			
	END TRY        
	BEGIN CATCH        
	  Select 'Error' as RetStat, Error_Message() as RetMsg        
	END CATCH