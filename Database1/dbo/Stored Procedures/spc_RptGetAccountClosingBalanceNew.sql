﻿CREATE PROCEDURE [dbo].[spc_RptGetAccountClosingBalanceNew]    
 (            
  @OurBranchID varchar(4),             
  @AccountID varchar(30),            
  @Date datetime,            
  @GLOrAccount char(1)='C',            
  @DateOrValue char(1)='D',            
  @ClearOrTotal char(1)='T',            
  @@ClosingBalance numeric(18,2) = 0 OUTPUT            
 )            
AS            
          
SET NOCOUNT ON          
            
 DECLARE             
  @frmDate datetime,            
  @LocalCurrency varchar(4),            
  @TempDate datetime,            
  @BankID varchar(6),            
  @ProductID varchar(6),            
  @CurrencyID varchar(4),            
  @CreditAmount numeric(18,2),            
  @DebitAmount numeric(18,2)            
            
 SELECT @@ClosingBalance = 0            
 SELECT @LocalCurrency=LocalCurrency, @BankID=OurBankID FROM t_GlobalVariables where OurBranchID = @OurBranchID            
            
 select @frmDate=convert(varchar,month(@Date)) + '/01/' + convert(varchar,year(@Date))             
            
 IF @GLOrAccount = 'C'            
 BEGIN            
  IF @ClearOrTotal = 'C'            
   SELECT @@ClosingBalance = ClearBalance + Effects FROM t_AccountMonthBalances WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID and Month(Month)= Month(@Date) and year(month) = year(@Date)            
  ELSE            
   SELECT @@ClosingBalance = ClearBalance + Effects FROM t_AccountMonthBalances WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID and Month(Month)= Month(@Date) and year(month) = year(@Date)            
            
  SELECT @ProductID = ProductID FROM t_Account WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID            
  SELECT @CurrencyID = CurrencyID FROM t_Products WHERE OurBranchID = @OurBranchID AND ProductID = @ProductID            
            
  IF @CurrencyID = @LocalCurrency            
  BEGIN            
   SELECT @CreditAmount = isnull (sum(Amount),0 )  FROM t_Transactions             
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND TrxType = 'C'             
   AND convert(varchar(10), wDate, 120) <=  @Date and convert(varchar(10), wDate, 120) >= @frmDate            
              
   SELECT @DebitAmount = isnull (sum(Amount),0 )  FROM t_Transactions             
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND TrxType = 'D'             
   AND convert(varchar(10), wDate, 120) <=  @Date and convert(varchar(10), wDate, 120) >= @frmDate            
  END            
  ELSE            
  BEGIN            
   SELECT @CreditAmount = isnull (sum(ForeignAmount),0 )  FROM t_Transactions             
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND TrxType = 'C'             
   AND convert(varchar(10), wDate, 120) <=  @Date and convert(varchar(10), wDate, 120) >= @frmDate            
              
   SELECT @DebitAmount = isnull (sum(ForeignAmount),0 )  FROM t_Transactions             
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND TrxType = 'D'             
   AND convert(varchar(10), wDate, 120) <=  @Date and convert(varchar(10), wDate, 120) >= @frmDate            
  END            
            
  SELECT @@ClosingBalance = @@ClosingBalance + @CreditAmount - @DebitAmount            
 END            
            
 ELSE            
            
 BEGIN            
 declare @CurrentYear datetime            
 select @CurrentYear = WorkingDate FROM t_Last where OurBranchID = @OurBranchID       
 IF year(@Date) < year(@CurrentYear)            
  BEGIN             
            
  SELECT @CurrencyID = CurrencyID FROM t_GL WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID            
            
  IF @CurrencyID = @LocalCurrency            
   SELECT @@ClosingBalance = OpeningBalance FROM t_GL WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID            
  ELSE            
   SELECT @@ClosingBalance = ForeignOpeningBalance FROM t_GL WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID            
              
  IF @CurrencyID = @LocalCurrency            
  BEGIN            
   SELECT @CreditAmount = isnull (sum(Amount),0 )  FROM t_GLTransactions             
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND IsCredit = 1             
   AND convert(varchar(10), [Date], 120) <= @Date            
      
              
   SELECT @DebitAmount = isnull (sum(Amount),0 )  FROM t_GLTransactions             
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND IsCredit = 0             
   AND convert(varchar(10), [Date], 120) <= @Date            
  END            
  ELSE            
  BEGIN            
   SELECT @CreditAmount = isnull (sum(ForeignAmount),0 )  FROM t_GLTransactions             
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND IsCredit = 1             
   AND convert(varchar(10), [Date], 120) <= @Date            
              
   SELECT @DebitAmount = isnull (sum(ForeignAmount),0 )  FROM t_GLTransactions             
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND IsCredit = 0             
   AND convert(varchar(10), [Date], 120) <= @Date            
  END            
            
  SELECT @@ClosingBalance = @@ClosingBalance + @CreditAmount - @DebitAmount            
            
  END            
 ELSE            
  BEGIN            
            
  SELECT @CurrencyID = CurrencyID FROM t_GL WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID            
            
  IF @CurrencyID = @LocalCurrency            
   SELECT @@ClosingBalance = OpeningBalance FROM t_GL WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID            
  ELSE            
   SELECT @@ClosingBalance = ForeignOpeningBalance FROM t_GL WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID            
            
  IF @CurrencyID = @LocalCurrency            
  BEGIN          
    
   SELECT @CreditAmount = isnull (sum(Amount),0 )  FROM t_GLTransactions             
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND IsCredit = 1             
   AND convert(varchar(10), [Date], 120) <= @Date            
              
   SELECT @DebitAmount = isnull (sum(Amount),0 )  FROM t_GLTransactions             
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND IsCredit = 0             
   AND convert(varchar(10), [Date], 120) <= @Date            
  END            
  ELSE            
  BEGIN            
   SELECT @CreditAmount = isnull (sum(ForeignAmount),0 )  FROM t_GLTransactions             
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND IsCredit = 1             
   AND convert(varchar(10), [Date], 120) <= @Date            
              
   SELECT @DebitAmount = isnull (sum(ForeignAmount),0 )  FROM t_GLTransactions             
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND IsCredit = 0             
   AND convert(varchar(10), [Date], 120) <= @Date            
  END            
            
  SELECT @@ClosingBalance = @@ClosingBalance + @CreditAmount - @DebitAmount            
            
  END            
 END            
             
 SELECT @@ClosingBalance AS ClosingBalance