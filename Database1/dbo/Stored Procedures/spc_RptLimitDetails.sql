﻿CREATE PROCEDURE [dbo].[spc_RptLimitDetails] (  
 @OurBranchID varchar(30)=''      
)      
AS        
set nocount on      
        
 SELECT t_Adv_Account_LimitMaintain.OurBranchID, t_Account.ClientID, t_Customer.Name AS ClientName,   
 t_Adv_Account_LimitMaintain.AccountID, t_Account.Name AS AccountName, t_Adv_Account_LimitMaintain.SanctionDate,   
 t_Adv_Account_LimitMaintain.AccountLimit as LimitAmount, t_Adv_Account_LimitMaintain.DrawingPower,   
 t_Adv_Account_LimitMaintain.ExpDate, t_Rate001.ChangeDate, t_Rate001.Limit1, t_Rate001.Rate1, t_Rate001.Limit2,   
 t_Rate001.Rate2, t_Rate001.Limit3, t_Rate001.Rate3, t_Adv_Account_Security.ValueOfSecurity,   
 t_Adv_Account_Security.Margin, t_Securities.Description AS SecurityDescription,   
 t_SecurityTypes.[Description] AS securityType, t_Adv_Account_LimitMaintain.IsBlocked,   
 t_Adv_Account_LimitMaintain.IsCancelled, t_Products.CurrencyID,   
 t_AccountBalance.ClearBalance + t_AccountBalance.Effects AS ClearBalance, t_AccountBalance.Limit,   
 t_Account.AccountID AS LimitAccountID, vc_RptGetDailyChanges.DailyChange, vc_RptGetDailyChanges.DailyLocalChange      
 /*      
 v_RptGetDailyChanges.PreviousBalance,         
 v_RptGetDailyChanges.PreviousLocalBalance        
 */      
 FROM   
 vc_RptGetDailyChanges   
 INNER JOIN t_Account ON vc_RptGetDailyChanges.OurBranchID = t_Account.OurBranchID   
 AND vc_RptGetDailyChanges.AccountID = t_Account.AccountID   
 LEFT OUTER JOIN t_AccountBalance ON t_Account.OurBranchID = t_AccountBalance.OurBranchID   
 AND t_Account.AccountID = t_AccountBalance.AccountID   
 LEFT OUTER JOIN t_Products ON t_Account.OurBranchID = t_Products.OurBranchID   
 AND t_Account.ProductID = t_Products.ProductID   
 LEFT OUTER JOIN t_Customer ON t_Account.OurBranchID = t_Customer.OurBranchID   
 AND t_Account.ClientID = t_Customer.ClientID   
 RIGHT OUTER JOIN t_Adv_Account_LimitMaintain ON t_Account.OurBranchID = t_Adv_Account_LimitMaintain.OurBranchID   
 AND t_Account.AccountID = t_Adv_Account_LimitMaintain.AccountID   
 LEFT OUTER JOIN t_Securities RIGHT OUTER JOIN        
    t_Adv_Account_Security ON   
    t_Securities.OurBranchID = t_Adv_Account_Security.OurBranchID AND         
    t_Securities.SecurityID = t_Adv_Account_Security.SecurityID ON         
    t_Adv_Account_LimitMaintain.OurBranchID = t_Adv_Account_Security.OurBranchID AND        
    t_Adv_Account_LimitMaintain.AccountID = t_Adv_Account_Security.AccountID AND         
    t_Adv_Account_Security.ValueOfSecurity =        
    ( SELECT MAX(valueofsecurity)        
      FROM t_Adv_Account_Security        
      WHERE t_Adv_Account_Security.OurBranchID = t_Adv_Account_LimitMaintain.OurBranchID   
      AND t_Adv_Account_Security.accountid = t_Adv_Account_LimitMaintain.AccountID  
    )        
 LEFT OUTER JOIN t_Rate001 ON t_Adv_Account_LimitMaintain.OurBranchID = t_Rate001.OurBranchID   
 AND t_Adv_Account_LimitMaintain.AccountID = t_Rate001.AccountID AND         
 t_Rate001.ChangeDate =        
    ( SELECT MAX(changedate)        
      FROM t_rate001        
      WHERE t_rate001.OurBranchID = t_Adv_Account_LimitMaintain.OurBranchID   
      AND t_rate001.accountid = t_Adv_Account_LimitMaintain.AccountID  
    ) AND t_AccountBalance.Limit <> 0        
 INNER JOIN t_SecurityTypes on t_Securities.OurBranchID = t_SecurityTypes.OurBranchID   
 AND t_Securities.SecurityTypeId = t_SecurityTypes.SecurityTypeId  
 WHERE (t_AccountBalance.Limit <> 0)        
        
        
/** Farhan Starts 19-01-2016 **/      
      
/*       
      
UNION        
        
SELECT t_Customer.ClientID, t_Customer.Name AS ClientName,         
    t_Account.AccountID, t_Account.Name AS AccountName,         
    t_LCLimits.SanctionDate, t_LCLimits.LimitAmount,         
    t_LCLimits.DrawingPower, t_LCLimits.ExpiryDate,         
    t_Rate001.ChangeDate, t_Rate001.Limit1, t_Rate001.Rate1,         
    t_Rate001.Limit2, t_Rate001.Rate2, t_Rate001.Limit3,         
    t_Rate001.Rate3, t_Adv_Account_Security.ValueOfSecurity,         
    t_Adv_Account_Security.Margin,         
    t_Securities.Description AS SecurityDescription,         
    t_Securities.Type AS SecurityType, t_LCLimits.IsBlocked,         
    t_LCLimits.IsDeleted, t_Products.CurrencyID,         
    t_AccountBalance.ClearBalance + t_AccountBalance.Effects AS ClearBalance,        
     t_Account.OurBranchID, t_AccountBalance.Limit,         
    t_Limits.LimitAccountID, v_RptGetDailyChanges.DailyChange,         
    v_RptGetDailyChanges.DailyLocalChange,         
    v_RptGetDailyChanges.PreviousBalance,         
    v_RptGetDailyChanges.PreviousLocalBalance        
FROM t_Adv_Account_Security INNER JOIN        
    t_Securities ON         
    t_Adv_Account_Security.SecurityID = t_Securities.SecurityID INNER JOIN        
    t_LCLimits ON         
    t_Adv_Account_Security.AccountID = t_LCLimits.AccountID LEFT OUTER        
     JOIN        
    v_RptGetDailyChanges INNER JOIN        
    t_AccountBalance INNER JOIN        
    t_Limits ON         
    t_AccountBalance.OurBranchID = t_Limits.OurBranchID AND         
    t_AccountBalance.AccountID = t_Limits.AccountID INNER JOIN        
    t_Account ON         
    t_AccountBalance.OurBranchID = t_Account.OurBranchID AND         
    t_AccountBalance.AccountID = t_Account.AccountID INNER JOIN        
    t_Customer ON         
    t_Account.ClientID = t_Customer.ClientID INNER JOIN        
    t_Products ON         
    t_Account.OurBranchID = t_Products.OurBranchID AND         
    t_Account.ProductID = t_Products.ProductID ON         
    v_RptGetDailyChanges.AccountID = t_Account.AccountID INNER JOIN        
    t_Rate001 ON t_Limits.AccountID = t_Rate001.AccountID ON         
    t_LCLimits.OurBranchID = t_Limits.OurBranchID AND         
    t_LCLimits.AccountID = t_Limits.LimitAccountID        
WHERE (t_AccountBalance.Limit <> 0) AND         
    (t_AccountBalance.ClearBalance + t_AccountBalance.Effects < 0)        
     AND (t_Account.AccountID LIKE '01%')        
        
UNION        
        
SELECT t_Customer.ClientID, t_Customer.Name AS ClientName,         
    t_Account.AccountID, t_Account.Name AS AccountName,         
    t_Adv_Account_LimitMaintain.SanctionDate, t_Adv_Account_LimitMaintain.LimitAmount,         
    t_Adv_Account_LimitMaintain.DrawingPower, t_Adv_Account_LimitMaintain.ExpiryDate,         
    t_Rate001.ChangeDate, t_Rate001.Limit1, t_Rate001.Rate1,         
    t_Rate001.Limit2, t_Rate001.Rate2, t_Rate001.Limit3,         
    t_Rate001.Rate3, t_Adv_Account_Security.ValueOfSecurity,         
    t_Adv_Account_Security.Margin,         
    t_Securities.Description AS SecurityDescription,         
    t_Securities.Type AS SecurityType, t_Adv_Account_LimitMaintain.IsBlocked,         
    t_Adv_Account_LimitMaintain.IsDeleted, t_Products.CurrencyID,         
    t_AccountBalance.ClearBalance + t_AccountBalance.Effects AS ClearBalance,        
     t_Account.OurBranchID, t_AccountBalance.Limit,         
    t_Limits.LimitAccountID, v_RptGetDailyChanges.DailyChange,         
    v_RptGetDailyChanges.DailyLocalChange,         
    v_RptGetDailyChanges.PreviousBalance,         
    v_RptGetDailyChanges.PreviousLocalBalance        
FROM v_RptGetDailyChanges INNER JOIN        
    t_AccountBalance INNER JOIN        
    t_Limits ON         
    t_AccountBalance.OurBranchID = t_Limits.OurBranchID AND         
    t_AccountBalance.AccountID = t_Limits.AccountID INNER JOIN        
    t_Account ON         
    t_AccountBalance.OurBranchID = t_Account.OurBranchID AND         
    t_AccountBalance.AccountID = t_Account.AccountID INNER JOIN        
    t_Customer ON         
    t_Account.ClientID = t_Customer.ClientID INNER JOIN        
    t_Adv_Account_LimitMaintain ON         
    t_Limits.OurBranchID = t_Adv_Account_LimitMaintain.OurBranchID AND         
    t_Limits.LimitAccountID = t_Adv_Account_LimitMaintain.AccountID INNER JOIN        
    t_Products ON         
    t_Account.OurBranchID = t_Products.OurBranchID AND         
    t_Account.AccountID = t_Products.ProductID ON         
    v_RptGetDailyChanges.AccountID = t_Account.AccountID INNER JOIN        
    t_Rate001 ON         
    t_Limits.AccountID = t_Rate001.AccountID INNER JOIN        
    t_Securities INNER JOIN        
    t_Adv_Account_Security ON         
    t_Securities.SecurityID = t_Adv_Account_Security.SecurityID ON         
    t_Adv_Account_LimitMaintain.AccountID = t_Adv_Account_Security.AccountID        
WHERE (t_AccountBalance.Limit <> 0) AND         
    (t_AccountBalance.ClearBalance + t_AccountBalance.Effects < 0)        
        
UNION        
        
SELECT t_Customer.ClientID, t_Customer.Name AS ClientName,         
    t_Account.AccountID, t_Account.Name AS AccountName,         
    t_TermLoans.SanctionDate,         
t_TermLoans.Limit AS LimitAmount,         
    t_TermLoans.DrawingPower, t_TermLoans.ExpiryDate,         
    t_Rate001.ChangeDate, t_Rate001.Limit1, t_Rate001.Rate1,         
    t_Rate001.Limit2, t_Rate001.Rate2, t_Rate001.Limit3,         
    t_Rate001.Rate3, t_Adv_Account_Security.ValueOfSecurity,         
    t_Adv_Account_Security.Margin,         
    t_Securities.Description AS SecurityDescription,         
    t_Securities.Type AS SecurityType, t_TermLoans.IsBlocked,         
    t_TermLoans.IsDeleted, t_Products.CurrencyID,         
    t_AccountBalance.ClearBalance + t_AccountBalance.Effects AS ClearBalance,        
     t_Account.OurBranchID, t_AccountBalance.Limit,         
    t_Limits.LimitAccountID, v_RptGetDailyChanges.DailyChange,         
    v_RptGetDailyChanges.DailyLocalChange,         
    v_RptGetDailyChanges.PreviousBalance,         
    v_RptGetDailyChanges.PreviousLocalBalance        
FROM t_Securities INNER JOIN        
    t_Adv_Account_Security ON         
    t_Securities.SecurityID = t_Adv_Account_Security.SecurityID INNER JOIN        
    t_AccountBalance INNER JOIN        
    t_Limits ON         
    t_AccountBalance.OurBranchID = t_Limits.OurBranchID AND         
    t_AccountBalance.AccountID = t_Limits.AccountID INNER JOIN        
    t_Account ON         
    t_AccountBalance.OurBranchID = t_Account.OurBranchID AND         
    t_AccountBalance.AccountID = t_Account.AccountID INNER JOIN        
    t_Customer ON         
    t_Account.ClientID = t_Customer.ClientID INNER JOIN        
    t_TermLoans ON         
    t_Limits.OurBranchID = t_TermLoans.OurBranchID AND         
    t_Limits.LimitAccountID = t_TermLoans.AccountID INNER JOIN        
    v_RptGetDailyChanges ON         
    t_Account.AccountID = v_RptGetDailyChanges.AccountID ON         
    t_Adv_Account_Security.AccountID = t_TermLoans.AccountID INNER JOIN        
    t_Rate001 ON         
    t_Limits.AccountID = t_Rate001.AccountID INNER JOIN        
    t_Products ON         
    t_Account.OurBranchID = t_Products.OurBranchID AND         
    t_Account.ProductID = t_Products.ProductID        
WHERE (t_AccountBalance.Limit <> 0) AND         
    (t_AccountBalance.ClearBalance + t_AccountBalance.Effects < 0)        
      
 /** Farhan Starts 19-01-2016 **/      
      
*/      
      
/*        
 SELECT * FROM v_RptLimitDetails WHERE OurBranchID = @OurBranchID        
 UNION        
 SELECT * FROM v_RptGetSODLC WHERE OurBranchID = @OurBranchID        
 UNION        
 SELECT * FROM v_RptGetSODOD WHERE OurBranchID = @OurBranchID        
 UNION        
 SELECT * FROM v_RptGetSODTermLoan WHERE OurBranchID = @OurBranchID        
        
*/