﻿create    procedure [dbo].[spc_Get321DailyBalance]           
(           
   @OurBranchID nvarchar(30),           
   @AccountID nvarchar(30),           
   @ZXAccountID nvarchar(30),           
   @IntMonth date,           
   @EndDate date           
)           
           
AS           
           
SET NOCOUNT ON           
           
declare @OpBalance decimal(21,3)           
declare @ZOpBalance decimal(21,3)           
declare @ZBalance decimal(21,3)           
declare @DiffBalance decimal(21,3)           
declare @Balance decimal(21,3)           
declare @tDebit decimal(21,3)           
declare @CcyID nvarchar(30)           
declare @IntTable table (           
   SNo int,           
   Dated date,           
   OpBalance decimal(21,3),           
   ZOpBalance decimal(21,3),           
   DiffBalance decimal(21,3)           
)           
declare @IntFinalTable table (           
   vDate date,           
   Balance decimal(21,3),           
   [Days] int           
)           
           
select @Balance = (case CurrencyID when 'AED' then (SUM(case TrxType when 'C' then Amount else Amount*-1 end)) else (SUM(case TrxType when 'C' then ForeignAmount else ForeignAmount*-1 end)) end)           
from t_Transactions           
where OurBranchID = @OurBranchID and AccountID = @AccountID AND MONTH(wDate) != MONTH(ValueDate) AND YEAR(wDate) = YEAR(@IntMonth) AND MONTH(wDate) = MONTH(@IntMonth) AND Status <> 'R'           
group by CurrencyID          
      
declare @PrevMonthBalance decimal(18,3) = 0      
select @CcyID = CurrencyID, @OpBalance = ClearBalance,@PrevMonthBalance = PrevMonthBalance      
from t_AccountMonthBalances am           
inner join t_Products p on am.OurBranchID = p.OurBranchID and am.ProductID = p.ProductID           
where am.OurBranchID = @OurBranchID and AccountID = @AccountID and [Month] = @IntMonth           
      
set @ZOpBalance = 0           
set @ZBalance = 0           
declare @ZXStartDate date      
declare @FilterStartDate date      
declare @RunningPrevMonthBalance decimal(18,3)      
declare @ZXAccountBalance decimal(18,3)      
select top 1 @ZXAccountBalance = ClearBalance from t_AccountBalance where AccountID = @ZXAccountID    
select top 1 @ZXStartDate = wDate from t_Transactions where AccountID = @ZXAccountID and TrxType = 'D' and RefNo <> 'ODINT' order by wDate desc      
    
if(@ZXStartDate >= @IntMonth or @ZXAccountBalance >= 0)      
begin    
set @Balance = 0    
set @PrevMonthBalance = 0      
end    
     
set @OpBalance = @OpBalance - @PrevMonthBalance      
set @FilterStartDate = @IntMonth      
set @RunningPrevMonthBalance = @PrevMonthBalance      
if(@ZXStartDate > @IntMonth)      
begin      
set @FilterStartDate = @ZXStartDate    
end      
    
set @OpBalance = isnull(@OpBalance,0) + isnull(@Balance,0)           
    
declare @i int = 0           
declare @dated date = @IntMonth           
while @dated <= @EndDate           
begin          
      
 declare @CurrDBal decimal(18,3)= 0      
 declare @CurrCBal decimal(18,3)= 0      
   set @Balance=0           
   select @CurrDBal = (case CurrencyID when 'AED' then (SUM(case TrxType when 'D' then Amount else 0 end)) else (SUM(case TrxType when 'D' then ForeignAmount else 0 end)) end)     ,      
   @CurrCBal = (case CurrencyID when 'AED' then (SUM(case TrxType when 'C' then Amount else 0 end)) else (SUM(case TrxType when 'C' then ForeignAmount else 0 end)) end)           
   from t_Transactions           
   where OurBranchID = @OurBranchID and AccountID = @AccountID AND CAST(wDate AS date) = @dated AND Status <> 'R'           
   group by CurrencyID           
         
  if(isnull(@CurrDBal,0) < @PrevMonthBalance and @PrevMonthBalance <> 0 and isnull(@CurrDBal,0) > 0)      
 begin      
  set @PrevMonthBalance = @PrevMonthBalance - isnull(@CurrDBal,0)      
  set @CurrDBal = 0      
 end      
 else if(isnull(@CurrDBal,0) > @PrevMonthBalance and @PrevMonthBalance <> 0 and isnull(@CurrDBal,0) > 0)      
 begin      
  set @CurrDBal = isnull(@CurrDBal,0) - @PrevMonthBalance      
  set @PrevMonthBalance = 0      
 end      
       
   set @OpBalance = isnull(@OpBalance,0) - isnull(@CurrDBal,0) + isnull(@CurrCBal,0)        
      set @tDebit=0           
   select @tDebit = (case CurrencyID when 'AED' then (SUM(case TrxType when 'C' then Amount else Amount*-1 end)) else (SUM(case TrxType when 'C' then ForeignAmount else ForeignAmount*-1 end)) end)           
   from t_Transactions           
   where OurBranchID = @OurBranchID and AccountID = @AccountID AND CAST(wDate AS date) between @FilterStartDate and @dated AND TrxType = 'D' AND Status <> 'R'           
   group by CurrencyID           
          
   set @Balance=0           
   select @Balance = (case CurrencyID when 'AED' then (SUM(case TrxType when 'C' then Amount else Amount*-1 end)) else (SUM(case TrxType when 'C' then ForeignAmount else ForeignAmount*-1 end)) end)           
   from t_Transactions           
   where OurBranchID = @OurBranchID and AccountID = @ZXAccountID AND CAST(wDate AS date) between @FilterStartDate and @dated AND Status <> 'R' and RefNo <> 'ODINT' and DescriptionID <> '300060'     
   group by CurrencyID           
   set @ZBalance = @Balance + ABS(@tDebit)      
        
  if(@RunningPrevMonthBalance > 0 and @tDebit < 0 and @RunningPrevMonthBalance > ABS(@tDebit))      
 set @ZBalance = @RunningPrevMonthBalance - ABS(@tDebit)      
   if @ZBalance > 0           
      set @DiffBalance = @OpBalance           
   else           
      set @DiffBalance = @OpBalance + @ZBalance           
           
   set @i = @i+1           
   insert into @IntTable values (@i, @dated, @OpBalance, @ZBalance, @DiffBalance)           
   set @dated = DATEADD(d, 1, @dated)           
end           
     --select * from @IntTable      
declare @intBalance decimal(21,3)=0, @lastBalance decimal(21,3)=0           
declare @intDated date, @lastDated date, @intDay int = 0           
declare @count int = 0, @j int = 0           
select @count = COUNT(*) from @IntTable           
           
--select * from @IntTable           
           
while @j < @count           
begin           
   set @j = @j+1           
   select @intDated = Dated, @intBalance = DiffBalance from @IntTable where SNo = @j           
   if @j = 1           
   begin           
      select @intDay=1, @lastDated = @intDated, @lastBalance = @intBalance           
   end           
   else           
   begin           
      if (@lastBalance = @intBalance)      
   begin           
         select @intDay=@intDay+1           
      end           
      else           
      begin           
         insert into @IntFinalTable values (@lastDated, @lastBalance, @intDay);           
         select @lastDated=@intDated, @intDay=1, @lastBalance=@intBalance           
      end           
   end           
end           
insert into @IntFinalTable values (@lastDated, @lastBalance, @intDay);           
           
select * from @IntFinalTable