﻿CREATE Procedure [dbo].[spc_GetGLInfo] (    
 @OurBranchID varchar(30),      
 @AccountID varchar(30)      
)      
    
AS      
    
SET NOCOUNT ON    
    
SELECT g.OurBranchID, g.AccountID, g.Description AS Name, g.AccountType, g.CurrencyID, g.Balance,     
g.ForeignBalance, g.ShadowBalance, c.Description AS CurrencyName, c.MeanRate, g.IsPosting, g.AccountClass    
    
FROM t_GL g    
    
INNER JOIN t_Currencies c ON g.OurBranchID = c.OurBranchID AND g.CurrencyID = c.CurrencyID    
    
Where g.OurBranchID = @OurBranchID and g.AccountID = @AccountID