﻿CREATE PROCEDURE [dbo].[sp_ATM_AddEditUBPSTransaction]  
(  
 @OurBranchID       varchar(30),   
 @WithdrawlBranchID      varchar(30),   
 @ATMID       varchar(30),   
 @AccountID           varchar(30),  
 @Amount        money,  
 @USDAmount       money=0,   
 @OtherCurrencyAmount      money=0,  
 @USDRate       money=0,  
 @Supervision       char(1),   
 @RefNo        varchar(50),  
 @PHXDate        datetime,  
 @MerchantType      varchar(30),  
 @OurIMD       varchar(30),  
 @AckInstIDCode      varchar(30),  
 @Currency       varchar(30),  
 @NameLocation      varchar(100),  
 @MCC        varchar(30),  
 @PHXDateTime        varchar(14),  
 @UBPS_RecordData      varchar(50),  
 @NewRecord       bit=1  
)  
AS  
-- DECLARE @BankShortName as varchar(6)  
  
 DECLARE @mScrollNo as int  
 DECLARE @mSerialNo as smallint  
 DECLARE @mAccountID as varchar(30)  
 DECLARE @mAccountName as varchar(100)  
 DECLARE @mAccountType as char  
 DECLARE @mProductID as varchar(30)  
 DECLARE @mCurrencyID as varchar(30)  
 DECLARE @mIsLocalCurrency as bit  
  SET @mIsLocalCurrency=1   
 DECLARE @mwDate as datetime  
 DECLARE @mTrxType as char  
 DECLARE @mGLID as varchar(30)  
  SET @mGLID = ''   
 DECLARE @mForeignAmount as money  
  SET @mForeignAmount=0   
 DECLARE @mExchangeRate as money  
  SET @mExchangeRate=1   
 DECLARE @mDescriptionID as varchar(30)  
  SET @mDescriptionID='987'   
 DECLARE @mDescription as varchar(100)  
 DECLARE @LocalCurrencyID as varchar(30)  
 DECLARE @LocalCurrency as varchar(30)  
 
 SELECT @LocalCurrencyID=LocalCurrencyID,@LocalCurrency=LocalCurrency FROM t_ATM_GlobalVariables  
    IF @Currency = @LocalCurrencyID  
     BEGIN  
      SET @mDescription=Right(@RefNo,6) + '-UBPS-' + left(@UBPS_RecordData,8) + '-' + substring(@UBPS_RecordData,34,24)  + '-' + Left(@RefNo,16) + '-' + @AccountID + '-' + @ATMID + '-' + SUBSTRING(@RefNo,17,4) + '/' + SUBSTRING(@RefNo,21,6)   
      SET @mDescription = @mDescription + '-' + @PHXDateTime  
     END  
    ELSE   
     IF @Currency = '840'  
      BEGIN  
       SET @mDescription=Right(@RefNo,6) + '-ATM(' + @ATMID + ') UBPS ('  +  left(@UBPS_RecordData,8)  + '/' +  substring(@UBPS_RecordData,34,24) + ') ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') AT ' + @NameLocation   
       SET @mDescription = @mDescription + '-' + @PHXDateTime  
      END  
     ELSE   
      BEGIN  
       SET @mDescription=Right(@RefNo,6) + '-ATM(' + @ATMID + ') UBPS ('  +  left(@UBPS_RecordData,8)  + '/' +  substring(@UBPS_RecordData,34,24) + ') (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount
) + '@' + convert(varchar(15),@USDRate) + ')) AT ' + @NameLocation   
       SET @mDescription = @mDescription + '-' + @PHXDateTime  
      END  
 DECLARE @mDescriptionCharges as varchar(100)  
    IF @Currency = @LocalCurrencyID  
     BEGIN  
      SET @mDescriptionCharges=Right(@RefNo,6) + '-UBPS-CHG-' + left(@UBPS_RecordData,8) + '-' + substring(@UBPS_RecordData,34,24)  + '-' + Left(@RefNo,16) + '-' + @AccountID + '-' + @ATMID + '-' + SUBSTRING(@RefNo,17,4) + '/' + SUBSTRING(@RefNo,21,6)   
      SET @mDescriptionCharges = @mDescriptionCharges + '-' + @PHXDateTime  
     END  
    ELSE   
     IF @Currency = '840'  
      BEGIN  
       SET @mDescriptionCharges=Right(@RefNo,6) + '-ATM(' + @ATMID + ') UBPS-CHG ('  +  left(@UBPS_RecordData,8)  + '/' +  substring(@UBPS_RecordData,34,24) + ') ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') AT ' + @NameLocation   
       SET @mDescriptionCharges = @mDescriptionCharges + '-' + @PHXDateTime  
      END  
     ELSE   
      BEGIN  
       SET @mDescriptionCharges=Right(@RefNo,6) + '-ATM(' + @ATMID + ') UBPS-CHG ('  +  left(@UBPS_RecordData,8)  + '/' +  substring(@UBPS_RecordData,34,24) + ') (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15)
,@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) AT ' + @NameLocation   
       SET @mDescriptionCharges = @mDescriptionCharges + '-' + @PHXDateTime  
      END  
 DECLARE @mDescriptionH as varchar(100)  
    IF @Currency = @LocalCurrencyID  
     BEGIN  
      SET @mDescriptionH=Right(@RefNo,6) + '-UBPS-' + left(@UBPS_RecordData,8) + '-' + substring(@UBPS_RecordData,34,24)  + '-' + Left(@RefNo,16) + '-' + @AccountID + '-' + @ATMID + '-' + SUBSTRING(@RefNo,17,4) + '/' + SUBSTRING(@RefNo,21,6)   
      SET @mDescriptionH = @mDescriptionH + '-' + @PHXDateTime  
     END  
    ELSE   
     IF @Currency = '840'  
      BEGIN  
       SET @mDescriptionH=Right(@RefNo,6) + '-ATM(' + @ATMID + ') UBPS ('  +  left(@UBPS_RecordData,8)  + '/' +  substring(@UBPS_RecordData,34,24) + ') ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') AT ' +  + @NameLocation + ' From : A/c # ' + @AccountID  
       SET @mDescriptionH = @mDescriptionH + '-' + @PHXDateTime  
      END  
     ELSE   
      BEGIN  
       SET @mDescriptionH=Right(@RefNo,6) + '-ATM(' + @ATMID + ') UBPS ('  +  left(@UBPS_RecordData,8)  + '/' +  substring(@UBPS_RecordData,34,24) + ') (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) AT ' +  + @NameLocation + ' From : A/c # ' + @AccountID  
       SET @mDescriptionH = @mDescriptionH + '-' + @PHXDateTime  
      END  
 DECLARE @mDescriptionHCharges as varchar(100)  
    IF @Currency = @LocalCurrencyID  
     BEGIN  
      SET @mDescriptionHCharges=Right(@RefNo,6) + '-UBPS-CHG-' + left(@UBPS_RecordData,8) + '-' + substring(@UBPS_RecordData,34,24)  + '-' + Left(@RefNo,16) + '-' + @AccountID + '-' + @ATMID + '-' + SUBSTRING(@RefNo,17,4) + '/' + SUBSTRING(@RefNo,21,6)  
      SET @mDescriptionHCharges = @mDescriptionHCharges + '-' + @PHXDateTime  
     END  
    ELSE   
     IF @Currency = '840'  
      BEGIN  
       SET @mDescriptionHCharges=Right(@RefNo,6) + '-ATM(' + @ATMID + ') UBPS-CHG ('  +  left(@UBPS_RecordData,8)  + '/' +  substring(@UBPS_RecordData,34,24) + ') ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') AT ' + @NameLocation + ' From : A/c # ' + @AccountID  
       SET @mDescriptionHCharges = @mDescriptionHCharges + '-' + @PHXDateTime  
      END  
     ELSE   
      BEGIN  
       SET @mDescriptionHCharges=Right(@RefNo,6) + '-ATM(' + @ATMID + ') UBPS-CHG ('  +  left(@UBPS_RecordData,8)  + '/' +  substring(@UBPS_RecordData,34,24) + ') (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15
),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) AT ' + @NameLocation + ' From : A/c # ' + @AccountID  
       SET @mDescriptionHCharges = @mDescriptionHCharges + '-' + @PHXDateTime  
      END  
 DECLARE @mDescriptionChargesExcise as varchar(255)  
    IF @Currency = @LocalCurrencyID  
     BEGIN  
      SET @mDescriptionChargesExcise=Right(@RefNo,6) + '-UBPS-FED-' + left(@UBPS_RecordData,8) + '-' + substring(@UBPS_RecordData,34,24)  + '-' + Left(@RefNo,16) + '-' + @AccountID + '-' + @ATMID + '-' + SUBSTRING(@RefNo,17,4) + '/' + SUBSTRING(@RefNo,21,
6)  
      SET @mDescriptionChargesExcise = @mDescriptionChargesExcise + '-' + @PHXDateTime  
     END  
    ELSE   
     IF @Currency = '840'  
      BEGIN  
       SET @mDescriptionChargesExcise=Right(@RefNo,6) + '-ATM(' + @ATMID + ') UBPS-FED ('  +  left(@UBPS_RecordData,8)  + '/' +  substring(@UBPS_RecordData,34,24) + ') ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') AT ' +
 @NameLocation + ' From : A/c # ' + @AccountID  
       SET @mDescriptionChargesExcise = @mDescriptionChargesExcise + '-' + @PHXDateTime  
      END  
     ELSE   
      BEGIN  
       SET @mDescriptionChargesExcise=Right(@RefNo,6) + '-ATM(' + @ATMID + ') UBPS-FED ('  +  left(@UBPS_RecordData,8)  + '/' +  substring(@UBPS_RecordData,34,24) + ') (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) AT ' + @NameLocation + ' From : A/c # ' + @AccountID  
       SET @mDescriptionChargesExcise = @mDescriptionChargesExcise + '-' + @PHXDateTime  
      END  
 DECLARE @mDescriptionHChargesExcise as varchar(255)  
    IF @Currency = @LocalCurrencyID  
     BEGIN  
      SET @mDescriptionHChargesExcise=Right(@RefNo,6) + '-UBPS-FED-' + left(@UBPS_RecordData,8) + '-' + substring(@UBPS_RecordData,34,24)  + '-' + Left(@RefNo,16) + '-' + @AccountID + '-' + @ATMID + '-' + SUBSTRING(@RefNo,17,4) + '/' + SUBSTRING(@RefNo,21,6)  
      SET @mDescriptionHChargesExcise = @mDescriptionHChargesExcise + '-' + @PHXDateTime  
     END  
    ELSE   
     IF @Currency = '840'  
      BEGIN  
       SET @mDescriptionHChargesExcise=Right(@RefNo,6) + '-ATM(' + @ATMID + ') UBPS-FED ('  +  left(@UBPS_RecordData,8)  + '/' +  substring(@UBPS_RecordData,34,24) + ') ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') AT ' + @NameLocation + ' From : A/c # ' + @AccountID  
       SET @mDescriptionHChargesExcise = @mDescriptionHChargesExcise + '-' + @PHXDateTime  
      END  
     ELSE   
      BEGIN  
       SET @mDescriptionHChargesExcise=Right(@RefNo,6) + '-ATM(' + @ATMID + ') UBPS-FED ('  +  left(@UBPS_RecordData,8)  + '/' +  substring(@UBPS_RecordData,34,24) + ') (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) AT ' + @NameLocation + ' From : A/c # ' + @AccountID  
       SET @mDescriptionHChargesExcise = @mDescriptionHChargesExcise + '-' + @PHXDateTime  
      END  
  
 DECLARE @mRemarks as varchar(200)  
 DECLARE @mBankCode as varchar(30)  
  SET @mBankCode='093'   
 DECLARE @mOperatorID as varchar(30)  
  SET @mOperatorID = 'ATM-' + @ATMID  
 DECLARE @mSupervisorID as varchar(30)  
  SET @mSupervisorID = @OurIMD   
  
--Checking Variables  
 DECLARE @vCount1 as bit  
 DECLARE @vCount2 as bit  
 DECLARE @vCount3 as bit  
 DECLARE @vCount4 as bit  
 DECLARE @vAccountID as varchar(30)  
 DECLARE @vClearBalance as money  
 DECLARE @vClearedEffects as money  
 DECLARE @vFreezeAmount as money  
 DECLARE @vMinBalance as money  
 DECLARE @vIsClose as char  
 DECLARE @vAreCheckBooksAllowed as bit  
 DECLARE @vAllowDebitTransaction as bit  
 DECLARE @vTaxAmount as money  
  SET @vTaxAmount = 0  
  
 DECLARE @vAccountLimit as money  
 DECLARE @vExpiryDate as datetime  
 DECLARE @vCheckLimit as bit  
  SET @vCheckLimit = 0  
 DECLARE @vODLimitAllow as bit  
  SET @vODLimitAllow = 0  
  
 DECLARE @vBranchName as varchar(100)  
 DECLARE @Status as int   
  
 DECLARE @mLastEOD as datetime  
-- DECLARE @mWorkingDate as datetime  
 DECLARE @mCONFRETI as datetime  
-- DECLARE @mWithdrawlBranchName as varchar(50)  
  
 DECLARE @WithDrawlCharges as money  
 DECLARE @WithDrawlChargesPercentage as money  
 DECLARE @IssuerAccountID as varchar(30)  
 DECLARE @IssuerAccountTitle as varchar(100)  
 DECLARE @IssuerChargesAccountID as varchar(30)  
 DECLARE @IssuerTitle as varchar(100)  
 DECLARE @mRemarksCharges as varchar(200)  
  
 DECLARE @WRDLCHGExciseDuty as money --samad  
 DECLARE @ExciseDutyPercentage as money --samad  
 DECLARE @ExciseDutyAccountID as varchar(30) --samad  
  
 DECLARE @TELECOMType as varchar(30) --samad  
  SET @TELECOMType = left(@UBPS_RecordData,8)  
  
 SET NOCOUNT ON  
  
 SELECT @Status = -1  
---------------------  
  
 BEGIN  
  
   SELECT @mLastEOD=LastEOD,@mwDate=WorkingDate,@mCONFRETI=CONFRETI FROM t_Last WHERE (OurBranchID = @OurBranchID)  
   IF @mLastEOD = @mwDate or @mCONFRETI = '1/1/9999'  
    BEGIN  
        
     SELECT @Status=9  --INVALID_DATE  
     SELECT @Status  as Status  
     RETURN (1)   
    END  
  
   SELECT @mScrollNo=(ISNULL(MAX(ScrollNo),0)+1) FROM t_ATM_TransferTransaction WHERE (OurBranchID = @OurBranchID) and (wDate = @mwDate)   
  
------------------------  
------------------------  
--Debit Transaction     
  
  
   SELECT @vCount1=count(*) From t_Account  
   WHERE (OurBranchID = @OurBranchID) AND( AccountID=@AccountID)  
   IF @vCount1=0  
    BEGIN  
        
     SELECT @Status=2  --INVALID_ACCOUNT  
     SELECT @Status  as Status  
     RETURN (1)   
    END  
  
   SET @WithDrawlCharges = 0  
   SET @WithDrawlChargesPercentage = 0  
   SET @ExciseDutyPercentage = 0  
   SET @WRDLCHGExciseDuty = 0  
--   IF @MCC = '6010' and @OurIMD <> '402581'  
    BEGIN  
     IF @Currency = @LocalCurrencyID  
     BEGIN  
      SELECT @WithDrawlCharges=UBPSCharges,@WithDrawlChargesPercentage=UBPSChargesPercentage,  
        @IssuerAccountID=UBPSAccountID,@IssuerAccountTitle=UBPSTitle,  
        @ExciseDutyPercentage=ExciseDutyPercentage,@ExciseDutyAccountID=ExciseDutyAccountID   --samad  
      From t_ATM_SwitchCharges  
      WHERE (MerchantType = @MerchantType)  
      IF @WithDrawlCharges < ((@WithDrawlChargesPercentage/100)*@Amount)  
       BEGIN  
        SET @WithDrawlCharges = ((@WithDrawlChargesPercentage/100)*@Amount)  
       END  
      SET @WRDLCHGExciseDuty = (@WithDrawlCharges*@ExciseDutyPercentage)/100  
     END  
    ELSE  
     BEGIN  
      SELECT @WithDrawlCharges=ForeignUBPSCharges,@WithDrawlChargesPercentage=ForeignUBPSChargesPercentage,  
        @IssuerAccountID=UBPSAccountID,@IssuerAccountTitle=UBPSTitle  
      From t_ATM_SwitchCharges  
      WHERE (MerchantType = @MerchantType)  
      IF @WithDrawlCharges < ((@WithDrawlChargesPercentage/100)*@Amount)  
       BEGIN  
        SET @WithDrawlCharges = ((@WithDrawlChargesPercentage/100)*@Amount)  
       END  
     END  
    END  
  
   IF @TELECOMType = 'WARID001' OR @TELECOMType = 'UFONE001' OR @TELECOMType = 'TELEN001'  
    BEGIN  
     SET @WithDrawlCharges = 0  
     SET @WithDrawlChargesPercentage = 0  
     SET @ExciseDutyPercentage = 0  
     SET @WRDLCHGExciseDuty = 0  
    END  
  
   SELECT Top 1 @vCheckLimit=CheckLimit FROM t_ATM_GlobalVariables  
   SELECT Top 1 @vODLimitAllow=ODLimitAllow FROM t_ATM_Banks WHERE BankIMD = left(@RefNo,6)  
  
   SELECT @mAccountName=B.Name, @mProductID=B.ProductID, @mCurrencyID=P.CurrencyID,@mAccountType='C',@vClearBalance=B.ClearBalance,@vClearedEffects=B.ClearedEffects,@vFreezeAmount=B.FreezeAmount,@vMinBalance=A.MinBalance,@vIsClose=A.Status, @vAreCheckBooksAllowed=P.AreCheckBooksAllowed, @vAllowDebitTransaction=S.AllowDebitTransaction, @vAccountLimit=B.Limit  
   FROM t_AccountBalance B INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND  B.ProductID = A.ProductID INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND  A.ProductID = P.ProductID LEFT OUTER JOIN t_AccountSpecialCondition S ON B.OurBranchID = S.OurBranchID AND B.AccountID = S.AccountID  
   WHERE  (A.OurBranchID = @OurBranchID) AND (A.AccountID = @AccountID)  
   IF @vAccountLimit > 0 AND @vCheckLimit = 1 AND @vODLimitAllow = 1  
    BEGIN  
     SELECT @vExpiryDate=ExpiryDate FROm t_AccountLimits WHERE (OurBranchID = @OurBranchID) and (AccountID = @AccountID)  
     IF @vExpiryDate > @mwDate  
      BEGIN  
       SET @vClearBalance=(@vClearBalance+@vAccountLimit)  
      END  
    END  
   IF @vIsClose = 'C' or @vIsClose = 'T' or @vIsClose = 'X' or @vAllowDebitTransaction = 1  
    BEGIN  
        
     SELECT @Status=3  --INACTIVE/CLOSE_ACCOUNT  
     SELECT @Status  as Status  
     RETURN (1)   
    END  
   IF @Amount >(@vClearBalance-@vClearedEffects)-@WithDrawlCharges-@WRDLCHGExciseDuty-@vTaxAmount or @Amount >((@vClearBalance-@vClearedEffects)-@vFreezeAmount-@vMinBalance-@WithDrawlCharges-@WRDLCHGExciseDuty-@vTaxAmount) or @Amount >((@vClearBalance-@vClearedEffects)-@vMinBalance-@WithDrawlCharges-@WRDLCHGExciseDuty-@vTaxAmount)  
    BEGIN  
        
     SELECT @Status=4  --LOW_BALANCE  
     SELECT @Status  as Status  
     RETURN (1)   
    END  
   IF @vAreCheckBooksAllowed = 0  
    BEGIN  
        
     SELECT @Status=3  --INVALID_PRODUCT  
     SELECT @Status  as Status  
     RETURN (1)   
    END  
   IF @mCurrencyID <> @LocalCurrency  
    BEGIN  
        
     SELECT @Status=3  --INVALID_CURRENCY  
     SELECT @Status  as Status  
     RETURN (1)   
    END  
  
   Set @mSerialNo = 1  
   Set @mTrxType = 'D'  
   SET @vBranchName = 'N/A'  
   SELECT @vBranchName=Heading2 FROM t_GlobalVariables WHERE (OurBranchID = @OurBranchID)  
  
  
  
     IF @Currency = @LocalCurrencyID  
      BEGIN  
       Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') UBPS Transaction From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
       Set @mRemarksCharges='Being Amount of ATM (' + @ATMID+ ') UBPS Charges Transaction From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
      END  
     ELSE   
      IF @Currency = '840'  
       BEGIN  
        Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') UBPS Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
        Set @mRemarksCharges='Being Amount of ATM (' + @ATMID+ ') UBPS Charges Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
       END  
      ELSE   
       BEGIN  
        Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') UBPS Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
        Set @mRemarksCharges='Being Amount of ATM (' + @ATMID+ ') UBPS Charges Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)  
       END  
  INSERT INTO t_ATM_TransferTransaction  
   (  
    ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,       
    valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,  
    BankCode,Remarks,OperatorID,SupervisorID,MCC,MerchantType,CreditCardNumber  
   )    
  
  VALUES  
  
  
  
   (  @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@AccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,  
    @mwDate,@mwDate,@PHXDate,@mTrxType,@Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescription,@ATMID,  
    @mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MCC,@MerchantType,@UBPS_RecordData  
   )  
     SELECT @Status=@@ERROR  
  
  
    IF @Status <> 0   
     BEGIN  
         
      RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR       RETURN (5000)  
     END  
--Debit Transaction Switch Charges     
  
IF @WithDrawlCharges > 0  
 BEGIN  
   Set @mSerialNo = 3  
  
  INSERT INTO t_ATM_TransferTransaction  
   (  
    ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,  
     valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,  
    BankCode,Remarks,OperatorID,SupervisorID,MCC,MerchantType,CreditCardNumber  
   )    
  
  VALUES  
  
   (  
    @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@AccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,  
    @mwDate,@mwDate,@PHXDate,@mTrxType,@Supervision,@mGLID,Round(@WithDrawlCharges,2),@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescriptionCharges,@ATMID,  
    @mBankCode,@mRemarksCharges,@mOperatorID,@mSupervisorID,@MCC,@MerchantType,@UBPS_RecordData  
   )  
     SELECT @Status=@@ERROR  
  
    IF @Status <> 0   
     BEGIN  
         
      RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR       RETURN (5000)  
     END  
 END  
--Credit Transaction     
IF @WithDrawlCharges > 0 AND @WRDLCHGExciseDuty > 0 AND @ExciseDutyAccountID <> ''   
 BEGIN  
   Set @mSerialNo = 5  
  
  INSERT INTO t_ATM_TransferTransaction  
   (  
    ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,  
     valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,  
    BankCode,Remarks,OperatorID,SupervisorID,MCC,MerchantType,CreditCardNumber  
   )    
  
  VALUES  
  
   (  
    @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@AccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,  
    @mwDate,@mwDate,@PHXDate,@mTrxType,@Supervision,@mGLID,@WRDLCHGExciseDuty,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescriptionChargesExcise,@ATMID,  
    @mBankCode,@mRemarksCharges,@mOperatorID,@mSupervisorID,@MCC,@MerchantType,@UBPS_RecordData  
   )  
     SELECT @Status=@@ERROR  
  
    IF @Status <> 0   
     BEGIN  
         
      RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR       RETURN (5000)  
     END  
 END  
  
  SELECT @vAccountID=AccountID, @mAccountName=Description,@mProductID='GL', @mCurrencyID=CurrencyID,@mAccountType='G' FROM t_GL   
  WHERE (OurBranchID = @OurBranchID) AND (AccountID =  @IssuerAccountID)  
  Set @mSerialNo = 2  
  Set @mTrxType = 'C'  
  
  INSERT INTO t_ATM_TransferTransaction  
   (  
    ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,  
    valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,  
    BankCode,Remarks,OperatorID,SupervisorID,MCC,MerchantType,CreditCardNumber  
   )    
  
  VALUES  
  
   (  
    @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@vAccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,  
    @mwDate,@mwDate,@PHXDate,@mTrxType,@Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescriptionH,@ATMID,  
    @mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MCC,@MerchantType,@UBPS_RecordData  
   )  
  
    SELECT @Status=@@ERROR  
  
    IF @Status <> 0   
     BEGIN  
         
      RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR  
      RETURN (5000)  
     END  
  
--Credit Transaction Switch Charges     
IF @WithDrawlCharges > 0  
 BEGIN  
  
  SELECT @vAccountID=AccountID, @mAccountName=Description,@mProductID='GL', @mCurrencyID=CurrencyID,@mAccountType='G' FROM t_GL   
  WHERE (OurBranchID = @OurBranchID) AND (AccountID =  @IssuerChargesAccountID)  
  
  Set @mSerialNo = 4  
  
  INSERT INTO t_ATM_TransferTransaction  
   (  
    ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,  
    valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,  
    BankCode,Remarks,OperatorID,SupervisorID,MCC,MerchantType,CreditCardNumber  
   )    
  
  VALUES  
  
   (  
    @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@vAccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,  
    @mwDate,@mwDate,@PHXDate,@mTrxType,@Supervision,@mGLID,Round(@WithDrawlCharges,2),@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescriptionHCharges,@ATMID,  
    @mBankCode,@mRemarksCharges,@mOperatorID,@mSupervisorID,@MCC,@MerchantType,@UBPS_RecordData  
   )  
  
    SELECT @Status=@@ERROR  
  
    IF @Status <> 0   
     BEGIN  
         
      RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR  
      RETURN (5000)  
     END  
 END  
  
IF @WithDrawlCharges > 0 AND @WRDLCHGExciseDuty > 0 AND @ExciseDutyAccountID <> ''   
 BEGIN  
  
  SELECT @vAccountID=AccountID, @mAccountName=Description,@mProductID='GL', @mCurrencyID=CurrencyID,@mAccountType='G' FROM t_GL   
  WHERE (OurBranchID = @OurBranchID) AND (AccountID =  @ExciseDutyAccountID)  
  
  Set @mSerialNo = 6  
  
  INSERT INTO t_ATM_TransferTransaction  
   (  
    ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,  
    valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,  
    BankCode,Remarks,OperatorID,SupervisorID,MCC,MerchantType,CreditCardNumber  
   )    
  
  VALUES  
  
   (  
    @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@vAccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,  
    @mwDate,@mwDate,@PHXDate,@mTrxType,@Supervision,@mGLID,@WRDLCHGExciseDuty,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescriptionHChargesExcise,@ATMID,  
    @mBankCode,@mRemarksCharges,@mOperatorID,@mSupervisorID,@MCC,@MerchantType,@UBPS_RecordData  
   )  
  
    SELECT @Status=@@ERROR  
  
    IF @Status <> 0   
     BEGIN  
         
      RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR  
      RETURN (5000)  
     END  
 END  
  
 IF @vAccountLimit > 0 AND @vCheckLimit = 1 AND @vODLimitAllow = 1  AND @vExpiryDate > @mwDate  
  BEGIN  
   SELECT @Status = 0 , @vClearBalance = Round(((IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0)-@vAccountLimit)-IsNull(@Amount,0)-isNull(@WithDrawlCharges,0)-isNull(@vTaxAmount,0)-isnull(@WRDLCHGExciseDuty,0)),2)*100  
   SELECT @Status As Status , @vClearBalance As ClearBalance  
  END   
 ELSE  
  BEGIN  
   SELECT @Status = 0 , @vClearBalance = Round(((IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0))-IsNull(@Amount,0)-isNull(@WithDrawlCharges,0)-isNull(@vTaxAmount,0)-isnull(@WRDLCHGExciseDuty,0)),2)*100  
   SELECT @Status As Status , @vClearBalance As ClearBalance  
  END   
  
  RETURN 0   
     
 END