﻿CREATE PROCEDURE [dbo].[sp_ATM_PRC_Get_TDB_TCR]
	@OurBranchID varchar(30)
	
 AS
	Declare @TDB money,@TCR money
	Declare @Count int
	
	Select @Count = Count(*)  from t_ATM_TransferTransaction
	where OurBranchID = @OurBranchID

	if @Count > 0
	BEGIN
		select @TDB=isnull(sum(amount),0)  from t_ATM_TransferTransaction
		Where (OurBranchID = @OurBranchID) and  (TrxType = 'D') and ( Supervision <> 'R')

		select @TCR=isnull(sum(amount),0)  from t_ATM_TransferTransaction
		Where (OurBranchID = @OurBranchID) and  (TrxType = 'C') and ( Supervision <> 'R')
	
		if @TDB = @TCR
		   Select 'Balanced'
		else
		   Select 'Error'
	END
	else
		Select @count