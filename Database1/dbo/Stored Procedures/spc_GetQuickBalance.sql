﻿CREATE PROCEDURE [dbo].[spc_GetQuickBalance]                
                
 @OurBranchID varchar(30),                
 @AccountID varchar(30)                
 AS                
 SET NOCOUNT ON                
 Declare @AccountType char(1),                
  @GlobalCurrencyID varchar(30),                    
  @GLBalance money,                
  @GLForeignBalance money,                
  @AccountName varchar(50),                
  @CurrencyID varchar(30)                    
                 
 IF (select count(AccountID) from t_Account where OurBranchID = @OurBranchID AND AccountID = @AccountID) > 0                
  SET @AccountType = 'C'                
 ELSE IF (select count(AccountID) from t_GL where OurBranchID = @OurBranchID AND AccountID = @AccountID) > 0                
  SET @AccountType = 'G'                 
 ELSE                
 BEGIN                
  Select '0' as RetStatus                
  Return                
 END                
                
 IF @AccountType = 'C'                
 BEGIN                                
  --declare @Limit money                                
  --set @Limit = 0        
  --exec @Limit = fnc_GetLimit @OurBranchID, @AccountID                                
                                
  SELECT '1' as RetStatus, ab.ClearBalance, ab.Effects, ab.Limit AS Limit, ab.ShadowBalance, ab.FreezeAmount, a.OurBranchID, ISNULL(a.AllowDebitTransaction,0) AS DebitFrozen, ISNULL(a.AllowCreditTransaction,0) AS CreditFrozen,                 
  a.AccountID, a.Name, a.ProductID, a.CurrencyID, ISNULL(a.Status,'') AS AccountStatus, a.Reminder, ISNULL(P.MinBalance,0) AS ProductMinBalance,                   
  ab.ClearBalance + ab.Effects + ab.Limit - ab.FreezeAmount - (ISNULL(P.MinBalance,0)) AS TotalBalance, c.ClientID,  cc.MeanRate, convert(varchar(19), a.OpenDate, 120) AS OpenDate, p.ProductType, p.Description AS ProductDesc,                
  case when ab.ClearBalance + ab.Limit - ab.FreezeAmount - (ISNULL(P.MinBalance,0)) > 0 then                   
  ab.ClearBalance + ab.Limit - ab.FreezeAmount - (ISNULL(P.MinBalance,0)) else 0 end AS DrawableBalance, a.IBAN,                
  c.GroupCode, case ISNULL(c.CategoryId,'') when 'Individual' then '1100' else ISNULL(s.EconoActCode,'1200') end AS EconoActCode,                
  case ISNULL(c.CategoryId,'') when 'Individual' then ISNULL(c1.ShortName,'') else ISNULL(c2.ShortName,'') end AS CountryCode              
  FROM t_AccountBalance ab                   
  INNER JOIN t_Account a ON ab.OurBranchID = a.OurBranchID AND ab.AccountID = a.AccountID                
  INNER JOIN t_Currencies cc ON ab.OurBranchID = cc.OurBranchID AND a.CurrencyID = cc.CurrencyID                
  INNER JOIN t_Products P ON a.OurBranchID = P.OurBranchID AND a.ProductID = P.ProductID                    
  INNER JOIN t_Customer c ON ab.OurBranchID = c.OurBranchID AND a.ClientID = c.ClientID                
  left join t_SICCode s on c.OurBranchID = s.OurBranchID and ISNULL(c.SICCODESCORP,'') = ISNULL(s.SICID,'')                
  left join t_Country c1 on c.OurBranchID = c1.OurBranchID and c1.CountryId = c.CountryCors            
  left join t_Country c2 on c.OurBranchID = c2.OurBranchID and c2.CountryId = c.RegAddCountryCorp            
  Where (a.OurBranchID = @OurBranchID) and (a.AccountID = @AccountID)                 
 END                                
 ELSE                
                
 BEGIN                
  Select @GlobalCurrencyID = LocalCurrency From t_GlobalVariables where OurBranchID = @OurBranchID                 
                
  Select @GLBalance = Balance,@GLForeignBalance=ForeignBalance,@AccountName=Description, @CurrencyID = CurrencyID From t_GL                
  Where (OurBranchID = @OurBranchID) and (AccountID = @AccountID)                
                
  IF @GLBalance is NULL                
   Select @GLBalance = 0                
                
  IF @CurrencyID = @GlobalCurrencyID  /*  @GLForeignBalance = 0 */                
  BEGIN                
   /* Cash Transactions */      
   Select @GLBalance = (@GLBalance - isnull(sum(Amount),0)) From t_CashTransactionModel                
   Where (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (TrxType = 'D') and (AccountType = 'G')                
   and (Supervision <> 'R') --and left(Refno, 9) <> 'LOCALCASH'                
                
   Select @GLBalance = (@GLBalance + isnull(sum(Amount),0)) From t_CashTransactionModel                
   Where (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (TrxType = 'C') and (AccountType = 'G')                
   and (Supervision <> 'R') --and left(Refno, 9) <> 'LOCALCASH'                    
                
      Select @GLBalance = (@GLBalance + isnull(sum(Amount),0)) From t_CashTransactionModel                
   Where (OurBranchID = @OurBranchID) and (GLID = @AccountID) and (TrxType = 'D')                 
   and (Supervision <> 'R') --and left(Refno, 9) <> 'LOCALCASH'                 
                
      Select @GLBalance = (@GLBalance - isnull(sum(Amount),0)) From t_CashTransactionModel                   
   Where (OurBranchID = @OurBranchID) and (GLID = @AccountID) and (TrxType = 'C')                 
   and (Supervision <> 'R') --and left(Refno, 9) <> 'LOCALCASH'                        
                
                
--Local Cash                 
  -- Select @GLBalance = (@GLBalance + isnull(sum(Amount),0)) From t_CashTransactionModel                
  -- Where (OurBranchID = @OurBranchID) and (GLID = @AccountID) and left(rtrim(Refno), 9) = 'LOCALCASH'                 
  -- and (Supervision <> 'R')                
                
                
   /* Transfer Transactions */                
   Select @GLBalance = @GLBalance - isnull(sum(Amount),0) From t_TransferTransactionModel                
   Where (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (TrxType = 'D') and (AccountType = 'G')                
   and (Supervision <> 'R')                
                
   Select @GLBalance = @GLBalance + isnull(sum(Amount),0) From t_TransferTransactionModel                
   Where (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (TrxType = 'C') and (AccountType = 'G')                
   and (Supervision <> 'R')                
                
   /* Inward Transactions */                
   Select @GLBalance = @GLBalance - isnull(sum(Amount),0) From t_InwardClearing                
   Where (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and  (AccountType = 'G') and TransactionType = 'D'                
  and status <> 'R'                
                
   Select @GLBalance = @GLBalance + isnull(sum(Amount),0) From t_InwardClearing                
   Where (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (AccountType = 'G') and TransactionType = 'C'                
   and status <> 'R'                
                
--t_ATM_CashTransaction                
    SELECT @GLBalance = @GLBalance + isnull(sum(Amount),0)                
    FROM t_ATM_CashTransaction                
    WHERE OurBranchID = @OurBranchID AND GLID = @AccountID                       
AND Supervision = 'C' AND TrxType = 'C'                
                
    SELECT @GLBalance = @GLBalance - isnull(sum(Amount),0)                
    FROM t_ATM_CashTransaction                
    WHERE OurBranchID = @OurBranchID AND GLID = @AccountID                       
AND Supervision = 'C' AND TrxType = 'D'                
                        
--'t_ATM_TransferTransaction                
    SELECT @GLBalance = @GLBalance + isnull(sum(Amount),0)                
    FROM t_ATM_TransferTransaction                
    WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                       
   AND Supervision = 'C' AND AccountType ='G' AND TrxType = 'C'                
                        
                
    SELECT @GLBalance = @GLBalance - isnull(sum(Amount),0)                
    FROM t_ATM_TransferTransaction                
    WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                 
   AND Supervision = 'C' AND AccountType ='G' AND TrxType = 'D'                
                
--'t_OnLine_CashTransaction                
    SELECT  @GLBalance = @GLBalance + isnull(sum(Amount),0)                
    FROM t_OnLineCashTransaction                
    WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                       
AND Supervision <> 'R' AND AccountType ='G' AND TrxType = 'C'                
                            
    SELECT  @GLBalance = @GLBalance - isnull(sum(Amount),0)            
    FROM t_OnLineCashTransaction                
    WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                       
AND Supervision <> 'R' AND AccountType ='G' AND TrxType = 'D'                
                
                
   Select '2' as RetStatus,@GLBalance,@AccountName                    
  END                
  ELSE                
  BEGIN                 
   /* Cash Transactions */            
   Select @GLForeignBalance = @GLForeignBalance - ForeignAmount From t_CashTransactionModel                
   Where (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (TrxType = 'D') and (AccountType = 'G')                
   and (Supervision = 'C') --and left(Refno, 9) <> 'LOCALCASH'             
                
   Select @GLForeignBalance = @GLForeignBalance +  ForeignAmount From t_CashTransactionModel                
   Where (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (TrxType = 'C') and (AccountType = 'G')                
   and (Supervision = 'C') --and left(Refno, 9) <> 'LOCALCASH'                
                
      Select @GLForeignBalance = @GLForeignBalance +  ForeignAmount From t_CashTransactionModel                
   Where (OurBranchID = @OurBranchID) and (GLID = @AccountID) and (TrxType = 'D') and (AccountType = 'C')                
   and (Supervision = 'C') --and left(Refno, 9) <> 'LOCALCASH'                
                
      Select @GLForeignBalance = @GLForeignBalance -  ForeignAmount From t_CashTransactionModel             Where (OurBranchID = @OurBranchID) and (GLID = @AccountID) and (TrxType = 'C') and (AccountType = 'C')                
   and (Supervision = 'C') --and left(Refno, 9) <> 'LOCALCASH'                
                
   /* Transfer Transactions */                
                
   Select @GLForeignBalance = @GLForeignBalance -  ForeignAmount From t_TransferTransactionModel                
   Where (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (TrxType = 'D') and (AccountType = 'G')                
   and (Supervision = 'C')                
                
   Select @GLForeignBalance = @GLForeignBalance +  ForeignAmount From t_TransferTransactionModel                
   Where (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (TrxType = 'C') and (AccountType = 'G')                
   and (Supervision = 'C')                
                
      Select @GLForeignBalance = @GLForeignBalance -  ForeignAmount From t_TransferTransactionModel                
   Where (OurBranchID = @OurBranchID) and (GLID = @AccountID) and (TrxType = 'D') and (AccountType = 'C')                
   and (Supervision = 'C')                
                
      Select @GLForeignBalance = @GLForeignBalance +  ForeignAmount From t_TransferTransactionModel                
   Where (OurBranchID = @OurBranchID) and (GLID = @AccountID) and (TrxType = 'C') and (AccountType = 'C')                
   and (Supervision = 'C')                
                
   /* Inward Transactions */                
   Select @GLForeignBalance = @GLForeignBalance - isnull(sum(ForeignAmount),0) From t_InwardClearing                
   Where (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and  (AccountType = 'G') and TransactionType = 'D'                
   and status <> 'R'                
                
   Select @GLForeignBalance = @GLForeignBalance + isnull(sum(ForeignAmount),0) From t_InwardClearing                
   Where (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (AccountType = 'G') and TransactionType = 'C'                
   and status <> 'R'                
                
                
--t_ATM_CashTransaction                
    SELECT @GLForeignBalance = @GLForeignBalance + isnull(sum(ForeignAmount),0)                
    FROM t_ATM_CashTransaction                
                 WHERE OurBranchID = @OurBranchID AND GLID = @AccountID                       
AND Supervision = 'C' AND TrxType = 'C'                
                
    SELECT @GLForeignBalance = @GLForeignBalance - isnull(sum(ForeignAmount),0)                
    FROM t_ATM_CashTransaction                
    WHERE OurBranchID = @OurBranchID AND GLID = @AccountID                       
AND Supervision = 'C' AND TrxType = 'D'                
                    
--'t_ATM_TransferTransaction                
    SELECT @GLForeignBalance = @GLForeignBalance + isnull(sum(ForeignAmount),0)                
    FROM t_ATM_TransferTransaction                
    WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                       
   AND Supervision = 'C' AND AccountType ='G' AND TrxType = 'C'                        
                
    SELECT @GLForeignBalance = @GLForeignBalance - isnull(sum(ForeignAmount),0)                
        FROM t_ATM_TransferTransaction              
    WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                       
   AND Supervision = 'C' AND AccountType ='G' AND TrxType = 'D'                
                
--'t_OnLine_CashTransaction                
    SELECT  @GLForeignBalance = @GLForeignBalance + isnull(sum(ForeignAmount),0)                
    FROM t_OnLineCashTransaction                
    WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                           
AND Supervision <> 'R' AND AccountType ='G' AND TrxType = 'C'                
                
    SELECT  @GLForeignBalance = @GLForeignBalance - isnull(sum(ForeignAmount),0)                
    FROM t_OnLineCashTransaction                
    WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                 
AND Supervision <> 'R' AND AccountType ='G' AND TrxType = 'D'                
                
   Select '2' as RetStatus,@GLForeignBalance,@AccountName                
  END                
 END