﻿CREATE procedure [dbo].[spc_WHTaxAmount]
(
 @OurBranchID Varchar(30),
 @Amount money=0,
 @mAccountID  Varchar(30),
 @mProductID  Varchar(30)='',
 @BranchType Char(1)='I'
)

AS

SET NOCOUNT ON

EXEC spc_GetCashParameterNew2 @OurBranchID, @Amount, @mAccountID, @mProductID

select AccountID, SUM(ChargesAmount) AS WHTax from temp_DailyTaxCalc GROUP BY AccountID