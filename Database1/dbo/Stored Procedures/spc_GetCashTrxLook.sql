﻿CREATE PROCEDURE [dbo].[spc_GetCashTrxLook]      
(        
      @OurBranchID       Varchar(30),        
      @OperatorID        Varchar(30)='',        
      @AccountID         Varchar(30)='',        
        
      @TrxTypeS           Char(1) = 'C',        
      @TrxTypeE           Char(1) = 'D',        
        
      @StatusS            Char(1)='U',        
      @StatusE            Char(1)='U',        
        
      @ScrollNoS         Numeric(18,0)=0,        
      @ScrollNoE         Numeric(18,0)=999999999,        
      @AmountS           Numeric(18,0)=0,        
      @AmountE           Numeric(18,0) = 9999999999999,        
        
      @ChequeIDS            Varchar(11)='00000000000',        
      @ChequeIDE            Varchar(11)='v' --'99999999999'        
)        
        
AS        
    
   set nocount on    
       
-- 1-            --Get Un-Supervised Trx.         
   IF @OperatorID = '' AND @AccountID = '' AND @StatusS ='U'        
      BEGIN        
            IF (SELECT count(ScrollNo) FROM t_CashTransactionModel        
               WHERE OurBranchID = @OurBranchID AND ScrollNo >= @ScrollNoS AND ScrollNo <= @ScrollNoE        
                                 AND Amount >= @AmountS AND Amount <= @AmountE        
                                 AND Not Supervision IN ('C','R') ) > 0        
               BEGIN        
                  SELECT '1' as RetStatus, ScrollNo, AccountID, AccountName, ProductID, CurrencyID, AccountType,         
                               TrxType,ChequeID, ChequeDate, Amount, ForeignAmount,         
                               GLID, Supervision, OperatorID, IsLocalCurrency,SupervisorID        
                           FROM t_CashTransactionModel                     
                           WHERE OurBranchID = @OurBranchID         
                                 AND ScrollNo >= @ScrollNoS AND ScrollNo <= @ScrollNoE        
                                 AND Amount >= @AmountS AND Amount <= @AmountE        
                                 AND Not Supervision IN ('C','R')        
               END        
            ELSE        
               BEGIN        
                  SELECT '0' AS RetStatus        
                  RETURN        
               END        
      END        
        
   ELSE IF @OperatorID <> '' AND @AccountID = '' AND @StatusS ='U'        
      BEGIN        
            IF (SELECT count(ScrollNo) FROM t_CashTransactionModel        
               WHERE OurBranchID = @OurBranchID AND ScrollNo >= @ScrollNoS AND ScrollNo <= @ScrollNoE        
                                 AND Amount >= @AmountS AND Amount <= @AmountE        
                                 AND Not Supervision IN ('C','R') AND OperatorID = @OperatorID) > 0        
               BEGIN        
                  SELECT '1' as RetStatus, ScrollNo, AccountID, AccountName, ProductID, CurrencyID, AccountType,         
                               TrxType,ChequeID, ChequeDate, Amount, ForeignAmount,         
                               GLID, Supervision, OperatorID, IsLocalCurrency,SupervisorID        
                           FROM t_CashTransactionModel                     
                           WHERE OurBranchID = @OurBranchID         
                                 AND ScrollNo >= @ScrollNoS AND ScrollNo <= @ScrollNoE        
                                 AND Amount >= @AmountS AND Amount <= @AmountE        
                                 AND Not Supervision IN ('C','R') AND OperatorID = @OperatorID        
               END        
            ELSE        
               BEGIN        
                  SELECT '0' AS RetStatus        
                  RETURN        
               END        
      END        
        
-- 2-        
   ELSE IF @OperatorID = '' AND @AccountID = ''        
      BEGIN        
            IF (SELECT count(ScrollNo) FROM t_CashTransactionModel        
               WHERE OurBranchID = @OurBranchID AND ScrollNo >= @ScrollNoS AND ScrollNo <= @ScrollNoE        
                                 AND Amount >= @AmountS AND Amount <= @AmountE         
                                 AND ChequeID >= @ChequeIDS AND ChequeID <= @ChequeIDE        
               AND TrxType >= @TrxTypeS AND TrxType <= @TrxTypeE        
                                 AND Supervision >= @StatusS AND Supervision <= @StatusE ) > 0        
   BEGIN        
                  SELECT '1' as RetStatus, ScrollNo, AccountID, AccountName, ProductID, CurrencyID, AccountType,         
          TrxType,ChequeID, ChequeDate, Amount, ForeignAmount,         
                               GLID, Supervision, OperatorID, IsLocalCurrency,SupervisorID        
                           FROM t_CashTransactionModel                     
                           WHERE OurBranchID = @OurBranchID         
                                 AND ScrollNo >= @ScrollNoS AND ScrollNo <= @ScrollNoE        
                                 AND Amount >= @AmountS AND Amount <= @AmountE                
                                 AND ChequeID >= @ChequeIDS AND ChequeID <= @ChequeIDE        
                                 AND TrxType >= @TrxTypeS AND TrxType <= @TrxTypeE        
                                 AND Supervision >= @StatusS AND Supervision <= @StatusE        
        
               END        
            ELSE        
               BEGIN        
                  SELECT '0' AS RetStatus        
                  RETURN        
               END        
      END        
        
-- 3-        
   ELSE IF @OperatorID = ''        
      BEGIN        
            IF (SELECT count(ScrollNo) FROM t_CashTransactionModel        
               WHERE OurBranchID = @OurBranchID AND ScrollNo >= @ScrollNoS AND ScrollNo <= @ScrollNoE        
                                 AND Amount >= @AmountS AND Amount <= @AmountE         
                                 AND ChequeID >= @ChequeIDS AND ChequeID <= @ChequeIDE        
                                 AND TrxType >= @TrxTypeS AND TrxType <= @TrxTypeE        
                                 AND Supervision >= @StatusS AND Supervision <= @StatusE        
                                 AND AccountID = @AccountID) > 0        
               BEGIN        
                  SELECT '1' as RetStatus, ScrollNo, AccountID, AccountName, ProductID, CurrencyID, AccountType,         
                               TrxType,ChequeID, ChequeDate, Amount, ForeignAmount,         
                               GLID, Supervision, OperatorID, IsLocalCurrency,SupervisorID        
                           FROM t_CashTransactionModel                     
                           WHERE OurBranchID = @OurBranchID         
                                 AND ScrollNo >= @ScrollNoS AND ScrollNo <= @ScrollNoE        
                                 AND Amount >= @AmountS AND Amount <= @AmountE            
                                 AND ChequeID >= @ChequeIDS AND ChequeID <= @ChequeIDE            
                                 AND TrxType >= @TrxTypeS AND TrxType <= @TrxTypeE        
                                 AND Supervision >= @StatusS AND Supervision <= @StatusE        
                                 AND AccountID = @AccountID        
               END        
            ELSE        
               BEGIN        
                  SELECT '0' AS RetStatus        
                  RETURN        
               END        
      END        
--4-        
   ELSE IF @AccountID = ''        
      BEGIN        
            IF (SELECT count(ScrollNo) FROM t_CashTransactionModel        
               WHERE OurBranchID = @OurBranchID AND ScrollNo >= @ScrollNoS AND ScrollNo <= @ScrollNoE        
                                 AND Amount >= @AmountS AND Amount <= @AmountE         
                                 AND ChequeID >= @ChequeIDS AND ChequeID <= @ChequeIDE        
                                 AND TrxType >= @TrxTypeS AND TrxType <= @TrxTypeE        
                                 AND Supervision >= @StatusS AND Supervision <= @StatusE         
      AND OperatorID = @OperatorID) > 0        
               BEGIN        
                  SELECT '1' as RetStatus, ScrollNo, AccountID, AccountName, ProductID, CurrencyID, AccountType,         
                               TrxType,ChequeID, ChequeDate, Amount, ForeignAmount,         
                               GLID, Supervision, OperatorID, IsLocalCurrency,SupervisorID        
                           FROM t_CashTransactionModel                     
                           WHERE OurBranchID = @OurBranchID         
                                 AND ScrollNo >= @ScrollNoS AND ScrollNo <= @ScrollNoE        
                                 AND Amount >= @AmountS AND Amount <= @AmountE                
                                 AND ChequeID >= @ChequeIDS AND ChequeID <= @ChequeIDE        
                                 AND TrxType >= @TrxTypeS AND TrxType <= @TrxTypeE        
                                 AND Supervision >= @StatusS AND Supervision <= @StatusE           
                                 AND OperatorID = @OperatorID        
               END        
            ELSE        
               BEGIN        
                  SELECT '0' AS RetStatus        
                  RETURN        
               END        
      END        
        
   ELSE         
      BEGIN        
            IF (SELECT count(ScrollNo) FROM t_CashTransactionModel        
               WHERE OurBranchID = @OurBranchID AND ScrollNo >= @ScrollNoS AND ScrollNo <= @ScrollNoE        
                                 AND Amount >= @AmountS AND Amount <= @AmountE         
                                 AND ChequeID >= @ChequeIDS AND ChequeID <= @ChequeIDE        
                                 AND TrxType >= @TrxTypeS AND TrxType <= @TrxTypeE        
                                 AND Supervision >= @StatusS AND Supervision <= @StatusE         
                                 AND AccountID = @AccountID AND OperatorID = @OperatorID) > 0        
               BEGIN        
                  SELECT '1' as RetStatus, ScrollNo, AccountID, AccountName, ProductID, CurrencyID, AccountType,         
                               TrxType,ChequeID, ChequeDate, Amount, ForeignAmount,         
                               GLID, Supervision, OperatorID, IsLocalCurrency,SupervisorID        
                           FROM t_CashTransactionModel                     
                           WHERE OurBranchID = @OurBranchID         
                                 AND ScrollNo >= @ScrollNoS AND ScrollNo <= @ScrollNoE        
                                 AND Amount >= @AmountS AND Amount <= @AmountE                
                                 AND ChequeID >= @ChequeIDS AND ChequeID <= @ChequeIDE        
                                 AND TrxType >= @TrxTypeS AND TrxType <= @TrxTypeE        
                                 AND Supervision >= @StatusS AND Supervision <= @StatusE           
                                 AND AccountID = @AccountID        
                                 AND OperatorID = @OperatorID        
               END        
            ELSE        
               BEGIN        
                  SELECT '0' AS RetStatus        
                  RETURN        
               END        
      END