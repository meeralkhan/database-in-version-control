﻿CREATE PROCEDURE [dbo].[spc_AccountBalanceHistory]
(
  @OurBranchID varchar(30)
)
AS

SET NOCOUNT ON

Declare @Wdate datetime
Select @Wdate = WorkingDate from t_Last where OurBranchID = @OurBranchID

INSERT INTO t_AccountBalanceHistory (wDate,OurBranchID,AccountID,ProductID,AccountName,ClearBalance,LocalClearBalance,Effects,LocalEffects,Limit)
Select @wDate,OurBranchID,AccountID,ProductID,[Name],ClearBalance,LocalClearBalance,Effects,LocalEffects,Limit From t_AccountBalance
WHERE OurBranchID = @OurBranchID

select 'Ok' AS RetStatus