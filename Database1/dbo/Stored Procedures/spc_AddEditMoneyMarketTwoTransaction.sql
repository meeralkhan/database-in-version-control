﻿CREATE Procedure [dbo].[spc_AddEditMoneyMarketTwoTransaction]
(  
  @DealNo varchar (30)='',  
  @DealNoS varchar (30)='',  
  @OurBranchID varchar (30),  
  @AccountID varchar (30)='',  
  @AccountIDS varchar (30)='',  
  @SecurityIssue varchar (30)='',  
  @Amount  money=0,  
  @InterestRate Decimal(18,6)=0,  
  @InterestAmount money=0,  
  @DealDate datetime,  
  @MaturityDate datetime,  
  @BrokerCode varchar (30),  
  @SettlementDate datetime,  
  @BrokerageAmount money=0,  
  @TransactionID money=0,  
  @ParentDealType varchar(2)='',  
  @TypeOfTransaction varchar(2)='',  
  @DealDescription varchar(100)='',  
  @Status varchar (1)='',  
  @SupervisionStatus varchar(1)='',    
  @OperatorID varchar(30)='',    
  @SupervisorID varchar(30)='',    
  @AccrualRemarks varchar (100)='',  
  @Price1 Money=0,  
  @CurrencyID varchar (30)='',
  @FXAmount  money=0,        
  @ExchangeRate Numeric(18,6)=0,
  
  @IsWePayAccGL char(1)='',        
  @WePayToAccount varchar(30)='',        
  @WeRecvAccount varchar(30)='',        
  @OurNostroAccount varchar(30)='',        
  @TheirNostroAccount varchar(30)='',        
  @OurNostroDescription varchar(100)='',        
  @TheirNostroDescription varchar(100)='',        
  
  @ClientID varchar(30)='N/A',  
  @RepoInterestRate  money=0,  
  @IsPostedGL char(1)=0,  
  @AccrualAppliedUpto datetime='',  
  @CalculateAccrualUpto datetime='',  
  @Capital money=0,  
  @CapInt money=0,  
  @IsPostForwardDeal char(1)='0',  
  
  @Tenor money=0,  
  @Price2 money=0,  
  @RND int=0,  
  @CouponRate decimal(18,6)=0,  
  
  @TaxRate money=0,
  @TaxAmount money=0,
  
  @AmountS  money=0,  
  @InterestRateS Decimal(18,6)=0,  
  @InterestAmountS money=0,  
  @DealDescriptionS varchar(100)='',  
  @AccrualRemarksS varchar (100)='',  
  @TypeOfTransactionS varchar(2)='',  
  @ClientIDS varchar(30)='N/A',  
  @CapitalS money=0,  
  @CapIntS money=0,  
  @BrokerCodeS varchar (30)=0,  
  @BrokerageAmountS money=0,  
  
  @PremiumAmount money=0,        
  @InterestPaid money=0,        
  @IsPremiumPaid char(1)='1',        
  
  @IsMultiSecurity char(1)='0',  
  @IsVerify int=0,  
  @VerifierName as Varchar(100)='',  
  
  @SBPPrice money=0,        
  @PDName Varchar(100)='',        
  
  @SGLInFavorOf varchar(30)='',
  @NewRecord bit = 1  
  
)
  
AS

SET NOCOUNT ON  
  
 IF (@NewRecord=1)   
  
  BEGIN   
  
   DECLARE @MaxDeal As Integer  
   DECLARE @Dealstr  As varchar(15)  
   DECLARE @DealstrS  As varchar(15)  
   DECLARE @MaxTrNo As Integer  
  
   SELECT @MaxTrNo=IsNull(max(TransactionID) + 1,1) from t_moneymarketnew  
  
   IF @TypeOFTransaction='RE' AND @TypeOFTransactionS='CL'  
   
    BEGIN  
  
     SELECT @MaxDeal=Isnull(max(Convert(integer,Substring(DealNo,patindex('%/%',Substring(Replace(DealNo,'-','/'),3,len(dealno)))+3,len(dealno))))+1,1) from t_moneymarketnew  
     WHERE TypeOfTransaction NOT IN ('FL','FB','PL') --TypeOfTransaction='RE'  
    
     SET @Dealstr = 'REPO/' +  cast(@MaxDeal as varchar(15))  
  
  
     SELECT @MaxDeal=Isnull(max(Convert(integer,Substring(DealNo,patindex('%/%',Substring(Replace(DealNo,'-','/'),3,len(dealno)))+3,len(dealno))))+2,1) from t_moneymarketnew  
     WHERE TypeOfTransaction NOT IN ('FL','FB','PL')--TypeOfTransaction='CL' OR TypeOfTransaction='CB'  
   
     SET @DealstrS = 'CALL/' +  cast(@MaxDeal as varchar(15))  
    END  
  
  
   ELSE IF @TypeOFTransaction='RR' AND @TypeOFTransactionS='CB'  
  
    BEGIN  
     SELECT @MaxDeal=Isnull(max(Convert(integer,Substring(DealNo,patindex('%/%',Substring(Replace(DealNo,'-','/'),3,len(dealno)))+3,len(dealno))))+1,1) from t_moneymarketnew  
     WHERE TypeOfTransaction NOT IN ('FL','FB','PL')--TypeOfTransaction='RR'  
   
     SET @Dealstr = 'REV/' +  cast(@MaxDeal as varchar(15))  
  
  
     SELECT @MaxDeal=Isnull(max(Convert(integer,Substring(DealNo,patindex('%/%',Substring(Replace(DealNo,'-','/'),3,len(dealno)))+3,len(dealno))))+2,1) from t_moneymarketnew  
     WHERE TypeOfTransaction NOT IN ('FL','FB','PL') --TypeOfTransaction='CL' OR TypeOfTransaction='CB'  
   
     SET @DealstrS = 'CALL/' +  cast(@MaxDeal as varchar(15))  
    END  
  
  
  
  
   INSERT INTO t_MoneyMarketNEW  
    (OurBranchID, AccountID,SecurityIssue, DealNo, Amount, InterestRate,InterestAmount, DealDate, MaturityDate,  
    BrokerCode,BrokerageAmount,SettlementDate,ParentDealType,TypeofTransaction,DealDescription,TransactionID,  
    Status,SupervisionStatus,OperatorID,SupervisorID, AccrualRemarks,Price1,ClientID,CurrencyID,FXAmount,ExchangeRate,RepoInterestRate,IsPostedGL,
    AccrualAppliedUpto,CalculateAccrualUpto,Capital,CapInt,IsPostForwardDeal,TaxRate,TaxAmount,  
    IsWePayAccGL,WePayToAccount,WeRecvAccount, OurNostroAccount, TheirNostroAccount, OurNostroDescription, TheirNostroDescription,
    Tenor,PremiumAmount, InterestPaid, IsPremiumPaid, IsMultiSecurity,Price2,RND,CouponRate,SBPPrice,PDName,SGLInFavorOf,IsVerify,VerifierName)  
   
   VALUES     
    (@OurBranchID, @AccountID, @SecurityIssue, @Dealstr, @Amount, @InterestRate, @InterestAmount, @DealDate, @MaturityDate,  
    @BrokerCode, @BrokerageAmount,@SettlementDate,@ParentDealType,@TypeOfTransaction, @DealDescription,@MaxTrNo,  
    @Status, @SupervisionStatus, @OperatorID,@SupervisorID, @AccrualRemarks,@Price1,@ClientID,@CurrencyID,@FXAmount,@ExchangeRate,@RepoInterestRate,
    @IsPostedGL,@AccrualAppliedUpto,@CalculateAccrualUpto,@Capital,@CapInt,@IsPostForwardDeal,@TaxRate,@TaxAmount,  
    @IsWePayAccGL,@WePayToAccount,@WeRecvAccount,@OurNostroAccount,@TheirNostroAccount,@OurNostroDescription,@TheirNostroDescription,
    @Tenor,@PremiumAmount, @InterestPaid, @IsPremiumPaid, @IsMultiSecurity,@Price2,@RND,@CouponRate,@SBPPrice,@PDName,@SGLInFavorOf,@IsVerify,@VerifierName)   
  
     
   INSERT INTO t_MoneyMarketNEW  
    (OurBranchID, AccountID,SecurityIssue,DealNo, Amount, InterestRate,InterestAmount, DealDate, MaturityDate,  
    BrokerCode,BrokerageAmount,SettlementDate,ParentDealType,TypeofTransaction,DealDescription,TransactionID,  
    Status,SupervisionStatus,OperatorID,SupervisorID, AccrualRemarks,Price1,ClientID,CurrencyID,FXAmount,ExchangeRate,RepoInterestRate,IsPostedGL,
    AccrualAppliedUpto,CalculateAccrualUpto,Capital,CapInt,IsPostForwardDeal,TaxRate,TaxAmount,  
    IsWePayAccGL,WePayToAccount,WeRecvAccount, OurNostroAccount, TheirNostroAccount, OurNostroDescription, TheirNostroDescription,
    Tenor,PremiumAmount, InterestPaid, IsPremiumPaid, IsMultiSecurity,Price2,RND,CouponRate,SBPPrice,PDName,IsVerify,VerifierName)  
   
   VALUES     
    (@OurBranchID, @AccountIDS,'', @DealstrS, @AmountS, @InterestRateS, @InterestAmountS, @DealDate, @MaturityDate,  
    @BrokerCodeS, @BrokerageAmountS,@SettlementDate,@ParentDealType,@TypeOfTransactionS, @DealDescriptionS,@MaxTrNo,  
    @Status, @SupervisionStatus, @OperatorID,@SupervisorID, @AccrualRemarksS,0,@ClientIDS,@CurrencyID,@FXAmount,@ExchangeRate,0,
    @IsPostedGL,@AccrualAppliedUpto,@CalculateAccrualUpto,@CapitalS,@CapIntS,@IsPostForwardDeal,@TaxRate,@TaxAmount,  
    @IsWePayAccGL,@WePayToAccount,@WeRecvAccount,@OurNostroAccount,@TheirNostroAccount,@OurNostroDescription,@TheirNostroDescription,
    @Tenor,@PremiumAmount, @InterestPaid, @IsPremiumPaid,'0',@Price2,@RND,@CouponRate,@SBPPrice,@PDName,@IsVerify,@VerifierName)   
    
    select @DealNo = @Dealstr, @DealNoS = @DealstrS
    
  END  
  
 ELSE  
  
  BEGIN   
   UPDATE t_MoneyMarketNEW SET  
  
    Amount=@Amount , InterestRate=@InterestRate,InterestAmount=@InterestAmount,   
    MaturityDate=@MaturityDate,BrokerCode=@BrokerCode,BrokerageAmount=@BrokerageAmount,  
    SettlementDate=@SettlementDate,ParentDealType=@ParentDealType,TypeOfTransaction=@TypeOfTransaction,  
    DealDescription=@DealDescription,Status=@Status,SupervisionStatus=@SupervisionStatus,FXAmount=@FXAmount,ExchangeRate=@ExchangeRate,  
    OperatorID=@OperatorID,SupervisorID=@SupervisorID, AccountID=@AccountID, SecurityIssue=@SecurityIssue,   
    AccrualRemarks=@AccrualRemarks,Price1=@Price1, ClientID=@ClientID, CurrencyID=@CurrencyID, RepoInterestRate=@RepoInterestRate,  
    IsPostedGL=@IsPostedGL,AccrualAppliedUpto=@AccrualAppliedUpto,CalculateAccrualUpto=@CalculateAccrualUpto, TaxRate = @TaxRate, TaxAmount = @TaxAmount,
    Capital=@Capital,CapInt=@CapInt, IsPostForwardDeal=@IsPostForwardDeal, Tenor=@Tenor,IsMultiSecurity=@IsMultiSecurity,  
    IsWePayAccGL =@IsWePayAccGL,WePayToAccount=@WePayToAccount, WeRecvAccount=@WeRecvAccount, OurNostroAccount=@OurNostroAccount, 
    TheirNostroAccount=@TheirNostroAccount, OurNostroDescription=@OurNostroDescription, TheirNostroDescription=@TheirNostroDescription,
    PremiumAmount=@PremiumAmount, InterestPaid=@InterestPaid,IsPremiumPaid=@IsPremiumPaid, SBPPrice=@SBPPrice, PDName=@PDName, 
    Price2=@Price2, SGLInFavorOf = @SGLInFavorOf, CouponRate=@CouponRate,RND=@RND,IsVerify=@IsVerify, VerifierName = VerifierName + '+' + @VerifierName  
  
   WHERE OurBranchID=@OurBranchID AND DealNo=@DealNo  
  
   UPDATE t_MoneyMarketNEW SET  
  
    Amount=@AmountS , InterestRate=@InterestRateS,InterestAmount=@InterestAmountS,   
    MaturityDate=@MaturityDate,BrokerCode=@BrokerCodeS,BrokerageAmount=@BrokerageAmountS,  
    SettlementDate=@SettlementDate,ParentDealType=@ParentDealType,TypeOfTransaction=@TypeOfTransactionS,  
    DealDescription=@DealDescriptionS,Status=@Status,SupervisionStatus=@SupervisionStatus ,  
    OperatorID=@OperatorID,SupervisorID=@SupervisorID, AccountID=@AccountIDS,FXAmount=@FXAmount,ExchangeRate=@ExchangeRate,  
    AccrualRemarks=@AccrualRemarksS, ClientID=@ClientIDS, CurrencyID=@CurrencyID,IsPostedGL=@IsPostedGL, TaxRate = @TaxRate, TaxAmount = @TaxAmount,
    AccrualAppliedUpto=@AccrualAppliedUpto,CalculateAccrualUpto=@CalculateAccrualUpto,  
    Capital=@CapitalS,CapInt=@CapIntS, IsPostForwardDeal=@IsPostForwardDeal,Tenor=@Tenor,IsMultiSecurity=@IsMultiSecurity,  
    IsWePayAccGL =@IsWePayAccGL,WePayToAccount=@WePayToAccount, WeRecvAccount=@WeRecvAccount, OurNostroAccount=@OurNostroAccount, 
    TheirNostroAccount=@TheirNostroAccount, OurNostroDescription=@OurNostroDescription, TheirNostroDescription=@TheirNostroDescription,
    PremiumAmount=@PremiumAmount, InterestPaid=@InterestPaid,IsPremiumPaid=@IsPremiumPaid, SBPPrice=@SBPPrice, PDName=@PDName, 
    Price2=@Price2, CouponRate=@CouponRate,RND=@RND,IsVerify=@IsVerify, VerifierName = VerifierName + '+' + @VerifierName  
      
   WHERE OurBranchID=@OurBranchID AND DealNo=@DealNoS  
  
  END  
  
  select 'Ok' AS RetStatus, @DealNo DealNo, @DealNoS DealNoS