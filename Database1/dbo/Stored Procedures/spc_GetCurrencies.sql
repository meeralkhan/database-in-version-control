﻿CREATE PROCEDURE [dbo].[spc_GetCurrencies] 
( 
 @OurBranchID varchar(4), 
 @CurrencyID varchar(4) = '' 
) 
AS 
 
set nocount on 
 
Begin Try 
 IF (@CurrencyID <> '') 
 BEGIN 
  IF Exists (SELECT Description FROM t_Currencies WHERE OurBranchID = @OurBranchID AND CurrencyID = @CurrencyID ) 
  begin 
   select 'Ok' As ReturnStatus, * from t_Currencies WHERE OurBranchID = @OurBranchID AND CurrencyID = @CurrencyID 
  end 
  ELSE 
  begin 
   raiserror('InvalidCurrency', 15, 1) 
  end 
 END 
 ELSE 
 BEGIN
  SELECT 'Ok' As ReturnStatus, CurrencyId, Description FROM t_Currencies WHERE t_Currencies.OurBranchID = @OurBranchID 
 END 
End Try 
Begin Catch 
 select 'Error' As ReturnStatus, Error_Message() as ReturnMessage 
End Catch