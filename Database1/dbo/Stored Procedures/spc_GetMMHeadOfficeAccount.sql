﻿CREATE PROCEDURE [dbo].[spc_GetMMHeadOfficeAccount]  
(  
  @OurBranchID varchar(30),  
  @SerialID varchar(30)  
)  
  
 AS  
 
 SELECT '1' AS IsGLAccount, p.AccountID, g.Description, g.CurrencyID
 FROM t_GLParameters p
 INNER JOIN t_GL g ON p.OurBranchID = g.OurBranchID AND p.AccountID = g.AccountID
 WHERE p.OurBranchID = @OurBranchID AND p.SerialID = @SerialID