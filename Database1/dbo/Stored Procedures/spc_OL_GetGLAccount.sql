﻿CREATE PROCEDURE [dbo].[spc_OL_GetGLAccount]    
(              
 @OurBranchID varchar(30),              
 @AccountID varchar(30),              
 @TrxType varchar(5)='',              
 @OperatorID varchar(30)='',               
 @ModuleType char(1)=''    
)              
              
AS              
 SET NOCOUNT ON              
              
 DECLARE @AccountName as Varchar(75)              
 DECLARE @AccountClass as Char(1)              
 DECLARE @CurrencyID as Varchar(4)              
 DECLARE @ClearBalance as Money              
 DECLARE @ShadowBalance as Money              
              
 DECLARE @DayDepositBal as money                
 DECLARE @DayDrawableBal as money                
 DECLARE @DayDepositBalUtilize as money                
 DECLARE @DayDrawableBalUtilize as money                
              
 DECLARE @IsPosting as Bit              
 DECLARE @LocalCurrency as Varchar(4)              
       
 SELECT @LocalCurrency =UPPER(LTRIM(RTRIM(LocalCurrency))) From t_GlobalVariables where OurBranchID = @OurBranchID  
     
 SELECT @TrxType = 'B', @DayDepositBal = 999999999999, @DayDrawableBal = 999999999999, @DayDepositBalUtilize = 0, @DayDrawableBalUtilize = 0    
     
 IF (Select Count(AccountID) From t_GL WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID) > 0               
  BEGIN              
         
   SELECT @AccountName=Description, @ShadowBalance=ShadowBalance, @ClearBalance = CASE CurrencyID WHEN @LocalCurrency THEN Balance ELSE      
   ForeignBalance END, @CurrencyID= UPPER(CurrencyID), @AccountClass = AccountClass, @IsPosting = IsPosting      
   FROM t_GL              
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID      
       
   IF @AccountClass <> 'P'              
   BEGIN              
    SELECT '6'  AS RetStatus --Account Class Not Define Posting               
    RETURN(1)                     
   END              
         
   IF @IsPosting = 0                 
   BEGIN              
    SELECT '7'  AS RetStatus --Posting Not Allowed              
    RETURN(1)                     
   END              
         
   IF (SELECT Count(*) From t_Products              
    WHERE OurBranchID = @OurBranchID AND (GLControl=@AccountID OR GLDebitBalance = @AccountID))>0               
   BEGIN              
    SELECT '9'  AS RetStatus --Posting Not Allowed IN Control Account              
    RETURN(1)                     
   END              
       
   SELECT '1' as RetStatus , @ClearBalance as ClearBalance,                
     @ShadowBalance as ShadowBalance,@AccountName as Name,      
     'GL' ProductID, 'General Ledger' ProductName, '' Reminder,      
     0 Effects, 0 FreezeAmount, 0 Limit, 0 ProductMinBalance, 0 AverageBalance,      
     @CurrencyID as CurrencyID, @TrxType TrxType,      
     @DayDepositBal DayDepositBal, @DayDrawableBal DayDrawableBal,      
     @DayDepositBalUtilize DayDepositBalUtilize, @DayDrawableBalUtilize DayDrawableBalUtilize      
           
  END              
 ELSE              
  BEGIN             
   SELECT 0 AS RetStatus --Account Does Not Exists              
   RETURN(0)              
  END