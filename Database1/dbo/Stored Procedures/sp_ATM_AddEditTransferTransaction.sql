﻿
CREATE PROCEDURE [dbo].[sp_ATM_AddEditTransferTransaction]
(      
 @OurBranchID       varchar(30),       
 @WithdrawlBranchID      varchar(30)='',       
 @ATMID       varchar(30),       
 @AccountID           varchar(30),      
 @Amount        money,      
 @USDAmount       money=0,      
 @OtherCurrencyAmount      money=0,      
 @USDRate           money=0,      
 @Supervision       char(1),       
 @RefNo        varchar(36),      
 @PHXDate        datetime,      
 @MerchantType      varchar(30),      
 @OurIMD       varchar(30),      
 @AckInstIDCode      varchar(30),      
 @Currency       varchar(3),      
 @NameLocation      varchar(40),      
 @MCC        varchar(30),      
 @TrxDesc        varchar(30)='',
 @NewRecord       bit=1      
)      
AS      
 DECLARE @BankShortName as varchar(30)      
 DECLARE @mScrollNo as int      
 DECLARE @mSerialNo as smallint      
 DECLARE @mAccountID as varchar(30)      
 DECLARE @cAccountName as varchar(50)      
 DECLARE @mAccountName as varchar(50)      
 DECLARE @mAccountType as char      
 DECLARE @mProductID as varchar(30)      
 DECLARE @mCurrencyID as varchar(30)      
 DECLARE @mIsLocalCurrency as bit      
  SET @mIsLocalCurrency=1       
 DECLARE @mwDate as datetime      
 DECLARE @mTrxType as char      
 DECLARE @mGLID as varchar(30)      
  SET @mGLID = ''       
 DECLARE @mForeignAmount as money      
  SET @mForeignAmount=0       
 DECLARE @mExchangeRate as money      
  SET @mExchangeRate=1       
 DECLARE @mDescriptionID as varchar(3)      
  SET @mDescriptionID='000'       
 DECLARE @mDescription as varchar(255)      
  SET @mDescription = ''  
 DECLARE @STAN as varchar(30)      
 
  BEGIN       
   SET @BankShortName = 'N/A'      
   SELECT @BankShortName = BankShortName From t_ATM_Banks      
   WHERE (BankIMD = @OurIMD)      
   IF @MerchantType <> '0000'       
--    SET @mDescription='ATM(' + @ATMID + ') IBK-CSH WDRL AT BK.' + @BankShortName  + ' (BR.' + @WithdrawlBranchID + ')'       
    IF @Currency = '586'      
     BEGIN      
      SET @mDescription='ATM(' + @ATMID + ') IBK-CSH WDRL AT ' + @NameLocation       
     END      
    ELSE       
     IF @Currency = '840'      
      BEGIN      
       SET @mDescription='ATM(' + @ATMID + ') IBK-CSH WDRL ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') AT ' + @NameLocation       
      END      
     ELSE       
      BEGIN      
       SET @mDescription='ATM(' + @ATMID + ') IBK-CSH WDRL (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) AT ' + @NameLocation       
      END      
   ELSE       
    SET @mDescription='ATM(' + @ATMID + ') IB-CSH WDRL AT BK.' + @BankShortName  + ' (BR.' + @WithdrawlBranchID + ')'       
  END       
 DECLARE @mDescriptionCharges as varchar(255)      
  BEGIN       
   IF @MerchantType <> '0000'       
--    SET @mDescriptionCharges='ATM(' + @ATMID + ') IBK-CSH WDRL-CHG AT BK.' + @BankShortName  + ' (BR.' + @WithdrawlBranchID + ')'       
    IF @Currency = '586'      
     BEGIN      
      SET @mDescriptionCharges='ATM(' + @ATMID + ') IBK-CSH WDRL-CHG AT ' + @NameLocation       
     END      
    ELSE       
     IF @Currency = '840'      
      BEGIN      
       SET @mDescriptionCharges='ATM(' + @ATMID + ') IBK-CSH WDRL-CHG ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') AT ' + @NameLocation       
      END      
     ELSE       
      BEGIN      
       SET @mDescriptionCharges='ATM(' + @ATMID + ') IBK-CSH WDRL-CHG (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) AT ' + @NameLocation       
      END      
  END       
 DECLARE @mDescriptionH as varchar(255)      
  BEGIN       
   IF @MerchantType <> '0000'       
--    SET @mDescriptionH='ATM(' + @ATMID + ') IBK-CSH WDRL AT BK.' + @BankShortName   + ' (BR.' + @WithdrawlBranchID + ') ' + ' From : A/c # ' + @AccountID      
    IF @Currency = '586'      
     BEGIN      
      SET @mDescriptionH='ATM(' + @ATMID + ') IBK-CSH WDRL AT ' +  + @NameLocation + ' From : A/c # ' + @AccountID      
     END      
    ELSE         
     IF @Currency = '840'      
      BEGIN      
       SET @mDescriptionH='ATM(' + @ATMID + ') IBK-CSH WDRL ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') AT ' +  + @NameLocation + ' From : A/c # ' + @AccountID      
      END      
     ELSE       
      BEGIN      
       SET @mDescriptionH='ATM(' + @ATMID + ') IBK-CSH WDRL (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) AT ' +  + @NameLocation + ' From : A/c # ' + @AccountID      
      END      
   ELSE       
    SET @mDescriptionH='ATM(' + @ATMID + ') IB-CSH WDRL AT BK.' + @BankShortName   + ' (BR.' + @WithdrawlBranchID + ') ' + ' From : A/c # ' + @AccountID      
  END       
 DECLARE @mDescriptionHCharges as varchar(255)      
  BEGIN       
   IF @MerchantType <> '0000'       
--    SET @mDescriptionHCharges='ATM(' + @ATMID + ') IBK-CSH WDRL-CHG AT BK.' + @BankShortName   + ' (BR.' + @WithdrawlBranchID + ') ' + ' From : A/c # ' + @AccountID      
    IF @Currency = '586'      
     BEGIN      
      SET @mDescriptionHCharges='ATM(' + @ATMID + ') IBK-CSH WDRL-CHG AT ' + @NameLocation + ' From : A/c # ' + @AccountID      
     END      
    ELSE       
     IF @Currency = '840'      
      BEGIN      
       SET @mDescriptionHCharges='ATM(' + @ATMID + ') IBK-CSH WDRL-CHG ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') AT ' + @NameLocation + ' From : A/c # ' + @AccountID      
      END      
     ELSE       
      BEGIN      
       SET @mDescriptionHCharges='ATM(' + @ATMID + ') IBK-CSH WDRL-CHG (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) AT ' + @NameLocation + ' From : A/c # ' + @AccountID      
      END      
  END       
 DECLARE @mRemarks as varchar(200)      
 DECLARE @mBankCode as varchar(30)      
  SET @mBankCode='1279'       
 DECLARE @mOperatorID as varchar(30)      
  SET @mOperatorID = 'ATM-' + @ATMID      
 DECLARE @mSupervisorID as varchar(30)      
  SET @mSupervisorID = @OurIMD       
      
--Checking Variables      
 DECLARE @vCount1 as bit      
 DECLARE @vCount2 as bit      
 DECLARE @vCount3 as bit      
 DECLARE @vCount4 as bit      
 DECLARE @vAccountID as varchar(30)      
 DECLARE @vClearBalance as money      
      
 DECLARE @AvailableBalance as money      
 DECLARE @value as varchar(1)      
      
 DECLARE @vClearedEffects as money      
 DECLARE @vFreezeAmount as money      
 Declare @Limit as money      
 DECLARE @vMinBalance as money      
 DECLARE @vIsClose as char      
 DECLARE @vAreCheckBooksAllowed as bit      
 DECLARE @vAllowDebitTransaction as bit      
 DECLARE @vBranchName as varchar(50)      
 DECLARE @Status as int       
      
 DECLARE @mLastEOD as datetime           
 DECLARE @mCONFRETI as datetime      
 DECLARE @mWithdrawlBranchName as varchar(50)
 DECLARE @WithDrawlCharges as money      
 DECLARE @WithDrawlChargesPercentage as money      
 DECLARE @IssuerAccountID as varchar(30)      
 DECLARE @IssuerAccountTitle as varchar(50)      
 DECLARE @IssuerChargesAccountID as varchar(30)      
 DECLARE @IssuerTitle as varchar(50)      
 DECLARE @mRemarksCharges as varchar(200)      
 Declare @BalanceEnquiryCharges as money      
      
 SET NOCOUNT ON      
      
 SELECT @Status = -1      
---------------------      
      
IF @Amount > 0       
      
 BEGIN      
      
   SELECT @mLastEOD=LastEOD,@mwDate=WorkingDate,@mCONFRETI=CONFRETI FROM t_Last WHERE (OurBranchID = @OurBranchID)      
   IF @mLastEOD = @mwDate or @mCONFRETI = '1/1/9999'      
    BEGIN      
            
     SELECT @Status=9  --INVALID_DATE      
     SELECT @Status  as Status      
     RETURN (1)       
    END      
      
   SELECT @mScrollNo=(ISNULL(MAX(ScrollNo),0)+1) FROM t_ATM_TransferTransaction WHERE (OurBranchID = @OurBranchID) and (convert(varchar(10), wDate, 120) = convert(varchar(10), @mwDate, 120)) 
      
------------------------      
------------------------      
--Debit Transaction         
      
      
   SELECT @vCount1=count(*) From t_Account      
   WHERE (OurBranchID = @OurBranchID) AND( AccountID=@AccountID)      
   IF @vCount1=0      
    BEGIN      
            
     SELECT @Status=2  --INVALID_ACCOUNT      
     SELECT @Status  as Status      
     RETURN (1)       
    END      
      
   SET @WithDrawlCharges = 0      
   SET @WithDrawlChargesPercentage = 0      
   IF @MCC = '6010' and @OurIMD = '402581'   --Local POS Cash Transaction      
    BEGIN      
    IF @Currency = '586'       
     BEGIN      
      SELECT @WithDrawlCharges=POSCashTransactionCharges,@WithDrawlChargesPercentage=POSCashTransactionChargesPercentage,      
        @IssuerAccountID=IssuerAccountID,@IssuerAccountTitle=IssuerAccountTitle,      
        @IssuerChargesAccountID=IssuerChargesAccountID,@IssuerTitle=IssuerTitle        
      From t_ATM_SwitchCharges      
      WHERE (MerchantType = '0000')      
      IF @WithDrawlCharges < ((@WithDrawlChargesPercentage/100)*@Amount)      
       BEGIN      
        SET @WithDrawlCharges = ((@WithDrawlChargesPercentage/100)*@Amount)      
       END      
     END      
    ELSE      
     BEGIN      
      SELECT @WithDrawlCharges=ForeignPOSCashTransactionCharges,@WithDrawlChargesPercentage=ForeignATMWithDrawlChargesPercentage,      
        @IssuerAccountID=IssuerAccountID,@IssuerAccountTitle=IssuerAccountTitle,      
        @IssuerChargesAccountID=IssuerChargesAccountID,@IssuerTitle=IssuerTitle        
      From t_ATM_SwitchCharges      
      WHERE (MerchantType = '0000')      
      IF @WithDrawlCharges < ((@WithDrawlChargesPercentage/100)*@Amount)      
       BEGIN      
        SET @WithDrawlCharges = ((@WithDrawlChargesPercentage/100)*@Amount)      
       END      
     END      
    END      
   ELSE IF @MCC = '6010' and @OurIMD <> '402581'   --Local POS Cash Transaction      
    BEGIN      
    IF @Currency = '586'       
     BEGIN      
      SELECT @WithDrawlCharges=POSCashTransactionCharges,@WithDrawlChargesPercentage=POSCashTransactionChargesPercentage,      
        @IssuerAccountID=IssuerAccountID,@IssuerAccountTitle=IssuerAccountTitle,      
        @IssuerChargesAccountID=IssuerChargesAccountID,@IssuerTitle=IssuerTitle        
      From t_ATM_SwitchCharges      
      WHERE (MerchantType = @MerchantType)      
      IF @WithDrawlCharges < ((@WithDrawlChargesPercentage/100)*@Amount)      
       BEGIN      
        SET @WithDrawlCharges = ((@WithDrawlChargesPercentage/100)*@Amount)      
       END      
     END      
    ELSE      
     BEGIN      
      SELECT @WithDrawlCharges=ForeignPOSCashTransactionCharges,@WithDrawlChargesPercentage=ForeignATMWithDrawlChargesPercentage,      
        @IssuerAccountID=IssuerAccountID,@IssuerAccountTitle=IssuerAccountTitle,      
        @IssuerChargesAccountID=IssuerChargesAccountID,@IssuerTitle=IssuerTitle        
      From t_ATM_SwitchCharges      
      WHERE (MerchantType = @MerchantType)      
      IF @WithDrawlCharges < ((@WithDrawlChargesPercentage/100)*@Amount)      
       BEGIN      
        SET @WithDrawlCharges = ((@WithDrawlChargesPercentage/100)*@Amount)      
       END      
     END      
    END      
   ELSE IF @MCC = '6011'  --Other ATM Withdrawl      
    BEGIN      
    IF @Currency = '586'       
     BEGIN      
       SELECT @WithDrawlCharges=WithDrawlCharges,@WithDrawlChargesPercentage=WithDrawlChargesPercentage,      
        @IssuerAccountID=IssuerAccountID,@IssuerAccountTitle=IssuerAccountTitle,      
        @IssuerChargesAccountID=IssuerChargesAccountID,@IssuerTitle=IssuerTitle        
      From t_ATM_SwitchCharges      
      WHERE (MerchantType = @MerchantType)      
      IF @WithDrawlCharges < ((@WithDrawlChargesPercentage/100)*@Amount)      
       BEGIN      
        SET @WithDrawlCharges = ((@WithDrawlChargesPercentage/100)*@Amount)      
       END      
     END      
    ELSE      
     BEGIN      
      SELECT @WithDrawlCharges=ForeignATMWithDrawlCharges,@WithDrawlChargesPercentage=ForeignATMWithDrawlChargesPercentage,      
        @IssuerAccountID=IssuerAccountID,@IssuerAccountTitle=IssuerAccountTitle,      
  @IssuerChargesAccountID=IssuerChargesAccountID,@IssuerTitle=IssuerTitle        
      From t_ATM_SwitchCharges      
      WHERE (MerchantType = @MerchantType)      
      IF @WithDrawlCharges < ((@WithDrawlChargesPercentage/100)*@Amount)      
       BEGIN      
        SET @WithDrawlCharges = ((@WithDrawlChargesPercentage/100)*@Amount)      
       END      
     END      
    END      
   ELSE IF @MCC = '0000' --Loacl ATM Withdrawl      
    BEGIN      
    IF @Currency = '586'       
     BEGIN      
      SELECT @WithDrawlCharges=WithDrawlCharges,@WithDrawlChargesPercentage=WithDrawlChargesPercentage,      
        @IssuerAccountID=IssuerAccountID,@IssuerAccountTitle=IssuerAccountTitle,      
        @IssuerChargesAccountID=IssuerChargesAccountID,@IssuerTitle=IssuerTitle        
      From t_ATM_SwitchCharges      
      WHERE (MerchantType = @MerchantType)      
      IF @WithDrawlCharges < ((@WithDrawlChargesPercentage/100)*@Amount)      
       BEGIN      
        SET @WithDrawlCharges = ((@WithDrawlChargesPercentage/100)*@Amount)      
       END      
     END      
    ELSE      
     BEGIN      
      SELECT @WithDrawlCharges=ForeignATMWithDrawlCharges,@WithDrawlChargesPercentage=ForeignATMWithDrawlChargesPercentage,      
        @IssuerAccountID=IssuerAccountID,@IssuerAccountTitle=IssuerAccountTitle,      
        @IssuerChargesAccountID=IssuerChargesAccountID,@IssuerTitle=IssuerTitle        
      From t_ATM_SwitchCharges      
      WHERE (MerchantType = @MerchantType)      
      IF @WithDrawlCharges < ((@WithDrawlChargesPercentage/100)*@Amount)      
       BEGIN      
        SET @WithDrawlCharges = ((@WithDrawlChargesPercentage/100)*@Amount)      
       END      
     END      
    END      
      
      
   SELECT @cAccountName = B.Name, @mAccountName=B.Name, @mProductID=B.ProductID, @mCurrencyID=P.CurrencyID,  
   @mAccountType='C', @vClearBalance=B.ClearBalance,@vClearedEffects=B.ClearedEffects,@vFreezeAmount=B.FreezeAmount,  
   @vMinBalance=A.MinBalance,@limit=isnull(B.Limit,0),@vIsClose=A.Status, @vAreCheckBooksAllowed=P.AreCheckBooksAllowed,  
   @vAllowDebitTransaction=S.AllowDebitTransaction      
   FROM t_AccountBalance B   
   INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND  B.ProductID = A.ProductID   
   INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND  A.ProductID = P.ProductID   
   LEFT OUTER JOIN t_AccountSpecialCondition S ON B.OurBranchID = S.OurBranchID AND B.AccountID = S.AccountID  
   WHERE (A.OurBranchID = @OurBranchID) AND (A.AccountID = @AccountID)  
    
   IF @vIsClose = 'C' or @vIsClose = 'T' or @vIsClose = 'X' or @vAllowDebitTransaction = 1      
    BEGIN      
            
     SELECT @Status=3  --INACTIVE/CLOSE_ACCOUNT      
     SELECT @Status  as Status      
     RETURN (1)       
    END      
    
     
   IF @Amount >(@vClearBalance-@vClearedEffects+@limit)-@WithDrawlCharges or @Amount >((@vClearBalance-@vClearedEffects +@limit )-@vFreezeAmount-@vMinBalance-@WithDrawlCharges) or @Amount >((@vClearBalance-@vClearedEffects+@limit)-@vMinBalance-@WithDrawlCharges)      
    BEGIN      
            
     SELECT @Status=4  --LOW_BALANCE      
     SELECT @Status  as Status      
     RETURN (1)       
    END      
   IF @vAreCheckBooksAllowed = 0      
    BEGIN      
            
     SELECT @Status=3  --INVALID_PRODUCT      
     SELECT @Status  as Status      
     RETURN (1)       
    END      
   IF @mCurrencyID <> 'PKR'      
    BEGIN      
            
     SELECT @Status=3  --INVALID_CURRENCY      
     SELECT @Status  as Status      
     RETURN (1)       
    END      
      
   Set @mSerialNo = 1      
   Set @mTrxType = 'D'      
   SET @vBranchName = 'N/A'      
   SELECT @vBranchName=Heading2 FROM t_GlobalVariables WHERE (OurBranchID = @OurBranchID)      
      
   SELECT @vCount4=count(*) From t_ATM_Branches      
   WHERE BranchID = @WithdrawlBranchID      
      
   IF @MerchantType <> '0000'       
    BEGIN      
--     Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Transaction From : BK.' + @BankShortName + ' (BR.' + @WithdrawlBranchID + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)      
--     Set @mRemarksCharges='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Charges Transaction From : BK.' + @BankShortName + ' (BR.' + @WithdrawlBranchID + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)      
     IF @Currency = '586'      
      BEGIN      
       Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Transaction From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)      
       Set @mRemarksCharges='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Charges Transaction From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)      
      END        
      ELSE       
      IF @Currency = '840'      
       BEGIN      
        Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)      
        Set @mRemarksCharges='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Charges Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)       
       END      
      ELSE       
       BEGIN      
        Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)      
        Set @mRemarksCharges='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Charges Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)      
       END      
    END      
   ELSE      
    IF @vCount4 > 0       
     BEGIN      
      SET  @mWithdrawlBranchName = 'N/A'      
      SELECT @mWithdrawlBranchName=BranchName FROM t_ATM_Branches      
      WHERE BranchID = @WithdrawlBranchID      
      Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Branch Cash Withdrawl Transaction From : BK.' + @BankShortName +  ' (' + upper(@mWithdrawlBranchName) + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)      
     END      
    --IF @vCount4 = 0           ELSE      
     BEGIN      
      Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Branch Cash Withdrawl Transaction From : BK.' + @BankShortName +  ' (BR.' +  @WithdrawlBranchID + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)      
     END      
	 
	 SET @STAN = RIGHT(@RefNo,6)
      
  INSERT INTO t_ATM_TransferTransaction      
   (      
    ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
   )  
  VALUES      
   (       
    @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@AccountID,@mAccountName,@mAccountType,@mProductID, @mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,@Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID, @mDescription + ' STAN : ' + @STAN,@ATMID,@mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MCC,@TrxDesc,@MerchantType,@STAN
   )      
       
    SELECT @Status=@@ERROR      
      
    IF @Status <> 0        
     BEGIN      
             
      RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR       
      RETURN (5000)      
     END      
--Debit Transaction Switch Charges         
IF @MerchantType <> '0000'  and @WithDrawlCharges > 0      
 BEGIN      
   Set @mSerialNo = 3      
   
   SET @STAN = RIGHT(@RefNo,6)
      
  INSERT INTO t_ATM_TransferTransaction      
   (      
    ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
   )        
      
  VALUES      
   (      
    @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@AccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,@Supervision,@mGLID,@WithDrawlCharges,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescription + ' STAN : ' + @STAN,@ATMID,@mBankCode,@mRemarksCharges,@mOperatorID,@mSupervisorID,@MCC,@TrxDesc+'|INT BR CW CHG',@MerchantType,@STAN
   )      
       
    SELECT @Status=@@ERROR       
      
    IF @Status <> 0       
     BEGIN      
             
      RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR       
      RETURN (5000)      
     END      
 END      
--Credit Transaction         
      
  SELECT @vAccountID=AccountID, @mAccountName=Description,@mProductID='GL', @mCurrencyID=CurrencyID,@mAccountType='G'   
  FROM t_GL       
  WHERE (OurBranchID = @OurBranchID) AND (AccountID =  @IssuerAccountID)      
         
  Set @mSerialNo = 2      
  Set @mTrxType = 'C'      
    
  SET @mDescription = ''  
  
  SET @STAN = RIGHT(@RefNo,6)
      
  INSERT INTO t_ATM_TransferTransaction      
   (      
    ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
   )        
      
  VALUES      
   (      
    @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@vAccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,@Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescription,@ATMID,@mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MCC,@TrxDesc,@MerchantType,@STAN
   )      
      
    SELECT @Status=@@ERROR      
      
    IF @Status <> 0       
     BEGIN      
             
      RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR      
      RETURN (5000)      
     END      
      
--Credit Transaction Switch Charges         
IF @MerchantType <> '0000'  and @WithDrawlCharges > 0      
 BEGIN      
  SELECT @vAccountID=AccountID, @mAccountName=Description,@mProductID='GL', @mCurrencyID=CurrencyID,@mAccountType='G'   
  FROM t_GL       
  WHERE (OurBranchID = @OurBranchID) AND (AccountID =  @IssuerChargesAccountID)      
         
  Set @mSerialNo = 4      
  
  SET @STAN = RIGHT(@RefNo,6)
      
  INSERT INTO t_ATM_TransferTransaction      
   (      
    ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
   )        
      
  VALUES      
      
   (      
    @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@vAccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,@Supervision,@mGLID,@WithDrawlCharges,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescription,@ATMID,@mBankCode,@mRemarksCharges,@mOperatorID,@mSupervisorID,@MCC,@TrxDesc+'|INT BR CW CHG',@MerchantType,@STAN
   )
      
    SELECT @Status=@@ERROR      
      
    IF @Status <> 0       
     BEGIN      
             
      RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR      
      RETURN (5000)      
     END      
 END      
      
    
    
  SELECT @Status = 0,
  @AvailableBalance = (isnull(@limit,0)+(isnull(@vClearBalance,0))-(isnull(@amount,0))-isNull(@WithDrawlCharges,0))*100,     
  @vClearBalance = ((IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0))-IsNull(@Amount,0)-isNull(@WithDrawlCharges,0))*100,@Value='3'  
  
  SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value, @cAccountName AccountName,@mwDate WDate  
  RETURN 0       
    
         
 END      
      
      
------------------------      
else      
  BEGIN      
       
     SELECT @mLastEOD=LastEOD,@mwDate=WorkingDate,@mCONFRETI=CONFRETI FROM t_Last WHERE (OurBranchID = @OurBranchID)      
     IF @mLastEOD = @mwDate or @mCONFRETI = '1/1/9999'      
      BEGIN      
              
       SELECT @Status=9  --INVALID_DATE      
       SELECT @Status  as Status      
       RETURN (1)       
      END      
        
     SELECT @mScrollNo=(ISNULL(MAX(ScrollNo),0)+1) FROM t_ATM_TransferTransaction WHERE (OurBranchID = @OurBranchID) and (convert(varchar(10), wDate, 120) = convert(varchar(10), @mwDate, 120)) 
        
  ------------------------      
  --Debit Transaction         
        
        
     SELECT @vCount1=count(*) From t_Account      
     WHERE (OurBranchID = @OurBranchID) AND( AccountID=@AccountID)      
     IF @vCount1=0      
      BEGIN      
              
       SELECT @Status=2  --INVALID_ACCOUNT      
       SELECT @Status  as Status      
       RETURN (1)       
      END      
        
     IF @MCC <> '0000'      
      BEGIN      
      IF @Currency = '586'       
       BEGIN      
        SELECT   @BalanceEnquiryCharges=BalanceEnquiryCharges,      
        @IssuerChargesAccountID=IssuerChargesAccountID       
      
        From t_ATM_SwitchCharges      
        WHERE (MerchantType = @MerchantType)      
         set @Amount=@BalanceEnquiryCharges      
       END      
      ELSE      
       BEGIN      
        SELECT  @BalanceEnquiryCharges=BalanceEnquiryCharges,      
        @IssuerChargesAccountID=IssuerChargesAccountID       
        From t_ATM_SwitchCharges      
        WHERE (MerchantType = @MerchantType)      
         set @Amount=@BalanceEnquiryCharges      
       END      
      END      
        
        
     SELECT @mAccountName=B.Name, @mProductID=B.ProductID, @mCurrencyID=P.CurrencyID,@mAccountType='C',  
     @vClearBalance=B.ClearBalance,@vClearedEffects=B.ClearedEffects,@vFreezeAmount=B.FreezeAmount,  
     @vMinBalance=A.MinBalance,@vIsClose=A.Status, @vAreCheckBooksAllowed=P.AreCheckBooksAllowed,   
     @vAllowDebitTransaction=S.AllowDebitTransaction        
     FROM t_AccountBalance B   
     INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND  B.ProductID = A.ProductID   
     INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND A.ProductID = P.ProductID   
     LEFT OUTER JOIN t_AccountSpecialCondition S ON B.OurBranchID = S.OurBranchID AND B.AccountID = S.AccountID      
     WHERE  (A.OurBranchID = @OurBranchID) AND (A.AccountID = @AccountID)      
       
     IF @vIsClose = 'C' or @vIsClose = 'T' or @vIsClose = 'X' or @vAllowDebitTransaction = 1      
      BEGIN      
              
       SELECT @Status=3  --INACTIVE/CLOSE_ACCOUNT      
       SELECT @Status  as Status      
       RETURN (1)       
      END      
     IF @Amount >(@vClearBalance-@vClearedEffects)-@WithDrawlCharges or @Amount >((@vClearBalance-@vClearedEffects)-@vFreezeAmount-@vMinBalance-@WithDrawlCharges) or @Amount >((@vClearBalance-@vClearedEffects)-@vMinBalance-@WithDrawlCharges)      
      BEGIN      
              
       SELECT @Status=4  --LOW_BALANCE      
       SELECT @Status  as Status      
       RETURN (1)       
      END      
     IF @vAreCheckBooksAllowed = 0      
      BEGIN      
              
       SELECT @Status=3  --INVALID_PRODUCT      
       SELECT @Status  as Status      
       RETURN (1)       
      END      
     IF @mCurrencyID <> 'PKR'      
      BEGIN      
              
       SELECT @Status=3  --INVALID_CURRENCY      
       SELECT @Status  as Status      
       RETURN (1)       
      END      
        
     Set @mSerialNo = 1      
     Set @mTrxType = 'D'      
     SET @vBranchName = 'N/A'      
     SELECT @vBranchName=Heading2 FROM t_GlobalVariables WHERE (OurBranchID = @OurBranchID)      
        
     SELECT @vCount4=count(*) From t_ATM_Branches      
     WHERE BranchID = @WithdrawlBranchID      
        
     IF @MerchantType <> '0000'       
      BEGIN      
       IF @Currency = '586'      
        BEGIN      
         SET @mDescriptionCharges='ATM(' + @ATMID + ') IBK-BAL ENQ-CHG AT BK.' + @BankShortName  + ' (BR.' + @WithdrawlBranchID + ')'       
         SET @mDescriptionHCharges='ATM(' + @ATMID + ') IBK-BAL ENQ-CHG AT BK.' + @BankShortName   + ' (BR.' + @WithdrawlBranchID + ') ' + ' From : A/c # ' + @AccountID      
      
         Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Transaction From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)      
         Set @mRemarksCharges='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Charges Transaction From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)      
        END        ELSE       
        IF @Currency = '840'      
         BEGIN      
          SET @mDescriptionCharges='ATM(' + @ATMID + ') IBK-BAL ENQ-CHG AT BK.' + @BankShortName  + ' (BR.' + @WithdrawlBranchID + ')'       
          SET @mDescriptionHCharges='ATM(' + @ATMID + ') IBK-BAL ENQ-CHG AT BK.' + @BankShortName   + ' (BR.' + @WithdrawlBranchID + ') ' + ' From : A/c # ' + @AccountID      
      
          Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)      
          Set @mRemarksCharges='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Charges Transaction ($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ') From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Ti
tle : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)         
         END      
        ELSE       
         BEGIN      
          SET @mDescriptionCharges='ATM(' + @ATMID + ') IBK-BAL ENQ-CHG AT BK.' + @BankShortName  + ' (BR.' + @WithdrawlBranchID + ')'       
          SET @mDescriptionHCharges='ATM(' + @ATMID + ') IBK-BAL ENQ-CHG AT BK.' + @BankShortName   + ' (BR.' + @WithdrawlBranchID + ') ' + ' From : A/c # ' + @AccountID      
      
          Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')) From :
' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)      
   Set @mRemarksCharges='Being Amount of ATM (' + @ATMID+ ') Inter Bank Cash Withdrawl Charges Transaction (' + @Currency + '-' + convert(varchar(15),@OtherCurrencyAmount) + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')
) From : ' + @NameLocation + ' By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)      
         END      
      END      
     ELSE      
      IF @vCount4 > 0       
       BEGIN      
        SET  @mWithdrawlBranchName = 'N/A'      
        SELECT @mWithdrawlBranchName=BranchName FROM t_ATM_Branches      
        WHERE BranchID = @WithdrawlBranchID      
        Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Branch Cash Withdrawl Transaction From : BK.' + @BankShortName +  ' (' + upper(@mWithdrawlBranchName) + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)      
       END      
      --IF @vCount4 = 0       
      ELSE      
       BEGIN      
        Set @mRemarks='Being Amount of ATM (' + @ATMID+ ') Inter Branch Cash Withdrawl Transaction From : BK.' + @BankShortName +  ' (BR.' +  @WithdrawlBranchID + ') By  A/c # ' + @AccountID + ' Title : ' + upper(@mAccountName) + ' At : '  + upper(@vBranchName)      
       END      
	   
	   SET @STAN = RIGHT(@RefNo,6)
        
    INSERT INTO t_ATM_TransferTransaction      
     (      
      ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
     )        
        
    VALUES     
     (         
      @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@AccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,@Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescription,@ATMID,@mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MCC,@TrxDesc,@MerchantType,@STAN
     )      
           
      SELECT @Status=@@ERROR      
        
      IF @Status <> 0          
       BEGIN      
               
        RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR         
        RETURN (5000)      
       END      
  --Credit Transaction         
      
     IF @MerchantType <> '0000'      
      BEGIN      
      IF @Currency = '586'       
       BEGIN      
        SELECT @BalanceEnquiryCharges=BalanceEnquiryCharges,      
        @IssuerChargesAccountID=IssuerChargesAccountID       
      
        From t_ATM_SwitchCharges      
        WHERE (MerchantType = @MerchantType)      
         set @Amount=@BalanceEnquiryCharges      
       END      
      ELSE      
       BEGIN      
        SELECT  @BalanceEnquiryCharges=BalanceEnquiryCharges,      
        @IssuerChargesAccountID=IssuerChargesAccountID       
        From t_ATM_SwitchCharges      
        WHERE (MerchantType = @MerchantType)      
         set @Amount=@BalanceEnquiryCharges      
       END      
        
      END      
      
        
    SELECT @vAccountID=Accountid, @mAccountName=Description,@mProductID='GL', @mCurrencyID=CurrencyID,@mAccountType='G' FROM t_GL       
    WHERE (OurBranchID =@OurBranchid) AND (AccountID =  @IssuerChargesAccountID)      
    BEGIN      
     Set @mSerialNo = 2      
     Set @mTrxType = 'C'      
	 
	 SET @STAN = RIGHT(@RefNo,6)
      
     INSERT INTO t_ATM_TransferTransaction      
      (      
       ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,BankCode,Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
      )        
         
     VALUES      
      (      
       @mScrollNo,@mSerialNo,@Refno,@OurBranchID,@WithdrawlBranchID,@vAccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,@mwDate,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,@Supervision,@mGLID,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,@mDescription,@ATMID,@mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MCC,@TrxDesc,@MerchantType,@STAN
      )      
      
           
       SELECT @Status=@@ERROR      
         
       IF @Status <> 0       
        BEGIN      
                
         RAISERROR('There Is An Error Occured While Creating Transaction In The Target Branch',16,1) WITH SETERROR      
         RETURN (5000)      
        END      
         
    END         
      
      
    SELECT @Status = 0 ,   
    @AvailableBalance = (isnull(@limit,0)+(isnull(@vClearBalance,0))-(isnull(@amount,0))-isNull(@WithDrawlCharges,0))*100,   
    @vClearBalance = ((IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0))-IsNull(@Amount,0)-isNull(@WithDrawlCharges,0))*100,  
    @Value='3'  
    SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value,  
    @cAccountName AccountName, @mwDate WDate  
    RETURN 0      
      
-- --     SELECT @Status = 0 , @vClearBalance = ((IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0))-IsNull(@Amount,0)-isNull(@WithDrawlCharges,0))*100      
-- --     SELECT @Status As Status , @vClearBalance As ClearBalance,@IssuerAccountid as accountid      
-- --     RETURN 0       
        
           
   END