﻿CREATE   PROCEDURE [dbo].[spc_GetCustomerStatementFCY]            
(                                
 @OurBranchID  varchar(30),                              
 @AccountID varchar(30),                              
 @sDate          Datetime,                                
 @eDate          Datetime                                
)                                
                                
AS                                
                                
  SET NOCOUNT ON                                
                                   
   DECLARE @OpeningBalance as money                                
   DECLARE @LocEqOpBal as money                                
                                
   DECLARE @crOpBal as money                                
   DECLARE @crLocEqOpBal as money                                
                                
   DECLARE @drOpBal as money                                
   DECLARE @drLocEqOpBal as money                                
                                
   DECLARE @tDate as Varchar(15)                                
   DECLARE @WorkingDate as datetime                                
                                
   SELECT @WorkingDate = WorkingDate From t_Last                                
   WHERE OurBranchID = @OurBranchID                                
                                
                                
 SET @OpeningBalance = 0                                
 SET @crOpBal = 0                                
 SET @drOpBal = 0                                
                                
 SET @LocEqOpBal = 0                                
 SET @crLocEqOpBal = 0                                
 SET @drLocEqOpBal = 0                                
                                
   SET @tDate = cast(Month(@sDate) as Varchar(2)) + '/01/' + cast(Year(@sDate)  as Varchar(4))                                
                                
   SELECT @OpeningBalance = IsNull(ClearBalance,0) , @LocEqOpBal = LocalClearBalance                                 
   FROM t_AccountMonthBalances                                
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND Month = @tDate                                
                                 
   SELECT @crOpBal = IsNull(SUM(ForeignAmount),0),@crLocEqOpBal = IsNull(SUM(Amount),0) From t_Transactions                                
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                                 
         AND TrxType = 'C' AND AccountType = 'C' and [Status] <> 'R'                
         --AND convert(varchar(10) , wDate,101) >= @tDate AND convert(varchar(10) , wDate,101) < @sDate                                
  AND wDate >= @tDate AND wDate < @sDate                                
                                
   SELECT @drOpBal = IsNull(SUM(ForeignAmount),0),@drLocEqOpBal = IsNull(SUM(Amount),0) From t_Transactions                                
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                                 
         AND TrxType = 'D' AND AccountType = 'C' and [Status] <> 'R'                
         --AND convert(varchar(10) , wDate,101) >= @tDate AND convert(varchar(10) , wDate,101) < @sDate                                
  AND wDate >= @tDate AND wDate < @sDate                                
                                 
   SET @OpeningBalance = @OpeningBalance + @crOpBal - @drOpbal                                
   SET @LocEqOpBal = @LocEqOpBal + @crLocEqOpBal - @drLocEqOpBal                                
                                
   IF @WorkingDate = @eDate                                 
      BEGIN                                
                                
         SELECT '' ChannelRefID,'' OperatorID,'' SupervisorID,'01/01/1900' as wDate, '01/01/1900' AS TrxTimeStamp, @sDate as ValueDate, 'OP' AS DescriptionID,'Opening Balance' as  Description,'' as ChequeID, '01/01/1900' AS ChequeDate, '' as TrxType,                
         IsNull(@LocEqOpBal,0) as Amount, IsNull(@OpeningBalance,0) as ForeignAmount, 0 AS ExchangeRate,'OP' As TableStr,0 ScrollNo ,'' DocumentType, '' Remarks            
                                 
         UNION ALL                                
                                
         SELECT ChannelRefID,OperatorID,SupervisorID,wDate, TrxTimeStamp,ValueDate, DescriptionID , Description, ChequeID, ChequeDate, TrxType, Amount, ForeignAmount, ExchangeRate                                
            ,'H' As TableStr,ScrollNo , DocumentType, Remarks            
         FROM t_Transactions                                
         WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                                 
            AND AccountType = 'C' and [Status] <> 'R'                
            AND convert(varchar(10) , wDate,101) >= @sDate AND convert(varchar(10) , wDate,101) <= @eDate         
   order by TrxTimeStamp asc  
                                 
/*              
         UNION ALL                                
              
--t_CashTransactionModel                                
         SELECT wDate,ValueDate , Description, ChequeID,TrxType, Amount, ForeignAmount                                
                  ,'C' As TableStr,ScrollNo , 'C' DocumentType                        
         FROM t_CashTransactionModel                                
         WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                                       
               AND Supervision = 'C'  AND AccountType ='C'                                
                                
         UNION ALL                                
                
--t_TransferTransactionModel                                
         SELECT wDate,ValueDate , Description, ChequeID,TrxType, Amount, ForeignAmount                                
                  ,'T' As TableStr,ScrollNo ,'T' DocumentType                                
         FROM t_TransferTransactionModel                                
         WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                             
               AND Supervision = 'C'  AND AccountType ='C'                                               
                                
        UNION ALL                                
                
--t_InWardClearing                                
         SELECT wDate,ValueDate , Description, ChequeID,TransactionType as TrxType, Amount, ForeignAmount                                
                  ,'I' As TableStr,SerialNo ScrollNo ,'ID' DocumentType                                
         FROM t_InWardClearing                                
         WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                                       
               AND Status <>'R'  AND AccountType ='C'                                
                                       
         UNION ALL                                
                               
--t_AllModelTransaction                                
          SELECT wDate,ValueDate , Description, ChequeID,TrxType, Amount, ForeignAmount                                
                   ,'A' As TableStr, ScrollNo ,'OW' DocumentType                                
          FROM t_AllModelTransaction                                
          WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                                       
                AND AccountType ='C'                                
                        
--          UNION ALL                                
                                        
--t_ATM_CashTransaction                                
         SELECT wDate,ValueDate , Description,'' as ChequeID,TrxType, Amount, ForeignAmount                                
                  ,'ATM' As TableStr,ScrollNo ,'AT' DocumentType                                
         FROM t_ATM_CashTransaction                                
         WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                        
               AND Supervision = 'C' AND AccountType ='C'                                
                                
         UNION ALL                                
                                        
--'t_ATM_TransferTransaction                                
         SELECT wDate,ValueDate , Description,'' as ChequeID,TrxType, Amount, ForeignAmount                                
                  ,'ATMT' As TableStr,ScrollNo ,'AT' DocumentType                                
         FROM t_ATM_TransferTransaction                                
         WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                                       
               AND Supervision = 'C' AND AccountType ='C'                                
                                
         UNION ALL                                
                                        
--'t_OnLine_CashTransaction                                
         SELECT wDate,ValueDate , Description, ChequeID,TrxType, Amount, ForeignAmount                                
                  ,'OL' As TableStr,ScrollNo ,'OL' DocumentType                  
         FROM t_OnLineCashTransaction                                
         WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                                       
               AND Supervision <> 'R' AND AccountType ='C'                                
                                   
         UNION ALL                    
              
--'t_InternetTransaction                      
         SELECT wDate,ValueDate , Description, 'V',TrxType, Amount, ForeignAmount                                    
                  ,'IT' As TableStr, ScrollNo,'IT' as DocumentType                                    
         FROM t_InternetTransaction                      
         WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                                           
               AND AccountType ='C' AND DescriptionID IN ('IT1','IT2')                      
  */                                   
                          
      END                                
                                
   ELSE -- If Statement not in workingdate                    
                                
      BEGIN                                
                                
         SELECT '' ChannelRefID,'' OperatorID,'' SupervisorID,'01/01/1900' as wDate, '01/01/1900' AS TrxTimeStamp, @sDate as ValueDate, 'OP' AS DescriptionID ,'Opening Balance' as  Description,'' as ChequeID, '01/01/1900' AS ChequeDate, '' as TrxType,      
   IsNull(@LocEqOpBal,0) as Amount, IsNull(@OpeningBalance,0) as ForeignAmount            
                            
               , 0 AS ExchangeRate,'OP' As TableStr,0 ScrollNo ,'' DocumentType, '' Remarks            
                                
         UNION ALL                                
                                
         SELECT ChannelRefID,OperatorID,SupervisorID,wDate, TrxTimeStamp,ValueDate, DescriptionID , Description, ChequeID, ChequeDate,TrxType, Amount, ForeignAmount, ExchangeRate                                
               ,'H' As TableStr,ScrollNo , DocumentType, Remarks            
         FROM t_Transactions                                
         WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                                 
            AND AccountType = 'C' and [Status] <> 'R'                
            AND convert(varchar(10) , wDate,101) >= @sDate AND convert(varchar(10) , wDate,101) <= @eDate       
   order by TrxTimeStamp asc  
                                  
      END