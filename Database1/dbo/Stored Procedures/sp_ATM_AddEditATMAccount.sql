﻿CREATE PROCEDURE [dbo].[sp_ATM_AddEditATMAccount]
	(
		@OurBranchID varchar(30),
		@ATMID varchar(30), 
		@ATMAccountID varchar(30), 
		@ATMAccountTitle varchar(100),  
		@ATMCurrencyID varchar(30), 
		@ATMCurrencyName varchar(100), 
		@ATMProductID varchar(30), 
		@NewRecord bit=1
	)
AS
	IF cast(@NewRecord as bit) = 1
		BEGIN
			INSERT INTO t_ATM_ATMAccount(OurBranchID,ATMID, ATMAccountID, ATMAccountTitle, ATMCurrencyID, ATMCurrencyName, ATMProductID) VALUES (@OurBranchID,@ATMID, @ATMAccountID, @ATMAccountTitle, @ATMCurrencyID, @ATMCurrencyName, @ATMProductID)
		END
	ELSE
		BEGIN
			UPDATE t_ATM_ATMAccount SET OurBranchID=@OurBranchID,ATMID=@ATMID, ATMAccountID=@ATMAccountID, ATMAccountTitle=@ATMAccountTitle, ATMCurrencyID=@ATMCurrencyID, ATMCurrencyName=@ATMCurrencyName, ATMProductID=@ATMProductID
		END