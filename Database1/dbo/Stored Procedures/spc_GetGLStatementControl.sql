﻿create   PROCEDURE [dbo].[spc_GetGLStatementControl]                     
(                       
 @OurBranchID  varchar(30),                       
 @AccountID varchar(30),                       
 @sDate          Datetime,                       
 @eDate          Datetime                       
)                       
                       
AS                       
                       
   SET NOCOUNT ON                       
                       
   DECLARE @OpeningBalance as money                       
   DECLARE @crOpBal as money                       
   DECLARE @drOpBal as money                       
                       
   DECLARE @WorkingDate as datetime                       
                       
   SELECT @WorkingDate = WorkingDate From t_Last                       
   WHERE OurBranchID = @OurBranchID                       
                       
 SET @OpeningBalance = 0                       
 SET @crOpBal = 0                       
 SET @drOpBal = 0                       
                       
                       
                       
   SELECT @OpeningBalance = IsNull(OpeningBalance,0)                       
   FROM t_GL                       
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                       
                       
   SELECT @crOpBal = IsNull(SUM(Amount),0) From t_GLTransactions                       
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                        
         AND IsCredit = '1'  and [Status] <> 'R'             
  --AND convert(varchar(10) , Date,101) < @sDate                       
  AND Date < @sDate                       
                       
   SELECT @drOpBal = IsNull(SUM(Amount),0) From t_GLTransactions                       
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                        
         AND IsCredit = '0' and [Status] <> 'R'             
  --AND convert(varchar(10) , Date,101) < @sDate                       
  AND Date < @sDate                       
                       
   SET @OpeningBalance = @OpeningBalance + @crOpBal - @drOpbal                       
                       
   IF @WorkingDate = @eDate                        
      BEGIN                       
         SELECT '' CostCenterID,'' SupervisorID,'' OperatorID,'01/01/1900' as wDate, '01/01/1900' AS TrxTimeStamp, @sDate as ValueDate ,'Opening Balance' as  Description,'' as ChequeID, '' as TrxType,IsNull(@OpeningBalance,0) as Amount                   
   
               ,'OP' As TableStr,0 ScrollNo ,'' DocumentType, '' as Remarks     ,'' ChannelRefID     
                       
         UNION ALL                       
                       
         SELECT CostCenterID,SupervisorID,OperatorID,Date as wDate, TrxTimeStamp,ValueDate , Description,'' as ChequeID,CASE IsCredit WHEN 1 THEN 'C' ELSE 'D' END As TrxType, Amount                       
               ,'H' As TableStr,ScrollNo ,TransactionType DocumentType, Remarks    ,ChannelRefID     
         FROM t_GLTransactions                       
         WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID and [Status] <> 'R'             
            AND convert(varchar(10) , Date,101) >= @sDate AND convert(varchar(10) , Date,101) <= @eDate                       
                      order by TrxTimeStamp                
                       
      END                       
                       
   ELSE -- If Statement not in workingdate                       
                       
      BEGIN                       
                       
         SELECT '' CostCenterID,'' SupervisorID,'' OperatorID,'01/01/1900' as wDate, '01/01/1900' AS TrxTimeStamp, @sDate as ValueDate ,'Opening Balance' as  Description,'' as ChequeID, '' as TrxType,IsNull(@OpeningBalance,0) as Amount                  
    
               ,'OP' As TableStr,0 ScrollNo ,'' DocumentType, '' as Remarks     ,'' ChannelRefID     
                       
         UNION ALL                       
                       
         SELECT CostCenterID,SupervisorID,OperatorID,Date as wDate, TrxTimeStamp,ValueDate , Description,'' as ChequeID,CASE IsCredit WHEN 1 THEN 'C' ELSE 'D' END As TrxType, Amount                       
               ,'H' As TableStr,ScrollNo, TransactionType DocumentType, Remarks     ,ChannelRefID     
   FROM t_GLTransactions                       
         WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID  and [Status] <> 'R'             
               AND convert(varchar(10) , Date,101) >= @sDate AND convert(varchar(10) , Date,101) <= @eDate                       
                order by TrxTimeStamp             
      END