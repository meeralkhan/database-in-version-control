﻿
CREATE Procedure [dbo].[sp_ATM_GetATMAccountBalance]
  
 (  
  @OurBranchID  varchar(30),  
  @ATMID  varchar(30)  
 )  
  
As  
  
SELECT OpeningBalance FROM t_GL  
 WHERE ( t_GL.OurBranchID = @OurBranchID)    
   AND AccountID = (SELECT ATMAccountID FROM t_ATM_ATMAccount  WHERE (t_ATM_ATMAccount.OurBranchID = @OurBranchID) and (t_ATM_ATMAccount.ATMID = @ATMID))