﻿
CREATE PROCEDURE [dbo].[spplus_GetATMGLAccountDetails] (
 @OurBranchID varchar(30),
 @AccountID varchar(30)=''
)  
 AS    
 if @AccountID <> ''    
  begin    
  
   if exists ( SELECT AccountID FROM t_GL WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID )
    
    select g.OurBranchID, g.AccountID, g.[Description] as AccountName, c.CurrencyID, c.[Description] as CurrencyName,
    'GL' as ProductID, 'General Ledger' as ProductName, 1 as ReturnValue 
    from t_GL g
    INNER JOIN t_Currencies c ON g.OurBranchID = c.OurBranchID AND g.CurrencyID = c.CurrencyID
    Where g.OurBranchID = @OurBranchID AND g.AccountID = @AccountID AND g.AccountClass = 'P'
      
   else    
   
    select 0 as ReturnValue    
    
  end    
    
 else    
  begin    
   
   select g.OurBranchID, g.AccountID, g.[Description] as AccountName, c.CurrencyID, c.[Description] as CurrencyName,
   'GL' as ProductID, 'General Ledger' as ProductName
   from t_GL g 
   INNER JOIN t_Currencies c ON g.OurBranchID = c.OurBranchID AND g.CurrencyID = c.CurrencyID
   where g.OurBranchID = @OurBranchID
   
  end