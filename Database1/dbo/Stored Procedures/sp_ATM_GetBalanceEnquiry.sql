﻿create   PROCEDURE [dbo].[sp_ATM_GetBalanceEnquiry]          
(            
 @OurBranchID       varchar(30),            
 @ATMID             varchar(30),             
 @AccountID         varchar(30),            
 @Supervision       char(1),             
 @RefNo             varchar(100),            
 @PHXDate           datetime,            
 @MerchantType      varchar(30)='',            
 @WithdrawlBranchID varchar(30)='',             
 @AckInstIDCode     varchar(30)='',            
 @ProcCode           varchar(50)='',            
 @SettlmntAmount    numeric(18,6)=0,          
 @ConvRate          decimal(15,9)=0,          
 @AcqCountryCode    varchar(3)='',          
 @CurrCodeTran      varchar(3)='',          
 @CurrCodeSett      varchar(3)='',          
 @ForwardInstID  varchar(10)='',          
 @MessageType  varchar(10)='',          
 @OrgTrxRefNo  varchar(100)='',          
          
 @POSEntryMode    varchar(3)='',          
 @POSConditionCode   varchar(2)='',          
 @POSPINCaptCode   varchar(2)='',          
 @RetRefNum     varchar(15)='',          
 @CardAccptID    varchar(15)='',          
 @CardAccptNameLoc   varchar(50)='',          
 @VISATrxID     varchar(15)='',          
 @CAVV      char(1)='',          
 @ResponseCode    char(2)='',          
 @ExtendedData    varchar(2)='',    
 /* TO BE UPDATE LATER */          
 @ChannelId     nvarchar(30)='4',          
 @ChannelRefID    nvarchar(100)='',          
          
 @NewRecord         bit=1            
 )          
AS            
BEGIN          
          
 SET NOCOUNT ON            
          
 DECLARE @sCount as int          
  SET @sCount = 0          
 DECLARE @cCount as int          
  SET @cCount = 0          
 DECLARE @Count as bit            
 DECLARE @ClearBalance as money            
 DECLARE @AvailableBalance as money            
 Declare @MinBalance as money          
 Declare @value as varchar(2)            
 DECLARE @vClearedEffects as money            
 DECLARE @vFreezeAmount as money            
 DECLARE @Limit as money            
 DECLARE @AcquiringChargesAccountID as varchar(30)            
 DECLARE @ExciseDutyAccountID as varchar(30)          
 DECLARE @AuthIDResp as varchar(6)          
  SET @AuthIDResp = '0'          
            
 DECLARE @ExciseDutyPercentage as money          
 DECLARE @BalanceEnquiryCharges as money            
 DECLARE @ExciseDutyAmount as money          
            
  SET @ExciseDutyPercentage = 0          
  SET @BalanceEnquiryCharges = 0          
  SET @ExciseDutyAmount = 0          
          
 DECLARE @BankShortName as varchar(30)          
  SET @BankShortName = 'N/A'              
 DECLARE @mScrollNo as int            
  SET @mScrollNo = 0          
 DECLARE @VoucherID int            
 DECLARE @IsCredit int            
 DECLARE @mSerialNo as int            
  SET @mSerialNo = 0          
 DECLARE @mwDate as datetime            
 DECLARE @mTrxType as char            
 DECLARE @mAccountID as varchar(30)            
 DECLARE @mAccountName as varchar(50)            
 DECLARE @mAccountType as char            
 DECLARE @mProductID as varchar(30)            
 DECLARE @mCurrencyID as varchar(30)            
          
 DECLARE @gAccountID as varchar(30)            
 DECLARE @gAccountName as varchar(50)            
 DECLARE @gAccountType as char            
 DECLARE @gProductID as varchar(30)            
 DECLARE @gCurrencyID as varchar(30)            
          
 DECLARE @LocalCurrency as varchar(30)          
 DECLARE @mIsLocalCurrency as bit            
  SET @mIsLocalCurrency=1             
 DECLARE @STAN as varchar(30)          
  SET @STAN = RIGHT(@RefNo,6)          
  SET @ChannelRefID = @STAN          
 DECLARE @GLControl as varchar(30)            
 DECLARE @mForeignAmount as money            
  SET @mForeignAmount=0             
 DECLARE @mExchangeRate as money            
  SET @mExchangeRate=1             
 DECLARE @mDescriptionID as varchar(30)            
  SET @mDescriptionID='000'             
 DECLARE @mDescriptionIDChg2 as varchar(30)            
  SET @mDescriptionIDChg2='000'             
 DECLARE @mDescriptionCharges as varchar(500)            
 DECLARE @mDescriptionHCharges as varchar(500)            
 DECLARE @mBankCode as varchar(4)            
  SET @mBankCode='1279'             
 DECLARE @mOperatorID as varchar(30)            
  SET @mOperatorID = 'ATM-' + @ATMID            
 DECLARE @mSupervisorID as varchar(30)            
  SET @mSupervisorID = 'N/A'          
 DECLARE @CurrencyCode as varchar(30)          
 DECLARE @vBranchName as varchar(50)            
 DECLARE @HoldStatus as char(1)          
 DECLARE @HoldAmount as money          
 DECLARE @FreezeAmount as money          
 DECLARE @vCheckLimit as bit          
  SET @vCheckLimit = 0          
 DECLARE @vODLimitAllow as bit          
  SET @vODLimitAllow = 0          
 Declare @SOCID as varchar(30)   
  SET @SOCID = '' 
 Declare @ATMChargeID as varchar(6)          
 Declare @NoOfBITrxATMMonthly as decimal(24,0)          
  SET @NoOfBITrxATMMonthly = 0          
 DECLARE @NoOfFreeBI as decimal(24,0)          
 DECLARE @ApplyCharges as char(1)          
  SET @ApplyCharges = '0'          
 Declare @NoOfCWTrxATMMonthly as decimal(24,0)        
  SET @NoOfCWTrxATMMonthly = 0 
 DECLARE @VatReferenceID as varchar(30)  
          
 SELECT @BankShortName = ISNULL(BankShortName,'') From t_ATM_Banks WHERE (OurBranchID = @OurBranchID AND BankIMD = @AckInstIDCode)          
 SELECT @vBranchName=Heading2, @LocalCurrency=LocalCurrency FROM t_GlobalVariables WHERE (OurBranchID = @OurBranchID)          
           
 --Checking Variables          
            
 DECLARE @Status as int             
 DECLARE @mLastEOD as datetime            
 DECLARE @mCONFRETI as datetime            
 DECLARE @mRemarksCharges as varchar(500)            
 DECLARE @mRemarksCharges2 as varchar(500)            
 DECLARE @vCount4 as smallint          
 DECLARE @vIsClose as char(1)          
 DECLARE @IsGCC as bit          
  SET @IsGCC = 0          
 DECLARE @AppWHTax as smallint
 DECLARE @WHTaxAmount as money
  SET @AppWHTax = 0
  SET @WHTaxAmount = 0
 DECLARE @MobileNo nvarchar(30)
 DECLARE @Email nvarchar(100)
 DECLARE @rCurrencyID nvarchar(30)
 DECLARE @ClientID nvarchar(30)
 DECLARE @NarrationID nvarchar(30)
  SET @MobileNo = ''
  SET @Email = ''
  SET @rCurrencyID = ''
  SET @ClientID = ''
  SET @NarrationID = ''
 DECLARE @CountryCode as nvarchar(3)
  SET @CountryCode = ''
            
 SELECT @Status = -1            
 
 SELECT @rCurrencyID=ISNULL(CurrencyID,'') FROM t_Currencies WHERE OurBranchID = @OurBranchID AND CurrencyCode = @CurrCodeTran
 select @IsGCC=ISNULL(IsGCC,0), @CountryCode=ISNULL(CountryCode,'') from t_Country where OurBranchID = @OurBranchID AND ShortName = RIGHT(@CardAccptNameLoc, 2)

 SET @sCount = 0          
 SET @cCount = 1          
          
 WHILE (@sCount <> @cCount)            
 BEGIN            
  exec @mScrollNo = fnc_GetRandomScrollNo          
  if not exists (SELECT ScrollNo from t_ATM_TransferTransaction Where OurBranchID = @OurBranchID AND ScrollNo = @mScrollNo)        
  BEGIN          
   SET @sCount = @sCount + 1            
  END          
 END          
 
 if (@CountryCode = '784')          
 BEGIN 
 
  SET @ATMChargeID = '600000'    
  
  --SET @mDescriptionID = '807'          --old
  --SET @mDescriptionIDChg2 = '808'          --old

  SET @mDescriptionID = '400730'          --new
  SET @mDescriptionIDChg2 = '400020'          --new

  SET @mDescriptionCharges='Balance Enquiry from Local ATM Fees (' + @ATMID+ ') Local Balance Enquiry Fees at ' + @CardAccptNameLoc
  SET @mRemarksCharges=''

  SET @mDescriptionHCharges='Value Added Tax (VAT) for transaction  - '+Cast(@mScrollNo as varchar(30))
  SET @mRemarksCharges2=''

 END          
 else if (@IsGCC = 1)          
 BEGIN   
  SET @ATMChargeID = 'GCC'          
  
  --SET @mDescriptionID = '807'          --old
  --SET @mDescriptionIDChg2 = '808'          --old

  SET @mDescriptionID = '400800'          --new
  SET @mDescriptionIDChg2 = '400020'          --new

  SET @mDescriptionCharges='Balance Enquiry from GCC ATM Fees (' + @ATMID+ ') GCC Balance Enquiry Fees at ' + @CardAccptNameLoc
  SET @mRemarksCharges=''

  SET @mDescriptionHCharges='Value Added Tax (VAT) for transaction  - '+Cast(@mScrollNo as varchar(30))
  SET @mRemarksCharges2=''

 END          
 ELSE          
 BEGIN          
  SET @ATMChargeID = '500000'          
  --SET @mDescriptionID = '809'          
  --SET @mDescriptionIDChg2 = '810'          

  SET @mDescriptionID = '400740'          --new
  SET @mDescriptionIDChg2 = '400020'          --new

  SET @mDescriptionCharges='Balance Enquiry from International ATM Fees (' + @ATMID+ ') International Balance Enquiry Fees at ' + @CardAccptNameLoc
  SET @mRemarksCharges=''

  SET @mDescriptionHCharges='Value Added Tax (VAT) for transaction  - '+Cast(@mScrollNo as varchar(30))
  SET @mRemarksCharges2=''
 END
 
 SET @NarrationID = @mDescriptionID
 
 IF (@ExtendedData = 'DA') -- For Declinals the account id has tokanized PAN
 begin
  SELECT @AccountID=ISNULL(AccountID,'') from t_DebitCards where OurBranchID = @OurBranchID AND DebitCardToken = @AccountID
 END
           
 SELECT @Count=Count(*) FROM t_AccountBalance WHERE (OurBranchID = @OurBranchID) AND (AccountID = @AccountID)           
 IF @Count=0    -- INVALID ACCOUNT          
 BEGIN            
  /** Rejection change STARTS 06/04/2021 **/
  INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp,ExtendedData)
  VALUES (@OurBranchID, @AccountID, @RefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), 0, @mForeignAmount, @mExchangeRate, 
  @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, @AckInstIDCode, @RetRefNum, @CardAccptID, @CardAccptNameLoc, '', @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '02', 'Invalid Account', 'Balance Enquiry Trx', '0', @ExtendedData)
  
  SELECT @Status=2, @AvailableBalance = 0, @ClearBalance = 0          
  SELECT @Status As Status, @ClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, '00' As value,           
  '784' as CurrencyCode, '0' as AuthIDResp, @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
  RETURN (1)
  
  /** Rejection change ENDS 06/04/2021 **/
 END            
 
 SELECT @vCheckLimit=CheckLimit FROM t_ATM_GlobalVariables Where OurBranchID = @OurBranchID          
 SELECT @vODLimitAllow=ODLimitAllow FROM t_ATM_Banks WHERE OurBranchID = @OurBranchID AND BankIMD = @AckInstIDCode          
           
 SELECT @ClearBalance=IsNull(ab.ClearBalance,0),@vClearedEffects=IsNull(ab.Effects,0),@mAccountName=ab.Name,@Limit=ISNULL(ab.Limit,0),@mAccountType='C',@mProductID=a.ProductID,
 @mCurrencyID=c.CurrencyID,@vFreezeAmount=ISNULL(ab.FreezeAmount,0),@vIsClose=ISNULL(a.Status,''),@value=ISNULL(p.productatmcode,'00'),@MinBalance=ISNULL(p.MinBalance,0), 
 @CurrencyCode = ISNULL(c.CurrencyCode,''),@SOCID=ISNULL(a.SOCID,''),@NoOfCWTrxATMMonthly=ISNULL(ab.NoOfCWTrxATMMonthly,0),@NoOfBITrxATMMonthly=ISNULL(ab.NoOfBITrxATMMonthly,0), 
 @MobileNo=ISNULL(cust.MobileNo,''),@Email=ISNULL(cust.Email,''),@ClientID=cust.ClientID
 FROM t_AccountBalance ab           
 INNER JOIN t_Account a ON ab.OurBranchID = a.OurBranchID AND ab.AccountID = a.AccountID AND ab.ProductID = a.ProductID          
 INNER JOIN t_Products p ON ab.OurBranchID = p.OurBranchID AND ab.ProductID = p.ProductID          
 INNER JOIN t_Currencies c ON ab.OurBranchID = c.OurBranchID AND p.CurrencyID = c.CurrencyID          
 INNER JOIN t_Customer cust ON ab.OurBranchID = cust.OurBranchID AND a.ClientID = cust.ClientID
 WHERE (ab.OurBranchID = @OurBranchID) AND (ab.AccountID = @AccountID)            
           
 --exec @Limit = [fnc_GetLimit] @OurBranchID, @AccountID          
           
 IF @Limit > 0 AND @vCheckLimit = 1 AND @vODLimitAllow = 1          
 BEGIN          
  SET @ClearBalance=(@ClearBalance+@Limit)
 END          

 --if (@AcqCountryCode = '784')
 --BEGIN          
  SET @NoOfBITrxATMMonthly = @NoOfBITrxATMMonthly + 1
 --END        

 /*****DECLINE ADVISE STARTS *****/
  
 IF (@ExtendedData = 'DA')
 begin
 
  Insert into t_ENDeclineAdvices (OurBranchID, AccountID, RetRefNum, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, 
  ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, CardAccptID, CardAccptNameLoc, 
  DescriptionID, VISATrxID, CAVV, ResponseCode)
  VALUES        
  (
   @OurBranchID,@AccountID,@RetRefNum,@STAN,@PHXDate,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@SettlmntAmount,@mForeignAmount,@mExchangeRate,@ConvRate,
   @MerchantType,@AcqCountryCode,@ForwardInstID,@ATMID,@CurrCodeTran,@CurrCodeSett,@SettlmntAmount,@ProcCode,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,
   @CardAccptID,@CardAccptNameLoc,@NarrationID,@VISATrxID,@CAVV,@ResponseCode    
  )
 
  SELECT @Status    = 0,       
  @AvailableBalance = ( isnull(@ClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@MinBalance,0) )*100,
  @ClearBalance    = ( IsNull(@ClearBalance,0)-IsNull(@vClearedEffects,0) )*100      
      
  SELECT @Status As Status, @ClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value, @CurrencyCode as CurrencyCode, '0' as AuthIDResp,@MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
  RETURN 0;    
  
 end
 
 /*****DECLINE ADVISE ENDS *****/
 
 IF @vIsClose = 'C' or @vIsClose = 'T' or @vIsClose = 'X' or @vIsClose = 'D' or @vIsClose = 'I'          
 BEGIN   --INACTIVE_ACCOUNT             
  /** Rejection change STARTS 06/04/2021 **/
  
  INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
  VALUES (@OurBranchID, @AccountID, @RefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), 0, @mForeignAmount, @mExchangeRate, 
  @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, @AckInstIDCode, @RetRefNum, @CardAccptID, @CardAccptNameLoc, @mDescriptionCharges, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '03', 'Account In-Active/ Closed/ Dormant/ Deceased/ Blocked', 'Balance Enquiry Trx', '0', @ExtendedData)
  
  SELECT @Status=3,
  @AvailableBalance = ( isnull(@ClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@MinBalance,0) )*100,          
  @ClearBalance     = ( IsNull(@ClearBalance,0)-IsNull(@vClearedEffects,0) )*100          
          
  SELECT @Status As Status, @ClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value,           
  @CurrencyCode as CurrencyCode, '0' as AuthIDResp, @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
  RETURN (1)
  
  /** Rejection change ENDS 06/04/2021 **/
 END
 
 IF (@SOCID <> '')
 BEGIN          
  SELECT @BalanceEnquiryCharges=ISNULL(AcqBICharges,0),@AcquiringChargesAccountID=ISNULL(BIChargesAccountID,0),@ExciseDutyAccountID=ISNULL(ExciseDutyAccountID,''),          
  @ExciseDutyPercentage=ISNULL(ExciseDutyPercentage,0)
  From t_SOCATMCharges
  WHERE (OurBranchID = @OurBranchID AND ProductID = @mProductID AND SOCID = @SOCID AND ATMChargeID = @ATMChargeID AND NoOfFreeBI < @NoOfBITrxATMMonthly)
  
  SELECT @NoOfFreeBI=ISNULL(NoOfFreeBI,0)
  From t_SOCATMCharges    
  WHERE (OurBranchID = @OurBranchID AND ProductID = @mProductID AND SOCID = @SOCID AND ATMChargeID = @ATMChargeID)
  
  if (@NoOfBITrxATMMonthly <= @NoOfFreeBI)
  BEGIN
   INSERT INTO t_FreeHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, 
   ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, NoOfCWTrxATMMonthly, NoOfBITrxATMMonthly)
   VALUES ( @OurBranchID, @AccountID, @RefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), 0, @mForeignAmount, @mExchangeRate, @ConvRate, 
   @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, 
   @AckInstIDCode, @RetRefNum, @CardAccptID, @CardAccptNameLoc, @mDescriptionCharges, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, @NoOfCWTrxATMMonthly, @NoOfBITrxATMMonthly)
  END
 END          
 
 if (@CountryCode = '784')
 BEGIN          
  -- Increase the counter only in case of Local Trxs      
  UPDATE t_AccountBalance SET NoOfBITrxATM=NoOfBITrxATM+1, AmountOfBITrxATM=AmountOfBITrxATM+@BalanceEnquiryCharges,         
  NoOfBITrxATMMonthly=NoOfBITrxATMMonthly+1, AmountOfBITrxATMMonthly=AmountOfBITrxATMMonthly+@BalanceEnquiryCharges      
  WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID 
 END        
 
 SELECT @mLastEOD=LastEOD,@mwDate=WorkingDate,@mCONFRETI=CONFRETI FROM t_Last WHERE (OurBranchID = @OurBranchID)            
          
 IF @BalanceEnquiryCharges > 0          
 BEGIN           
           
  IF (@ExciseDutyPercentage > 0)
  BEGIN
   SET @ExciseDutyAmount = ROUND(@BalanceEnquiryCharges * @ExciseDutyPercentage / 100, 2)
   SET @AppWHTax = 1
   SET @WHTaxAmount = @ExciseDutyAmount
  END
            
  IF @mLastEOD = @mwDate or @mCONFRETI = '1/1/9999'            
  BEGIN  --End of day
   /** Rejection change STARTS 06/04/2021 **/
  
   INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
   VALUES (@OurBranchID, @AccountID, @RefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), 0, @mForeignAmount, @mExchangeRate, 
   @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, @AckInstIDCode, @RetRefNum, @CardAccptID, @CardAccptNameLoc, @mDescriptionCharges, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '95', 'Day End', 'Balance Enquiry Trx', '0', @ExtendedData)
   
   SELECT @Status=95,
   @AvailableBalance = ( isnull(@ClearBalance,0)-IsNull(@vClearedEffects,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@MinBalance,0) )*100,          
   @ClearBalance     = ( IsNull(@ClearBalance,0)-IsNull(@vClearedEffects,0) )*100          
            
   SELECT @Status As Status, @ClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value,           
   @CurrencyCode as CurrencyCode, '0' as AuthIDResp, @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
   RETURN (1)          
   
   /** Rejection change ENDS 06/04/2021 **/
  END            
      
  if (@MessageType = '0200' OR @MessageType = '0100')
  begin    
   IF @BalanceEnquiryCharges > (@ClearBalance-@vFreezeAmount-@MinBalance-@ExciseDutyAmount)          
   BEGIN   --LOW_BALANCE            
    /** Rejection change STARTS 06/04/2021 **/
  
    INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
    VALUES (@OurBranchID, @AccountID, @RefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), 0, @mForeignAmount, @mExchangeRate, 
    @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, @AckInstIDCode, @RetRefNum, @CardAccptID, @CardAccptNameLoc, @mDescriptionCharges, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '04', 'Low Balance', 'Balance Enquiry Trx', '0', @ExtendedData)
    
    SELECT @Status=4,          
    @AvailableBalance = ( isnull(@ClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@MinBalance,0) )*100,          
    @ClearBalance     = ( IsNull(@ClearBalance,0)-IsNull(@vClearedEffects,0) )*100          
             
    SELECT @Status As Status, @ClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value,           
    @CurrencyCode as CurrencyCode, '0' as AuthIDResp, @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
    RETURN (1)            
    
    /** Rejection change ENDS 06/04/2021 **/
   END            
  END    
  
  select @VatReferenceID = dbo.fn_GenerateText(10)
       
  SET @VoucherID = @mScrollNo     

  Set @mSerialNo = @mSerialNo + 1          
  Set @mTrxType = 'D'          
  Set @IsCredit = 0          
            
  --SELECT @vCount4=count(*) From t_ATM_Branches WHERE OurBranchID = @OurBranchID AND BranchID = @WithdrawlBranchID          
           
  exec @AuthIDResp = fnc_GetRandomNumber          
           
  INSERT INTO t_ATM_TransferTransaction          
  (            
   ScrollNo,SerialNo,RefNo,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,          
   Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,[Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MerchantType,STAN,          
   SettlmntAmount,ConvRate,AcqCountryCode,CurrCodeTran,CurrCodeSett,ATMStatus,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,          
   CardAccptID,CardAccptNameLoc,VISATrxID,ProcCode,CAVV,ResponseCode,ForwardInstID,ClearingRefNo
  )              
  VALUES            
  (            
   @mScrollNo,@mSerialNo,@RefNo+'|CHGS1',@OurBranchID,@WithdrawlBranchID,@AccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,@mwDate,          
   @mwDate + ' ' + convert(varchar(12), GetDate(), 114),@PHXDate,@mTrxType,@Supervision,NULL,@BalanceEnquiryCharges,@mForeignAmount,@mExchangeRate,          
   @mDescriptionID,@mDescriptionCharges,@ATMID,@mBankCode,@mRemarksCharges,@mOperatorID,@mSupervisorID,@MerchantType,@STAN,@SettlmntAmount,          
   @ConvRate,@AcqCountryCode,@CurrCodeTran,@CurrCodeSett,@Supervision,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@RetRefNum,@AuthIDResp,          
   @CardAccptID,@CardAccptNameLoc,@VISATrxID,@ProcCode,@CAVV,@ResponseCode,@ForwardInstID,@RefNo
  )          
    
  INSERT INTO t_Transactions          
  (            
   OurBranchID,ScrollNo,SerialNo,RefNo,wDate,AccountType,DocumentType,AccountID,AccountName,ProductID,CurrencyID,ValueDate,TrxType,ChequeID,ChequeDate,Amount,          
   ForeignAmount,ExchangeRate,ProfitLoss,MeanRate,DescriptionID,[Description],BankCode,BranchCode,TrxPrinted,GLID,[Status],IsLocalCurrency,OperatorID,          
   SupervisorID,AdditionalData,IsMainTrx,DocType,GLVoucherID,Remarks,MerchantType,STAN,SettlmntAmount,ConvRate,AcqCountryCode,CurrCodeTran,CurrCodeSett,ATMStatus,          
   POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,CardAccptID,CardAccptNameLoc,VISATrxID,ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,ForwardInstID,ClearingRefNo,AppWHTax,WHTaxAmount,WHTScrollNo, VATID,[Percent],TClientId,TAccountid,TProductId,VatReferenceID
  )
  VALUES
  (
   @OurBranchID,@mScrollNo,@mSerialNo,@RefNo+'|CHGS1',@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@mAccountType,'ATT',@AccountID,@mAccountName,@mProductID,          
   @mCurrencyID,@mwDate,@mTrxType,'',@mwDate,@BalanceEnquiryCharges,@mForeignAmount,@mExchangeRate,0,@mExchangeRate,@mDescriptionID,          
   @mDescriptionCharges,@mBankCode,@WithdrawlBranchID,0,NULL,@Supervision,@mIsLocalCurrency,@mOperatorID,@mSupervisorID,          
   null,'1','ATT',@VoucherID,@mRemarksCharges,@MerchantType,@STAN,@SettlmntAmount,@ConvRate,@AcqCountryCode,@CurrCodeTran,@CurrCodeSett,@Supervision,          
   @POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@RetRefNum,@AuthIDResp,@CardAccptID,@CardAccptNameLoc,@VISATrxID,@ChannelId,@ChannelRefID,          
   @ProcCode+'|BI CHG',@CAVV,@ResponseCode,@ForwardInstID,@RefNo,@AppWHTax,@WHTaxAmount,@mScrollNo, 
   (case when @ExciseDutyAmount > 0 then '003' else NULL end), 
   (case when @ExciseDutyAmount > 0 then @ExciseDutyPercentage else NULL end),
   @ClientID,@AccountID,@mProductID,
   (case when @ExciseDutyAmount > 0 then @VatReferenceID else NULL end)
  )          
                          
  select @GLControl = ISNULL(GLControl, '') from t_Products where OurBranchID = @OurBranchID and ProductID = @mProductID 
  
  --set @mScrollNo = 0 for @GLControl before @mSerialNo
                
  insert into t_GLTransactions           
  (          
   OurBranchID,RefNo,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,ExchangeRate,IsCredit,TransactionType,          
   TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,IsMainTrx,DocType,Remarks,MerchantType,STAN,SettlmntAmount,ConvRate,          
   AcqCountryCode,CurrCodeTran,CurrCodeSett,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,CardAccptID,CardAccptNameLoc,VISATrxID,          
   ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,ForwardInstID, VATID,[Percent],TClientId,TAccountid,TProductId
  )            
  values           
  (          
   @OurBranchID,@RefNo+'|CHGS1',@GLControl,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@mwDate,@mDescriptionID,    
   @mDescriptionCharges,@mCurrencyID,@BalanceEnquiryCharges,@mForeignAmount,@mExchangeRate,@IsCredit,'ATMTransfer','L',@mOperatorID,          
   @mSupervisorID, '',0,@mSerialNo,null,'0','ATT',@mRemarksCharges,@MerchantType,@STAN,@SettlmntAmount,@ConvRate,@AcqCountryCode,@CurrCodeTran,          
   @CurrCodeSett,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@RetRefNum,@AuthIDResp,@CardAccptID,@CardAccptNameLoc,@VISATrxID,@ChannelId,          
   @ChannelRefID,@ProcCode+'|BI CHG',@CAVV,@ResponseCode,@ForwardInstID,
   (case when @ExciseDutyAmount > 0 then '003' else NULL end), 
   (case when @ExciseDutyAmount > 0 then @ExciseDutyPercentage else NULL end),
   @ClientID,@AccountID,@mProductID
  )          
           
  -- Credit GL Charges Account          
           
  SELECT @gAccountID=AccountID, @gAccountName=[Description],@gProductID='GL', @gCurrencyID=CurrencyID,@gAccountType='G' FROM t_GL             
  WHERE (OurBranchID = @OurBranchID) AND (AccountID =  @AcquiringChargesAccountID)            
            
  SET @mSerialNo = @mSerialNo + 1            
  SET @mTrxType = 'C'            
  SET @IsCredit = 1          
           
  INSERT INTO t_ATM_TransferTransaction          
  (            
   ScrollNo,SerialNo,Refno,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,          
   Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,[Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MerchantType,STAN,          
   SettlmntAmount,ConvRate,AcqCountryCode,CurrCodeTran,CurrCodeSett,ATMStatus,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,          
   CardAccptID,CardAccptNameLoc,VISATrxID,ProcCode,CAVV,ResponseCode,ForwardInstID,ClearingRefNo
  )              
  VALUES            
  (            
   @mScrollNo,@mSerialNo,@RefNo+'|CHGS1',@OurBranchID,@WithdrawlBranchID,@gAccountID,@gAccountName,@gAccountType,@gProductID,@gCurrencyID,@mIsLocalCurrency,@mwDate,          
   @mwDate + ' ' + convert(varchar(12), GetDate(), 114),@PHXDate,@mTrxType,@Supervision,NULL,@BalanceEnquiryCharges,@mForeignAmount,@mExchangeRate,          
   @mDescriptionID,@mDescriptionCharges,@ATMID,@mBankCode,@mRemarksCharges,@mOperatorID,@mSupervisorID,@MerchantType,@STAN,@SettlmntAmount,          
   @ConvRate,@AcqCountryCode,@CurrCodeTran,@CurrCodeSett,@Supervision,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@RetRefNum,@AuthIDResp,          
   @CardAccptID,@CardAccptNameLoc,@VISATrxID,@ProcCode,@CAVV,@ResponseCode,@ForwardInstID,@RefNo
  )          
           
  insert into t_GLTransactions           
  (          
   OurBranchID,RefNo,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,ExchangeRate,IsCredit,TransactionType,          
   TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,IsMainTrx,DocType,Remarks,MerchantType,STAN,SettlmntAmount,ConvRate,          
   AcqCountryCode,CurrCodeTran,CurrCodeSett,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,CardAccptID,CardAccptNameLoc,VISATrxID,          
   ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,ForwardInstID, VATID,[Percent],TClientId,TAccountid,TProductId
  )            
  values     
  (          
   @OurBranchID,@RefNo+'|CHGS1',@gAccountID,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@mwDate,@mDescriptionID,          
   @mDescriptionCharges,@mCurrencyID,@BalanceEnquiryCharges,@mForeignAmount,@mExchangeRate,@IsCredit,'ATMTransfer','L',@mOperatorID,          
   @mSupervisorID, '',@mScrollNo,@mSerialNo,null,'0','ATT',@mRemarksCharges,@MerchantType,@STAN,@SettlmntAmount,@ConvRate,@AcqCountryCode,@CurrCodeTran,          
   @CurrCodeSett,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@RetRefNum,@AuthIDResp,@CardAccptID,@CardAccptNameLoc,@VISATrxID,@ChannelId,          
   @ChannelRefID,@ProcCode+'|BI CHG',@CAVV,@ResponseCode,@ForwardInstID,
   (case when @ExciseDutyAmount > 0 then '003' else NULL end), 
   (case when @ExciseDutyAmount > 0 then @ExciseDutyPercentage else NULL end),
   @ClientID,@AccountID,@mProductID
  )          
           
  Insert Into BalChk           
  (          
   OurBranchID, AccountID, ProductID, [Name], LastUpdateTime, ClearBalance, Limit, Effects, Shadow, FreezeAmt, MinBalance, TotalBalance, AvailableBalance          
  )          
  SELECT OurBranchID, AccountID, ProductID, [Name], @mwDate + ' ' + convert(varchar(12), GetDate(), 114), ISNULL(ClearBalance,0), ISNULL(Limit,0), ISNULL(Effects,0),           
  ISNULL(ShadowBalance,0), ISNULL(FreezeAmount,0), @MinBalance,           
  (isnull(@Limit,0)+ISNULL(ClearBalance,0)-ISNULL(Effects,0)),          
  (isnull(@Limit,0)+ISNULL(ClearBalance,0)-ISNULL(FreezeAmount,0)-@MinBalance)          
  FROM t_AccountBalance WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID          
           
  IF (@ExciseDutyAmount > 0)          
  BEGIN          
            
   Set @mSerialNo = @mSerialNo + 1          
   Set @mTrxType = 'D'          
   Set @IsCredit = 0          
             
   --SELECT @vCount4=count(*) From t_ATM_Branches WHERE OurBranchID = @OurBranchID AND BranchID = @WithdrawlBranchID          
            
   INSERT INTO t_ATM_TransferTransaction          
   (            
    ScrollNo,SerialNo,RefNo,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,          
    Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,[Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MerchantType,STAN,          
    SettlmntAmount,ConvRate,AcqCountryCode,CurrCodeTran,CurrCodeSett,ATMStatus,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,          
    CardAccptID,CardAccptNameLoc,VISATrxID,ProcCode,CAVV,ResponseCode,ForwardInstID,ClearingRefNo
   )              
   VALUES            
   (            
    @mScrollNo,@mSerialNo,@RefNo+'|CHGS2',@OurBranchID,@WithdrawlBranchID,@AccountID,@mAccountName,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,@mwDate,          
    @mwDate + ' ' + convert(varchar(12), GetDate(), 114),@PHXDate,@mTrxType,@Supervision,NULL,@ExciseDutyAmount,@mForeignAmount,@mExchangeRate,          
    @mDescriptionIDChg2,@mDescriptionHCharges,@ATMID,@mBankCode,@mRemarksCharges2,@mOperatorID,@mSupervisorID,@MerchantType,@STAN,@SettlmntAmount,          
    @ConvRate,@AcqCountryCode,@CurrCodeTran,@CurrCodeSett,@Supervision,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@RetRefNum,@AuthIDResp,          
    @CardAccptID,@CardAccptNameLoc,@VISATrxID,@ProcCode,@CAVV,@ResponseCode,@ForwardInstID,@RefNo
   )          
            
   INSERT INTO t_Transactions          
   (            
    OurBranchID,ScrollNo,SerialNo,RefNo,wDate,AccountType,DocumentType,AccountID,AccountName,ProductID,CurrencyID,ValueDate,TrxType,ChequeID,ChequeDate,Amount,          
    ForeignAmount,ExchangeRate,ProfitLoss,MeanRate,DescriptionID,[Description],BankCode,BranchCode,TrxPrinted,GLID,[Status],IsLocalCurrency,OperatorID,          
    SupervisorID,AdditionalData,IsMainTrx,DocType,GLVoucherID,Remarks,MerchantType,STAN,SettlmntAmount,ConvRate,AcqCountryCode,CurrCodeTran,CurrCodeSett,ATMStatus,          
    POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,CardAccptID,CardAccptNameLoc,VISATrxID,ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,ForwardInstID,ClearingRefNo, VATID,[Percent],TClientId,TAccountid,TProductId,VatReferenceID
   )            
   VALUES             
   (            
    @OurBranchID,@mScrollNo,@mSerialNo,@RefNo+'|CHGS2',@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@mAccountType,'ATT',@AccountID,@mAccountName,@mProductID,          
    @mCurrencyID,@mwDate,@mTrxType,'',@mwDate,@ExciseDutyAmount,@mForeignAmount,@mExchangeRate,0,@mExchangeRate,@mDescriptionIDChg2,          
    @mDescriptionHCharges,@mBankCode,@WithdrawlBranchID,0,NULL,@Supervision,@mIsLocalCurrency,@mOperatorID,@mSupervisorID,          
    null,'1','ATT',@VoucherID,@mRemarksCharges2,@MerchantType,@STAN,@SettlmntAmount,@ConvRate,@AcqCountryCode,@CurrCodeTran,@CurrCodeSett,@Supervision,          
    @POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@RetRefNum,@AuthIDResp,@CardAccptID,@CardAccptNameLoc,@VISATrxID,@ChannelId,@ChannelRefID,          
    @ProcCode+'|BI CHG',@CAVV,@ResponseCode,@ForwardInstID,@RefNo,
	(case when @ExciseDutyAmount > 0 then '003' else NULL end), 
    (case when @ExciseDutyAmount > 0 then @ExciseDutyPercentage else NULL end),
    @ClientID,@AccountID,@mProductID,
	(case when @ExciseDutyAmount > 0 then @VatReferenceID else NULL end)
   ) 
   
   --set @mScrollNo = 0 for @GLControl before @mSerialNo
            
   insert into t_GLTransactions           
   (          
    OurBranchID,RefNo,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,ExchangeRate,IsCredit,TransactionType,          
    TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,IsMainTrx,DocType,Remarks,MerchantType,STAN,SettlmntAmount,ConvRate,          
    AcqCountryCode,CurrCodeTran,CurrCodeSett,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,CardAccptID,CardAccptNameLoc,VISATrxID,          
    ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,ForwardInstID, VATID,[Percent],TClientId,TAccountid,TProductId
   )            
   values           
   (          
    @OurBranchID,@RefNo+'|CHGS2',@GLControl,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@mwDate,@mDescriptionIDChg2,          
    @mDescriptionHCharges,@mCurrencyID,@ExciseDutyAmount,@mForeignAmount,@mExchangeRate,@IsCredit,'ATMTransfer','L',@mOperatorID,          
    @mSupervisorID, '',0,@mSerialNo,null,'0','ATT',@mRemarksCharges2,@MerchantType,@STAN,@SettlmntAmount,@ConvRate,@AcqCountryCode,@CurrCodeTran,          
    @CurrCodeSett,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@RetRefNum,@AuthIDResp,@CardAccptID,@CardAccptNameLoc,@VISATrxID,@ChannelId,          
    @ChannelRefID,@ProcCode+'|BI CHG',@CAVV,@ResponseCode,@ForwardInstID,
	(case when @ExciseDutyAmount > 0 then '003' else NULL end), 
    (case when @ExciseDutyAmount > 0 then @ExciseDutyPercentage else NULL end),
    @ClientID,@AccountID,@mProductID
   )          
            
   -- Credit GL Excise Duty Account           
            
   SELECT @gAccountID=AccountID, @gAccountName=[Description],@gProductID='GL', @gCurrencyID=CurrencyID,@gAccountType='G' FROM t_GL             
   WHERE (OurBranchID = @OurBranchID) AND (AccountID =  @ExciseDutyAccountID)            
            
   SET @mSerialNo = @mSerialNo + 1            
   SET @mTrxType = 'C'            
   SET @IsCredit = 1          
            
   INSERT INTO t_ATM_TransferTransaction          
   (            
    ScrollNo,SerialNo,RefNo,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,          
    Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,[Description],ATMID,BankCode,Remarks,OperatorID,SupervisorID,MerchantType,STAN,          
    SettlmntAmount,ConvRate,AcqCountryCode,CurrCodeTran,CurrCodeSett,ATMStatus,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,          
    CardAccptID,CardAccptNameLoc,VISATrxID,ProcCode,CAVV,ResponseCode,ForwardInstID,ClearingRefNo
   )              
   VALUES            
   (            
    @mScrollNo,@mSerialNo,@RefNo+'|CHGS2',@OurBranchID,@WithdrawlBranchID,@gAccountID,@gAccountName,@gAccountType,@gProductID,@gCurrencyID,@mIsLocalCurrency,@mwDate,          
    @mwDate + ' ' + convert(varchar(12), GetDate(), 114),@PHXDate,@mTrxType,@Supervision,NULL,@ExciseDutyAmount,@mForeignAmount,@mExchangeRate,          
    @mDescriptionIDChg2,@mDescriptionHCharges,@ATMID,@mBankCode,@mRemarksCharges2,@mOperatorID,@mSupervisorID,@MerchantType,@STAN,@SettlmntAmount,          
    @ConvRate,@AcqCountryCode,@CurrCodeTran,@CurrCodeSett,@Supervision,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@RetRefNum,@AuthIDResp,          
    @CardAccptID,@CardAccptNameLoc,@VISATrxID,@ProcCode,@CAVV,@ResponseCode,@ForwardInstID,@RefNo
   )          
            
   insert into t_GLTransactions           
   (          
    OurBranchID,RefNo,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,ExchangeRate,IsCredit,TransactionType,          
    TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,IsMainTrx,DocType,Remarks,MerchantType,STAN,SettlmntAmount,ConvRate,          
    AcqCountryCode,CurrCodeTran,CurrCodeSett,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,CardAccptID,CardAccptNameLoc,VISATrxID,          
    ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,ForwardInstID, VATID,[Percent],TClientId,TAccountid,TProductId,VatReferenceID
   )            
   values           
   (          
    @OurBranchID,@RefNo+'|CHGS2',@gAccountID,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@mwDate,@mDescriptionIDChg2,          
    @mDescriptionHCharges,@mCurrencyID,@ExciseDutyAmount,@mForeignAmount,@mExchangeRate,@IsCredit,'ATMTransfer','L',@mOperatorID,          
    @mSupervisorID, '',@mScrollNo,@mSerialNo,null,'0','ATT',@mRemarksCharges2,@MerchantType,@STAN,@SettlmntAmount,@ConvRate,@AcqCountryCode,@CurrCodeTran,          
    @CurrCodeSett,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@RetRefNum,@AuthIDResp,@CardAccptID,@CardAccptNameLoc,@VISATrxID,@ChannelId,          
    @ChannelRefID,@ProcCode+'|BI CHG',@CAVV,@ResponseCode,@ForwardInstID,
	(case when @ExciseDutyAmount > 0 then '003' else NULL end), 
    (case when @ExciseDutyAmount > 0 then @ExciseDutyPercentage else NULL end),
    @ClientID,@AccountID,@mProductID,
	(case when @ExciseDutyAmount > 0 then @VatReferenceID else NULL end)
   )          
            
   Insert Into BalChk           
   (          
    OurBranchID, AccountID, ProductID, [Name], LastUpdateTime, ClearBalance, Limit, Effects, Shadow, FreezeAmt, MinBalance, TotalBalance, AvailableBalance          
   )          
   SELECT OurBranchID, AccountID, ProductID, [Name], @mwDate + ' ' + convert(varchar(12), GetDate(), 114), ISNULL(ClearBalance,0), ISNULL(Limit,0), ISNULL(Effects,0),           
   ISNULL(ShadowBalance,0), ISNULL(FreezeAmount,0), @MinBalance,           
   (isnull(@Limit,0)+ISNULL(ClearBalance,0)-ISNULL(Effects,0)),          
   (isnull(@Limit,0)+ISNULL(ClearBalance,0)-ISNULL(FreezeAmount,0)-@MinBalance)          
   FROM t_AccountBalance WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID          
            
  END          
       
  SELECT @ClearBalance=IsNull(ab.ClearBalance,0),@vClearedEffects=IsNull(ab.Effects,0),@mAccountName=ab.Name,@Limit=ISNULL(ab.Limit,0),    
  @mAccountType='C',@mProductID=ab.ProductID, @mCurrencyID=c.CurrencyID,@vFreezeAmount=ISNULL(ab.FreezeAmount,0),@vIsClose=ISNULL(a.Status,''),          
  @value=ISNULL(p.productatmcode,'00'), @MinBalance=ISNULL(p.MinBalance,0), @CurrencyCode = ISNULL(c.CurrencyCode,'')          
  FROM t_AccountBalance ab           
  INNER JOIN t_Account a ON ab.OurBranchID = a.OurBranchID AND ab.AccountID = a.AccountID AND ab.ProductID = a.ProductID          
  INNER JOIN t_Products p ON ab.OurBranchID = p.OurBranchID AND ab.ProductID = p.ProductID          
  INNER JOIN t_Currencies c ON ab.OurBranchID = c.OurBranchID AND p.CurrencyID = c.CurrencyID          
  WHERE (ab.OurBranchID = @OurBranchID) AND (ab.AccountID = @AccountID)            
            
  --exec @Limit = [fnc_GetLimit] @OurBranchID, @AccountID          
           
  IF @Limit > 0 AND @vCheckLimit = 1 AND @vODLimitAllow = 1          
  BEGIN          
   SET @ClearBalance=(@ClearBalance+@Limit)          
  END          
           
 END            
           
 SELECT @Status    = 0,           
 @AvailableBalance = ( isnull(@ClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@MinBalance,0) )*100,          
 @ClearBalance     = ( IsNull(@ClearBalance,0)-IsNull(@vClearedEffects,0) )*100
            
 SELECT @Status As Status,@ClearBalance As ClearBalance,@AvailableBalance as  AvailableBalance ,@Value as value,           
 @CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp, @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
 RETURN 0
END