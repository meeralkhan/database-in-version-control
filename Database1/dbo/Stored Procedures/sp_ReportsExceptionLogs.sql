﻿
CREATE   PROCEDURE [dbo].[sp_ReportsExceptionLogs]
(
@ErrorStartTime		nvarchar(max) = NULL,
@Exception			nvarchar(max) = NULL,
@InnerException		nvarchar(max) = NULL,
@RequestParams		nvarchar(max) = NULL,
@level				nvarchar(max) = NULL,
@ErrorEndTime		nvarchar(max) = NULL
)
AS


BEGIN
	INSERT INTO t_ReportsExceptionLogs (ErrorStartTime, Exception, InnerException, RequestParams, level, ErrorEndTime)
	VALUES (@ErrorStartTime,@Exception,@InnerException,@RequestParams,@level,@ErrorEndTime)
END