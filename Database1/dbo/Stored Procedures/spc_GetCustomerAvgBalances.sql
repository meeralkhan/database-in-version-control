﻿

CREATE PROCEDURE [dbo].[spc_GetCustomerAvgBalances]  
(  
 @OurBranchID nvarchar(30),  
 @StartDate datetime,  
 @EndDate datetime  
)  
AS  
begin try  
    
  set nocount on  
    
  declare @AccountID varchar(30)  
  declare @ProductID varchar(30)  
  declare @CurrencyID varchar(30)  
  declare @AccountType char(1)  
    
  declare @OpeningBalance money  
  declare @TotalCredit money  
  declare @TotalDebit money  
    
  declare @Month datetime  
  declare @StartDates datetime  
    
  set @StartDates = @StartDate  
  set @Month = DATEADD(DAY, -(DAY(@StartDates)-1), @StartDates)  
    
  DELETE FROM t_AccountDailyBalance WHERE OurBranchID = @OurBranchID  
    
  declare curs CURSOR for  
  select a.OurBranchID, a.AccountID, p.ProductID, p.CurrencyID, isnull(a.ClearBalance, 0)  
  from t_AccountBalance a  
  inner join t_Products p ON a.OurBranchID = p.OurBranchID and a.ProductID = p.ProductID  
  left join t_AccountMonthBalances m ON a.OurBranchID = m.OurBranchID and a.AccountID = m.AccountID and m.Month = @Month  
  where a.OurBranchID = @OurBranchID and ISNULL(p.AverageBalProcessing, 0) = 1  
    
  open curs  
  FETCH NEXT FROM curs INTO @OurBranchID, @AccountID, @ProductID, @CurrencyID, @OpeningBalance  
    
  WHILE (@@FETCH_STATUS = 0)  
  BEGIN  
      
    set @StartDates = @StartDate  
    WHILE (@StartDates <= @EndDate)  
    BEGIN  
        
   SELECT @TotalCredit = ISNULL(SUM(Amount), 0) from t_Transactions where OurBranchID = @OurBranchID AND AccountId = @AccountId AND ProductId = @ProductId AND CONVERT(VARCHAR(10), wDate, 101) = CONVERT(VARCHAR(10), @StartDates, 101) AND TrxType = 'C'  
     
   SELECT @TotalDebit= ISNULL(SUM(Amount), 0) from t_Transactions where OurBranchID = @OurBranchID AND AccountId = @AccountId AND ProductId = @ProductId AND CONVERT(VARCHAR(10), wDate, 101) = CONVERT(VARCHAR(10), @StartDates, 101) AND TrxType = 'D'  
     
   SET @OpeningBalance = ((@OpeningBalance + @TotalCredit) - @TotalDebit)  
     
   INSERT INTO t_AccountDailyBalance values (@OurBranchID, @AccountID, @ProductID, @StartDates, @OpeningBalance)  
     
   SET @StartDates = DATEADD(DAY, +1, @StartDates)  
       
    END  
      
    FETCH NEXT FROM curs INTO @OurBranchID, @AccountID, @ProductID, @CurrencyID, @OpeningBalance  
      
  END  
    
  close curs  
  deallocate curs  
  
end try  
begin catch  
    
  close curs  
  deallocate curs  
    
end catch