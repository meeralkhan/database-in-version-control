﻿CREATE PROCEDURE [dbo].[spc_GetGLStatementFCYControl]                  
(                    
 @OurBranchID  varchar(30),                    
 @AccountID varchar(30),                    
 @sDate          Datetime,                    
 @eDate          Datetime                    
)                    
                    
AS                    
                    
  SET NOCOUNT ON                    
                    
   DECLARE @OpeningBalance as money                    
   DECLARE @LocEqOpBal as money                    
                    
   DECLARE @crOpBal as money                    
   DECLARE @crLocEqOpBal as money                    
   DECLARE @drOpBal as money                    
   DECLARE @drLocEqOpBal as money                    
                    
   DECLARE @WorkingDate as datetime                    
                    
   SELECT @WorkingDate = WorkingDate From t_Last                    
   WHERE OurBranchID = @OurBranchID                    
                    
 SET @OpeningBalance = 0                    
 SET @crOpBal = 0                    
 SET @drOpBal = 0                    
                    
 SET @LocEqOpBal = 0                    
 SET @crLocEqOpBal = 0                    
 SET @drLocEqOpBal = 0                    
                    
                    
   SELECT @OpeningBalance = IsNull(ForeignOpeningBalance,0),@LocEqOpBal = IsNull(OpeningBalance,0)                    
   FROM t_GL                    
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                    
                    
   SELECT @crOpBal = IsNull(SUM(ForeignAmount),0), @crLocEqOpBal = IsNull(SUM(Amount),0) From t_GLTransactions                    
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                     
         AND IsCredit = '1' and [Status] <> 'R'            
  --AND convert(varchar(10) , Date,101) < @sDate                    
   AND Date < @sDate                    
                    
   SELECT @drOpBal = IsNull(SUM(ForeignAmount),0), @drLocEqOpBal = IsNull(SUM(Amount),0) From t_GLTransactions                    
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                     
         AND IsCredit = '0' and [Status] <> 'R'            
  --AND convert(varchar(10) , Date,101) < @sDate                    
  AND Date < @sDate                    
                    
   SET @OpeningBalance = @OpeningBalance + @crOpBal - @drOpBal                    
   SET @LocEqOpBal = @LocEqOpBal + @crLocEqOpBal - @drLocEqOpBal                    
                    
   IF @WorkingDate = @eDate                     
      BEGIN                    
                    
         SELECT '' OperatorID,'' SupervisorID,'01/01/1900' as wDate, '01/01/1900' as TrxTimeStamp, @sDate as ValueDate ,'Opening Balance' as  Description,'' as ChequeID, '' as TrxType, IsNull(@LocEqOpBal,0) as Amount, 
		 IsNull(@OpeningBalance,0) as ForeignAmount,'' as RefNo               
    
       
               ,'OP' As TableStr,0 ScrollNo ,'' DocumentType, '' Remarks    ,'' ChannelRefID    
                    
         UNION ALL                    
                    
         SELECT OperatorID,SupervisorID,Date as wDate, TrxTimeStamp, ValueDate , Description,'' as ChequeID,CASE IsCredit WHEN 1 THEN 'C' ELSE 'D' END As TrxType, Amount, ForeignAmount,'' As RefNo                    
               ,'H' As TableStr,ScrollNo ,TransactionType DocumentType, Remarks    , ChannelRefID    
         FROM t_GLTransactions                    
         WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID and [Status] <> 'R'            
            AND convert(varchar(10) , Date,101) >= @sDate AND convert(varchar(10) , Date,101) <= @eDate                    
                    order by TrxTimeStamp asc
/*            
         UNION ALL                    
            
--t_CashTransactionModel                    
         SELECT wDate,ValueDate , Description, ChequeID,TrxType, Amount, ForeignAmount, RefNo                    
                  ,'C' As TableStr,ScrollNo ,'C' DocumentType                             FROM t_CashTransactionModel                    
         WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                           
               AND Supervision = 'C'                    
                  
         UNION ALL                    
                    
--t_CashTransactionModel Credit Side                    
         SELECT wDate,ValueDate , Description, ChequeID,'D' as TrxType, Amount, ForeignAmount, RefNo                    
                  ,'CCr' As TableStr, ScrollNo ,'C' DocumentType                    
         FROM t_CashTransactionModel                    
         WHERE OurBranchID = @OurBranchID AND GLID = @AccountID                           
               AND Supervision = 'C' AND TrxType = 'C'                    
                    
         UNION ALL                    
            --t_CashTransactionModel Debit Side                    
         SELECT wDate,ValueDate , Description, ChequeID,'C' as TrxType, Amount, ForeignAmount, RefNo                    
                  ,'CDr' As TableStr,ScrollNo ,'C' DocumentType                    
         FROM t_CashTransactionModel                    
         WHERE OurBranchID = @OurBranchID AND GLID = @AccountID                           
               AND Supervision = 'C' AND TrxType = 'D'                    
                    
         UNION ALL                    
            
--t_TransferTransactionModel                    
         SELECT wDate,ValueDate , Description, ChequeID,TrxType, Amount, ForeignAmount, RefNo                    
                  ,'T' As TableStr,ScrollNo ,'T' DocumentType                    
         FROM t_TransferTransactionModel                    
         WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                           
               AND Supervision = 'C'  AND AccountType ='G'                                   
                    
        UNION ALL                    
            
--t_InWardClearing                    
         SELECT wDate,ValueDate , Description, ChequeID,TransactionType as TrxType, Amount, ForeignAmount, '' as RefNo                    
                  ,'I' As TableStr,SerialNo ScrollNo , 'ID' DocumentType                    
         FROM t_InWardClearing                    
         WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                           
               AND Status <>'R'  AND AccountType ='G'                    
                    
         UNION ALL                    
                    
                    
--t_InWardClearing Main Office Account With Gl Parameter Serial No - 2                    
         SELECT wDate,ValueDate , Description, ChequeID,'C' as TrxType, Amount, ForeignAmount, '' as RefNo                    
                  ,'IMo' As TableStr,SerialNO ScrollNo ,'ID' DocumentType                    
         FROM t_InWardClearing                    
         WHERE OurBranchID = @OurBranchID AND AccountID = (SELECT AccountID From t_GLParameters WHERE SerialID ='2' AND OurBranchID = @OurBranchID )                    
               AND Status <>'R'  AND AccountType ='G'                    
            
         UNION ALL                    
                            
--t_ATM_CashTransaction                    
         SELECT wDate,ValueDate , Description,'' as ChequeID,TrxType, Amount, ForeignAmount, RefNo                    
                  ,'ATM' As TableStr,ScrollNo ,'AT' DocumentType                    
         FROM t_ATM_CashTransaction                    
         WHERE OurBranchID = @OurBranchID AND GLID = @AccountID                           
               AND Supervision = 'C'                    
                    
         UNION ALL                    
                            
--'t_ATM_TransferTransaction                    
         SELECT wDate,ValueDate , Description,'' as ChequeID,TrxType, Amount, ForeignAmount, RefNo                    
                  ,'ATMT' As TableStr,ScrollNo ,'AT' DocumentType                    
         FROM t_ATM_TransferTransaction                    
         WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                           
               AND Supervision = 'C' AND AccountType ='G'                    
                    
         UNION ALL                    
                            
--'t_OnLine_CashTransaction           
         SELECT wDate,ValueDate , Description, ChequeID,TrxType, Amount, ForeignAmount, RefNo                    
                  ,'OL' As TableStr,ScrollNo ,'OL' DocumentType                    
         FROM t_OnLineCashTransaction                    
         WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                           
               AND Supervision <> 'R' AND AccountType ='G'                    
              
--/////////////////////////////////                    
--Control Account                    
   UNION ALL                    
            
--t_CashTransactionModel                    
         SELECT wDate,ValueDate , Description, ChequeID,TrxType, Amount, ForeignAmount, RefNo                    
                  ,'C1' As TableStr,ScrollNo ,'C' DocumentType                    
         FROM t_CashTransactionModel                    
         WHERE OurBranchID = @OurBranchID AND ProductID IN (SELECT Distinct ProductID  From t_Products WHERE OurBranchID = @OurBranchID AND GLControl = @AccountID )             
               AND Supervision = 'C'                    
                    
         UNION ALL                    
            
--t_TransferTransactionModel                    
         SELECT wDate,ValueDate , Description, ChequeID,TrxType, Amount, ForeignAmount, RefNo                    
                  ,'T1' As TableStr,ScrollNo ,'T' DocumentType                    
         FROM t_TransferTransactionModel                    
         WHERE OurBranchID = @OurBranchID  AND ProductID IN (SELECT Distinct ProductID  From t_Products WHERE OurBranchID = @OurBranchID AND GLControl = @AccountID )                    
               AND Supervision = 'C'                     
                    
        UNION ALL                    
            
--t_InWardClearing                
         SELECT wDate,ValueDate , Description, ChequeID,TransactionType as TrxType, Amount, ForeignAmount, '' as RefNo                    
                  ,'I1' As TableStr,SerialNo ScrollNo , 'ID' DocumentType                    
         FROM t_InWardClearing                    
         WHERE OurBranchID = @OurBranchID AND ProductID IN (SELECT Distinct ProductID  From t_Products WHERE OurBranchID = @OurBranchID AND GLControl = @AccountID )                    
               AND Status <>'R'                    
            
         UNION ALL                    
                            
--t_ATM_CashTransaction                    
         SELECT wDate,ValueDate , Description,'' as ChequeID,TrxType, Amount, ForeignAmount, RefNo                    
                  ,'ATM1' As TableStr,ScrollNo ,'AT' DocumentType                    
       FROM t_ATM_CashTransaction                    
         WHERE OurBranchID = @OurBranchID  AND ProductID IN (SELECT Distinct ProductID  From t_Products WHERE OurBranchID = @OurBranchID AND GLControl = @AccountID )                    
               AND Supervision = 'C'                    
                    
         UNION ALL                    
                            
--'t_ATM_TransferTransaction                    
         SELECT wDate,ValueDate , Description,'' as ChequeID,TrxType, Amount, ForeignAmount, RefNo                    
                  ,'ATMT1' As TableStr,ScrollNo ,'AT' DocumentType                    
         FROM t_ATM_TransferTransaction                    
         WHERE OurBranchID = @OurBranchID  AND ProductID IN (SELECT Distinct ProductID  From t_Products WHERE OurBranchID = @OurBranchID AND GLControl = @AccountID )                    
               AND Supervision = 'C'                    
                    
         UNION ALL                    
                     
--'t_OnLine_CashTransaction                    
         SELECT wDate,ValueDate , Description, ChequeID,TrxType, Amount, ForeignAmount, RefNo                    
                  ,'OL1' As TableStr,ScrollNo ,'OL' DocumentType                    
         FROM t_OnLineCashTransaction                    
         WHERE OurBranchID = @OurBranchID  AND ProductID IN (SELECT Distinct ProductID  From t_Products WHERE OurBranchID = @OurBranchID AND GLControl = @AccountID )                    
               AND Supervision <> 'R'                    
*/            
      END                    
                    
   ELSE -- If Statement not in workingdate                    
                    
      BEGIN                    
                    
         SELECT '' OperatorID,'' SupervisorID,'01/01/1900' as wDate, '01/01/1900' as TrxTimeStamp, @sDate as ValueDate ,'Opening Balance' as  Description,'' as ChequeID, '' as TrxType, IsNull(@LocEqOpBal,0) as Amount, 
		 IsNull(@OpeningBalance,0) as ForeignAmount , '' as RefNo            
    
          
               ,'OP' As TableStr,0 ScrollNo ,'' DocumentType, '' Remarks    ,'' ChannelRefID    
                   
         UNION ALL                    
                    
         SELECT  OperatorID,SupervisorID,Date as wDate, TrxTimeStamp,ValueDate , Description,'' as ChequeID,CASE IsCredit WHEN 1 THEN 'C' ELSE 'D' END As TrxType, Amount, ForeignAmount, '' as RefNo                    
               ,'H' As TableStr,ScrollNo ,TransactionType DocumentType, Remarks    , ChannelRefID    
         FROM t_GLTransactions                    
         WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID and [Status] <> 'R'            
               AND convert(varchar(10) , Date,101) >= @sDate AND convert(varchar(10) , Date,101) <= @eDate                    
                        order by TrxTimeStamp asc
      END