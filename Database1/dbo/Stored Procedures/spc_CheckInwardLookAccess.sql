﻿
--spc_CheckInwardLookAccess '2027', 'admin', 'R'

CREATE PROCEDURE [dbo].[spc_CheckInwardLookAccess]
 
 @OurBranchID varchar(4),
 @OperatorID varchar(15),
 @Type char(1)
 
AS
 
 SET NOCOUNT ON
 
 if @Type = 'H'
 begin
   If Exists(Select DebitSupervisionLimit from t_MapDrive Where (OurBranchID = @OurBranchID) 
   and (OperatorID = @OperatorID) and (FormToRun = 'frmInwardHonoured'))
   begin
     Select '1' As ReturnValue,'H' as Type,AddAccess,EditAccess,DeleteAccess,DebitSupervisionLimit
     from t_MapDrive Where (OurBranchID = @OurBranchID) and (OperatorID = @OperatorID)
     and (FormToRun = 'frmInwardHonoured')
   end
   else
   begin
     Select '0' As ReturnValue
   end
 end
 else if @Type = 'R'
 begin
   If Exists(Select DebitSupervisionLimit from t_MapDrive Where (OurBranchID = @OurBranchID) 
   and (OperatorID = @OperatorID) and (FormToRun = 'frmInwardReturned'))
   begin
     Select '1' As ReturnValue,'R' as Type,AddAccess,EditAccess,DeleteAccess,DebitSupervisionLimit
     from t_MapDrive Where (OurBranchID = @OurBranchID) and (OperatorID = @OperatorID)
     and (FormToRun = 'frmInwardReturned')
   end
   else
   begin
     Select '0' As ReturnValue
   end
 end
 else
 begin
   Select '0' As ReturnValue
 end