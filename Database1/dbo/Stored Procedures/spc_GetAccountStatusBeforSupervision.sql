﻿CREATE PROCEDURE [dbo].[spc_GetAccountStatusBeforSupervision]        
(        
 @OurBranchID varchar(30),        
 @AccountID varchar(30),        
 @TrxType varchar(5)        
)        
        
AS        
 SET NOCOUNT ON        
        
 DECLARE @Status as Char(1)        
 DECLARE @ProductID as Varchar(6)        
 DECLARE @CurrencyID as Varchar(4)        
        
 DECLARE @ClearBalance as Money        
 DECLARE @Effects as Money        
 DECLARE @ShadowBalance as Money        
 DECLARE @Limit as Money        
 DECLARE @BlockedAmount as Money        
 DECLARE @ProductMinBalance AS MONEY    
        
 DECLARE @IsFreezed as Bit        
 DECLARE @IsProductFreezed as Bit        
        
 DECLARE @AllowCrDr as Bit        
 DECLARE @CanGoCrDr as Bit        
        
 IF (Select Count(AccountID) From t_AccountBalance WHERE   OurBranchID = @OurBranchID AND AccountID = @AccountID) > 0         
  BEGIN        
        
   SELECT @Status=IsNull(Status,''), @ProductID=ProductID        
   FROM t_Account        
   WHERE   OurBranchID = @OurBranchID AND AccountID = @AccountID        
        
        
   IF IsNull(@Status,'') <> ''        
    BEGIN  
      IF @TrxType = 'C'  
      begin  
        IF @Status <> 'T'  
        begin  
          SELECT @Status AS RetStatus --Ret. Status  
          RETURN(1)  
        end  
      end  
      else  
      begin  
        SELECT @Status AS RetStatus --Ret. Status  
        RETURN(1)  
      end  
    END           
        
   SELECT  @ClearBalance=ClearBalance, @Effects=Effects, @ShadowBalance=ShadowBalance,@Limit = Limit,        
    @BlockedAmount = CASE IsFreezed WHEN 1 THEN FreezeAmount        
         WHEN 0 THEN 0        
       END,        
     @IsFreezed = IsFreezed        
   FROM t_AccountBalance         
   WHERE   OurBranchID = @OurBranchID AND AccountID = @AccountID        
        
   SELECT @CurrencyID= UPPER(CurrencyID), @IsProductFreezed=IsFreezed, @ProductMinBalance= ISNULL(MinBalance,0),     
   @AllowCrDr = Case @TrxType WHEN 'C' THEN AllowCredit    
           WHEN 'D' THEN AllowDebit        
                  END ,        
       @CanGoCrDr = Case @TrxType     WHEN 'C' THEN CanGoInCredit          
              WHEN 'D' THEN CanGoInDebit        
                  END         
   FROM t_Products        
   WHERE   OurBranchID = @OurBranchID AND ProductID = @ProductID        
        
   IF @IsProductFreezed = 1              
    BEGIN        
     SELECT '2' AS RetStatus -- 2 - Product Is Freezed        
     RETURN(0)             
    END        
          
   IF @AllowCrDr = 0        
     BEGIN        
      SELECT '4' AS RetStatus -- 4 - In Product Condition Dr/Cr Trx Not Allowed        
      RETURN(0)             
     END            
         
   SELECT @AllowCrDr = Case @TrxType WHEN 'C' THEN AllowCreditTransaction WHEN 'D' THEN AllowDebitTransaction END        
     FROM t_Account      
     WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID        
             
     IF @AllowCrDr = 1        
      BEGIN        
       SELECT '3' AS RetStatus -- 3 - In Account Special Condition Dr/Cr Transaction Not Allowed        
       RETURN(0)             
      END        
        
   SELECT '1' as RetStatus , @ClearBalance as ClearBalance, @Effects as Effects,         
     @ShadowBalance as ShadowBalance, @Limit as Limit,        
     @ProductID as ProductID, @CurrencyID as CurrencyID,    
     @ProductMinBalance AS ProductMinBalance,    
     @BlockedAmount as BlockedAmount,        
     @CanGoCrDr as CanGoCrDr, @IsFreezed as IsFreezed        
             
            
  END        
 ELSE        
  BEGIN        
   SELECT 0 AS RetStatus --Account Does Not Exists        
   RETURN(0)        
  END