﻿CREATE PROCEDURE [dbo].[spc_CIFHierarchy]
(
  @OurBranchID nvarchar(30),
  @ClientID nvarchar(30)
)
AS

SET NOCOUNT ON

declare @CID NVARCHAR(30)

CREATE TABLE #CIFs ( ClientID NVARCHAR(30), ParentClientID NVARCHAR(30) );

CREATE TABLE #Temps (ClientID NVARCHAR(30), ParentClientID NVARCHAR(30) );

CREATE TABLE #Temps1 (ClientID NVARCHAR(30), ParentClientID NVARCHAR(30) );

CREATE TABLE #Temps2 (ClientID NVARCHAR(30), ParentClientID NVARCHAR(30) );

insert into #CIFs
select ClientID, ISNULL(ParentClientID,'') from t_Customer where OurBranchID = @OurBranchID AND (ClientID = @ClientID OR ParentClientID = @ClientID)

insert into #Temps
select * from #CIFs where ClientID != @ClientID

insert into #Temps2
select * from #CIFs where ParentClientID != @ClientID

WHILE exists (select ClientID from #Temps)
BEGIN
    --select * from #Temps

	select top 1 @CID = ClientID FROM #Temps
	
	insert into #Temps1
	select ClientID, ISNULL(ParentClientID,'') from t_Customer
	where OurBranchID = @OurBranchID
	AND (ClientID <> @CID and ParentClientID = @CID)
	AND ClientID NOT IN (select ClientID from #CIFs)

	insert into #CIFs
	select * from #Temps1
	
	insert into #Temps
	select * from #Temps1

	--select * from #Temps

	delete from #Temps1
	delete from #Temps where ClientID = @CID
END

WHILE exists (select ParentClientID from #Temps2)
BEGIN
    
	select top 1 @CID = ParentClientID FROM #Temps2
	
	insert into #Temps1
	select ClientID, ISNULL(ParentClientID,'') from t_Customer
	where OurBranchID = @OurBranchID
	AND (ParentClientID <> @CID and ClientID = @CID)
	AND ParentClientID NOT IN (select ClientID from #CIFs)

	insert into #CIFs
	select * from #Temps1
	
	insert into #Temps2
	select * from #Temps1

	--select * from #Temps

	delete from #Temps1
	delete from #Temps2 where ParentClientID = @CID
END

select ClientID, ParentClientID from #CIFs order by ClientID