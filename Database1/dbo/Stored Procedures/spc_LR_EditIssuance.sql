﻿CREATE PROCEDURE [dbo].[spc_LR_EditIssuance] (  
 @OurBranchID varchar(30),    
 @SerialNo decimal(24,0),    
 @InstrumentType nvarchar(100),    
 @InstrumentNo decimal(24,0),    
 @ControlNo decimal(24,0) = 0  
)    
AS    
BEGIN  
  
  set nocount on
  
 DECLARE @ScrollNo as int    
 DECLARE @ScrollSerialNo as int    
 DECLARE @mInstrumentType as varchar(20)    
    
 BEGIN TRY  
  UPDATE t_LR_Issuance SET InstrumentNo=@InstrumentNo, ControlNo=@ControlNo, Erase=0    
  WHERE OurBranchID=@OurBranchID and SerialNo = @SerialNo  
  
  --SELECT @ScrollNo=ScrollNo  
  --FROM t_LR_Issuance    
  --WHERE OurBranchID=@OurBranchID and SerialNo = @SerialNo  
  
  --IF @InstrumentType = 'PO'  
  --BEGIN    
  --select * from t_TransferTransactionModel where ScrollNo = 62  
  -- UPDATE t_TransferTransactionModel SET [Description] = 'PO # ' + convert(varchar(15),@InstrumentNo)     
  -- WHERE OurBranchID=@OurBranchID and ScrollNo = @ScrollNo and SerialNo = @ScrollSerialNo  
  --END  
    
  --IF @InstrumentType = 'BC'  
  --BEGIN    
  -- UPDATE t_TransferTransactionModel SET [Description] = 'BC # ' + convert(varchar(15),@InstrumentNo)     
  -- WHERE OurBranchID=@OurBranchID and ScrollNo = @ScrollNo and SerialNo = @ScrollSerialNo  
  --END    
  
  --IF @InstrumentType = 'PS'  
  --BEGIN    
  -- UPDATE t_TransferTransactionModel SET     
  -- Description = 'PS # ' + convert(varchar(15),@InstrumentNo)     
  -- WHERE OurBranchID=@OurBranchID and ScrollNo = @ScrollNo and SerialNo = @ScrollSerialNo    
  --END    
     
  --IF @InstrumentType = 'DD'    
  --BEGIN  
  -- UPDATE t_TransferTransactionModel SET     
  -- Description = 'DD No. ' + convert(varchar(15),@InstrumentNo)  + '/' + convert(varchar(15),@ControlNo) + ' D/o ' + @DrawnBankID + ' / ' + @DrawnBranchID    
  -- WHERE OurBranchID=@OurBranchID and ScrollNo = @ScrollNo and SerialNo = @ScrollSerialNo    
  --END    
     
  --IF @InstrumentType = 'TT'    
  --BEGIN    
  -- UPDATE t_TransferTransactionModel SET     
  -- Description = 'TT No. ' +  convert(varchar(15),@ControlNo) + ' D/o ' + @DrawnBankID + ' / ' + @DrawnBranchID    
  -- WHERE OurBranchID=@OurBranchID and ScrollNo = @ScrollNo and SerialNo = @ScrollSerialNo    
  --END    
    
  select 'Ok' as ReturnStatus, 'SavedSuccessfully' as ReturnMessage  
    
 END TRY  
   
 BEGIN CATCH  
   
  select 'Error' as ReturnStatus, Error_Message() as ReturnMessage  
   
 END CATCH  
   
END