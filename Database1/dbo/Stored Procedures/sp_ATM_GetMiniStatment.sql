﻿CREATE PROCEDURE [dbo].[sp_ATM_GetMiniStatment]
(
	@BranchID 	varchar(30),
	@AccountID 	varchar(30)
	)

AS

	SET NOCOUNT ON
	DECLARE @Status as int	

	Declare @QueryString nvarchar(4000)
	Declare @CountToday int
	Declare @CountTodayTransaction int
	Declare @CountPrevious int
	Declare @Count as bit
	Declare @WorkingDate as datetime
	
	Select @WorkingDate = WorkingDate from t_Last Where OurBranchID = @BranchID

	SELECT @Count=Count(*)
	FROM t_AccountBalance
	WHERE (OurBranchID = @BranchID) AND (AccountID = @AccountID)
	
	IF @Count=0	
	BEGIN
		 
		SELECT @Status=1
		SELECT @Status  as Status
		RETURN (1)	
	END

	DELETE FROM t_ATM_Temp WHERE OurBranchID = @BranchID AND AccountID = @AccountID

	SELECT @CountToday = Count(*)
	FROM v_ATM_GetTodayTransactions
	WHERE (OurBranchID = @BranchID) AND (AccountID = @AccountID)
	
	IF @CountToday < 8
	BEGIN
		SELECT @CountPrevious = (8 - @CountToday)
	END
	ELSE
	BEGIN
		SELECT @CountPrevious = 0
	END
		
	IF @CountPrevious > 0
	BEGIN			
		SELECT @QueryString ='  INSERT INTO dbo.t_ATM_Temp SELECT TOP ' + convert(char(3),@CountPrevious) + ' OurBranchID, AccountID, ValueDate, wDate, Amount, TrxType, TrxDesc, DocumentType '
		SELECT @QueryString= @QueryString + ' FROM t_Transactions'
		SELECT @QueryString= @QueryString + ' WHERE OurBranchID = ' + '''' + @BranchID + '''' + ' AND AccountID = ' + '''' + @AccountID + ''''
		SELECT @QueryString= @QueryString + ' AND CONVERT(varchar(10), wDate, 120) < ' + '''' + CONVERT(varchar(10), @WorkingDate, 120) + ''''
		SELECT @QueryString= @QueryString + ' AND ISNULL(Status,'''') <> ''R'' ORDER BY wDate DESC'
				
		EXEC (@QueryString)
	END
		
	IF @CountToday > 0
	BEGIN
		SELECT @QueryString ='  INSERT INTO  dbo.t_ATM_Temp '
		SELECT @QueryString= @QueryString + ' SELECT OurBranchID, AccountID, ValueDate, wDate, Amount, TrxType, TrxDesc, DocumentType '
		SELECT @QueryString= @QueryString + ' FROM v_ATM_GetTodayTransactions '
		SELECT @QueryString= @QueryString + ' WHERE OurBranchID = ' + '''' + @BranchID + '''' + ' AND AccountID = ' + '''' + @AccountID + ''''
		SELECT @QueryString= @QueryString + ' ORDER BY wDate DESC'
		EXEC (@QueryString)
	END
	
	SELECT ValueDate, wDate, Amount, DRCR, TrxType FROM (	
		SELECT TOP 8 ValueDate, wDate, Amount,  
				CASE TrxType 
					WHEN 'D' then '-' 
					WHEN 'C' then ' ' 
				END AS DRCR, 

				CASE DocumentType      	
					WHEN 'C' THEN  	
						Case TrxType  		
							When 'D' THEN 'CASH DEBIT' 		
							When 'C' THEN 'CASH CREDIT' 	
						END     
					WHEN 'T' THEN  	
						Case TrxType  		
							When 'D' THEN 'DEBIT TRANSFER' 		
							When 'C' THEN 'CREDIT TRANSFER' 	
						END      
					WHEN 'AT' THEN  	
						Case left(TrxDesc,2)
							When '01' THEN
								Case ltrim(substring(TrxDesc,4,20))
									WHEN '' THEN 
										Case TrxType
											WHEN 'D' THEN 'ATM CASH WD'
											WHEN 'C' THEN 'ATM CASH DEP'
										END
									ELSE ltrim(substring(TrxDesc,4,20))
								END
							When '85' THEN 'IBR-CW CR ADV'
							When '31' THEN 'BAL ENQ CHG'
							When '30' THEN 'BAL ENQ CHG'
							When '39' THEN
								Case ltrim(substring(TrxDesc,4,20))
									WHEN '' THEN 
										Case TrxType
											WHEN 'D' THEN 'IBR-FT DB'
											WHEN 'C' THEN 'IBR-FT CR'
										END
									ELSE ltrim(substring(TrxDesc,4,20))
								END
							When '40' THEN
								Case ltrim(substring(TrxDesc,4,20))
									WHEN '' THEN 
										Case TrxType
											WHEN 'D' THEN 'IBR-FT DB'
											WHEN 'C' THEN 'IBR-FT CR'
										END
									ELSE ltrim(substring(TrxDesc,4,20))
								END
							WHEN '41' THEN
								Case ltrim(substring(TrxDesc,4,20))
									WHEN '' THEN 
										Case TrxType
											WHEN 'D' THEN 'IBK-FT DB'
											WHEN 'C' THEN 'IBK-FT CR'
										END
									ELSE ltrim(substring(TrxDesc,4,20))
								END
							WHEN '48' THEN
								Case ltrim(substring(TrxDesc,4,20))
									WHEN '' THEN 
										Case TrxType
											WHEN 'D' THEN 'IBK-FT DB'
											WHEN 'C' THEN 'IBK-FT CR'
										END
									ELSE ltrim(substring(TrxDesc,4,20))
								END
							WHEN '53' THEN 'MINI STAT CHG'
							When '73' THEN
								Case ltrim(substring(TrxDesc,4,20))
									WHEN '' THEN 'BILL PAYMENT'
									ELSE ltrim(substring(TrxDesc,4,20))
								END
							ELSE ''
						END      
					WHEN '' THEN  	
						Case TrxType  		
							When 'D'THEN 'DEBIT' 		
							When 'C'THEN 'CREDIT' 	
						END      
					WHEN 'ID' THEN 'INWARD CLEARING'
					WHEN 'O' THEN 'OUTWARD CLEARING'
					WHEN 'OC' THEN  	
						Case TrxType  		
							When 'D' THEN 'ONLINE C-WITH'
							When 'C' THEN 'ONLINE C-DEPT' 	
						END     
					WHEN 'OT' THEN  	
						Case TrxType  		
							When 'D' THEN 'ONLINE TR-DEBIT'
							When 'C' THEN 'ONLINE TR-CREDIT'
						END     
				END AS TrxType  
		FROM t_ATM_Temp
		WHERE OurBranchID = @BranchID AND AccountID = @AccountID
			
		UNION ALL 

		SELECT '1/1/1900' AS ValueDate, '1/1/1900' AS wDate, (ClearBalance-ClearedEffects) AS Amount, '' AS TrxType, 
		'' AS DRCR
		FROM t_AccountBalance
		WHERE (OurBranchID = @BranchID) AND (AccountID = @AccountID)
			
	) AS ABC
	Order by ABC.wDate DESC
		
	SELECT @Status=@@ERROR

	IF @Status <> 0 
		BEGIN
			 
			RAISERROR('There Is An Error Occured While Balance Enquiry In The Local Branch',16,1) WITH SETERROR
			RETURN (5000)
		END
		
		SELECT @Status = 0 
		SELECT @Status As Status
		RETURN 0