﻿CREATE PROCEDURE [dbo].[spc_UpdateEffectiveBalance]  
 @OurBranchID varchar(30),  
 @RefNo varchar(100),  
 @AccountID varchar(30),  
 @ScrollNo int,  
 @SerialNo int,  
 @ValueDate datetime,  
 @Amount money,  
 @ForeignAmount Money,  
 @IsLocalCurrency integer=1,  
 @AccountType char(1)='C'  
   
 AS  
   
 SET NOCOUNT ON  
   
 IF @AccountType='C'  
 BEGIN          
  IF @IsLocalCurrency = 1          
  BEGIN          
   Update t_AccountBalance          
   Set Effects =  Effects - @Amount,          
   ClearBalance = ClearBalance + @Amount          
   Where  OurBranchID = @OurBranchID and AccountID = @AccountID          
  END          
  ELSE          
  BEGIN          
   Update t_AccountBalance          
   Set Effects =  Effects - @ForeignAmount,          
   LocalEffects = LocalEffects - @Amount,          
   ClearBalance = ClearBalance + @ForeignAmount,          
   LocalClearBalance = LocalClearBalance + @Amount          
   Where  OurBranchID = @OurBranchID and AccountID = @AccountID          
  END          
 END          
 else      
 begin      
   select 'Error' AS RetStatus, 'AccountType' AS RetMessage      
 end  
   
 Update t_Clearing SET Status = 'C'  
 Where OurBranchID = @OurBranchID and RefNo = @RefNo and AccountID = @AccountID
 
 select 'Ok' AS RetStatus