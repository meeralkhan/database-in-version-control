﻿CREATE procedure [dbo].[sp_ATM_GetReconciliation]

	@WorkingDate as datetime
	
AS

BEGIN
	SELECT * FROM t_ATM_TransactionHistory Where wDate > @WorkingDate
END