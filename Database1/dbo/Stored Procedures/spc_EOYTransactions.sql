﻿CREATE proc [dbo].[spc_EOYTransactions]   (      
@year nvarchar(10)   
)         
as
SET NOCOUNT ON
drop table if exists t_EOYGLTransaction;
select a.*, b.Balance, b.ForeignBalance,c.RetainedEarningGL,0 IsProcessed into t_EOYGLTransaction from            
(select OurBranchID, AccountID, CurrencyID,      
SUM(cast( case IsCredit when 1 then Amount else Amount*-1 end as decimal(21,6))) LCYTrxBalance,            
SUM(cast(case IsCredit when 1 then ForeignAmount else ForeignAmount*-1 end as decimal(21,6))) FCYTrxBalance            
from t_GLTransactions where YEAR(ValueDate) = @year   
and Status != 'R' group by OurBranchID, AccountID, CurrencyID)            
a inner join t_GL b on a.OurBranchID = b.OurBranchID and a.AccountID = b.AccountID and b.AccountType IN ('I','E')            
inner join t_Currencies c on c.OurBranchID = a.OurBranchID and c.CurrencyID = a.CurrencyID       
WHERE A.LCYTrxBalance <> 0   
select 'Ok' as ReturnStatus, 'Done' AS ReturnMessage