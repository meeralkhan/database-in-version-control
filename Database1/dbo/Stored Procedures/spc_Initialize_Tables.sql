﻿CREATE procedure [dbo].[spc_Initialize_Tables]    
(    
 @OurBranchID varchar(30),    
 @ProgramID varchar(30),    
 @Table varchar(100)    
)    
AS    
    
SET NOCOUNT ON    
    
DECLARE @SQLQuery varchar(5000)    
    
if @ProgramID = 'CASH'    
begin    
    
 set @SQLQuery = 'SELECT * INTO ' + @Table + ' FROM t_CashTransactionModel WHERE OurBranchID = ' + @OurBranchID    
    
 exec (@SQLQuery)    
    
 DELETE FROM t_CashTransactionModel WHERE OurBranchID = @OurBranchID    
    
 select 'Ok' as ReturnStatus, 'Done' AS ReturnMessage    
    
end    
else if @ProgramID = 'OLCASH'    
begin    
    
 set @SQLQuery = 'SELECT * INTO ' + @Table + ' FROM t_OnLineCashTransaction WHERE OurBranchID = ' + @OurBranchID    
    
 exec (@SQLQuery)    
    
 DELETE FROM t_OnLineCashTransaction WHERE OurBranchID = @OurBranchID    
     
 select 'Ok' as ReturnStatus, 'Done' AS ReturnMessage    
    
end    
else if @ProgramID = 'TRANSFER'    
begin    
    
 set @SQLQuery = 'SELECT * INTO ' + @Table + ' FROM t_TransferTransactionModel WHERE OurBranchID = ' + @OurBranchID    
    
 exec (@SQLQuery)    
    
 DELETE FROM t_TransferTransactionModel WHERE OurBranchID = @OurBranchID    
    
 select 'Ok' as ReturnStatus, 'Done' AS ReturnMessage    
    
end    
else if @ProgramID = 'ATMCASH'    
begin    
    
 set @SQLQuery = 'SELECT * INTO ' + @Table + ' FROM t_ATM_CashTransaction WHERE OurBranchID = ' + @OurBranchID    
    
 exec (@SQLQuery)    
    
 DELETE FROM t_ATM_CashTransaction WHERE OurBranchID = @OurBranchID    
    
 select 'Ok' as ReturnStatus, 'Done' AS ReturnMessage    
    
end    
else if @ProgramID = 'ATMTRANSFER'    
begin    
    
 set @SQLQuery = 'SELECT * INTO ' + @Table + ' FROM t_ATM_TransferTransaction WHERE OurBranchID = ' + @OurBranchID    
    
 exec (@SQLQuery)    
    
 DELETE FROM t_ATM_TransferTransaction WHERE OurBranchID = @OurBranchID    
    
 select 'Ok' as ReturnStatus, 'Done' AS ReturnMessage    
    
end    
else if @ProgramID = 'INWARD'    
begin    
    
 set @SQLQuery = 'SELECT * INTO ' + @Table + ' FROM t_InwardClearing WHERE OurBranchID = ' + @OurBranchID    
    
 exec (@SQLQuery)    
    
 DELETE FROM t_InwardClearing WHERE OurBranchID = @OurBranchID    
    
 select 'Ok' as ReturnStatus, 'Done' AS ReturnMessage    
    
end    
else if @ProgramID = 'BUREAU'    
begin    
    
 set @SQLQuery = 'SELECT * INTO ' + @Table + ' FROM t_BureaDeChange WHERE OurBranchID = ' + @OurBranchID    
    
 exec (@SQLQuery)    
    
 DELETE FROM t_BureaDeChange WHERE OurBranchID = @OurBranchID    
    
 select 'Ok' as ReturnStatus, 'Done' AS ReturnMessage    
    
end    
else if @ProgramID = 'OUTWARD'    
begin    
     
 /*    
 set @SQLQuery = 'SELECT * INTO ' + @Table + ' FROM t_OutwardClearing WHERE OurBranchID = ' + @OurBranchID    
 exec (@SQLQuery)    
 */    
     
 DELETE FROM t_OutwardClearing WHERE OurBranchID = @OurBranchID    
    
 select 'Ok' as ReturnStatus, 'Done' AS ReturnMessage    
    
end    
else    
begin    
    
 select 'Error' as ReturnStatus, 'Invalid Program ID, Pls! Contact Proper Person...' AS ReturnMessage    
    
end