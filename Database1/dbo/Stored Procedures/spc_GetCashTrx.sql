﻿CREATE PROCEDURE [dbo].[spc_GetCashTrx]      
(        
 @OurBranchID varchar(4),        
 @GLID varchar(30)='',        
 @ScrollNo numeric(10,0)=0        
)        
        
AS        
           
   DECLARE @AccountType as Char(1)        
        
   SET NOCOUNT ON        
           
           
   BEGIN           
      IF @GLID = '' AND @ScrollNo = 0         
         BEGIN        
            IF (SELECT count(OurBranchID) FROM t_CashTransactionModel        
               WHERE OurBranchID = @OurBranchID ) > 0        
               BEGIN        
                  Print 'Right Now Not Used'        
               END        
            ELSE        
               BEGIN        
                  SELECT '0' as RetStatus --Trx Not Found        
                  RETURN        
               END        
         END        
        
      ELSE IF @GLID = '' AND @ScrollNo <> 0        
         BEGIN        
            IF (SELECT count(ScrollNo) FROM t_CashTransactionModel        
               WHERE OurBranchID = @OurBranchID AND ScrollNo = @ScrollNo) > 0        
               BEGIN        
                     SELECT @AccountType = AccountType --,@AccountID = AccountID         
                     FROM t_CashTransactionModel        
                     WHERE (OurBranchID = @OurBranchID AND ScrollNo = @ScrollNo)        
        
                     IF @AccountType = 'C'        
                        BEGIN        
                           SELECT '1' as RetStatus, T.RefNo, T.AccountID, T.AccountName,T.ProductID, T.CurrencyID,T.AccountType,         
                               T.wDate, T.TrxType,T.ChequeID, T.ChequeDate, T.Amount, T.ForeignAmount,         
                               T.ExchangeRate, T.DescriptionID, T.Description,T.GLID, T.Supervision,         
                               T.OperatorID, T.SupervisorID, T.IsLocalCurrency, B.ClearBalance, B.Reminder,        
                               B.Effects, B.ShadowBalance, B.Limit, B.FreezeAmount as BlockedAmount,        
                               P.Description as ProductDescription, C.Description as CurrencyDescription                
        , AppWHTax, WHTaxMode, WHTaxAmount, WHTaxScrollNo, ISNULL(P.MinBalance,0) AS ProductMinBalance,        
          T.UtilityNumber ,T.AppChg,T.Chgmode,T.ChgScrollno,T.AdditionalData,T.Refno        
                           FROM t_CashTransactionModel as T, t_AccountBalance as B,         
                                t_Products as P, t_Currencies as C        
                    
                           WHERE T.OurBranchID = B.OurBranchID AND T.AccountID = B.AccountID        
                                 AND T.OurBranchID = P.OurBranchID AND T.ProductID = P.ProductID        
                                 AND T.OurBranchID = C.OurBranchID AND T.CurrencyID = C.CurrencyID        
                                 AND T.OurBranchID = @OurBranchID AND T.ScrollNo = @ScrollNo        
                        END        
                     ELSE        
        
                        BEGIN        
                           SELECT '1' as RetStatus,T.RefNo, T.AccountID, T.AccountName,T.ProductID, T.CurrencyID,T.AccountType,         
                               T.wDate, T.TrxType,T.ChequeID, T.ChequeDate, T.Amount, T.ForeignAmount,         
                               T.ExchangeRate, T.DescriptionID, T.Description,T.GLID, T.Supervision,         
                               T.OperatorID,  T.SupervisorID,T.IsLocalCurrency, CASE  T.IsLocalCurrency WHEN 0 THEN G.Balance WHEN 1 THEN G.ForeignBalance END AS ClearBalance, '' as Reminder,        
                               0 as Effects, G.ShadowBalance, 0 as Limit, 0 BlockedAmount, 0 ProductMinBalance,        
                               'General Ledger' as ProductDescription, C.Description as CurrencyDescription                                      
        , AppWHTax, WHTaxMode, WHTaxAmount, WHTaxScrollNo,        
          T.UtilityNumber  ,T.AppChg,T.Chgmode,T.ChgScrollno,T.AdditionalData,T.Refno        
                 FROM t_CashTransactionModel as T, t_GL as G,         
                                t_Currencies as C        
                    
                           WHERE T.OurBranchID = G.OurBranchID AND T.AccountID = G.AccountID                                         
                                 AND T.OurBranchID = C.OurBranchID AND T.CurrencyID = C.CurrencyID        
                               AND T.OurBranchID = @OurBranchID AND T.ScrollNo = @ScrollNo        
                        END        
    END        
            ELSE        
               BEGIN        
                  SELECT '0' as RetStatus -- Trx Not Found        
                  RETURN        
               END        
         END        
        
      ELSE        
        
         BEGIN           
            IF (SELECT count(OurBranchID) FROM t_CashTransactionModel        
               WHERE OurBranchID = @OurBranchID AND ScrollNo = @ScrollNo AND GLID = @GLID) > 0        
               BEGIN        
                  Print 'Right Now Not Used'        
               END        
            ELSE        
               BEGIN        
                  SELECT '0' as RetStatus -- Trx Not Found        
                  RETURN        
               END        
         END        
        
   END