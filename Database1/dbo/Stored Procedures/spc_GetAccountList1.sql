﻿CREATE PROCEDURE [dbo].[spc_GetAccountList1]
(  
  @OurBranchID varchar(30),  
  @WorkingDate datetime='',  
  @Type varchar(1)=''  
)  
AS   
 
 DECLARE @SecurityType as Varchar(2)     
  
 IF @Type='N'  
    
  BEGIN   
      Select PartyID as AccountID,PartyDescription as Name from t_MoneyMarketAccountM  
      Where OurBranchID = @OurBranchID AND Type = @Type  
  END  
   
 ELSE IF @Type='S'  
    
  BEGIN   
      Select PartyID as AccountID,PartyDescription as Name from t_MoneyMarketAccountM  
      Where OurBranchID = @OurBranchID AND Type = @Type AND EndDate>@WorkingDate  
  END  
  
 ELSE IF @Type='B'  
  
  BEGIN   
   SELECT @SecurityType=SecurityID From t_MMSecurityType,t_MoneyMarketAccountM  
   WHERE t_MoneyMarketAccountM.OurBranchID=@OurBranchID AND t_MoneyMarketAccountM.SecurityType=@Type  
    AND t_MMSecurityType.Behavior = t_MoneyMarketAccountM.SecurityType  
  
      Select PartyID as AccountID,PartyDescription as Name from t_MoneyMarketAccountM  
      Where OurBranchID = @OurBranchID AND Type = 'S' AND (SecurityType='B' OR SecurityType=@SecurityType) AND EndDate>@WorkingDate  
  END  
    
 ELSE   
  
  BEGIN   
      Select PartyID as AccountID,PartyDescription as Name from t_MoneyMarketAccountM  
      Where OurBranchID = @OurBranchID  
  END