﻿  
CREATE PROCEDURE [dbo].[spplus_GetATMUtilityCharges] (
 @OurBranchID varchar(30),
 @CompanyNumber varchar(30)=''
)  
 AS    
 if @CompanyNumber <> ''    
  begin    
   if exists ( SELECT CompanyNumber FROM t_ATM_UtilityCharges WHERE OurBranchID = @OurBranchID AND CompanyNumber = @CompanyNumber )
    
    SELECT CompanyNumber, WithdrawalCharges, WithdrawlChargesPercentage, IssuerAccountID, IssuerAccountTitle, AcquiringAccountID, AcquiringAccountTitle, IssuerChargesAccountID, IssuerTitle, AcquiringChargesAccountID, AcquiringTitle, ExciseDutyAccountID,
	ExciseDutyAccountTitle=ISNULL((SELECT [Description] from t_GL WHERE OurBranchID = @OurBranchID AND AccountID = ExciseDutyAccountID),''),
	ExciseDutyPercentage, ChargeType, 1 as ReturnValue 
	from t_ATM_UtilityCharges
	Where OurBranchID = @OurBranchID AND CompanyNumber = @CompanyNumber
    
   else    
   
    select 0 as ReturnValue    
    
  end    
    
 else    
  begin    
   SELECT CompanyNumber, WithdrawalCharges, WithdrawlChargesPercentage, ExciseDutyPercentage, IssuerAccountID, AcquiringAccountID, IssuerChargesAccountID,AcquiringChargesAccountID, ExciseDutyAccountID
   
   FROM t_ATM_UtilityCharges WHERE OurBranchID = @OurBranchID 
   
  end