﻿CREATE PROCEDURE [dbo].[spc_DeleteInwardTransaction]      
        
 @OurBranchID varchar(4),        
 @SerialNo int,        
 @AccountID varchar(30),        
 @ChequeID varchar(11)='',        
 @Amount money=0,        
 @ForeignAmount money=0,        
 @CurrencyID varchar(4)='',        
 @AccountType char(1)='C',        
 @Status char(1)='',        
 @TransactionType char(1)='',        
 @wDate datetime='',        
 @ClearBalance money=0,        
 @Effects money=0,        
 @Limit money=0,        
 @Log_On Char(1)= 'Y',        
 @OperatorID varchar(15)='',        
 @ComputerName varchar(50)='',        
 @ChargesScrollno money=0,        
 @SupervisorID varchar(50)='',        
 @ApplyCharges bit=0,        
 @NewRecord bit=0        
AS        
        
 SET NOCOUNT ON        
        
 DECLARE @RetStatus Int        
 DECLARE @mDate as Datetime        
        
 SELECT @mDate = WorkingDate From t_Last WHERE OurBranchID = @OurBranchID    
        
 IF @SerialNo <> 0        
  BEGIN         
        
   EXEC @RetStatus = spc_DeleteInwardTransactionMain @OurBranchID , @SerialNo , @AccountID        
        
   IF @RetStatus <> 0        
    BEGIN        
     SELECT @RetStatus as RetStatus, 'K' as Supervision        
     RETURN(1)            
    END        
        
   SELECT @mDate = Convert(varchar(10),@mDate,101) + ' ' + convert(varchar(20),Getdate(),114)        
        
-- Write On LOg In Secondry DataBase oN Next Phase After Discussion        
--   IF @Log_On = 'Y'        
            
   SELECT '0' as RetStatus        
   RETURN(0)              
  END