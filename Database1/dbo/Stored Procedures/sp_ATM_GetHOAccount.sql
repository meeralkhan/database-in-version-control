﻿CREATE PROCEDURE [dbo].[sp_ATM_GetHOAccount]
	(
		@OurBranchID varchar(30)
	)
	
AS
	BEGIN
		SELECT HOAccountID, HOAccountTitle, HOCurrencyID, HOCurrencyName, HOProductID
		FROM t_ATM_HOAccount
		WHERE OurBranchID=@OurBranchID
	END