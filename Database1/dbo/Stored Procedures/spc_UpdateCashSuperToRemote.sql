﻿CREATE PROCEDURE [dbo].[spc_UpdateCashSuperToRemote]              
(                    
 @OurBranchID varchar(30),                  
 @AccountID varchar(30),                  
 @SupervisorID varchar(30),                  
 @ScrollNo numeric(10),                
 @NewRecord bit = 0                  
)                  
                  
AS                  
   SET NOCOUNT ON                  
                  
   DECLARE @vDate datetime                   
   DECLARE @mDate Varchar(32)                  
   DECLARE @ChequeID varchar(11)                  
   DECLARE @ClearBalance money                  
   DECLARE @Effects money                  
   DECLARE @Limit money                  
   DECLARE @TrxType char(1)                  
   DECLARE @Amount money                  
   DECLARE @ForeignAmount money                  
   DECLARE @IsLocalCurrency bit                  
   DECLARE @Status char(1)                  
   DECLARE @AccountType char(1)                   
                  
 /* Get Cash Transaction */                  
   SELECT @vDate=ValueDate,@ChequeID= ChequeID,@AccountType=AccountType,@TrxType=TrxType                  
         ,@Amount = Amount, @ForeignAmount = ForeignAmount,                  
         @IsLocalCurrency=IsLocalCurrency,@Status = Supervision                    
   FROM t_CashTransactionModel                  
   WHERE (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (ScrollNo = @ScrollNo)                   
                  
   SELECT @mDate = Convert(varchar(10),@vDate ,101) + ' ' + convert(varchar(20),GetDate(),114)                  
                  
   IF @AccountType = 'C'                  
      BEGIN                  
 /* Get Current ClearBalance, Effective Balance and Limit */                   
         SELECT @ClearBalance=ClearBalance,@Effects=Effects,@limit=Limit                   
         FROM t_AccountBalance                  
         WHERE (OurBranchID = @OurBranchID) AND (AccountID = @AccountID)                   
      END                  
   ELSE                  
      SELECT @ClearBalance=0,@Effects=0,@limit=0                  
                  
   IF @Status = '*'                  
      BEGIN                  
         SELECT @Status='C'                  
                  
         /*UpDate Transaction File Status*/                  
         UPDATE t_CashTransactionModel                  
         SET Supervision = 'P'              
         WHERE (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (ScrollNo = @ScrollNo)                  
                   
         /* Create a New record For Superviser */                  
         INSERT INTO t_SupervisedBy                  
               (OurBranchID,AccountID,[Date],ScrollNo,ChequeID,ClearBalance,Effects,Limit,TrxType,Amount,Status,SupervisorID,TransactionCategory)                  
         VALUES                  
               (@OurBranchID,@AccountID,@mDate,@ScrollNo,@ChequeID,@ClearBalance,@Effects,@Limit,@TrxType,@Amount,@Status,@SupervisorID,'C')                  
                 
   /* Update Customer Status */                  
         Update t_CustomerStatus                  
         Set Status = 'P', IsOverDraft = '1'          
         Where (OurBranchID=@OurBranchID) and (AccountID = @AccountID)                   
                  and (ScrollNo = @ScrollNo) and  (Left(TransactionType,1)='C')                  
                  
         /*@Update OtherTransaction Table For Cash*/                  
         /*                
         Update t_OtherTransactionStatus                  
         Set Status = @Status                  
         Where (OurBranchID=@OurBranchID) and (AccountID = @AccountID) and (ScrollNo = @ScrollNo)                   
               and  (Left(TransactionType,1)='C')  /*and (Left(RefNo,2) = 'FD')*/                  
         */                
-----------------------------------------                  
-----------------------------------------                  
      
--Local Remittance Reject                  
 Update t_LR_Issuance Set      
 Supervision = @Status,      
 SuperviseBy = @SupervisorID,      
 SuperviseTime = @mDate      
 Where (OurBranchID=@OurBranchID) and  (PaidScrollNo = @ScrollNo)      
 and (CONVERT(varchar(20), wDate, 101) = @vDate) AND PaidMode = 'CH'    
     
/*      
--Local Remittance Advice Reject                  
 Update t_LR_Advice                  
 Set Supervision = @Status                  
 Where (OurBranchID=@OurBranchID) and  (ScrollNo = @ScrollNo) and  (wDate = @vDate)                  
-------------------------------------------------------------         
*/                
                
         SELECT '0' as RetStatus                  
         RETURN                  
      END                  
                  
   ELSE                  
      BEGIN                  
         IF @Status = 'C'                  
            BEGIN                  
               SELECT '2' as RetStatus                  
               RETURN                  
            END                  
                  
         IF @Status = 'R'                  
            BEGIN                  
               SELECT '3' as RetStatus                  
               RETURN                  
         END                  
                  
         SELECT '4' as RetStatus                  
         RETURN                  
      END