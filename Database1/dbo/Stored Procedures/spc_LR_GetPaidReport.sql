﻿CREATE PROCEDURE [dbo].[spc_LR_GetPaidReport]
(    
 @OurBranchID varchar(30),    
 @InstrumentType varchar(30),    
 @FromDate datetime,    
 @ToDate datetime    
)    
AS    
BEGIN    
    
 SELECT a.InstrumentType, a.InstrumentNo, b.BankID, b.FullName as BankName, c.BranchID, c.BranchName, a.ControlNo,   
 a.wDate, a.PaidDate, a.PaidScrollNo, a.PaidMode, a.InstrumentAccountID, a.Amount, a.PayeeName, a.BeneficiaryName,     
 a.CreateBy as OperatorID    
 FROM t_LR_Issuance a    
 INNER JOIN t_LR_Banks b ON a.OurBranchID = b.OurBranchID AND a.DrawnBankID = b.BankID      
 INNER JOIN t_LR_Branches c ON a.OurBranchID=c.OurBranchID AND b.BankID = c.BankID AND a.DrawnBranchID = c.BranchID      
 WHERE (a.OurBranchID = @OurBranchID) and (a.InstrumentType = @InstrumentType) and a.Paid = 1     
 AND (convert(varchar(10), a.PaidDate, 120) between @FromDate and @ToDate)    
    
END