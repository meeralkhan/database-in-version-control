﻿CREATE PROCEDURE [dbo].[spc_GetGLPostingAccount]                    
(                    
 @OurBranchID varchar(30),                    
 @AccountID varchar(30),                    
 @TrxType varchar(5)='',                    
 @OperatorID varchar(30)='',                     
 @ModuleType char(1)='',                    
 @GLRealTime char(1)='0',      
 @IsSystem char(1)='0'      
)                   
AS                    
 SET NOCOUNT ON                    
                    
 DECLARE @AccountName as Varchar(75)                    
 DECLARE @AccountClass as Char(1)  
 DECLARE @CurrencyID as Varchar(30)                    
 DECLARE @CurrencyName as Varchar(50)                    
 DECLARE @ClearBalance as Money                    
 DECLARE @ShadowBalance as Money                    
                    
 DECLARE @MeanRate as decimal(18,6)                
                    
 DECLARE @IsPosting as Bit                    
 DECLARE @GLID as Varchar(30)                    
 DECLARE @LocalCurrency as Varchar(30)                    
 DECLARE @IsPostAllGLAccounts as Bit                    
                    
 SELECT @LocalCurrency =UPPER(LTRIM(RTRIM(LocalCurrency))), @IsPostAllGLAccounts=isnull(IsPostAllGLAccounts,'0')            
 From t_GlobalVariables where OurBranchID = @OurBranchID            
                     
 IF @IsPostALLGLAccounts = 1                    
  IF (Select Count(AccountID) From t_UserAccountAccess                     
    WHERE  OurBranchID = @OurBranchID AND UserID = @OperatorID AND AccountType = 'G' AND AccountID = @AccountID                     
      AND (TrxType = @TrxType OR TrxType = 'B') AND AuthStatus = '') <= 0                     
   BEGIN                    
    SELECT '10'  AS RetStatus, 0.00 as ClearBalance, 0.00 as ShadowBalance, '' as AccountName, '' as CurrencyID, '' as CurrencyName, '' as GLID, 0.00 as MeanRate --User Account Rights Not Define                    
    RETURN(0)                           
   END                      
                    
                    
 IF (Select Count(AccountID) From t_GL WHERE   OurBranchID = @OurBranchID AND AccountID = @AccountID) > 0                     
  BEGIN                    
                 
   SELECT @AccountName=Description,                         
                               @ShadowBalance=ShadowBalance,                    
          @ClearBalance = CASE CurrencyID WHEN @LocalCurrency THEN Balance                    
         ELSE ForeignBalance                    
       END,                    
     @CurrencyID= UPPER(CurrencyID),                    
                                 @AccountClass = AccountClass,@IsPosting = IsPosting                    
                    
   FROM t_GL                    
   WHERE   OurBranchID = @OurBranchID AND AccountID = @AccountID                        
                 
   if @GLRealTime = '1'              
   begin              
     if @LocalCurrency = @CurrencyID              
     begin              
       exec @ClearBalance = fn_GetGLRealTimeBalance @OurBranchID, @AccountID        
     end              
     else              
     begin              
       exec @ClearBalance = fn_GetGLRealTimeFBalance @OurBranchID, @AccountID        
     end              
   end              
                 
--   IF @OperatorID<>''                    
   IF @ModuleType = 'C'                    
    BEGIN                     
     IF @TrxType = 'L'                    
                             BEGIN                    
      IF @LocalCurrency = @CurrencyID                     
       BEGIN                    
        SELECT '8'  AS RetStatus, 0.00 as ClearBalance, 0.00 as ShadowBalance, '' as AccountName, '' as CurrencyID, '' as CurrencyName, '' as GLID, 0.00 as MeanRate --Local Debit Not Allowed In Loacl Currency                    
        RETURN(0)                           
       END                    
                    
       SELECT @GLID=IsNull(GLID,'')  FROM t_CashierMultiCurrency                    
       WHERE OurBranchID = @OurBranchID AND OperatorID = @OperatorID AND CurrencyID = @LocalCurrency                    
         END                    
     ELSE                     
      BEGIN                    
       SELECT @GLID=IsNull(GLID,'')  FROM t_CashierMultiCurrency                    
       WHERE OurBranchID = @OurBranchID AND OperatorID = @OperatorID AND CurrencyID = @CurrencyID                    
      END                    
                    
     IF IsNull(@GLID,'')=''                    
      BEGIN                    
       SELECT '5'  AS RetStatus, 0.00 as ClearBalance, 0.00 as ShadowBalance, '' as AccountName, '' as CurrencyID, '' as CurrencyName, '' as GLID, 0.00 as MeanRate --Cashier Not Autorize For this Type of Trx. or Till A/c Not Define                 
       RETURN(1)                           
      END                    
                    
    END                    
                    
                        IF @AccountClass <> 'P'                    
      BEGIN                   
    SELECT '6'  AS RetStatus, 0.00 as ClearBalance, 0.00 as ShadowBalance, '' as AccountName, '' as CurrencyID, '' as CurrencyName, '' as GLID, 0.00 as MeanRate --Account Class Not Deifne Posting                     
    RETURN(1)                           
      END                    
                    
                         IF @IsSystem = '0' AND @IsPosting = 0                       
              BEGIN                    
    SELECT '7'  AS RetStatus, 0.00 as ClearBalance, 0.00 as ShadowBalance, '' as AccountName, '' as CurrencyID, '' as CurrencyName, '' as GLID, 0.00 as MeanRate --Posting Not Allowed                    
    RETURN(1)                           
       END                    
                    
                    
   IF (SELECT Count(*) From t_Products                    
    WHERE OurBranchID = @OurBranchID AND (GLControl=@AccountID OR GLDebitBalance = @AccountID))>0                     
              BEGIN                    
    SELECT '9'  AS RetStatus, 0.00 as ClearBalance, 0.00 as ShadowBalance, '' as AccountName, '' as CurrencyID, '' as CurrencyName, '' as GLID, 0.00 as MeanRate --Posting Not Allowed IN Control Account                    
    RETURN(1)                           
       END                    
                    
                    
   SELECT @CurrencyName = Description, @MeanRate =IsNull(MeanRate,0) FROM t_Currencies                    
   WHERE OurBranchID=@OurBranchID AND CurrencyID=@CurrencyID                    
                    
                    
   SELECT '1' as RetStatus , @ClearBalance as ClearBalance,                      
     @ShadowBalance as ShadowBalance,@AccountName as AccountName,                    
     @CurrencyID as CurrencyID,                    
     @CurrencyName as CurrencyName,                         
     @GLID as GLID, @MeanRate as MeanRate                    
                        
  END                    
 ELSE                    
  BEGIN                   
   SELECT '0' AS RetStatus, 0.00 as ClearBalance, 0.00 as ShadowBalance, '' as AccountName, '' as CurrencyID, '' as CurrencyName, '' as GLID, 0.00 as MeanRate --Account Does Not Exists                    
   RETURN(0)                    
  END