﻿CREATE PROCEDURE [dbo].[spc_LR_EditInstrumentNo]        
(        
 @OurBranchID nvarchar(30),          
 @DrawnBankID nvarchar(30),          
 @DrawnBranchID nvarchar(30),          
 @InstrumentType nvarchar(50),          
 @InstrumentNo decimal(24,0),        
 @ControlNo decimal(24,0),        
 @OperatorID nvarchar(30),        
 @OperatorDate datetime,        
 @OperatorTerminal nvarchar(30),        
 @frmName nvarchar(50) = 'frmLRInstrumentNo'        
)          
As          
BEGIN        
      
set nocount on         
      
 DECLARE @iCount int          
 DECLARE @mDrawnBank varchar(4)          
 DECLARE @mDrawnBranch varchar(4)          
 DECLARE @mDDTT_Inst int          
 DECLARE @mDDTT_Ctrl int          
        
 BEGIN TRY        
         
  if @InstrumentType = 'PO'        
  begin          
   Select @iCount = count(*) FROM t_LR_InstrumentNo where OurBranchID = @OurBranchID and         
   BankID=@DrawnBankID and BranchID = @DrawnBranchID          
   if @iCount > 0            
   Begin          
    Update t_LR_InstrumentNo set PaymentOrder = @InstrumentNo, UpdateBy = @OperatorID,         
    UpdateTime=@OperatorDate, UpdateTerminal=@OperatorTerminal        
    where OurBranchID = @OurBranchID and BankID = @DrawnBankID and BranchID = @DrawnBranchID    
   End          
   else          
   Begin          
    Insert Into t_LR_InstrumentNo (OurBranchID, BankID, BranchID, PaymentOrder, CreateBy,         
    CreateTime, CreateTerminal, frmName)         
    Values(@OurBranchID, @DrawnBankID, @DrawnBranchID, @InstrumentNo, @OperatorID,        
    @OperatorDate, @OperatorTerminal, @frmName)         
   End        
  end        
  else if @InstrumentType = 'BC'        
  begin          
   Select @iCount = count(*) FROM t_LR_InstrumentNo where OurBranchID = @OurBranchID and         
   BankID=@DrawnBankID and BranchID = @DrawnBranchID          
   if @iCount > 0            
   Begin          
    update t_LR_InstrumentNo set BankersCheque = @InstrumentNo, UpdateBy = @OperatorID,         
    UpdateTime=@OperatorDate, UpdateTerminal=@OperatorTerminal        
    where OurBranchID = @OurBranchID and BankID=@DrawnBankID and BranchID = @DrawnBranchID        
   End          
   else          
   Begin          
    Insert Into t_LR_InstrumentNo (OurBranchID, BankID, BranchID, BankersCheque, CreateBy,        
    CreateTime, CreateTerminal, frmName)         
    Values(@OurBranchID, @DrawnBankID, @DrawnBranchID, @InstrumentNo, @OperatorID,        
    @OperatorDate, @OperatorTerminal, @frmName)          
   End          
  end          
  else if @InstrumentType = 'PS'        
  begin          
   Select @iCount = count(*) FROM t_LR_InstrumentNo where OurBranchID = @OurBranchID and         
   BankID=@DrawnBankID and BranchID = @DrawnBranchID          
   if @iCount > 0            
   Begin          
    update t_LR_InstrumentNo set PaySlip = @InstrumentNo, UpdateBy= @OperatorID,         
    UpdateTime=@OperatorDate, UpdateTerminal=@OperatorTerminal         
    where OurBranchID = @OurBranchID and BankID=@DrawnBankID and BranchID = @DrawnBranchID          
   End          
   else          
   Begin          
    Insert Into t_LR_InstrumentNo (OurBranchID, BankID, BranchID, PaySlip, CreateBy,         
    CreateTime, CreateTerminal, frmName)         
    Values(@OurBranchID, @DrawnBankID, @DrawnBranchID, @InstrumentNo, @OperatorID,         
    @OperatorDate, @OperatorTerminal, @frmName)          
   End          
  end          
  else if @InstrumentType = 'DD'        
  begin          
   Select @iCount = count(*) FROM t_LR_InstrumentNo where OurBranchID = @OurBranchID and         
   BankID=@DrawnBankID and BranchID = @DrawnBranchID          
   if @iCount > 0            
   Begin          
    update t_LR_InstrumentNo set DDInst = @InstrumentNo, DDCtrl = @ControlNo, CreateBy = @OperatorID,        
    CreateTime=@OperatorDate, CreateTerminal=@OperatorTerminal         
    where OurBranchID = @OurBranchID and BankID=@DrawnBankID and BranchID = @DrawnBranchID           
   End          
   else   
   Begin          
    Select @mDDTT_Inst = ( Select (isnull(max(InstrumentNo),1))         
          from t_LR_Issuance Where InstrumentType = 'TT' and OurBranchID = @OurBranchID  ),     
      @mDDTT_Ctrl = ( isnull(max(ControlNo),0)+1)         
          From t_LR_Issuance Where InstrumentType = 'TT' and OurBranchID = @OurBranchID and         
          DrawnBankID=@DrawnBankID and DrawnBranchID = @DrawnBranchID           
                 
    Insert Into t_LR_InstrumentNo (OurBranchID, BankID, BranchID, DDInst, TTInst, DDCtrl,         
    TTCtrl, CreateBy, CreateTime, CreateTerminal, frmName)         
    Values(@OurBranchID, @DrawnBankID, @DrawnBranchID, @InstrumentNo, @mDDTT_Inst, @ControlNo,        
    @mDDTT_Ctrl, @OperatorID, @OperatorDate, @OperatorTerminal, @frmName)          
           
    update t_LR_InstrumentNo Set DDInst = @InstrumentNo, TTInst=@mDDTT_Inst         
    Where OurBranchID = @OurBranchID AND OurBranchID <> @DrawnBranchID        
   End          
  end        
  else if @InstrumentType = 'TT'          
  begin          
   Select @iCount = count(*) FROM t_LR_InstrumentNo where OurBranchID = @OurBranchID and BankID=@DrawnBankID         
   and BranchID = @DrawnBranchID          
   if @iCount > 0            
   Begin          
    update t_LR_InstrumentNo set TTInst = @InstrumentNo where OurBranchID = @OurBranchID and         
    BranchID <> @OurBranchID          
        
    update t_LR_InstrumentNo set TTCtrl = @ControlNo, CreateBy = @OperatorID, CreateTime = @OperatorDate,        
    CreateTerminal=@OperatorTerminal         
    where OurBranchID = @OurBranchID and BankID=@DrawnBankID and BranchID = @DrawnBranchID           
   End          
   else          
   Begin          
    Select @mDDTT_Inst = ( Select (isnull(max(InstrumentNo),1)) from t_LR_Issuance         
          Where InstrumentType = 'DD' and OurBranchID = @OurBranchID ),        
      @mDDTT_Ctrl = (isnull(max(ControlNo),0)+1) From t_LR_Issuance Where InstrumentType = 'DD' and        
          OurBranchID = @OurBranchID and DrawnBankID=@DrawnBankID and DrawnBranchID = @DrawnBranchID           
        
    Insert Into t_LR_InstrumentNo (OurBranchID, BankID, BranchID, DDInst, TTInst, DDCtrl,         
    TTCtrl, CreateBy, CreateTime, CreateTerminal, frmName)           
    Values(@OurBranchID, @DrawnBankID, @DrawnBranchID, @mDDTT_Inst, @InstrumentNo, @mDDTT_Ctrl,        
    @ControlNo, @OperatorID, @OperatorDate, @OperatorTerminal, @frmName)          
        
    update t_LR_InstrumentNo Set TTInst = @InstrumentNo, DDInst=@mDDTT_Inst Where OurBranchID = @OurBranchID AND OurBranchID<>@DrawnBranchID          
   End          
  end          
          
  select 'Ok' as ReturnStatus, 'SavedSuccessfully' as ReturnMessage        
          
 END TRY        
         
 BEGIN CATCH        
  Select 'Error' as ReturnStatus, Error_Message() as ReturnMessage        
 END CATCH        
END