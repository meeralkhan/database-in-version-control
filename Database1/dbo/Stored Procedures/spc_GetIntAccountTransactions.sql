﻿
CREATE PROCEDURE [dbo].[spc_GetIntAccountTransactions]      
(      
 @OurBranchID varchar(30),      
 @AccountID varchar(30),      
 @ProductID varchar(30),      
 @FromDate datetime,      
 @ToDate datetime,      
 @InterestProcedure varchar(30)=''      
)      
AS      
BEGIN      
   
 set nocount on  
   
 Declare @MOAccountID varchar(30)      
      
 SELECT @MOAccountID=AccountID      
 FROM t_GlParameters WHERE SerialID='2' AND OurBranchID=@OurBranchID /* Main Office Account */      
       
 IF @InterestProcedure='001'      
 BEGIN      
  SELECT ScrollNo, SerialNo, AccountID, CurrencyID, AccountType, ValueDate, wdate, TrxType, Amount,       
  ForeignAmount, ExchangeRate, IsLocalCurrency      
  FROM t_Transactions      
  WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID      
  AND valueDate Between @FromDate AND @ToDate      
        
  UNION      
      
  SELECT ScrollNo, SerialNo, AccountID, CurrencyID, AccountType, ValueDate, wdate, TrxType, Amount,      
  ForeignAmount, ExchangeRate, IsLocalCurrency      
  FROM t_TransferTransactionModel      
  WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID      
  AND valueDate Between @FromDate AND @ToDate AND Supervision='C'      
        
  UNION      
        
  SELECT ScrollNo, '100' as SerialNo, AccountID, CurrencyID, AccountType, ValueDate, wdate, TrxType, Amount,      
  ForeignAmount, ExchangeRate, IsLocalCurrency      
  FROM t_CashTransactionModel      
  WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID      
  AND valueDate Between @FromDate AND @ToDate AND Supervision='C'      
      
  UNION      
      
  SELECT SerialNo as ScrollNo, SerialNo, AccountID, CurrencyID, AccountType, ValueDate, wdate,       
  TransactionType as TrxType, Amount, ForeignAmount, ExchangeRate, 1 -- IsLocalCurrency      
  FROM t_InwardClearing      
  WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID      
  AND valueDate Between @FromDate AND @ToDate AND Status <>'R'      
        
  UNION      
        
  SELECT ScrollNo, '101' as SerialNo, AccountID, CurrencyID, AccountType, ValueDate, wdate, TrxType, Amount,       
  ForeignAmount, ExchangeRate, IsLocalCurrency       
  FROM t_AllModelTransaction      
  WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID      
  AND valueDate Between @FromDate AND @ToDate      
  
  UNION
        
  --Only For Debit Interest  -- Modify as on 11Apr2002      
  
  --'t_OnLine_CashTransaction & t_OL_LocalTransferTransaction      
      
  SELECT ScrollNo, SerialNo, AccountID, CurrencyID, AccountType, ValueDate, wdate, TrxType, Amount, ForeignAmount,      
  ExchangeRate, IsLocalCurrency        
  FROM t_OnLineCashTransaction      
  WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID      
  AND valueDate Between @FromDate AND @ToDate AND Supervision='C'      
  
  UNION 
  
  --'t_InternetTransaction          
  SELECT ScrollNo, SerialNo, AccountID, CurrencyID, AccountType, ValueDate, wdate, TrxType, Amount, ForeignAmount,
  ExchangeRate, IsLocalCurrency  
  FROM t_InternetTransaction          
  WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID
  AND valueDate Between @FromDate AND @ToDate
  
  /* -------Commented By Farhan--------      
        
  UNION        
        
  SELECT ScrollNo, SerialNo, AccountID, CurrencyID, AccountType, ValueDate, wdate, TrxType, Amount, ForeignAmount,      
  ExchangeRate, IsLocalCurrency        
  FROM t_OL_LocalTransferTransaction      
  WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID       
  AND valueDate Between @FromDate AND @ToDate AND Supervision='C'      

  --t_ATM_CashTransaction & --t_ATM_TransferTransaction      
        
  UNION      
      
  SELECT ScrollNo, SerialNo, AccountID, CurrencyID, AccountType, ValueDate, wdate, TrxType, Amount,      
  ForeignAmount, ExchangeRate, IsLocalCurrency      
  FROM t_ATM_CashTransaction      
  WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID      
  AND valueDate Between @FromDate AND @ToDate AND Supervision='C'      
        
  UNION      
        
  SELECT ScrollNo,SerialNo, AccountID, CurrencyID, AccountType, ValueDate, wdate, TrxType, Amount, ForeignAmount,      
  ExchangeRate, IsLocalCurrency        
  FROM t_ATM_TransferTransaction      
  WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID      
  AND valueDate Between @FromDate AND @ToDate AND Supervision='C'      
        
  UNION        
        
  SELECT 555 as ScrollNo, '1' as SerialNo, AccountID, 'PKR' as CurrencyID, 'C' as AccountType,       
  ChangeDate as ValueDate, ChangeDate as wdate, 'C' as TrxType, 0 as Amount, 0 as ForeignAmount,       
  0 as ExchangeRate,1 as IsLocalCurrency      
  FROM t_Rate001      
  WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ChangeDate Between @FromDate AND @ToDate       
      
  -------Commented By Farhan-------- */      
        
  ORDER BY ValueDate        
      
 END      
 ELSE      
 BEGIN      
  SELECT ScrollNo, SerialNo, AccountID, CurrencyID, AccountType, ValueDate, wdate, TrxType, Amount, ForeignAmount,      
  ExchangeRate, IsLocalCurrency      
  FROM t_transactions      
  WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID       
  AND valueDate Between @FromDate AND @ToDate        
        
  UNION      
      
  SELECT ScrollNo, SerialNo, AccountID, CurrencyID, AccountType, ValueDate, wdate, TrxType, Amount, ForeignAmount,      
  ExchangeRate, IsLocalCurrency      
  FROM t_TransferTransactionModel      
  WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID      
  AND valueDate Between @FromDate AND @ToDate AND Supervision='C'      
      
  UNION      
      
  SELECT ScrollNo, '100' as SerialNo, AccountID, CurrencyID, AccountType, ValueDate, wdate, TrxType, Amount,       
  ForeignAmount, ExchangeRate, IsLocalCurrency      
  FROM t_CashTransactionModel      
  WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID      
  AND valueDate Between @FromDate AND @ToDate AND Supervision='C'      
      
  UNION        
      
  SELECT SerialNo as ScrollNo, SerialNo, AccountID, CurrencyID, AccountType, ValueDate, wdate,       
  TransactionType as TrxType, Amount, ForeignAmount, ExchangeRate, 1 -- IsLocalCurrency      
  FROM t_InwardClearing      
  WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID      
  AND valueDate Between @FromDate AND @ToDate AND Status=''      
      
  UNION         
      
  SELECT SerialNo as ScrollNo, SerialNo, AccountID, CurrencyID, AccountType, ValueDate, wdate, 'C' as TrxType, Amount,      
  ForeignAmount, ExchangeRate,1 -- IsLocalCurrency      
  FROM t_InwardClearing      
  WHERE OurBranchID=@OurBranchID 
  AND AccountID=@MOAccountID --AND ProductID=@ProductID AND valueDate Between @FromDate AND @ToDate AND Status=''      
      
  UNION         
      
  SELECT ScrollNo, '101' as SerialNo, AccountID, CurrencyID, AccountType, ValueDate, wdate, TrxType, Amount,       
  ForeignAmount, ExchangeRate, IsLocalCurrency      
  FROM t_AllModelTransaction      
  WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID      
  AND valueDate Between @FromDate AND @ToDate      
  
  UNION      
        
  SELECT ScrollNo, SerialNo, AccountID, CurrencyID, AccountType, ValueDate, wdate, TrxType, Amount, ForeignAmount,      
  ExchangeRate, IsLocalCurrency        
  FROM t_OnLineCashTransaction      
  WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID       
  AND valueDate Between @FromDate AND @ToDate AND Supervision='C'      
  
  UNION 
  
  --'t_InternetTransaction          
  SELECT ScrollNo, SerialNo, AccountID, CurrencyID, AccountType, ValueDate, wdate, TrxType, Amount, ForeignAmount,
  ExchangeRate, IsLocalCurrency  
  FROM t_InternetTransaction          
  WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID
  AND valueDate Between @FromDate AND @ToDate 
  
  /* -------Commented By Farhan--------      
        
  UNION      
        
  SELECT ScrollNo, SerialNo, AccountID, CurrencyID, AccountType, ValueDate, wdate, TrxType, Amount, ForeignAmount,      
  ExchangeRate, IsLocalCurrency        
  FROM t_OL_LocalTransferTransaction      
  WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID      
  AND valueDate Between @FromDate AND @ToDate AND Supervision='C'        
        
  
      
  UNION        
      
  SELECT ScrollNo, SerialNo, AccountID, CurrencyID, AccountType, ValueDate, wdate, TrxType, Amount, ForeignAmount,      
  ExchangeRate, IsLocalCurrency        
  FROM t_ATM_CashTransaction      
  WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID       
  AND valueDate Between @FromDate AND @ToDate AND Supervision='C'      
        
  UNION      
        
  SELECT ScrollNo, SerialNo, AccountID, CurrencyID, AccountType, ValueDate, wdate, TrxType, Amount, ForeignAmount,      
  ExchangeRate, IsLocalCurrency       
  FROM t_ATM_TransferTransaction       
  WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ProductID=@ProductID       
  AND valueDate Between @FromDate AND @ToDate AND Supervision='C'      
  
        
  -------Commented By Farhan-------- */      
        
  ORDER BY ValueDate        
      
 END      
END