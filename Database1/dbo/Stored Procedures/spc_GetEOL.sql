﻿CREATE PROCEDURE [dbo].[spc_GetEOL]
(
  @OurBranchID varchar(30),
  @EOL varchar(1)=''
)

AS

SET NOCOUNT ON

IF @EOL = 'Y'
  
  SELECT * FROM vc_GetEOL WHERE OurBranchID = @OurBranchID AND (ABS(ClearBalance) > Limit)          
  
  union
  
  SELECT a.ClientID, c.Name AS ClientName, al.AccountID,         
  a.Name AS AccountName, al.SanctionDate,         
  al.AccountLimit as LimitAmount, al.DrawingPower,         
  al.ExpDate, t_Rate001.ChangeDate, t_Rate001.Limit1, t_Rate001.Rate1,          
  t_Rate001.Limit2, t_Rate001.Rate2, t_Rate001.Limit3, t_Rate001.Rate3, aas.ValueOfSecurity,        
  aas.Margin, s.[Description] AS SecurityDescription,        
  t_SecurityTypes.[Description] AS SecurityType, al.IsBlocked,         
  al.IsCancelled, p.CurrencyID, ab.ClearBalance,         
  al.OurBranchID, ab.Limit           
  FROM t_AccountBalance ab
  RIGHT OUTER JOIN t_Account a ON ab.OurBranchID = a.OurBranchID AND ab.AccountID = a.AccountID
  LEFT OUTER  JOIN t_Products p ON a.OurBranchID = p.OurBranchID AND a.ProductID = p.ProductID
  LEFT OUTER JOIN t_Customer c ON a.OurBranchID = c.OurBranchID AND a.ClientID = c.ClientID
  RIGHT OUTER JOIN t_Adv_Account_LimitMaintain al ON a.OurBranchID = al.OurBranchID AND a.AccountID = al.AccountID
  LEFT OUTER JOIN t_Securities s
  RIGHT OUTER JOIN t_Adv_Account_Security aas ON s.OurBranchID = aas.OurBranchID AND s.SecurityID = aas.SecurityID
  ON al.OurBranchID = aas.OurBranchID AND al.AccountID = aas.AccountID AND aas.ValueOfSecurity =          
  (    
   SELECT MAX(valueofsecurity) FROM t_Adv_Account_Security WHERE OurBranchID = al.OurBranchID AND AccountID = al.AccountID    
  )
  LEFT OUTER JOIN t_Rate001 ON al.OurBranchID = t_Rate001.OurBranchID AND al.AccountID = t_Rate001.AccountID AND t_Rate001.ChangeDate =     
  (    
   SELECT MAX(changedate)          
   FROM t_rate001 WHERE t_rate001.OurBranchID = al.OurBranchID AND t_rate001.accountid = al.AccountID    
  ) AND ab.Limit <> 0          
  INNER JOIN t_SecurityTypes on s.OurBranchID=t_SecurityTypes.OurBranchID AND s.SecurityTypeID=t_SecurityTypes.SecurityTypeId         
  
  WHERE ab.OurBranchID = @OurBranchID AND (ab.Limit = 0) AND  (ab.ClearBalance < 0)
  AND (al.isblocked=1) and (al.IsCancelled = 0) AND (ABS(ab.ClearBalance) > ab.Limit)          
 ELSE          
          
 SELECT * FROM vc_GetEOL WHERE OurBranchID = @OurBranchID  AND (ABS(ClearBalance) < Limit)          
 union          
 SELECT a.ClientID, c.Name AS ClientName, al.AccountID,         
 a.Name AS AccountName, al.SanctionDate,         
 al.AccountLimit as LimitAmount, al.DrawingPower,         
 al.ExpDate, t_Rate001.ChangeDate, t_Rate001.Limit1, t_Rate001.Rate1,          
 t_Rate001.Limit2, t_Rate001.Rate2, t_Rate001.Limit3, t_Rate001.Rate3, aas.ValueOfSecurity,         
 aas.Margin, s.Description AS SecurityDescription,         
 t_SecurityTypes.[Description] AS securityType, al.IsBlocked,         
 al.IsCancelled, p.CurrencyID, ab.ClearBalance,         
 al.OurBranchID, ab.Limit           
  FROM t_AccountBalance ab RIGHT OUTER JOIN t_Account a ON ab.OurBranchID = a.OurBranchID AND           
      ab.AccountID = a.AccountID LEFT OUTER  JOIN t_Products p ON           
           a.OurBranchID = p.OurBranchID AND a.ProductID = p.ProductID LEFT OUTER JOIN          
      t_Customer c ON a.OurBranchID = c.OurBranchID AND a.ClientID = c.ClientID RIGHT OUTER JOIN t_Adv_Account_LimitMaintain al ON           
      a.OurBranchID = al.OurBranchID AND a.AccountID = al.AccountID LEFT OUTER JOIN          
      t_Securities s RIGHT OUTER JOIN t_Adv_Account_Security aas ON s.OurBranchID = aas.OurBranchID AND           
      s.SecurityID = aas.SecurityID ON al.OurBranchID = aas.OurBranchID AND          
       al.AccountID = aas.AccountID AND aas.ValueOfSecurity =          
          (    
           SELECT MAX(valueofsecurity)      
           FROM t_Adv_Account_Security     
           WHERE OurBranchID = al.OurBranchID AND AccountID = al.AccountID    
          )          
       LEFT OUTER JOIN t_Rate001 ON  al.OurBranchID = t_Rate001.OurBranchID AND           
      al.AccountID = t_Rate001.AccountID AND t_Rate001.ChangeDate =     
      (    
       SELECT MAX(changedate)          
       FROM t_rate001     
       WHERE t_rate001.OurBranchID = al.OurBranchID AND t_rate001.accountid = al.AccountID    
      )  AND ab.Limit <> 0          
 INNER JOIN t_SecurityTypes on s.OurBranchID=t_SecurityTypes.OurBranchID AND s.SecurityTypeID=t_SecurityTypes.SecurityTypeId         
   WHERE ab.OurBranchID = @OurBranchID AND (ab.Limit = 0) AND  (ab.ClearBalance < 0) and (al.isblocked=1)          
   and (al.IsCancelled = 0) and  (ABS(ab.ClearBalance) < ab.Limit)