﻿CREATE PROCEDURE [dbo].[spc_OL_AddEditCashTransactionLocal]
(  
 @ScrollNo varchar(30), 
 @SerialNo int=1, 
 @OurBranchID varchar(30), 
 @CustomerAccountID varchar(30), 
 @CustomerAccountName varchar(100), 
 @CustomerProductID varchar(30), 
 @CustomerCurrencyID varchar(30), 
 @CustomerAccountType char(1), 
 @CustomerTrxType char(1), 
 @CustomerIsLocalCurrency Char(1), 
 @ValueDate datetime, 
 @wDate datetime, 
 @ChequeID varchar(30), 
 @ChequeDate DateTime, 
 @Amount money, 
 @ForeignAmount Money, 
 @ExchangeRate decimal(18,6), 
 @DescriptionID varchar(30), 
 @Description varchar(255), 
 @Supervision char(1), 
 @OperatorID varchar(30), 
 @SupervisorID varchar(30), 
 @SourceBranch Varchar(30), 
 @TargetBranch varchar(30), 
 @Remarks varchar(1000), 
 @RefNo varchar(30), 
 @BankId varchar(30), 
 @GLID varchar(30),        
 @AddData text=NULL        
)  
  
AS  
 
 DECLARE @LastEOD  as datetime  
 DECLARE @mDate varchar(50)  
 DECLARE @Status char(1)
 DECLARE @AccountClass char(1)
 DECLARE @IsPosting bit
 DECLARE @AllowCrDr bit
 DECLARE @IsFreezed bit
 
 set nocount on  
 
 BEGIN
   
   Select @LastEOD = LastEOD From t_Last Where OurBranchID = @OurBranchID      
   
   If @LastEOD >= @wDate   
   BEGIN  
     SELECT 'Error' AS RetStatus, 'WorkingDate' As RetMessage
     RETURN(0)  
   END  
   
   SET @Description = @Description + ' --- ' + cast(GETDATE() as varchar(50))  
   SET @mDate = convert(varchar(10),@wDate,101) + ' ' + convert(varchar(20), getdate(),114)  
   
   if @CustomerProductID = 'GL'
   begin
     
     IF (SELECT Count(AccountID) From t_GL WHERE OurBranchID=@OurBranchID AND AccountID=@CustomerAccountID) > 0
     begin
       
       SELECT @AccountClass = AccountClass, @IsPosting = IsPosting From t_GL
       WHERE OurBranchID=@OurBranchID AND AccountID=@CustomerAccountID
       
       IF UPPER(@AccountClass) <> 'P'
       BEGIN
         SELECT 'Error' AS RetStatus, 'NotAPostingAccount' As RetMessage
         RETURN(0)
       END
       
       IF @IsPosting = 0
       BEGIN
         SELECT 'Error' AS RetStatus, 'GLPostingNotAllowed' As RetMessage
         RETURN(0)
       END
       
       IF (SELECT Count(*) From t_Products WHERE OurBranchID = @OurBranchID AND 
       (GLControl=@CustomerAccountID OR GLDebitBalance = @CustomerAccountID)) > 0
       BEGIN
         SELECT 'Error' AS RetStatus, 'GLPostingNotAllowedControlAccount' As RetMessage
         RETURN(0)
       END
       
     end
     ELSE
     BEGIN
       SELECT 'Error' AS RetStatus, 'AccountNotFound' As RetMessage
       RETURN(0)
     END
     
   end
   else
   begin
     
     IF (SELECT Count(AccountID) From t_Account WHERE OurBranchID=@OurBranchID AND AccountID=@CustomerAccountID) > 0
     BEGIN
       
       SELECT @CustomerProductID = ProductID, @Status = ISNULL(Status,''), @AllowCrDr = Case @CustomerTrxType
     WHEN 'C' THEN AllowCreditTransaction  
     WHEN 'D' THEN AllowDebitTransaction  
   END
       FROM t_Account
       WHERE OurBranchID=@OurBranchID AND AccountID=@CustomerAccountID
       
 IF @Status = 'C'
       BEGIN
         SELECT 'Error' AS RetStatus, 'AccountClosed' As RetMessage
         RETURN(0)
       END
       
       IF @Status = 'D'
       BEGIN
         SELECT 'Error' AS RetStatus, 'AccountBlocked' As RetMessage
         RETURN(0)
       END
       
       IF @Status = 'T'
       BEGIN
         SELECT 'Error' AS RetStatus, 'AccountDormant' As RetMessage
         RETURN(0)
       END
       
       IF @Status = 'X'
       BEGIN
         SELECT 'Error' AS RetStatus, 'AccountDeceased' As RetMessage
         RETURN(0)
       END
       
       IF @AllowCrDr = 1
    BEGIN  
         SELECT 'Error' AS RetStatus, 'SPTrxNotAllowed' As RetMessage
         RETURN(0)
       END
       
       SELECT @IsFreezed=IsFreezed, @AllowCrDr = Case @CustomerTrxType
     WHEN 'C' THEN AllowCredit
     WHEN 'D' THEN AllowDebit
         END
       FROM t_Products
       WHERE OurBranchID = @OurBranchID AND ProductID = @CustomerProductID
       
       IF @IsFreezed = 1
       BEGIN
         SELECT 'Error' AS RetStatus, 'ProductFreezed' As RetMessage
         RETURN(0)
       END
       
       IF @AllowCrDr = 0
       BEGIN
         SELECT 'Error' AS RetStatus, 'ProductTrxNotAllowed' As RetMessage
         RETURN(0)
       END
       
     END
     ELSE
     BEGIN
       SELECT 'Error' AS RetStatus, 'AccountNotFound' As RetMessage
       RETURN(0)
     END
     
   end
   
   INSERT INTO t_OnLineLocalCashTransaction  
   (  
    ScrollNo, SerialNo, OurBranchID, AccountID, AccountName, ProductID, CurrencyID, AccountType, ValueDate, wDate, TrxType,  
    ChequeID, ChequeDate, Amount, ForeignAmount, ExchangeRate, DescriptionID, Description, Supervision,  
    OperatorID, SupervisorID, IsLocalCurrency, SourceBranch, TargetBranch, Remarks, RefNo, BankID, GLID, AdditionalData  
   )    
   
   VALUES
   (  
    @ScrollNo,@SerialNo,@OurBranchID,@CustomerAccountID,@CustomerAccountName,@CustomerProductID,  
    @CustomerCurrencyID,@CustomerAccountType,@ValueDate,@mDate,@CustomerTrxType,@ChequeID,@ChequeDate,  
    @Amount, @ForeignAmount,@ExchangeRate, @DescriptionID,@Description,@Supervision, @OperatorID,@SupervisorID,  
    @CustomerIsLocalCurrency,@SourceBranch, @TargetBranch,@Remarks,@RefNo, @BankID, @GLID, @AddData        
   )  
   
   SELECT @Status=@@ERROR  
   
   IF @Status <> 0   
   BEGIN  
     SELECT 'Error' AS RetStatus, 'ChkStatus' As RetMessage, @Status AS Status
     RETURN(0)
   END  
     
  declare @IsCredit int  
  declare @VoucherID int  
  declare @TransactionMethod char(1)  
  declare @CsGLControl varchar(30)  
  declare @DescID varchar(30)  
  declare @Desc varchar(255)  
    
  select @VoucherID = ISNULL(MAX(VoucherID),0) + 1 from t_GLTransactions where OurBranchID = @OurBranchID  
    
  if @CustomerTrxType = 'C'  
    set @IsCredit = 1  
  else  
    set @IsCredit = 0  
    
  if @CustomerIsLocalCurrency = 1    
  begin    
    set @ForeignAmount = 0    
    set @ExchangeRate = 1    
    set @TransactionMethod = 'L'    
  end    
  else    
    set @TransactionMethod = 'A'  
    
  if @CustomerAccountType = 'C'  
  begin  
       
     insert into t_Transactions (OurBranchID, ScrollNo, SerialNo, RefNo, wDate, AccountType, DocumentType, AccountID, AccountName,  
     ProductID, CurrencyID, ValueDate, TrxType, ChequeID, ChequeDate, Amount, ForeignAmount, ExchangeRate, ProfitLoss, MeanRate,  
     DescriptionID, [Description], BankCode, BranchCode, TrxPrinted, [Status], IsLocalCurrency, OperatorID, SupervisorID,  
     AdditionalData, Remarks, IsMainTrx, DocType, GLVoucherID, SourceBranch, TargetBranch, GLID)  
       
     VALUES (@OurBranchID,@ScrollNo,@SerialNo,@Refno,@mDate,@CustomerAccountType,'OC',@CustomerAccountID,@CustomerAccountName,  
     @CustomerProductID,@CustomerCurrencyID, @ValueDate,@CustomerTrxType,@ChequeID,@ChequeDate, @Amount, @ForeignAmount,@ExchangeRate,  
     0,@ExchangeRate,@DescriptionID,@Description,@BankID,'', 0, 'C', @CustomerIsLocalCurrency, @OperatorID, @SupervisorID,  
     @AddData, @Remarks,'1','OC', @VoucherID, @SourceBranch, @TargetBranch, @GLID)  
       
     select @CsGLControl = ISNULL(GLControl, '') from t_Products where OurBranchID = @OurBranchID and ProductID = @CustomerProductID  
       
     if @CustomerTrxType = 'C'  
     begin  
       set @DescID = 'OL0'  
       set @Desc = 'Account ID: ' + @CustomerAccountID +', Account Name: ' + @CustomerAccountName +' , Module: Online-Cash Credit Entry...'  
     end  
     else  
     begin  
       set @DescID = 'OL1'  
       set @Desc = 'Account ID: ' + @CustomerAccountID +', Account Name: ' + @CustomerAccountName +' , Module: Online-Cash Debit Entry...'  
     end  
       
     insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],    
     CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,    
     Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType, SourceBranch, TargetBranch)    
       
     values (@OurBranchID, @CsGLControl, @VoucherID, '1', @mDate, @ValueDate, @DescID, @Desc,    
     @CustomerCurrencyID, @Amount, @ForeignAmount, @ExchangeRate, @IsCredit, 'OnLineCash', @TransactionMethod,  
     @OperatorID, @SupervisorID, '', '0', '0', '', '0', 'OC',@SourceBranch, @TargetBranch)    
       
  end  
  else  
  begin  
       
     insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],    
     CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,    
     Indicator, ScrollNo, SerialNo, AdditionalData, Remarks, IsMainTrx, DocType, GlID, SourceBranch, TargetBranch)    
       
     values (@OurBranchID, @CustomerAccountID, @VoucherID, '1', @mDate, @ValueDate, @DescriptionID, @Description,    
     @CustomerCurrencyID, @Amount, @ForeignAmount, @ExchangeRate, @IsCredit, 'OnLineCash', @TransactionMethod,  
     @OperatorID, @SupervisorID,'', @ScrollNo, @SerialNo, @AddData, @Remarks, '1', 'OC', @GlID, @SourceBranch, @TargetBranch)  
       
  end  
    
  if @CustomerTrxType = 'C'    
  begin    
    set @IsCredit = 0    
    set @DescriptionID = 'OC3'  
    set @Description = 'Account ID: ' + @CustomerAccountID +', Account Name: ' + @CustomerAccountName +' , Module: Online-Cash Debit Entry...'    
  end    
  else    
  begin    
    set @IsCredit = 1    
    set @DescriptionID = 'OC2'    
    set @Description = 'Account ID: ' + @CustomerAccountID +', Account Name: ' + @CustomerAccountName +' , Module: Online-Cash Credit Entry...'    
  end  
    
  set @VoucherID = @VoucherID + 1  
    
  insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],    
  CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,    
  Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType, SourceBranch, TargetBranch)    
    
  values (@OurBranchID, @GlID, @VoucherID, '1', @mDate, @ValueDate, @DescriptionID, @Description,    
  @CustomerCurrencyID, @Amount, @ForeignAmount, @ExchangeRate, @IsCredit, 'OnLineCash', @TransactionMethod,  
  @OperatorID, @SupervisorID,'', @ScrollNo, @SerialNo, @AddData, '0', 'OC', @SourceBranch, @TargetBranch)  
    
  SELECT 'Ok' AS RetStatus, 'TrxPosted' As RetMessage
  RETURN(0)  
    
 END