﻿
CREATE PROCEDURE [dbo].[spc_OL_UpdateTransactionStatus]
(
  @OurBranchID Varchar(30),
  @ScrollNo int,
  @RefNo Varchar(30),
  @SerialNo int,
  @Status char(1),
  @Supervised bit=0,
  @UpdateSupervision bit=0,
  @UpdateStatus bit=0
)

 AS
 
 SET NOCOUNT ON
 
 If @UpdateStatus = 1  And @UpdateSupervision = 0
 BEGIN
  Update t_OL_TransactionStatus Set Status = @Status
  Where OurBranchId = @OurBranchId And ScrollNo = @ScrollNo
 END
 
 If @UpdateStatus = 0 And @UpdateSupervision = 1
 BEGIN
  Update t_OL_TransactionStatus Set Supervised = @Supervised, RefNo = @RefNo
  Where OurBranchId = @OurBranchId And ScrollNo = @ScrollNo
 END
 
 If @UpdateStatus = 1  And @UpdateSupervision = 1
 BEGIN
  Update t_OL_TransactionStatus Set Status = @Status, Supervised = @Supervised, RefNo = @RefNo
  Where OurBranchId = @OurBranchId And ScrollNo = @ScrollNo
 END
 
 SELECT 'Ok' AS RetStatus, 'SavedSuccesfully' AS RetMessage