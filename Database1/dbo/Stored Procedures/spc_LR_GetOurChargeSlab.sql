﻿CREATE PROCEDURE [dbo].[spc_LR_GetOurChargeSlab] (
	@OurBranchID varchar(30),  
	@Apply varchar(20),  
	@Amount money,  
	@BankID varchar(30),
	@ChargeID varchar(30)=''  
)  
AS  
BEGIN
	set nocount on
	
	BEGIN TRY
		IF upper(@Apply) = 'BC'
		BEGIN
			IF @ChargeID <> ''
			BEGIN
				select 'Ok' as ReturnStatus, a.OurBranchID, a.BankID, a.ChargeID, d.Description as ChargeType, 
				a.ChargeName, a.PaymentOrder, a.PaySlip, a.DemandDraft, a.TeleTransfer, b.Charges, b.Minimum, 
				b.Maximum, b.IsPercentage, b.FromAmount, b.ToAmount, c.AccountID, c.Description as AccountName, 
				c.CurrencyID, a.AuthStatus as ChargeStatus, c.AuthStatus as GLStatus
				
				from t_LR_OurCharges a 
				LEFT JOIN t_LR_OurChargesSlab b on a.OurBranchID=b.OurBranchID AND a.BankID=b.BankID AND a.ChargeID=b.ChargeID
				INNER JOIN t_GL c on a.OurBranchID=c.OurBranchID AND a.AccountID=c.AccountID
				INNER JOIN t_LR_ChargeType d on a.OurBranchID=d.OurBranchID AND a.ChargeTypeID=d.ChargeTypeID
				Where (a.OurBranchID = @OurBranchID) AND (@Amount >= b.FromAmount) AND (@Amount <= b.ToAmount) 
				AND (a.BankersCheque = 1) AND (a.BankID = @BankID) AND a.ChargeID = @ChargeID
			END
			ELSE
			BEGIN
				select 'Ok' as ReturnStatus, a.OurBranchID, a.BankID, a.ChargeID, d.Description as ChargeType, 
				a.ChargeName, a.PaymentOrder, a.PaySlip, a.DemandDraft, a.TeleTransfer, b.Charges, b.Minimum, b.Maximum, 
				b.IsPercentage, b.FromAmount, b.ToAmount, c.AccountID, c.Description as AccountName, c.CurrencyID,
				a.AuthStatus as ChargeStatus, c.AuthStatus as GLStatus
				
				from t_LR_OurCharges a 
				LEFT JOIN t_LR_OurChargesSlab b on a.OurBranchID=b.OurBranchID AND a.BankID=b.BankID AND a.ChargeID=b.ChargeID
				INNER JOIN t_GL c on a.OurBranchID=c.OurBranchID AND a.AccountID=c.AccountID
				INNER JOIN t_LR_ChargeType d on a.OurBranchID=d.OurBranchID AND a.ChargeTypeID=d.ChargeTypeID
				Where (a.OurBranchID = @OurBranchID) AND (@Amount >= b.FromAmount) AND (@Amount <= b.ToAmount) 
				AND (a.BankersCheque = 1) AND (a.BankID = @BankID)  
			END
		END
		ELSE IF upper(@Apply) = 'PO'
		BEGIN
			IF @ChargeID <> ''
			BEGIN
				select 'Ok' as ReturnStatus, a.OurBranchID, a.BankID, a.ChargeID, d.Description as ChargeType, 
				a.ChargeName, a.PaymentOrder, a.PaySlip, a.DemandDraft, a.TeleTransfer, b.Charges, b.Minimum, b.Maximum, 
				b.IsPercentage, b.FromAmount, b.ToAmount, c.AccountID, c.Description as AccountName, c.CurrencyID,
				a.AuthStatus as ChargeStatus, c.AuthStatus as GLStatus
				
				from t_LR_OurCharges a 
				LEFT JOIN t_LR_OurChargesSlab b on a.OurBranchID=b.OurBranchID AND a.BankID=b.BankID AND a.ChargeID=b.ChargeID
				INNER JOIN t_GL c on a.OurBranchID=c.OurBranchID AND a.AccountID=c.AccountID
				INNER JOIN t_LR_ChargeType d on a.OurBranchID=d.OurBranchID AND a.ChargeTypeID=d.ChargeTypeID
				Where (a.OurBranchID = @OurBranchID) AND (@Amount >= b.FromAmount) AND (@Amount <= b.ToAmount) 
				AND (a.PaymentOrder = 1) AND (a.BankID = @BankID) AND a.ChargeID = @ChargeID
			END
			ELSE
			BEGIN
				select 'Ok' as ReturnStatus, a.OurBranchID, a.BankID, a.ChargeID, d.Description as ChargeType, 
				a.ChargeName, a.PaymentOrder, a.PaySlip, a.DemandDraft, a.TeleTransfer, b.Charges, b.Minimum, b.Maximum, 
				b.IsPercentage, b.FromAmount, b.ToAmount, c.AccountID, c.Description as AccountName, c.CurrencyID,
				a.AuthStatus as ChargeStatus, c.AuthStatus as GLStatus
				
				from t_LR_OurCharges a 
				LEFT JOIN t_LR_OurChargesSlab b on a.OurBranchID=b.OurBranchID AND a.BankID=b.BankID AND a.ChargeID=b.ChargeID
				INNER JOIN t_GL c on a.OurBranchID=c.OurBranchID AND a.AccountID=c.AccountID
				INNER JOIN t_LR_ChargeType d on a.OurBranchID=d.OurBranchID AND a.ChargeTypeID=d.ChargeTypeID
				Where (a.OurBranchID = @OurBranchID) AND (@Amount >= b.FromAmount) AND (@Amount <= b.ToAmount) 
				AND (a.PaymentOrder = 1) AND (a.BankID = @BankID)
			END
		END
		ELSE IF upper(@Apply) = 'PS'  
		BEGIN
			IF @ChargeID <> ''
			BEGIN
				select 'Ok' as ReturnStatus, a.OurBranchID, a.BankID, a.ChargeID, d.Description as ChargeType, 
				a.ChargeName, a.PaymentOrder, a.PaySlip, a.DemandDraft, a.TeleTransfer, b.Charges, b.Minimum, b.Maximum, 
				b.IsPercentage, b.FromAmount, b.ToAmount, c.AccountID, c.Description as AccountName, c.CurrencyID,
				a.AuthStatus as ChargeStatus, c.AuthStatus as GLStatus
				
				from t_LR_OurCharges a 
				LEFT JOIN t_LR_OurChargesSlab b on a.OurBranchID=b.OurBranchID AND a.BankID=b.BankID AND a.ChargeID=b.ChargeID
				INNER JOIN t_GL c on a.OurBranchID=c.OurBranchID AND a.AccountID=c.AccountID
				INNER JOIN t_LR_ChargeType d on a.OurBranchID=d.OurBranchID AND a.ChargeTypeID=d.ChargeTypeID
				Where (a.OurBranchID = @OurBranchID) AND (@Amount >= b.FromAmount) AND (@Amount <= b.ToAmount) 
				AND (a.PaySlip = 1) AND (a.BankID = @BankID) AND a.ChargeID = @ChargeID
			END
			ELSE
			BEGIN
				select 'Ok' as ReturnStatus, a.OurBranchID, a.BankID, a.ChargeID, d.Description as ChargeType, 
				a.ChargeName, a.PaymentOrder, a.PaySlip, a.DemandDraft, a.TeleTransfer, b.Charges, b.Minimum, b.Maximum, 
				b.IsPercentage, b.FromAmount, b.ToAmount, c.AccountID, c.Description as AccountName, c.CurrencyID,
				a.AuthStatus as ChargeStatus, c.AuthStatus as GLStatus
				
				from t_LR_OurCharges a 
				LEFT JOIN t_LR_OurChargesSlab b on a.OurBranchID=b.OurBranchID AND a.BankID=b.BankID AND a.ChargeID=b.ChargeID
				INNER JOIN t_GL c on a.OurBranchID=c.OurBranchID AND a.AccountID=c.AccountID
				INNER JOIN t_LR_ChargeType d on a.OurBranchID=d.OurBranchID AND a.ChargeTypeID=d.ChargeTypeID
				Where (a.OurBranchID = @OurBranchID) AND (@Amount >= b.FromAmount) AND (@Amount <= b.ToAmount) 
				AND (a.PaySlip = 1) AND (a.BankID = @BankID)  
			END
		END
		ELSE IF upper(@Apply) = 'DD'  
		BEGIN
			IF @ChargeID <> ''
			BEGIN
				select 'Ok' as ReturnStatus, a.OurBranchID, a.BankID, a.ChargeID, d.Description as ChargeType, 
				a.ChargeName, a.PaymentOrder, a.PaySlip, a.DemandDraft, a.TeleTransfer, b.Charges, b.Minimum, b.Maximum, 
				b.IsPercentage, b.FromAmount, b.ToAmount, c.AccountID, c.Description as AccountName, c.CurrencyID,
				a.AuthStatus as ChargeStatus, c.AuthStatus as GLStatus
				
				from t_LR_OurCharges a 
				LEFT JOIN t_LR_OurChargesSlab b on a.OurBranchID=b.OurBranchID AND a.BankID=b.BankID AND a.ChargeID=b.ChargeID
				INNER JOIN t_GL c on a.OurBranchID=c.OurBranchID AND a.AccountID=c.AccountID
				INNER JOIN t_LR_ChargeType d on a.OurBranchID=d.OurBranchID AND a.ChargeTypeID=d.ChargeTypeID
				Where (a.OurBranchID = @OurBranchID) AND (@Amount >= b.FromAmount) AND (@Amount <= b.ToAmount)
				AND (a.DemandDraft = 1) AND (a.BankID = @BankID) AND a.ChargeID = @ChargeID
			END
			ELSE
			BEGIN
				select 'Ok' as ReturnStatus, a.OurBranchID, a.BankID, a.ChargeID, d.Description as ChargeType, 
				a.ChargeName, a.PaymentOrder, a.PaySlip, a.DemandDraft, a.TeleTransfer, b.Charges, b.Minimum, b.Maximum, 
				b.IsPercentage, b.FromAmount, b.ToAmount, c.AccountID, c.Description as AccountName, c.CurrencyID,
				a.AuthStatus as ChargeStatus, c.AuthStatus as GLStatus
				
				from t_LR_OurCharges a 
				LEFT JOIN t_LR_OurChargesSlab b on a.OurBranchID=b.OurBranchID AND a.BankID=b.BankID AND a.ChargeID=b.ChargeID
				INNER JOIN t_GL c on a.OurBranchID=c.OurBranchID AND a.AccountID=c.AccountID
				INNER JOIN t_LR_ChargeType d on a.OurBranchID=d.OurBranchID AND a.ChargeTypeID=d.ChargeTypeID
				Where (a.OurBranchID = @OurBranchID) AND (@Amount >= b.FromAmount) AND (@Amount <= b.ToAmount)
				AND (a.DemandDraft = 1) AND (a.BankID = @BankID)
			END
		END
		ELSE IF upper(@Apply) = 'TT'  
		BEGIN
			IF @ChargeID <> ''
			BEGIN
				select 'Ok' as ReturnStatus, a.OurBranchID, a.BankID, a.ChargeID, d.Description as ChargeType, 
				a.ChargeName, a.PaymentOrder, a.PaySlip, a.DemandDraft, a.TeleTransfer, b.Charges, b.Minimum, b.Maximum, 
				b.IsPercentage, b.FromAmount, b.ToAmount, c.AccountID, c.Description as AccountName, c.CurrencyID,
				a.AuthStatus as ChargeStatus, c.AuthStatus as GLStatus
				
				from t_LR_OurCharges a 
				LEFT JOIN t_LR_OurChargesSlab b on a.OurBranchID=b.OurBranchID AND a.BankID=b.BankID AND a.ChargeID=b.ChargeID
				INNER JOIN t_GL c on a.OurBranchID=c.OurBranchID AND a.AccountID=c.AccountID
				INNER JOIN t_LR_ChargeType d on a.OurBranchID=d.OurBranchID AND a.ChargeTypeID=d.ChargeTypeID
				Where (a.OurBranchID = @OurBranchID) AND (@Amount >= b.FromAmount) AND (@Amount <= b.ToAmount) 
				AND (a.TeleTransfer= 1) and (a.BankID = @BankID) AND a.ChargeID = @ChargeID 
			END
			ELSE
			BEGIN
				select 'Ok' as ReturnStatus, a.OurBranchID, a.BankID, a.ChargeID, d.Description as ChargeType, 
				a.ChargeName, a.PaymentOrder, a.PaySlip, a.DemandDraft, a.TeleTransfer, b.Charges, b.Minimum, b.Maximum, 
				b.IsPercentage, b.FromAmount, b.ToAmount, c.AccountID, c.Description as AccountName, c.CurrencyID,
				a.AuthStatus as ChargeStatus, c.AuthStatus as GLStatus
				
				from t_LR_OurCharges a 
				LEFT JOIN t_LR_OurChargesSlab b on a.OurBranchID=b.OurBranchID AND a.BankID=b.BankID AND a.ChargeID=b.ChargeID
				INNER JOIN t_GL c on a.OurBranchID=c.OurBranchID AND a.AccountID=c.AccountID
				INNER JOIN t_LR_ChargeType d on a.OurBranchID=d.OurBranchID AND a.ChargeTypeID=d.ChargeTypeID
				Where (a.OurBranchID = @OurBranchID) AND (@Amount >= b.FromAmount) AND (@Amount <= b.ToAmount) 
				AND (a.TeleTransfer= 1) and (a.BankID = @BankID)  
			END
		END
		ELSE
		BEGIN
			raiserror('InvalidInstType', 15, 1)
		END
	END TRY
	BEGIN CATCH
		Select 'Error' as ReturnStatus, ERROR_MESSAGE() as ReturnMessage
	END CATCH
END