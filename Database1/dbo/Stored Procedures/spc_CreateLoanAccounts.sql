﻿CREATE procedure [dbo].[spc_CreateLoanAccounts]         
(         
   @OurBranchID nvarchar(30),
   @ClientID nvarchar(30),
   @ProductID nvarchar(30),
   @CreateNew nvarchar(1)='0'
)

AS

SET NOCOUNT ON

if not exists (select top 1 * from t_Customer where OurBranchID = @OurBranchID and ClientID = @ClientID)
begin
   select 'Error' AS [Status], 'MSG-1000027' AS MessageCode, @ClientID AS ClientID, '' AS AccountType, '' AS AccountID, '' AS AccountTitle, '' AS ProductID, '' AS ProductDescription, '' AS CurrencyID, '' AS CurrencyDescription         
   return
end

if not exists (select top 1 * from t_Products where OurBranchID = @OurBranchID and ProductID = @ProductID)         
begin         
   select 'Error' AS [Status], 'MSG-1000028' AS MessageCode, @ClientID AS ClientID, '' AS AccountType, '' AS AccountID, '' AS AccountTitle, '' AS ProductID, '' AS ProductDescription, '' AS CurrencyID, '' AS CurrencyDescription         
   return         
end            
         
if not exists (select top 1 * from t_Account where OurBranchID = @OurBranchID and ClientID = @ClientID)         
begin         
   select 'Error' AS [Status], 'MSG-1000029' AS MessageCode, @ClientID AS ClientID, '' AS AccountType, '' AS AccountID, '' AS AccountTitle, '' AS ProductID, '' AS ProductDescription, '' AS CurrencyID, '' AS CurrencyDescription         
   return         
end         
         
declare @IsAdvProd nvarchar(30)         
declare @FinanceProductType nvarchar(30)         
declare @RecProductID nvarchar(30)         
declare @CapProductID nvarchar(30)         
declare @TrxProductID nvarchar(30)         
declare @FirstProductID nvarchar(30)         
declare @SecondProductID nvarchar(30)         
declare @ProdDesc nvarchar(100)         
declare @CurrID nvarchar(30)         
declare @CurrDesc nvarchar(100)         
         
create table #LoanAccounts         
(         
   Status nvarchar(10),         
   MessageCode nvarchar(30),         
   ClientID nvarchar(30),         
   AccountType nvarchar(30),         
   AccountID nvarchar(30),         
   AccountTitle nvarchar(200),         
   ProductID nvarchar(30),         
   ProductDescription nvarchar(100),         
   CurrencyID nvarchar(30),         
   CurrencyDescription nvarchar(100)         
)         
         
         
select @IsAdvProd = ISNULL(p.IsAdvancesProduct,''), @ProductID = p.ProductID, @ProdDesc = p.Description, @CurrID = p.CurrencyID, @CurrDesc = c.Description,         
@FinanceProductType = ISNULL(p.FinanceProductType,''), @RecProductID = ISNULL(ReceivableProductID,''), @CapProductID = ISNULL(CapitalizedProductID,''),         
@TrxProductID = ISNULL(TransactionACProductID,''), @FirstProductID = ISNULL(FirstSearchProductID,''), @SecondProductID = ISNULL(SecondSearchProductID,'')         
from t_Products p         
inner join t_Currencies c on p.OurBranchID = c.OurBranchID and p.CurrencyID = c.CurrencyID         
where p.OurBranchID = @OurBranchID and p.ProductID = @ProductID         
         
if @IsAdvProd != 'Yes'         
begin         
   select 'Error' AS [Status], 'MSG-1000030' AS MessageCode, @ClientID AS ClientID, '' AS AccountType, '' AS AccountID, '' AS AccountTitle, '' AS ProductID, '' AS ProductDescription, '' AS CurrencyID, '' AS CurrencyDescription         
   return         
end         
         
if @FinanceProductType = ''         
begin         
   select 'Error' AS [Status], 'MSG-1000031' AS MessageCode, @ClientID AS ClientID, '' AS AccountType, '' AS AccountID, '' AS AccountTitle, '' AS ProductID, '' AS ProductDescription, '' AS CurrencyID, '' AS CurrencyDescription         
   return         
end         
     
if @CreateNew = '1'     
begin     
   exec spc_CreateLoanAccount @OurBranchID, @ClientID, @ProductID         
         
   insert into #LoanAccounts         
   select 'Ok', 'MSG-000000', @ClientID, 'Loan', a.AccountID, a.Name, p.ProductID, p.Description, c.CurrencyID, c.Description         
   from t_Account a         
   inner join t_Products p on a.OurBranchID = p.OurBranchID and a.ProductID = p.ProductID       
   inner join t_Currencies c on a.OurBranchID = c.OurBranchID and a.CurrencyID = c.CurrencyID         
   where a.OurBranchID = @OurBranchID and a.ClientID = @ClientID and a.ProductID = @ProductID         
end     
else     
begin         
if exists (select top 1 * from t_Account where OurBranchID = @OurBranchID and ClientID = @ClientID and ProductID = @ProductID)         
begin         
 insert into #LoanAccounts         
 select 'Ok', 'MSG-000000', @ClientID, 'Loan', a.AccountID, a.Name, p.ProductID, p.Description, c.CurrencyID, c.Description         
 from t_Account a         
 inner join t_Products p on a.OurBranchID = p.OurBranchID and a.ProductID = p.ProductID         
 inner join t_Currencies c on a.OurBranchID = c.OurBranchID and a.CurrencyID = c.CurrencyID         
 where a.OurBranchID = @OurBranchID and a.ClientID = @ClientID and a.ProductID = @ProductID         
end         
else         
begin         
    exec spc_CreateLoanAccount @OurBranchID, @ClientID, @ProductID         
         
 insert into #LoanAccounts         
 select 'Ok', 'MSG-000000', @ClientID, 'Loan', a.AccountID, a.Name, p.ProductID, p.Description, c.CurrencyID, c.Description         
 from t_Account a         
 inner join t_Products p on a.OurBranchID = p.OurBranchID and a.ProductID = p.ProductID         
 inner join t_Currencies c on a.OurBranchID = c.OurBranchID and a.CurrencyID = c.CurrencyID         
 where a.OurBranchID = @OurBranchID and a.ClientID = @ClientID and a.ProductID = @ProductID         
end         
end     
if @RecProductID != ''        
begin        
    if exists (select top 1 * from t_Account where OurBranchID = @OurBranchID and ClientID = @ClientID and ProductID = @RecProductID)        
 begin        
  insert into #LoanAccounts        
  select 'Ok', 'MSG-000000', @ClientID, 'Receivable', a.AccountID, a.Name, p.ProductID, p.Description, c.CurrencyID, c.Description        
  from t_Account a        
  inner join t_Products p on a.OurBranchID = p.OurBranchID and a.ProductID = p.ProductID        
  inner join t_Currencies c on a.OurBranchID = c.OurBranchID and a.CurrencyID = c.CurrencyID        
  where a.OurBranchID = @OurBranchID and a.ClientID = @ClientID and a.ProductID = @RecProductID        
 end        
 else        
    begin        
                
  exec spc_CreateLoanAccount @OurBranchID, @ClientID, @RecProductID        
             
  insert into #LoanAccounts        
     select 'Ok', 'MSG-000000', @ClientID, 'Receivable', a.AccountID, a.Name, p.ProductID, p.Description, c.CurrencyID, c.Description        
     from t_Account a        
     inner join t_Products p on a.OurBranchID = p.OurBranchID and a.ProductID = p.ProductID        
     inner join t_Currencies c on a.OurBranchID = c.OurBranchID and a.CurrencyID = c.CurrencyID        
     where a.OurBranchID = @OurBranchID and a.ClientID = @ClientID and a.ProductID = @RecProductID        
    end        
end        
        
if @CapProductID != ''        
begin        
    if exists (select top 1 * from t_Account where OurBranchID = @OurBranchID and ClientID = @ClientID and ProductID = @CapProductID)        
 begin        
  insert into #LoanAccounts        
  select 'Ok', 'MSG-000000', @ClientID, 'Capitalize', a.AccountID, a.Name, p.ProductID, p.Description, c.CurrencyID, c.Description        
  from t_Account a        
  inner join t_Products p on a.OurBranchID = p.OurBranchID and a.ProductID = p.ProductID        
  inner join t_Currencies c on a.OurBranchID = c.OurBranchID and a.CurrencyID = c.CurrencyID        
  where a.OurBranchID = @OurBranchID and a.ClientID = @ClientID and a.ProductID = @CapProductID        
 end        
 else        
    begin        
                
  exec spc_CreateLoanAccount @OurBranchID, @ClientID, @CapProductID        
                 insert into #LoanAccounts        
     select 'Ok', 'MSG-000000', @ClientID, 'Capitalize', a.AccountID, a.Name, p.ProductID, p.Description, c.CurrencyID, c.Description        
     from t_Account a        
     inner join t_Products p on a.OurBranchID = p.OurBranchID and a.ProductID = p.ProductID      
     inner join t_Currencies c on a.OurBranchID = c.OurBranchID and a.CurrencyID = c.CurrencyID        
     where a.OurBranchID = @OurBranchID and a.ClientID = @ClientID and a.ProductID = @CapProductID        
    end        
end        
        
if @TrxProductID != ''        
begin        
    if exists (select top 1 * from t_Account where OurBranchID = @OurBranchID and ClientID = @ClientID and ProductID = @TrxProductID)        
 begin        
  insert into #LoanAccounts        
  select 'Ok', 'MSG-000000', @ClientID, 'Transaction', a.AccountID, a.Name, p.ProductID, p.Description, c.CurrencyID, c.Description        
  from t_Account a        
  inner join t_Products p on a.OurBranchID = p.OurBranchID and a.ProductID = p.ProductID        
  inner join t_Currencies c on a.OurBranchID = c.OurBranchID and a.CurrencyID = c.CurrencyID        
  where a.OurBranchID = @OurBranchID and a.ClientID = @ClientID and a.ProductID = @TrxProductID        
 end        
 else        
    begin        

  exec spc_CreateLoanAccount @OurBranchID, @ClientID, @TrxProductID        
             
  insert into #LoanAccounts        
     select 'Ok', 'MSG-000000', @ClientID, 'Transaction', a.AccountID, a.Name, p.ProductID, p.Description, c.CurrencyID, c.Description        
     from t_Account a        
     inner join t_Products p on a.OurBranchID = p.OurBranchID and a.ProductID = p.ProductID        
     inner join t_Currencies c on a.OurBranchID = c.OurBranchID and a.CurrencyID = c.CurrencyID        
     where a.OurBranchID = @OurBranchID and a.ClientID = @ClientID and a.ProductID = @TrxProductID        
    end        
end        
        
select * from #LoanAccounts