﻿CREATE PROCEDURE [dbo].[spc_User_Token]    
(    
   @OurBranchID VARCHAR(30),    
   @Username VARCHAR(30),    
   @Token VARCHAR(50),    
   @CreateTime DATETIME,    
   @ExpireTime DATETIME,    
   @Type CHAR(1)    
)    
AS    
    
SET NOCOUNT ON    
    
Begin Try    
       
   IF EXISTS (SELECT Username from tbl_UserTokens WHERE OurBranchID = @OurBranchID AND Username = @Username)    
   BEGIN    
      DELETE FROM tbl_UserTokens WHERE OurBranchID = @OurBranchID AND Username = @Username;    
   END    
       
   INSERT INTO tbl_UserTokens (OurBranchID, Username, Token, CreateTime, ExpireTime, [Type])    
   VALUES (@OurBranchID, @Username, @Token, @CreateTime, @ExpireTime, @Type);    
       
   select 'Ok' As ReturnStatus, 'SavedSuccessfully'  AS ReturnMessage    
       
End Try    
Begin Catch    
       
   select 'Error' As ReturnStatus, Error_Message()  AS ReturnMessage    
       
End Catch