﻿
CREATE   PROCEDURE [dbo].[spc_SetTLDPD]
AS
SET NOCOUNT ON;
SET ANSI_WARNINGS OFF;
UPDATE A SET
A.NoOfDPD = ISNULL(B.NoOfDPD, 0)
from t_Disbursement a
inner join v_CalculateTLDPD b on a.OurBranchID = b.OurBranchID and a.AccountID = b.AccountID and a.DealID = b.DealID;
SELECT 'Ok' AS RetStatus;