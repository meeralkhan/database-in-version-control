﻿create procedure [dbo].[spc_ArchiveLogs]
AS

SET NOCOUNT ON

insert into LogsArchived ([Application], [Logged], [Level], [Message], [Logger], [Callsite], [Exception],
[UserId], [Email], [DeviceID], [IP], [FunctionName], [ControllerName], [StartTime], [EndTime], [Status],
[RequestParameters], [RequestResponse], [Channel], [LogId], [RequestDateTime], [ErrorCode])
select [Application], [Logged], [Level], [Message], [Logger], [Callsite], [Exception],
[UserId], [Email], [DeviceID], [IP], [FunctionName], [ControllerName], [StartTime], [EndTime],
[Status], [RequestParameters], [RequestResponse], [Channel], [LogId], [RequestDateTime], [ErrorCode]
from [Logs]

truncate table [Logs];

select 'Ok' AS ReturnStatus, 'Done' AS ReturnMessage