﻿CREATE PROCEDURE [dbo].[spc_GetOutwardClearing] (        
 @OurBranchID varchar(30),          
 @ScrollNo numeric(10,0)=0      
)        
AS        
BEGIN        
        
 SET NOCOUNT ON        
        
 declare @ScrollSum numeric(18,2),    
   @SerialCount int,          
   @TotalRecord int,          
   @ReturnValue as char(1)          
          
 set @ReturnValue='0'          
          
 select @ScrollSum=sum(Amount), @SerialCount=Max(SerialNo), @TotalRecord=Count(SerialNo)        
 from t_OutwardClearing        
 where (OurBranchID=@OurBranchID) and ScrollNo=@ScrollNo        
         
 If Exists( select ScrollNo From t_OutwardClearing         
   where OurBranchID=@OurBranchID and ScrollNo=@ScrollNo )     
 Begin        
  set @ReturnValue='1'          
          
  select @ReturnValue as ReturnValue, a.ClearingType, a.ScrollNo, a.SerialNo, @SerialCount as SerialCount, @TotalRecord as TotalRecord,     
  a.ValueDate, a.wDate, a.AccountType, a.TransactionType, a.AccountID, a.AccountName,     
  isnull(c.ProductID,'GL') as ProductID, isnull(c.[Description], 'General Ledger') as ProductName,    
  b.CurrencyID, b.[Description] as CurrencyName, a.ChequeID, a.ChequeDate, a.IsLocalCurrency, a.Amount, a.VoucherAmount,    
  a.ForeignAmount, a.ExchangeRate, a.TheirAccount, a.DescriptionID, a.[Description], a.DrawerName, a.BankCode,     
  d.FullName, a.BranchCode, a.BranchName, a.[Status], @ScrollSum as ScrollSum, isnull(c.MinBalance,0) as ProductMinBalance,    
  a.AdditionalData    
      
  from t_OutwardClearing a    
  INNER JOIN t_Currencies b on a.OurBranchID=b.OurBranchID AND a.CurrencyID=b.CurrencyID    
  LEFT JOIN t_Products c on a.OurBranchID=c.OurBranchID AND a.ProductID=c.ProductID     
  INNER JOIN t_Banks d on a.OurBranchID=d.OurBranchID AND a.BankCode=d.BankID    
      
  where a.OurBranchID = @OurBranchID and a.ScrollNo = @ScrollNo      
 End        
 Else          
 BEGIN        
  SELECT @ReturnValue as ReturnValue        
 END        
           
END