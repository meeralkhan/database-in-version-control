﻿CREATE Procedure [dbo].[spc_GetCleanCashBook]          
 (            
  @OurBranchID varchar(30)='100'            
 )            
As            
          
set nocount on          
              
 IF @OurBranchID<>''            
 BEGIN            
       
 --  CASH             
 SELECT distinct Description, VC.ProductID ,VC.AccountID, VC.CurrencyID,            
 DrAmount=            
 -- case when vc_GetCleanCashBook.AccountID=vc_GetCleanCashBookB.AccountID then            
             
 -- Sum(vc_GetCleanCashBook.DrAmount) - Sum(vc_GetCleanCashBookB.Amount)             
 --else            
  Sum(VC.DrAmount)             
 --end             
  ,            
            
 CrAmount=             
 --case when vc_GetCleanCashBook.AccountID=vc_GetCleanCashBookC.AccountID then            
             
 -- Sum(vc_GetCleanCashBook.CrAmount) - Sum(vc_GetCleanCashBookC.Amount)             
 --else            
  Sum(VC.CrAmount)             
 --end            
  ,            
            
 --Sum(vc_GetCleanCashBook.CrAmount) as CrAmount,            
 Isnull(Sum(VC.NoOfDrTrx),0) as NoOfDrTrx, Isnull(Sum(VC.NoOfCrTrx),0) as NoOfCrTrx, Sum(VC.DrFAmount) as DrFAmount,      
 Sum(VC.CrFAmount) as CrFAmount      
       
 From vc_GetCleanCashBook VC       
 where VC.OurBranchID = @OurBranchID      
 /*left outer join  vc_GetCleanCashBookB             
 on             
 vc_GetCleanCashBook.Accountid=vc_GetCleanCashBookB.Accountid            
            
 left outer join  vc_GetCleanCashBookC             
 on             
 vc_GetCleanCashBook.Accountid=vc_GetCleanCashBookC.Accountid            
*/            
 Group by VC.Description,VC.ProductID, VC.AccountID, VC.CurrencyID            
 --, vc_GetCleanCashBookb.AccountID,vc_GetCleanCashBookC.Accountid            
 Order by ProductID            
            
END