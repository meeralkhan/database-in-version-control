﻿CREATE PROCEDURE [dbo].[spc_SetBalanceSheetAcc]

AS

SET NOCOUNT ON

delete from t_BalanceSheetAcc where RelevantPool = 'Equity'

insert into t_BalanceSheetAcc (CreateBy, CreateTime, CreateTerminal, AuthStatus, VerifyStatus, frmName, OurBranchID, AccountCode, GLName, RelevantPool, [Type])
select 'System', GETDATE(), 'System', '', 'Y', 'System', '079', AccountID, STRING_AGG(GLName,' ') GLName, 'Equity', 'I'
from GL_rEjmiTzvsa4AToScbGkhB where (substring(AccountID,4,1) = '9' OR AccountID IN ('1103200026', '3023000002', '4070000005')) and AccountID NOT IN (select AccountCode from t_BalanceSheetAcc)
group by AccountID


insert into t_BalanceSheetAcc (CreateBy, CreateTime, CreateTerminal, AuthStatus, VerifyStatus, frmName, OurBranchID, AccountCode, GLName, RelevantPool, [Type])
select 'System', GETDATE(), 'System', '', 'Y', 'System', '079', AccountID, STRING_AGG(GLName,' ') GLName, 'Equity', 'C'
from GL_rEjmiTzvsa4AToScbGkhB where substring(AccountID,4,1) != '9' and AccountID NOT IN ('1103200026', '3023000002', '4070000005') and AccountID NOT IN (select AccountCode from t_BalanceSheetAcc)
group by AccountID

--select * from t_BalanceSheetAcc where AccountCode = '2059010048'

select 'Ok' AS ReturnStatus