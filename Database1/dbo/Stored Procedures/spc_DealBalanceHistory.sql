﻿CREATE   PROCEDURE [dbo].[spc_DealBalanceHistory]
AS
SET NOCOUNT ON
INSERT INTO t_DealBalanceHistory  (wDate, OurBranchID, AccountID, DealID, ProductID, Name, OutstandingBalance, OutstandingBalanceLCY, Accrual, PAccrual, tAccrual, tPAccrual, RiskCode, NoOfDPD)
select c.LASTEOD,a.OurBranchID,a.AccountID,a.DealID,b.ProductID,b.Name,isnull(dob.Balance,0) OutstandingBalance,
CONVERT(DECIMAL(22,2),case b.CurrencyID when 'AED' then dob.Balance else dob.Balance * cur.MeanRate end,2) OutstandingBalanceLCY,
case when c.WORKINGDATE > a.MaturityDate then 0 else isnull(a.Accrual,0) end Accrual,0 PAccrual, isnull(a.tAccrual,0) tAccrual,isnull(a.tPAccrual,0) tPAccrual,
ISNULL(a.RiskCode, ''), ISNULL(a.NoOfDPD, 0)
from t_Disbursement a
inner join v_DealOutstandingBalance dob on a.DealID = dob.DealID and a.AccountID = dob.AccountID
inner join t_Account b on a.OurBranchID = b.OurBranchID and a.AccountID = b.AccountID
inner join t_Currencies cur on cur.CurrencyID = b.CurrencyID and cur.OurBranchID = b.OurBranchID
inner join t_Last c on a.OurBranchID = c.OurBranchID
where isnull(a.IsClosed,0) = 0

SELECT 'Ok' AS RetStatus