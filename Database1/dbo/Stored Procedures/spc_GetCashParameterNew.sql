﻿CREATE PROCEDURE [dbo].[spc_GetCashParameterNew]            
(                
 @OurBranchID Varchar(30),                
 @Amount money=0,                
 @mAccountID  Varchar(30),                
 @mProductID  Varchar(30)='',                
 @BranchType Char(1)='I'                  
)                
                
AS                
        
SET NOCOUNT ON        

/*
if object_id('dbo.temp_DailyTaxCalc') is not null            
BEGIN            
 drop table temp_DailyTaxCalc            
END            
            
create table temp_DailyTaxCalc            
(            
 AppWHTax bit NOT NULL DEFAULT(0),            
 ChargeRate money NOT NULL DEFAULT(0),            
 ChargesAmount money NOT NULL DEFAULT(0),            
 GLSerialID nvarchar(max) NOT NULL DEFAULT(0),            
 AccountID varchar(30) NOT NULL DEFAULT(''),            
 [Description] varchar(50) NOT NULL DEFAULT(''),            
 CurrencyID varchar(30) NOT NULL DEFAULT('')            
)            
            
If @BranchType='I'                
Begin                
                
 DECLARE @AppTax AS bit              
 DECLARE @SerialID AS nvarchar(max)                
 DECLARE @Counter AS Integer                
 DECLARE @Type AS Char(1)                
 DECLARE @ChargeAmount AS money                
 DECLARE @ChargeRate AS money                
 DECLARE @AmountFrom AS money                
----------------------------added by ilyas----------                
 DECLARE @PreviousAmount AS money                
----------------------------------------------------                
----------------------------added by S. Farhan Zaidi----------              
 DECLARE @TaxFiler as bit            
 DECLARE @TaxFilerRate as money            
----------------------------------------------------              
 DECLARE @AccountID AS Varchar(30)                
 DECLARE @Description AS Varchar(255)                
 DECLARE @CurrencyID AS Varchar(30)                
                 
select @AppTax=ISNULL(ApplyTax,0), @TaxFiler=isnull(TaxFiler, 0) from t_Account               
where accountid=@mAccountID And ProductID=@mProductID              
              
if @AppTax = 1 --Means With Holding Tax is Exempted            
BEGIN              
 insert into temp_DailyTaxCalc (AppWHTax, ChargesAmount, GLSerialID) values (0, 0, 0)            
 Return            
END              
              
if @TaxFiler = 1 --Means No Tax will be deducted because its a taxfiler            
BEGIN              
 insert into temp_DailyTaxCalc (AppWHTax, ChargesAmount, GLSerialID) values (0, 0, 0)            
 Return            
END              
            
------------------------- added by ilyas-----------------------                
 set @PreviousAmount=(Select isnull(CashTotDr,0) from t_Account                
 where accountid=@mAccountID And ProductID=@mProductID)      
       
-----------------------------------------------------------------                
 set @PreviousAmount=isnull(@PreviousAmount,0)                
                
 SELECT @AccountID = '',@SerialID = 0, @Counter = 0, @ChargeAmount = 0            
 , @Description = '' , @CurrencyID = '', @ChargeRate = 0 , @AmountFrom = 0                
                
 SELECT @SerialID = GLParameterID, @Counter = 1 , @Type = Type, @ChargeRate = NonTaxFilerAmount, @TaxFilerRate = TaxFilerAmount,            
 @ChargeAmount = NonTaxFilerAmount, @AmountFrom = AmountFrom             
 From t_CashParameter WHERE @PreviousAmount+@Amount >= AmountFrom AND @Amount < AmountTo AND Module = 'Transfer'            
                 
            
------------------------- added by S. Farhan Zaidi-----------------------              
if @TaxFiler = 0            
begin            
 set @ChargeRate=@ChargeRate            
end            
else            
begin            
 set @ChargeRate=@TaxFilerRate            
end            
-----------------------------------------------------------------               
       
 IF @Counter <> 0 and (@PreviousAmount+@Amount-@AmountFrom) <> 0                
  BEGIN                
           
  SELECT @AccountID = AccountID, @Description = Description , @CurrencyID =CurrencyID                 
  From t_GL WHERE OurBranchID = @OurBranchID AND AccountID =                 
  (SELECT AccountID From t_GLParameters WHERE OurBranchID = @OurBranchID AND SerialID = @SerialID)                
                
                
   IF @Type = 'R'                
    if @PreviousAmount<=@AmountFrom                
     SELECT @ChargeAmount = ((@PreviousAmount+@Amount) * @ChargeRate/ 100)                 
    else if @PreviousAmount>@AmountFrom                
     SELECT @ChargeAmount = ((@Amount) * @ChargeRate / 100)                 
                   
   IF ROUND(@ChargeAmount,0) > 0                 
                
    insert into temp_DailyTaxCalc (AppWHTax, ChargeRate, ChargesAmount, GLSerialID, AccountID, Description, CurrencyID)            
    values (1, @ChargeRate, ROUND(@ChargeAmount,0), @SerialID, @AccountID, @Description, @CurrencyID)            
                
                
   ELSE                
               
    insert into temp_DailyTaxCalc (AppWHTax, ChargesAmount, GLSerialID) values (0, 0, @SerialID)            
                
  END       ELSE                
              
  insert into temp_DailyTaxCalc (AppWHTax, ChargesAmount, GLSerialID) values (0, 0, 0)            
                
End                
                
Else                
              
  insert into temp_DailyTaxCalc (AppWHTax, ChargesAmount, GLSerialID) values (0, 0, 0) 
*/