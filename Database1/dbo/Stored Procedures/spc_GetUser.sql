﻿CREATE PROCEDURE [dbo].[spc_GetUser]        
(          
   @OurBranchID     varchar(30),          
   @OperatorID      varchar(30),          
   @ProgramID       varchar(50)          
)          
          
AS          
      
SET NOCOUNT ON      
          
   DECLARE @Password as Varchar(32)          
   DECLARE @TrustLevel as Int          
       
   IF ( SELECT Count(OperatorID) FROM t_Map           
               WHERE OurBranchID = @OurBranchID AND OperatorID = @OperatorID ) > 0           
      BEGIN          
         IF ( SELECT Count(OperatorID) FROM t_MapDrive           
               WHERE OurBranchID = @OurBranchID AND OperatorID = @OperatorID AND FormToRun = @ProgramID AND AuthorizeAccess = 1 AND Auth = 0 ) > 0           
            BEGIN          
                 
               if (@ProgramID = 'frmCashRemote' OR @ProgramID = 'frmCashRemoteAccess' OR @ProgramID = 'frmRemoteAccess' OR @ProgramID = 'frmTransferRemote')  
               begin  
                    
                  SELECT '1' as RetStatus, m.OperatorID, md.Password, m.TrustLevel, md.CreditSupervisionLimit, md.DebitSupervisionLimit  
                  FROM t_Map m  
                  INNER JOIN t_MapDrive md ON m.OurBranchID = md.OurBranchID AND m.OperatorID = md.OperatorID  
                  WHERE m.OurBranchID = @OurBranchID AND m.OperatorID = @OperatorID AND md.FormToRun = @ProgramID  
                    
               end  
               else  
               begin  
                    
                  SELECT '1' as RetStatus, m.OperatorID, m.Password, m.TrustLevel, md.CreditSupervisionLimit, md.DebitSupervisionLimit  
                  FROM t_Map m  
                  INNER JOIN t_MapDrive md ON m.OurBranchID = md.OurBranchID AND m.OperatorID = md.OperatorID  
                  WHERE m.OurBranchID = @OurBranchID AND m.OperatorID = @OperatorID AND md.FormToRun = @ProgramID  
                    
               end  
                 
                       
            END          
         ELSE          
            BEGIN          
               SELECT '2' AS RetStatus --Permission Denied          
            END          
      END          
          
   ELSE          
      SELECT '0' AS RetStatus --User Not Found 