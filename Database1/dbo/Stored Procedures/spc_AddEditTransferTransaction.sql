﻿CREATE      PROCEDURE [dbo].[spc_AddEditTransferTransaction]                                 
(                                 
 @ScrollNo money,                                 
 @SerialID int,                                 
 @OurBranchID varchar(30),                                 
 @AccountID varchar(30),                                 
 @AccountName varchar(100)='',                                 
 @ProductID varchar(30)='GL',                                 
 @CurrencyID varchar(30),                                 
 @AccountType Char(1),                                 
 @wDate datetime,                                 
 @ValueDate datetime=@wDate,                                 
 @TrxType Char(1)='',                                 
 @ChequeID varchar(30)='V',                                 
 @ChequeDate DateTime=@wDate,                                 
 @Amount money,                                 
 @ForeignAmount Money,                                 
 @ExchangeRate decimal(18,6)=1,                                 
 @DescriptionID varchar(30)='',                                 
 @Description nvarchar(max)='',                                 
 @AddData TEXT = '',                                 
 @BankCode varchar(30),                                 
 @BranchCode varchar(30),                                 
 @GlID varchar(30)='',                                 
 @Supervision char(1)='',                                 
 @IsSupervision bit=0,                                 
 @OperatorID varchar(30),                                 
 @SupervisorID varchar(30)='',                                 
-- @RemotePass bit=0,                                 
-- @ClearBalance money=0,                                 
-- @OutDp Money=0,                                 
-- @PassBy varchar(8)='',                                 
 @RemoteDescription varchar(100)='',                                 
-- @EffectiveBalance money=0,                                 
 @Status char(1)='',                                 
 @IsLocalCurrency int=1,                                 
-- @DebitPostingLimit money=0,                                 
 @PostingLimit money=0,                                 
 @IsOverDraft bit=1,                                 
 @TrxPrinted bit=0,                                 
 @ProfitOrLoss bit=0,                                 
 @TrxMode char(1)='C',                                 
 @RefNo varchar(100)='',                                 
 @AppWHTax bit = 0,                                 
 @WHTaxAmount money = 0,                                 
 @TotAmount money = 0,                                 
 @ChannelId varchar(30) = '',                                 
 @VirtualAccountID varchar(30) = '',                                 
 @VirtualAccountTitle varchar(100) = '',                                 
 @ChannelRefID varchar(200) = '',                                 
 @NewRecord bit = 1 ,                                 
 @TrxTimeStamp datetime = @wDate,                                 
 @PrevDayBalance int = 0,                                 
 @ChargeID nvarchar(30) = null ,                                 
 @VATId nvarchar(200)=null,                                 
 @VatPercent nvarchar(200)=null ,                                 
 @PrevYearEntry int = 0 ,                                 
 @TClientId nvarchar(30)=null,                                 
 @TAccountId nvarchar(30)=null,                                 
@TProductId nvarchar(30)=null,                                 
@CostCenterID nvarchar(30)=null,                                
@GLVendorID nvarchar(30) = null,                                
@memo2 nvarchar(250) = null,                          
@VendorID nvarchar(250) = null,                          
@CostTypeID nvarchar(250) = null,                          
@CustomerLifecycleID nvarchar(250) = null,                      
@ProjectId nvarchar(100)=null        ,    
@PostExpenseID nvarchar(30) = null  ,  
@VatReferenceID nvarchar(30) =null  
 )                                 
                                 
AS                                 
 SET NOCOUNT ON                                 
                                 
 DECLARE @LocalCurrency as NVarchar(30)                                 
 select @LocalCurrency = LocalCurrency from t_GlobalVariables where OurBranchID = @OurBranchID;                      
                             
 if (UPPER(@AccountType) = 'C')                            
 begin                            
    select @ProductID = ProductID, @CurrencyID = CurrencyID from t_Account where OurBranchID = @OurBranchID and AccountID = @AccountID                            
 end                            
                        
 if (UPPER(@CurrencyID) = UPPER(@LocalCurrency))                                 
 set @IsLocalCurrency = 1                                 
 else              
 set @IsLocalCurrency = 0                                 
                                 
 DECLARE @RetStatus as int                                 
 DECLARE @Amt as Money                                 
 DECLARE @IsDebit as Bit                                 
 DECLARE @mDate as Varchar(50)                                 
                                 
 DECLARE @GoInEffects as Int                                 
 DECLARE @LocEq as Money                                 
 DECLARE @tGLID as Varchar(30)                                 
 DECLARE @CustomDescription as nVarchar(max) = @Description                                 
                                 
 set @Description = REPLACE(@Description,'~',' ')                                 
 set @tGLID = @GlID                                 
 SET @mDate = Convert(varchar(10),@wDate ,101) + ' ' + convert(varchar(20),GetDate(),114)                                 
 IF(@RefNo = 'ExFunds' OR @RefNo = 'ShFunds')                          
 BEGIN                          
 SET @mDate = Convert(varchar(10),@wDate ,101)                          
 END                          
                                 
 if @GlID = ''                                 
 set @tGLID = null                                 
                      
 BEGIN                                 
 SET @RetStatus = 0                                 
                                 
 IF @IsLocalCurrency = 0                                 
 SELECT @Amt = @ForeignAmount, @LocEq = @Amount                                 
 ELSE                                 
 BEGIN                                 
 IF @AppWHTax = 0                                 
 SELECT @Amt =                         
 @Amount, @LocEq = @Amount                                 
 ELSE                                 
 SELECT @Amt = @totAmount, @LocEq = @Amount                                 
 END                                 
                                 
 IF @TrxType = 'C'                                 
                                 
 SELECT @IsDebit = 0                                 
 ELSE                                 
 SELECT @IsDebit = 1                                 
                                 
 declare @DailyTaxCalcAppWHTax as bit                                 
 set @DailyTaxCalcAppWHTax = 0                                 
                                 
 if @IsDebit = 1 AND @DescriptionID IN (select DescriptionID from t_TransactionDescriptions where                                 
 OurBranchID = @OurBranchID AND DescriptionID = @DescriptionID AND ISNULL(IsInternal,1) = 0 and IsCredit = 0) AND @AccountType = 'C'                                 
                                 
 begin                                 
 exec spc_GetCashParameterNew @OurBranchID, @Amt, @AccountID, @ProductID                                 
                                 
                                 
 declare @ClubWHTaxAmt as money                                 
 declare @WHTChargesAmount money                                 
                                 
 select @DailyTaxCalcAppWHTax = AppWHTax, @WHTChargesAmount=ChargesAmount                                 
 from temp_DailyTaxCalc                                 
                                 
 if @DailyTaxCalcAppWHTax = 1                                 
 begin                                 
 set @ClubWHTaxAmt = @Amt + @WHTChargesAmount                                 
                     
 EXEC @RetStatus = spc_AmountVerify @OurBranchID, @AccountID, @TrxType, @ClubWHTaxAmt, @AccountType, @Supervision,                                 
 @PostingLimit, @SupervisorID                                 
, @LocEq                                 
 end                                 
 else                   
 begin                                 
                                 
 EXEC @RetStatus = spc_AmountVerify @OurBranchID, @AccountID, @TrxType, @Amt, @AccountType, @Supervision,                                 
 @PostingLimit, @SupervisorID, @LocEq                                 
 end                                 
 end                                 
 else                                 
 begin                                 
                                 
 EXEC @RetStatus = spc_AmountVerify @OurBranchID, @AccountID, @TrxType, @Amt, @AccountType, @Supervision,                                 
 @PostingLimit, @SupervisorID, @LocEq                                 
 end                                 
                                 
                                 
 IF @RetStatus = 97                                 
 SET @Supervision = 'C'                                 
 ELSE IF @RetStatus = 98                                 
 SET @Supervision = '*'                                 
 ELSE IF @RetStatus = 99                                 
                          
 SET @Supervision = 'P'                                 
 ELSE                                 
 BEGIN                                 
 SELECT @RetStatus as Status, 'V' as Supervision                                 
 RETURN(1)                                 
                                 
 END                                 
                                 
 declare @WHTScrollNo int                                 
 set @WHTScrollNo = 0                                 
                                 
 if @DailyTaxCalcAppWHTax = 1 AND @IsDebit = 1 AND @DescriptionID IN                                 
 (select DescriptionID from t_TransactionDescriptions where                                 
 OurBranchID = @OurBranchID AND DescriptionID = @DescriptionID AND ISNULL(IsInternal,1) = 0 and IsCredit = 0) AND @AccountType = 'C'                                 
begin                                 
 select @WHTScrollNo=@ScrollNo+1                                 
 end                                 
                                 
 if @WHTScrollNo = @ScrollNo                                 
 begin                                 
 set @WHTScrollNo = @WHTScrollNo + 1                                 
 end                                 
                                 
 INSERT INTO t_TransferTransactionModel                                 
 (ScrollNo,SerialNo,OurBranchID,AccountID,AccountName,ProductID,CurrencyID,AccountType,ValueDate,wDate,TrxTimeStamp,TrxType,ChequeID,                                 
 ChequeDate, Amount, ForeignAmount,ExchangeRate,DescriptionID,[Description],AdditionalData,BankCode,BranchCode,                                 
 TrxPrinted,ProfitorLoss,Glid,Issupervision,Supervision,RemoteDescription,OperatorID,SupervisorID,Refno,IsLocalCurrency,                                 
 AppWHTax, WHTaxScrollNo,ChannelId,VirtualAccountID,VirtualAccountTitle,ChannelRefID, PrevDayEntry,ChargeID,VatID,[Percent],PrevYearEntry, TClientId ,TAccountid ,TProductId,CostCenterID                                 
 ,GLVendorID,memo2,VendorID,CostTypeID,CustomerLifecycleID,ProjectId,PostExpenseID,VatReferenceID)                                 
 VALUES (@ScrollNo,@SerialID,@OurBranchID,@AccountID,@AccountName,@ProductID,@CurrencyID,@AccountType,@ValueDate,@mDate,@TrxTimeStamp,                                 
 @TrxType,@ChequeID,@ChequeDate, @Amount, @ForeignAmount,@ExchangeRate,@DescriptionID,@Description,@AddData,@BankCode,                                 
 @BranchCode,@TrxPrinted,@ProfitorLoss,@Glid, @IsSupervision, @Supervision, @RemoteDescription,@OperatorID,                                  
 @SupervisorID,@Refno,@IsLocalCurrency, @DailyTaxCalcAppWHTax,@WHTScrollNo,@ChannelId,@VirtualAccountID,@VirtualAccountTitle,@ChannelRefID,                                 
 @PrevDayBalance,@ChargeID,@VatID,@VatPercent,@PrevYearEntry,@TClientId,@TAccountId,@TProductId,@CostCenterID,@GLVendorID,@memo2,@VendorID,@CostTypeID,@CustomerLifecycleID,@ProjectId,@PostExpenseID,@VatReferenceID)                                 
                                 
                                 
 declare @IsCredit int                                 
 declare @VoucherID int                    
 declare @TransactionMethod char(1)                                 
 declare @GLControl nvarchar                                 
(30)                                 
 declare @DescID nvarchar(30)                                 
 declare @Desc nvarchar(100)                                 
                                 
 if @Supervision = 'C'                                 
 begin                                 
                                 
                          
 select @VoucherID = @ScrollNo -- ISNULL(MAX(VoucherID),0) + 1 from t_GLTransactions where OurBranchID = @OurBranchID                                 
                                 
 if @TrxType = 'C'                                 
                                 
 begin                                 
 set @IsCredit = 1                                 
 set @DescID = 'TR0'                                 
 set @Desc = 'Account ID: ' + @AccountID +', Account Name: ' + @AccountName +' , Module: Transfer Credit Entry...'                                 
                                 
 end                                 
 else                                 
 begin                                 
 set @IsCredit = 0                                 
 set @DescID = 'TR1'                                 
 set @Desc = 'Account ID: ' +                                 
 @AccountID +', Account Name: ' + @AccountName +' , Module: Transfer Debit Entry...'                                 
end                                 
                                 
 if @IsLocalCurrency = 1                                 
 begin                                 
                                 
 set @ForeignAmount = 0                                 
 set @ExchangeRate = 1                                 
 set @TransactionMethod = 'L'                                 
 end                                 
 else                                 
 begin                                 
                                 
 set @TransactionMethod = 'A'                                 
 end                                 
                                 
 if @AccountType = 'C'                                 
 begin                                 
                                 
 insert into                                 
t_Transactions (OurBranchID, ScrollNo, SerialNo, RefNo, wDate,TrxTimeStamp, AccountType, DocumentType, AccountID,                                 
 AccountName, ProductID, CurrencyID, ValueDate, TrxType, ChequeID, ChequeDate, Amount, ForeignAmount,                                 
                                 
 ExchangeRate, ProfitLoss, MeanRate, DescriptionID, [Description], BankCode, BranchCode, TrxPrinted, GLID,                            
 [Status], IsLocalCurrency, OperatorID, SupervisorID, AdditionalData, IsMainTrx, DocType, GLVoucherID, AppWHTax,                                 
 WHTaxMode, WHTaxAmount, WHTScrollNo, ChannelId, VirtualAccountID, VirtualAccountTitle,ChannelRefID,PrevDayEntry,ChargeID,VatID,                                 
 [Percent],PrevYearEntry,TClientId ,TAccountid,TProductId,CostCenterID ,GLVendorID,memo2,VatReferenceID)                                 
                                 
                                 
 VALUES(@OurBranchID,@ScrollNo,@SerialID,@Refno,@mDate,@TrxTimeStamp,@AccountType,'T',@AccountID,@AccountName,@ProductID,                                 
 @CurrencyID,@ValueDate,@TrxType,@ChequeID,@ChequeDate,@Amount,@ForeignAmount,@ExchangeRate,@ProfitorLoss,                                 
 @ExchangeRate,@DescriptionID,@Description,@BankCode,@BranchCode,@TrxPrinted,@tGLID,@Supervision,@IsLocalCurrency,       
 @OperatorID, @SupervisorID,@AddData,'1','T'                                 
, @VoucherID,@AppWHTax,'T',@WHTaxAmount,@WHTScrollNo,                                 
 @ChannelId,@VirtualAccountID,@VirtualAccountTitle,@ChannelRefID,@PrevDayBalance,@ChargeID,@VATId,@VATPercent,@PrevYearEntry                                 
 ,@TClientId,@TAccountId,@TProductId,@CostCenterID,@GLVendorID,@memo2,@VatReferenceID)                                 
                                 
                                 
 select @GLControl = ISNULL(GLControl, '') from t_Products where OurBranchID = @OurBranchID and ProductID = @ProductID                                 
                                 
 insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate,TrxTimeStamp, DescriptionID,                                 
 [Description], CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod,                                 
 OperatorID, SupervisorID, Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType, ChannelId,                                 
 VirtualAccountID, VirtualAccountTitle,ChannelRefID,PrevDayEntry,TClientId ,TAccountid,TProductId,CostCenterID,VatID,[percent],GLVendorID,VendorID,CostTypeID,CustomerLifecycleID,ProjectId,PostExpenseID,VatReferenceID)                                 
                                 
                                 
 values (@OurBranchID, @GLControl, @VoucherID, '1', @mDate, @ValueDate,@TrxTimeStamp, @DescID, @Desc, @CurrencyID, @Amount,                                 
 @ForeignAmount, @ExchangeRate, @IsCredit, 'Transfer', @TransactionMethod, @OperatorID, @SupervisorID, '', '0',                                 
 '0', @AddData, '0', 'T', @ChannelId, @VirtualAccountID, @VirtualAccountTitle,@ChannelRefID,@PrevDayBalance,@TClientId,@TAccountId,@TProductId,@CostCenterID,@VATId,@VatPercent,@GLVendorID,                          
 @VendorID,@CostTypeID,@CustomerLifecycleID,@ProjectId,@PostExpenseID,@VatReferenceID)                                 
                                 
                                 
 end                                 
 else                                 
 begin                                 
                                 
 insert into t_GLTransactions (Refno,OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate,                                 
TrxTimeStamp, DescriptionID, [Description],                                 
 CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,                                 
 Indicator, ScrollNo, SerialNo,                                 
 AdditionalData, IsMainTrx, DocType, ChannelId, VirtualAccountID, VirtualAccountTitle,ChannelRefID,PrevDayEntry,                                 
 TClientId ,TAccountid ,TProductId,CostCenterID,VatID,[Percent],GLVendorID,memo2,VendorID,CostTypeID,CustomerLifecycleID,ProjectId,PostExpenseID,VatReferenceID)                                 
                                 
 values (@Refno,@OurBranchID, @AccountID, @VoucherID, '1', @mDate, @ValueDate,@TrxTimeStamp, @DescriptionID, @Description,                                 
 @CurrencyID, @Amount, @ForeignAmount, @ExchangeRate, @IsCredit, 'Transfer', @TransactionMethod, @OperatorID, @SupervisorID,                                 
 '', @ScrollNo, @SerialID, @AddData, '1', 'T', @ChannelId, @VirtualAccountID, @VirtualAccountTitle,@ChannelRefID,@PrevDayBalance                                 
                                 
 ,@TClientId,@TAccountId,@TProductId,@CostCenterID,@VATId,@VatPercent,@GLVendorID,@memo2,@VendorID,@CostTypeID,@CustomerLifecycleID,@ProjectId,@PostExpenseID,@VatReferenceID)                                 
                                 
 end                                 
                                 
 end                                 
                                 
 if @DailyTaxCalcAppWHTax = 1 AND @IsDebit = 1 AND @DescriptionID IN (select DescriptionID from t_TransactionDescriptions where                                 
 OurBranchID = @OurBranchID AND DescriptionID = @DescriptionID AND ISNULL(IsInternal,1) = 0 and IsCredit = 0) AND @AccountType = 'C'                                 
                                 
                                 
 begin                                 
 declare @WHTChargeRate money                                 
 declare @WHTGLSerialID int                                 
 declare @WHTAccountID varchar(30)                                 
                                 
 declare @WHTDescription varchar(50)                                 
 declare @WHTDescription2 varchar(255)                                 
 declare @WHTCurrencyID varchar(4)                                 
                                 
 select @WHTChargeRate=ChargeRate, @WHTChargesAmount=ChargesAmount, @WHTGLSerialID=GLSerialID,                                 
 @WHTAccountID=AccountID, @WHTDescription=[Description], @WHTCurrencyID=CurrencyID                                 
 from temp_DailyTaxCalc                                 
                                 
                                 
 --Customer                                 
 set @WHTDescription2 = 'Charge ' + cast(@WHTChargeRate as varchar(100)) + '/- Withholding Tax On Withdrawals Transaction(s)'                                 
 set @WHTDescription2 = @WHTDescription2 + ' While Cheque# ' + @ChequeID + ' And Amount: ' + cast(@Amount as varchar(50))                                 
                      
 INSERT INTO t_TransferTransactionModel                                 
 (ScrollNo,SerialNo,OurBranchID,AccountID,AccountName,ProductID,CurrencyID,AccountType,ValueDate,wDate,TrxTimeStamp,TrxType,                                 
 ChequeID,ChequeDate, Amount, ForeignAmount,ExchangeRate,DescriptionID,Description,AdditionalData,BankCode,BranchCode,                                 
 TrxPrinted,ProfitorLoss,Glid,Issupervision,Supervision,RemoteDescription,OperatorID,SupervisorID,Refno,IsLocalCurrency,                                 
 ChannelId, VirtualAccountID, VirtualAccountTitle, ChannelRefID, PrevDayEntry,ChargeID,VatID,[Percent],PrevYearEntry           
,TClientId,TAccountid,TProductId ,CostCenterID,GLVendorID,memo2,VendorID,CostTypeID,CustomerLifecycleID,ProjectId,PostExpenseID,VatReferenceID)                                 
 VALUES(@WHTScrollNo,1,@OurBranchID,@AccountID,@AccountName,@ProductID,@CurrencyID,@AccountType,@ValueDate,@mDate,@TrxTimeStamp,@TrxType,                                 
 'V',@ChequeDate, @WHTChargesAmount, 0.00, 1.00, 'WH1',@WHTDescription2,@AddData,@BankCode,@BranchCode,                                 
 @TrxPrinted,@ProfitorLoss,'', @IsSupervision, @Supervision, @RemoteDescription,@OperatorID,@SupervisorID,                                 
 'WHTax-'+                                 
cast(@WHTScrollNo as varchar(10)),@IsLocalCurrency, @ChannelId, @VirtualAccountID, @VirtualAccountTitle, @ChannelRefID, @PrevDayBalance,                                 
 @ChargeID,@VATId,@VATPercent,@PrevYearEntry,@TClientId,@TAccountId,@TProductId,@CostCenterID,@GLVendorID,@memo2,@VendorID,@CostTypeID,@CustomerLifecycleID,@ProjectId,@PostExpenseID,@VatReferenceID)                                 
                                 
                                 
 -- Update Account Balance for Customer only                                 
                                 
 EXEC @RetStatus = spc_CashUpdateBalance @OurBranchID, @AccountID, @WHTChargesAmount, 0, 1, @Supervision, 1, 0, 0, @PrevDayBalance                                 
 IF @Supervision = '*' OR @Supervision = 'P'                                 
 BEGIN                                 
 IF @Supervision = 'P'                                 
 SELECT @IsOverDraft = 1                                 
 ELSE                                 
                                 
 SELECT @IsOverDraft = 0                                 
                                 
 IF @TrxType = 'D'                                 
 BEGIN                                 
 INSERT INTO t_CustomerStatus                                 
 (ScrollNo,SerialNo,OurBranchID,AccountID,GLID,Amount,Rate,Status,AccountName,TransactionType,IsOverDraft,OperatorID)                                 
 VALUES (@WHTScrollNo,1,@OurBranchID,@AccountID,'',@WHTChargesAmount*-1,1,@Supervision,@AccountName,'T',@IsOverDraft,@OperatorID)                                 
                                 
 END                                 
 END                                 
                                 
 select @VoucherID = @WHTScrollNo -- ISNULL(MAX(VoucherID),0) + 1 from t_GLTransactions where OurBranchID = @OurBranchID                                 
                                 
                                 
 -- WHT Debit                                 
 if @Supervision = 'C'                                 
 begin                                 
                                 
 insert into t_Transactions (OurBranchID, ScrollNo, SerialNo, RefNo, wDate, TrxTimeStamp, AccountType, DocumentType, AccountID, AccountName,                                 
 ProductID, CurrencyID, ValueDate, TrxType, ChequeID, ChequeDate, Amount, ForeignAmount, ExchangeRate, ProfitLoss, MeanRate,                                 
                                 
 DescriptionID, [Description], BankCode, BranchCode, TrxPrinted, [Status], IsLocalCurrency, OperatorID, SupervisorID,                                 
 AdditionalData, IsMainTrx, DocType, GLVoucherID, ChannelId, VirtualAccountID, VirtualAccountTitle, ChannelRefID,PrevDayEntry,ChargeID,VatID,                                 
 [Percent],PrevYearEntry ,TClientId ,TAccountid,TProductId,CostCenterID,GLVendorID,memo2,VatReferenceID )                                 
                                 
 VALUES (@OurBranchID,@WHTScrollNo,1,'WHTax-'+cast(@WHTScrollNo as varchar(10)),                                 
@mDate,@TrxTimeStamp,@AccountType,'T',@AccountID,@AccountName,                                 
 @ProductID,@CurrencyID,@ValueDate,@TrxType,'V',@ChequeDate, @WHTChargesAmount, 0, 1,@ProfitorLoss, 1, 'WH1', @WHTDescription2,                                 
 @BankCode,@BranchCode, @TrxPrinted, @Supervision, @IsLocalCurrency, @OperatorID, @SupervisorID, @AddData,'1','T', @VoucherID,                                 
 @ChannelId, @VirtualAccountID, @VirtualAccountTitle, @ChannelRefID,@PrevDayBalance,@ChargeID,@VATId,@VATPercent,@PrevYearEntry                                 
 ,@TClientId,@TAccountId,@TProductId,@CostCenterID,@GLVendorID,@memo2,@VatReferenceID )                                 
                                 
 insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, TrxTimeStamp, DescriptionID, [Description],                                 
 CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,                                 
 Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType, ChannelId, VirtualAccountID, VirtualAccountTitle, ChannelRefID,PrevDayEntry                                 
 ,TClientId ,TAccountid                                 
 ,TProductId,CostCenterID ,VatID,[Percent],GLVendorID,memo2,VendorID,CostTypeID,CustomerLifecycleID ,ProjectId   ,PostExpenseID    ,VatReferenceID                    
 )                                 
                                 
 values (@OurBranchID, @GLControl, @VoucherID, '1', @mDate, @ValueDate, @TrxTimeStamp, @DescID, @Desc, @CurrencyID, @WHTChargesAmount, 0,                                 
 1, @IsCredit                                 
, 'Transfer', @TransactionMethod, @OperatorID, @SupervisorID, '', '0', '0', @AddData, '0', 'T', @ChannelId, @VirtualAccountID,                                 
 @VirtualAccountTitle, @ChannelRefID,@PrevDayBalance,@TClientId,@TAccountId,@TProductId,@CostCenterID,@VATId,@VatPercent,@GLVendorID,@memo2                          
 ,@VendorID,@CostTypeID,@CustomerLifecycleID,@ProjectId,@PostExpenseID,@VatReferenceID)                                 
                                 
                                 
 end                                 
 -- GL                                 
 set @WHTDescription2 = 'Charge ' + cast(@WHTChargeRate as varchar(100)) + '/- Withholding Tax On Withdrawals Transaction(s) '                                 
                                 
 set @WHTDescription2 = @WHTDescription2 + 'While A/C# ' + @AccountID + ' A/C Name: ' + @AccountName + ' ProductID: ' + @ProductID                                 
 set @WHTDescription2 = @WHTDescription2 + ' Cheque# ' + @ChequeID + ' And Amount: ' + cast(@Amount as varchar(50))                                 
                                 
 INSERT INTO t_TransferTransactionModel                                 
 (ScrollNo,SerialNo,OurBranchID,AccountID,AccountName,ProductID,CurrencyID,AccountType,ValueDate,wDate,TrxTimeStamp,TrxType,                                 
 ChequeID,ChequeDate, Amount, ForeignAmount,ExchangeRate,DescriptionID,Description,AdditionalData,BankCode,BranchCode,                                 
 TrxPrinted,ProfitorLoss,Glid,Issupervision,Supervision,RemoteDescription,OperatorID,SupervisorID,Refno,IsLocalCurrency,                                 
 ChannelId, VirtualAccountID, VirtualAccountTitle,ChannelRefID,PrevDayEntry,ChargeID,VatID,[Percent],PrevYearEntry                                 
,TClientId ,TAccountid                                 
 ,TProductId ,CostCenterID,GLVendorID,memo2,VendorID,CostTypeID,CustomerLifecycleID,ProjectId,PostExpenseID,VatReferenceID)                                 
                                 
 VALUES                                 
 (@WHTScrollNo,2,@OurBranchID,@WHTAccountID,@WHTDescription,'GL',@WHTCurrencyID,'G',@ValueDate,@mDate,@TrxTimeStamp,'C',                                 
 'V',@ChequeDate, @WHTChargesAmount, 0.00, 1.00, 'WH2',@WHTDescription2,@AddData,@BankCode,@BranchCode,                                 
 @TrxPrinted,@ProfitorLoss,'', 0, 'C', @RemoteDescription,@OperatorID,@SupervisorID,                                 
 'WHTax-'+cast(@WHTScrollNo as varchar(10)),@IsLocalCurrency, @ChannelId, @VirtualAccountID, @VirtualAccountTitle, @ChannelRefID,@PrevDayBalance,                     
 @ChargeID,@VATId,@VATPercent,@PrevYearEntry,@TClientId,@TAccountId,@TProductId,@CostCenterID,@GLVendorID,@memo2,@VendorID,@CostTypeID,@CustomerLifecycleID,@ProjectId,@PostExpenseID,@VatReferenceID)                                 
                                 
                                 
 -- WHT Credit                                 
                                 
 insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate,TrxTimeStamp, DescriptionID, [Description],                                 
                                 
 CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,                                 
 Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType, ChannelId, VirtualAccountID, VirtualAccountTitle, ChannelRefID,                                 
 PrevDayEntry                                 
,TClientId ,TAccountid                                 
 ,TProductId ,CostCenterID ,VatID,[Percent],GLVendorID,memo2,VendorID,CostTypeID,CustomerLifecycleID      ,ProjectId    ,PostExpenseID                   ,VatReferenceID  
 )                                 
                                 
 values (@OurBranchID, @WHTAccountID, @VoucherID, '2', @mDate, @ValueDate,@TrxTimeStamp, 'WH2', @WHTDescription2, @WHTCurrencyID,                                 
 @WHTChargesAmount, 0,                                 
                                 
 1, 1, 'Transfer', @TransactionMethod, @OperatorID, @SupervisorID, '', @WHTScrollNo, '2', @AddData, '1', 'T',                                 
 @ChannelId, @VirtualAccountID, @VirtualAccountTitle, @ChannelRefID,@PrevDayBalance,@TClientId,@TAccountId,@TProductId,@CostCenterID,@VATId,@VatPercent,@GLVendorID,@memo2                          
 ,@VendorID,@CostTypeID,@CustomerLifecycleID,@ProjectId,@PostExpenseID,@VatReferenceID)                                 
                                 
 end                                 
                                 
 IF @ValueDate > @wDate and @TrxType = 'C'                                 
 BEGIN                                 
 SET @GoInEffects = 1                                 
                                 
 INSERT INTO t_Clearing                                 
 (OurBranchID, ScrollNo,SerialNo,ValueDate,ChequeID, ChequeDate, AccountID, BankID, Amount, ForeignAmount,TrxID, IsLocalCurrency,                                 
 OperatorID, SupervisorID)                                 
                                 
 VALUES                                 
 (@OurBranchID, @ScrollNo,@SerialID,@ValueDate,@ChequeID, @ChequeDate, @AccountID, @BankCode, @Amount,@ForeignAmount,@TrxType,                                 
 @IsLocalCurrency,@OperatorID,@SupervisorID)                                 
 END                              
                                 
 ELSE                                 
 SET @GoInEffects = 0                                 
                                 
                                 
 IF @TrxType = 'D' AND @ChequeID <> 'V' AND @ChequeID <> ''                                 
                                 
BEGIN                                 
                                 
 -- Get Validate Cheaque No                                 
 EXEC @RetStatus = spc_ValidateChequeNo @OurBranchID, @AccountID, @ChequeID, @ChequeID                                 
                                 
                                 
 IF @RetStatus <> 0                                 
 BEGIN                                 
 SELECT @RetStatus as Status,'C' as Supervision                                 
 RETURN(1)                                 
 END                                 
                                 
                                 
 INSERT INTO t_ChequePaid                                 
 (OurBranchID, AccountID,ChequeID,Date,TrxTimeStamp,AccountType,CreateBy,CreateTime,CreateTerminal,SuperviseBy)                                 
 VALUES                                 
                   
 (@OurBranchID,@AccountID, @ChequeID,@ValueDate,@AccountType,@OperatorID,@mDate,@TrxTimeStamp,'',@SupervisorID)                                 
 END                                 
                                 
--select @Supervision                                 
                        
--return                                 
                                 
 declare @TrxRestriction char(1),                                 
 @CrDailyCount decimal(24,0), @DrDailyCount decimal(24,0), @CrDailyThreshold decimal(21,3), @DrDailyThreshold decimal(21,3),                                 
                                 
 @CrMonthlyCount decimal(24,0), @DrMonthlyCount decimal(24,0), @CrMonthlyThreshold decimal(21,3), @DrMonthlyThreshold decimal(21,3),                                 
 @cCrDailyCount decimal(24,0), @cDrDailyCount decimal(24,0), @cCrDailyThreshold decimal(21,3), @cDrDailyThreshold decimal(21,3),              
 @cCrMonthlyCount decimal(24,0), @cDrMonthlyCount decimal(24,0), @cCrMonthlyThreshold decimal(21,3), @cDrMonthlyThreshold decimal(21,3)                                 
                                 
                                
 select @TrxRestriction = ISNULL(TrxRestrictions, 'N'),                                 
 @CrDailyCount = ISNULL(NoOfCrTrx, 0), @DrDailyCount = ISNULL(NoOfDrTrx, 0), @CrDailyThreshold = ISNULL(AmountOfCrTrx, 0),                                 
 @DrDailyThreshold = ISNULL(AmountOfDrTrx, 0),                                 
 @CrMonthlyCount = ISNULL(NoOfCrTrxMonthly, 0), @DrMonthlyCount = ISNULL(NoOfDrTrxMonthly, 0),                                 
 @CrMonthlyThreshold = ISNULL(AmountOfCrTrxMonthly, 0), @DrMonthlyThreshold = ISNULL(AmountOfDrTrxMonthly, 0)                                 
 from t_Products where OurBranchID = @OurBranchID and ProductID = @ProductID                                 
                                 
 IF @TrxRestriction = 'Y'                                 
 BEGIN                                 
                                 
 IF @Supervision <> '*' and @Supervision <> 'P'                                 
 BEGIN                                 
                                 
 select           
 @cCrDailyCount = NoOfCrTrx, @cDrDailyCount = NoOfDrTrx, @cCrDailyThreshold = AmountOfCrTrx, @cDrDailyThreshold = AmountOfDrTrx,                                 
 @cCrMonthlyCount = NoOfCrTrxMonthly, @cDrMonthlyCount = NoOfDrTrxMonthly, @cCrMonthlyThreshold = AmountOfCrTrxMonthly,                                 
 @cDrMonthlyThreshold = AmountOfDrTrxMonthly                                 
                                 
 from t_AccountBalance                                 
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                                 
                                 
 if @TrxType = 'C'                                 
 BEGIN                                 
                                 
                                 
 if (@cCrDailyCount + 1) > @CrDailyCount                                 
 begin                                 
 SELECT 31 as [Status], 'V' as Supervision                                 
 RETURN(1)                                 
                                 
 end                                 
 if (@cCrDailyThreshold + @Amount) > @CrDailyThreshold                                 
 begin                                 
 SELECT 32 as [Status], 'V' as Supervision                                 
 RETURN(1)                           
                                 
 end                                 
 if (@cCrMonthlyCount + 1) > @CrMonthlyCount                                 
begin                                 
 SELECT 33 as [Status], 'V' as Supervision                                 
 RETURN(1)                                 
                                 
 end                                 
 if (@cCrMonthlyThreshold + @Amount) > @CrMonthlyThreshold              begin                                 
 SELECT 34 as [Status], 'V' as Supervision                                 
 RETURN(1)                                 
                                 
 end                                 
                                 
 END                                 
 ELSE                                 
 BEGIN                                 
                                 
 if (@cDrDailyCount + 1) > @DrDailyCount                                 
 begin                                 
 SELECT 35 as [Status], 'V' as Supervision                                 
 RETURN(1)                                 
 end                                 
 if (@cDrDailyThreshold + @Amount) > @DrDailyThreshold                                 
                                 
 begin                                 
 SELECT 36 as [Status], 'V' as Supervision                                 
 RETURN(1)                                 
 end          
 if (@cDrMonthlyCount + 1) > @DrMonthlyCount                                 
                                 
 begin                                 
 SELECT 37 as [Status], 'V' as Supervision                                 
 RETURN(1)                                 
 end                                 
 if (@cDrMonthlyThreshold + @Amount) > @DrMonthlyThreshold                                 
                                 
 begin                                 
 SELECT 38 as [Status], 'V' as Supervision                                 
 RETURN(1)                                 
 end                                 
                                 
 END                                 
                                 
 END                                 
 END                                 
                                 
 IF @Supervision <> '*' and @Supervision <> 'P'                                 
 BEGIN                                 
 if @TrxType = 'C'                                 
                                 
 BEGIN                                 
 UPDATE t_AccountBalance SET                                 
 NoOfCrTrx = NoOfCrTrx+1, AmountOfCrTrx = AmountOfCrTrx+@Amount,                                 
 NoOfCrTrxMonthly = NoOfCrTrxMonthly+1, AmountOfCrTrxMonthly = AmountOfCrTrxMonthly+@Amount                                 
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                                 
 END                                 
 ELSE                                 
 BEGIN                                 
                                 
 UPDATE t_AccountBalance SET                                 
 NoOfDrTrx = NoOfDrTrx+1, AmountOfDrTrx = AmountOfDrTrx+@Amount,                           
 NoOfDrTrxMonthly = NoOfDrTrxMonthly+1, AmountOfDrTrxMonthly = AmountOfDrTrxMonthly+@Amount                                 
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                                 
 END                                 
 END                                 
                                 
                                 
 IF @Supervision = '*' OR @Supervision = 'P'                                 
 BEGIN                                 
 IF @Supervision = 'P'                                 
 SELECT @IsOverDraft = 1                                 
 ELSE                       
SELECT @IsOverDraft = 0                                 
                                 
 IF @TrxType = 'D'                                 
 BEGIN                                 
 IF @AccountType = 'G'                                 
 BEGIN                                 
 INSERT INTO t_CustomerStatus                                 
 (ScrollNo,SerialNo,OurBranchID,AccountID,GLID,Amount,Rate,Status,AccountName,TransactionType,IsOverDraft,OperatorID)                                 
 VALUES                 
(@ScrollNo,@SerialID,@OurBranchID,@AccountID,@GLID,@Amt*-1,@ExchangeRate,@Supervision,@AccountName,'TG',@IsOverDraft,@OperatorID)                                 
 END                                 
 ELSE                                 
 BEGIN                                 
 INSERT INTO                                 
t_CustomerStatus                                 
 (ScrollNo,SerialNo,OurBranchID,AccountID,GLID,Amount,Rate,Status,AccountName,TransactionType,IsOverDraft,OperatorID)                               
 VALUES                                 
 (@ScrollNo,@SerialID,@OurBranchID,@AccountID,@GLID,@Amt*-1,@ExchangeRate,@Supervision,@AccountName,'T',@IsOverDraft,@OperatorID)                                 
 END                                 
 END                                 
 ELSE                                 
 BEGIN                                 
                                 
 IF @AccountType = 'G'                         
 BEGIN                                 
 INSERT INTO t_CustomerStatus                                 
 (ScrollNo,SerialNo,OurBranchID,AccountID,GLID,Amount,Rate,Status,AccountName,TransactionType,IsOverDraft,OperatorID)                                 
 VALUES                                 
 (@ScrollNo,@SerialID,@OurBranchID,@AccountID,@GLID,@Amt,@ExchangeRate,@Supervision,@AccountName,'TG',@IsOverDraft,@OperatorID)                                 
 END                                 
                                 
 ELSE                                 
 BEGIN                                 
 INSERT INTO t_CustomerStatus                                 
 (ScrollNo,SerialNo,OurBranchID,AccountID,GLID,Amount,Rate,Status,AccountName,TransactionType,IsOverDraft,OperatorID)                                 
 VALUES                                 
 (@ScrollNo,@SerialID,@OurBranchID,@AccountID,@GLID,@Amt,@ExchangeRate,@Supervision,@AccountName,'T',@IsOverDraft,@OperatorID)                                 
 END                                 
                                 
 END                                 
 END                                 
                                 
                                 
-- IF @Supervision <> 'P' AND @AccountType = 'C'                                 
 IF @AccountType = 'C'                            
                                 
 BEGIN                                 
                                 
 EXEC @RetStatus = spc_CashUpdateBalance @OurBranchID, @AccountID, @Amount, @ForeignAmount, @IsDebit,@Supervision,                                 
 @IsLocalCurrency,@GoInEffects,0, @PrevDayBalance                                 
                                 
                   
                
 declare @SMSText nvarchar(max)                                 
 declare @SMSToSend nvarchar(max)                                 
 declare @BalanceAmount money                                 
 declare @BeneficiaryName varchar(50)                                 
 declare @CcyRounding int                                 
 declare @DelimeterPosition int                                 
 declare @ProductMinBalance decimal(18,5)                
 declare @TemplateID nvarchar(50)               
 declare @IsNot nvarchar(50)               
 --SMS              
 if exists(select DescriptionID from t_TransactionDescriptions where OurBranchID = @OurBranchID and DescriptionID = @DescriptionID and IsSms = 1)                   
              
 begin                                 
                                 
 select @SMSText = SmsText,@DelimeterPosition = isnull(DelimiterPosition,1) from t_TransactionDescriptions                                 
                                 
 where OurBranchID = @OurBranchID and DescriptionID = @DescriptionID and IsSms = 1                                 
 select @ProductMinBalance = MinBalance from t_Products where ProductID = @ProductID and OurBranchID = @OurBranchID                                 
 select @BalanceAmount = (ClearBalance + Limit - FreezeAmount - @ProductMinBalance) from t_AccountBalance                                 
 where AccountID = @AccountID                                 
 declare @txtAmount nvarchar(30) = ''                                 
 declare @txtBalanceAmount nvarchar(30) = ''                                 
                                 
 if (@CurrencyID = 'AED')                                 
 begin                                 
 select @txtAmount = substring(Format(@Amount, 'C'), 2, LEN(Format(@Amount, 'C')))                                 
 select @txtBalanceAmount = substring(Format(@BalanceAmount, 'C'), 2, LEN(Format(@BalanceAmount, 'C')))                                 
 end                                 
 else                                 
 begin                                 
 select @CcyRounding = ISNULL(CRRounding,2) from t_Currencies where OurBranchID = @OurBranchID and                                 
CurrencyID = @CurrencyID;                                 
 if @CcyRounding = 0                                 
 begin                                 
 select @txtAmount = substring(Format(@Amount, 'C0'), 2, LEN(Format(@Amount, 'C0')))                                 
 select @txtBalanceAmount =                                 
substring(Format(@BalanceAmount, 'C0'), 2, LEN(Format(@BalanceAmount, 'C0')))                                 
 end                                 
 else if @CcyRounding = 3                                 
 begin                                 
 select @txtAmount = substring(Format(@Amount, 'C3')                                 
, 2, LEN(Format(@Amount, 'C3')))                                 
 select @txtBalanceAmount = substring(Format(@BalanceAmount, 'C3'), 2, LEN(Format(@BalanceAmount, 'C3')))                                 
 end                                 
 else                                 
 begin                                 
                                 
 select @txtAmount = substring(Format(@Amount, 'C'), 2, LEN(Format(@Amount, 'C')))                                 
 select @txtBalanceAmount = substring(Format(@BalanceAmount, 'C'), 2, LEN(Format(@BalanceAmount, 'C')))                                 
 end                                 
 end                                 
                                 
 select top 1 @BeneficiaryName= Value from STRING_SPLIT(@CustomDescription,'~')                                 
WHERE value NOT IN (select top (@DelimeterPosition) Value from STRING_SPLIT(@CustomDescription,'~') )                                 
                                 
                                 
 SELECT @SMSToSend = REPLACE(@SMSText, '{{OurBranchID}}', isnull(@OurBranchID,''))                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{AccountID}}', isnull(@AccountID,''))                                
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{TrType}}', isnull(@TrxType,''))                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{TrxType}}', isnull(@TrxType,''))                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{wDate}}', isnull(@wDate,''))                                 
                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{AccountType}}', isnull(@AccountType,''))                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{AccountName}}', isnull(@AccountName,''))                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{ProductID}}', isnull(@ProductID,''))                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{CurrencyID}}', isnull(@CurrencyID,''))                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{ChequeID}}', isnull(@ChequeID,''))                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{ChequeDate}}', isnull(@ChequeDate,''))                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{Amount}}', @txtAmount)                                 
                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{ForeignAmount}}', isnull(@ForeignAmount,''))                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{ExchangeRate}}', isnull(@ExchangeRate,''))                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{DescriptionID}}', isnull(@DescriptionID,''))                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{Description}}', isnull(@Description,''))                             
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{ChannelRefID}}', isnull(@ChannelRefID,''))                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{TrxTimeStamp}}', isnull(@TrxTimeStamp,''))                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{BalanceAmount}}', isnull(@txtBalanceAmount,''))                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{SenderBeneName}}', isnull(@BeneficiaryName,''))                                 
                                 
 insert into t_SMSTable ([OurBranchID],[AccountID],[TrxType],[wDate],[AccountType],[AccountName],[ProductID],[CurrencyID],[ChequeID],[ChequeDate],                                 
 [Amount],[ForeignAmount],[ExchangeRate],[DescriptionID],[Description],[ChannelRefID],[TrxTimeStamp],[SMSText],[SMSToSend],[IsSend])                                 
 values                                 
( @OurBranchID, @AccountID, @TrxType, @wDate, @AccountType, @AccountName, @ProductID, @CurrencyID, @ChequeID, @ChequeDate,                                 
 @Amount, @ForeignAmount, @ExchangeRate, @DescriptionID, @Description, @ChannelRefID, @TrxTimeStamp, @SMSText, @SMSToSend, 0)                                 
                                 
 end                                 
 if exists(select DescriptionID from t_TransactionDescriptions where OurBranchID = @OurBranchID and DescriptionID = @DescriptionID and IsEmail = 1)                                 
    --Email                           
 begin                                 
                                 
 select @SMSText = EmailText from t_TransactionDescriptions where OurBranchID = @OurBranchID and DescriptionID = @DescriptionID and IsEmail = 1                                 
                                 
                                 
 SELECT @SMSToSend = REPLACE(isnull(@SMSText,''), '{{OurBranchID}}', isnull(@OurBranchID,''))                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{AccountID}}', isnull(@AccountID,''))                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{TrType}}', isnull(@TrxType,''))                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{TrxType}}', isnull(@TrxType,''))                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{wDate}}', isnull(@wDate,''))                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{AccountType}}', isnull(@AccountType,''))                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{AccountName}}', isnull(@AccountName,''))                                 
                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{ProductID}}', isnull(@ProductID,''))                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{CurrencyID}}', isnull(@CurrencyID,''))                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{ChequeID}}', isnull(@ChequeID,''))                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{ChequeDate}}', isnull(@ChequeDate,''))                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{Amount}}', isnull(@txtAmount,'')                                 
)                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{ForeignAmount}}', isnull(@ForeignAmount,''))                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{ExchangeRate}}', isnull(@ExchangeRate,''))                                 
                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{DescriptionID}}', isnull(@DescriptionID,''))                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{Description}}', isnull(@Description,''))                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{ChannelRefID}}', isnull(@ChannelRefID,''))                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{TrxTimeStamp}}', isnull(@TrxTimeStamp,''))                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{BalanceAmount}}', isnull(@txtBalanceAmount,''))                                 
 SELECT @SMSToSend = REPLACE(@SMSToSend, '{{SenderBeneName}}', isnull(@BeneficiaryName,''))                                 
                                 
                                 
 end                                 
                  
          
 if exists(select DescriptionID from t_TransactionDescriptions where OurBranchID = @OurBranchID and DescriptionID = @DescriptionID and IsNotification = 1)                                 
--Notification                                 
 begin                     
 --declare @ProductMinBalance decimal(18,5)               
  declare               
  @Balance money ,              
  @Phone nvarchar(60),              
  @Email nvarchar(60)                               
 --select @SMSText = EmailText from t_TransactionDescriptions where OurBranchID = @OurBranchID and DescriptionID = @DescriptionID and IsEmail = 1                                 
       --@OurBranchID                          
              
 select @TemplateID=TemplateID,@IsNot= isnull(IsNotification,0) from t_TransactionDescriptions where OurBranchID = @OurBranchID               
 and DescriptionID = @DescriptionID               
 select @Balance=ClearBalance from t_AccountBalance where AccountID=@AccountID and OurBranchID=@OurBranchID              
 select @Phone=MobileNo,@Email=email from t_Customer where ClientID=@TClientId and OurBranchID=@OurBranchID              
               
 insert into t_notification (AccountId,TrxType,wDate,AccountType,AccountName,ProductID,CurrencyID,Amount,ClientId,Email,                                 
 PhoneNo,AccountBalance,TemplateId,TrxId,OurBranchId,countryISOCode,IsSend,ChannelRefId,DescriptionID,[Description],ForeignAmount,ExchangeRate,SenderBeneName)                                 
               
              
 values                               
(@AccountID , @TrxType, @wDate, @AccountType, @AccountName, @ProductID, @CurrencyID, @Amount, @TClientId, @Email,                                 
 @Phone, @Balance, @TemplateID, CONCAT(@ChannelRefID,'-', @SerialID), @OurBranchID, '784', '0', @ChannelRefID, @DescriptionID,@Description, @ForeignAmount              
 ,@ExchangeRate,isnull(@BeneficiaryName,'')              
 )                                   
                                
                                 
                                 
end                  
              
              
              
    
              
              
              
                               
                   
 IF @RetStatus <> 0                                 
 BEGIN                                 
 SELECT @RetStatus as Status,'B' as Supervision                                 
 RETURN(1)                                 
                                 
 END                                 
 END                
               
              
              
              
                                 
 SELECT 0 as Status,@Supervision as Supervision                                 
 RETURN(0)                                 
 END