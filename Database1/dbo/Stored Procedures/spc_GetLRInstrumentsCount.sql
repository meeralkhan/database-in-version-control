﻿CREATE procedure [dbo].[spc_GetLRInstrumentsCount] (
 @OurBranchID nvarchar(30),        
 @InstrumentType nvarchar(50),        
 @InstrumentNo decimal(24,0)=0,      
 @ControlNo decimal(24,0)=0      
)        
As        
BEGIN        
        
SET NOCOUNT ON        
        
  if @InstrumentType = 'PO' OR @InstrumentType = 'BC' OR @InstrumentType = 'PS' OR @InstrumentType = 'DD'      
  BEGIN      
  SELECT Count(*) AS Count1 FROM t_LR_Issuance Where OurBranchID = @OurBranchID and       
  InstrumentType = @InstrumentType and InstrumentNo = @InstrumentNo        
          
  Union All         
          
  SELECT Count(*) AS Count1 FROM t_LR_Erase Where OurBranchID = @OurBranchID and         
  InstrumentType = @InstrumentType and InstrumentNo = @InstrumentNo         
          
  Union All         
          
  SELECT Count(*) AS Count1 FROM t_LR_Lost Where OurBranchID = @OurBranchID and         
  InstrumentType = @InstrumentType and InstrumentNo = @InstrumentNo         
 END      
 else if @InstrumentType = 'TT'      
 BEGIN      
  SELECT Count(*) AS Count1 FROM t_LR_Issuance Where OurBranchID = @OurBranchID and       
  InstrumentType = @InstrumentType and ControlNo = @ControlNo      
          
  Union All         
          
  SELECT Count(*) AS Count1 FROM t_LR_Erase Where OurBranchID = @OurBranchID and         
  InstrumentType = @InstrumentType and ControlNo = @ControlNo      
          
  Union All         
          
  SELECT Count(*) AS Count1 FROM t_LR_Lost Where OurBranchID = @OurBranchID and         
  InstrumentType = @InstrumentType and ControlNo = @ControlNo      
 END      
END