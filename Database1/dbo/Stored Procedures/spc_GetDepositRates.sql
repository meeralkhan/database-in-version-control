﻿
CREATE PROCEDURE [dbo].[spc_GetDepositRates]
(
  @OurBranchID varchar(30),
  @ProductID   varchar(30),
  @Date        datetime,
  @Amount      money
)

AS

SET NOCOUNT ON

BEGIN
  
  select * from t_DepositRates
  where OurBranchID = @OurBranchID and ProductID = @ProductID and EffectiveDate >= @Date
  and @Amount between Minamount and MaxAmount order by EffectiveDate desc
  
END