﻿create procedure [dbo].[spc_GetCharity]    
(    
   @OurBranchID nvarchar(30),    
   @AccountID nvarchar(30)    
)    
AS    
BEGIN    
       
   declare @wDate datetime    
   declare @MonthCharity money    
   declare @TotalCharity money    
    
   select @wDate = WorkingDate from t_last where OurBranchID = @OurBranchID    
    
   select @MonthCharity = SUM(Amount) from t_Transactions    
   where OurBranchID = @OurBranchID    
   and AccountID = @AccountID    
   and TrxType = 'D'    
   and DescriptionID IN ('CH0','200190')    
   and Year(wDate) = Year(@wDate)    
   and Month(wDate) = Month(@wDate)    
    
   select @TotalCharity = SUM(Amount) from t_Transactions    
   where OurBranchID = @OurBranchID    
   and AccountID = @AccountID    
   and TrxType = 'D'    
   and DescriptionID IN ('CH0','200190')    
    
   select @AccountID AS AccountID, ISNULL(@MonthCharity,0) AS MonthCharity, ISNULL(@TotalCharity,0) AS TotalCharity    
    
END