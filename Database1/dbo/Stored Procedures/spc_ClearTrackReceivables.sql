﻿create   procedure [dbo].[spc_ClearTrackReceivables]
(
   @OurBranchID nvarchar(30),
   @AccountID nvarchar(30),
   @DealID nvarchar(30)
)
AS
BEGIN
   
   declare @wDate datetime;
   select @wDate = WorkingDate + convert(varchar(12), GETDATE(), 114) from t_Last where OurBranchID = @OurBranchID;
   
   declare @intAccID nvarchar(100) = '', @trxAccID nvarchar(100) = '', @accID nvarchar(100) = '';
   select @intAccID = interestAccountID, @trxAccID = TrxAccID from t_Disbursement
   where OurBranchID = @OurBranchID and AccountID = @AccountID and DealID = @DealID

   declare @tmpTable table
   (
      SNo int IDENTITY(1,1) NOT NULL PRIMARY KEY,
      HoldRefNo nvarchar(100),
      IsFreeze int,
      Amount money,
      [Type] nvarchar(30),
	  InstNo int,
	  wDate datetime
   );

   insert into @tmpTable
   select HoldRefNo, IsFreeze, Amount, Type, InstNo, wDate from t_TrackReceivables where OurBranchID = @OurBranchID and AccountID = @AccountID and DealID = @DealID and IsFreeze = 1 order by [Type] desc, wDate

   declare @freezeAmt money = 0;
   declare @tRows int = 0, @rowNo int = 0, @refNo nvarchar(100) = '', @type nvarchar(1) = '';
   select @tRows = COUNT(SNo) from @tmpTable

   WHILE @tRows > 0
   BEGIN
      select top 1 @rowNo = SNo, @refNo = HoldRefNo, @type = [Type] from @tmpTable where IsFreeze = 1;
	  update @tmpTable set IsFreeze = 0 where SNo = @rowNo;
	  select @tRows = COUNT(SNo) from @tmpTable where IsFreeze = 1;

	  set @accID = @intAccID
	  if (@type = 'F')
	  begin
	     set @accID = @trxAccID
	  end

	  update t_TrackReceivables set UpdateBy = 'ES-API', UpdateTime = GETDATE(), UpdateTerminal = '',
	  IsFreeze = 0, ReleaseDate = @wDate, ReleaseComments = 'Clearing Track receivable on Full Early settlement.'
	  where OurBranchID = @OurBranchID and AccountID = @AccountID and DealID = @DealID and HoldRefNo = @refNo;

	  set @freezeAmt = 0;
	  select @freezeAmt = Amount from t_AccountFreeze where OurBranchID = @OurBranchID and AccountID = @accID and ATMTrxHoldRef = @refNo;

	  update t_AccountFreeze set UpdateBy = 'ES-API', UpdateTime = GETDATE(), UpdateTerminal = '',
	  IsFreeze = 0, UnFreezedDate = @wDate, ReleaseComments = 'Clearing Track receivable on Full Early settlement.'
	  where OurBranchID = @OurBranchID and AccountID = @accID and ATMTrxHoldRef = @refNo;
	  
	  update t_AccountBalance set IsFreezed = case when (FreezeAmount-@freezeAmt <= 0) then 0 else 1 end,
	  FreezeAmount = case when (FreezeAmount-@freezeAmt <= 0) then 0 else FreezeAmount-@freezeAmt end
	  where OurBranchID = @OurBranchID and AccountID = @accID

   END

   SELECT 'Ok' As RetStatus;
END