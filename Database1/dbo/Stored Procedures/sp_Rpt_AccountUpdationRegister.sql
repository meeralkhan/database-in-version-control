﻿CREATE Procedure [dbo].[sp_Rpt_AccountUpdationRegister]    
@OurBranchID Varchar(5),    
@FromDate Date,    
@ToDate Date    
    
AS  
Set nocount on  
 Declare @cols Varchar(MAX);  
 --,@OurBranchID Varchar(5);  
 Declare @sql Varchar(MAX),@Insertsql Varchar(MAX), @sql2 Varchar(MAX),@UpdateCols VARCHAR(MAX);     
 --set @OurBranchID = '03'  
     
 IF OBJECT_ID('tempdb..#AccountHistoryTemp') IS NOT NULL    
  DROP TABLE #AccountHistoryTemp    
    
 Create Table #AccountHistoryTemp (CreateBy nvarchar(Max) null, CreateTime nvarchar(Max) null)    
    
 Select @cols = COALESCE(@cols + ',  ' + name+ ' nvarchar(Max) null', name+ ' nvarchar(Max) null')     
        From sys.columns where object_id in (Select object_id from sys.tables where name='t_Account_History') And name not in ('CreateBy','CreateTime')    
    
 Select @sql=N'Alter Table #AccountHistoryTemp ADD '+@cols+', SNo int; ';    
    
 Select @Insertsql=N'Insert into  #AccountHistoryTemp Select * From (Select *,ROW_NUMBER() OVER (PARTITION BY AccountID ORDER BY wDate DESC) AS SNo '    
 Select @Insertsql=@Insertsql+'from t_Account_History where UpdateTime is not null And OurBranchID='''+@OurBranchID+''') as t Where t.SNo<=2 ;'    
  print @Insertsql  
 Select @UpdateCols = 'Update #AccountHistoryTemp Set '+STUFF((SELECT  ', ' +   QUOTENAME(Substring(FieldId,4,100))+'=IsNull('+QUOTENAME(Substring(FieldId,4,100))+', '''')'                         
 FROM tbl_Form_Fields where FrmName ='frmAccountMaintenance' And  IsDisplayField=0 And Substring(FieldId,4,100) Not In ('AccountID','Name','SignatureInstructions','OtherInstructions') Order by OrderNo                                        
  FOR XML PATH(''), TYPE).value('.', 'VARCHAR(MAX)') ,1,1,'')    
      
 --print @sql    
 exec(@sql)    
 exec(@Insertsql)    
 exec(@UpdateCols)    
 --exec(@sql2)    
    
 Select Distinct  OV.AccountID,OV.Name,OV.ColName,OldValue,NewValue,OV.CreateBy,OV.SuperviseBy,ov.wdate    
 From (    
 Select *    
 from dbo.#AccountHistoryTemp Where SNo=2) as OV     
 UNPIVOT    
 (OldValue For ColName IN ([ModeOfOperation],[Address],[EmpID],[CountryID],[Reminder],[DebitCardNo],[StateBankCode],[StateID],[NameKin],[IntroducerAccountNo],    
 [AccountType],[CityID],[Code],[NatureID],[FNameKin],[IntroducedBy],[IDType],[IDNoKin],[GeoCode],[HoldMail],[ZakatExemption],[LifeInsurance],[TaxFiler],[ApplyTax],    
 [AESCode],[GuardianName],[RelationshipCode],[GuardianCNIC],[IntroducerAddress],[AddressKin],[AddressKinCountryID],[StatementFrequency],[AddressKinStateID],    
 [IntroducerCountryID],[AddressKinCityID],[ContactPersonName],[CPRelationShip],[Designation],[IntroducerStateID],[PhNoResKin],[IntroducerCityID],[PhNoOFFKin],    
 [Notes],[Phone1],[Phone2],[MobNoKin],[MobileNo],[Fax],[FAXKin],[EmailID],[EmailKin],[RelationShipKin],[IsWHTax],[AllowCreditTransaction],    
 [AllowDebitTransaction],[CloseRemarks],[CreditNeedsSupervision],[DebitNeedsSupervision],[NotServiceCharges],[NotStopPaymentCharges],[NotChequeBookCharges],[ClearingTransactionDays],[PurposeOfAccount],[PurposeOfAccountOthers],[TurnOver],[tTurnOver],  
 [NoOfDrTrx],[NoOfCrTrx],[DrThresholdLimit],[CrThresholdLimit],[ProductCash],[ProductClearing],[ProductCollection],[ProductRemittance],[ProductCross],[ProductOthers],[ProductOthersDesc],[PurposeOfIntFT])) as OV    
 Inner Join     
 (    
 Select *    
 from dbo.#AccountHistoryTemp Where SNo=1) as NV     
 UNPIVOT    
 (NewValue For ColName IN ([ModeOfOperation],[Address],[EmpID],[CountryID],[Reminder],[DebitCardNo],[StateBankCode],[StateID],[NameKin],[IntroducerAccountNo],[AccountType],[CityID],[Code],[NatureID],[FNameKin],[IntroducedBy],    
 [IDType],[IDNoKin],[GeoCode],[HoldMail],[ZakatExemption],[LifeInsurance],[TaxFiler],[ApplyTax],[AESCode],[GuardianName],[RelationshipCode],[GuardianCNIC],[IntroducerAddress],[AddressKin],[AddressKinCountryID],[StatementFrequency],    
 [AddressKinStateID],[IntroducerCountryID],[AddressKinCityID],[ContactPersonName],[CPRelationShip],[Designation],[IntroducerStateID],[PhNoResKin],[IntroducerCityID],[PhNoOFFKin],[Notes],[Phone1],[Phone2],[MobNoKin],[MobileNo],    
 [Fax],[FAXKin],[EmailID],[EmailKin],[RelationShipKin],[IsWHTax],[AllowCreditTransaction],[AllowDebitTransaction],[CloseRemarks],[CreditNeedsSupervision],[DebitNeedsSupervision],[NotServiceCharges],[NotStopPaymentCharges],    
 [NotChequeBookCharges],[ClearingTransactionDays],[PurposeOfAccount],[PurposeOfAccountOthers],[TurnOver],[tTurnOver],[NoOfDrTrx],[NoOfCrTrx],[DrThresholdLimit],[CrThresholdLimit],[ProductCash],[ProductClearing],[ProductCollection],    
 [ProductRemittance],[ProductCross],[ProductOthers],[ProductOthersDesc],[PurposeOfIntFT])) as NV On OV.ColName=NV.ColName And OV.AccountID=NV.AccountID    
 Where  IsNull(OV.OldValue,'') <>IsNull(NV.NewValue,'')     
 Order By OV.AccountID