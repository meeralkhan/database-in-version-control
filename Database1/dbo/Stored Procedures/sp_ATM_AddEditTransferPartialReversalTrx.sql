﻿CREATE PROCEDURE [dbo].[sp_ATM_AddEditTransferPartialReversalTrx]  
(  
 @OurBranchID       varchar(30),     
 @OrgTrxRefNo       varchar(100)='',    
 @AmtActualTran  money=0,    
 @AmtActualStl  money=0,    
 @ActualTranFee  money=0,    
 @AccountStlFee  money=0,    
 @ForwardInstID     varchar(10),    
 @ResponseCode  varchar(2)='',  
 @VISATrxID varchar(15)='',
 @ConvRate  decimal(15,9)=0,
 @AuthIDResp varchar(6)='',
 @NewRecord   bit=1    
)  
 AS  
 SET NOCOUNT ON  
  
 DECLARE @Status            as int  
 DECLARE @AccountID         as varchar (30)  
 DECLARE @Amount            as money 
 DECLARE @HoldAmount            as money 
 DECLARE @mRefNo 			as varchar(100)
 DECLARE @Description       as varchar (255)   
 DECLARE @mDescription       as varchar (255)   
 DECLARE @mwDate            as datetime  
 DECLARE @mLastEOD          as datetime  
 DECLARE @mCONFRETI         as datetime  
 DECLARE @vIsClose          as char(1)  
 DECLARE @vClearBalance as money        
 DECLARE @AvailableBalance as money        
 DECLARE @Value as varchar(2)='00'  
 DECLARE @vClearedEffects as money        
 DECLARE @vFreezeAmount as money        
 DECLARE @vMinBalance as money        
 DECLARE @Limit as money        
 DECLARE @CurrencyCode as varchar(30)  
 DECLARE @vCount1 as int  
 DECLARE @HoldStatus as char(1)  
 DECLARE @vCheckLimit as bit  
  SET @vCheckLimit = 0  
 DECLARE @vODLimitAllow as bit  
  SET @vODLimitAllow = 0  
 DECLARE @AcqInstID as varchar(15)  
 DECLARE @RemainingAmount numeric(18,6)  
 DECLARE @FreezeAmount money  
 DECLARE @cCount as int    
  SET @cCount = 0    
 DECLARE @sCount as int    
  SET @sCount = 0  
 DECLARE @STAN as varchar(30) 
 DECLARE @PHXDate as datetime 
 DECLARE @ForeignAmount as numeric(18,6)           
  SET @ForeignAmount=0        
 DECLARE @ExchangeRate as numeric(18,6)        
  SET @ExchangeRate=1        
 DECLARE @MerchantType as varchar(30)
 DECLARE @AcqCountryCode as varchar(3)
 DECLARE @ATMID as varchar(30)
 DECLARE @CurrCodeTran as varchar(3)
 DECLARE @CurrCodeSett as varchar(3)
 DECLARE @SettlmntAmount as numeric(18,6)
 DECLARE @ProcCode as varchar(50)
 DECLARE @POSEntryMode as varchar(3)
 DECLARE @POSConditionCode as varchar(2)
 DECLARE @POSPINCaptCode as varchar(2)
 DECLARE @AckInstIDCode as varchar(15)
 DECLARE @RetRefNum as varchar(15)
 DECLARE @CardAccptID as varchar(15)
 DECLARE @NameLocation as varchar(50)
 DECLARE @CAVV as char(1)
 DECLARE @DescriptionID as varchar(30)
 DECLARE @MobileNo nvarchar(30)
 DECLARE @Email nvarchar(100)
 DECLARE @rCurrencyID nvarchar(30)
 DECLARE @ClientID nvarchar(30)
 DECLARE @NarrationID nvarchar(30)
  SET @MobileNo = ''
  SET @Email = ''
  SET @rCurrencyID = ''
  SET @ClientID = ''
  SET @NarrationID = ''
  
 if (@AuthIDResp = '')
 begin
  SET @AuthIDResp = '0'
 end
  
 SELECT @mLastEOD=LastEOD,@mwDate=WorkingDate,@mCONFRETI=CONFRETI FROM t_Last WHERE (OurBranchID = @OurBranchID)  
 
 SELECT @AccountID=AccountID,@STAN=STAN,@PHXDate=PHXDate,@ForeignAmount=ForeignAmount,@ExchangeRate=ExchangeRate,@Amount=Amount,@MerchantType=MerchantType,
 @AcqCountryCode=AcqCountryCode,@ATMID=ATMID,@CurrCodeTran=CurrCodeTran,@CurrCodeSett=CurrCodeSett,@SettlmntAmount=SettlmntAmount,@ProcCode=ProcCode,@POSEntryMode=POSEntryMode,
 @POSConditionCode=POSConditionCode,@POSPINCaptCode=POSPINCaptCode,@AckInstIDCode=AcqInstID,@RetRefNum=RetRefNum,
 @AuthIDResp = Case when @AuthIDResp = '0' then ISNULL(AuthIDResp,'0') else @AuthIDResp end,@CardAccptID=CardAccptID,
 @NameLocation=CardAccptNameLoc,@Description=ISNULL([Description],''),@CAVV=CAVV,@DescriptionID=DescriptionID
 FROM t_ATM_HoldTrxs WHERE OurBranchID = @OurBranchID AND (RefNo = @OrgTrxRefNo OR VISATrxID = @VISATrxID)
 
 SET @NarrationID = @DescriptionID
 
 IF @mLastEOD = @mwDate or @mCONFRETI = '1/1/9999'  
 BEGIN --INVALID_DATE
  INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, 
  ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, 
  CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
  VALUES (@OurBranchID, @AccountID, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @ForeignAmount, @ExchangeRate, @ConvRate, 
  @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, 
  @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @Description, @VISATrxID, @CAVV, @ResponseCode, @DescriptionID, '95', 'Day End', 'Partial Reversal Trx', @AuthIDResp, '')
  
  SELECT @Status = 95, @AvailableBalance = 0, @vClearBalance = 0  
  SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value,'784' as CurrencyCode, @AuthIDResp as AuthIDResp, 
  @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
  RETURN (1)  
 END  
  
 SELECT @vCheckLimit=CheckLimit FROM t_ATM_GlobalVariables Where OurBranchID = @OurBranchID  
 SELECT @vODLimitAllow=ODLimitAllow FROM t_ATM_Banks WHERE OurBranchID = @OurBranchID AND BankIMD = @AcqInstID  
  
 DECLARE @IsVISATrx as bit  
     SET @IsVISATrx = 0  
      
    IF (@ForwardInstID = '500000')  
     SET @IsVISATrx = 1  
    ELSE  
     SET @IsVISATrx = 0  
  
 IF (@IsVISATrx = 1)  
 BEGIN  
 
  SELECT @AccountID=AccountID, @mDescription=Description
  FROM t_ATM_HoldTrxs WHERE OurBranchID = @OurBranchID AND (RefNo = @OrgTrxRefNo OR VISATrxID = @VISATrxID)
  
  Select * Into #TempAddEditTransferPartialReversal FROM t_ATM_HoldTrxs WHERE (OurBranchID=@OurBranchID) AND (AccountID=@AccountID)     
  AND (RefNo = @OrgTrxRefNo OR VISATrxID=@VISATrxID) AND HoldStatus IN ('H','P')    
   
  SELECT @cCount=COUNT(*) from #TempAddEditTransferPartialReversal    

  WHILE (@cCount <> @sCount)    
  BEGIN    
   SET @sCount = @sCount + 1    
   
   SELECT @AccountID=AccountID, @mRefNo=RefNo, @HoldStatus=HoldStatus, @Amount=Amount, @Description=Description, 
   @AuthIDResp = Case when @AuthIDResp = '' then ISNULL(AuthIDResp,'0') else @AuthIDResp end
   FROM (    
    SELECT AccountID, RefNo, HoldStatus, Amount, Description, AuthIDResp, ROW_NUMBER() OVER (ORDER BY RefNo) AS RowNum    
    FROM #TempAddEditTransferPartialReversal
   ) AS oTable    
   WHERE oTable.RowNum = @sCount    
   
   UPDATE t_ATM_HoldTrxs SET RefNo = 'R'+@mRefNo, HoldStatus = 'R', ReleaseDate = @mwDate + ' ' + convert(varchar(12), GetDate(), 114),  
   [Description] = 'Reversal of Amount: '+CAST(@Amount as varchar(30))+' '+@Description, RemainingAmount = 0
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND RefNo = @mRefNo AND HoldStatus IN ('H','P')  
	
   SELECT @HoldAmount=Amount FROM t_AccountFreeze     
   WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND IsFreeze = 1 AND ATMTrxHoldRef = @mRefNo    
	
   --update account freeze to unfreeze    
   Update t_AccountFreeze SET ATMTrxHoldRef= 'R'+@mRefNo, IsFreeze = 0, UnFreezedDate = @mwDate + ' ' + convert(varchar(12), GetDate(), 114), ReleaseComments = 'Reversal Of Amount: '+CAST(@Amount as varchar(30))+' '+@Description
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND ATMTrxHoldRef = @mRefNo    
	
   Update t_AccountBalance SET FreezeAmount = ISNULL(FreezeAmount,0) - ISNULL(@HoldAmount,0)    
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID    
	
   SELECT @FreezeAmount = FreezeAmount FROM t_AccountBalance WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID    
	
   IF (@FreezeAmount <= 0)    
   BEGIN    
    Update t_AccountBalance SET IsFreezed = 0    
    WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID    
   END    
  END    
  
  -- #######hold only the settlement amount received.
  INSERT INTO t_ATM_HoldTrxs         
  (        
   OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID,         
   CurrCodeTran, CurrCodeSett, SettlmntAmount, HoldStatus, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, AuthIDResp,         
   CardAccptID, CardAccptNameLoc, [Description], RemainingAmount, VISATrxID, CAVV, ResponseCode, DescriptionID    
  )          
  SELECT OurBranchID, AccountID, @OrgTrxRefNo, STAN, PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114) , @AmtActualStl, ForeignAmount, ExchangeRate, @ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, @AmtActualTran, 'H', ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, 
  RetRefNum, @AuthIDResp, CardAccptID, CardAccptNameLoc, @mDescription, 0, VISATrxID, CAVV, ResponseCode, DescriptionID
  FROM t_ATM_HoldTrxs WHERE OurBranchID = @OurBranchID AND (RefNo = @OrgTrxRefNo OR RefNo = 'R'+@OrgTrxRefNo)
  
  INSERT INTO t_AccountFreeze        
  (        
   CreateBy, CreateTime, CreateTerminal, SuperviseBy, SuperviseTime, SuperviseTerminal, OurBranchID, AccountID, IsFreeze, [Date], ExpiredOn, Amount, Comments, frmName, ATMTrxHoldRef        
  )
  select CreateBy, @mwDate + ' ' + convert(varchar(12), GetDate(), 114) , CreateTerminal, SuperviseBy, @mwDate + ' ' + convert(varchar(12), GetDate(), 114) , SuperviseTerminal, OurBranchID, AccountID, 1, @mwDate, @mwDate, @AmtActualStl, Comments, frmName, @OrgTrxRefNo        
  FROM t_AccountFreeze where OurBranchID = @OurBranchID AND AccountID = @AccountID AND (ATMTrxHoldRef = @OrgTrxRefNo OR ATMTrxHoldRef = 'R'+@OrgTrxRefNo)
  
  Update t_AccountBalance SET IsFreezed = 1, FreezeAmount = FreezeAmount + @AmtActualStl        
  WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID
  
  SELECT @vClearBalance=ISNULL(B.ClearBalance,0),@vClearedEffects=ISNULL(B.Effects,0),@vFreezeAmount=ISNULL(B.FreezeAmount,0),@vMinBalance=ISNULL(P.MinBalance,0),
  @vIsClose=ISNULL(A.Status,''),@CurrencyCode = ISNULL(c.CurrencyCode,''),@value=ISNULL(p.productatmcode,'00') ,@Limit=ISNULL(B.Limit,0), 
  @MobileNo=ISNULL(cust.MobileNo,''),@Email=ISNULL(cust.Email,''),@ClientID=cust.ClientID
  FROM t_AccountBalance B   
  INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND  B.ProductID = A.ProductID   
  INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND  A.ProductID = P.ProductID   
  INNER JOIN t_Currencies C ON A.OurBranchID = C.OurBranchID AND A.CurrencyID = C.CurrencyID  
  INNER JOIN t_Customer cust ON B.OurBranchID = cust.OurBranchID AND A.ClientID = cust.ClientID
  WHERE (A.OurBranchID = @OurBranchID) AND (A.AccountID = @AccountID)  
  
  --exec @Limit = [fnc_GetLimit] @OurBranchID, @AccountID  
	  
  IF @Limit > 0 AND @vCheckLimit = 1 AND @vODLimitAllow = 1  
  BEGIN  
   SET @vClearBalance=(@vClearBalance+@Limit)  
  END  
  
  Insert Into BalChk     
  (    
   OurBranchID, AccountID, ProductID, [Name], LastUpdateTime, ClearBalance, Limit, Effects, Shadow, FreezeAmt, MinBalance, TotalBalance, AvailableBalance    
  )    
  SELECT OurBranchID, AccountID, ProductID, [Name], @mwDate + ' ' + convert(varchar(12), GetDate(), 114), ISNULL(ClearBalance,0), ISNULL(Limit,0),     
  ISNULL(Effects,0), ISNULL(ShadowBalance,0), ISNULL(FreezeAmount,0), @vMinBalance,     
  (isnull(@Limit,0)+ISNULL(ClearBalance,0)-ISNULL(Effects,0)),    
  (isnull(@Limit,0)+ISNULL(ClearBalance,0)-ISNULL(FreezeAmount,0)-@vMinBalance)    
  FROM t_AccountBalance WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID    
  
  SELECT @Status = 0,     
  @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,  
  @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100  
	 
  SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value,@CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp,
  @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
   
  RETURN (0)  
 END  
 ELSE  
 BEGIN  
 
  INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, 
  ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, 
  CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
  VALUES (@OurBranchID, @AccountID, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @ForeignAmount, @ExchangeRate, @ConvRate, 
  @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, 
  @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @Description, @VISATrxID, @CAVV, @ResponseCode, @DescriptionID, '80', 'Invalid Message Format', 'Partial Reversal Trx', @AuthIDResp, '')
  
  SELECT @Status = 80, @AvailableBalance = 0, @vClearBalance = 0  
  SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value,'784' as CurrencyCode, @AuthIDResp as AuthIDResp,
  @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
      
  RETURN (1)  
 END