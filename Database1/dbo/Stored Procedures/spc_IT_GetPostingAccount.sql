﻿CREATE PROCEDURE [dbo].[spc_IT_GetPostingAccount]  
(    
  @OurBranchID varchar(30),    
  @HOBranchID varchar(30),    
  @TargetBranchID varchar(30),    
  @CurrencyID varchar(30)    
)    
    
AS    
    
SET NOCOUNT ON    
    
select BranchID, IsHOGL, HOAccountID, HOAccountName, IsBranchGL, BranchAccountID, BrAccountName    
from t_ITBranchAccounts    
where OurBranchID = @OurBranchID AND BranchID = @OurBranchID AND CurrencyID = @CurrencyID AND AuthStatus = ''    
    
union all    
    
select BranchID, IsHOGL, HOAccountID, HOAccountName, IsBranchGL, BranchAccountID, BrAccountName    
from t_ITBranchAccounts   
where OurBranchID = @OurBranchID AND BranchID = @HOBranchID AND CurrencyID = @CurrencyID AND AuthStatus = ''    
    
union all    
    
select BranchID, IsHOGL, HOAccountID, HOAccountName, IsBranchGL, BranchAccountID, BrAccountName    
from t_ITBranchAccounts    
where OurBranchID = @OurBranchID AND BranchID = @TargetBranchID AND CurrencyID = @CurrencyID AND AuthStatus = ''