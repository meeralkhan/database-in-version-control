﻿CREATE PROCEDURE [dbo].[spc_crGetOutwardReturn]    
            
 @OurBranchID varchar(30)='',            
 @ReturnDate datetime='01/01/1900'            
 AS            
           
 set nocount on          
--  If Exists(SELECT t_ClearingReturn.ScrollNo FROM t_ClearingReturn LEFT OUTER JOIN            
--       t_Banks ON t_ClearingReturn.OurBranchID = t_Banks.OurBranchID AND             
--       t_ClearingReturn.BankCode = t_Banks.BankID LEFT OUTER JOIN            
--       t_branches ON t_Banks.OurBranchID = t_branches.OurBranchID AND             
--       t_ClearingReturn.BranchCode = t_branches.BranchID AND             
--       t_Banks.BankID = t_branches.BankID            
--       WHERE t_ClearingReturn.OurBranchID = @OurBranchID AND Convert(Varchar(10),wDate,101) = @ReturnDate)            
--             
--   SELECT '1' As ReturnValue, t_ClearingReturn.ScrollNo, t_ClearingReturn.SerialNo,             
--        t_ClearingReturn.OurBranchID, t_ClearingReturn.AccountID,             
--        t_ClearingReturn.ProductID, t_ClearingReturn.ChequeID,             
--        t_ClearingReturn.ChequeDate, t_ClearingReturn.CurrencyID,             
--        t_ClearingReturn.AccountName,             
--        t_ClearingReturn.AccountType, t_ClearingReturn.wDate,             
--        t_ClearingReturn.Amount, t_ClearingReturn.ForeignAmount,             
--        t_ClearingReturn.Reason, t_ClearingReturn.OperatorID,             
--        t_Banks.FullName AS BankName,             
--        t_branches.Name AS BranchName            
--              
--   FROM t_ClearingReturn LEFT OUTER JOIN            
--        t_Banks ON             
--        t_ClearingReturn.OurBranchID = t_Banks.OurBranchID AND             
--        t_ClearingReturn.BankCode = t_Banks.BankID LEFT OUTER JOIN            
--        t_branches ON             
--        t_Banks.OurBranchID = t_branches.OurBranchID AND             
--        t_ClearingReturn.BranchCode = t_branches.BranchID AND             
--        t_Banks.BankID = t_branches.BankID            
--              
--   WHERE t_ClearingReturn.OurBranchID = @OurBranchID AND Convert(Varchar(10),wDate,101) = @ReturnDate            
--  Else            
--   Select '0' as ReturnValue            
--             
            
            
 If Exists(SELECT t_OutwardReturns.ScrollNo FROM t_OutwardReturns LEFT OUTER JOIN            
      t_Banks ON t_OutwardReturns.OurBranchID = t_Banks.OurBranchID AND             
      t_OutwardReturns.BankID = t_Banks.BankID LEFT OUTER JOIN            
      t_branches ON t_Banks.OurBranchID = t_branches.OurBranchID AND             
      t_OutwardReturns.BranchID = t_branches.BranchID AND             
      t_Banks.BankID = t_branches.BankID            
      WHERE t_OutwardReturns.OurBranchID = @OurBranchID AND Convert(Varchar(10),t_OutwardReturns.wDate,120) = @ReturnDate)    
            
  SELECT '1' As ReturnValue, t_OutwardReturns.ScrollNo, t_OutwardReturns.SerialNo,             
       t_OutwardReturns.OurBranchID, t_OutwardReturns.AccountID,             
       t_OutwardReturns.ProductID, t_OutwardReturns.ChequeID,             
       t_OutwardReturns.ChequeDate, t_OutwardReturns.CurrencyID,             
       t_OutwardReturns.AccountName,             
       t_OutwardReturns.AccountType, t_OutwardReturns.wDate,             
       t_OutwardReturns.Amount, t_OutwardReturns.ForeignAmount,             
       t_OutwardReturns.Reason, t_OutwardReturns.OperatorID,             
       t_Banks.FullName AS BankName,             
       t_branches.Name AS BranchName            
             
  FROM t_OutwardReturns LEFT OUTER JOIN            
       t_Banks ON             
       t_OutwardReturns.OurBranchID = t_Banks.OurBranchID AND             
       t_OutwardReturns.BankID = t_Banks.BankID LEFT OUTER JOIN            
       t_branches ON             
       t_Banks.OurBranchID = t_branches.OurBranchID AND             
       t_OutwardReturns.BranchID = t_branches.BranchID AND             
       t_Banks.BankID = t_branches.BankID        
             
  WHERE t_OutwardReturns.OurBranchID = @OurBranchID AND Convert(Varchar(10),wDate,120) = @ReturnDate 
 Else            
  Select '0' as ReturnValue