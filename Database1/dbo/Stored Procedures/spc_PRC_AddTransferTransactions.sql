﻿CREATE PROCEDURE [dbo].[spc_PRC_AddTransferTransactions]      
 @OurBranchID nvarchar(30),        
 @ScrollNo numeric(10,0),          
 @SerialNo int,        
 @RefNo nvarchar(100)=null,    
 @wDate datetime,        
 @AccountType Char(2),          
 @DocumentType char(2) = 'T',        
 @AccountID nvarchar(30),        
 @AccountName varchar(50),        
 @ProductID varchar(30),        
 @CurrencyID varchar(30),        
 @ValueDate datetime,        
 @TrxType Char(1),        
 @ChequeID varchar(50),        
 @ChequeDate DateTime=null,        
 @Amount money,        
 @ForeignAmount Money,        
 @ExchangeRate decimal(10,6),        
 @ProfitLoss money,        
 @MeanRate Money,        
 @DescriptionID varchar(30),        
 @Description varchar(255),        
 @BankCode varchar(30)= '',        
 @BranchCode varchar(30) = '',        
 @TheirAccount varchar(30) = null,        
 @ExtraDetails varchar(50) = null,        
 @ChequeDigit char(1)=null,        
 @VoucherCode char(4)=null,        
 @ReturnCode char(4)=null,        
 @DrawerOrPayee varchar(50)=null,        
 @TrxPrinted bit,        
 @GlID nvarchar(60)=null,        
 @Status char(10),        
 @IsLocalCurrency bit,        
 @OperatorID varchar(30),        
 @SupervisorID varchar(30),    
 @IsMainTrx smallint = 0,     
 @DocType char(2) = null,     
 @GLVoucherID decimal(24,0) = null,    
 @Remarks text = null,
 @AdditionalData text = ''
AS        
BEGIN        
        
  SET NOCOUNT ON         
        
  declare @RetStatus char(1)        
        
  if @IsLocalCurrency=1        
      SET @ExchangeRate=1        
          
  SET @RetStatus = '0'        
         
  Insert Into t_Transactions          
  (OurBranchID, ScrollNo, SerialNo, RefNo, wDate, AccountType, DocumentType, AccountID, AccountName, ProductID, CurrencyID, ValueDate,     
  TrxType, ChequeID, ChequeDate, Amount, ForeignAmount, ExchangeRate, ProfitLoss, MeanRate, DescriptionID, [Description], BankCode,     
  BranchCode, TheirAccount, ExtraDetails, ChequeDigit, VoucherCode, ReturnCode, DrawerOrPayee, TrxPrinted, GLID, [Status],     
  IsLocalCurrency, OperatorID, SupervisorID, IsMainTrx, DocType, GLVoucherID, Remarks, AdditionalData)  
  Values        
  (@OurBranchID, @ScrollNo, @SerialNo, @RefNo, @wDate, @AccountType, @DocumentType, @AccountID, @AccountName, @ProductID, @CurrencyID,     
  @ValueDate, @TrxType, @ChequeID, @ChequeDate, @Amount, @ForeignAmount,@ExchangeRate, @ProfitLoss, @MeanRate, @DescriptionID,     
  @Description, @BankCode, @BranchCode, @TheirAccount, @ExtraDetails, @ChequeDigit, @VoucherCode, @ReturnCode, @DrawerOrPayee,     
  @TrxPrinted, @GLID, @Status, @IsLocalCurrency, @OperatorID, @SupervisorID, @IsMainTrx, @DocType, @GLVoucherID, @Remarks, @AdditionalData)        
          
  select @RetStatus as RetStatus        
          
 END