﻿CREATE Procedure [dbo].[spc_GetMMSecuritySaleValue]  
(  
  @OurBranchID varchar(30),  
  @SecurityID varchar(30),  
  @SettlementDate datetime,  
  @MaturityDate datetime,
  @IsSecuritySale varchar(1)='1'
)  

As

SET NOCOUNT ON

DECLARE   
  @mAmount As Numeric(18,2),  
  @mTotal as Numeric(18,2),  
  @mMaxHoldingValue as Numeric(18,2),  
  @mOpeningBal as Numeric(18,2),  
  @mMaxRRDate as datetime,  
  @mMinOPSettlementDate as datetime  
--  @mMaturityDate as datetime   
    
  
 IF @IsSecuritySale='1'  
  
  BEGIN  
  
   SELECT @mMinOPSettlementDate=IsNull(min(SettlementDate),'01/01/1900') FROM t_MoneyMarketNew  
   WHERE OurBranchID = @OurBranchID AND Status<>'R' AND  
    SecurityIssue=@SecurityID AND typeOfTransaction='OP' AND SettlementDate <=@SettlementDate   
  
   SELECT @mAmount=isnull(sum(Amount),0) FROM t_MoneyMarketNew  
   WHERE OurBranchID = @OurBranchID AND Status<>'R' AND  
    SecurityIssue= @SecurityID AND typeOfTransaction='OP' and Settlementdate <= @Settlementdate  
  
   SET @mMaxHoldingValue = @mAmount  
  
   SELECT @mAmount=isnull(sum(Amount),0) FROM t_MoneyMarketNew  
   WHERE OurBranchID = @OurBranchID AND Status<>'R' AND  
    SecurityIssue= @SecurityID AND typeOfTransaction='OS'  
  
   SET @mMaxHoldingValue=@mMaxHoldingValue - @mAmount  
  
--------------------------------Checkin Security In Pledger MODE  
   SELECT @mAmount=isnull(sum(Amount),0) FROM t_MoneyMarketNew  
   WHERE OurBranchID = @OurBranchID AND Status<>'R' AND SecurityIssue=@SecurityID AND typeOfTransaction='PL' and   
                   settlementdate <= @SettlementDate  
  
   SET @mMaxHoldingValue=@mMaxHoldingValue - @mAmount  
  
   SELECT @mAmount=isnull(sum(Amount),0) FROM t_MoneyMarketNew  
   WHERE OurBranchID = @OurBranchID AND Status<>'R' AND SecurityIssue=@SecurityID AND typeOfTransaction='PL' and   
                   Maturitydate <= @SettlementDate  
  
   SET @mMaxHoldingValue=@mMaxHoldingValue + @mAmount  
-------------------------------------------------------------------  
  
   IF @mMaxHoldingValue > 0   
    BEGIN  
     SELECT @mMaxRRDate =isNull(Max(t_MoneyMarketNew.MaturityDate),'01/01/1900') FROM t_MMSecurityTransaction, t_MoneyMarketNew  
     WHERE t_MoneyMarketNew.OurBranchID = @OurBranchID AND t_MoneyMarketNew.Status<>'R'   
      AND t_MMSecurityTransaction.DealNo=t_MoneyMarketNew.DealNo  
       AND t_MMSecurityTransaction.SecurityID= @SecurityID AND t_MoneyMarketNew.TypeOfTransaction='RR'  
  
     IF @mMaxRRDate < @SettlementDate  
      SET @mMaxRRDate=@SettlementDate  
  
     SELECT @mAmount=isnull(sum(t_MMSecurityTransaction.Amount),0) FROM t_MMSecurityTransaction, t_MoneyMarketNew  
     WHERE t_MoneyMarketNew.OurBranchID = @OurBranchID AND t_MoneyMarketNew.Status<>'R'   
      AND t_MMSecurityTransaction.DealNo=t_MoneyMarketNew.DealNo  
       AND t_MMSecurityTransaction.SecurityID= @SecurityID AND t_MoneyMarketNew.TypeOfTransaction='RR'  
       
     SET @mTotal= @mAmount  
    
     SELECT @mAmount=isnull(sum(t_MMSecurityTransaction.Amount),0) FROM t_MMSecurityTransaction, t_MoneyMarketNew  
     WHERE t_MoneyMarketNew.OurBranchID = @OurBranchID AND t_MoneyMarketNew.Status<>'R'   
      AND t_MMSecurityTransaction.DealNo=t_MoneyMarketNew.DealNo and MaturityDate <=@mMaxRRDate  
       AND t_MMSecurityTransaction.SecurityID= @SecurityID AND t_MoneyMarketNew.TypeOfTransaction='RR'  
       
     SET @mTotal=@mTotal - @mAmount   
  
     SELECT @mAmount=isnull(sum(t_MMSecurityTransaction.Amount),0) FROM t_MMSecurityTransaction, t_MoneyMarketNew  
     WHERE t_MoneyMarketNew.OurBranchID = @OurBranchID AND t_MoneyMarketNew.Status<>'R'   
      AND t_MMSecurityTransaction.DealNo=t_MoneyMarketNew.DealNo   
      AND t_MMSecurityTransaction.SecurityID= @SecurityID AND (t_MoneyMarketNew.TypeOfTransaction='RE')       
  
     SET @mTotal=@mTotal - @mAmount  
  
     SELECT @mAmount=isnull(sum(t_MMSecurityTransaction.Amount),0) FROM t_MMSecurityTransaction, t_MoneyMarketNew  
     WHERE t_MoneyMarketNew.OurBranchID = @OurBranchID AND t_MoneyMarketNew.Status<>'R'   
      AND t_MoneyMarketNew.MaturityDate <=@mMaxRRdate  
      AND t_MMSecurityTransaction.DealNo=t_MoneyMarketNew.DealNo   
      AND t_MMSecurityTransaction.SecurityID= @SecurityID AND (t_MoneyMarketNew.TypeOfTransaction='RE')       
  
     SET @mTotal = @mTotal + @mAmount  
  
     IF @mTotal <= 0   
      SET @mMaxHoldingValue= @mMaxHoldingValue + @mTotal  
       
    END  
  
   SELECT 'Ok' AS ReturnStatus, @mMaxHoldingValue AS Amount,  @mMinOPSettlementDate as SettlementDate  
    
  END  
 ELSE      
  
  BEGIN  
  
   SELECT @mAmount=isnull(sum(Amount),0) FROM t_MoneyMarketNew  
   WHERE OurBranchID = @OurBranchID AND Status<>'R' AND  
    SecurityIssue=@SecurityID AND typeOfTransaction='OP' and Settlementdate <= @SettlementDate  
  
   SET @mTotal = @mAmount  
  
   SELECT @mAmount=isnull(sum(Amount),0) FROM t_MoneyMarketNew  
   WHERE OurBranchID = @OurBranchID AND Status<>'R' AND  
    SecurityIssue=@SecurityID AND typeOfTransaction='OS' and settlementdate <= @SettlementDate  
  
   SET @mTotal=@mTotal - @mAmount  
  
   SELECT @mAmount=isnull(sum(t_MMSecurityTransaction.Amount),0) FROM t_MMSecurityTransaction, t_MoneyMarketNew  
   WHERE t_MoneyMarketNew.OurBranchID = @OurBranchID AND t_MoneyMarketNew.Status<>'R'   
    AND t_MMSecurityTransaction.DealNo=t_MoneyMarketNew.DealNo and SettlementDate <=@SettlementDate  
     AND t_MMSecurityTransaction.SecurityID=@SecurityID AND t_MoneyMarketNew.TypeOfTransaction='RR'  
       
   SET @mTotal=@mTotal + @mAmount  
    
   SELECT @mAmount=isnull(sum(t_MMSecurityTransaction.Amount),0) FROM t_MMSecurityTransaction, t_MoneyMarketNew  
   WHERE t_MoneyMarketNew.OurBranchID = @OurBranchID AND t_MoneyMarketNew.Status<>'R'   
    AND t_MMSecurityTransaction.DealNo=t_MoneyMarketNew.DealNo and MaturityDate <=@SettlementDate  
     AND t_MMSecurityTransaction.SecurityID=@SecurityID AND t_MoneyMarketNew.TypeOfTransaction='RR'  
       
   SET @mTotal=@mTotal - @mAmount  
  
   SELECT @mAmount=isnull(sum(t_MMSecurityTransaction.Amount),0) FROM t_MMSecurityTransaction, t_MoneyMarketNew  
   WHERE t_MoneyMarketNew.OurBranchID = @OurBranchID AND t_MoneyMarketNew.Status<>'R'   
    AND t_MoneyMarketNew.settlementDate <= @SettlementDate   
    AND t_MMSecurityTransaction.DealNo=t_MoneyMarketNew.DealNo   
    AND t_MMSecurityTransaction.SecurityID=@SecurityID AND t_MoneyMarketNew.TypeOfTransaction='RE'  
  
   SET @mTotal = @mTotal - @mAmount  
  
   SELECT @mAmount=isnull(sum(t_MMSecurityTransaction.Amount),0) FROM t_MMSecurityTransaction, t_MoneyMarketNew  
   WHERE t_MoneyMarketNew.OurBranchID = @OurBranchID AND t_MoneyMarketNew.Status<>'R'   
    AND t_MoneyMarketNew.MaturityDate <=@SettlementDate  
    AND t_MMSecurityTransaction.DealNo=t_MoneyMarketNew.DealNo   
    AND t_MMSecurityTransaction.SecurityID=@SecurityID AND t_MoneyMarketNew.TypeOfTransaction='RE'       
  
   SET @mTotal = @mTotal + @mAmount  
  
  
--PLedge Is Out   
   SELECT @mAmount=isnull(sum(Amount),0) FROM t_MoneyMarketNew  
   WHERE OurBranchID = @OurBranchID AND Status<>'R' AND  
    SecurityIssue=@SecurityID AND typeOfTransaction='PL' and   
                   settlementdate <= @SettlementDate  
  
   SET @mTotal=@mTotal - @mAmount  
  
--PLedge Is IN  
   SELECT @mAmount=isnull(sum(Amount),0) FROM t_MoneyMarketNew  
   WHERE OurBranchID = @OurBranchID AND Status<>'R' AND  
    SecurityIssue=@SecurityID AND typeOfTransaction='PL' and   
                   MaturityDate <= @SettlementDate  
  
   SET @mTotal=@mTotal + @mAmount    
   SET @mOpeningBal = @mTotal  
  
--Security Check in Forward Deal  
   SELECT @mAmount=isnull(sum(Amount),0) FROM t_MoneyMarketNew  
   WHERE OurBranchID = @OurBranchID AND Status<>'R' AND  
    SecurityIssue=@SecurityID AND typeOfTransaction='OP' and Settlementdate <= @MaturityDate  
  
   SET @mTotal = @mAmount  
  
   SELECT @mAmount=isnull(sum(Amount),0) FROM t_MoneyMarketNew  
   WHERE OurBranchID = @OurBranchID AND Status<>'R' AND  
    SecurityIssue=@SecurityID AND typeOfTransaction='OS' and settlementdate <= @MaturityDate  
  
   SET @mTotal = @mTotal - @mAmount  
  
   SELECT @mAmount=isnull(sum(t_MMSecurityTransaction.Amount),0) FROM t_MMSecurityTransaction, t_MoneyMarketNew  
   WHERE t_MoneyMarketNew.OurBranchID = @OurBranchID AND t_MoneyMarketNew.Status<>'R'   
    AND t_MMSecurityTransaction.DealNo=t_MoneyMarketNew.DealNo and SettlementDate <=@MaturityDate  
     AND t_MMSecurityTransaction.SecurityID=@SecurityID AND t_MoneyMarketNew.TypeOfTransaction='RR'  
       
   SET @mTotal=@mTotal + @mAmount  
    
   SELECT @mAmount=isnull(sum(t_MMSecurityTransaction.Amount),0) FROM t_MMSecurityTransaction, t_MoneyMarketNew  
   WHERE t_MoneyMarketNew.OurBranchID = @OurBranchID AND t_MoneyMarketNew.Status<>'R'   
    AND t_MMSecurityTransaction.DealNo=t_MoneyMarketNew.DealNo and MaturityDate <= @MaturityDate  
     AND t_MMSecurityTransaction.SecurityID=@SecurityID AND t_MoneyMarketNew.TypeOfTransaction='RR'  
       
   SET @mTotal = @mTotal - @mAmount  
  
   SELECT @mAmount=isnull(sum(t_MMSecurityTransaction.Amount),0) FROM t_MMSecurityTransaction, t_MoneyMarketNew  
   WHERE t_MoneyMarketNew.OurBranchID = @OurBranchID AND t_MoneyMarketNew.Status<>'R'   
    AND t_MoneyMarketNew.settlementDate <= @MaturityDate   
    AND t_MMSecurityTransaction.DealNo=t_MoneyMarketNew.DealNo   
    AND t_MMSecurityTransaction.SecurityID=@SecurityID AND t_MoneyMarketNew.TypeOfTransaction='RE'  
       
  
   SET @mTotal = @mTotal - @mAmount  
  
   SELECT @mAmount=isnull(sum(t_MMSecurityTransaction.Amount),0) FROM t_MMSecurityTransaction, t_MoneyMarketNew  
   WHERE t_MoneyMarketNew.OurBranchID = @OurBranchID AND t_MoneyMarketNew.Status<>'R'   
    AND t_MoneyMarketNew.MaturityDate <=@MaturityDate  
    AND t_MMSecurityTransaction.DealNo=t_MoneyMarketNew.DealNo   
    AND t_MMSecurityTransaction.SecurityID=@SecurityID AND t_MoneyMarketNew.TypeOfTransaction='RE'  
       
  
   SET @mTotal = @mTotal + @mAmount  
  
   SELECT @mAmount=isnull(sum(Amount),0) FROM t_MoneyMarketNew  
   WHERE OurBranchID = @OurBranchID AND Status<>'R' AND SecurityIssue=@SecurityID AND typeOfTransaction='PL' and   
                   settlementdate <= @MaturityDate  
  
   SET @mTotal=@mTotal - @mAmount  
  
   SELECT @mAmount=isnull(sum(Amount),0) FROM t_MoneyMarketNew  
   WHERE OurBranchID = @OurBranchID AND Status<>'R' AND SecurityIssue=@SecurityID AND typeOfTransaction='PL' and   
                   Maturitydate <= @MaturityDate  
  
   SET @mTotal=@mTotal + @mAmount  
  
   SET @mMaxHoldingValue = @mTotal  
  
   IF @mOpeningBal > @mMaxHoldingValue  
    SET @mTotal = @mMaxHoldingValue  
   ELSE  
    SET @mTotal = @mOpeningBal  
  
   SELECT 'Ok' AS ReturnStatus, @mTotal AS Amount,IsNull(@mMinOPSettlementDate,'01/01/1900')   as SettlementDate  
  
  END