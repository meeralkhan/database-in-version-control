﻿CREATE PROCEDURE [dbo].[spc_StopPaymentLetter]  
(        
 @OurBranchId varchar(30),        
 @FAccount varchar(30),        
 @TDate datetime,        
 @Flag varchar(2)      
)      
As        
BEGIN      
      
set nocount on      
        
 if @Flag = 'D'        
  Begin        
   Select  t_StopPayments.AccountID, t_StopPayments.ChequeID,  t_StopPayments.description,        
    t_StopPayments.Date,  t_Account.Name,t_Account.Address,'' as Reference        
   from   t_StopPayments inner join t_Account on        
     t_StopPayments.OurBranchId=t_Account.OurBranchId AND        
     t_StopPayments.AccountID=t_Account.AccountID        
   where  t_StopPayments.OurBranchId = @OurBranchId And Convert(varchar(10),t_StopPayments.Date,101) = @TDate        
   order by t_StopPayments.AccountID, t_StopPayments.ChequeID        
  end         
        
 else        
  if @Flag = 'A'        
   Begin        
    Select  t_StopPayments.AccountID, t_StopPayments.ChequeID, t_StopPayments.description,        
       t_StopPayments.Date,  t_Account.Name,t_Account.Address,'' as Reference        
    from   t_StopPayments inner join t_Account on        
       t_StopPayments.OurBranchId=t_Account.OurBranchId AND        
       t_StopPayments.AccountID=t_Account.AccountID        
    where  t_StopPayments.OurBranchId = @OurBranchId And t_StopPayments.AccountID = @FAccount        
       AND Convert(varchar(10),t_StopPayments.Date,101) = @TDate        
    order by t_StopPayments.AccountID, t_StopPayments.ChequeID        
   end         
END