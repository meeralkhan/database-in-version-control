﻿CREATE   Proc [dbo].[UpdateAccount]    
@UpdateBy  nvarchar(100),      
@UpdateTerminal  nvarchar(100),      
@Auth  nvarchar(100),      
@AuthStatus  nvarchar(100),      
@OurBranchID  nvarchar(100),      
@ClientID  nvarchar(100),      
@AccountID nvarchar(100),    
@Name  nvarchar(100),      
@ProductID  nvarchar(100),      
@Address  nvarchar(100),      
@HoldMail  nvarchar(100),      
@Phone1  nvarchar(100),      
@Phone2  nvarchar(100),      
@Fax  nvarchar(100),      
@EmailID  nvarchar(100),      
@NatureID  nvarchar(100),      
@RelationshipCode  nvarchar(100),      
@StateBankCode  nvarchar(100),      
@AccountType  nvarchar(100),      
@MobileNo  nvarchar(100),      
@tAccrual money       
as     
declare @accountnum as  numeric(18,0)
declare @productPrefix as varchar(100)
declare @accountCount int
declare @acc as varchar(100)
declare @currencyID as varchar(100)    

select @currencyID=CurrencyID, @productPrefix = AccountPrefix from t_Products where ProductID = @productID   
  
update t_Account set UpdateBy=@UpdateBy, UpdateTime=dbo.GetWorkingDate(@OurBranchID), UpdateTerminal=@UpdateTerminal, 
Auth=@Auth,  AuthStatus=@AuthStatus,  OurBranchID=@OurBranchID, ClientID=@ClientID, [Name]=@Name, ProductID=@ProductID, 
CurrencyID=@CurrencyID, [Address]=@Address,Phone1=@Phone1, Phone2=@Phone2, Fax=@Fax, EmailID=@EmailID,  NatureID=@NatureID, 
RelationshipCode=@RelationshipCode, StateBankCode=@StateBankCode,tAccrual=@tAccrual,  AccountType=@AccountType
where OurBranchID=@OurBranchID and ClientID=@ClientID and  accountid= @AccountID   
  
 select * from t_Account where OurBranchID = @OurBranchID and accountid=@AccountID and clientid=@ClientID