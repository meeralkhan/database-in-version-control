﻿create procedure [dbo].[spc_GetDisbAccounts]
(
   @OurBranchID nvarchar(30),
   @AccountID nvarchar(30)
)

AS

SET NOCOUNT ON

select * from t_Account
where OurBranchID = @OurBranchID
and AccountID = @AccountID
and ProductID IN (select ProductID from t_Products where OurBranchID = @OurBranchID and ISNULL(IsInstLoan, '') = 'Yes' and ISNULL(IsAdvancesProduct, '') = 'Yes' AND isnull(FinanceProductType,'') NOT IN ('','OD'))