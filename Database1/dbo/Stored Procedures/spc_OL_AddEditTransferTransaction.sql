﻿CREATE PROCEDURE [dbo].[spc_OL_AddEditTransferTransaction]       
(       
 @ScrollNo int,       
 @RefNo varchar(30),       
 @IBCANo int,       
 @SerialNo int,       
 @OurBranchID varchar(30),       
 @BranchID varchar(30),       
 @AccountID varchar(30),       
 @AccountName varchar(100),       
 @ProductID varchar(30),       
 @CurrencyID varchar(30),       
 @AccountType char(1),       
 @TrxType char(1),       
 @IsLocalCurrency Char(1),       
 @ValueDate datetime,       
 @wDate datetime,       
 @ChequeID varchar(30),       
 @ChequeDate DateTime,       
 @Amount money,       
 @ForeignAmount Money,       
 @ExchangeRate Money,       
 @DescriptionID varchar(30),       
 @Description varchar(255),       
 @Supervision char(1),       
 @OperatorID varchar(30),       
 @SupervisorID varchar(30),       
 @SourceBranchID varchar(30),       
 @TargetBranchID varchar(30),       
     
 @MethodType varchar(2)='',              
 @DocumentType Varchar(2)='',              
 @InstrumentType varchar(50)='',              
 @IssueDate datetime='01/01/1900',              
 @InstrumentNo int=0,              
 @ControlNo  int=0,              
 @Beneficiary varchar(255)='',              
 @DrawnBank  varchar(100)='',              
 @DrawnBranch varchar(100)='',                
     
 @AppWHTax Char(1) = 0,       
 @WHTaxAmount money = 0,       
 @WHTaxMode Varchar(2) ='',       
 @WHTaxAccountID varchar(30)='',       
 @AddData text=NULL       
)       
AS       
       
 set nocount on       
     
 IF @MethodType = 'LR'              
 BEGIN              
   INSERT INTO t_OL_LRPayment              
   (              
    ScrollNo,DocumentType,IssueBranchID,wDate,AccountID,AccountName,InstrumentType,IssueDate, InstrumentNo, ControlNo,      
    Amount,ForeignAmount,Beneficiary, DrawanBank,DrawanBranch, BankID, BranchID      
   )              
              
   VALUES              
   (              
    @RefNo,@DocumentType, @TargetBranchID,@wDate,@AccountID,@AccountName,@InstrumentType,              
    @IssueDate,@InstrumentNo,@ControlNo, @Amount, @ForeignAmount,@Beneficiary, @DrawnBank, @DrawnBranch, @DrawnBank, @OurBranchID      
   )              
 END              
      
     
 INSERT INTO t_OnLineTransferSupervisionRequired (ScrollNo, RefNo, IBCANo, SerialNo, OurBranchID, BranchID, AccountID,       
 AccountName, ProductID, CurrencyID, AccountType, ValueDate, wDate, TrxType, ChequeID, ChequeDate, Amount, ForeignAmount,       
 ExchangeRate, DescriptionID, [Description], Supervision, OperatorID, SupervisorID, IsLocalCurrency, SourceBranchID,       
 TargetBranchID, AppWHTax, WHTaxAmount, WHTaxMode, WHTaxAccountID, MethodType, AdditionalData )       
       
 VALUES (@ScrollNo, @RefNo, @IBCANo, @SerialNo, @OurBranchID, @BranchID, @AccountID, @AccountName,       
 @ProductID, @CurrencyID, @AccountType, @ValueDate, convert(varchar(10),@wDate,101) + ' ' + convert(varchar(20), getdate(),114), @TrxType, @ChequeID, @ChequeDate, @Amount, @ForeignAmount,       
 @ExchangeRate, @DescriptionID, @Description, @Supervision, @OperatorID, @SupervisorID, @IsLocalCurrency,       
 @SourceBranchID, @TargetBranchID, @AppWHTax, @WHTaxAmount, @WHTaxMode, @WHTaxAccountID, @MethodType, @AddData )       
       
 select 'Ok' AS RetStatus, @ScrollNo AS ScrollNo, @IBCANo AS IBCANo, @RefNo AS RefNo