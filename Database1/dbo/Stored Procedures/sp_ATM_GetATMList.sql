﻿CREATE PROCEDURE [dbo].[sp_ATM_GetATMList]

	(
		@OurBranchID varchar(30),
		@ATMID varchar(30)=''
	)	
AS

 	IF @ATMID=''
		BEGIN
			SELECT ATMID, ATMAccountID FROM t_ATM_ATMAccount
			WHERE OurBranchID=@OurBranchID
		END
	ELSE
		BEGIN
			SELECT OurBranchID,ATMID, ATMAccountID FROM t_ATM_ATMAccount
			WHERE OurBranchID=@OurBranchID and ATMID=@ATMID
		END