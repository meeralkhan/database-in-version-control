﻿CREATE Procedure [dbo].[spc_GetMMPartyAndSecurity]  
 (  
  @OurBranchID varchar(30)='',  
  @PartyID varchar(30)='',   
  @Type varchar(2) =''
 )  
As  

 SET NOCOUNT ON
 
 DECLARE @SecurityType AS Varchar(2)  
  
 IF @PartyID<>'' AND @OurBranchID<>''   
  IF @Type<>'B'  
  
   BEGIN  
    SELECT 'Ok' AS ReturnStatus, * FROM t_MoneyMarketAccountM    
    WHERE (OurBranchID = @OurBranchID)  AND (PartyID = @PartyID) AND (Type=@Type)  
   END  
  
  ELSE     
   BEGIN  
    SELECT @SecurityType=SecurityID From t_MMSecurityType,t_MoneyMarketAccountM  
    WHERE t_MoneyMarketAccountM.OurBranchID=@OurBranchID AND t_MoneyMarketAccountM.SecurityType=@Type  
     AND t_MMSecurityType.Behavior=t_MoneyMarketAccountM.SecurityType   
  
    SELECT 'Ok' AS ReturnStatus, * FROM t_MoneyMarketAccountM    
    WHERE (OurBranchID = @OurBranchID)  AND (PartyID = @PartyID) AND (Type='S') AND (SecurityType='B' OR SecurityType=@SecurityType)  
   END