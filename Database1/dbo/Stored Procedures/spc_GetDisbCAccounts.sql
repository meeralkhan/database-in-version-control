﻿create procedure [dbo].[spc_GetDisbCAccounts]  
(  
   @OurBranchID nvarchar(30),  
   @AccountID nvarchar(30)  
)  
  
AS  
  
SET NOCOUNT ON  
  
select * from t_Account  
where OurBranchID = @OurBranchID  
and ClientID IN (select ClientID from t_Account where OurBranchID = @OurBranchID and AccountID = @AccountID)  
and ProductID IN (select CapitalizedProductID from t_Products where OurBranchID = @OurBranchID and ProductID IN (select ProductID from t_Account where OurBranchID = @OurBranchID and AccountID = @AccountID))