﻿CREATE PROCEDURE [dbo].[spc_GLBalanceHistory]
(
  @OurBranchID varchar(30)
)
AS

SET NOCOUNT ON

Declare @Wdate datetime
Select @Wdate = WorkingDate from t_Last where OurBranchID = @OurBranchID

INSERT INTO t_GLBalanceHistory (OurBranchID,wDate,AccountID,[Description],CurrencyID,AccountType,ForeignBalance,Balance)
Select OurBranchID,@wDate,AccountID,[Description],CurrencyID,AccountType,ForeignBalance,Balance From t_GL
WHERE OurBranchID = @OurBranchID AND AccountClass = 'P'

select 'Ok' AS RetStatus