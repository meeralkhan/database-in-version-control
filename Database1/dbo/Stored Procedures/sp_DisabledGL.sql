﻿create PROCEDURE [dbo].[sp_DisabledGL]  
(
	@OurBranchID Varchar(30),
	@Date Varchar(50)
)
AS 
 Set nocount on  
DECLARE @Command AS VARCHAR(MAX)
DECLARE @Command1 AS VARCHAR(MAX)
DECLARE @OurBr Varchar(30)

SET @OurBr = @OurBranchID
IF OBJECT_ID(N'TmpDisabledGL', N'U') IS NOT NULL
BEGIN
	drop table TmpDisabledGL
END

--IF (OBJECT_ID(N't_GLOwners', N'U') IS NOT NULL) AND (OBJECT_ID(N't_Departments', N'U') IS NOT NULL) 
IF EXISTS(SELECT 1 FROM sys.Objects WHERE Object_id = OBJECT_ID(N't_GLOwners') AND Type = N'U') 
AND EXISTS(SELECT 1 FROM sys.Objects WHERE Object_id = OBJECT_ID(N't_Departments') AND Type = N'U') 
BEGIN
	SET @Command1 = 'SELECT Distinct g.AccountID, g.Description, g.OurBranchID, g.AccountType, 
	g.CurrencyID, g.Balance, g.ForeignBalance, (SELECT TOP 1 TrxTimeStamp FROM dbo.t_GLTransactions 
	WHERE AccountID = g.AccountID AND OurBranchID = g.OurBranchID Order By TrxTimeStamp desc) LastTrxTime,
	COALESCE(Cast(t.UpdateTime as date), Cast(t.CreateTime as Date)) DisabledDate, ISNULL(o.OwnerName,''-'') OwnerName, 
	ISNULL(d.DeptName,''-'') Department, t.IsPosting, COALESCE(t.UpdateBy,t.CreateBy) Maker, 
	ISNULL(t.SuperviseBy,''-'') Checker
	into TmpDisabledGL FROM t_GL g
	INNER JOIN t_GL_Logs t ON t.AccountID = g.AccountID AND t.OurBranchID = g.OurBranchID -- AND g.IsPosting = t.IsPosting
	LEFT JOIN t_GLOwners o ON o.OurBranchID = g.OurBranchID AND o.OwnerID = g.GLOwnerID
	Left JOIN t_Departments d ON d.OurBranchID = o.OurBranchID AND d.DeptID = o.OwnerDeptID
	WHERE t.IsPosting = 0 and t.AuthStatus <> ''E'' and g.AccountClass = ''P'' Order by AccountID, OurBranchID;'
END
ELSE
BEGIN
	SET @Command1 = 'SELECT Distinct g.AccountID, g.Description, g.OurBranchID, 
	g.AccountType, g.CurrencyID, g.Balance, g.ForeignBalance,
	(SELECT TOP 1 TrxTimeStamp FROM dbo.t_GLTransactions WHERE AccountID = g.AccountID 
	AND OurBranchID = g.OurBranchID Order By TrxTimeStamp desc) LastTrxTime,
	COALESCE(Cast(t.UpdateTime as date), Cast(t.CreateTime as Date)) DisabledDate, ''-'' OwnerName, ''-'' Department, 
	t.IsPosting, COALESCE(t.UpdateBy,t.CreateBy) Maker, ISNULL(t.SuperviseBy,''-'') Checker
	into TmpDisabledGL FROM t_GL g
	INNER JOIN t_GL_Logs t ON t.AccountID = g.AccountID AND t.OurBranchID = g.OurBranchID -- AND g.IsPosting = t.IsPosting
	WHERE t.IsPosting = 0 and t.AuthStatus <> ''E'' and g.AccountClass = ''P'' Order by AccountID, OurBranchID;'
END
EXECUTE(@Command1)

IF EXISTS(SELECT 1 FROM sys.Objects WHERE Object_id = OBJECT_ID(N'tbl_DisabledGLS') AND Type = N'U')
BEGIN
	IF EXISTS(SELECT 1 FROM sys.Objects WHERE Object_id = OBJECT_ID(N't_GLOwners') AND Type = N'U') 
	AND EXISTS(SELECT 1 FROM sys.Objects WHERE Object_id = OBJECT_ID(N't_Departments') AND Type = N'U') 
	BEGIN
		IF EXISTS(SELECT 1 FROM sys.Objects WHERE Object_id = OBJECT_ID(N'TmpDisabledGL') AND Type = N'U')
		BEGIN
			SET @Command = 'INSERT into TmpDisabledGL Select Distinct g.AccountID, 
			g.Description, g.OurBranchID, g.AccountType,g.CurrencyID, g.Balance, g.ForeignBalance,
			(SELECT TOP 1 TrxTimeStamp FROM t_GLTransactions WHERE AccountID = g.AccountID 
			AND OurBranchID = g.ourbranchid ORDER BY TrxTimeStamp DESC) LastTrxTime,
			Cast(dm.wDate as Date) DisabledDate, ISNULL(o.OwnerName,''-'') OwnerName, ISNULL(d.DeptName,''-'') Department,
			''0'' IsPosting, ''SYSTEM'' Maker, ''SYSTEM'' Checker from tbl_DisabledGLS dm
			inner join t_GL g on dm.AccountID = g.AccountID AND dm.OurBranchID = g.OurBranchID 
			LEFT JOIN t_GLOwners o ON o.OurBranchID = g.OurBranchID AND o.OwnerID = g.GLOwnerID
			Left JOIN t_Departments d ON d.OurBranchID = o.OurBranchID AND d.DeptID = o.OwnerDeptID'
		END
		ELSE
		BEGIN
			SET @Command = 'Select Distinct g.AccountID, 
			g.Description, g.OurBranchID, g.AccountType,g.CurrencyID, g.Balance, g.ForeignBalance,
			(SELECT TOP 1 TrxTimeStamp FROM t_GLTransactions WHERE AccountID = g.AccountID 
			AND OurBranchID = g.ourbranchid ORDER BY TrxTimeStamp DESC) LastTrxTime,
			Cast(dm.wDate as Date) DisabledDate, ISNULL(o.OwnerName,''-'') OwnerName, ISNULL(d.DeptName,''-'') Department,
			''0'' IsPosting, ''SYSTEM'' Maker, ''SYSTEM'' Checker into TmpDisabledGL from tbl_DisabledGLS dm
			inner join t_GL g on dm.AccountID = g.AccountID AND dm.OurBranchID = g.OurBranchID 
			LEFT JOIN t_GLOwners o ON o.OurBranchID = g.OurBranchID AND o.OwnerID = g.GLOwnerID
			Left JOIN t_Departments d ON d.OurBranchID = o.OurBranchID AND d.DeptID = o.OwnerDeptID'
		END
	END
	ELSE
	BEGIN
		IF EXISTS(SELECT 1 FROM sys.Objects WHERE Object_id = OBJECT_ID(N'TmpDisabledGL') AND Type = N'U')
		BEGIN
			SET @Command = 'INSERT into TmpDisabledGL Select Distinct g.AccountID, 
			g.Description, g.OurBranchID, g.AccountType,g.CurrencyID, g.Balance, g.ForeignBalance,
			(SELECT TOP 1 TrxTimeStamp FROM t_GLTransactions WHERE AccountID = g.AccountID 
			AND OurBranchID = g.ourbranchid ORDER BY TrxTimeStamp DESC) LastTrxTime,
			Cast(dm.wDate as Date) DisabledDate, ''-'' OwnerName, ''-'' Department,
			''0'' IsPosting, ''SYSTEM'' Maker, ''SYSTEM'' Checker from tbl_DisabledGLS dm
			inner join t_GL g on dm.AccountID = g.AccountID AND dm.OurBranchID = g.OurBranchID' 
		END
		ELSE
		BEGIN
			SET @Command = 'Select Distinct g.AccountID, 
			g.Description, g.OurBranchID, g.AccountType,g.CurrencyID, g.Balance, g.ForeignBalance,
			(SELECT TOP 1 TrxTimeStamp into TmpDisabledGL FROM t_GLTransactions WHERE AccountID = g.AccountID 
			AND OurBranchID = g.ourbranchid ORDER BY TrxTimeStamp DESC) LastTrxTime,
			Cast(dm.wDate as Date) DisabledDate, ''-'' OwnerName, ''-'' Department,
			''0'' IsPosting, ''SYSTEM'' Maker, ''SYSTEM'' Checker into TmpDisabledGL from tbl_DisabledGLS dm
			inner join t_GL g on dm.AccountID = g.AccountID AND dm.OurBranchID = g.OurBranchID' 
		END
	END
END

EXECUTE(@Command)
Select * from TmpDisabledGL where OurBranchID = @OurBr And Cast(DisabledDate as Date) <= @Date Order By AccountID, OurBranchID