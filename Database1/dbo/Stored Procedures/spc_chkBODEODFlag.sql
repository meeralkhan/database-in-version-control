﻿CREATE Procedure [dbo].[spc_chkBODEODFlag]  
(  
 @OurBranchID nvarchar(30)  
)  
  
AS  
  
SET NOCOUNT ON  
  
declare @IsBODStart int  
declare @IsBODEnd int  
declare @IsEODStart int  
declare @IsEODEnd int  
  
Begin Try  
   
 if ((select COUNT(OurBranchID) from t_last where OurBranchID = @OurBranchID) > 0)  
 BEGIN  
    
   select @IsBODStart = COUNT(IsApply) from t_SOD_Programs  
   where OurBranchID = @OurBranchID AND ProgramID = 'VALIDATE' and IsStart = '1'  
     
   select @IsBODEnd = COUNT(IsApply) from t_SOD_Programs  
   where OurBranchID = @OurBranchID AND ProgramID = 'INTSOD' and IsEnd = '1'  
     
   select @IsEODStart = COUNT(IsApply) from t_EOD_Programs  
   where OurBranchID = @OurBranchID AND ProgramID = 'VALIDATE' and IsStart = '1'  
     
   select @IsEODEnd = COUNT(IsApply) from t_EOD_Programs  
   where OurBranchID = @OurBranchID AND ProgramID = 'EOD' and IsEnd = '1'  
     
   Select 'Ok' As ReturnStatus, @IsBODStart AS IsBODStart, @IsBODEnd AS IsBODEnd,   
   @IsEODStart AS IsEODStart, @IsEODEnd AS IsEODEnd  
     
 END  
 else  
 begin  
  Select 'Error' As ReturnStatus,  'Branch ID Not Found...' AS ReturnMessage  
 end  
  
End Try  
Begin Catch  
   
 Select 'Error' As ReturnStatus,  ERROR_MESSAGE() AS ReturnMessage  
   
End Catch