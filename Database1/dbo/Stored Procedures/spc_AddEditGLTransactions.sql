﻿Create PROCEDURE [dbo].[spc_AddEditGLTransactions]    
(        
  @OurBranchID nvarchar(30),      
  @RefNo nvarchar(100) = '',      
  @AccountID nvarchar(30),      
  @VoucherID numeric(10,0),      
  @SerialID numeric (10,0),      
  @Date datetime,      
  @ValueDate datetime=null,      
  @DescriptionID varchar(30),      
  @Description varchar (255),      
  @CurrencyID varchar(30),      
  @Amount money,      
  @ForeignAmount money,      
  @ExchangeRate numeric(18, 6)=1,      
  @IsCredit bit,      
  @TransactionType Varchar (32),      
  @TransactionMethod char(1)='',      
  @OperatorID varchar(30),      
  @SupervisorID varchar(30),      
  @Indicator char(2),      
  @ScrollNo int,      
  @SerialNo int,  
  @Status char(1) = '',  
  @IsMainTrx smallint = 0,  
  @DocType char(2) = '',  
  @Remarks text = '',
  @AdditionalData TEXT = ''
 )        
AS        
BEGIN      
      
  SET NOCOUNT ON       
        
  DECLARE @RetStatus char(1)      
        
  SET @RetStatus = '0'      
      
  INSERT INTO t_GLTransactions (OurBranchID, RefNo, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],   
  CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID, Indicator,   
  ScrollNo, SerialNo, [Status], IsMainTrx, DocType, Remarks, AdditionalData)      
  VALUES (@OurBranchID, @RefNo, @AccountID, @VoucherID, @SerialID, @Date, @ValueDate, @DescriptionID, @Description, @CurrencyID, @Amount,   
  @ForeignAmount, @ExchangeRate, @IsCredit, @TransactionType, @TransactionMethod, @OperatorID, @SupervisorID, @Indicator, @ScrollNo,   
  @SerialNo, @Status, @IsMainTrx, @DocType, @Remarks, @AdditionalData)      
        
  select @RetStatus as RetStatus      
      
END