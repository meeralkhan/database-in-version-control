﻿

/****** Object:  StoredProcedure [dbo].[spc_Get_UserAccess]    Script Date: 06/04/2021 2:29:22 PM ******/


CREATE       PROCEDURE [dbo].[spc_Get_UserAccess]  
(  
  @OurBranchID VARCHAR(30),  
  @UserName VARCHAR(100),  
  @FormName VARCHAR(100)  
)  
AS  
          
SET NOCOUNT ON          
          
BEGIN TRY  
    Declare @RoleID varchar(10)
	Select  @RoleID= RoleID from t_AssignRoles where OurBranchID = @OurBranchID AND OperatorID = @UserName And Auth=0

  --IF NOT EXISTS(SELECT OperatorID FROM t_MapDrive WHERE OurBranchID = @OurBranchID AND OperatorID = @UserName AND FormToRun = @FormName AND Auth = 0)  
  IF NOT EXISTS(SELECT OperatorID FROM t_MapDrive WHERE OurBranchID = @OurBranchID AND OperatorID IN (Select  RoleID from t_AssignRoles 
  where OurBranchID = @OurBranchID AND OperatorID = @UserName And Auth=0)
  
  AND FormToRun = @FormName AND Auth = 0)  

  BEGIN  
     Select 'Error' as ReturnStatus, 'UnSufficientRights' as ReturnMessage;  
  END  
  ELSE  
  BEGIN  
     --Select 'Ok' as ReturnStatus, 'Access Granted' as ReturnMessage,   
     --FullAccess, AddAccess, EditAccess, DeleteAccess, VerifyAccess, AuthorizeAccess,            
     --CreditPostingLimit, DebitPostingLimit, CreditVerifyLimit, DebitVerifyLimit,       
     --CreditSupervisionLimit, DebitSupervisionLimit            
     --FROM t_MapDrive            
     --WHERE OurBranchID = @OurBranchID AND OperatorID = @UserName AND FormToRun = @FormName AND Auth = 0 
	 Select 'Ok' as ReturnStatus, 'Access Granted' as ReturnMessage,
	 Max(Cast(FullAccess as int)) FullAccess, Max(Cast(AddAccess as int)) AddAccess, Max(Cast(EditAccess as int)) EditAccess, Max(Cast(DeleteAccess as int)) DeleteAccess, 
	 Max(Cast(VerifyAccess as int)) VerifyAccess,  Max(Cast(AuthorizeAccess as int)) AuthorizeAccess,  Max(CreditPostingLimit) CreditPostingLimit, 
	 Max(DebitPostingLimit) DebitPostingLimit,  Max(CreditVerifyLimit) CreditVerifyLimit, Max(DebitVerifyLimit) DebitVerifyLimit,  
	 Max(CreditSupervisionLimit) CreditSupervisionLimit, Max(DebitSupervisionLimit) DebitSupervisionLimit
     FROM t_MapDrive            
     WHERE OurBranchID = @OurBranchID AND OperatorID  IN (Select RoleID from t_AssignRoles where OurBranchID = @OurBranchID AND OperatorID = @UserName And Auth=0)
	 
	 AND FormToRun = @FormName AND Auth = 0 
  END  
  
END TRY  
  
BEGIN CATCH  
       
     Select 'Error' as ReturnStatus, Error_Message() as ReturnMessage;  
       
END CATCH