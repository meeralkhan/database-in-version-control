﻿CREATE PROCEDURE [dbo].[spc_GetCustomerStatementFCYTime]    
(    
 @OurBranchID varchar(30),    
 @AccountID varchar(30),    
 @sDate Datetime,    
 @eDate Datetime    
)    
    
AS    
BEGIN    
     
 SET NOCOUNT ON    
     
 DECLARE @CurrencyID nvarchar(30)    
 DECLARE @OpeningBalance as money    
 DECLARE @LocEqOpBal as money    
    
 DECLARE @crOpBal as money    
 DECLARE @crLocEqOpBal as money    
    
 DECLARE @drOpBal as money    
 DECLARE @drLocEqOpBal as money    
    
 DECLARE @tDate as Varchar(15)    
 DECLARE @WorkingDate as datetime    
    
 SELECT @WorkingDate = WorkingDate From t_Last    
 WHERE OurBranchID = @OurBranchID    
    
    
 SET @OpeningBalance = 0    
 SET @crOpBal = 0    
 SET @drOpBal = 0    
    
 SET @LocEqOpBal = 0    
 SET @crLocEqOpBal = 0    
 SET @drLocEqOpBal = 0    
     
 SELECT @OurBranchID = OurBranchID, @AccountID = AccountID, @CurrencyID = CurrencyID    
 FROM t_Account    
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID    
    
 SET @tDate = cast(Month(@sDate) as Varchar(2)) + '/01/' + cast(Year(@sDate)as Varchar(4))    
    
 SELECT @OpeningBalance = IsNull(ClearBalance,0) , @LocEqOpBal = LocalClearBalance     
 FROM t_AccountMonthBalances    
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND Month = @tDate    
     
 SELECT @crOpBal = IsNull(SUM(ForeignAmount),0),@crLocEqOpBal = IsNull(SUM(Amount),0) From t_Transactions    
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID     
 AND TrxType = 'C' AND AccountType = 'C' and [Status] <> 'R'    
 AND convert(varchar(10), wDate,101) >= @tDate AND convert(varchar(10), wDate,101) < @sDate    
    
 SELECT @drOpBal = IsNull(SUM(ForeignAmount),0),@drLocEqOpBal = IsNull(SUM(Amount),0) From t_Transactions    
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID     
 AND TrxType = 'D' AND AccountType = 'C' and [Status] <> 'R'    
 AND convert(varchar(10), wDate,101) >= @tDate AND convert(varchar(10), wDate,101) < @sDate    
     
 SET @OpeningBalance = @OpeningBalance + @crOpBal - @drOpbal    
 SET @LocEqOpBal = @LocEqOpBal + @crLocEqOpBal - @drLocEqOpBal    
     
 select OurBranchID, AccountID, CurrencyID, Amount, ForeignAmount, IsCredit, ValueDate, [Date], ChannelRefID, TrxType, VoucherID, SerialID, DescriptionID, Description, TrxTimeStamp, TableStr from (      
    
   SELECT @OurBranchID AS OurBranchID, @AccountID AS AccountID, @CurrencyID AS CurrencyID, IsNull(@LocEqOpBal,0) as Amount, IsNull(@OpeningBalance,0) as ForeignAmount, '0' AS IsCredit, @sDate as ValueDate,    
   '01/01/1900' as [Date], '' AS ChannelRefID, '' AS TrxType, 0 as VoucherID, 0 as SerialID, 'OP' AS DescriptionID, 'Opening Balance' as Description, '01/01/1900' AS TrxTimeStamp, 'OP' As TableStr    
    
   UNION ALL    
    
   SELECT OurBranchID, AccountID, CurrencyID, Amount, ForeignAmount, case TrxType when 'C' then '1' else '0' end AS IsCredit, ValueDate, wDate AS [Date], ISNULL(ChannelRefID,'') AS ChannelRefID,    
   TrxType, ScrollNo as VoucherID, SerialNo as SerialID, DescriptionID, Description, TrxTimeStamp, 'H' As TableStr    
   FROM t_Transactions    
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID     
   AND AccountType = 'C' and [Status] <> 'R'    
   AND convert(varchar(10), wDate,101) >= @sDate AND convert(varchar(10) , wDate,101) <= @eDate    
    
 ) a order by TrxTimeStamp    
    
END