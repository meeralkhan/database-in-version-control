﻿CREATE PROCEDURE [dbo].[spc_CashUpdateBalance]           
(          
   @OurBranchID varchar(30),          
   @AccountID varchar(30),          
   @Amount money=0,          
   @ForeignAmount Money=0,          
   @IsDebit bit=0,          
   @Supervision Char(1)=' ',          
   @IsLocalCurrency integer=1,          
   @GoInEffects integer=0,          
   @NewRecord bit = 0,  
   @PrevDayBalance int = 0  
)          
          
AS          
         
   SET NOCOUNT ON      
         
   DECLARE @RetStatus as int          
          
      /* If No Supervision Required */          
   IF @Supervision = 'C'          
      BEGIN --S1------------------          
         IF @IsDebit = 0  --S2=====================          
            BEGIN          
               IF @IsLocalCurrency = 1 AND @GoInEffects = 0          
      begin  
                  UPDATE t_AccountBalance          
                  SET ClearBalance = ClearBalance + @Amount          
                  WHERE  OurBranchID = @OurBranchID and AccountID = @AccountID          
  
      if @PrevDayBalance = 1  
           
      UPDATE t_PrevAccountBalances  
                     SET ClearBalance = ClearBalance + @Amount          
                     WHERE  OurBranchID = @OurBranchID and AccountID = @AccountID          
  
               end  
               ELSE IF @IsLocalCurrency = 1 AND @GoInEffects = 1          
      begin  
                  UPDATE t_AccountBalance          
                  SET Effects =  Effects + @Amount          
                  WHERE  OurBranchID = @OurBranchID and AccountID = @AccountID              
  
      if @PrevDayBalance = 1  
           
      UPDATE t_PrevAccountBalances  
                     SET Effects =  Effects + @Amount          
      WHERE  OurBranchID = @OurBranchID and AccountID = @AccountID              
  
      end  
               ELSE IF @IsLocalCurrency = 0 AND @GoInEffects = 0          
      begin  
  
                  UPDATE t_AccountBalance          
                  SET ClearBalance = ClearBalance + @ForeignAmount,          
                     LocalClearBalance = LocalClearBalance + @Amount          
                  WHERE  OurBranchID = @OurBranchID and AccountID = @AccountID       
        
      if @PrevDayBalance = 1  
           
      UPDATE t_PrevAccountBalances  
                     SET ClearBalance = ClearBalance + @ForeignAmount,          
                     LocalClearBalance = LocalClearBalance + @Amount          
      WHERE  OurBranchID = @OurBranchID and AccountID = @AccountID   
  
      end  
               ELSE IF @IsLocalCurrency = 0 AND @GoInEffects = 1          
      begin  
                  UPDATE t_AccountBalance          
                  SET Effects = Effects + @ForeignAmount,          
                     LocalEffects = LocalEffects + @Amount          
                  WHERE  OurBranchID = @OurBranchID and AccountID = @AccountID          
        
      if @PrevDayBalance = 1  
           
      UPDATE t_PrevAccountBalances  
                     SET Effects = Effects + @ForeignAmount,          
                     LocalEffects = LocalEffects + @Amount          
      WHERE  OurBranchID = @OurBranchID and AccountID = @AccountID          
  
      end  
            END --E2          
          
         ELSE --S2===================================          
            BEGIN          
               IF @IsLocalCurrency = 1                      
      begin  
                  Update t_AccountBalance          
                  SET ClearBalance = ClearBalance - @Amount          
                  WHERE  OurBranchID = @OurBranchID and AccountID = @AccountID          
  
      if @PrevDayBalance = 1  
           
      UPDATE t_PrevAccountBalances  
                     SET ClearBalance = ClearBalance - @Amount          
                     WHERE  OurBranchID = @OurBranchID and AccountID = @AccountID          
  
      end  
               ELSE IF @IsLocalCurrency = 0  
      begin  
      UPDATE t_AccountBalance          
                  SET ClearBalance = ClearBalance - @ForeignAmount,          
      LocalClearBalance = LocalClearBalance - @Amount          
                  WHERE  OurBranchID = @OurBranchID and AccountID = @AccountID                
  
      if @PrevDayBalance = 1  
           
      UPDATE t_PrevAccountBalances  
                     SET ClearBalance = ClearBalance - @ForeignAmount,          
      LocalClearBalance = LocalClearBalance - @Amount          
                     WHERE  OurBranchID = @OurBranchID and AccountID = @AccountID                
  
      end  
            END --E2================================          
      END          
/* If Supervision is Required */          
   ELSE --E1----------------------------------------          
      BEGIN          
          
-- Only Debit Trx In Shadow either its supervision or remote pass          
-- so Credit Trx Comment its discussed           
/*          
         IF @IsDebit = 0           
          
            BEGIN          
               IF @IsLocalCurrency = 1          
                  UPDATE t_AccountBalance          
                  SET ShadowBalance = ShadowBalance + @Amount          
                  WHERE  OurBranchID = @OurBranchID and AccountID = @AccountID          
                         
               ELSE IF @IsLocalCurrency = 0          
                  UPDATE t_AccountBalance          
                  SET ShadowBalance = ShadowBalance + @ForeignAmount          
                  WHERE  OurBranchID = @OurBranchID and AccountID = @AccountID          
            END          
          
         ELSE           
*/          
         IF @IsDebit = '1'           
            BEGIN          
          
               IF @IsLocalCurrency = 1          
      begin  
                  UPDATE t_AccountBalance          
                  SET ShadowBalance = ShadowBalance - @Amount          
                  WHERE  OurBranchID = @OurBranchID and AccountID = @AccountID          
        
      if @PrevDayBalance = 1  
           
      UPDATE t_PrevAccountBalances  
                     SET ShadowBalance = ShadowBalance - @Amount          
                     WHERE  OurBranchID = @OurBranchID and AccountID = @AccountID          
  
      end  
               ELSE IF @IsLocalCurrency = 0  
      begin  
           
      UPDATE t_AccountBalance          
                  SET ShadowBalance = ShadowBalance - @ForeignAmount          
                  WHERE  OurBranchID = @OurBranchID and AccountID = @AccountID        
        
      if @PrevDayBalance = 1  
           
      UPDATE t_PrevAccountBalances  
                     SET ShadowBalance = ShadowBalance - @ForeignAmount          
                     WHERE  OurBranchID = @OurBranchID and AccountID = @AccountID        
    
      end  
            END          
          
      END          
          
   SELECT @RetStatus = @@ERROR          
          
   IF @RetStatus <> 0          
      RETURN(30)          
   ELSE          
      RETURN(0)