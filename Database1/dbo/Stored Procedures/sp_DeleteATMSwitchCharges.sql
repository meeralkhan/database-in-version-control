﻿
CREATE PROCEDURE [dbo].[sp_DeleteATMSwitchCharges]
(    
  @OurBranchID varchar(30),
  @MerchantType varchar(30)
 )    
AS    
     
   DELETE FROM t_ATM_SwitchCharges
   WHERE  OurBranchID = @OurBranchID AND MerchantType = @MerchantType