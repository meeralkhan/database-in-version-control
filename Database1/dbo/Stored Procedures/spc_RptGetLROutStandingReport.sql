﻿CREATE procedure [dbo].[spc_RptGetLROutStandingReport] (  
 @OurBranchID varchar(30),  
 @FromDate datetime,  
 @ToDate datetime,  
 @InstrumentType varchar(30) = ''  
)  
As  
BEGIN  
 set nocount on  
   
 if @InstrumentType = ''  
 BEGIN  
  
  SELECT a.SerialNo, a.InstrumentType, a.wDate, b.BankID, b.FullName as BankName, c.BranchID, c.BranchName, a.InstrumentNo,   
  a.ControlNo, case a.Supervision  
   when 'C' then 'Cleared'  
   when '' then 'Cleared'  
   when '*' then 'Under Supervision'  
   when 'U' then 'Under Supervision'  
   when 'P' then 'Remote Pass Supervision'  
   when 'R' then 'Rejected'  
  end as SupervisionStatus,  
  MarkingStatus = Case   
   When isnull(Supervision,'') = 'R' then 'Rejected'  
   When isnull(Paid,'0') = 1 then 'Paid'   
   When isnull(Cancel,'0') = 1 then 'Cancel'   
   When isnull(Lost,'0') = 1 then 'Lost'   
   When isnull([Stop],'0') = 1 and isnull(Lost,'0')=0 and isnull(Cancel,'0')=0 and isnull(Paid,'0')=0 then 'OutStanding(Stop)'   
   When isnull([Stop],'0')= 0 then 'OutStanding'   
  End,   
  a.PayeeName, a.Amount, a.Charges, a.BeneficiaryName, a.CancelDate as MarkingDate, a.CreateBy as OperatorID,   
  a.SuperviseBy as SupervisorID, a.AdditionalData  
  FROM t_LR_Issuance a  
  INNER JOIN t_LR_Banks b ON a.OurBranchID = b.OurBranchID AND a.DrawnBankID = b.BankID  
  INNER JOIN t_LR_Branches c ON a.OurBranchID=c.OurBranchID AND b.BankID = c.BankID AND a.DrawnBranchID = c.BranchID  
  WHERE a.OurBranchID=@OurBranchID   
  AND ((isnull([Stop],'0') = 1 and isnull(Lost,'0')=0 and isnull(Cancel,'0')=0 and isnull(Paid,'0')=0)  
  OR (isnull([Stop],'0')= 0 AND ISNULL([Paid], '0') = 0))  
  AND ISNULL(Supervision, '') IN ('C', '')  
  AND convert(varchar(10), a.wDate,120) >= @FromDate   
  AND convert(varchar(10), a.wDate,120) <= @ToDate  
  
 END  
 ELSE  
 BEGIN  
  
  SELECT a.SerialNo, a.InstrumentType, a.wDate, b.BankID, b.FullName as BankName, c.BranchID, c.BranchName, a.InstrumentNo, a.ControlNo,   
  case a.Supervision  
   when 'C' then 'Cleared'  
   when '' then 'Cleared'  
   when '*' then 'Under Supervision'  
   when 'U' then 'Under Supervision'  
   when 'P' then 'Remote Pass Supervision'  
   when 'R' then 'Rejected'  
  end as SupervisionStatus,  
  MarkingStatus = Case   
   When isnull(Supervision,'') = 'R' then 'Rejected'  
   When isnull(Paid,'0') = 1 then 'Paid'   
   When isnull(Cancel,'0') = 1 then 'Cancel'   
   When isnull(Lost,'0') = 1 then 'Lost'   
   When isnull([Stop],'0') = 1 and isnull(Lost,'0')=0 and isnull(Cancel,'0')=0 and isnull(Paid,'0')=0 then 'OutStanding(Stop)'   
   When isnull([Stop],'0')= 0 then 'OutStanding'   
  End,   
  a.PayeeName, a.Amount, a.Charges, a.BeneficiaryName, a.CancelDate as MarkingDate, a.CreateBy as OperatorID,   
  a.SuperviseBy as SupervisorID, a.AdditionalData, a.Paid  
  FROM t_LR_Issuance a  
  INNER JOIN t_LR_Banks b ON a.OurBranchID = b.OurBranchID AND a.DrawnBankID = b.BankID  
  INNER JOIN t_LR_Branches c ON a.OurBranchID=c.OurBranchID AND b.BankID = c.BankID AND a.DrawnBranchID = c.BranchID  
  WHERE a.OurBranchID=@OurBranchID AND a.InstrumentType=@InstrumentType  
  AND ((isnull([Stop],'0') = 1 and isnull(Lost,'0')=0 and isnull(Cancel,'0')=0 and isnull(Paid,'0')=0)  
  OR (isnull([Stop],'0')= 0 AND ISNULL([Paid], '0') = 0))  
  AND ISNULL(Supervision, '') IN ('C', '')  
  AND convert(varchar(10), a.wDate,120) >= @FromDate   
  AND convert(varchar(10), a.wDate,120) <= @ToDate  
 END  
END