﻿CREATE PROCEDURE [dbo].[spc_DeleteInwardTransactionMain]            
            
 @OurBranchID varchar(30),              
 @SerialNo int,              
 @AccountID varchar(30),              
 @ChequeID varchar(11)='',              
 @Amount money=0,              
 @ForeignAmount money=0,              
 @CurrencyID varchar(4)='',              
 @AccountType char(1)='C',              
 @Status char(1)='',              
 @TransactionType char(1)='',              
 @wDate datetime='',              
 @ClearBalance money=0,              
 @Effects money=0,              
 @Limit money=0,              
 @NewRecord bit=0              
AS              
               
 SET NOCOUNT ON            
               
 Declare @LocalCurrency varchar(4)              
 --DECLARE @AccountStatus char(1)              
               
 Select  @AccountID = AccountID,@wDate=wDate, @ChequeID = ChequeID , @CurrencyID = CurrencyID ,             
 @Amount=Amount, @ForeignAmount=ForeignAmount,              
 @TransactionType=TransactionType, @AccountType = AccountType , @Status = Status               
 From   t_InwardClearing              
 Where SerialNo = @SerialNo AND OurBranchID = @OurBranchID            
             
 --SELECT @Status = ModuleStatus FROM t_Authorization_RejectedTrx            
 --WHERE month(wDate) = month(@wDate) and day(wDate) = day(@wDate) and year(wDate) = year(@wDate)               
 --and ModuleType='I' and SerialNo = @SerialNo AND OurBranchID = @OurBranchID            
             
 /* Needs to Discuss */              
 --SELECT @AccountStatus = ''              
 --IF @AccountType = 'C'              
 -- BEGIN              
 --  SELECT @AccountStatus = IsNull(Status,'') FROM t_Account              
 --  WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID              
               
 --  IF Upper(Ltrim(RTrim(@AccountStatus))) = 'C'              
 --       BEGIN              
 --          SELECT '4' as RetStatus -- Closed Account Can Not Rejected...              
 --          RETURN (4)              
 --       END                 
 -- END              
                
 Select @ClearBalance=ClearBalance,@Effects = Effects,@Limit = Limit               
 From t_AccountBalance              
 Where OurBranchID = @OurBranchID and AccountID = @AccountID              
               
 Select @LocalCurrency = LocalCurrency From t_GlobalVariables Where OurBranchID = @OurBranchID              
              
-- Delete Entry From Transaction Table              
 Delete From t_InwardClearing              
 Where SerialNo = @SerialNo AND OurBranchID = @OurBranchID and AccountID = @AccountID            
             
-- Delete Entry From ChequePaid Table              
 Delete From t_ChequePaid               
 Where OurBranchID = @OurBranchID and AccountID = @AccountID and ChequeID = @ChequeID            
             
--         Update Balance Table              
 IF @AccountType = 'C'               
 BEGIN              
  IF @Status = ''              
   BEGIN              
    IF @LocalCurrency = @CurrencyID              
     Update t_AccountBalance              
     Set  ClearBalance = (ClearBalance + @Amount)              
     Where (OurBranchID = @OurBranchID) and (AccountID = @AccountID)              
    ELSE              
     Update t_AccountBalance              
     Set  ClearBalance = (ClearBalance + @ForeignAmount),              
     LocalClearBalance = LocalClearBalance + @Amount              
     Where (OurBranchID = @OurBranchID) and (AccountID = @AccountID)              
   END              
              
  ELSE IF @Status = 'U' OR @Status = '*' OR @Status = 'E'              
   BEGIN              
    IF @LocalCurrency = @CurrencyID              
     Update t_AccountBalance              
     Set  ShadowBalance = (ShadowBalance + @Amount)              
     Where (OurBranchID = @OurBranchID) and (AccountID = @AccountID)              
    ELSE              
     Update t_AccountBalance              
     Set  ShadowBalance = (ShadowBalance + @ForeignAmount)              
     Where (OurBranchID = @OurBranchID) and (AccountID = @AccountID)              
   END              
  ELSE              
   BEGIN              
    IF @LocalCurrency = @CurrencyID              
     Update t_AccountBalance              
     Set  ShadowBalance = (ShadowBalance + @Amount)              
     Where (OurBranchID = @OurBranchID) and (AccountID = @AccountID)              
    ELSE              
     Update t_AccountBalance              
     Set  ShadowBalance = (ShadowBalance + @ForeignAmount)              
     Where (OurBranchID = @OurBranchID) and (AccountID = @AccountID)              
              
   END              
END              
              
 Delete From t_CustomerStatus              
 Where (OurBranchID = @OurbranchID) and (AccountID = @AccountID) and (SerialNo = @SerialNo) AND TransactionType = 'DI'              
              
 /* Create a New record For Superviser */       Insert Into t_SupervisedBy              
 (OurBranchID,AccountID,[Date],ScrollNo,ChequeID,ClearBalance,Effects,Limit,TrxType,Amount,Status,SupervisorID,TransactionCategory)              
 Values              
 (@OurBranchID,@AccountID,@wDate,@SerialNo,'Deleted',@ClearBalance,@Effects,@Limit,'D',@Amount,@Status,'','I')              
              
 IF @AccountType = 'G'               
  BEGIN              
 --Local Remittance Unpaid Mark              
   Update t_LR_Issuance              
   Set Paid = 0,              
             PaidDate = '1/1/1900',              
          PaidScrollNo = 0,              
          Supervision = 'C',              
          PaidMode = 'NA'              
   Where (OurBranchID=@OurBranchID) and (InstrumentAccountID = @AccountID)      
   and  (PaidScrollNo = @SerialNo) and  (convert(varchar(10),PaidDate,101) =convert(varchar(10), @wDate,101))       
   and (PaidMode = 'IW')              
               
 --Local Remittance Unpaid Mark            
 /*            
   Update t_LR_Advice              
   Set Paid = 0,            
   PaidDate = '1/1/1900',            
   PaidScrollNo = 0,            
   PaidMode = 'NA'            
   Where (OurBranchID=@OurBranchID) and (InstrumentAccountID = @AccountID)            
   and (PaidScrollNo = @SerialNo) and (PaidMode = 'IW')            
   and (convert(varchar(10),PaidDate,101) = convert(varchar(10), @wDate,101))            
 */            
               
  END              
              
 Return(0)              
              
SET QUOTED_IDENTIFIER  OFF    SET ANSI_NULLS  ON