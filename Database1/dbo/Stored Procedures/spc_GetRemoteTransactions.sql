﻿CREATE PROCEDURE [dbo].[spc_GetRemoteTransactions]    
(      
 @OurBranchID varchar(30),
 @DbSupervisionLimit money=0,      
 @ScrollNo money=0      
)      
      
AS      
 SET NOCOUNT ON      
      
  IF @ScrollNo <> 0       
   BEGIN      
         IF (SELECT COUNT(*) FROM t_CashTransactionModel WHERE OurBranchID = @OurBranchID AND ScrollNo = @ScrollNo AND Supervision = 'P'      
                            And ((IsLocalCurrency = 1 and abs(Amount) <= @DbSupervisionLimit)       
                       OR (IsLocalCurrency = 0 and abs(ForeignAmount) <= @DbSupervisionLimit))) > 0       
            BEGIN      
               SELECT '1' as RetStatus, a.ScrollNo, a.AccountID, a.AccountType, a.ChequeID, a.Amount, a.ForeignAmount,a.Supervision,       
              a.RemoteDescription, a.IsLocalCurrency,a.OperatorID, b.Name, b.Limit,b.ClearBalance,      
              b.Effects ,A.CurrencyID, a.ProductID, a.AppWHTax, a.WHTaxAmount, a.WHTaxMode , a.WHTaxScrollNo,a.AppChg,      
       a.Chgmode,a.ChgScrollno,a.Refno      
      
               FROM t_CashTransactionModel  a, t_AccountBalance b      
               Where (a.OurBranchID = b.OurBranchID)  AND (a.AccountID = b.AccountID)        
                   And  (a.OurBranchID = @OurBranchID  AND ScrollNo = @ScrollNo )      
                   And ((IsLocalCurrency = 1 and abs(Amount) <= @DbSupervisionLimit)       
                       OR (IsLocalCurrency = 0 and abs(ForeignAmount) <= @DbSupervisionLimit))      
                   And  (Supervision = 'P')      
      
               Order By scrollno desc      
            END      
      
         ELSE      
            BEGIN      
               SELECT '0' as RetStatus      
               RETURN      
            END      
   END      
  ELSE      
   BEGIN         
         IF (SELECT COUNT(*) FROM t_CashTransactionModel WHERE OurBranchID = @OurBranchID AND Supervision = 'P'      
                            And ((IsLocalCurrency = 1 and abs(Amount) <= @DbSupervisionLimit)       
                       OR (IsLocalCurrency = 0 and abs(ForeignAmount) <= @DbSupervisionLimit))) > 0       
            BEGIN      
               SELECT '1' as RetStatus, a.ScrollNo, a.AccountID, a.AccountType, a.ChequeID, a.Amount, a.ForeignAmount,a.Supervision,       
              a.RemoteDescription, a.IsLocalCurrency,a.OperatorID, b.Name, b.Limit,b.ClearBalance,      
              b.Effects, A.CurrencyID, a.ProductID, a.AppWHTax, a.WHTaxAmount, a.WHTaxMode , a.WHTaxScrollNo,a.AppChg,      
       a.Chgmode,a.ChgScrollno,a.Refno      
      
               FROM t_CashTransactionModel  a, t_AccountBalance b      
               Where (a.OurBranchID = b.OurBranchID)  AND (a.AccountID = b.AccountID)        
                   And  (a.OurBranchID = @OurBranchID)      
                   And ((IsLocalCurrency = 1 and abs(Amount) <= @DbSupervisionLimit)       
                       OR (IsLocalCurrency = 0 and abs(ForeignAmount) <= @DbSupervisionLimit))      
                   And  (Supervision = 'P')      
      
               Order By scrollno desc      
            END      
        
         ELSE      
            BEGIN      
               SELECT '0' as RetStatus      
               RETURN      
            END      
   END