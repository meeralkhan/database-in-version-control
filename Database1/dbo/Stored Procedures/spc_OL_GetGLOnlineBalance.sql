﻿CREATE PROCEDURE [dbo].[spc_OL_GetGLOnlineBalance]      
(              
 @OurBranchID varchar(30),              
 @AccountID varchar(30),              
 @TrxType varchar(5)='',              
 @OperatorID varchar(30)='',               
 @ModuleType char(1)='',              
 @GLRealTime char(1)='0'        
)              
              
AS              
 SET NOCOUNT ON              
              
 DECLARE @AccountName as Varchar(75)              
 DECLARE @AccountClass as Char(1)              
 DECLARE @CurrencyID as Varchar(4)              
 DECLARE @CurrencyName as Varchar(50)              
 DECLARE @ClearBalance as Money              
 DECLARE @ShadowBalance as Money              
              
 DECLARE @DayDepositBal as money                
 DECLARE @DayDrawableBal as money                
 DECLARE @DayDepositBalUtilize as money                
 DECLARE @DayDrawableBalUtilize as money                
       
 DECLARE @MeanRate as decimal(18,6)          
              
 DECLARE @IsPosting as Bit              
 DECLARE @LocalCurrency as Varchar(4)              
 DECLARE @IsPostAllGLAccounts as Bit              
       
 SELECT @LocalCurrency =UPPER(LTRIM(RTRIM(LocalCurrency))) From t_GlobalVariables WHERE OurBranchID = @OurBranchID    
 SELECT @IsPostAllGLAccounts = IsGLRestrict From t_OL_GlobalVariables WHERE OurBranchID = @OurBranchID    
       
 SELECT @TrxType = 'B', @DayDepositBal = 999999999999.00, @DayDrawableBal = 999999999999.00      
       
 IF @IsPostALLGLAccounts = 1      
 BEGIN      
        
  SET @TrxType = 'N'      
        
  IF (Select Count(AccountID) From t_OL_Accounts WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND AccountType = 'G' ) > 0      
   BEGIN              
          
    SELECT @TrxType = TrxType, @DayDepositBal=DayDepositBal, @DayDrawableBal = DayDrawableBal      
    FROM t_OL_Accounts      
    WHERE OurBranchID = @OurBranchID AND ACcountID = @AccountID AND AccountType = 'G'      
          
   END      
  ELSE      
   BEGIN      
    SELECT '10' AS RetStatus --OL Trx. not allowed      
    RETURN(0)      
   END      
         
 END      
       
 IF (Select Count(AccountID) From t_GL WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID) > 0               
  BEGIN              
         
   SELECT @AccountName=Description, @ShadowBalance=ShadowBalance, @ClearBalance = CASE CurrencyID WHEN @LocalCurrency THEN Balance ELSE      
   ForeignBalance END, @CurrencyID= UPPER(CurrencyID), @AccountClass = AccountClass, @IsPosting = IsPosting      
   FROM t_GL              
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID      
         
   if @GLRealTime = '1'        
   begin        
     if @LocalCurrency = @CurrencyID        
     begin        
       exec @ClearBalance = fn_GetGLRealTimeBalance @OurBranchID, @AccountID        
     end        
     else        
     begin        
       exec @ClearBalance = fn_GetGLRealTimeFBalance @OurBranchID, @AccountID        
     end        
   end        
         
   IF @AccountClass <> 'P'              
   BEGIN              
    SELECT '6'  AS RetStatus --Account Class Not Deifne Posting               
    RETURN(1)                     
   END              
         
   IF @IsPosting = 0                 
   BEGIN              
    SELECT '7'  AS RetStatus --Posting Not Allowed              
    RETURN(1)                     
   END              
         
   IF (SELECT Count(*) From t_Products              
    WHERE OurBranchID = @OurBranchID AND (GLControl=@AccountID OR GLDebitBalance = @AccountID))>0               
   BEGIN              
    SELECT '9'  AS RetStatus --Posting Not Allowed IN Control Account              
    RETURN(1)                     
   END              
         
   SELECT @CurrencyName = Description, @MeanRate =IsNull(MeanRate,0) FROM t_Currencies              
   WHERE OurBranchID=@OurBranchID AND CurrencyID=@CurrencyID              
         
   SELECT @DayDepositBalUtilize = IsNull(Sum(Amount),0) FROM t_OnLineCashTransaction      
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND TrxType = 'C' AND Not DescriptionID Like 'TR%'      
         
   SELECT @DayDrawableBalUtilize = IsNull(Sum(Amount),0) FROM t_OnLineCashTransaction      
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND TrxType = 'D' AND Not DescriptionID Like 'TR%'      
         
   SELECT '1' as RetStatus , @ClearBalance as ClearBalance,                
     @ShadowBalance as ShadowBalance,@AccountName as Name,      
     'GL' ProductID, 'General Ledger' ProductName, '' Reminder,      
     0 Effects, 0 FreezeAmount, 0 Limit, 0 ProductMinBalance, 0 AverageBalance,      
     @CurrencyID as CurrencyID,              
     @CurrencyName as CurrencyName,                   
     @MeanRate as MeanRate, @TrxType TrxType,      
     @DayDepositBal DayDepositBal, @DayDrawableBal DayDrawableBal,      
     @DayDepositBalUtilize DayDepositBalUtilize, @DayDrawableBalUtilize DayDrawableBalUtilize      
           
  END              
 ELSE              
  BEGIN             
   SELECT 0 AS RetStatus --Account Does Not Exists              
   RETURN(0)              
  END