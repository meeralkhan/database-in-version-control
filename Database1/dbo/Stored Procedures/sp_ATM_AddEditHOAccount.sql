﻿CREATE PROCEDURE [dbo].[sp_ATM_AddEditHOAccount]
	(
		@OurBranchID varchar(30),
		@HOAccountID varchar(30), 
		@HOAccountTitle varchar(100),  
		@HOCurrencyID varchar(30), 
		@HOCurrencyName varchar(100), 
		@HOProductID varchar(30), 
		@NewRecord bit=1
	)
AS
	IF cast(@NewRecord as bit) = 1
		BEGIN
			INSERT INTO t_ATM_HOAccount(OurBranchID, HOAccountID, HOAccountTitle, HOCurrencyID, HOCurrencyName, HOProductID) VALUES (@OurBranchID, @HOAccountID, @HOAccountTitle, @HOCurrencyID, @HOCurrencyName, @HOProductID)
		END
	ELSE
		BEGIN
			UPDATE t_ATM_HOAccount SET OurBranchID=@OurBranchID, HOAccountID=@HOAccountID, HOAccountTitle=@HOAccountTitle, HOCurrencyID=@HOCurrencyID, HOCurrencyName=@HOCurrencyName, HOProductID=@HOProductID
		END