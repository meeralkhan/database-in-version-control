﻿create   PROCEDURE [dbo].[spc_GetCustomerStatement]                                                
(                                                  
 @OurBranchID  varchar(30),                                                  
 @AccountID varchar(30),                                                  
 @sDate Datetime,                                                  
 @eDate Datetime                                                  
)                                                  
                                                  
AS                                                  
                                                  
  SET NOCOUNT ON                                                  
                                                  
   DECLARE @LocalCurrency as Varchar(4)                                                     
   DECLARE @OpeningBalance as money                                                  
                                                  
   DECLARE @crOpBal as money                                                  
                                                  
                                                  
   DECLARE @drOpBal as money                                                  
   DECLARE @tDate as Varchar(15)                                                  
   DECLARE @WorkingDate as datetime                                                  
                                                  
   SELECT @WorkingDate = WorkingDate From t_Last                                                  
   WHERE OurBranchID = @OurBranchID                                                  
                                 
   if @eDate > @WorkingDate                              
   begin                              
      set @eDate = @WorkingDate                              
   end                              
                                 
   SET @tDate = cast(Month(@sDate) as Varchar(2)) + '/01/' + cast(Year(@sDate)  as Varchar(4))                                                  
                                                  
 SET @OpeningBalance = 0                                                  
 SET @crOpBal = 0                                                  
 SET @drOpBal = 0                                                  
                                                  
   SELECT @OpeningBalance = IsNull(ClearBalance,0)                                                   
   FROM t_AccountMonthBalances                                                  
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                                                   
   AND Month = @tDate                                                  
                                                 
   SELECT @crOpBal = IsNull(SUM(Amount),0) From t_Transactions                                                  
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                                                   
         AND TrxType = 'C' AND AccountType = 'C' AND [Status] <> 'R'                                                  
    --     AND convert(varchar(10) , wDate,101) >= @tDate AND convert(varchar(10) , wDate,101) < @sDate                                                  
  AND wDate >= @tDate AND wDate < @sDate                                                  
                                                  
   SELECT @drOpBal = IsNull(SUM(Amount),0) From t_Transactions                                                  
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                                                   
         AND TrxType = 'D' AND AccountType = 'C' AND [Status] <> 'R'                                                  
         --AND convert(varchar(10) , wDate,101) >= @tDate AND convert(varchar(10) , wDate,101) < @sDate                                                  
  AND wDate >= @tDate AND wDate < @sDate                                                  
                            
   SET @OpeningBalance = @OpeningBalance + @crOpBal - @drOpbal                                   
                                                  
   IF @WorkingDate = @eDate                                                   
      BEGIN                                                  
                
         select ChannelRefID,SupervisorID,OperatorID,0.0 as ForeignAmount,0.0 as ExchangeRate,wDate, TrxTimeStamp, ValueDate, DescriptionID, Description, ChequeID, ChequeDate, TrxType, Amount, TableStr, ScrollNo, DocumentType, Remarks from (                          
                                   
         SELECT '' ChannelRefID,'' SupervisorID,''OperatorID,0.0 as ForeignAmount,0.0 as ExchangeRate,'01/01/1900' as wDate, '01/01/1900' as TrxTimeStamp, @sDate as ValueDate,'OP' AS DescriptionID,'Opening Balance' as  Description,'' as ChequeID,   
   '01/01/1900' AS ChequeDate, '' as TrxType,              
            
         IsNull(@OpeningBalance,0) as Amount , 'OP' As TableStr, 0 as ScrollNo, '' as DocumentType, '' as Remarks              
                                                   
         UNION ALL               
                                                  
         SELECT ChannelRefID,SupervisorID,OperatorID,0.0 as ForeignAmount,0.0 as ExchangeRate,wDate, TrxTimeStamp,ValueDate, DescriptionID, Description, ChequeID, ChequeDate, TrxType, Amount, 'H' As TableStr, ScrollNo, DocumentType, Remarks              
         FROM t_Transactions                                                  
         WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                               
            AND AccountType = 'C' AND [Status] <> 'R'                                  
            AND convert(varchar(10) , wDate,101) >= @sDate AND convert(varchar(10) , wDate,101) <= @eDate                      
                                                        
/*                                                  
--t_CashTransactionModel                                                  
         SELECT wDate,ValueDate , Description, ChequeID,TrxType, Amount                                                  
                  ,'C' As TableStr, ScrollNo,'C' as DocumentType                                          
         FROM t_CashTransactionModel                                                  
         WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                                                 
               AND Supervision IN ('C')  AND AccountType ='C'                                                  
                                                  
         UNION ALL                                                  
                        
--t_TransferTransactionModel                                              
         SELECT wDate,ValueDate , Description, ChequeID,TrxType, Amount                                                  
                  ,'T' As TableStr, ScrollNo,'T' as DocumentType                                                  
         FROM t_TransferTransactionModel                                                  
         WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                                                        
               AND Supervision IN ('C','#')  AND AccountType ='C'                                                                 
                                                        
        UNION ALL                                           
                        
                        
--t_InWardClearing                                                  
         SELECT wDate,ValueDate , Description, ChequeID,TransactionType as TrxType, Amount,                        
         'I' As TableStr,SerialNo as  ScrollNo,'ID' as DocumentType                                                  
         FROM t_InWardClearing                                                  
         WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID    
               AND Status <>'R'  AND AccountType ='C'                                                  
                                                     
                          
         UNION ALL                                                  
                    
--t_AllModelTransaction                                                  
                                          
          SELECT wDate,ValueDate , Description, ChequeID,TrxType, Amount                                                  
                   ,'A' As TableStr, ScrollNo, 'O' AS DocumentType                                                  
          FROM t_AllModelTransaction                                                  
      WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                                                         
                AND AccountType ='C'                                                  
                                              
              
--          UNION ALL                                                  
            --t_ATM_CashTransaction                                                  
         SELECT wDate,ValueDate , Description,'' as ChequeID,TrxType, Amount                                                  
                  ,'ATM' As TableStr, ScrollNo,'AT' as DocumentType                                            
         FROM t_ATM_CashTransaction                                                  
         WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                                                         
               AND Supervision = 'C' AND AccountType ='C'                                                  
                                 
         UNION ALL                                                  
                                                          
--'t_ATM_TransferTransaction                                                  
         SELECT wDate,ValueDate , Description,'' as ChequeID,TrxType, Amount                                                  
                  ,'ATMT' As TableStr, ScrollNo,'AT' as DocumentType                               
         FROM t_ATM_TransferTransaction                                                  
         WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                                                         
               AND Supervision = 'C' AND AccountType ='C'                                                  
                                                  
         UNION ALL                                                  
                                                          
--'t_OnLine_CashTransaction                                                  
         SELECT wDate,ValueDate , Description, ChequeID,TrxType, Amount                                                  
 ,'OL' As TableStr, ScrollNo,'OL' as DocumentType                                                  
         FROM t_OnLineCashTransaction                                                  
         WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                                                         
               AND Supervision <> 'R' AND AccountType ='C' AND DescriptionID IN ('OT1','OT2')                                                  
                        
         UNION ALL                                                  
                                                          
--'t_OL_LocalTransferTransaction                                                  
         SELECT wDate,ValueDate , Description, ChequeID,TrxType, Amount                                                  
                  ,'OT' As TableStr, ScrollNo,'OT' as DocumentType                                                  
         FROM t_OnLineCashTransaction                                                  
         WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                                      
               AND Supervision <> 'R' AND AccountType ='C' AND DescriptionID IN ('TR0','TR1')                                               
                    
*/                                   
                
UNION ALL                
                 
--'t_InternetTransaction                 
 SELECT '' ChannelRefID,'' SupervisorID,'' OperatorID,0.0 as ForeignAmount,0.0 as ExchangeRate,wDate, wDate AS TrxTimeStamp,ValueDate , DescriptionID, Description, 'V', '01/01/1900',TrxType, Amount, 'IT' As TableStr, ScrollNo,'IT' as DocumentType, Remarks              
 FROM t_InternetTransaction                 
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                 
AND AccountType IN ('B','C') AND DescriptionID IN ('IT1','IT2','BP1','BP2')                 
                   
         ) a order by TrxTimeStamp asc                      
      END                                                  
                                                  
   ELSE -- If Statement not in workingdate                                                  
                                                  
      BEGIN          
                                   
         select ChannelRefID,SupervisorID,OperatorID,0.0 as ForeignAmount,0.0 as ExchangeRate,wDate, TrxTimeStamp, ValueDate, DescriptionID, Description, ChequeID, ChequeDate, TrxType, Amount, TableStr, ScrollNo, DocumentType, Remarks from (                          
                                   
         SELECT '' ChannelRefID,'' SupervisorID,'' OperatorID,0.0 as ForeignAmount,0.0 as ExchangeRate,'01/01/1900' as wDate, '01/01/1900' as TrxTimeStamp, @sDate as ValueDate, 'OP' AS DescriptionID,'Opening Balance' as  Description,'' as ChequeID,   
   '01/01/1900' AS ChequeDate, '' as TrxType,    
   IsNull(@OpeningBalance,0) as Amount         
             
                                          
               ,'OP' As TableStr, 0 as ScrollNo, '' as DocumentType, '' as Remarks              
                                                    
         UNION ALL                                                  
                                                  
         SELECT ChannelRefID,SupervisorID,OperatorID,0.0 as ForeignAmount,0.0 as ExchangeRate,wDate,TrxTimeStamp,ValueDate, DescriptionID, Description, ChequeID, ChequeDate,TrxType, Amount                                            
               ,'H' As TableStr, ScrollNo, DocumentType, Remarks              
         FROM t_Transactions                                                  
         WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID                                                   
            AND AccountType = 'C' AND [Status] <> 'R'                                                  
            AND convert(varchar(10) , wDate,101) >= @sDate AND convert(varchar(10) , wDate,101) <= @eDate                                                  
                                   
         ) a order by TrxTimeStamp asc                          
                                   
      END