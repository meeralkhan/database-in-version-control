﻿CREATE PROCEDURE [dbo].[spc_AddEditClearingReturn_New]  
   
 @ScrollNo money=0,                
 @SerialNo int=0,                
 @OurBranchID varchar(30),                
 @AccountID varchar(30),                
 @AccountType Char(1),                
 @ValueDate datetime=null,                
 @wDate datetime=null,                
 @TrxType Char(1)='D',              
 @ChequeID varchar(30)='',                
 @ChequeDate DateTime=null,                
 @Amount money=0,                
 @ForeignAmount Money=0,                
 @ExchangeRate Money=0,                
 @DescriptionID varchar(30)='',                
 @Description varchar(255)='',                
 @BankCode varchar(30),                
 @BranchCode varchar(30),                
 @TrxPrinted bit=1,                
 @ProfitOrLoss money=0,                
 @GlID varchar(30)='',                
 @Supervision char(1)='C',                
 @IsSupervision bit=0,                
 @OperatorID varchar(30),                
 @SupervisorID varchar(30)='',                
 @AccountName varchar(100)='',                
 @RemotePass bit=0,                
 @ClearBalance money=0,                
 @OutDp Money=0,                
 @PassBy varchar(30)='',                
 @RemoteDescription varchar(100)='',                
 @EffectiveBalance money=0,                
 @Status char(1)='',                
 @IsLocalCurrency int=1,                
 @DebitPostingLimit money=0,                
 @CreditPostingLimit money=0,                
 @IsOverDraft bit=1,                
 @TrxMode char(1)='',                
 @RefNo varchar(100)='',                 
 @Reason varchar(255)='',                
 @Charges money=0,                
 @ApplyCharges bit = 0,  
 @ChgScrollNo int = 0,
 @ChargeAmt money = 0,
 @NewRecord bit = 1  
                
 AS                
                
 SET NOCOUNT ON                 
 DECLARE @LocalCurrency as varchar(30),                
  @ProductID varchar(30),                
  @CurrencyID varchar(30),                
  @MaxScrollNo as money,                
  @MaxSerialNo as money,                
  @MOACID varchar(30),                
  @MODescription varchar(100),                
  @MOCurrencyID varchar(30),                 
  @TrxMethod char(1)  
   
 SELECT @Supervision = 'C'  
   
 SELECT @LocalCurrency=LocalCurrency From t_GlobalVariables where OurBranchID = @OurBranchID  
   
 IF @AccountType = 'C'  
   select @ProductID = ProductID , @CurrencyID = CurrencyID from t_Account  
   where OurBranchID = @OurBranchID and AccountID = @AccountID  
 ELSE  
   select @ProductID = 'GL' , @CurrencyID = CurrencyID from t_GL  
   where OurBranchID = @OurBranchID and AccountID = @AccountID  
   
 if UPPER(@LocalCurrency) = UPPER(@CurrencyID)  
 begin  
      
    set @TrxMethod = 'L'  
      
    select @MOACID = ISNULL(g.AccountID,''), @MODescription = ISNULL(g.Description,''), @MOCurrencyID = ISNULL(g.CurrencyID,'')  
    from t_GLParameters gp  
    inner join t_GL g ON gp.OurBranchID = g.OurBranchID and gp.AccountID = g.AccountID  
    where gp.OurBranchID = @OurBranchID and gp.SerialID = '2'  
      
 end  
 else  
 begin  
      
    set @TrxMethod = 'A'  
      
    select @MOACID = ISNULL(g.AccountID,''), @MODescription = ISNULL(g.Description,''), @MOCurrencyID = ISNULL(g.CurrencyID,'')  
    from t_CurrenciesAccount c  
    inner join t_GLParameters gp ON c.OurBranchID = gp.OurBranchID and c.HOSerialID = gp.SerialID  
    inner join t_GL g ON gp.OurBranchID = g.OurBranchID and gp.AccountID = g.AccountID  
    where c.OurBranchID = @OurBranchID and c.CurrencyID = @CurrencyID  
      
 end  
   
 IF isnull(@MOACID,'') = ''  
 begin  
   select '1' as ReturnStatus --Head Office Account Not Defined  
   return  
 end  
   
 BEGIN                
  SET @MaxScrollNo = 0                
  SET @MaxSerialNo = 0                
  
  UPDATE t_Clearing SET [Status] = 'R'
  WHERE (OurBranchID = @OurBranchID) and (RefNo = @RefNo) and (AccountID = @AccountID)
  
  Insert Into t_OutwardReturns                
  (ScrollNo,SerialNo,OurBranchID,RefNo,AccountID,AccountName,ProductID,CurrencyID,AccountType,                
  ValueDate,wDate,TransactionType,ChequeID,ChequeDate, Amount, ForeignAmount,ExchangeRate,                
  BankID,BranchID,Reason,Charges,OperatorID)                
  Values                
  (@ScrollNo,@SerialNo,@OurBranchID,@RefNo,@AccountID,@AccountName,@ProductID,@CurrencyID,@AccountType,                
  @ValueDate,@wDate,@TrxType,@ChequeID,@ChequeDate, @Amount, @ForeignAmount,@ExchangeRate,                
  @BankCode,@BranchCode,@Reason,@Charges,@OperatorID)                
                 
  IF @AccountType = 'C'                
   BEGIN                
    IF @LocalCurrency = @CurrencyID                
     Update t_AccountBalance                 
     Set Effects =  Effects - @Amount --, ClearBalance = ClearBalance + @Amount                
     Where  OurBranchID = @OurBranchID and AccountID = @AccountID                
    ELSE                
     Update t_AccountBalance                
     Set Effects =  Effects - @ForeignAmount, LocalEffects = LocalEffects - @Amount                
     --,ClearBalance = ClearBalance + @ForeignAmount, LocalClearBalance = LocalClearBalance + @Amount                
     Where  OurBranchID = @OurBranchID and AccountID = @AccountID                
   END                
                
/* Debit Transfer Entry For The Given Acount */                
                
  select @MaxScrollNo = isnull ( Max(ScrollNo) , 0 ) + 1 from  t_TransferTransactionModel                
  where OurBranchID = @OurBranchID  
    
  set @MaxSerialNo = 1  
    
  Insert Into t_TransferTransactionModel  
  (ScrollNo,SerialNo,OurBranchID,AccountID,AccountName,ProductID,CurrencyID,AccountType,ValueDate,                
  wDate,TrxType,ChequeID,ChequeDate, Amount, ForeignAmount,ExchangeRate,DescriptionID,Description,                
  BankCode,BranchCode,TrxPrinted,ProfitorLoss,Glid,Issupervision,Supervision,RemoteDescription,OperatorID,                
  SupervisorID,Refno,IsLocalCurrency)                
  Values                
  (@MaxScrollNo,@MaxSerialNo,@OurBranchID,@AccountID,@AccountName,@ProductID,@CurrencyID,                
  @AccountType,@ValueDate,@wDate,@TrxType,@ChequeID,@ChequeDate, @Amount, @ForeignAmount,                
  @ExchangeRate,@DescriptionID,@Description,@BankCode,@BranchCode,@TrxPrinted,@ProfitorLoss,@Glid,                
  @IsSupervision, @Supervision, @RemoteDescription,@OperatorID,@SupervisorID,'',@IsLocalCurrency)  
    
  declare @GLControl varchar(30)  
  declare @VoucherID int  
    
  select @VoucherID = ISNULL(MAX(VoucherID),0) + 1 from t_GLTransactions where OurBranchID = @OurBranchID  
  select @GLControl = ISNULL(GLControl, '') from t_Products where OurBranchID = @OurBranchID and ProductID = @ProductID  
    
  -- Debit Customer  
  insert into t_Transactions (OurBranchID, ScrollNo, SerialNo, RefNo, wDate, AccountType, DocumentType, AccountID, AccountName,  
  ProductID, CurrencyID, ValueDate, TrxType, ChequeID, ChequeDate, Amount, ForeignAmount, ExchangeRate, ProfitLoss, MeanRate,  
  DescriptionID, [Description], BankCode, BranchCode, TrxPrinted, [Status], IsLocalCurrency, OperatorID,  
  SupervisorID, AdditionalData, IsMainTrx, DocType, GLVoucherID)  
    
  VALUES (@OurBranchID,@MaxScrollNo,@MaxSerialNo,'',@wDate,@AccountType,'T',@AccountID,@AccountName,  
  @ProductID,@CurrencyID,@ValueDate,@TrxType,@ChequeID,@ChequeDate, @Amount, @ForeignAmount, @ExchangeRate, @ProfitorLoss, @ExchangeRate,  
  @DescriptionID, @Description, @BankCode, @BranchCode, @TrxPrinted, 'C', @IsLocalCurrency, @OperatorID,  
  @SupervisorID, '', '1', 'T', @VoucherID)  
    
  -- Debit GL Ctrl  
  insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],      
  CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,      
  Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType)  
    
  values (@OurBranchID, @GLControl, @VoucherID, '1', @wDate, @ValueDate, @DescriptionID, @Description,  
  @CurrencyID, @Amount, @ForeignAmount, @ExchangeRate, 0, 'Transfer', @TrxMethod, @OperatorID, @SupervisorID,  
  '', @MaxScrollNo, @MaxSerialNo, '', '0', 'T')  
    
/* Credit Transfer Entry For The Head Office Acount */                
    
  SET @VoucherID = @VoucherID + 1  
  SET @MaxSerialNo = @MaxSerialNo + 1  
    
  Insert Into t_TransferTransactionModel                
  (ScrollNo,SerialNo,OurBranchID,AccountID,AccountName,ProductID,CurrencyID,AccountType,ValueDate,                
  wDate,TrxType,ChequeID,ChequeDate, Amount, ForeignAmount,ExchangeRate,DescriptionID,Description,                
  BankCode,BranchCode,TrxPrinted,ProfitorLoss,Glid,Issupervision,Supervision,RemoteDescription,OperatorID,                
  SupervisorID,Refno,IsLocalCurrency)                
  Values                
  (@MaxScrollNo,@MaxSerialNo,@OurBranchID,@MOACID,@MODescription,'GL',@MOCurrencyID,                
  'G',@ValueDate,@wDate,'C', 'V' ,@ChequeDate, @Amount, @ForeignAmount,                
  @ExchangeRate, 'ORH', 'Outward Return To Head Office Against Cheque ' + @ChequeID,@BankCode,@BranchCode,                
  @TrxPrinted,@ProfitorLoss,@Glid,@IsSupervision, @Supervision, @RemoteDescription,@OperatorID,@SupervisorID,                
  '',@IsLocalCurrency)  
    
  -- Credit HO Account  
  insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],  
  CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,  
  Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType)  
    
  values (@OurBranchID, @MOACID, @VoucherID, '1', @wDate, @ValueDate, 'ORH', 'Outward Return To Head Office Against Cheque ' + @ChequeID,  
  @CurrencyID, @Amount, @ForeignAmount, @ExchangeRate, 1, 'Transfer', @TrxMethod, @OperatorID, @SupervisorID,  
  '', @MaxScrollNo, @MaxSerialNo, '', '1', 'T')  
   
 END                
   
 Select '0' as ReturnStatus, @MaxScrollNo as ScrollNo