﻿CREATE PROCEDURE [dbo].[spc_UpdateLastTrxDate_All]
AS
SET NOCOUNT ON

UPDATE t_AccountBalance SET
NoOfCrTrx = 0, NoOfDrTrx = 0, AmountOfCrTrx = 0, AmountOfDrTrx = 0,
NoOfCWTrxATM = 0, NoOfBITrxATM = 0, AmountOfCWTrxATM = 0, AmountOfBITrxATM = 0
where (NoOfCrTrx>0 OR NoOfDrTrx>0 OR AmountOfCrTrx>0 OR AmountOfDrTrx>0 OR
NoOfCWTrxATM>0 OR NoOfBITrxATM>0 OR AmountOfCWTrxATM>0 OR AmountOfBITrxATM>0)

select 'Ok' AS RetStatus