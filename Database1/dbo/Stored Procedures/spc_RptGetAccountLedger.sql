﻿CREATE Procedure [dbo].[spc_RptGetAccountLedger] (
	@OurBranchID varchar(30),  
	@AccountFrom varchar(30),    
	@FromDate datetime,          
	@ToDate datetime
)
AS
BEGIN

SET NOCOUNT ON
  
	SELECT distinct isnull(dval.AccountID,cval.AccountID) as AccountID, isnull(dval.[Description],cval.[Description]) as [Description], 
	isnull(dval.Balance,0) as dAmount, isnull(cval.Balance,0) as cAmount, isnull(dval.wdate,cval.wdate) as wdate
	FROM 
	(          
		SELECT distinct tg.AccountID, tg.[Description], SUM(tt.Amount) as Balance, wdate=tt.date
		FROM t_GL as tg, t_GLTransactions tt 
		WHERE tg.OurBranchID=tt.OurBranchID AND tg.OurBranchID=@OurBranchID AND tg.AccountID = @AccountFrom 
		AND tg.AccountID=tt.AccountID AND convert(varchar(10), tt.[date],120) >= @FromDate AND convert(varchar(10), tt.[date],120) <= @ToDate
		AND tt.IsCredit=0 
		GROUP BY tg.OurBranchID, tg.AccountID, tg.PrintID, tg.[Description], tt.[date]
	) as dval

	full outer join 
	( 
		SELECT distinct tg.AccountID, tg.[Description], SUM(tt.Amount) as Balance, wdate=tt.[date]
		FROM t_GL as tg, t_GLTransactions tt 
		WHERE tg.OurBranchID=tt.OurBranchID AND tg.OurBranchID=@OurBranchID AND tg.AccountID = @AccountFrom 
		AND tg.AccountID=tt.AccountID AND convert(varchar(10), tt.[date],120) >= @FromDate and convert(varchar(10), tt.[date],120) <= @ToDate
		AND IsCredit = 1 
		GROUP BY tg.OurBranchID, tg.AccountID, tg.PrintID, tg.[Description], tt.[date]
	) as Cval

	on

	cval.AccountID=dval.AccountID and cval.wdate=dval.wdate 
	ORDER BY wdate          
END