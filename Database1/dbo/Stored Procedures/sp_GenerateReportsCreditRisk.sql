﻿CREATE PROCEDURE [dbo].[sp_GenerateReportsCreditRisk]
(
@CIF Varchar(15),
@OurBrID VARCHAR(30),
@sdate Varchar(50),
@edate Varchar(50),
@GroupCIF varchar(1),
@RptTime DATETIME				 
)

AS
BEGIN
	Set nocount on

	DECLARE @ClientID varchar(15)  DECLARE @OurBranchID VARCHAR(30)	  DECLARE @cCount as decimal(24,0)     declare @sCount as decimal(24,0)		DECLARE @Counter as INT		DECLARE @ClearBalance AS MONEY
	DECLARE @ssDate datetime 	DECLARE @eeDate datetime		DECLARE @GroupID VARCHAR(50)	DECLARE @AccountID VARCHAR(30)	DECLARE @DueDate AS DATETIME	DECLARE @DealID AS VARCHAR	DECLARE @SerialID AS VARCHAR
    DECLARE @Flag AS BIT	DECLARE @DPD AS NUMERIC		DECLARE @CurrentDPD AS NUMERIC		DECLARE @PeakDPD AS NUMERIC		DECLARE @Bal AS MONEY	DECLARE @ABHBal AS MONEY	DECLARE @SettleDate AS DATETIME	DECLARE @Limit AS MONEY		DECLARE @MaxClearBalance AS MONEY
	DECLARE @SHName AS VARCHAR(50)		DECLARE @SHCountry AS VARCHAR(30)		DECLARE @ABHBal1 AS MONEY	DECLARE @ABHBalOld AS MONEY DECLARE @ABHBal2 AS MONEY	DECLARE @ABHCount as INT
	DECLARE @PeakUtilization12M AS MONEY	DECLARE @PeakUtilizationYTD AS MONEY	DECLARE @PeakUtilizationDTD AS MONEY	DECLARE @TotDPD AS NUMERIC	DECLARE @SHParentID as decimal(24,0)
	DECLARE @AvgDailyUtilization12M AS MONEY	DECLARE @AvgDailyUtilizationYTD AS MONEY	DECLARE @AvgDailyUtilizationDTD AS MONEY					DECLARE @SHID as decimal(24,0)
	DECLARE @OverDraftClassID VARCHAR(30)		DECLARE @Classification VARCHAR(30)			DECLARE @SubClassification VARCHAR(30)						DECLARE @SHOwnerShip AS DECIMAL(22,2)
	DECLARE @RiskCode VARCHAR(30)				DECLARE @RiskDescription VARCHAR(100)		DECLARE @SHcCount as decimal(24,0)     declare @SHsCount as decimal(24,0)	
	DECLARE @YTDDate DATETIME	DECLARE @M12TDDate DATETIME		DECLARE @ccCount as decimal(24,0)     declare @ssCount as decimal(24,0)		DECLARE @SanctionDate DATETIME
	DECLARE	@PeakDTDABHBal AS MONEY		DECLARE @PeakDTDBal AS MONEY				DECLARE @PeakDTDCounter AS DECIMAL(24,0)		DECLARE @LoanType VARCHAR(30)
	DECLARE	@Peak12MABHBal AS MONEY		DECLARE @Peak12MBal AS MONEY				DECLARE @Peak12MCounter AS DECIMAL(24,0)		declare @PDCount as decimal(24,0)
	DECLARE	@PeakYTDABHBal AS MONEY		DECLARE @PeakYTDBal AS MONEY				DECLARE @PeakYTDCounter AS DECIMAL(24,0)		DECLARE @OLDDPD AS NUMERIC
	DECLARE @NoOfReschedule AS INT		DECLARE @LastRescheduleDate AS DATETIME		DECLARE @Provisioning as MONEY		

	SET @M12TDDate = convert(varchar(10), DATEADD(d,1,DATEADD(yy,-1,@edate)), 120)
	SET @YTDDate = convert(varchar(10), DATEADD(yy, DATEDIFF(yy, 0, @edate), 0), 120)

	SET @ssDate = convert(varchar(10), @sdate, 120)
	SET @eeDate = convert(varchar(10), @edate, 120)
	SET @ClientID = @CIF
	SET @OurBranchID = @OurBrID

	SELECT @GroupID=ISNULL(GroupID,'') FROM dbo.t_Customer WHERE ClientID = @ClientID AND OurBranchID = @OurBranchID

	IF @GroupCIF = '1'
	BEGIN

		INSERT INTO TmpRptRiskCreditDetail Select c.ClientID , c.Name ,'Loan' AS "LoanType",mg.GroupID,mg.GroupName,r.FullName "RelationManager",a.ProductID,a.AccountID, CAST(d.DealID AS VARCHAR(20)) DealID, 
		CAST(p.InstNo AS VARCHAR(20)) SerialID, l.Limit, p.InstAmount, p.DueDate , DATEDIFF(d,p.DueDate,@eeDate) DPD,DATEDIFF(d,p.DueDate,@eeDate) CurrentDPD,DATEDIFF(d,p.DueDate,@eeDate) PeakDPD, 
		p.PayDate, CAST(p.IsPosted AS VARCHAR(20)) IsPosted,
		(Select Isnull(Sum(-InstAmount),0) from t_Adv_PaymentSchedule Where AccountID = d.AccountID AND DealID = d.DealID AND isposted = 0 AND DueDate <= @eeDate) + 
		(Select Top 1 Isnull(-Balance,0) from t_Adv_PaymentSchedule Where AccountID = d.AccountID AND DealID = d.DealID AND DueDate <= @eeDate Order by DueDate desc, InstNo desc) ClearBalance, 
		(Select Isnull(Sum(-InstAmount),0) from t_Adv_PaymentSchedule Where AccountID = d.AccountID AND DealID = d.DealID AND isposted = 0 AND DueDate <= @eeDate) + 
		(Select Top 1 Isnull(-Balance,0) from t_Adv_PaymentSchedule Where AccountID = d.AccountID AND DealID = d.DealID AND DueDate <= @eeDate Order by DueDate desc, InstNo desc) MaxClearBalance, 
		d.MaturityDate "ExpiryDate",CAST('-' AS VARCHAR(50)) SHName,
		CAST('-' AS VARCHAR(30)) SHCountry, a.CurrencyID,CAST(1 AS int) M12,Cast (0 AS MONEY) PeakUtilization12M,Cast (0 AS MONEY) PeakUtilizationYTD,Cast (0 AS MONEY) PeakUtilizationDTD 
		,Cast (0 AS MONEY) AvgDailyUtilization12M,Cast (0 AS MONEY) AvgDailyUtilizationYTD,Cast (0 AS MONEY) AvgDailyUtilizationDTD 
		,d.RiskCode ,rc.Description "RiskDescription", CAST('-' AS VARCHAR(30)) OverDraftClassID, CAST('-' AS VARCHAR(30)) "Classification",CAST('-' AS VARCHAR(30)) "SubClassification", 
		d.DisbursementDate "SanctionDate", CAST(0 AS INT) NoOfReschedule, CAST(NULL AS DATETIME) LastRescheduleDate,  ISNULL(d.ProvAmount,0) Provisioning, ISNULL(cc.LimitSecType,'-') LimitSecType      
		,(SELECT isnull(SUM(CollateralValue),0) FROM dbo.t_Collaterals WHERE OurBranchID = c.OurBranchID AND ClientID = c.ClientID AND CollateralStatus <> 'C') "InvoiceValue", @RptTime, 0 PDCount 
		FROM dbo.t_Customer c
		INNER JOIN t_Account a ON a.OurBranchID = c.OurBranchID AND a.ClientID = c.ClientID
		INNER JOIN dbo.vc_AccountBalanceHistory ab ON ab.AccountID = a.AccountID
		INNER JOIN dbo.t_Adv_PaymentSchedule p ON p.OurBranchID = a.OurBranchID AND p.AccountID = a.AccountID and DueDate <= @eeDate
		INNER JOIN t_Disbursement d ON a.AccountID = d.AccountID AND d.OurBranchID = a.OurBranchID AND d.DealID = p.DealID AND d.DisbursementDate <= @eeDate
		INNER JOIN dbo.t_RiskClassification rc ON rc.OurBranchID = d.OurBranchID AND rc.Code = d.RiskCode
		INNER JOIN t_FacilityProduct l ON l.OurBranchID = a.OurBranchID AND l.FacilityID = d.DealID AND c.ClientID = l.ClientID  AND a.ProductID = l.ProductID
		LEFT JOIN t_LinkCIFCollateral cc ON cc.OurBranchID = l.OurBranchID AND cc.ClientID = l.ClientID AND cc.SerialNo = l.FacilityID		
		LEFT JOIN dbo.t_RelationshipOfficer r ON r.OurBranchID = c.OurBranchID AND r.OfficerID = c.RManager
		LEFT JOIN dbo.t_ManageGroups mg ON mg.OurBranchID = c.OurBranchID AND mg.GroupID = c.GroupID
		WHERE c.OurBranchID = @OurBranchID AND c.ClientID = @ClientID AND ab.wDate = @eeDate 

		INSERT INTO TmpRptRiskCreditMain SELECT c.ClientID, c.Name, 'Faci' AS "LoanType", mg.GroupID,mg.GroupName,r.FullName "RelationManager",a.ProductID,a.AccountID,
		CAST(l.FacilityID AS VARCHAR(20)) "DealID", CAST(l.SerialID AS VARCHAR(20)) SerialID,l.Limit,CAST('0' AS MONEY) InstAmount, l.EffectiveDate DueDate, CAST(0 AS INT) DPD,CAST(0 AS INT) CurrentDPD,CAST(0 AS INT) PeakDPD,
		CAST(NULL AS DATETIME) PayDate,CAST('-' AS VARCHAR(20)) IsPosted,ab.clearbalance,ab.ClearBalance "MaxClearBalance", l.EffectiveDate "ExpiryDate",CAST('-' AS VARCHAR(50)) SHName,
		CAST('-' AS VARCHAR(30)) SHCountry, a.CurrencyID,CAST(1 AS int) M12,Cast (0 AS MONEY) PeakUtilization12M,Cast (0 AS MONEY) PeakUtilizationYTD,Cast (0 AS MONEY) PeakUtilizationDTD 
		,Cast (0 AS MONEY) AvgDailyUtilization12M,Cast (0 AS MONEY) AvgDailyUtilizationYTD,Cast (0 AS MONEY) AvgDailyUtilizationDTD
		,Cast ('-' AS VARCHAR(30)) RiskCode ,Cast ('-' AS VARCHAR(100)) RiskDescription,CAST('-' AS VARCHAR(30)) OverDraftClassID,CAST('-' AS VARCHAR(30)) "Classification",CAST('-' AS VARCHAR(30)) "SubClassification"
		,(SELECT TOP 1 EffectiveDate FROM dbo.t_OverdraftLimits WHERE AccountID = a.AccountID ORDER BY EffectiveDate) "SanctionDate", CAST(0 AS INT) NoOfReschedule, CAST(NULL AS DATETIME) LastRescheduleDate,  
		CAST(0 AS MONEY) Provisioning, ISNULL(cc.LimitSecType,'-') LimitSecType  
		,(SELECT isnull(SUM(CollateralValue),0) FROM dbo.t_Collaterals WHERE OurBranchID = c.OurBranchID AND ClientID = c.ClientID AND CollateralStatus <> 'C') "InvoiceValue", @RptTime, 0 PDCount 
		FROM dbo.t_LinkFacilityAccount l
		INNER JOIN dbo.t_Account a ON a.AccountID = l.AccountID AND l.OurBranchID = a.OurBranchID
		INNER JOIN dbo.t_Customer c ON c.ClientID = a.ClientID AND c.OurBranchID = a.OurBranchID
		INNER JOIN dbo.vc_AccountBalanceHistory ab ON ab.AccountID = l.AccountID
		LEFT JOIN dbo.t_LinkCIFCollateral cc ON cc.OurBranchID = l.OurBranchID AND cc.ClientID = l.ClientID AND cc.SerialNo = l.FacilityID
		LEFT join dbo.t_RelationshipOfficer r ON r.OurBranchID = c.OurBranchID AND r.OfficerID = c.RManager
		LEFT JOIN dbo.t_ManageGroups mg ON mg.OurBranchID = c.OurBranchID AND mg.GroupID = c.GroupID
		WHERE c.OurBranchID = @OurBranchID AND c.ClientID = @ClientID AND l.AccountID NOT IN (SELECT AccountID FROM dbo.t_Disbursement) 
		AND a.ProductID IN (SELECT ProductID FROM dbo.t_Products WHERE ProductType IN ('AE')) AND ab.wDate = @eeDate AND l.EffectiveDate <= @eeDate

		SET @cCount=0     SET @sCount=0     
		SELECT @cCount=COUNT(*) from dbo.t_Customer WHERE OurBranchID = @OurBranchID AND GroupID = @GroupID AND ClientID <> @CIF
      
		WHILE (@cCount <> @sCount)         
		BEGIN             
			SET @sCount = @sCount + 1             
			SELECT @ClientID=ClientID FROM ( SELECT ClientID,ROW_NUMBER() OVER (ORDER BY ClientID) AS RowNum FROM dbo.t_Customer WHERE OurBranchID = @OurBranchID AND GroupID = @GroupID AND ClientID <> @CIF ) AS oTable 
			WHERE oTable.RowNum = @sCount 

			INSERT INTO TmpRptRiskCreditDetail 
			SELECT c.ClientID , c.Name ,'Loan' AS "LoanType",mg.GroupID,mg.GroupName,r.FullName "RelationManager",a.ProductID,a.AccountID, d.DealID AS "DealID", CAST(p.InstNo AS VARCHAR(20)) SerialID, l.Limit, p.InstAmount, p.DueDate , 
			DATEDIFF(d,p.DueDate,@eeDate) DPD,DATEDIFF(d,p.DueDate,@eeDate) CurrentDPD,DATEDIFF(d,p.DueDate,@eeDate) PeakDPD, p.PayDate, CAST(p.IsPosted AS VARCHAR(20)) IsPosted,
			(Select Isnull(Sum(-InstAmount),0) from t_Adv_PaymentSchedule Where AccountID = d.AccountID AND DealID = d.DealID AND isposted = 0 AND DueDate <= @eeDate) + 
			(Select Top 1 Isnull(-Balance,0) from t_Adv_PaymentSchedule Where AccountID = d.AccountID AND DealID = d.DealID AND DueDate <= @eeDate Order by DueDate desc, InstNo desc) ClearBalance, 
			(Select Isnull(Sum(-InstAmount),0) from t_Adv_PaymentSchedule Where AccountID = d.AccountID AND DealID = d.DealID AND isposted = 0 AND DueDate <= @eeDate) + 
			(Select Top 1 Isnull(-Balance,0) from t_Adv_PaymentSchedule Where AccountID = d.AccountID AND DealID = d.DealID AND DueDate <= @eeDate Order by DueDate desc, InstNo desc) MaxClearBalance, 
			d.MaturityDate,'-','-',a.CurrencyID,1,
			0,0,0,0,0,0,d.RiskCode ,rc.Description "RiskDescription" ,'-','-','-',d.DisbursementDate,0,NULL,0, ISNULL(cc.LimitSecType,'-')     
			,(SELECT isnull(SUM(CollateralValue),0) FROM dbo.t_Collaterals WHERE OurBranchID = c.OurBranchID AND ClientID = c.ClientID AND CollateralStatus <> 'C') "InvoiceValue", @RptTime, 0 PDCount  
			FROM dbo.t_Customer c
			INNER JOIN t_Account a ON a.OurBranchID = c.OurBranchID AND a.ClientID = c.ClientID
			INNER JOIN dbo.vc_AccountBalanceHistory ab ON ab.AccountID = a.AccountID
			INNER JOIN dbo.t_Adv_PaymentSchedule p ON p.OurBranchID = a.OurBranchID AND p.AccountID = a.AccountID and DueDate <= @eeDate
			INNER JOIN t_Disbursement d ON a.AccountID = d.AccountID AND d.OurBranchID = a.OurBranchID AND d.DealID = p.DealID AND d.DisbursementDate <= @eeDate
			INNER JOIN dbo.t_RiskClassification rc ON rc.OurBranchID = d.OurBranchID AND rc.Code = d.RiskCode  
			INNER JOIN t_FacilityProduct l ON l.OurBranchID = a.OurBranchID AND l.FacilityID = d.DealID AND c.ClientID = l.ClientID  AND a.ProductID = l.ProductID
			LEFT JOIN t_LinkCIFCollateral cc ON cc.OurBranchID = l.OurBranchID AND cc.ClientID = l.ClientID AND cc.SerialNo = l.FacilityID		
			LEFT join dbo.t_RelationshipOfficer r ON r.OurBranchID = c.OurBranchID AND r.OfficerID = c.RManager
			LEFT JOIN dbo.t_ManageGroups mg ON mg.OurBranchID = c.OurBranchID AND mg.GroupID = c.GroupID
			WHERE c.OurBranchID = @OurBranchID AND c.ClientID = @ClientID AND ab.wDate = @eeDate 

			INSERT INTO TmpRptRiskCreditMain 
			SELECT c.ClientID, c.Name, 'Faci' AS "LoanType", mg.GroupID,mg.GroupName,r.FullName "RelationManager",a.ProductID,a.AccountID, CAST(l.FacilityID AS VARCHAR(20)) "DealID",l.SerialID, l.Limit,
			'-' AS InstAmount, l.EffectiveDate DueDate, CAST(0 AS INT) DPD,CAST(0 AS INT) CurrentDPD,CAST(0 AS INT) PeakDPD,'' PayDate,CAST('-' AS VARCHAR(20)) IsPosted,ab.clearbalance,ab.ClearBalance,
			'','-','-', a.CurrencyID,1,
			0,0,0,0,0,0,'-','-','-','-','-',(SELECT TOP 1 EffectiveDate FROM dbo.t_OverdraftLimits WHERE AccountID = a.AccountID ORDER BY EffectiveDate) "SanctionDate",0,NULL,0, ISNULL(cc.LimitSecType,'-')      
			,(SELECT isnull(SUM(CollateralValue),0) FROM dbo.t_Collaterals WHERE OurBranchID = c.OurBranchID AND ClientID = c.ClientID AND CollateralStatus <> 'C') "InvoiceValue", @RptTime, 0 PDCount 
			FROM dbo.t_LinkFacilityAccount l
			INNER JOIN dbo.t_Account a ON a.AccountID = l.AccountID AND l.OurBranchID = a.OurBranchID
			INNER JOIN dbo.t_Customer c ON c.ClientID = a.ClientID AND c.OurBranchID = a.OurBranchID
			INNER JOIN dbo.vc_AccountBalanceHistory ab ON ab.AccountID = l.AccountID
			INNER JOIN dbo.t_LinkCIFCollateral lcc ON lcc.OurBranchID = c.OurBranchID AND lcc.ClientID = c.ClientID AND lcc.SerialNo = l.FacilityID
			LEFT JOIN dbo.t_LinkCIFCollateral cc ON cc.OurBranchID = l.OurBranchID AND cc.ClientID = l.ClientID AND cc.SerialNo = l.FacilityID
			LEFT join dbo.t_RelationshipOfficer r ON r.OurBranchID = c.OurBranchID AND r.OfficerID = c.RManager
			LEFT JOIN dbo.t_ManageGroups mg ON mg.OurBranchID = c.OurBranchID AND mg.GroupID = c.GroupID
			WHERE c.OurBranchID = @OurBranchID AND c.ClientID = @ClientID AND l.AccountID NOT IN (SELECT AccountID FROM dbo.t_Disbursement)  
			AND ab.wDate = @eeDate AND a.ProductID IN (SELECT ProductID FROM t_Products WHERE ProductType IN ('AE'))  AND l.EffectiveDate <= @eeDate
		END 
	END --- @GROUPCIF	
	IF @GroupCIF = '0'
	BEGIN
		INSERT INTO TmpRptRiskCreditDetail SELECT c.ClientID , c.Name ,'Loan' AS "LoanType",mg.GroupID,mg.GroupName,r.FullName "RelationManager",a.ProductID,a.AccountID, CAST(d.DealID AS VARCHAR(20)) DealID, 
		CAST(p.InstNo AS VARCHAR(20)) SerialID, l.Limit, p.InstAmount, p.DueDate , DATEDIFF(d,p.DueDate,@eeDate) DPD,DATEDIFF(d,p.DueDate,@eeDate) CurrentDPD,DATEDIFF(d,p.DueDate,@eeDate) PeakDPD, 
		p.PayDate, CAST(p.IsPosted AS VARCHAR(20)) IsPosted,
		(Select Isnull(Sum(-InstAmount),0) from t_Adv_PaymentSchedule Where AccountID = d.AccountID AND DealID = d.DealID AND isposted = 0 AND DueDate <= @eeDate) + 
		(Select Top 1 Isnull(-Balance,0) from t_Adv_PaymentSchedule Where AccountID = d.AccountID AND DealID = d.DealID AND DueDate <= @eeDate Order by DueDate desc, InstNo desc) ClearBalance, 
		(Select Isnull(Sum(-InstAmount),0) from t_Adv_PaymentSchedule Where AccountID = d.AccountID AND DealID = d.DealID AND isposted = 0 AND DueDate <= @eeDate) + 
		(Select Top 1 Isnull(-Balance,0) from t_Adv_PaymentSchedule Where AccountID = d.AccountID AND DealID = d.DealID AND DueDate <= @eeDate Order by DueDate desc, InstNo desc) MaxClearBalance, 
		d.MaturityDate "ExpiryDate",CAST('-' AS VARCHAR(50)) SHName,
		CAST('-' AS VARCHAR(30)) SHCountry, a.CurrencyID,CAST(1 AS int) M12,Cast (0 AS MONEY) PeakUtilization12M,Cast (0 AS MONEY) PeakUtilizationYTD,Cast (0 AS MONEY) PeakUtilizationDTD 
		,Cast (0 AS MONEY) AvgDailyUtilization12M,Cast (0 AS MONEY) AvgDailyUtilizationYTD,Cast (0 AS MONEY) AvgDailyUtilizationDTD 
		,d.RiskCode ,rc.Description "RiskDescription", CAST('-' AS VARCHAR(30)) OverDraftClassID, CAST('-' AS VARCHAR(30)) "Classification",CAST('-' AS VARCHAR(30)) "SubClassification", 
		d.DisbursementDate "SanctionDate", CAST(0 AS INT) NoOfReschedule, CAST(NULL AS DATETIME) LastRescheduleDate,  ISNULL(d.ProvAmount,0) Provisioning, ISNULL(cc.LimitSecType,'-') LimitSecType      
		,(SELECT isnull(SUM(CollateralValue),0) FROM dbo.t_Collaterals WHERE OurBranchID = c.OurBranchID AND ClientID = c.ClientID AND CollateralStatus <> 'C') "InvoiceValue", @RptTime, 0 PDCount  
		FROM dbo.t_Customer c
		INNER JOIN t_Account a ON a.OurBranchID = c.OurBranchID AND a.ClientID = c.ClientID
		INNER JOIN dbo.vc_AccountBalanceHistory ab ON ab.AccountID = a.AccountID
		INNER JOIN dbo.t_Adv_PaymentSchedule p ON p.OurBranchID = a.OurBranchID AND p.AccountID = a.AccountID and DueDate <= @eeDate
		INNER JOIN t_Disbursement d ON a.AccountID = d.AccountID AND d.OurBranchID = a.OurBranchID AND d.DealID = p.DealID AND d.DisbursementDate <= @eeDate
		INNER JOIN t_FacilityProduct l ON l.OurBranchID = a.OurBranchID AND l.FacilityID = d.DealID AND c.ClientID = l.ClientID  AND a.ProductID = l.ProductID
		LEFt JOIN t_LinkCIFCollateral cc ON cc.OurBranchID = l.OurBranchID AND cc.ClientID = l.ClientID AND cc.SerialNo = l.FacilityID		
		INNER JOIN dbo.t_RiskClassification rc ON rc.OurBranchID = d.OurBranchID AND rc.Code = d.RiskCode
		LEFT JOIN dbo.t_RelationshipOfficer r ON r.OurBranchID = c.OurBranchID AND r.OfficerID = c.RManager
		LEFT JOIN dbo.t_ManageGroups mg ON mg.OurBranchID = c.OurBranchID AND mg.GroupID = c.GroupID
		WHERE c.OurBranchID = @OurBranchID AND ab.wDate = @eeDate AND ((p.IsPosted = 0 AND p.DueDate <= @eeDate) OR 
		(p.IsPosted = 1 AND (p.PayDate > p.DueDate AND p.PayDate BETWEEN @ssDate AND @eeDate)))

		

		INSERT INTO TmpRptRiskCreditMain SELECT c.ClientID, c.Name, 'Faci' AS "LoanType", mg.GroupID,mg.GroupName,r.FullName "RelationManager",a.ProductID,a.AccountID,
		CAST(l.FacilityID AS VARCHAR(20)) "DealID", CAST(l.SerialID AS VARCHAR(20)) SerialID,l.Limit,CAST('0' AS MONEY) InstAmount, l.EffectiveDate DueDate, CAST(0 AS INT) DPD,CAST(0 AS INT) CurrentDPD,CAST(0 AS INT) PeakDPD,
		CAST(NULL AS DATETIME) PayDate,CAST('-' AS VARCHAR(20)) IsPosted,ab.clearbalance,ab.ClearBalance "MaxClearBalance", l.EffectiveDate "ExpiryDate",CAST('-' AS VARCHAR(50)) SHName,
		CAST('-' AS VARCHAR(30)) SHCountry, a.CurrencyID,CAST(1 AS int) M12,Cast (0 AS MONEY) PeakUtilization12M,Cast (0 AS MONEY) PeakUtilizationYTD,Cast (0 AS MONEY) PeakUtilizationDTD 
		,Cast (0 AS MONEY) AvgDailyUtilization12M,Cast (0 AS MONEY) AvgDailyUtilizationYTD,Cast (0 AS MONEY) AvgDailyUtilizationDTD
		,Cast ('-' AS VARCHAR(30)) RiskCode ,Cast ('-' AS VARCHAR(100)) RiskDescription,CAST('-' AS VARCHAR(30)) OverDraftClassID,CAST('-' AS VARCHAR(30)) "Classification",CAST('-' AS VARCHAR(30)) "SubClassification"
		,(SELECT TOP 1 EffectiveDate FROM dbo.t_OverdraftLimits WHERE AccountID = a.AccountID ORDER BY EffectiveDate) "SanctionDate", CAST(0 AS INT) NoOfReschedule, CAST(NULL AS DATETIME) LastRescheduleDate,  
		CAST(0 AS MONEY) Provisioning, ISNULL(cc.LimitSecType,'-') LimitSecType  
		,(SELECT isnull(SUM(CollateralValue),0) FROM dbo.t_Collaterals WHERE OurBranchID = c.OurBranchID AND ClientID = c.ClientID AND CollateralStatus <> 'C') "InvoiceValue", @RptTime, 0 PDCount  
		FROM dbo.t_LinkFacilityAccount l
		INNER JOIN dbo.t_Account a ON a.AccountID = l.AccountID AND l.OurBranchID = a.OurBranchID
		INNER JOIN dbo.t_Customer c ON c.ClientID = a.ClientID AND c.OurBranchID = a.OurBranchID
		INNER JOIN dbo.vc_AccountBalanceHistory ab ON ab.AccountID = l.AccountID
		INNER JOIN dbo.t_OverdraftClassifications oc ON oc.OverdraftClassID = ISNULL(a.OverdraftClassID,'001')
		LEFT JOIN dbo.t_LinkCIFCollateral cc ON cc.OurBranchID = l.OurBranchID AND cc.ClientID = l.ClientID AND cc.SerialNo = l.FacilityID
		LEFT join dbo.t_RelationshipOfficer r ON r.OurBranchID = c.OurBranchID AND r.OfficerID = c.RManager
		LEFT JOIN dbo.t_ManageGroups mg ON mg.OurBranchID = c.OurBranchID AND mg.GroupID = c.GroupID
		WHERE c.OurBranchID = @OurBranchID AND l.AccountID NOT IN (SELECT AccountID FROM dbo.t_Disbursement) AND l.EffectiveDate <= @eeDate
		AND a.ProductID IN (SELECT ProductID FROM dbo.t_Products WHERE ProductType IN ('AE')) AND ab.wDate = @eeDate AND (ab.limit + ab.ClearBalance < 0 OR oc.OverdraftClassID <> '001')
	END
	IF @GroupCIF = ''
	BEGIN
		INSERT INTO TmpRptRiskCreditDetail SELECT c.ClientID , c.Name ,'Loan' AS "LoanType",mg.GroupID,mg.GroupName,r.FullName "RelationManager",a.ProductID,a.AccountID, CAST(d.DealID AS VARCHAR(20)) DealID, 
		CAST(p.InstNo AS VARCHAR(20)) SerialID, l.Limit, p.InstAmount, p.DueDate , DATEDIFF(d,p.DueDate,@eeDate) DPD,DATEDIFF(d,p.DueDate,@eeDate) CurrentDPD,DATEDIFF(d,p.DueDate,@eeDate) PeakDPD, 
		p.PayDate, CAST(p.IsPosted AS VARCHAR(20)) IsPosted,
		(Select Isnull(Sum(-InstAmount),0) from t_Adv_PaymentSchedule Where AccountID = d.AccountID AND DealID = d.DealID AND isposted = 0 AND DueDate <= @eeDate) + 
		(Select Top 1 Isnull(-Balance,0) from t_Adv_PaymentSchedule Where AccountID = d.AccountID AND DealID = d.DealID AND DueDate <= @eeDate Order by DueDate desc, InstNo desc) ClearBalance, 
		(Select Isnull(Sum(-InstAmount),0) from t_Adv_PaymentSchedule Where AccountID = d.AccountID AND DealID = d.DealID AND isposted = 0 AND DueDate <= @eeDate) + 
		(Select Top 1 Isnull(-Balance,0) from t_Adv_PaymentSchedule Where AccountID = d.AccountID AND DealID = d.DealID AND DueDate <= @eeDate Order by DueDate desc, InstNo desc) MaxClearBalance, 
		d.MaturityDate "ExpiryDate",CAST('-' AS VARCHAR(50)) SHName,
		CAST('-' AS VARCHAR(30)) SHCountry, a.CurrencyID,CAST(1 AS int) M12,Cast (0 AS MONEY) PeakUtilization12M,Cast (0 AS MONEY) PeakUtilizationYTD,Cast (0 AS MONEY) PeakUtilizationDTD 
		,Cast (0 AS MONEY) AvgDailyUtilization12M,Cast (0 AS MONEY) AvgDailyUtilizationYTD,Cast (0 AS MONEY) AvgDailyUtilizationDTD 
		,d.RiskCode ,rc.Description "RiskDescription", CAST('-' AS VARCHAR(30)) OverDraftClassID, CAST('-' AS VARCHAR(30)) "Classification",CAST('-' AS VARCHAR(30)) "SubClassification", 
		d.DisbursementDate "SanctionDate", CAST(0 AS INT) NoOfReschedule, CAST(NULL AS DATETIME) LastRescheduleDate,  ISNULL(d.ProvAmount,0) Provisioning, ISNULL(cc.LimitSecType,'-') LimitSecType      
		,(SELECT isnull(SUM(CollateralValue),0) FROM dbo.t_Collaterals WHERE OurBranchID = c.OurBranchID AND ClientID = c.ClientID AND CollateralStatus <> 'C') "InvoiceValue", @RptTime, 0 PDCount 
		FROM dbo.t_Customer c
		INNER JOIN t_Account a ON a.OurBranchID = c.OurBranchID AND a.ClientID = c.ClientID
		INNER JOIN dbo.vc_AccountBalanceHistory ab ON ab.AccountID = a.AccountID
		INNER JOIN dbo.t_Adv_PaymentSchedule p ON p.OurBranchID = a.OurBranchID AND p.AccountID = a.AccountID and p.DueDate <= @eeDate
		INNER JOIN t_Disbursement d ON a.AccountID = d.AccountID AND d.OurBranchID = a.OurBranchID AND d.DealID = p.DealID AND d.DisbursementDate <= @eeDate
		INNER JOIN t_FacilityProduct l ON l.OurBranchID = a.OurBranchID AND l.FacilityID = d.DealID AND c.ClientID = l.ClientID  AND a.ProductID = l.ProductID
		LEFT JOIN t_LinkCIFCollateral cc ON cc.OurBranchID = l.OurBranchID AND cc.ClientID = l.ClientID AND cc.SerialNo = l.FacilityID		
		INNER JOIN dbo.t_RiskClassification rc ON rc.OurBranchID = d.OurBranchID AND rc.Code = d.RiskCode
		LEFT JOIN dbo.t_RelationshipOfficer r ON r.OurBranchID = c.OurBranchID AND r.OfficerID = c.RManager
		LEFT JOIN dbo.t_ManageGroups mg ON mg.OurBranchID = c.OurBranchID AND mg.GroupID = c.GroupID
		WHERE c.OurBranchID = @OurBranchID AND ab.wDate = @eeDate AND ab.ClearBalance < 0

	
		INSERT INTO TmpRptRiskCreditMain SELECT c.ClientID, c.Name, 'Faci' AS "LoanType", mg.GroupID,mg.GroupName,r.FullName "RelationManager",a.ProductID,a.AccountID,
		CAST(l.FacilityID AS VARCHAR(20)) "DealID", CAST(l.SerialID AS VARCHAR(20)) SerialID,l.Limit,CAST('0' AS MONEY) InstAmount, l.EffectiveDate DueDate, CAST(0 AS INT) DPD,CAST(0 AS INT) CurrentDPD,CAST(0 AS INT) PeakDPD,
		CAST(NULL AS DATETIME) PayDate,CAST('-' AS VARCHAR(20)) IsPosted,ab.clearbalance,ab.ClearBalance "MaxClearBalance", l.EffectiveDate "ExpiryDate",CAST('-' AS VARCHAR(50)) SHName,
		CAST('-' AS VARCHAR(30)) SHCountry, a.CurrencyID,CAST(1 AS int) M12,Cast (0 AS MONEY) PeakUtilization12M,Cast (0 AS MONEY) PeakUtilizationYTD,Cast (0 AS MONEY) PeakUtilizationDTD 
		,Cast (0 AS MONEY) AvgDailyUtilization12M,Cast (0 AS MONEY) AvgDailyUtilizationYTD,Cast (0 AS MONEY) AvgDailyUtilizationDTD
		,Cast ('-' AS VARCHAR(30)) RiskCode ,Cast ('-' AS VARCHAR(100)) RiskDescription,CAST('-' AS VARCHAR(30)) OverDraftClassID,CAST('-' AS VARCHAR(30)) "Classification",CAST('-' AS VARCHAR(30)) "SubClassification"
		,(SELECT TOP 1 EffectiveDate FROM dbo.t_OverdraftLimits WHERE AccountID = a.AccountID ORDER BY EffectiveDate) "SanctionDate", CAST(0 AS INT) NoOfReschedule, CAST(NULL AS DATETIME) LastRescheduleDate,  
		CAST(0 AS MONEY) Provisioning, ISNULL(cc.LimitSecType,'-') LimitSecType  
		,(SELECT isnull(SUM(CollateralValue),0) FROM dbo.t_Collaterals WHERE OurBranchID = c.OurBranchID AND ClientID = c.ClientID AND CollateralStatus <> 'C') "InvoiceValue", @RptTime, 0 PDCount  
		FROM dbo.t_LinkFacilityAccount l
		INNER JOIN dbo.t_Account a ON a.AccountID = l.AccountID AND l.OurBranchID = a.OurBranchID
		INNER JOIN dbo.t_Customer c ON c.ClientID = a.ClientID AND c.OurBranchID = a.OurBranchID
		INNER JOIN dbo.vc_AccountBalanceHistory ab ON ab.AccountID = l.AccountID
		INNER JOIN dbo.t_OverdraftClassifications oc ON oc.OverdraftClassID = ISNULL(a.OverdraftClassID,'001')
		LEFT JOIN dbo.t_LinkCIFCollateral cc ON cc.OurBranchID = l.OurBranchID AND cc.ClientID = l.ClientID AND cc.SerialNo = l.FacilityID
		LEFT join dbo.t_RelationshipOfficer r ON r.OurBranchID = c.OurBranchID AND r.OfficerID = c.RManager
		LEFT JOIN dbo.t_ManageGroups mg ON mg.OurBranchID = c.OurBranchID AND mg.GroupID = c.GroupID
		WHERE c.OurBranchID = @OurBranchID AND l.AccountID NOT IN (SELECT AccountID FROM dbo.t_Disbursement)  AND l.EffectiveDate <= @eeDate
		AND a.ProductID IN (SELECT ProductID FROM dbo.t_Products WHERE ProductType IN ('AE')) AND ab.wDate = @eeDate AND ab.ClearBalance < 0 
	END


	SET @cCount=0     SET @sCount=0     
	SELECT @cCount=COUNT(*) from dbo.TmpRptRiskCreditMain WHERE LoanType = 'Faci' AND RptTime = @RptTime

	WHILE (@cCount <> @sCount)         
	BEGIN 
		SET @sCount = @sCount + 1  
		SET @Flag = 0	SET @DPD = 0	SET @ABHBal = 0	SET @Bal = 0	SET @SanctionDate = ''	SET @ABHBalOld = 0   SET @PDCount = 0

		SELECT @ClientID=ClientID, @AccountID=AccountID, @Limit=Limit, @DealID=DealID, @SerialID=SerialID, @ClearBalance=ClearBalance, @MaxClearBalance=MaxClearBalance, @SanctionDate = SanctionDate FROM 
		(SELECT ClientID,AccountID,Limit,DealID,SerialID,ClearBalance,MaxClearBalance,SanctionDate,ROW_NUMBER() OVER (ORDER BY AccountID) AS RowNum FROM dbo.TmpRptRiskCreditMain WHERE LoanType = 'Faci' AND RptTime = @RptTime) AS oTable 
		WHERE oTable.RowNum = @sCount 

		IF @sdate <= @M12TDDate 
		BEGIN
			SET @ssDate =  @sdate
		END	
		ELSE
		BEGIN
			SET @ssDate =  @M12TDDate
		END	

		WHILE @ssDate <= @eeDate
		BEGIN
			SET @ABHCount = 0	SET @PeakDPD = 0
			SELECT @ABHCount = Count(*) FROM dbo.vc_AccountBalanceHistory WHERE AccountID = @AccountID AND wDate = @ssDate
			IF @ABHCount <> 0 
			BEGIN
----------------------------------------
				Select TOP 1 @OverDraftClassID = OverDraftClassID, @Classification = Classification, @SubClassification = SubClassification from v_ODClassifiedAccountsHistory 
				WHERE AccountID = @AccountID AND WorkingDate <= @ssDate ORDER BY WorkingDate desc

				SELECT @ABHBal = ClearBalance, @Limit = limit FROM dbo.vc_AccountBalanceHistory WHERE AccountID = @AccountID AND wDate = @ssDate
				-- Max Balance/Utilization
				IF @ABHBal < @MaxClearBalance
				Begin
					SET @MaxClearBalance = @ABHBal
					 UPDATE TmpRptRiskCreditMain SET MaxClearBalance = @MaxClearBalance WHERE AccountID = @AccountID AND DealID = @DealID
				End
				-- DPD and DPD Start Date
				IF -@ABHBal > @Limit
				BEGIN
					IF @Flag = 0 
					BEGIN
						SET @DueDate = @ssDate
						SET @PDCount = @PDCount + 1
					END
					SET @DPD = @DPD + 1
					SET @Flag = 1
					SET @Bal = @ABHBal
				END
				-- If out from PD/OD
				IF (@ABHBal > 0 OR -@ABHBal <= @Limit) AND @Flag = 1 
				BEGIN
					SET @Flag = 0
					INSERT INTO TmpRptRiskCreditDetail VALUES (@ClientID,'','Faci','','','','',@AccountID,@DealID,@SerialID,@limit,-@Bal-@Limit,@DueDate,@DPD,@CurrentDPD,@PeakDPD,@ssDate,'-',@Bal,@MaxClearBalance,'','-','-','',1,0,0,0,0,0,0,'-','-' ,@OverDraftClassID,@Classification,@SubClassification,@SanctionDate,0,NULL,0,'-',0,@RptTime,0)
					SET @DPD = 0
				END
				IF (-@ABHBal > @Limit) AND @ssDate = @eeDate 
				BEGIN
					--SET @Flag = 0
					INSERT INTO TmpRptRiskCreditDetail VALUES (@ClientID,'','Faci','','','','',@AccountID,@DealID,@SerialID,@limit,-@Bal-@Limit,@DueDate,@DPD,@CurrentDPD,@PeakDPD,NULL,'-',@Bal,@MaxClearBalance,'','-','-','',1,0,0,0,0,0,0,'-','-' ,@OverDraftClassID,@Classification,@SubClassification,@SanctionDate,0,NULL,0,'-',0,@RptTime,0)
					--SET @DPD = 0
				END
				
				IF (@ABHBal <> @ABHBalOld)  
				BEGIN
					SET @ABHBalOld = @ABHBal
					IF @ABHBal >= 0  SET @ABHBalOld = 0
  					INSERT INTO TmpRptRiskCreditDetail2 VALUES (@ClientID,'','Faci','','','','',@AccountID,@DealID,@SerialID,@limit,-@Bal-@Limit,@ssDate,@DPD,@CurrentDPD,@PeakDPD,NULL,'-',@ABHBalOld,@MaxClearBalance,'','-','-','',1,0,0,0,0,0,0,'-','-' ,@OverDraftClassID,@Classification,@SubClassification,@SanctionDate,0,NULL,0,'-',0,@RptTime,0)
					SET @ABHBalOld = @ABHBal
				END

				IF @Limit+@ClearBalance < 0 
				BEGIN
					UPDATE TmpRptRiskCreditMain SET InstAmount = -(@Limit+@ClearBalance) WHERE AccountID = @AccountID AND DealID = @DealID AND RptTime = @RptTime
				END

				IF @ssDate = @eeDate
				BEGIN
					UPDATE dbo.TmpRptRiskCreditMain SET Limit = @Limit, CurrentDPD = @DPD WHERE AccountID = @AccountID AND DealID = @DealID AND RptTime = @RptTime
					SELECT @PeakDPD = MAX(DPD) FROM dbo.TmpRptRiskCreditDetail WHERE AccountID = @AccountID AND DealID = @DealID AND RptTime = @RptTime
					UPDATE dbo.TmpRptRiskCreditMain SET PeakDPD = @PeakDPD WHERE AccountID = @AccountID AND DealID = @DealID AND RptTime = @RptTime
                END
-- SHARE HOLDER
				SET @SHName = '-'	SET @SHCountry = '-'	SET @SHParentID = 0		SET @SHID = 0	SET @SHOwnerShip = 0

				SELECT @SHID = ShareHolderID, @SHParentID = ParentShareHolderId, @SHName = ShareHolderName, @SHOwnerShip = OwnershipPercentage, @SHCountry = Country FROM 
				(SELECT s.ShareHolderID, s.ParentShareHolderId, s.ShareHolderName, s.OwnershipPercentage ,CASE WHEN s.ShareholderType = 'IND' THEN s.SHCountry ELSE s.CountryofIncorporation END AS Country, 
				ROW_NUMBER() OVER (ORDER BY s.OwnershipPercentage DESC, s.ParentShareHolderId) AS RowNum FROM dbo.t_CIF_ShareHolders c 
				INNER JOIN dbo.t_ShareHolders s ON s.OurBranchID = c.OurBranchID AND s.ShareHolderID = c.ShareHolderID WHERE c.ClientID = @ClientID) AS oTable
				WHERE oTable.RowNum = 1

				IF @SHParentID IS NOT NULL
				BEGIN
					SELECT @SHID = s.ShareHolderID, @SHParentID = s.ParentShareHolderId ,@SHName =s.ShareHolderName, @SHCountry = CASE WHEN s.ShareholderType = 'IND' THEN s.SHCountry ELSE s.CountryofIncorporation END FROM dbo.t_ShareHolders s WHERE s.ShareHolderID = @SHParentID
				END

				Update TmpRptRiskCreditMain Set SHName = @SHName, SHCountry = @SHCountry where ClientID = @ClientID AND RptTime = @RptTime
				Update TmpRptRiskCreditDetail Set SHName = @SHName, SHCountry = @SHCountry where ClientID = @ClientID AND RptTime = @RptTime

-- Main Table D2D Classification 
				SET @OverDraftClassID = '-'	SET @Classification = '-'	SET @SubClassification = '-'
				Select TOP 1 @OverDraftClassID = OverDraftClassID, @Classification = Classification, @SubClassification = SubClassification from v_ODClassifiedAccountsHistory Where AccountID = @AccountID
				AND WorkingDate >= @sDate and WorkingDate <= @eDate order by AccountID, WorkingDate desc

				Update TmpRptRiskCreditMain Set OverDraftClassID = @OverDraftClassID, Classification = @Classification, SubClassification = @SubClassification where AccountID = @AccountID AND RptTime = @RptTime
			END
			SET @ssDate=DATEADD(d,1,@ssDate)
		END
-- Main Table Total DPD @ Account
		SET @TotDPD = 0
		SELECT @TotDPD = SUM(DPD) FROM dbo.TmpRptRiskCreditDetail WHERE AccountID = @AccountID AND DealID = @DealID AND RptTime = @RptTime
		UPDATE dbo.TmpRptRiskCreditMain SET DPD = @TotDPD WHERE AccountID = @AccountID AND DealID = @DealID AND RptTime = @RptTime
-- PDCount
		UPDATE dbo.TmpRptRiskCreditMain SET PDCount = @PDCount WHERE AccountID = @AccountID AND DealID = @DealID AND RptTime = @RptTime
		SET @PDCount = 0
-- Reschedule/Restructured OD
		SET @NoOfReschedule = 0		SET @LastRescheduleDate = NULL
		SELECT TOP 1 @LastRescheduleDate = EffectiveDate FROM dbo.t_OverdraftLimits WHERE AccountID = @AccountID AND FacilityID = @DealID ORDER BY EffectiveDate DESC
		SELECT @NoOfReschedule = COUNT(*)-1 FROM dbo.t_OverdraftLimits WHERE AccountID = @AccountID AND FacilityID = @DealID
		UPDATE dbo.TmpRptRiskCreditMain SET NoOfReschedule = @NoOfReschedule, LastRescheduleDate = @LastRescheduleDate WHERE AccountID = @AccountID AND DealID = @DealID AND RptTime = @RptTime
-- Provisioning
		SET @Provisioning = 0
		SELECT TOP 1 @Provisioning = CAST(ISNULL(Provisioning,0) AS MONEY) FROM t_Provisioning  WHERE AccountID = @AccountID AND DealID = @DealID ORDER BY ProvDate DESC
		UPDATE dbo.TmpRptRiskCreditMain SET Provisioning = @Provisioning WHERE AccountID = @AccountID AND DealID = @DealID AND RptTime = @RptTime
	END

	DECLARE @OLDACCOUNTID varchar(30)	DECLARE @OLDDEALID varchar(30)	DECLARE @OLDDUEDATE Datetime

---- LOAN ACCOUNTS DETAIL DPD
	SET @cCount=0     SET @sCount=0     SET @OLDDPD = 0		SET @OLDACCOUNTID = ''	SET @OLDDEALID = ''
	SELECT @cCount=COUNT(*) from dbo.TmpRptRiskCreditDetail WHERE LoanType = 'Loan' AND RptTime = @RptTime

	WHILE (@cCount <> @sCount)         
	BEGIN 
		SET @sCount = @sCount + 1  
		SET @Flag = 0	SET @DPD = 0	

		SELECT @ClientID=ClientID, @AccountID=AccountID, @DealID=DealID, @DPD=DPD, @DueDate=DueDate FROM 
		(SELECT ClientID, AccountID, DealID, DPD, DueDate, ROW_NUMBER() OVER (ORDER BY AccountID,DealID) AS RowNum FROM dbo.TmpRptRiskCreditDetail WHERE LoanType = 'Loan' AND RptTime = @RptTime 
		) AS pTable 
		WHERE pTable.RowNum = @sCount Order By ClientID, AccountID, DealID, DueDate

		IF @sCount = 1 AND @DPD > 0 
		BEGIN
			SET @OLDDPD = @DPD	SET @OLDACCOUNTID = @AccountID	SET @OLDDEALID = @DealID
			UPDATE TmpRptRiskCreditDetail SET PDCount = 1 WHERE AccountID = @AccountID AND DealID = @DealID AND RptTime = @RptTime AND DPD = @DPD
		END
		IF @sCount = 1 AND @DPD = 0
		BEGIN
			SET @OLDDPD = @DPD	SET @OLDACCOUNTID = @AccountID	SET @OLDDEALID = @DealID
		END

		IF @sCount > 1 AND @DPD > 0 AND @OLDACCOUNTID <> @AccountID 
		BEGIN
			SET @OLDDPD = @DPD	SET @OLDACCOUNTID = @AccountID	SET @OLDDEALID = @DealID
			UPDATE TmpRptRiskCreditDetail SET PDCount = 1 WHERE AccountID = @AccountID AND DealID = @DealID AND RptTime = @RptTime AND DPD = @DPD
		END
	END

	INSERT INTO TmpRptRiskCreditMain 
	Select ClientID, Name, LoanType,GRoupID, GroupName, RelationManager, ProductID, AccountID, DealID, DealID, Limit, 
	Sum(CASE WHEN IsPosted = 1 THEN 0 ELSE InstAmount END) InstAmount
	,Max(DueDate) DueDate, Max(DPD) DPD, Max(CurrentDPD) CurrentDPD,Max(PeakDPD) PeakDPD, Max(PayDate) PayDate,
	Case When Sum(CASE WHEN IsPosted = 1 THEN 0 ELSE InstAmount END) > 0 then 0 else 1 END Isposted
	, ClearBalance, MaxClearBalance, ExpiryDate, SHName, SHCountry, CurrencyID, M12, PeakUtilization12M, PeakUtilizationYTD, PeakUtilizationDTD, AvgDailyUtilization12M, 
	AvgDailyUtilizationYTD, AvgDailyUtilizationDTD, RiskCode, RiskDescription, OverDraftClassID, Classification, SubClassification, SanctionDate, NoOfReschedule, LastRescheduleDate, 
	Provisioning, LimitSecType,InvoiceValue, RptTime, Sum(PDCount) PDCount
	from TmpRptRiskCreditDetail WHERE LoanType = 'Loan' AND RptTime = @RptTime  
	group By ClientID, Name, LoanType,GRoupID, GroupName, RelationManager, ProductID, AccountID, DealID, DealID, Limit --, IsPosted
	, ClearBalance, MaxClearBalance, ExpiryDate, SHName, SHCountry, CurrencyID, M12, PeakUtilization12M, PeakUtilizationYTD, PeakUtilizationDTD, AvgDailyUtilization12M, 
	AvgDailyUtilizationYTD, AvgDailyUtilizationDTD, RiskCode, RiskDescription, OverDraftClassID, Classification, SubClassification, SanctionDate, NoOfReschedule, Provisioning, InvoiceValue, 
	LastRescheduleDate, LimitSecType,RptTime

---- LOAN ACCOUNTS
	SET @cCount=0     SET @sCount=0     
	SELECT @cCount=COUNT(*) from dbo.TmpRptRiskCreditMain WHERE LoanType = 'Loan' AND RptTime = @RptTime

	WHILE (@cCount <> @sCount)         
	BEGIN 
		SET @sCount = @sCount + 1  
		SET @Flag = 0	SET @DPD = 0	SET @ABHBal = 0	SET @Bal = 0	SET @SanctionDate = ''	SET @ABHBalOld = 0

		SELECT @ClientID=ClientID, @AccountID=AccountID, @DealID=DealID, @SerialID=SerialID FROM 
		(SELECT ClientID,AccountID,DealID,SerialID,ROW_NUMBER() OVER (ORDER BY AccountID,DealID) AS RowNum FROM dbo.TmpRptRiskCreditMain WHERE LoanType = 'Loan' AND RptTime = @RptTime) AS pTable 
		WHERE pTable.RowNum = @sCount 
		SET @NoOfReschedule = 0		SET @LastRescheduleDate = NULL	
		SELECT TOP 1 @NoOfReschedule = ISNULL(ReSchID,0),  @LastRescheduleDate = ISNULL(CONVERT(varchar(10), CreateTime, 120),NULL) FROM dbo.t_Adv_Deal_Reschedule WHERE AccountID = @AccountID AND DealID = @DealID ORDER BY CreateTime DESC
		UPDATE dbo.TmpRptRiskCreditMain SET NoOfReschedule = @NoOfReschedule, LastRescheduleDate = @LastRescheduleDate WHERE AccountID = @AccountID AND DealID = @DealID AND RptTime = @RptTime
-- Provisioning
		--SET @Provisioning = 0
		--SELECT TOP 1 @Provisioning = ISNULL(Provisioning,0) FROM t_Provisioning  WHERE AccountID = @AccountID AND DealID = @DealID ORDER BY ProvDate DESC
		--UPDATE dbo.TmpRptRiskCreditMain SET Provisioning = @Provisioning WHERE AccountID = @AccountID AND DealID = @DealID AND RptTime = @RptTime
	END

	UPDATE dbo.TmpRptRiskCreditMain SET LastRescheduleDate = NULL WHERE NoOfReschedule = 0 AND RptTime = @RptTime


	if @GroupCIF = '1'
	begin
	-- PeakUtilization 12M
		SET @cCount=0     SET @sCount=0   SET @Counter = 0  SET @Peak12MCounter = 0		SET @PeakYTDCounter = 0		SET @PeakDTDCounter = 0
		SELECT @cCount=COUNT(ClientID) from dbo.TmpRptRiskCreditMain

		WHILE (@cCount <> @sCount)         
		BEGIN 
			SET @sCount = @sCount + 1  
			SET @Peak12MABHBal = 0	SET @PeakUtilization12M = 0	SET @Peak12MBal = 0	SET @Peak12MCounter = 0
			SET @PeakYTDABHBal = 0	SET @PeakUtilizationYTD = 0	SET @PeakYTDBal = 0	SET @PeakYTDCounter = 0
			SET @PeakDTDABHBal = 0	SET @PeakUtilizationDTD = 0	SET @PeakDTDBal = 0	SET @PeakDTDCounter = 0

			SELECT @ClientID=ClientID, @LoanType=LoanType FROM (SELECT DISTINCT ClientID,Loantype,ROW_NUMBER() OVER (ORDER BY ClientID) AS RowNum FROM dbo.TmpRptRiskCreditMain Where RptTime = @RptTime) AS oTable WHERE oTable.RowNum = @sCount 
			SELECT TOP 1 @SanctionDate = convert(varchar(10), SanctionDate, 120) FROM dbo.TmpRptRiskCreditMain WHERE ClientID = @ClientID AND RptTime = @RptTime ORDER BY SanctionDate

			IF @sdate <= @M12TDDate 
			BEGIN
				SET @ssDate =  @sdate
			END	
			ELSE
			BEGIN
				SET @ssDate =  @M12TDDate
			END	

			WHILE @ssDate <= @eeDate
			BEGIN
					SET @Peak12MABHBal = 0	SET @PeakYTDABHBal = 0		SET @PeakDTDABHBal = 0
					IF @LoanType = 'Faci'
					BEGIN
						SELECT @Peak12MABHBal = ISNULL(SUM(ab.ClearBalance),0) FROM dbo.vc_AccountBalanceHistory ab INNER JOIN dbo.t_Account a ON a.AccountID = ab.AccountID 
						WHERE ab.AccountID IN (SELECT AccountID FROM TmpRptRiskCreditMain Where LoanType = 'Faci' AND RptTime = @RptTime) AND a.ClientID = @ClientID AND wDate = @ssDate AND ab.ClearBalance < 0
					END

					SET @PeakYTDABHBal = @Peak12MABHBal
					SET @PeakDTDABHBal = @Peak12MABHBal

					IF @LoanType <> 'Faci'
					BEGIN
						SELECT @Peak12MABHBal = (SELECT Sum(instamount) FROM TmpRptRiskCreditDetail Where LoanType <> 'Faci' AND RptTime = @RptTime AND ClientID = @ClientID) 
					END

					SET @PeakYTDABHBal = @PeakYTDABHBal + @Peak12MABHBal
					SET @PeakDTDABHBal = @PeakDTDABHBal + @Peak12MABHBal
				
					IF @ssDate >= @M12TDDate AND @Peak12MABHBal <> 0
					BEGIN
						SET @Peak12MBal = @Peak12MBal + @Peak12MABHBal
						SET @Peak12MCounter = @Peak12MCounter + 1
						IF @Peak12MABHBal < @PeakUtilization12M
						BEGIN
							SET @PeakUtilization12M = @Peak12MABHBal
						END
					END

					IF @ssDate >= @YTDDate AND @PeakYTDABHBal <> 0
					BEGIN
						SET @PeakYTDBal = @PeakYTDBal + @PeakYTDABHBal
						SET @PeakYTDCounter = @PeakYTDCounter + 1
						IF @PeakYTDABHBal < @PeakUtilizationYTD
						BEGIN
							SET @PeakUtilizationYTD = @PeakYTDABHBal
						END
					END

					IF @ssDate >= @SanctionDate AND @PeakDTDABHBal <> 0
					BEGIN				
						SET @PeakDTDBal = @PeakDTDBal + @PeakDTDABHBal
						SET @PeakDTDCounter = @PeakDTDCounter + 1
						IF @PeakDTDABHBal < @PeakUtilizationDTD
						BEGIN
							SET @PeakUtilizationDTD = @PeakDTDABHBal
						END
					END

					SET @ssDate=DATEADD(d,1,@ssDate)
			END
    		if @Peak12MCounter <> 0 
			begin
				UPDATE TmpRptRiskCreditMain SET PeakUtilization12M = @PeakUtilization12M, AvgDailyUtilization12M = @Peak12MBal/@Peak12MCounter WHERE ClientID = @ClientID AND RptTime = @RptTime
    		end
			else
			begin
				UPDATE TmpRptRiskCreditMain SET PeakUtilization12M = @PeakUtilization12M, AvgDailyUtilization12M = @Peak12MBal WHERE ClientID = @ClientID AND RptTime = @RptTime
			end

    		if @PeakYTDCounter <> 0 
			begin
				UPDATE TmpRptRiskCreditMain SET PeakUtilizationYTD = @PeakUtilizationYTD, AvgDailyUtilizationYTD = @PeakYTDBal/@PeakYTDCounter WHERE ClientID = @ClientID AND RptTime = @RptTime
    		end
			else
			begin
				UPDATE TmpRptRiskCreditMain SET PeakUtilizationYTD = @PeakUtilizationYTD, AvgDailyUtilizationYTD = @PeakYTDBal WHERE ClientID = @ClientID AND RptTime = @RptTime
			end
			
     		if @PeakDTDCounter <> 0 
			begin
				 UPDATE TmpRptRiskCreditMain SET PeakUtilizationDTD = @PeakUtilizationDTD, AvgDailyUtilizationDTD = @PeakDTDBal/@PeakDTDCounter WHERE ClientID = @ClientID AND RptTime = @RptTime
    		end
			else
			begin
				 UPDATE TmpRptRiskCreditMain SET PeakUtilizationDTD = @PeakUtilizationDTD, AvgDailyUtilizationDTD = @PeakDTDBal WHERE ClientID = @ClientID AND RptTime = @RptTime
			end
		END
	END --GroupCIF


	UPDATE TmpRptRiskCreditMain SET PayDate = NULL WHERE PayDate = '1900-01-01 00:00:00.000' AND RptTime = @RptTime
	UPDATE TmpRptRiskCreditMain SET OverDraftClassID = '001', Classification = 'Performing', SubClassification = 'Standard' WHERE OverDraftClassID = '-' AND LoanType = 'Faci' AND RptTime = @RptTime
	INSERT INTO TmpRptRiskCreditUtilzation SELECT DISTINCT ClientID,Name, PeakUtilization12M,AvgDailyUtilization12M,PeakUtilizationYTD,AvgDailyUtilizationYTD,@RptTime FROM TmpRptRiskCreditMain WHERE RptTime = @RptTime
	UPDATE M SET m.OverDraftClassID = o.OverdraftClassID, m.Classification = o.Classification, m.SubClassification = o.SubClassification FROM TmpRptRiskCreditMain AS M INNER JOIN dbo.t_OverdraftClassifications AS O ON '0'+o.OverdraftClassID = m.RiskCode WHERE M.LoanType = 'Loan' AND RptTime = @RptTime
	INSERT INTO TmpRptRiskCreditDetail2 SELECT * FROM dbo.TmpRptRiskCreditMain WHERE LoanType = 'Loan' AND RptTime = @RptTime
	
	select '1' as abc
	
END