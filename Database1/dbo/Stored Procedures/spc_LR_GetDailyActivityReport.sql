﻿CREATE PROCEDURE [dbo].[spc_LR_GetDailyActivityReport]
(  
 @OurBranchID varchar(30),  
 @AccountID varchar(30),  
 @InstrumentType varchar(30),  
 @WorkingDate datetime  
)  
  
AS  
BEGIN  
 SELECT InstrumentType,InstrumentNo,wDate,InstrumentAccountID,Amount,PayeeName,BeneficiaryName,Paid,PaidDate,Cancel,  
 CancelDate, Lost  
 FROM t_LR_Issuance  
 WHERE OurBranchID = @OurBranchID AND InstrumentAccountID = @AccountID   
 AND convert(varchar(10), wDate,120) = @WorkingDate AND InstrumentType = @InstrumentType AND Supervision IN ('C','')   
 AND InstrumentNo > 0 AND Erase = 0  
  
END