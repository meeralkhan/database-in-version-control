﻿create procedure [dbo].[v_ODAccrualAccountwise]
(
   @OurBranchID nvarchar(30),
   @AccountID nvarchar(30)
)
AS
begin
   
   select OurBranchID, AccountID, AccountName, ProductID, CurrencyID, Basis, ValueDate, [Days], RateType, Balance, Rate, Amount
   from t_RptODAccruals where OurBranchID = @OurBranchID and AccountID = @AccountID and Balance != 0
   and AccrualDate IN (select MAX(AccrualDate) from t_RptODAccruals where AccountID = @AccountID group by YEAR(AccrualDate), MONTH(AccrualDate))

end