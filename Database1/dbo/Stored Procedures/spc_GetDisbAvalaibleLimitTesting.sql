﻿CREATE Procedure [dbo].[spc_GetDisbAvalaibleLimitTesting](           
@ProductID nvarchar(20), --= 'TL001';           
@FacilityID nvarchar(100),-- = 'jbHqAzW5CrPDnB3W01MWuhJHL';           
@AccountID nvarchar(40),-- = '0330100000012901';           
@OurBranchID nvarchar(10)-- = '03'           
)           
as         
SET NOCOUNT ON             
declare @FacilitySerialNo nvarchar(5)           
declare @ClientID nvarchar(20)           
declare @TotalLimit nvarchar(20)           
declare @FacilityLimitType nvarchar(20)           
declare @ProductLimit nvarchar(20)           
declare @ProductLimitType nvarchar(20)           
declare @AvailableLimit decimal = 0           
           
select @ProductLimit = b.Limit, @ProductLimitType = b.LimitType,@TotalLimit=a.Limit,@FacilityLimitType=a.LimitType,@ClientID = a.ClientID,@FacilitySerialNo = SerialNo from t_LinkCIFCollateral a           
inner join t_FacilityProduct b on a.OurBranchID = b.OurBranchID and a.SerialNo = b.FacilityID and a.ClientID = b. ClientID and b.ProductID = @ProductID           
where ReferenceNo = @FacilityID and a.OurBranchID = @OurBranchID           
if(@FacilityLimitType = 'Fixed')           
 begin           
  select @AvailableLimit =@ProductLimit - isnull(sum(PrincipalAmount),0) from t_Adv_PaymentSchedule a           
  inner join t_Disbursement b on a.OurBranchID = b.OurBranchID and isnull(IsClosed,0) = 0 and a.AccountID = b.AccountID and a.DealID = b.DealID and a.IsPosted != 1 and FacilityID = @FacilityID and b.AccountID in (select AccountID from t_Account           
 
  where ClientID = @ClientID and ProductID = @ProductID and OurBranchID = @OurBranchID) and b.FacilityID = @FacilityID       
  if(@AvailableLimit is null)       
  begin       
 set  @AvailableLimit = @ProductLimit;       
  end       
 end           
else if(@FacilityLimitType = 'Revolving')           
 begin           
  if(@ProductLimitType = 'Fixed')           
   begin           
    select @AvailableLimit = @ProductLimit - isnull(sum(PrincipalAmount),0) from t_Adv_PaymentSchedule a           
    inner join t_Disbursement b on a.OurBranchID = b  
.OurBranchID and isnull(IsClosed,0) = 0 and a.AccountID = b.AccountID and a.DealID = b.DealID and a.IsPosted != 1 and FacilityID = @FacilityID and b.AccountID in            
    (select AccountID from t_Account where ClientID = @ClientID and ProductID = @ProductID and OurBranchID = @OurBranchID)   and b.FacilityID = @FacilityID          
  if(@AvailableLimit is null)       
   begin       
  set  @AvailableLimit = @ProductLimit;       
   end       
   end           
  else if(@ProductLimitType = 'Revolving')           
  
   begin           
    declare @FixProductLimit decimal           
    declare @RevolvingProductLimit decimal           
    declare @TotalRevUtilizedLimit decimal           
    declare @ProductUtilizedLimit decimal           
    select @FixProductLimit = sum(case when LimitType = 'Fixed' then isnull(limit,0) else 0 end),@RevolvingProductLimit = sum(case when LimitType = 'Revolving' then isnull(limit,0) else 0 end)            
    from t_FacilityProduct where FacilityID = @FacilitySerialNo and ClientID = @ClientID  
 and OurBranchID = @OurBranchID           
    if(@RevolvingProductLimit > (@TotalLimit - @FixProductLimit)) begin set @RevolvingProductLimit = @TotalLimit - @FixProductLimit end            
    select @TotalRevUtilizedLimit = sum(isnull(PrincipalAmount,0)) from t_Adv_PaymentSchedule a           
    inner join t_Disbursement b on a.OurBranchID = b.OurBranchID and isnull(IsClosed,0) = 0 and a.AccountID = b.AccountID and a.DealID = b.DealID and a.IsPosted <> 1 and FacilityID = @FacilityID and b.AccountID in (select AccountID from t_Account         
   
    where ClientID = @ClientID and ProductID in (select ProductID from t_FacilityProduct where FacilityID = @FacilitySerialNo and ClientID = @ClientID and OurBranchID = @OurBranchID            
    and LimitType = 'Revolving') and OurBranchID = @OurBranchID)            
     if(@TotalRevUtilizedLimit is null)      
         set  @TotalRevUtilizedLimit = 0;  
    select @ProductUtilizedLimit = sum(isnull(PrincipalAmount,0)) from t_Adv_PaymentSchedule a           
    inner join  
 t_Disbursement b on a.OurBranchID = b.OurBranchID and isnull(IsClosed,0) = 0 and a.AccountID = b.AccountID and a.DealID = b.DealID and a.IsPosted <> 1 and FacilityID = @FacilityID and b.AccountID in (select AccountID from t_Account           
    where ClientID = @ClientID and ProductID = @ProductID and OurBranchID = @OurBranchID)            
    if(@ProductUtilizedLimit is null)      
         set  @ProductUtilizedLimit = 0;  
    declare @MaxRevLimit decimal = @TotalLimit - @FixProductLimit;           
    if((@RevolvingProductLimit + @FixProductLimit) > @TotalLimit)           
     begin            
      set @RevolvingProductLimit = @TotalLimit - @FixProductLimit;           
     end           
    set @AvailableLimit = @ProductLimit - @ProductUtilizedLimit           
   
   if(@AvailableLimit > (@RevolvingProductLimit - @TotalRevUtilizedLimit))           
     begin           
      set @AvailableLimit = @RevolvingProductLimit - @TotalRevUtilizedLimit           
     end           
   end           
 end           
 select isnull(@AvailableLimit,0) as AvailableLimit,ISNULL(@ProductLimit,0) TotalLimit