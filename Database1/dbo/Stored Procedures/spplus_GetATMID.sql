﻿  
CREATE PROCEDURE [dbo].[spplus_GetATMID] (
 @OurBranchID varchar(30),
 @ATMID varchar(30)=''
)  
 AS    
 if @ATMID <> ''    
  begin    
   if exists ( SELECT ATMID FROM t_ATM_ATMAccount WHERE OurBranchID = @OurBranchID AND ATMID = @ATMID )
    
    select ATMID, OurBranchID, ATMAccountID, ATMAccountTitle, ATMCurrencyID, ATMCurrencyName, ATMProductID, 
    'General Ledger' as ATMProductName, CDMAccountID, CDMAccountTitle, CDMCurrencyID, CDMCurrencyName, CDMProductID, 
    'General Ledger' as CDMProductName, 1 as ReturnValue 
    from t_ATM_ATMAccount
    Where OurBranchID = @OurBranchID AND ATMID = @ATMID
      
   else    
   
    select 0 as ReturnValue    
    
  end    
    
 else    
  begin    
   select ATMID, OurBranchID, ATMAccountID, ATMAccountTitle, ATMCurrencyID, ATMCurrencyName, ATMProductID, 
   'General Ledger' as ATMProductName, CDMAccountID, CDMAccountTitle, CDMCurrencyID, CDMCurrencyName, CDMProductID, 
   'General Ledger' as CDMProductName
   From t_ATM_ATMAccount where OurBranchID = @OurBranchID
  end