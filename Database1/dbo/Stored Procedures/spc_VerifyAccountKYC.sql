﻿create Procedure [dbo].[spc_VerifyAccountKYC]  
(  
 @OurBranchID varchar(30),  
 @AccountID varchar(30)  
)  
AS  
BEGIN  
   
 SET NOCOUNT ON      
   
 DECLARE @AccOpenDate datetime  
 DECLARE @AccRunningMonths smallint  
 DECLARE @AccTurnOver varchar(30)       
 DECLARE @AccTurnOverAmt money       
 DECLARE @DrThresholdLimit money       
 DECLARE @CrThresholdLimit money       
 DECLARE @NoOfDrTrx numeric(24,0)       
 DECLARE @NoOfCrTrx numeric(24,0)  
 DECLARE @wDate Datetime  
 DECLARE @mWDate Datetime  
 DECLARE @mAccTurnOver money  
 DECLARE @KYCParam varchar(100)  
 DECLARE @Message varchar(max)  
  
 SELECT @AccOpenDate=OpenDate, @AccTurnOver=ISNULL(TurnOver, ''), @AccTurnOverAmt=ISNULL(tTurnOver,0), @DrThresholdLimit=ISNULL(DrThresholdLimit,0),  
 @CrThresholdLimit=ISNULL(CrThresholdLimit,0), @NoOfDrTrx=ISNULL(NoOfDrTrx,0), @NoOfCrTrx=ISNULL(NoOfCrTrx,0)  
 FROM t_Account  
 WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID AND ISNULL(Status,'') <> 'C'  
   
 SELECT @AccRunningMonths = DateDiff(MONTH,@AccOpenDate,@wDate)  
 SELECT @wDate = WorkingDate FROM t_Last where OurBranchID = @OurBranchID  
 SELECT @mWDate = convert(varchar(10), @wDate, 120) + ' ' + convert(varchar(10), getDate(),108)  
  
 if @AccTurnOver <> '' AND MONTH(@wDate) = 12 AND @AccRunningMonths > 1  
 BEGIN  
 SELECT @mAccTurnOver=SUM(Amount) FROM t_Transactions   
 WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND Year(wDate) = Year(@wDate) AND TrxType = 'C' AND Status = 'C'  
  
 SET @KYCParam = 'Account Turnover (Per Annum)'  
 SET @Message = ''  
  
 if @AccTurnOver = 'Below 1M'  
 BEGIN   
  if @mAccTurnOver > 1000000   
  BEGIN  
   SET @Message = 'KYC Account TurnOver is:['+@AccTurnOver+'] Actual Account TurnOver is:['+CAST(@mAccTurnOver as varchar(50))+']'  
  END  
 END  
 ELSE IF @AccTurnOver = '1M to 5M'  
 BEGIN  
  IF @mAccTurnOver < 1000000   
  BEGIN  
   SET @Message = 'KYC Account TurnOver is:['+@AccTurnOver+'] Actual Account TurnOver is:['+CAST(@mAccTurnOver as varchar(50))+']'  
  END  
  ELSE IF @mAccTurnOver > 5000000   
  BEGIN  
   SET @Message = 'KYC Account TurnOver is:['+@AccTurnOver+'] Actual Account TurnOver is:['+CAST(@mAccTurnOver as varchar(50))+']'  
  END  
 END  
 ELSE IF @AccTurnOver = '5M to 10M'  
 BEGIN  
  IF @mAccTurnOver < 5000000   
  BEGIN  
   SET @Message = 'KYC Account TurnOver is:['+@AccTurnOver+'] Actual Account TurnOver is:['+CAST(@mAccTurnOver as varchar(50))+']'  
  END  
  ELSE IF @mAccTurnOver > 10000000  
  BEGIN  
   SET @Message = 'KYC Account TurnOver is:['+@AccTurnOver+'] Actual Account TurnOver is:['+CAST(@mAccTurnOver as varchar(50))+']'  
  END  
 END  
 ELSE IF @AccTurnOver = 'Above 10M'  
 BEGIN  
  IF @mAccTurnOver < 10000000  
  BEGIN  
   SET @Message = 'KYC Account TurnOver is:['+@AccTurnOver+'] Actual Account TurnOver is:['+CAST(@mAccTurnOver as varchar(50))+']'  
  END  
  ELSE IF @mAccTurnOver < @AccTurnOverAmt  
  BEGIN  
   SET @Message = 'KYC Account TurnOver is:['+@AccTurnOver+'-->'+CAST(@AccTurnOverAmt as varchar(50))+'] Actual Account TurnOver is:['+CAST(@mAccTurnOver as varchar(50))+']'  
  END  
 END  
  
 IF @Message <> ''  
 BEGIN  
  Insert Into t_AccKYCExcLogs (OurBranchID, AccountID, [DateTime], KYCParam, [Message])  
  VALUES (@OurBranchID, @AccountID, @mWDate, @KYCParam, @Message)  
 END  
 END  
  
END