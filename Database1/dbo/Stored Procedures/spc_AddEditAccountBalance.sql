﻿CREATE Procedure [dbo].[spc_AddEditAccountBalance]       
(       
 @OurBranchID varchar(30),       
 @AccountID varchar(30),       
 @ProductID varchar(30)='',       
 @IsEffects char(1)='1',       
 @IsLocalCurrency char(1)='1',       
 @Amount money=0,       
 @ForeignAmount money =0       
)       
As       
       
 SET NOCOUNT ON      
       
 DECLARE @mEffects money       
 DECLARE @mClearBalance money       
 DECLARE @mLocalEffects money       
 DECLARE @mLocalClearBalance money       
       
 SELECT @mEffects=isNull(Effects,0), @mClearBalance=isNull(ClearBalance,0),       
 @mLocalEffects=isNull(LocalEffects,0), @mLocalClearBalance=isNull(LocalClearBalance,0)       
 FROM t_AccountBalance       
 WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID      
         
 IF @IsLocalCurrency='1'       
 BEGIN       
         
   IF @IsEffects='1'       
   BEGIN       
           
     UPDATE t_AccountBalance SET       
     Effects=@Amount + @mEffects       
     WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID       
           
   END       
   ELSE       
   BEGIN       
           
     UPDATE t_AccountBalance SET       
     ClearBalance=@Amount + @mClearBalance       
     WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID       
   END       
 END       
 ELSE       
 BEGIN       
   IF @IsEffects='1'       
   BEGIN       
     UPDATE t_AccountBalance SET       
     Effects=@ForeignAmount + @mEffects,       
     LocalEffects=@Amount + @mLocalEffects       
     WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID       
   END       
   ELSE       
   BEGIN       
     UPDATE t_AccountBalance SET       
     ClearBalance=@ForeignAmount + @mClearBalance,       
     LocalClearBalance=@Amount + @mLocalClearBalance       
     WHERE OurBranchID=@OurBranchID AND AccountID=@AccountID       
   END       
 END       
       
 SELECT 'Ok' AS RetStatus