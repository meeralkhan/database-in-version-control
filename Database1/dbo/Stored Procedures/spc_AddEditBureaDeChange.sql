﻿CREATE PROCEDURE [dbo].[spc_AddEditBureaDeChange]       
 @ScrollNo int=0,       
 @OurBranchID varchar(30),       
 @TransactionType char(1),       
 @SellOrBuy char(1),       
 @CurrencyID varchar(30),       
 @wDate datetime,       
 @ChequeID varchar(30)='V',       
       
 @Amount money,       
 @ForeignAmount Money,       
 @ExchangeRate decimal(18,6),       
 @Commission money=0,       
 @CommRate decimal(18,6)=0,       
 @NetAmount Money=0,    
 @ProfitOrLoss Money=0,       
     
 @DescriptionID varchar(30)='',       
 @Description varchar(255)='',       
       
 @OperatorID varchar(30),       
 @SupervisorID varchar(30)='',       
 @Supervision char(1)='C',       
       
 @CashAccountID varchar(30),       
 @CommissionAccountID varchar(30),       
 @CurrencyAccountID varchar(30),       
 @ProfitOrLossAccountID varchar(30),       
 @PostingLimit money=0,      
       
 @AdditionalData text      
       
AS      
 
 SET NOCOUNT ON
 
 if @ScrollNo = 0       
 BEGIN       
  select @ScrollNo=isnull(max(ScrollNo),0)+1 from t_BureaDeChange       
  Where (OurBranchID=@OurBranchID)       
 END       
       
 if @Amount > @PostingLimit      
 begin      
  set @Supervision = '*'      
 end      
       
 Insert Into t_BureaDeChange       
 (ScrollNo,OurBranchID,CurrencyID,SellOrBuy,wDate,TrxType,ChequeID,Amount,ForeignAmount,ExchangeRate,Commission,      
 CommRate,DescriptionID,Description,NetAmount,ProfitOrLoss,CashAccountID,CommAccountID,      
 ProfitOrLossAccountID,CurrAccountID,Supervision,OperatorID,SupervisorID,AdditionalData)       
 Values       
 (@ScrollNo,@OurBranchID,@CurrencyID,@SellOrBuy,@wDate,@TransactionType,@ChequeID,@Amount,@ForeignAmount,@ExchangeRate,      
 @Commission,@CommRate,@DescriptionID,@Description,@NetAmount,@ProfitOrLoss,@CashAccountID,@CommissionAccountID,      
 @ProfitOrLossAccountID,@CurrencyAccountID,@Supervision,@OperatorID,@SupervisorID,@AdditionalData)       
 
 if @Supervision = 'C'
 begin
    
    declare @IsCredit int
    declare @VoucherID int
    declare @LocalCurrency varchar(30)
    
    select @VoucherID = ISNULL(MAX(VoucherID),0) + 1 from t_GLTransactions where OurBranchID = @OurBranchID
    select @LocalCurrency = ISNULL(LocalCurrency,'') from t_GlobalVariables where OurBranchID = @OurBranchID
    
    if @SellOrBuy = 'B'  
      set @IsCredit = 1
    else
      set @IsCredit = 0
    
    insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],  
    CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,  
    Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType, GLID)  
    
    values (@OurBranchID, @CurrencyAccountID, @VoucherID, '1', @wDate, @wDate, @DescriptionID,@Description,
    @CurrencyID, @Amount,@ForeignAmount,@ExchangeRate, @IsCredit, 'Bureau', 'A',
    @OperatorID, @SupervisorID, '', @ScrollNo, '1', @AdditionalData, '1', 'B', @CashAccountID)
    
    if @Commission > 0
    begin
       
       insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],  
       CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,  
       Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType)  
       
       values (@OurBranchID, @CommissionAccountID, @VoucherID, '2', @wDate, @wDate, @DescriptionID, @Description,
       @LocalCurrency, @Commission, 0, 1, 1, 'Bureau', 'L', @OperatorID, @SupervisorID, '', @ScrollNo, '2', '', '0', 'B')
       
    end
    
    set @VoucherID = @VoucherID + 1
    
    set @Description = @Description + ' - Scroll # ' + CONVERT(varchar(30),@ScrollNo)
	set @Description = @Description + ' - Teller Account ID ' + @CashAccountID
	set @Description = @Description + ' - Foreign Amount ' + CONVERT(varchar(30),@ForeignAmount)
	set @Description = @Description + ' - Amount ' + CONVERT(varchar(30),@Amount)
	set @Description = @Description + ' - Commission ' + CONVERT(varchar(30),@Commission)
	set @Description = @Description + ' - Net Local Amount ' + CONVERT(varchar(30),@NetAmount)
	
    if @SellOrBuy = 'B'  
    begin
      set @IsCredit = 0
      set @DescriptionID = 'BC0'
      set @NetAmount = @Amount + @Commission
    end
    else
    begin
      set @IsCredit = 1
      set @DescriptionID = 'BC1'
      set @NetAmount = @Amount - @Commission
    end
    
    insert into t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description],  
    CurrencyID, Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,  
    Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType)  
    
    values (@OurBranchID, @CashAccountID, @VoucherID, '1', @wDate, @wDate, @DescriptionID, @Description,
    @LocalCurrency, @NetAmount, 0, 1, @IsCredit, 'Bureau', 'L', @OperatorID, @SupervisorID, '', @ScrollNo, '1', '', '0', 'B')
    
 end
 
 select 'Ok' AS RetStatus, @ScrollNo AS ScrollNo, @Supervision AS Supervision