﻿CREATE PROCEDURE [dbo].[spc_GetCB950Transactions]
( 
 @OurBranchID varchar(30), 
 @AccountID varchar(30), 
 @sDate Datetime
)
AS 
 
 SET NOCOUNT ON 
 
 DECLARE @sdDate Datetime, @eDate Datetime
 set @sdDate = CONVERT(VARCHAR(10),@sDate,101);
 set @sDate = @sdDate
 set @eDate = @sDate

 DECLARE @BaseCurrency as NVarchar(30) 
 DECLARE @CurrencyID as NVarchar(30)

 SELECT @BaseCurrency = LocalCurrency FROM t_GlobalVariables WHERE OurBranchID = @OurBranchID
 SELECT @CurrencyID = CurrencyID FROM t_Account WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID

 if @CurrencyID = @BaseCurrency
 begin
    SELECT SUBSTRING(CONVERT(VARCHAR(8),ValueDate,112),3,8) AS ValueDate, SUBSTRING(CONVERT(VARCHAR(8),wDate,112),3,8) AS PostingDate,
	TrxType, Amount AS TrxAmount, CONVERT(varchar(10),ScrollNo)+CONVERT(varchar(10),SerialNo) AS TrxRefNo
    FROM t_Transactions 
    WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID 
    AND AccountType = 'C' AND [Status] <> 'R' 
    AND convert(varchar(10) , wDate,101) >= @sDate AND convert(varchar(10) , wDate,101) <= @eDate 
    order by TrxTimeStamp
 end
 else
 begin
    SELECT SUBSTRING(CONVERT(VARCHAR(8),ValueDate,112),3,8) AS ValueDate, SUBSTRING(CONVERT(VARCHAR(8),wDate,112),3,8) AS PostingDate,
	TrxType, ForeignAmount AS TrxAmount, CONVERT(varchar(10),ScrollNo)+CONVERT(varchar(10),SerialNo) AS TrxRefNo
    FROM t_Transactions 
    WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID 
    AND AccountType = 'C' AND [Status] <> 'R' 
    AND convert(varchar(10) , wDate,101) >= @sDate AND convert(varchar(10) , wDate,101) <= @eDate 
    order by TrxTimeStamp
 end