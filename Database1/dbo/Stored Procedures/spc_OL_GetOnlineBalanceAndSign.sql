﻿CREATE Procedure [dbo].[spc_OL_GetOnlineBalanceAndSign]          
  @OurBranchID varchar(30),          
  @AccountID  varchar(30)          
As            
           
 Set nocount on          
            
 DECLARE @IsAccountRestrict as bit            
 DECLARE @DayDepositBal as money            
 DECLARE @DayDrawableBal as money            
 DECLARE @DayDepositBalUtilize as money            
 DECLARE @DayDrawableBalUtilize as money            
 DECLARE @TrxType as Char(1)              
            
 DECLARE @wDate as Datetime            
 DECLARE @AvgBalance as money            
 DECLARE @SigInst as Varchar(5000)            
 DECLARE @OtherInst as Varchar(5000)            
            
 SET @IsAccountRestrict = 1            
 SET @DayDepositBal = -1            
 SET @DayDrawableBal = -1            
 SET @TrxType = 'B'            
            
 SELECT @AvgBalance = 0, @SigInst = '', @OtherInst = ''            
            
 SELECT @IsAccountRestrict = IsAccountRestrict, @DayDepositBal = DayDepositBal, @DayDrawableBal = DayDrawableBal            
 From t_OL_GlobalVariables WHERE OurBranchID = @OurBranchID
            
 IF (SELECT Count(*) From t_Account WHERE OurBranchID = @OurBranchID AND AccountID   = @AccountID )> 0            
  BEGIN            
            
   SELECT @AvgBalance = IsNull(AverageBalance,0)            
   From t_AccountAverageBalances            
   WHERE OurBranchID = @OurBranchID AND AccountID   = @AccountID             
   AND Convert(Varchar(10),Month,101) = (select convert(varchar(10), dateadd(dd,-day(dateadd(mm,-1,workingdate)),dateadd(mm,-1,workingdate)+1),101)           
   from t_last WHERE OurBranchID = @OurBranchID )            
               
   SELECT @SigInst = IsNull(SignatureInstructions,''), @OtherInst = IsNull(OtherInstructions,'')             
   FROM t_AccountFunctions          
   WHERE OurBranchID = @OurBranchID AND AccountID   = @AccountID            
            
   IF @IsAccountRestrict = 1            
    BEGIN            
     SET @TrxType = 'N'            
              
     IF (SELECT Count(*) From t_OL_Accounts             
       WHERE OurBranchID = @OurBranchID AND ACcountID = @AccountID AND AccountType = 'C' ) > 0            
      BEGIN            
       SELECT @TrxType = TrxType, @DayDepositBal=DayDepositBal, @DayDrawableBal = DayDrawableBal            
       FROM t_OL_Accounts            
       WHERE OurBranchID = @OurBranchID AND ACcountID = @AccountID AND AccountType = 'C'                 
      END            
    END            
             
   SELECT @DayDepositBalUtilize = IsNull(Sum(Amount),0)             
   FROM t_OnLineCashTransaction            
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND TrxType = 'C'             
     AND Not DescriptionID Like 'TR%'            
               
   SELECT @DayDrawableBalUtilize = IsNull(Sum(Amount),0)             
   FROM t_OnLineCashTransaction            
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND TrxType = 'D'              
     AND Not DescriptionID Like 'TR%'            
              
   SELECT A.Name, A.ProductID, A.Status, A.CloseDate, A.CloseDate AccountCloseDate, '0' AS MinBalance, B.ClearBalance,           
   B.Limit, B.Effects, B.ShadowBalance, B.FreezeAmount, B.IsFreezed, p.MinBalance ProductMinBalance, B.Reminder,     
   C.CurrencyID, C.MeanRate, P.Description AS ProductName, P.CanGoInCredit, P.CanGoInDebit, P.IsFreezed AS ProductFreezed,     
   isnull(P.AllowCredit,'0') AllowCredit, isnull(P.AllowDebit,'0') AllowDebit,             
   isnull(A.AllowCreditTransaction,'0') AllowCreditTransaction, isnull(A.AllowDebitTransaction,'0') AllowDebitTransaction,     
   @TrxType TrxType, @DayDepositBal DayDepositBal, @DayDrawableBal DayDrawableBal, @DayDepositBalUtilize DayDepositBalUtilize,     
   @DayDrawableBalUtilize DayDrawableBalUtilize,            
   @AvgBalance AverageBalance, @SigInst SignatureInstruction, @OtherInst OtherInstruction            
       
   FROM t_Account A          
   INNER JOIN t_AccountBalance B ON A.OurBranchID = B.OurBranchID AND A.AccountID = B.AccountID          
   INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND A.ProductID = P.ProductID          
   INNER JOIN t_Currencies C ON P.OurBranchID = C.OurBranchID AND P.CurrencyID = C.CurrencyID    
   WHERE A.OurBranchID = @OurBranchID AND A.AccountID   = @AccountID            
             
  END            
 ELSE            
  SELECT AccountID From t_Account            
  WHERE OurBranchID = @OurBranchID AND AccountID   = @AccountID