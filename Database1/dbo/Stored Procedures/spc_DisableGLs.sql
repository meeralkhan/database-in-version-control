﻿CREATE   PROCEDURE [dbo].[spc_DisableGLs]     
AS         
SET NOCOUNT ON         
       
INSERT INTO t_DisabledGLS  (OurBranchID, a.AccountID, a.wDate, a.AccountClass, a.Description, a.CurrencyID, a.GLOwnerID,      
a.OldIsPosting, a.IsPosting, a.LogID)      
select a.OurBranchID,a.AccountID, c.WORKINGDATE wDate,a.AccountClass,a.Description,a.CurrencyID,a.GLOwnerID,     
a.IsPosting OldIsPosting,0 IsPosting,0 LogID from t_gl a inner join t_Last c on a.OurBranchID = c.OurBranchID       
where a.OurBranchID+a.AccountID not in (select b.OurBranchID+b.AccountID from t_GLTransactions b where a.OurBranchID = b.OurBranchID and b.Date > DATEADD(month, -6, c.WORKINGDATE)) and      
a.AccountClass = 'P' and IsPosting = 1 and a.CreateTime < DATEADD(month, -6, c.WORKINGDATE) and a.Balance = 0     
     
update a set a.IsPosting = 0      
from t_gl a      
inner join t_DisabledGLS b on a.OurBranchID = b.OurBranchID and a.AccountID = b.AccountID      
inner join t_Last c on b.wDate = c.WORKINGDATE and c.OurBranchID = b.ourbranchid     
     
SELECT 'Ok' AS RetStatus