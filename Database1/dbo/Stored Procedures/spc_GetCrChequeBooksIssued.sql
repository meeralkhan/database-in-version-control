﻿CREATE PROCEDURE [dbo].[spc_GetCrChequeBooksIssued] (        
 @OurBranchID varchar(30),              
 @AccountIDFrom varchar(30)='',              
 @AccountIDTo varchar(30)='',              
 @IssuedFrom datetime,              
 @IssuedTo datetime             
)             
AS                
set nocount on          
        
IF (@AccountIDFrom <> '') AND (@AccountIDTo <> '')        
BEGIN         
 SELECT C.AccountID, A.CurrencyID, C.DateIssued, C.ChequeStart, C.ChequeEnd, C.CreateBy as OperatorID,         
 C.SuperviseBy as SupervisorID, A.Name        
         
 FROM t_Cheque C        
 INNER JOIN t_Account A ON C.OurBranchID = A.OurBranchID AND C.AccountID = A.AccountID        
 WHERE C.OurBranchID = @OurBranchID AND (C.AccountID BETWEEN @AccountIDFrom AND @AccountIDTo)   
 AND (C.DateIssued BETWEEN @IssuedFrom AND @IssuedTo)         
END              
ELSE IF (@AccountIDFrom <> '')      
BEGIN                         
 SELECT C.AccountID, A.CurrencyID, C.DateIssued, C.ChequeStart, C.ChequeEnd, C.CreateBy as OperatorID,         
 C.SuperviseBy as SupervisorID, A.Name        
 FROM t_Cheque C        
 INNER JOIN t_Account A ON C.OurBranchID = A.OurBranchID AND C.AccountID = A.AccountID           
 WHERE C.OurBranchID = @OurBranchID AND (C.AccountID = @AccountIDFrom)   
 AND (C.DateIssued BETWEEN @IssuedFrom AND @IssuedTo)        
END                   
ELSE                 
BEGIN                         
 SELECT C.AccountID, A.CurrencyID, C.DateIssued, C.ChequeStart, C.ChequeEnd, C.CreateBy as OperatorID,         
 C.SuperviseBy as SupervisorID, A.Name        
 FROM t_Cheque C        
 INNER JOIN t_Account A ON C.OurBranchID = A.OurBranchID AND C.AccountID = A.AccountID         
 WHERE C.OurBranchID = @OurBranchID AND (C.AccountID = @AccountIDTo)   
 AND (C.DateIssued BETWEEN @IssuedFrom AND @IssuedTo)        
END