﻿CREATE PROCEDURE [dbo].[sp_ATM_AddEditCreditTransaction]
(    
 @OurBranchID            varchar(30),     
 @WithdrawlBranchID      varchar(30)='',  
 @CustomerBranchID       varchar(30)='',     
 @ATMID                  varchar(30),     
 @AccountID              varchar(30)='',    
 @AccountID2             varchar(30),    
 @Amount                 money,    
 @USDAmount              money=0,    
 @OtherCurrencyAmount    money=0,    
 @USDRate                money=0,    
 @Supervision            char(1),     
 @RefNo                  varchar(50),    
 @PHXDate                datetime,    
 @MerchantType           varchar(30)='',    
 @AckInstIDCode          varchar(30)='',    
 @Currency               varchar(30),    
 @NameLocation           varchar(50),    
 @ProcCode                varchar(50)='',  
 @SettlmntAmount         numeric(18,6)=0,  
 @ConvRate               decimal(15,9)=0,  
 @AcqCountryCode         varchar(3)='',  
 @CurrCodeTran           varchar(3)='',  
 @CurrCodeSett           varchar(3)='',  
 @ForwardInstID       varchar(10)='',  
 @MessageType       varchar(10)='',  
 @OrgTrxRefNo       varchar(100)='',  
  
 @POSEntryMode    varchar(3)='',  
 @POSConditionCode   varchar(2)='',  
 @POSPINCaptCode   varchar(2)='',  
 @RetRefNum     varchar(15)='',  
 @CardAccptID    varchar(15)='',  
 @VISATrxID     varchar(15)='',  
 @CAVV      char(1)='',  
 @ResponseCode    varchar(2)='',  
 @ExtendedData    varchar(2)='',    
 
 /* TO BE UPDATE LATER */  
 @ChannelId     nvarchar(30)='4',  
 @ChannelRefID    nvarchar(100)='',  
 @AuthIDResp varchar(6)='',
 @NewRecord              bit=1    
)  
AS  
BEGIN  
  
 SET NOCOUNT ON    
  
 DECLARE @BankShortName as varchar(30)    
  SET @BankShortName = 'N/A'    
 DECLARE @mScrollNo as int    
 DECLARE @mSerialNo as int    
  SET @mSerialNo = 0  
 DECLARE @mAccountID as varchar(30)    
 DECLARE @vAccountID as varchar(30)    
 DECLARE @mAccountName as varchar(50)    
 DECLARE @GLAccountName as varchar(50)    
 DECLARE @mAccountType as char    
 DECLARE @mProductID as varchar(30)    
 DECLARE @mCurrencyID as varchar(30)    
 DECLARE @mIsLocalCurrency as bit    
  SET @mIsLocalCurrency=1     
 DECLARE @mwDate as datetime    
 DECLARE @mTrxType as char    
 DECLARE @STAN as varchar(30)  
  SET @STAN = RIGHT(@RefNo,6)  
 DECLARE @mGLID as varchar(30)    
  SET @mGLID = ''     
 DECLARE @mForeignAmount as money    
  SET @mForeignAmount=0     
 DECLARE @mExchangeRate as money    
  SET @mExchangeRate=1     
 DECLARE @mDescriptionID as varchar(30)    
  SET @mDescriptionID='000'     
 DECLARE @mDescription as varchar(500)    
 DECLARE @mRemarks as varchar(500)    
 DECLARE @mBankCode as varchar(30)    
 DECLARE @mOperatorID as varchar(30)    
  SET @mOperatorID = 'ATM-' + @ATMID    
 DECLARE @mSupervisorID as varchar(30)    
  SET @mSupervisorID = 'N/A'  
 DECLARE @ATMChargeID as varchar(10)
   
 --Checking Variables  
 DECLARE @vCount1 as int    
 DECLARE @vCount4 as smallint
  SET @vCount1 = 0
  SET @vCount4 = 0
 DECLARE @vClearBalance as money    
 DECLARE @vClearBalance2 as money    
 DECLARE @vClearedEffects as money    
 DECLARE @vFreezeAmount as money    
 DECLARE @vMinBalance as money    
 DECLARE @vIsClose as char    
 DECLARE @vAreCheckBooksAllowed as bit    
 DECLARE @vAllowCreditTransaction as bit    
 DECLARE @vBranchName as varchar(50)    
  SET @vBranchName = 'N/A'    
 DECLARE @Status as int  
 DECLARE @mLastEOD as datetime    
 --DECLARE @mWorkingDate as datetime    
 DECLARE @mCONFRETI as datetime    
 DECLARE @mWithdrawlBranchName as varchar(50)  
 DECLARE @Super as varchar(1)    
 DECLARE @AccountDigit as smallint   
  SET @AccountDigit= 16  
 DECLARE @LocalCurrencyID as varchar(3)  
 DECLARE @LocalCurrency as varchar(3)  
 DECLARE @IsCredit int     
 DECLARE @VoucherID int    
 DECLARE @GLControl nvarchar(30)    
 DECLARE @CurrencyCode as varchar(30)  
  SET @CurrencyCode = ''  
  
 DECLARE @vAccountLimit as money  
 DECLARE @vCheckLimit as bit  
  SET @vCheckLimit = 0  
 DECLARE @vODLimitAllow as bit  
  SET @vODLimitAllow = 0  
 DECLARE @AvailableBalance as money        
 DECLARE @Value as varchar(2)    
  SET @value='00'    
 DECLARE @HoldStatus as char(1)  
 DECLARE @HoldAmount as money  
 DECLARE @FreezeAmount as money  
 DECLARE @remAmount as numeric(18,6)  
 DECLARE @mRefNo as varchar(50)  
 DECLARE @mHoldStatus as char(1)  
 DECLARE @sCount as int  
  SET @sCount = 0  
 DECLARE @cCount as int  
  SET @cCount = 0  
 DECLARE @mCount as int  
  SET @mCount = 0
 DECLARE @IsClearingTrx as bit  
  SET @IsClearingTrx = 0  
 DECLARE @IsLocalCurrency as bit  
  SET @IsLocalCurrency = 0   
 DECLARE @mRetRefNum as varchar(15)  
 DECLARE @ForeignAmount as money  
 DECLARE @IsGCC as bit  
  SET @IsGCC = 0  
 DECLARE @ValueDate as datetime
 DECLARE @MobileNo nvarchar(30)
 DECLARE @Email nvarchar(100)
 DECLARE @rCurrencyID nvarchar(30)
 DECLARE @ClientID nvarchar(30)
 DECLARE @NarrationID nvarchar(30)
  SET @MobileNo = ''
  SET @Email = ''
  SET @rCurrencyID = ''
  SET @ClientID = ''
  SET @NarrationID = ''
 DECLARE @CountryCode as nvarchar(3)
  SET @CountryCode = ''
 DECLARE @TrxDetails as varchar(50)
  SET @TrxDetails = 'Credit Tran'
 DECLARE @VatReferenceID as varchar(30)  
  
 SELECT @Status = -1    
 
 SELECT @rCurrencyID=ISNULL(CurrencyID,'') FROM t_Currencies WHERE OurBranchID = @OurBranchID AND CurrencyCode = @CurrCodeTran
  
 SELECT @vBranchName=Heading2,@mBankCode=OurBankID FROM t_GlobalVariables WHERE (OurBranchID = @OurBranchID)  
 SELECT @BankShortName = BankShortName, @vODLimitAllow=ODLimitAllow From t_ATM_Banks WHERE (OurBranchID = @OurBranchID AND BankIMD = @AckInstIDCode)  
 SELECT @LocalCurrencyID=LocalCurrencyID,@LocalCurrency=LocalCurrency,@vCheckLimit=CheckLimit FROM t_ATM_GlobalVariables WHERE OurBranchID = @OurBranchID  
 SELECT @mLastEOD=LastEOD,@mwDate=WorkingDate,@mCONFRETI=CONFRETI FROM t_Last WHERE (OurBranchID = @OurBranchID)  
 
 select @IsGCC=ISNULL(IsGCC,0), @CountryCode=ISNULL(CountryCode,'') from t_Country where OurBranchID = @OurBranchID AND ShortName = RIGHT(@NameLocation, 2)
 
 if (@AuthIDResp = '')
 begin
  SET @AuthIDResp = '0'
 end
  
 IF (@MessageType = '0220' OR @MessageType = '0221')  
 BEGIN  
  IF (@ForwardInstID = '500000')  
  BEGIN
   IF (@VISATrxID != '')  
   BEGIN
    SET @IsClearingTrx = 1
	SET @TrxDetails = 'Credit Tran II'
   END
   ELSE
   BEGIN
    SET @IsClearingTrx = 0
	SET @TrxDetails = 'Credit Tran'
   END
  END
  ELSE  
  BEGIN
   SET @IsClearingTrx = 0  
   SET @TrxDetails = 'Credit Tran'
  END
 END  
 
 if (@CountryCode = '784')      
 BEGIN      
  SET @ATMChargeID = '600000'    
 END      
 else if (@IsGCC = 1)      
 BEGIN      
  SET @ATMChargeID = 'GCC'      
 END      
 ELSE      
 BEGIN      
  SET @ATMChargeID = '500000'      
 END      

 if (@IsGCC = 1 OR @CountryCode = '784') 
 BEGIN  
  if (@ProcCode = '20') -- Credit Voucher/ Merchandise Return Clearing  
  BEGIN
   --SET @mDescriptionID = '813'  --old
   SET @mDescriptionID = '100240'  --new
   SET @mDescription = 'Credit Voucher/ Merchandise Return Transaction Local'
   --SET @mRemarks='ATM('+@ATMID+') CR VOUCH-MERCH RET LOCAL TRX From : ' + @NameLocation + '-' + convert(varchar(20), @PHXDate, 120)
   SET @mRemarks=''
  END
  ELSE if (@ProcCode = '23') -- Credit Transaction  
  BEGIN
   --SET @mDescriptionID = '811'  --old
   SET @mDescriptionID = '100220'  --new
   SET @mDescription = 'Credit OCT Transaction Local'
   --SET @mRemarks='Being Amount of LOCAL CREDIT TRX From ' + @NameLocation + '-' + convert(varchar(20), @PHXDate, 120)  
   SET @mRemarks='' 
  END
  ELSE if (@ProcCode = '26') -- OCT Transaction  
  BEGIN
   --SET @mDescriptionID = '811'  --old
   SET @mDescriptionID = '100220'  --new
   SET @mDescription = 'Credit OCT Transaction Local'
   --SET @mRemarks='Being Amount of LOCAL CREDIT TRX From ' + @NameLocation + '-' + convert(varchar(20), @PHXDate, 120)  
   SET @mRemarks=''
  END
  ELSE  
  BEGIN
   --SET @mDescriptionID = '811'  --old
   SET @mDescriptionID = ''  --new
   SET @mDescription = ''
   SET @mRemarks=''
  END
 END  
 ELSE  
 BEGIN  
  if (@ProcCode = '20') -- Credit Voucher/ Merchandise Return Clearing  
  BEGIN
   --SET @mDescriptionID = '814'  --old
   SET @mDescriptionID = '100250'  --new
   SET @mDescription = 'Credit Voucher/ Merchandise Return Transaction International'
   --SET @mRemarks='Being Amount of CR VOUCH-MERCH RET INT. TRX From : ' + @NameLocation + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')'
   SET @mRemarks=''
   
  END
  ELSE if (@ProcCode = '23') -- Credit Transaction  
  BEGIN
   --SET @mDescriptionID = '812'  --old
   SET @mDescriptionID = '100230'  --new
   SET @mDescription = 'Credit OCT Transaction International'
   --SET @mRemarks='Being Amount of CR INT. TRX From : ' + @NameLocation + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')'
   SET @mRemarks=''
  END
  ELSE if (@ProcCode = '26') -- OCT Transaction  
  BEGIN
   --SET @mDescriptionID = '812'  --old
   SET @mDescriptionID = '100230'  --new
   SET @mDescription = 'Credit OCT Transaction International'
   --SET @mRemarks='Being Amount of CR INT. TRX From : ' + @NameLocation + '($' + convert(varchar(15),@USDAmount) + '@' + convert(varchar(15),@USDRate) + ')'
   SET @mRemarks=''
  END
  ELSE  
  BEGIN
   --SET @mDescriptionID = '812'  --old
   SET @mDescriptionID = ''  --new
   SET @mDescription = ''
   SET @mRemarks=''
  END
 END  
 
 SET @NarrationID = @mDescriptionID
 SET @ValueDate = @mwDate
 
 IF (@ExtendedData = 'DA') -- For Declinals the account id has tokanized PAN
 begin
  SELECT @AccountID2=ISNULL(AccountID,'') from t_DebitCards where OurBranchID = @OurBranchID AND DebitCardToken = @AccountID2
 END
 
 IF len(@AccountID2) <>  @Accountdigit  
 BEGIN --FLD Error    
  /** Rejection change STARTS 06/04/2021 **/
  
  INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
  VALUES (@OurBranchID, @AccountID2, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, 
  @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @mDescription, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '09', 'Field Error', @TrxDetails, @AuthIDResp, @ExtendedData)
  
  SELECT @Status=9, @AvailableBalance = 0, @vClearBalance = 0  
  SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @Value As value, '784' as CurrencyCode, @AuthIDResp as AuthIDResp, @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
  RETURN (1)   
  
  /** Rejection change ENDS 06/04/2021 **/
  
 END  

 SELECT @vCount1=count(*) From t_Account WHERE (OurBranchID = @OurBranchID) AND (AccountID = @AccountID2)  
 IF @vCount1=0  
 BEGIN  --INVALID_Account  
  /** Rejection change STARTS 06/04/2021 **/
   
  INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp)
  VALUES (@OurBranchID, @AccountID2, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @mDescription, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '02', 'Invalid Account', @TrxDetails, @AuthIDResp)
  
  SELECT @Status = 2, @AvailableBalance = 0, @vClearBalance = 0  
  SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, '00' As value, '784' as CurrencyCode, @AuthIDResp as AuthIDResp, @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
  RETURN (1)  
  
  /** Rejection change ENDS 06/04/2021 **/
 END   
 
 SELECT @mAccountName=B.Name,@mProductID=B.ProductID,@mCurrencyID=P.CurrencyID,@mAccountType='C',@vClearBalance=ISNULL(B.ClearBalance,0),  
 @vClearedEffects=ISNULL(B.Effects,0),@vFreezeAmount=ISNULL(B.FreezeAmount,0),@vMinBalance=ISNULL(P.MinBalance,0),@vIsClose=ISNULL(A.Status,''),  
 @vAreCheckBooksAllowed=P.AreCheckBooksAllowed,@vAllowCreditTransaction=ISNULL(A.AllowCreditTransaction,'0'),@CurrencyCode = ISNULL(c.CurrencyCode,''),  
 @value=ISNULL(p.productatmcode,'00'),@vAccountLimit=ISNULL(B.Limit,0),@MobileNo=ISNULL(cust.MobileNo,''),@Email=ISNULL(cust.Email,''),@ClientID=cust.ClientID
 FROM t_AccountBalance B   
 INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND B.ProductID = A.ProductID   
 INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND A.ProductID = P.ProductID   
 INNER JOIN t_Currencies C ON A.OurBranchID = C.OurBranchID AND A.CurrencyID = C.CurrencyID  
 INNER JOIN t_Customer cust ON A.OurBranchID = cust.OurBranchID AND A.ClientID = cust.ClientID
 WHERE (A.OurBranchID = @OurBranchID) AND (A.AccountID = @AccountID2)  
  
 --exec @vAccountLimit = [fnc_GetLimit] @OurBranchID, @AccountID2  
  
 IF @vAccountLimit > 0 AND @vCheckLimit = 1 AND @vODLimitAllow = 1  
 BEGIN  
  SET @vClearBalance=(@vClearBalance+@vAccountLimit)  
 END  
  
 if (@LocalCurrencyID = @CurrencyCode)  
 BEGIN  
  SET @IsLocalCurrency = 1  
 END  
 
 IF (@ExtendedData = 'DA')
 begin
 
  Insert into t_ENDeclineAdvices (OurBranchID, AccountID, RetRefNum, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, 
  ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, CardAccptID, CardAccptNameLoc, 
  DescriptionID, VISATrxID, CAVV, ResponseCode)
  VALUES        
  (        
   @OurBranchID,@AccountID2,@RetRefNum,@STAN,@PHXDate,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@Amount,@mForeignAmount,@mExchangeRate,@ConvRate,@MerchantType,
   @AcqCountryCode,@ForwardInstID,@ATMID,@CurrCodeTran,@CurrCodeSett,@SettlmntAmount,@ProcCode,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@CardAccptID,
   @NameLocation,@NarrationID,@VISATrxID,@CAVV,@ResponseCode    
  )
 
  SELECT @Status    = 0,       
  @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,      
  @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100      
      
  SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value, @CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp,@MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
  RETURN 0;    
  
 end
 
 IF @mLastEOD = @mwDate or @mCONFRETI = '1/1/9999'    
 BEGIN --INVALID_DATE    
  /** Rejection change STARTS 06/04/2021 **/
   
  INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
  VALUES (@OurBranchID, @AccountID2, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @mDescription, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '95', 'Day End', @TrxDetails, @AuthIDResp, @ExtendedData)
  
  SELECT @Status=95, @AvailableBalance = 0, @vClearBalance = 0  
  SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @Value As value, '784' as CurrencyCode, @AuthIDResp as AuthIDResp, @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
  RETURN (1)  
  
  /** Rejection change ENDS 06/04/2021 **/
 END  
   
 IF @Amount <=0    
 BEGIN  --BAD AMOUNT    
  /** Rejection change STARTS 06/04/2021 **/
   
  INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
  VALUES (@OurBranchID, @AccountID2, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @mDescription, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '48', 'Bad Amount', @TrxDetails, @AuthIDResp, @ExtendedData)
  
  SELECT @Status=48, @AvailableBalance = 0, @vClearBalance = 0  
  SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @Value As value, '784' as CurrencyCode, @AuthIDResp as AuthIDResp, @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
  RETURN (1)  
  
  /** Rejection change ENDS 06/04/2021 **/
 END  
 
 /** If its a clearing trx then release hold -- STARTS **/  
  
 IF (@IsClearingTrx = 1)  
 BEGIN  
  SELECT @mCount=ISNULL(count(*),0)
  FROM t_ATM_HoldTrxs WHERE (OurBranchID=@OurBranchID)   
  AND (AccountID=@AccountID2) AND HoldStatus IN ('H','P') AND (RefNo IN (@OrgTrxRefNo, @OrgTrxRefNo+'|CHGS1', @OrgTrxRefNo+'|CHGS2') AND VISATrxID=@VISATrxID)   
  group by HoldStatus  
  
  SELECT @remAmount = SUM(ISNULL(RemainingAmount,0)) FROM t_ATM_HoldTrxs WHERE (OurBranchID=@OurBranchID) AND (AccountID=@AccountID2)   
   AND (RefNo = @OrgTrxRefNo AND VISATrxID=@VISATrxID) AND HoldStatus IN ('H','P') group by HoldStatus  
  
  IF @mCount = 0  
  BEGIN --NO TRX ON ACCOUNT  
   
   /** Rejection change STARTS 06/04/2021 **/
   
   INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
   VALUES (@OurBranchID, @AccountID2, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @mDescription, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '21', 'No Transaction on Account', @TrxDetails, @AuthIDResp, @ExtendedData)
   
   SELECT @Status = 21,  
   @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,  
   @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100  
   SELECT @Status As Status , @vClearBalance As ClearBalance , @AvailableBalance As AvailableBalance , @value As [value], @CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp, @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
   RETURN (1)   
   
   /** Rejection change ENDS 06/04/2021 **/
  END  
  
  if (@CurrCodeSett = '784' AND @CurrCodeTran = '784') --Validate amount only if its a local currency trx..    
  BEGIN  
   IF @Amount <> @remAmount  
   BEGIN --BAD AMOUNT  
    /** Rejection change STARTS 06/04/2021 **/
    
    INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
    VALUES (@OurBranchID, @AccountID2, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @mDescription, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '48', 'Bad Amount', @TrxDetails, @AuthIDResp, @ExtendedData)
    
    SELECT @Status = 48,  
    @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,  
    @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100  
       
    SELECT @Status As Status , @vClearBalance As ClearBalance , @AvailableBalance As AvailableBalance , @value As [value], @CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp, @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
    RETURN (1)   
    
    /** Rejection change ENDS 06/04/2021 **/
   END  
  END  
  
  Select * Into #TempCreditTransaction FROM t_ATM_HoldTrxs WHERE (OurBranchID=@OurBranchID) AND (AccountID=@AccountID2)   
  AND (RefNo IN (@OrgTrxRefNo,@OrgTrxRefNo+'|CHGS1',@OrgTrxRefNo+'|CHGS2') AND VISATrxID=@VISATrxID) AND HoldStatus IN ('H','P')  
  
  SELECT @cCount=COUNT(*) from #TempCreditTransaction  
    
  WHILE (@cCount <> @sCount)  
  BEGIN  
   SET @sCount = @sCount + 1  
     
   --Clear Hold Trx Status  
   SELECT @mAccountID=AccountID, @mRefNo=RefNo, @mHoldStatus=HoldStatus, @ForeignAmount=ForeignAmount, @mRetRefNum=RetRefNum, @ValueDate=convert(varchar(10), wDate, 120)    
   FROM (  
    SELECT AccountID, RefNo, HoldStatus, ForeignAmount, RetRefNum, wDate, ROW_NUMBER() OVER (ORDER BY RefNo) AS RowNum  
    FROM #TempCreditTransaction  
   ) AS oTable  
   WHERE oTable.RowNum = @sCount  
  
   UPDATE t_ATM_HoldTrxs SET HoldStatus = 'C', ReleaseDate = @mwDate + ' ' + convert(varchar(12), GetDate(), 114)  
   WHERE OurBranchID = @OurBranchID AND AccountID = @mAccountID AND HoldStatus = @mHoldStatus AND RefNo = @mRefNo  
  
   if (@ProcCode = '20') -- Credit Voucher/ Merchandise Return Clearing  
   BEGIN  
    --Get Hold Amount  
    SELECT @HoldAmount=Amount FROM t_AccountFreezeCredit  
    WHERE OurBranchID=@OurBranchID AND AccountID=@mAccountID AND IsFreeze = 1 AND ATMTrxHoldRef = @mRefNo  
  
 --update account freeze to unfreeze  
    Update t_AccountFreezeCredit SET IsFreeze = 0, UnFreezedDate = @mwDate + ' ' + convert(varchar(12), GetDate(), 114), ReleaseComments = @mDescription  
    WHERE OurBranchID = @OurBranchID AND AccountID = @mAccountID AND ATMTrxHoldRef = @mRefNo  
  
    update t_Clearing Set Status = 'C'  
    WHERE OurBranchID = @OurBranchID AND AccountID = @mAccountID AND ChequeID = @RetRefNum  
  
    IF @IsLocalCurrency = 1  
    BEGIN  
     Update t_AccountBalance Set Effects =  ISNULL(Effects,0) - @HoldAmount WHERE OurBranchID = @OurBranchID and AccountID = @mAccountID  
    END  
    ELSE  
    BEGIN  
     Update t_AccountBalance Set Effects =  ISNULL(Effects,0) - @ForeignAmount, LocalEffects = ISNULL(LocalEffects,0) - @HoldAmount  
     Where  OurBranchID = @OurBranchID and AccountID = @mAccountID  
    END  
   END  
   ELSE  
   BEGIN  
    --Get Hold Amount  
    SELECT @HoldAmount=Amount FROM t_AccountFreeze   
    WHERE OurBranchID=@OurBranchID AND AccountID=@mAccountID AND IsFreeze = 1 AND ATMTrxHoldRef = @mRefNo  
  
    --update account freeze to unfreeze  
    Update t_AccountFreeze SET IsFreeze = 0, UnFreezedDate = @mwDate + ' ' + convert(varchar(12), GetDate(), 114), ReleaseComments = @mDescription  
    WHERE OurBranchID = @OurBranchID AND AccountID = @mAccountID AND ATMTrxHoldRef = @mRefNo  
       
    Update t_AccountBalance SET FreezeAmount = ISNULL(FreezeAmount,0) - ISNULL(@HoldAmount,0)  
    WHERE OurBranchID = @OurBranchID AND AccountID = @mAccountID  
      
    SELECT @FreezeAmount = FreezeAmount FROM t_AccountBalance WHERE OurBranchID = @OurBranchID AND AccountID = @mAccountID  
      
    IF (@FreezeAmount <= 0)  
    BEGIN  
     Update t_AccountBalance SET IsFreezed = 0  
     WHERE OurBranchID = @OurBranchID AND AccountID = @mAccountID  
    END  
   END  
  END  
  
  /* Log the Updated Balance after releasing freeze */  
      
  Insert Into BalChk   
  (  
   OurBranchID, AccountID, ProductID, [Name], LastUpdateTime, ClearBalance, Limit, Effects, Shadow, FreezeAmt, MinBalance, TotalBalance, AvailableBalance  
  )  
  SELECT OurBranchID, AccountID, ProductID, [Name], @mwDate + ' ' + convert(varchar(12), GetDate(), 114), ISNULL(ClearBalance,0), ISNULL(Limit,0), ISNULL(Effects,0), ISNULL(ShadowBalance,0),   
  ISNULL(FreezeAmount,0), @vMinBalance,   
  (isnull(@vAccountLimit,0)+ISNULL(ClearBalance,0)-ISNULL(Effects,0)),  
  (isnull(@vAccountLimit,0)+ISNULL(ClearBalance,0)-ISNULL(FreezeAmount,0)-@vMinBalance)  
  FROM t_AccountBalance WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID2  
  
  /* Get Latest Balance again.. */  
    
  SELECT @mAccountName=B.Name,@mProductID=B.ProductID,@mCurrencyID=P.CurrencyID,@mAccountType='C',@vClearBalance=ISNULL(B.ClearBalance,0),  
  @vClearedEffects=ISNULL(B.Effects,0),@vFreezeAmount=ISNULL(B.FreezeAmount,0),@vMinBalance=ISNULL(P.MinBalance,0),  
  @vIsClose=ISNULL(A.Status,''),@vAreCheckBooksAllowed=P.AreCheckBooksAllowed,@vAllowCreditTransaction=ISNULL(A.AllowCreditTransaction,'0'),  
  @CurrencyCode = ISNULL(C.CurrencyCode,''),@value=ISNULL(p.productatmcode,'00'),@vAccountLimit=ISNULL(B.Limit,0)
  FROM t_AccountBalance B   
  INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND B.ProductID = A.ProductID   
  INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND A.ProductID = P.ProductID   
  INNER JOIN t_Currencies C ON A.OurBranchID = C.OurBranchID AND A.CurrencyID = C.CurrencyID  
  WHERE (A.OurBranchID = @OurBranchID) AND (A.AccountID = @AccountID2)  
    
  --exec @vAccountLimit = [fnc_GetLimit] @OurBranchID, @AccountID2  
    
  IF @vAccountLimit > 0 AND @vCheckLimit = 1 AND @vODLimitAllow = 1  
  BEGIN  
   SET @vClearBalance=(@vClearBalance+@vAccountLimit)  
  END  
  
  /** If its a clearing trx then release hold -- ENDS **/  
 END   
 ELSE
 BEGIN
  SET @OrgTrxRefNo = @RefNo
 END
  
 SELECT @vCount1=count(*),@Super=Supervision FROM t_ATM_TransferTransaction WHERE (OurBranchID = @OurBranchID) AND   
 (AccountID=@AccountID2) AND (RefNo=@RefNo) AND (AtmID=@ATMID) GROUP BY Supervision  
   
 IF @vCount1>0 and @Super='C'  
 BEGIN  --DUP_TRAN    
  /** Rejection change STARTS 06/04/2021 **/
    
  INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
  VALUES (@OurBranchID, @AccountID2, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @mDescription, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '10', 'Duplicate Transaction', @TrxDetails, @AuthIDResp, @ExtendedData)
  
  SELECT @Status = 10,  
  @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,  
  @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100  
  
  SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @Value As value, @CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp, @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
  RETURN (1)
  
  /** Rejection change ENDS 06/04/2021 **/
 END    
   
 IF @vCount1>0 and @Super='R'    
 BEGIN  --ORIG_ALREADY_REVERSED    
  /** Rejection change STARTS 06/04/2021 **/
    
  INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
  VALUES (@OurBranchID, @AccountID2, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @mDescription, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '35', 'Orig Already Reversed', @TrxDetails, @AuthIDResp, @ExtendedData)
  
  SELECT @Status = 35,  
  @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,  
  @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100  
  
  SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @Value As value, @CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp, @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
  RETURN (1)   
  
  /** Rejection change ENDS 06/04/2021 **/
 END    
   
 SET @sCount = 0  
 SET @cCount = 1  
  
 WHILE (@sCount <> @cCount)    
 BEGIN    
  exec @mScrollNo = fnc_GetRandomScrollNo  
  if not exists (SELECT ScrollNo from t_ATM_TransferTransaction Where OurBranchID = @OurBranchID AND ScrollNo = @mScrollNo)  
  BEGIN  
   SET @sCount = @sCount + 1    
  END  
 END  
  
 SET @VoucherID = @mScrollNo
   
 SELECT @vCount4=count(*) From t_ATM_Branches WHERE OurBranchID = @OurBranchID AND BranchID = @WithdrawlBranchID    
 
 if (@CountryCode = '784')    
 BEGIN    
  IF (@ForwardInstID = '500000')
  BEGIN    
   SET @ATMChargeID = '500000'    
  END
  ELSE
  BEGIN
   SET @ATMChargeID = '600000'    
  END
 END
 else if (@IsGCC = 1)    
 BEGIN    
  IF (@ForwardInstID = '500000')
  BEGIN    
   SET @ATMChargeID = '500000'    
  END
  ELSE
  BEGIN
   SET @ATMChargeID = 'GCC'    
  END
 END
 ELSE    
 BEGIN    
  SET @ATMChargeID = '500000'    
 END
   
 Select @vAccountID=AccountID, @GLAccountName=[Description] from t_GL where OurBranchID = @OurBranchID AND   
 AccountID = (SELECT ATMAccountID FROM t_ATM_ATMAccount WHERE OurBranchID = @OurBranchID AND ATMID = @ATMChargeID)  
  
 IF @vAccountID=''  
 BEGIN  --INVALID_ACCOUNT PLS MAINTAIN FIRST  
  /** Rejection change STARTS 06/04/2021 **/
    
  INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
  VALUES (@OurBranchID, @AccountID2, @OrgTrxRefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @mForeignAmount, @mExchangeRate, @ConvRate, @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, @AckInstIDCode, @RetRefNum, @CardAccptID, @NameLocation, @mDescription, @VISATrxID, @CAVV, @ResponseCode, @mDescriptionID, '79', 'Invalid ATM ID', @TrxDetails, @AuthIDResp, @ExtendedData)
  
  SELECT @Status=79,  
  @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,  
  @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100  
  
  SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @Value As value, @CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp, @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
  RETURN (1)  
  
  /** Rejection change ENDS 06/04/2021 **/
 END  
 
 if (@AuthIDResp = '0')
 begin
  exec @AuthIDResp = fnc_GetRandomNumber  
 end
  
 SET @mSerialNo = @mSerialNo + 1    
 SET @mAccountType='G'    
 SET @mCurrencyID=@LocalCurrency  
 SET @mTrxType = 'D'    
 SET @IsCredit = 0  
   
 INSERT INTO t_ATM_TransferTransaction  
 (  
  ScrollNo,SerialNo,RefNo,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,  
  Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,BankCode,Remarks,OperatorID,SupervisorID,MerchantType,STAN,ConvRate,  
  AcqCountryCode,CurrCodeTran,CurrCodeSett,SettlmntAmount,ATMStatus,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,CardAccptID,  
  CardAccptNameLoc,VISATrxID,ProcCode,CAVV,ResponseCode,ForwardInstID,ClearingRefNo
 )  
 VALUES  
 (  
  @mScrollNo,@mSerialNo,@OrgTrxRefNo,@OurBranchID,@WithdrawlBranchID,@vAccountID,@GLAccountName,@mAccountType,'GL',@mCurrencyID,@mIsLocalCurrency, @ValueDate,  
  @mwDate + ' ' + convert(varchar(12), GetDate(), 114),@PHXDate,@mTrxType,@Supervision,NULL,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,  
  @mDescription,@ATMID,@mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MerchantType,@STAN,@ConvRate,@AcqCountryCode,@CurrCodeTran,  
  @CurrCodeSett,@SettlmntAmount,@Supervision,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@RetRefNum,@AuthIDResp,@CardAccptID,@NameLocation,  
  @VISATrxID,@ProcCode,@CAVV,@ResponseCode,@ForwardInstID,@RefNo
 )  
  
 INSERT INTO t_GLTransactions     
 (    
  OurBranchID,RefNo,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,ExchangeRate,IsCredit,TransactionType,  
  TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,IsMainTrx,DocType,Remarks,MerchantType,STAN,SettlmntAmount,ConvRate,  
  AcqCountryCode,CurrCodeTran,CurrCodeSett,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,CardAccptID,CardAccptNameLoc,VISATrxID,  
  ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,ForwardInstID,TClientId,TAccountid,TProductId
 )    
 VALUES    
 (    
  @OurBranchID,@OrgTrxRefNo,@vAccountID,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@ValueDate,@mDescriptionID,@mDescription,  
  @mCurrencyID,@Amount,@mForeignAmount,@mExchangeRate,@IsCredit,'ATMTransfer','L',@mOperatorID,@mSupervisorID,'',@mScrollNo,@mSerialNo,null,'0','ATT',@mRemarks,  
  @MerchantType,@STAN,@SettlmntAmount,@ConvRate,@AcqCountryCode,@CurrCodeTran,@CurrCodeSett,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,  
  @RetRefNum,@AuthIDResp,@CardAccptID,@NameLocation,@VISATrxID,@ChannelId,@ChannelRefID,@ProcCode,@CAVV,@ResponseCode,@ForwardInstID,@ClientID,@AccountID2,@mProductID
 )  
  
 -- IF @vIsClose = 'C' or @vIsClose = 'T' or @vIsClose = 'X' or @vIsClose = 'D' or @vIsClose = 'I' or @vAllowCreditTransaction = 1  
 -- BEGIN  --INACTIVE/CLOSE_ACCOUNT    
  -- SELECT @Status=3,  
  -- @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,  
  -- @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100  
  
  -- SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @Value As value,   
  -- @CurrencyCode as CurrencyCode, '0' as AuthIDResp  
  -- RETURN (1)  
 -- END  
   
 --Credit Transaction  
 SELECT @GLControl = ISNULL(GLControl, '') from t_Products where OurBranchID = @OurBranchID and ProductID = @mProductID    
  
 SET @mSerialNo = @mSerialNo + 1    
 SET @mTrxType = 'C'  
 SET @IsCredit = 1  
  
 INSERT INTO t_ATM_TransferTransaction  
 (  
  ScrollNo,SerialNo,RefNo,OurBranchID,WithdrawlBranchID,AccountID,AccountName,AccountType,ProductID,CurrencyID,IsLocalCurrency,valueDate,wDate,PHXDate,TrxType,  
  Supervision,GLID,Amount,ForeignAmount,ExchangeRate,DescriptionID,Description,ATMID,BankCode,Remarks,OperatorID,SupervisorID,MerchantType,STAN,ConvRate,  
  AcqCountryCode,CurrCodeTran,CurrCodeSett,SettlmntAmount,ATMStatus,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,CardAccptID,  
  CardAccptNameLoc,VISATrxID,ProcCode,CAVV,ResponseCode,ForwardInstID,ClearingRefNo
 )  
 VALUES  
 (  
  @mScrollNo,@mSerialNo,@OrgTrxRefNo,@OurBranchID,@WithdrawlBranchID,@AccountID2,@mAccountName,'C',@mProductID,@mCurrencyID,@mIsLocalCurrency, @ValueDate,  
  @mwDate + ' ' + convert(varchar(12), GetDate(), 114),@PHXDate,@mTrxType,@Supervision,NULL,@Amount,@mForeignAmount,@mExchangeRate,@mDescriptionID,  
  @mDescription,@ATMID,@mBankCode,@mRemarks,@mOperatorID,@mSupervisorID,@MerchantType,@STAN,@ConvRate,@AcqCountryCode,@CurrCodeTran,  
  @CurrCodeSett,@SettlmntAmount,@Supervision,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@RetRefNum,@AuthIDResp,@CardAccptID,@NameLocation,  
  @VISATrxID,@ProcCode,@CAVV,@ResponseCode,@ForwardInstID,@RefNo
 )  
  
 INSERT INTO t_Transactions  
 (    
  OurBranchID,ScrollNo,SerialNo,RefNo,wDate,AccountType,DocumentType,AccountID,AccountName,ProductID,CurrencyID,ValueDate,TrxType,ChequeID,ChequeDate,Amount,  
  ForeignAmount,ExchangeRate,ProfitLoss,MeanRate,DescriptionID,[Description],BankCode,BranchCode,TrxPrinted,GLID,[Status],IsLocalCurrency,OperatorID,  
  SupervisorID,AdditionalData,IsMainTrx,DocType,GLVoucherID,Remarks,MerchantType,STAN,SettlmntAmount,ConvRate,AcqCountryCode,CurrCodeTran,CurrCodeSett,ATMStatus,  
  POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,CardAccptID,CardAccptNameLoc,VISATrxID,ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,ForwardInstID,ClearingRefNo,TClientId,TAccountid,TProductId
 )    
 VALUES     
 (    
  @OurBranchID,@mScrollNo,@mSerialNo,@OrgTrxRefNo,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),'C','ATT',@AccountID2,@mAccountName,@mProductID,@mCurrencyID,  
  @ValueDate,@mTrxType,'',@mwDate,@Amount,@mForeignAmount,@mExchangeRate,0,@mExchangeRate,@mDescriptionID,@mDescription,@mBankCode,@WithdrawlBranchID,  
  0,NULL,@Supervision,@mIsLocalCurrency,@mOperatorID,@mSupervisorID,null,'1','ATT',@VoucherID,@mRemarks,@MerchantType,@STAN,@SettlmntAmount,@ConvRate,  
  @AcqCountryCode,@CurrCodeTran,@CurrCodeSett,@Supervision,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,@RetRefNum,@AuthIDResp,@CardAccptID,  
  @NameLocation,@VISATrxID,@ChannelId,@ChannelRefID,@ProcCode,@CAVV,@ResponseCode,@ForwardInstID,@RefNo,@ClientID,@AccountID2,@mProductID
 )  

 --set @mScrollNo = 0 for @GLControl before @mSerialNo
  
 INSERT INTO t_GLTransactions     
 (    
  OurBranchID,RefNo,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,ExchangeRate,IsCredit,TransactionType,  
  TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,IsMainTrx,DocType,Remarks,MerchantType,STAN,SettlmntAmount,ConvRate,  
  AcqCountryCode,CurrCodeTran,CurrCodeSett,POSEntryMode,POSConditionCode,POSPINCaptCode,AcqInstID,RetRefNum,AuthIDResp,CardAccptID,CardAccptNameLoc,VISATrxID,  
  ChannelId,ChannelRefID,ProcCode,CAVV,ResponseCode,ForwardInstID,TClientId,TAccountid,TProductId
 )    
 VALUES    
 (    
  @OurBranchID,@OrgTrxRefNo,@GLControl,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(12), GetDate(), 114),@ValueDate,@mDescriptionID,@mDescription,  
  @mCurrencyID,@Amount,@mForeignAmount,@mExchangeRate,@IsCredit,'ATMTransfer','L',@mOperatorID,@mSupervisorID,'',0,@mSerialNo,null,'0','ATT',@mRemarks,  
  @MerchantType,@STAN,@SettlmntAmount,@ConvRate,@AcqCountryCode,@CurrCodeTran,@CurrCodeSett,@POSEntryMode,@POSConditionCode,@POSPINCaptCode,@AckInstIDCode,  
  @RetRefNum,@AuthIDResp,@CardAccptID,@NameLocation,@VISATrxID,@ChannelId,@ChannelRefID,@ProcCode,@CAVV,@ResponseCode,@ForwardInstID,@ClientID,@AccountID2,@mProductID
 )  
  
 Insert Into BalChk   
 (  
  OurBranchID, AccountID, ProductID, [Name], LastUpdateTime, ClearBalance, Limit, Effects, Shadow, FreezeAmt, MinBalance, TotalBalance, AvailableBalance  
 )  
 SELECT OurBranchID, AccountID, ProductID, [Name], @mwDate + ' ' + convert(varchar(12), GetDate(), 114), ISNULL(ClearBalance,0), ISNULL(Limit,0), ISNULL(Effects,0), ISNULL(ShadowBalance,0),   
 ISNULL(FreezeAmount,0), @vMinBalance,   
 (isnull(@vAccountLimit,0)+ClearBalance-ISNULL(Effects,0)),  
 (isnull(@vAccountLimit,0)+ClearBalance-ISNULL(FreezeAmount,0)-@vMinBalance)  
 FROM t_AccountBalance WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID2  
  
 SELECT @mAccountName=B.Name,@mProductID=B.ProductID,@mCurrencyID=P.CurrencyID,@mAccountType='C',@vClearBalance=ISNULL(B.ClearBalance,0),  
 @vClearedEffects=ISNULL(B.Effects,0),@vFreezeAmount=ISNULL(B.FreezeAmount,0),@vMinBalance=ISNULL(P.MinBalance,0),@vIsClose=ISNULL(A.Status,''),  
 @vAreCheckBooksAllowed=P.AreCheckBooksAllowed,@vAllowCreditTransaction=ISNULL(A.AllowCreditTransaction,'0'),@CurrencyCode = ISNULL(c.CurrencyCode,''),  
 @value=ISNULL(p.productatmcode,'00'),@vAccountLimit=ISNULL(B.Limit,0)
 FROM t_AccountBalance B   
 INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND B.ProductID = A.ProductID   
 INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND A.ProductID = P.ProductID   
 INNER JOIN t_Currencies C ON A.OurBranchID = C.OurBranchID AND A.CurrencyID = C.CurrencyID  
 WHERE (A.OurBranchID = @OurBranchID) AND (A.AccountID = @AccountID2)  
    
 --exec @vAccountLimit = [fnc_GetLimit] @OurBranchID, @AccountID2  
    
 IF @vAccountLimit > 0 AND @vCheckLimit = 1 AND @vODLimitAllow = 1  
 BEGIN  
  SET @vClearBalance=(@vClearBalance+@vAccountLimit)  
 END  
  
 SELECT @Status = 0,   
 @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,  
 @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100  
     
 SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value, @CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp, @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
 RETURN 0     
   
END