﻿CREATE PROCEDURE [dbo].[spc_AccountHistory]  (    
@AccountID nvarchar(30)    
)    
AS      
SET NOCOUNT ON      
Declare @SerialNO int = 1;    
select @SerialNO = ISNULL(max(SerialID),0)+1 from t_Account_History where AccountID = @AccountID    
INSERT INTO t_Account_History (SerialID, wDate, CreateBy, CreateTime, CreateTerminal, UpdateBy, UpdateTime, UpdateTerminal, Auth, AuthStatus, SuperviseBy, SuperviseTime, SuperviseTerminal, OurBranchID,     
AccountID, ClientID, Name, ProductID, IBAN, CurrencyID, Address, Status, HoldMail, OpenDate, Phone1, Phone2, Fax, EmailID, Reminder, Notes, NatureID, RelationshipCode, EmpID, StateBankCode, Code, GeoCode,     
AESCode, RiskCode, NameKin, FNameKin, AddressKin, PhNoResKin, PhNoOFFKin, MobNoKin, FAXKin, EmailKin, IntroducerAccountNo, IntroducedBy, IntroducerAddress, CloseDate, CloseRemarks, RelationShipKin,     
ModeOfOperation, TaxFiler, IsWHTax, AllowCreditTransaction, CreditNeedsSupervision, AllowDebitTransaction, NotServiceCharges, DebitNeedsSupervision, NotStopPaymentCharges, NotChequeBookCharges,     
ClearingTransactionDays, frmName, CashTotDr, CashTotCr, CashTotDrF, CashTotCrF, ApplyTax, StatementFrequency, LackOfFunds, InSufficientFundsAccountID, InSufficientFundsAccountID2, AccountType, GuardianName,     
GuardianCNIC, VerifyBy, VerifyTime, VerifyTerminal, VerifyStatus, AccessFunds, AccessFundsAccountID, ShortOfFunds, ExceedAmount, ShortageAmount, ShortageAccountID, MobileNo, Accrual, AccrualUpto, tAccrual,     
LastPayDate, LastDebitTransaction, LastCreditTransaction, LastPayWDate, LifeInsurance, ZakatExemption, ContactPersonName, CPRelationShip, DebitCardNo, PurposeOfAccount, PurposeOfAccountOthers, TurnOver,     
tTurnOver, DrThresholdLimit, CrThresholdLimit, NoOfDrTrx, NoOfCrTrx, ProductCash, ProductClearing, ProductCollection, ProductRemittance, ProductCross, ProductOthers, ProductOthersDesc, PurposeOfIntFT,     
OldClientID, IsCharity, CharityAmount, ActivationDate, Designation, IDType, CountryID, StateID, CityID, IntroducerCountryID, IntroducerStateID, IntroducerCityID, AddressKinCountryID, AddressKinStateID,     
AddressKinCityID, IDNoKin, SOCID, DbAccrual, DbAccrualUpto, DbtAccrual, NostroAccountID, NostroIBAN, SWIFT, NostroAddress, NostroCountry, MinBalanceReq, BankName, BranchName, SortCode, IsReconcile,     
OverdraftClassID,RealTimeComment,ShortageComment,ExcessComment)    
select @SerialNO, WORKINGDATE, CreateBy, CreateTime, CreateTerminal, UpdateBy, UpdateTime, UpdateTerminal, Auth, AuthStatus, SuperviseBy, SuperviseTime, SuperviseTerminal, a.OurBranchID,     
AccountID, ClientID, Name, ProductID, IBAN, CurrencyID, Address, Status, HoldMail, OpenDate, Phone1, Phone2, Fax, EmailID, Reminder, Notes, NatureID, RelationshipCode, EmpID, StateBankCode, Code, GeoCode,     
AESCode, RiskCode, NameKin, FNameKin, AddressKin, PhNoResKin, PhNoOFFKin, MobNoKin, FAXKin, EmailKin, IntroducerAccountNo, IntroducedBy, IntroducerAddress, CloseDate, CloseRemarks, RelationShipKin,     
ModeOfOperation, TaxFiler, IsWHTax, AllowCreditTransaction, CreditNeedsSupervision, AllowDebitTransaction, NotServiceCharges, DebitNeedsSupervision, NotStopPaymentCharges, NotChequeBookCharges,     
ClearingTransactionDays, a.frmName, CashTotDr, CashTotCr, CashTotDrF, CashTotCrF, ApplyTax, StatementFrequency, LackOfFunds, InSufficientFundsAccountID, InSufficientFundsAccountID2, AccountType, GuardianName,     
GuardianCNIC, VerifyBy, VerifyTime, VerifyTerminal, VerifyStatus, AccessFunds, AccessFundsAccountID, ShortOfFunds, ExceedAmount, ShortageAmount, ShortageAccountID, MobileNo, Accrual, AccrualUpto, tAccrual,     
LastPayDate, LastDebitTransaction, LastCreditTransaction, LastPayWDate, LifeInsurance, ZakatExemption, ContactPersonName, CPRelationShip, DebitCardNo, PurposeOfAccount, PurposeOfAccountOthers, TurnOver,     
tTurnOver, DrThresholdLimit, CrThresholdLimit, NoOfDrTrx, NoOfCrTrx, ProductCash, ProductClearing, ProductCollection, ProductRemittance, ProductCross, ProductOthers, ProductOthersDesc, PurposeOfIntFT,     
OldClientID, IsCharity, CharityAmount, ActivationDate, Designation, IDType, CountryID, StateID, CityID, IntroducerCountryID, IntroducerStateID, IntroducerCityID, AddressKinCountryID, AddressKinStateID,     
AddressKinCityID, IDNoKin, SOCID, DbAccrual, DbAccrualUpto, DbtAccrual, NostroAccountID, NostroIBAN, SWIFT, NostroAddress, NostroCountry, MinBalanceReq, BankName, BranchName, SortCode, IsReconcile,     
OverdraftClassID,RealTimeComment,ShortageComment,ExcessComment  from t_Account a    
inner join t_Last c on a.OurBranchID = c.OurBranchID and a.AccountID = @AccountID