﻿create   procedure [dbo].[spc_CreateTermDepositAccount]          
(                          
   @OurBranchID nvarchar(30),                          
   @ClientID nvarchar(30),                          
   @ProductID nvarchar(30)                          
)      
                          
AS                          
                          
SET NOCOUNT ON                          
  DECLARE @SQL nvarchar(max)                         
   declare @AccountID nvarchar(30), @AccountPrefix nvarchar(30), @Name nvarchar(max), @CurrencyID nvarchar(30), @desc nvarchar(100)                          
                             
   select @AccountPrefix = AccountPrefix, @desc = [Description], @CurrencyID = CurrencyID                          
   from t_Products                          
   where OurBranchID = @OurBranchID and ProductID = @ProductID                          
                             
   select @Name = Name + ' - ' + @desc                          
   from t_Customer                          
   where OurBranchID = @OurBranchID and ClientID = @ClientID                          
                             
   select @AccountID = @OurBranchID + @AccountPrefix + @ClientID + '01'                          
                             
   insert into t_Account                          
   (CreateBy, CreateTime, CreateTerminal, Auth, AuthStatus, OurBranchID, AccountID, ClientID, Name, ProductID, CurrencyID, Status, OpenDate, ModeOfOperation, frmName, CashTotDr, CashTotCr, CashTotDrF, CashTotCrF, ApplyTax, StatementFrequency)             
  
    
      
        
          
            
             
   values ('System', dbo.GetWorkingDate(@OurBranchID), HOST_NAME(), 0, '', @OurBranchID, @AccountID, @ClientID, @Name, @ProductID, @CurrencyID, '', dbo.GetWorkingDate(@OurBranchID), 'Singly', '', 0, 0, 0, 0, 0, 'N')                          
              
         declare @serialcount  int ;                    
       SELECT @serialcount= Max(serialid) from t_Account_History where AccountID=@AccountID and OurBranchID=@OurBranchID;                      
                   
    begin                
                
                
       --CreateAccountHistorystart     
	   

 SET @SQL ='exec spc_AccountHistory @accounttid'        
exec sp_executesql @SQL,N'@accounttid nvarchar(max)',    
@accounttid=@AccountID; 
-- SET @SQL ='exec spc_AccountHistory '''+@AccountID+''''        
-- print @SQL        
--EXEC (@SQL)                   
   --CreateAccountHistoryEnd                  
end            
                  
   insert t_HO_Account(CreateBy, CreateTime, CreateTerminal, OurBranchID, AccountID, ClientID, SuperviseBy, SuperviseTime, SuperviseTerminal)                          
   values ('System', dbo.GetWorkingDate(@OurBranchID), HOST_NAME(), @OurBranchID, @AccountID, @ClientID, 'System', dbo.GetWorkingDate(@OurBranchID), HOST_NAME())                          
                          
   insert into t_AccountCIF (OurBranchID, AccountID, ClientID) values (@OurBranchID, @AccountID, @ClientID)                                 
                          
   insert into t_AccountBalance (CreateBy,CreateTime,CreateTerminal,OurBranchID,AccountID,ProductID,[Name], ClearBalance,Effects,Limit,ShadowBalance,LocalClearBalance,LocalEffects,IsFreezed,FreezeAmount,ClearedEffects)                          
   values ('System', dbo.GetWorkingDate(@OurBranchID), HOST_NAME(), @OurBranchID, @AccountID, @ProductID, @Name, 0, 0, 0, 0, 0, 0, 0, 0, 0)                          
                          
   insert into t_AccountFunctions(CreateBy,CreateTime,CreateTerminal,OurBranchID,AccountID,ProductID,[Function],SignatureInstructions,OtherInstructions)                          
   values ('System', dbo.GetWorkingDate(@OurBranchID), HOST_NAME(), @OurBranchID, @AccountID, @ProductID, '', '', '')         
                          
   --insert into t_AccountOperatedBy(CreateBy,CreateTime,CreateTerminal, AuthStatus, OurBranchID, AccountID, ColumnID, [Name], [Address],Phone, NIC,  MobileNo, Email)                          
   --values ('System', dbo.GetWorkingDate(@OurBranchID), HOST_NAME(), '', @OurBranchID, @AccountID, (select ISNULL(max(ColumnId),0)+1 from t_AccountOperatedBy), @Name, '', '', '', '', '')                          
                    
insert into t_fdaccounts(frmname,CreateBy,CreateTime,CreateTerminal, AuthStatus, OurBranchID,clientid,ProductID,DepositAccountID)                          
   values ('frmFDAccGenerate','System', dbo.GetWorkingDate(@OurBranchID), HOST_NAME(), '', @OurBranchID, @ClientID,@ProductID, @AccountID)                          
                    
 select * from t_Account  where  accountid = @AccountID