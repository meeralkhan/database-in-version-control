﻿CREATE PROCEDURE [dbo].[spc_DeleteCashTrx] 
(   
 @OurBranchID varchar(30),   
 @AccountID varchar(30),   
 @SupervisorID varchar(30),   
 @ScrollNo numeric(24,0),   
 @AppWHTax char(1) = 0,   
 @WHTaxAmount money = 0,   
 @WHTaxMode char(1) = '',   
 @WHTaxScrollNo Numeric(24,0) = 0,   
   
 @ChgScrollno money = 0,   
 @ChgMode char(1) = '',   
   
 @AppChg bit=0,   
 @CRefno varchar(15)='',  
 @AddData text = '',  
 @NewRecord Bit=0 
)   
   
AS   
   SET NOCOUNT ON   
   
   DECLARE @vDate datetime    
   DECLARE @mDate Varchar(32)   
   DECLARE @ChequeID varchar(11)   
   DECLARE @ClearBalance money   
   DECLARE @Effects money   
   DECLARE @Limit money   
   DECLARE @TrxType char(1)   
   DECLARE @Amount money   
   DECLARE @ForeignAmount money   
   DECLARE @ExchangeRate decimal(24,6)  
   DECLARE @IsLocalCurrency bit   
   DECLARE @Status char(1)   
   DECLARE @AccountType char(1)   
   DECLARE @Supervision char(1)   
   DECLARE @RefNo varchar(100)    
   DECLARE @AccountStatus char(1)   
   DECLARE @tDate Varchar(10)   
   DECLARE @AuthStatus char(1)  
     
   DECLARE @GLVoucherID decimal(24,0)  
   DECLARE @wDate datetime  
   DECLARE @GLID varchar(30)  
   DECLARE @ProductID varchar(30)  
   DECLARE @GLControl varchar(30)  
   DECLARE @AccountName varchar(100)  
   DECLARE @CurrencyID varchar(30)  
   DECLARE @OperatorID varchar(30)  
   DECLARE @ValueDate datetime  
   DECLARE @ChequeDate varchar(30)  
   DECLARE @DescriptionID varchar(30)  
   DECLARE @Description varchar(100)  
   DECLARE @BankCode varchar(30)  
   DECLARE @BranchCode varchar(30)  
   DECLARE @UtilityNumber varchar(100)  
     
   SELECT @Status = 'R',@AuthStatus='',@GLVoucherID=0,@ExchangeRate=1,@GLID='',@ProductID='',@GLControl=''  
     
   /* Get Cash Transaction */  
   SELECT @vDate=ValueDate,@ChequeID= ChequeID,@AccountType=AccountType,@TrxType=TrxType,@Amount = Amount, @ForeignAmount = ForeignAmount,  
   @ExchangeRate=ExchangeRate,@IsLocalCurrency=IsLocalCurrency,@Supervision = Supervision,@RefNo = RefNo, @GLID = GlID, @ProductID = ProductID,  
   @OperatorID=OperatorID, @ChequeDate=ChequeDate, @wDate=wDate,@AccountName=AccountName,@CurrencyID=CurrencyID,@ValueDate=ValueDate,  
   @DescriptionID=DescriptionID,@Description=Description,@BankCode=BankCode,@BranchCode=BranchCode,@UtilityNumber=UtilityNumber,  
   @AddData=AdditionalData,@AppWHTax=AppWHTax,@WHTaxMode=WHTaxMode,@WHTaxAmount=WHTaxAmount  
   FROM t_CashTransactionModel  
   WHERE (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (ScrollNo = @ScrollNo)  
     
   SET @tDate = Convert(varchar(10),@vDate,101)   
   IF @Supervision = 'R'    
      BEGIN   
         SELECT '3' as RetStatus -- Trx. Already Rejected   
         RETURN   
      END   
   
   IF LEFT(@RefNo,3) = 'ADV'    
   BEGIN   
      SELECT '7' as RetStatus -- Trx. Already Rejected   
      RETURN   
   END   
   
 /* Initialize */   
 SELECT @AccountStatus = ''   
 IF @AccountType = 'C'   
  BEGIN   
   SELECT @AccountStatus = IsNull(Status,'') FROM t_Account   
   WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID   
      
   IF Upper(Ltrim(RTrim(@AccountStatus))) = 'C'   
        BEGIN   
           SELECT '4' as RetStatus -- Closed Account Can Not Rejected...   
           RETURN   
        END      
  END   
   
 IF @Supervision = 'C'  
 BEGIN  
      
    IF @AccountType = 'C'  
    begin  
         
       update t_Transactions set [Status] = 'R'  
       where OurBranchID = @OurBranchID and AccountID = @AccountID and DocType = 'C'  
       and CONVERT(varchar(10),wDate,101)= @tDate and ScrollNo = @ScrollNo  
         
       select @GLVoucherID = isnull(GLVoucherID,0) from t_Transactions  
       where OurBranchID = @OurBranchID and AccountID = @AccountID and DocType = 'C'  
       and CONVERT(varchar(10),wDate,101)= @tDate and ScrollNo = @ScrollNo  
         
       select @GLControl = ISNULL(GLControl, '') from t_Products  
       where OurBranchID = @OurBranchID and ProductID = @ProductID  
         
    end  
    else  
    begin  
         
       select @GLControl = @AccountID  
         
       select @GLVoucherID = isnull(VoucherID,0) from t_GLTransactions  
       where OurBranchID = @OurBranchID and AccountID = @AccountID and DocType = 'C'  
       and CONVERT(varchar(10),[Date],101)= @tDate and ScrollNo = @ScrollNo  
         
    end  
 END  
   
 IF @Supervision = '#'   
  BEGIN      
   Set @AuthStatus='#'    
   SET @SUPERVISION = (SELECT  ModuleStatus    
   FROM t_Authorization_RejectedTrx   
      WHERE OurBranchID = @OurBranchID and ScrollNo = @ScrollNo   
     AND ModuleType = 'C' AND Status = '#'         
     AND convert(varchar(10), wDate,101) = @tDate )   
  END   
    
     
     
   SELECT @mDate = Convert(varchar(10),@vDate ,101) + ' ' + convert(varchar(20),GetDate(),114)   
   
   /*UpDate Transaction File Status*/   
   UPDATE t_CashTransactionModel   
   SET Supervision = @Status, SupervisorID = @SupervisorID   
   WHERE (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (ScrollNo = @ScrollNo)   
   
   IF @AccountType = 'C'   
      BEGIN   
         /* Get Current ClearBalance, Effective Balance and Limit */    
         SELECT @ClearBalance=ClearBalance,@Effects=Effects,@limit=IsNull(Limit ,0)   
         FROM t_AccountBalance   
         WHERE (OurBranchID = @OurBranchID) AND (AccountID = @AccountID)    
   
   
         IF @Supervision = '*' OR @Supervision = 'P'   
 BEGIN   
/*   
  IF @TrxType = 'C'    
       BEGIN   
          IF @IsLocalCurrency='1'   
  BEGIN   
     Update t_AccountBalance   
     --Set ShadowBalance = ShadowBalance - @Amount   
     Where  OurBranchID = @OurBranchID and AccountID = @AccountID   
 END   
          ELSE IF @IsLocalCurrency='0'   
  BEGIN   
     Update t_AccountBalance   
     --Set ShadowBalance = ShadowBalance - @ForeignAmount   
     Where  OurBranchID = @OurBranchID and AccountID = @AccountID   
  END   
       END   
*/   
    --ELSE --IF @TrxType = 'D'    
    IF @TrxType = 'D'    
       BEGIN   
          IF @IsLocalCurrency = '1'   
  BEGIN   
     Update t_AccountBalance   
     Set ShadowBalance = ShadowBalance + @Amount   
     Where  OurBranchID = @OurBranchID and AccountID = @AccountID   
  END   
          ELSE IF @IsLocalCurrency='0'   
  BEGIN   
     Update t_AccountBalance   
     Set ShadowBalance = ShadowBalance + @ForeignAmount   
     Where  OurBranchID = @OurBranchID and AccountID = @AccountID   
  END   
       END   
 END   
   
    ELSE IF @Supervision = 'C'   
 BEGIN   
    IF @TrxType = 'C'    
       BEGIN   
     IF @AuthStatus=''    
     BEGIN   
           IF @IsLocalCurrency='1'   
   BEGIN   
      Update t_AccountBalance   
      Set ClearBalance = ClearBalance - @Amount   
      Where  OurBranchID = @OurBranchID and AccountID = @AccountID   
   END   
    
           ELSE IF @IsLocalCurrency='0'   
   BEGIN   
      Update t_AccountBalance   
      Set ClearBalance = ClearBalance - @ForeignAmount,   
         LocalClearBalance = LocalClearBalance - @Amount   
      Where  OurBranchID = @OurBranchID and AccountID = @AccountID   
   END   
     END   
       END   
    ELSE --IF @TrxType = 'D'    
       BEGIN   
          IF @IsLocalCurrency='1'   
  BEGIN   
     Update t_AccountBalance   
     Set ClearBalance = ClearBalance + @Amount   
     Where  OurBranchID = @OurBranchID and AccountID = @AccountID   
  END   
          ELSE IF @IsLocalCurrency='0'   
  BEGIN   
     Update t_AccountBalance   
     Set ClearBalance = ClearBalance + @ForeignAmount,   
        LocalClearBalance = LocalClearBalance + @Amount   
     Where  OurBranchID = @OurBranchID and AccountID = @AccountID   
  END   
       END -- Trx Type End   
 END -- Supervision End   
      END--A/c Type End   
   
   ELSE   
      BEGIN   
         SELECT @ClearBalance=0,@Effects=0,@limit=0   
      END   
    
   /* Create a New record For Superviser */   
   IF @SupervisorID <> ''   
      BEGIN   
         IF @IsLocalCurrency = 0    
 SELECT @Amount = @ForeignAmount   
   
         INSERT INTO t_SupervisedBy   
    (OurBranchID,AccountID,[Date],ScrollNo,ChequeID,ClearBalance,Effects,Limit,TrxType,Amount,Status,SupervisorID,TransactionCategory)   
         VALUES   
    (@OurBranchID,@AccountID,@mDate,@ScrollNo,@ChequeID,@ClearBalance,@Effects,@Limit,@TrxType,@Amount,@Status,@SupervisorID,'C')   
      END     
     
   /* Update Customer Status */   
   Update t_CustomerStatus   
   Set Status = @Status   
   Where (OurBranchID=@OurBranchID) and (AccountID = @AccountID)    
       and (ScrollNo = @ScrollNo) and  (Left(TransactionType,1)='C')   
  /* 
   /*@Update OtherTransaction Table For Cash*/   
   Update t_OtherTransactionStatus   
   Set Status = @Status   
   Where (OurBranchID=@OurBranchID) and (AccountID = @AccountID) and (ScrollNo = @ScrollNo)    
    and  (Left(TransactionType,1)='C')    
  */          
 
--Local Remittance Unpaid Mark   
  Update t_LR_Issuance   
  Set Paid = 0,   
  PaidDate = '1/1/1900',   
  PaidScrollNo = 0,   
  Supervision = 'C',        
  SuperviseBy = '',        
  SuperviseTime = '1/1/1900',        
  PaidMode = 'NA'        
  Where (OurBranchID=@OurBranchID) 
  and (InstrumentAccountID = @AccountID)           
  and  (PaidScrollNo = @ScrollNo)           
  and  (convert(varchar(20), PaidDate, 101) = CONVERT(varchar(20), @vDate, 101))           
  and (PaidMode = 'CH')   
 
  /*          
 
--Local Remittance Advice Unpaid Mark     
  Update t_LR_Advice   
  Set Paid = 0,   
  PaidDate = '1/1/1900',   
  PaidScrollNo = 0,   
  PaidMode = 'NA'   
  Where (OurBranchID=@OurBranchID) 
  and (InstrumentAccountID = @AccountID)           
  and  (PaidScrollNo = @ScrollNo)           
  and  (PaidDate = @vDate)           
  and (PaidMode = 'CH')   
   
  UpDate t_LRSRemit   
  SET Status = 'R'   
  Where (OurBranchID=@OurBranchID)           
  and (RefNo=@RefNo)           
  and (Date = @vDate)    
 
  */          
   
   IF @TrxType = 'D'   
      BEGIN   
         IF UPPER(LTRIM(RTRIM(@ChequeID))) <> 'V' AND LTRIM(RTRIM(@ChequeID)) <> ''   
 BEGIN   
    DELETE FROM t_ChequePaid   
    WHERE (OurBranchID = @OurBranchID) and (AccountID = @AccountID) and (ChequeID = @ChequeID)   
 END   
      END   
 /* 
 UPDATE t_Authorization_RejectedTrx   
 SET Status = 'C', SupervisorID = @SupervisorID, SupDate = @mDate, SupTerminalID = Host_Name()   
 WHERE OurBranchID = @OurBranchID AND ScrollNO = @ScrollNO AND ModuleType = 'C' AND Status = '#'   
 */  
   
 if @GLVoucherID = 0  
 begin  
      
    if @AccountType = 'C'  
    begin  
         
       insert into t_RejectedTrx (OurBranchID, ScrollNo, SerialNo, RefNo, wDate, AccountType, DocumentType, AccountID, AccountName,  
       ProductID, CurrencyID, ValueDate, TrxType, ChequeID, ChequeDate, Amount, ForeignAmount, ExchangeRate, ProfitLoss, MeanRate,  
       DescriptionID, [Description], BankCode, BranchCode, TrxPrinted, GLID, [Status], IsLocalCurrency, OperatorID,  
       SupervisorID, UtilityNumber, AdditionalData, IsMainTrx, DocType, AppWHTax, WHTaxMode, WHTaxAmount)  
         
       VALUES (@OurBranchID,@ScrollNo,'0',@RefNo,@wDate,@AccountType,'C',@AccountID,@AccountName,  
       @ProductID,@CurrencyID,@ValueDate,@TrxType,@ChequeID,@ChequeDate, @Amount, @ForeignAmount,@ExchangeRate,0,@ExchangeRate,  
       @DescriptionID,@Description,@BankCode,@BranchCode, 0, @GLID, 'R', @IsLocalCurrency, @OperatorID, @SupervisorID,   
       @UtilityNumber,@AddData,'1','C', @AppWHTax, @WHTaxMode, @WHTaxAmount)  
         
    end  
    else  
    begin  
         
       DECLARE @IsCredit int  
       DECLARE @TransactionMethod char(1)  
         
       if @TrxType = 'C'  
          set @IsCredit = 1  
       else  
          set @IsCredit = 0  
         
       if @IsLocalCurrency = '1'  
       begin  
 
          set @ForeignAmount = 0  
          set @ExchangeRate = 1  
          set @TransactionMethod = 'L'  
 
       end  
       else  
          set @TransactionMethod = 'A'  
 
       insert into t_RejectedGLTrx  
       (OurBranchID, AccountID, VoucherID, SerialID, [Date], ValueDate, DescriptionID, [Description], CurrencyID,  
       Amount, ForeignAmount, ExchangeRate, IsCredit, TransactionType, TransactionMethod, OperatorID, SupervisorID,   
       Indicator, ScrollNo, SerialNo, AdditionalData, IsMainTrx, DocType, GLID)  
         
       VALUES (@OurBranchID, @AccountID, '1', '1', @wDate, @ValueDate, @DescriptionID, @Description, @CurrencyID, @Amount, @ForeignAmount,  
       @ExchangeRate, @IsCredit, 'Cash', @TransactionMethod, @OperatorID, @SupervisorID, '', @ScrollNo, '0', @AddData, '1', 'C', @GLID)  
         
    end  
      
 end  
 else  
 begin  
      
    update t_GLTransactions set [Status] = 'R'  
    where OurBranchID = @OurBranchID and DocType = 'C'  
    and CONVERT(varchar(10),[Date],101) = @tDate  and VoucherID = @GLVoucherID  
      
    if @IsLocalCurrency = '1'  
    begin  
         
       if @TrxType = 'C'  
       begin  
 
          Update t_GL Set Balance = Balance + @Amount Where OurBranchID = @OurBranchID and AccountID = @GLID  
          Update t_GL Set Balance = Balance - @Amount Where OurBranchID = @OurBranchID and AccountID = @GLControl  
 
       end  
       else  
       begin  
 
          Update t_GL Set Balance = Balance - @Amount Where OurBranchID = @OurBranchID and AccountID = @GLID  
          Update t_GL Set Balance = Balance + @Amount Where OurBranchID = @OurBranchID and AccountID = @GLControl  
 
       end  
         
    end  
    else  
    begin  
         
       if (left(@RefNo,9) = 'LOCALCASH')  
       begin  
          if @TrxType = 'C'  
    
  Update t_GL Set Balance = Balance + @Amount Where OurBranchID = @OurBranchID and AccountID = @GLID  
    
          else  
    
  Update t_GL Set Balance = Balance - @Amount Where OurBranchID = @OurBranchID and AccountID = @GLID  
       end  
       else  
       begin  
          if @TrxType = 'C'  
    
  Update t_GL Set Balance = Balance + @Amount, ForeignBalance = ForeignBalance + @ForeignAmount  
  Where OurBranchID = @OurBranchID and AccountID = @GLControl  
    
          else  
    
  Update t_GL Set Balance = Balance - @Amount, ForeignBalance = ForeignBalance - @ForeignAmount  
  Where OurBranchID = @OurBranchID and AccountID = @GLControl  
       end  
         
       if @TrxType = 'C'  
       begin  
 
          Update t_GL Set Balance = Balance + @Amount, ForeignBalance = ForeignBalance + @ForeignAmount  
          Where OurBranchID = @OurBranchID and AccountID = @GLControl  
 
       end  
       else  
       begin  
 
          Update t_GL Set Balance = Balance + @Amount, ForeignBalance = ForeignBalance + @ForeignAmount  
          Where OurBranchID = @OurBranchID and AccountID = @GLControl  
 
       end  
         
    end  
      
 end  
   
 SELECT '0' as RetStatus  
 RETURN