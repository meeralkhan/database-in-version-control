﻿create PROCEDURE [dbo].[spc_RptGetAccountOpenedClosed]    
 (        
 @OurBranchID varchar(4),        
 @OpenDate datetime = '',        
 @AccountID varchar(30)=''        
 )        
        
 AS        
 set nocount on    
 IF (@OpenDate <> '' AND @AccountID <> '')    
  BEGIN        
   SELECT * FROM vc_RptGetAccountOpenedClosed WHERE OurBranchID = @OurBranchID AND     
   (OpenDate = @OpenDate OR closedate = @OpenDate) AND AccountID = @AccountID     
   order by productid, clientid        
  END        
        
 ELSE IF (@OpenDate <> '')        
  BEGIN        
   SELECT * FROM vc_RptGetAccountOpenedClosed WHERE OurBranchID = @OurBranchID AND     
   OpenDate = @OpenDate OR closedate = @OpenDate        
   order by productid, clientid        
  END        
        
 ELSE        
 BEGIN        
  IF  (@AccountID <> '')        
   BEGIN        
    SELECT * FROM vc_RptGetAccountOpenedClosed WHERE OurBranchID = @OurBranchID  and AccountID=@AccountID        
            
   END        
  ELSE        
   BEGIN        
    SELECT * FROM vc_RptGetAccountOpenedClosed WHERE OurBranchID = @OurBranchID order by productid, clientid        
   END        
 END