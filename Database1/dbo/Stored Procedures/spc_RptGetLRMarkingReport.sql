﻿  
CREATE procedure [dbo].[spc_RptGetLRMarkingReport] (  
 @OurBranchID varchar(30),  
 @InstrumentType varchar(30),  
 @FromDate datetime,  
 @ToDate datetime,  
 @Marking varchar(30)  
)  
As  
BEGIN  
 if @Marking = 'Cancel'  
 BEGIN  
  select a.SerialNo, b.BankID, b.FullName as BankName, c.BranchID, c.BranchName, a.InstrumentNo,   
  a.ControlNo, case a.Supervision  
   when 'C' then 'Cleared'  
   when '*' then 'Under Supervision'  
   when 'P' then 'Remote Pass Supervision'  
   when 'R' then 'Rejected'  
   end as SupervisionStatus,  
  MarkingStatus = Case   
   When isnull(Paid,'0') = 1 then 'Paid'   
   When isnull(Cancel,'0') = 1 then 'Cancel'   
   When isnull(Lost,'0') = 1 then 'Lost'   
   When isnull([Stop],'0') = 1 and isnull(Lost,'0')=0 and isnull(Cancel,'0')=0 and isnull(Paid,'0')=0 then 'OutStanding(Stop)'   
   When isnull([Stop],'0')= 0 then 'OutStanding'   
  End,   
  a.PayeeName, a.Amount, a.Charges, a.BeneficiaryName,   
  a.CancelDate as MarkingDate,   
  a.CreateBy as OperatorID, a.SuperviseBy as SupervisorID, a.AdditionalData  
  
  from t_LR_Issuance a  
  INNER JOIN t_LR_Banks b ON a.OurBranchID = b.OurBranchID AND a.DrawnBankID = b.BankID  
  INNER JOIN t_LR_Branches c ON a.OurBranchID=c.OurBranchID AND b.BankID = c.BankID AND a.DrawnBranchID = c.BranchID  
  where a.OurBranchID=@OurBranchID   
  AND a.Cancel = 1  
  AND a.InstrumentType = @InstrumentType  
  AND convert(varchar(20), a.wDate,101) between @FromDate and @ToDate  
 END  
 ELSE if @Marking = 'Lost'  
 BEGIN  
  select a.SerialNo, b.BankID, b.FullName as BankName, c.BranchID, c.BranchName, a.InstrumentNo,   
  a.ControlNo, case a.Supervision  
   when 'C' then 'Cleared'  
   when '*' then 'Under Supervision'  
   when 'P' then 'Remote Pass Supervision'  
   when 'R' then 'Rejected'  
   end as SupervisionStatus,  
  MarkingStatus = Case   
   When isnull(Paid,'0') = 1 then 'Paid'   
   When isnull(Cancel,'0') = 1 then 'Cancel'   
   When isnull(Lost,'0') = 1 then 'Lost'   
   When isnull([Stop],'0') = 1 and isnull(Lost,'0')=0 and isnull(Cancel,'0')=0 and isnull(Paid,'0')=0 then 'OutStanding(Stop)'   
   When isnull([Stop],'0')= 0 then 'OutStanding'   
  End,   
    
  a.PayeeName, a.Amount, a.Charges, a.BeneficiaryName,   
  a.LostDate as MarkingDate,   
  a.CreateBy as OperatorID, a.SuperviseBy as SupervisorID, a.AdditionalData  
  
  from t_LR_Issuance a  
  INNER JOIN t_LR_Banks b ON a.OurBranchID = b.OurBranchID AND a.DrawnBankID = b.BankID  
  INNER JOIN t_LR_Branches c ON a.OurBranchID=c.OurBranchID AND b.BankID = c.BankID AND a.DrawnBranchID = c.BranchID  
  where a.OurBranchID=@OurBranchID   
  AND a.Lost = 1  
  AND a.InstrumentType = @InstrumentType  
  AND convert(varchar(20), a.wDate,101) between @FromDate and @ToDate  
 END  
 ELSE if @Marking = 'Stop'  
 BEGIN  
  select a.SerialNo, b.BankID, b.FullName as BankName, c.BranchID, c.BranchName, a.InstrumentNo,   
  a.ControlNo, case a.Supervision  
   when 'C' then 'Cleared'  
   when '*' then 'Under Supervision'  
   when 'P' then 'Remote Pass Supervision'  
   when 'R' then 'Rejected'  
   end as SupervisionStatus,  
  MarkingStatus = Case   
   When isnull(Paid,'0') = 1 then 'Paid'   
   When isnull(Cancel,'0') = 1 then 'Cancel'   
   When isnull(Lost,'0') = 1 then 'Lost'   
   When isnull([Stop],'0') = 1 and isnull(Lost,'0')=0 and isnull(Cancel,'0')=0 and isnull(Paid,'0')=0 then 'OutStanding(Stop)'   
   When isnull([Stop],'0')= 0 then 'OutStanding'   
  End,   
  a.PayeeName, a.Amount, a.Charges, a.BeneficiaryName,   
  a.StopDate as MarkingDate,   
  a.CreateBy as OperatorID,   
  a.SuperviseBy as SupervisorID, a.AdditionalData  
  
  from t_LR_Issuance a  
  INNER JOIN t_LR_Banks b ON a.OurBranchID = b.OurBranchID AND a.DrawnBankID = b.BankID  
  INNER JOIN t_LR_Branches c ON a.OurBranchID=c.OurBranchID AND b.BankID = c.BankID AND a.DrawnBranchID = c.BranchID  
  where a.OurBranchID=@OurBranchID   
  AND a.[Stop] = 1  
  AND a.InstrumentType = @InstrumentType  
  AND convert(varchar(20), a.wDate,101) between @FromDate and @ToDate  
 END  
 ELSE if @Marking = 'Paid'  
 BEGIN  
  select a.SerialNo, b.BankID, b.FullName as BankName, c.BranchID, c.BranchName, a.InstrumentNo,   
  a.ControlNo, case a.Supervision  
   when 'C' then 'Cleared'  
   when '*' then 'Under Supervision'  
   when 'P' then 'Remote Pass Supervision'  
   when 'R' then 'Rejected'  
   end as SupervisionStatus,  
  MarkingStatus = Case   
   When isnull(Paid,'0') = 1 then 'Paid'   
   When isnull(Cancel,'0') = 1 then 'Cancel'   
   When isnull(Lost,'0') = 1 then 'Lost'   
   When isnull([Stop],'0') = 1 and isnull(Lost,'0')=0 and isnull(Cancel,'0')=0 and isnull(Paid,'0')=0 then 'OutStanding(Stop)'   
   When isnull([Stop],'0')= 0 then 'OutStanding'   
  End,   
  a.PayeeName, a.Amount, a.Charges, a.BeneficiaryName,   
  a.PaidDate as MarkingDate,   
  a.CreateBy as OperatorID,   
  a.SuperviseBy as SupervisorID, a.AdditionalData  
  
  from t_LR_Issuance a  
  INNER JOIN t_LR_Banks b ON a.OurBranchID = b.OurBranchID AND a.DrawnBankID = b.BankID  
  INNER JOIN t_LR_Branches c ON a.OurBranchID=c.OurBranchID AND b.BankID = c.BankID AND a.DrawnBranchID = c.BranchID  
  where a.OurBranchID=@OurBranchID   
  AND a.Paid = 1  
  AND a.InstrumentType = @InstrumentType  
  AND convert(varchar(20), a.wDate,101) between @FromDate and @ToDate  
 END  
END