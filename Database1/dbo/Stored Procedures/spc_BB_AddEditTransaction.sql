﻿CREATE PROCEDURE [dbo].[spc_BB_AddEditTransaction]  
(    
  
 @OurBranchID varchar(30),   
 @CustomerAccountID varchar(30),   
 @CustomerAccountName varchar(100),           
 @CustomerProductID varchar(30),           
 @CustomerCurrencyID varchar(30),           
 @CustomerAccountType char(1),           
 @CustomerTrxType char(1),           
 @CustomerIsLocalCurrency Char(1),           
 @ValueDate datetime,           
 @wDate datetime='',           
 @Amount money,           
 @ForeignAmount Money,           
 @ExchangeRate decimal(18,6),           
 @DescriptionID varchar(30),           
 @Description varchar(255),           
 @SourceBranch Varchar(30),           
 @TargetBranch varchar(30),           
 @Remarks varchar(1000),           
 @RefNo varchar(30),    
     
 @ScrollNo int,           
 @SerialNo int    
)    
AS            
           
 DECLARE @mDate varchar(50)            
 DECLARE @AccountClass char(1)          
 DECLARE @IsPosting bit          
 DECLARE @AllowCrDr bit          
 DECLARE @IsFreezed bit          
 DECLARE @RetStatus as int      
 DECLARE @Status varchar(30)    
 Declare @TotalAmount money    
           
 set nocount on            
           
 BEGIN          
   
   SET @TotalAmount = @Amount    
   SELECT @wDate=WorkingDate from t_Last WHERE OurBranchID = @OurBranchID        
             
   SET @Description = @Description + ' --- ' + cast(GETDATE() as varchar(50))            
   SET @mDate = convert(varchar(10),@wDate,101) + ' ' + convert(varchar(20), getdate(),114)            
       
   if @CustomerProductID <> 'GL'   
   begin          
     EXEC @RetStatus = spc_AmountVerify @OurBranchID, @CustomerAccountID, @CustomerTrxType, @Amount,         
     'C', 'N', '999999999999.99', '', @Amount        
       
     if @RetStatus <> '97'    
     BEGIN    
       SELECT 'Error' AS RetStatus, 'ErrPostingTrx' As RetMessage          
       RETURN(0)          
     END         
   end    
        
   if @CustomerProductID = 'GL'  
   begin          
     IF (SELECT Count(AccountID) From t_GL WHERE OurBranchID=@OurBranchID AND AccountID=@CustomerAccountID) > 0          
     begin          
                 
       SELECT @AccountClass = AccountClass, @IsPosting = IsPosting From t_GL          
       WHERE OurBranchID=@OurBranchID AND AccountID=@CustomerAccountID          
                 
       IF UPPER(@AccountClass) <> 'P'          
       BEGIN          
         SELECT 'Error' AS RetStatus, 'NotAPostingAccount' As RetMessage          
         RETURN(0)          
       END          
                 
       IF @IsPosting = 0          
       BEGIN          
         SELECT 'Error' AS RetStatus, 'GLPostingNotAllowed' As RetMessage          
         RETURN(0)          
       END          
                 
       IF (SELECT Count(*) From t_Products WHERE OurBranchID = @OurBranchID AND           
       (GLControl=@CustomerAccountID OR GLDebitBalance = @CustomerAccountID)) > 0          
       BEGIN          
         SELECT 'Error' AS RetStatus, 'GLPostingNotAllowedControlAccount' As RetMessage          
         RETURN(0)          
       END          
                 
     end          
     ELSE          
     BEGIN          
       SELECT 'Error' AS RetStatus, 'AccountNotFound' As RetMessage          
       RETURN(0)          
     END                  
   end          
       
   INSERT INTO t_BBTransactions            
   (            
    ScrollNo, SerialNo, OurBranchID, AccountID, AccountName, ProductID, CurrencyID, AccountType, ValueDate, wDate,     
    TrxType, Amount, ForeignAmount, ExchangeRate, DescriptionID, [Description], IsLocalCurrency, SourceBranch,     
    TargetBranch, Remarks, RefNo  
   )    
             
   VALUES          
   (            
    @ScrollNo, @SerialNo, @OurBranchID, @CustomerAccountID, @CustomerAccountName, @CustomerProductID,     
    @CustomerCurrencyID, @CustomerAccountType, @ValueDate, @mDate, @CustomerTrxType, @Amount, @ForeignAmount,    
    @ExchangeRate, @DescriptionID, @Description, @CustomerIsLocalCurrency, @SourceBranch, @TargetBranch,    
    @Remarks,@RefNo  
   )            
       
   declare @IsDebit bit    
       
   if @CustomerAccountType = 'C'  
   Begin    
      if @CustomerTrxType = 'D'    
      BEGIN    
         SET @IsDebit = 1    
      end    
      else    
      begin    
         SET @IsDebit = 0    
      end    
      
      EXEC @RetStatus = spc_CashUpdateBalance @OurBranchID, @CustomerAccountID, @Amount, @ForeignAmount, @IsDebit,     
      'C', @CustomerIsLocalCurrency, 0    
   end    
             
   SELECT @Status=@@ERROR            
             
   IF @Status <> 0             
   BEGIN            
     SELECT 'Error' AS RetStatus, 'ChkStatus' As RetMessage, @Status AS Status          
     RETURN(0)          
   END              
             
   SELECT 'Ok' AS RetStatus, 'TrxPosted' As RetMessage, @ScrollNo ScrollNo, @RefNo RefNo    
   RETURN(0)          
             
 END