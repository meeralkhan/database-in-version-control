﻿
CREATE PROCEDURE [dbo].[sp_ATM_AddIntBrCashCrTransaction]
(    
 @OurBranchID        varchar(30),     
 @CustomerBranchID      varchar(30)='',     
 @WithdrawlBranchID      varchar(30)='',       
 @ATMID       varchar(30),     
 @AccountID           varchar(30),    
 @Amount        money=0,    
 @ChargeAmount        money=0,    
 @USDAmount       money=0,    
 @OtherCurrencyAmount      money=0,    
 @USDRate           money=0,    
 @Supervision       char(1),     
 @RefNo        varchar(36),    
 @PHXDate       datetime,     
 @MerchantType                  varchar(30),    
 @OurIMD       varchar(30),     
 @AckInstIDCode      varchar(30),    
 @Currency       varchar(3),    
 @NameLocation      varchar(40),    
 @MCC        varchar(30),    
 @CreditCardNumber varchar(50)='',    
 @TrxDesc varchar(30)='',
 @NewRecord       bit=1    
)    
AS
 BEGIN     
 
  DECLARE @BankShortName as varchar(30)    
  DECLARE @mScrollNo as money    
  DECLARE @mSerialNo as money    
   SET @mSerialNo=0
  DECLARE @mCustomerAccountID as varchar(30)    
   SET @mCustomerAccountID=@AccountID     
  DECLARE @mAccountName as varchar(50)    
  DECLARE @mAccountType as char    
  DECLARE @mProductID as varchar(30)    
  DECLARE @mCurrencyID as varchar(30)    
  DECLARE @mIsLocalCurrency as char    
   SET @mIsLocalCurrency=1     
  DECLARE @mwDate as datetime    
  DECLARE @mTrxType as char    
   SET @mTrxType='D'     
  DECLARE @mGLID as varchar(30)    
  DECLARE @mForeignAmount as money    
   SET @mForeignAmount=0     
  DECLARE @mExchangeRate as money    
   SET @mExchangeRate=1     
  DECLARE @mDescriptionID as varchar(3)    
   SET @mDescriptionID='000'     
  DECLARE @mDescription as varchar(255)    
  DECLARE @mDescriptionCharges as varchar(255)    
  DECLARE @STAN as varchar(30)
   SET @BankShortName = 'N/A' 
  DECLARE @VoucherID int
  DECLARE @IsCredit int
  DECLARE @GLControl nvarchar(30)   
  
   SELECT @BankShortName = BankShortName From t_ATM_Banks    
   WHERE (OurBranchID = @OurBranchID AND BankIMD = @AckInstIDCode) 
   
   IF @OurIMD = @AckInstIDCode
   BEGIN
    SET @mDescription='ATM(' + @ATMID + ') CSH WDRL : A/c # ' + @AccountID + ' OF BK.' + @BankShortName + '(BR.' + @CustomerBranchID + ')'
	SET @mDescriptionCharges='ATM(' + @ATMID + ') CSH WDRL-CHG : A/c # ' + @AccountID + ' OF BK.' + @BankShortName + '(BR.' + @CustomerBranchID + ')'
   END
   ELSE     
   BEGIN
    SET @mDescription='ATM(' + @ATMID + ') CSH WDRL : A/c # ' + @AccountID + ' OF BK.' + @BankShortName + '(BR.' + @CustomerBranchID + ')'
	SET @mDescriptionCharges='ATM(' + @ATMID + ') CSH WDRL-CHG : A/c # ' + @AccountID + ' OF BK.' + @BankShortName + '(BR.' + @CustomerBranchID + ')'
   END     
   
  DECLARE @mRemarks as varchar(200)    
  DECLARE @mOperatorID as varchar(30)    
   SET @mOperatorID = 'ATM-' + @ATMID    
  DECLARE @mSupervisorID as varchar(30)    
   SET @mSupervisorID = @AckInstIDCode    
  DECLARE @mLastEOD as datetime    
  DECLARE @mCONFRETI as datetime    
  DECLARE @mCustomerBranchName as varchar(50)    
  
  --Checking Variables    
  DECLARE @vCount1 as bit    
  DECLARE @vCount2 as bit    
  DECLARE @vCount3 as bit    
  DECLARE @vCount4 as bit    
  DECLARE @vAccountID as varchar(30)    
   SET @vAccountID = @AccountID    
  DECLARE @vClearBalance as money    
  DECLARE @AvailableBalance as money    
  DECLARE @Value as varchar(2)
   SET @value='00'
  DECLARE @vClearedEffects as money    
  DECLARE @vFreezeAmount as money    
  DECLARE @vMinBalance as money    
  DECLARE @limit as money    
  DECLARE @vIsClose as char    
  DECLARE @vAreCheckBooksAllowed as bit    
  DECLARE @vAllowDebitTransaction as bit    
  DECLARE @vBranchName as varchar(50)    
  DECLARE @Status as int
  DECLARE @WithDrawlCharges as money    
  DECLARE @WithDrawlChargesPercentage as money    
  DECLARE @AcquiringAccountID as varchar(30)    
  DECLARE @AcquiringAccountTitle as varchar(50)    
  DECLARE @AcquiringChargesAccountID as varchar(30)    
  DECLARE @AcquiringTitle as varchar(50)    
  DECLARE @Super as varchar(1)    
  DECLARE @AccountDigit as varchar(15)    
   SET @AccountDigit=14
  DECLARE @ExciseDutyAccountID as varchar(30)
  DECLARE @ExciseDutyPercentage as money
   SET @ExciseDutyPercentage = 0
  DECLARE @ExciseDutyAmount as money
   SET @ExciseDutyAmount = 0
  DECLARE @AcquiringChargesAmount as money
   SET @AcquiringChargesAmount = 0
  
  SET NOCOUNT ON    
  SELECT @Status = -1    
  
  IF @Amount > 0     
  BEGIN
   SELECT @mLastEOD=LastEOD,@mwDate=WorkingDate,@mCONFRETI=CONFRETI FROM t_Last WHERE (OurBranchID = @OurBranchID)
   IF @mLastEOD = @mwDate or @mCONFRETI = '1/1/9999'    
   BEGIN
    SELECT @Status=95  --INVALID_DATE    
    SELECT @Status  as Status    
    RETURN (1)     
   END    
   
   SELECT @vCount1=count(*),@Super=Supervision From t_ATM_CashTransaction
   WHERE (OurBranchID = @OurBranchID) AND( CustomerAccountID=@AccountID) AND (Refno=@Refno) and (AtmID=@ATMID) group by Supervision
   
   IF @vCount1=1 and @Super='C'    
   BEGIN 
    SELECT @Status = 0 , @AvailableBalance = 0, @vClearBalance = 0,@Value='00'
    SELECT @Status As Status , @vClearBalance As ClearBalance , @AvailableBalance As AvailableBalance , @value As value     
    RETURN (1)     
   END
   
   SELECT @vCount1=count(*),@Super=Supervision From t_ATM_TransactionHistory
   WHERE (OurBranchID = @OurBranchID) AND( CustomerAccountID=@AccountID) AND (Refno=@Refno) and (AtmID=@ATMID)  group by Supervision    
   
   IF @vCount1=1 and @Super='C'    
   BEGIN 
	SELECT @Status = 0 , @AvailableBalance = 0, @vClearBalance = 0,@Value='00'
    SELECT @Status As Status , @vClearBalance As ClearBalance , @AvailableBalance As AvailableBalance , @value As value     
    RETURN (1)     
   END    
   
   IF @vCount1=1 and @Super='R'
   BEGIN 
    SELECT @Status=35  --ORIG_ALREADY_REVERSED    
    SELECT @Status  as Status    
    RETURN (1)     
   END
   
   SET  @vBranchName = 'N/A'    
   SELECT @vBranchName=Heading2 FROM t_GlobalVariables  WHERE (OurBranchID = @OurBranchID)    
   
   SET @WithDrawlCharges = 0    
   SET @WithDrawlChargesPercentage = 0    
   SET @ExciseDutyPercentage = 0
   SET @AcquiringChargesAmount = 0
   
   SELECT @WithDrawlCharges=WithDrawlCharges,@WithDrawlChargesPercentage=WithDrawlChargesPercentage,
   @AcquiringAccountID=IssuerAccountID,@AcquiringAccountTitle=IssuerAccountTitle,
   @AcquiringChargesAccountID=AcquiringChargesAccountID,@AcquiringTitle=AcquiringTitle, @ExciseDutyAccountID=ExciseDutyAccountID,
   @ExciseDutyPercentage=ExciseDutyPercentage,@AcquiringChargesAmount=AcquiringChargesAmount
   From t_ATM_SwitchCharges    
   WHERE (OurBranchID = @OurBranchID AND MerchantType = @MerchantType) 
   
   IF (@OurBranchID <> @CustomerBranchID and @MerchantType='0000')
   BEGIN 
    IF @MerchantType = '0000'--@OurIMD = @AckInstIDCode    
    BEGIN    
     SET @mDescription='ATM(' + @ATMID + ') CSH WDRL : A/c # ' + @AccountID + ' OF BK.' + @BankShortName + '(BR.'+ @CustomerBranchID + ')'
	 SET @mDescriptionCharges='ATM(' + @ATMID + ') CSH WDRL-CHG : A/c # ' + @AccountID + ' OF BK.' + @BankShortName
    END    
	ELSE     
	BEGIN    
	 SET @mDescription='ATM(' + @ATMID + ') CSH WDRL : A/c # ' + @AccountID + ' OF BK.' + @BankShortName
     SET @mDescriptionCharges='ATM(' + @ATMID + ') CSH WDRL-CHG : A/c # ' + @AccountID + ' OF BK.' + @BankShortName
    END
   END
   ELSE
   BEGIN
    IF @MerchantType = '0000'--@OurIMD = @AckInstIDCode    
    BEGIN 
     SET @mDescription='ATM(' + @ATMID + ') CSH WDRL : A/c # ' + @AccountID + ' OF BK.' + @BankShortName + '(BR.'+ @CustomerBranchID + ')'
	 SET @mDescriptionCharges='ATM(' + @ATMID + ') CSH WDRL-CHG : A/c # ' + @AccountID + ' OF BK.' + @BankShortName
    END    
   END
    
   IF NOT EXISTS (SELECT AccountID FROM t_GL WHERE (OurBranchID = @OurBranchID) AND 
   (AccountID = (SELECT ATMAccountID FROM t_ATM_ATMAccount WHERE OurBranchID = @OurBranchID and ATMID = @ATMID)))  
   BEGIN  
    SELECT @Status=79  --INVALID ATMID PLEASE MAINTAIN  
    SELECT @Status  as Status    
    RETURN (1)     
   END
	
   SET @mGLID = '00000000' 
   SELECT @mGLID=AccountID FROM t_GL WHERE (OurBranchID = @OurBranchID) and   
   (AccountID = (SELECT ATMAccountID FROM t_ATM_ATMAccount WHERE OurBranchID = @OurBranchID and ATMID = @ATMID))    
    
   SELECT @vCount4=count(*) From t_ATM_Branches    
   WHERE OurBranchID = @OurBranchID AND BranchID = @CustomerBranchID    
    
   IF @OurIMD <> @AckInstIDCode
   BEGIN    
    SET @mRemarks='Being Amount of ATM Cash Withdrawl Transaction From : ' + upper(@vBranchName) + '  By A/c # ' + @vAccountID + ' At : ' + @BankShortName + '(' + @CustomerBranchID+ ')'
   END
   ELSE    
   BEGIN
    IF @vCount4 > 0     
	BEGIN    
     SELECT @mCustomerBranchName=BranchName FROM t_ATM_Branches WHERE OurBranchID = @OurBranchID AND BranchID = @CustomerBranchID
	 
	 IF @CustomerBranchID <> @OurBranchID
     BEGIN
      SET @mRemarks='Being Amount of ATM Cash Withdrawl Transaction From : ' + upper(@vBranchName) + '  By A/c # ' + @vAccountID + ' At : ' + @BankShortName + '(' +  upper(@mCustomerBranchName) + ')'
     END    
     ELSE    
     BEGIN          
	  SET @mRemarks='Being Amount of ATM Cash Withdrawl Transaction From : ' + upper(@vBranchName) + '  By A/c # ' + @vAccountID + ' At : ' + @BankShortName + '(' +  upper(@mCustomerBranchName) + ')'
     END    
    END
	ELSE    
	BEGIN    
	 IF @CustomerBranchID <> @OurBranchID    
	 BEGIN     
	  SET @mRemarks='Being Amount of ATM Cash Withdrawl Transaction From : ' + upper(@vBranchName) + '  By A/c # ' + @vAccountID + ' At : '  + @BankShortName + '(' + @CustomerBranchID+ ')'    
     END     
	 ELSE    
	 BEGIN     
	  SET @mRemarks='Being Amount of ATM Cash Withdrawl Transaction From : ' + upper(@vBranchName) + '  By A/c # ' + @vAccountID + ' At : '  + @BankShortName + '(' + @CustomerBranchID+ ')'    
     END     
    END    
   END
   
   SELECT @mScrollNo=(ISNULL(MAX(ScrollNo),0)+1) FROM t_ATM_CashTransaction 
   WHERE (OurBranchID = @OurBranchID) and (convert(varchar(10), wDate, 120) = convert(varchar(10), @mwDate, 120))
   
   SET @mSerialNo = @mSerialNo+1
   SET @STAN = RIGHT(@RefNo,6)
   SET @mAccountType = 'G'
   SET @mProductID = 'GL'
   SET @mCurrencyID = 'PKR'
   SET @IsCredit = 0
   
   SELECT @VoucherID = ISNULL(MAX(VoucherID),0) + 1 from t_GLTransactions where OurBranchID = @OurBranchID
   
   INSERT INTO t_ATM_CashTransaction 
   (
    OurBranchID,ScrollNo,SerialNo,RefNo,CustomerBranchID,ATMID,CustomerAccountID,AccountID,AccountName,AccountType,ProductID,
    CurrencyID,IsLocalCurrency,ValueDate,wDate,PHXDate,TrxType,Supervision,GLID,Amount,ForeignAmount,ExchangeRate,
    DescriptionID,[Description],Remarks,OperatorID,SupervisorID,MCC,TrxDesc,MerchantType,STAN
   )
   VALUES    
   (
	@OurBranchID,@mScrollNo,@mSerialNo,@RefNo,@CustomerBranchID,@ATMID,@mCustomerAccountID,@AcquiringAccountID,
	@AcquiringAccountTitle,@mAccountType,@mProductID,@mCurrencyID,@mIsLocalCurrency,@mwDate,
	@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@PHXDate,@mTrxType,@Supervision,@mGLID,@Amount,@mForeignAmount,
	@mExchangeRate,@mDescriptionID,@mDescription+' STAN: '+@STAN,@mRemarks,@mOperatorID,@mSupervisorID,@MCC,@TrxDesc,
	@MerchantType,@STAN
   )
   
   INSERT INTO t_GLTransactions 
   (
    OurBranchID,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,
    ExchangeRate,IsCredit,TransactionType,TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,
    IsMainTrx,DocType,Remarks,TrxDesc,MerchantType,STAN
   )
   VALUES
   ( 
    @OurBranchID,@AcquiringAccountID,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@mwDate,
    @mDescriptionID,@mDescription+' STAN: '+@STAN,@mCurrencyID,@Amount,@mForeignAmount,@mExchangeRate,@IsCredit,'ATMTransfer',
    'L',@mOperatorID,@mSupervisorID,'',@mScrollNo,@mSerialNo,null,'1','AT',@mRemarks,@TrxDesc,@MerchantType,@STAN 
   )
   
   IF @mTrxType = 'D'
   BEGIN
    SET @IsCredit = 1
   END
   ELSE
   BEGIN
    SET @IsCredit = 0
   END
   
   INSERT INTO t_GLTransactions 
   (
    OurBranchID,AccountID,VoucherID,SerialID,[Date],ValueDate,DescriptionID,[Description],CurrencyID,Amount,ForeignAmount,
    ExchangeRate,IsCredit,TransactionType,TransactionMethod,OperatorID,SupervisorID,Indicator,ScrollNo,SerialNo,AdditionalData,
    IsMainTrx,DocType,Remarks,TrxDesc,MerchantType,STAN
   )
   VALUES
   ( 
    @OurBranchID,@mGLID,@VoucherID,@mSerialNo,@mwDate + ' ' + convert(varchar(10), GetDate(), 108),@mwDate,
    @mDescriptionID,@mDescription+' STAN: '+@STAN,@mCurrencyID,@Amount,@mForeignAmount,@mExchangeRate,@IsCredit,'ATMTransfer',
    'L',@mOperatorID,@mSupervisorID,'',@mScrollNo,@mSerialNo,null,'0','AT',@mRemarks,@TrxDesc,@MerchantType,@STAN 
   )
   
   SELECT @Status=@@ERROR
   
   IF @Status <> 0
   BEGIN 
	RAISERROR('There Is An Error Occured While Creating Transaction In The Source Branch',16,1) WITH SETERROR    
	RETURN (5000)    
   END
   
   SELECT @Status           = 0, 
          @AvailableBalance = (isnull(@limit,0)+(isnull(@vClearBalance,0))-IsNull(@vClearedEffects,0)-(isnull(@Amount,0))-(isNull(@WithDrawlCharges,0)))*100,
          @vClearBalance    = ((IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0))-IsNull(@Amount,0)-isNull(@WithDrawlCharges,0))*100
   
   SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value
   RETURN 0 
     
  END
 END