﻿
CREATE PROCEDURE [dbo].[spc_PRC_PreviousDayBalances] (  
 @OurBranchID varchar(30)  
)  
AS  
BEGIN  
  
 SET NOCOUNT ON  
   
 BEGIN TRY  
   
  Declare @Wdate datetime    
    
  Select @Wdate = WorkingDate from t_Last where OurBranchID = @OurBranchID  
  
  Truncate table t_PreviousDayBalances_Cust   
  Truncate table t_PreviousDayBalances_GL  
    
  Insert Into t_PreviousDayBalances_Cust (CreateBy, CreateTime, CreateTerminal, OurBranchID, WorkingDate, AccountID,  
  ProductID, [Name], ClearBalance, Effects, LocalClearBalance, LocalEffects)  
  Select 'SYSTEM', (@Wdate + ' ' + CONVERT(varchar(10), GetDate(), 108)), host_Name(), OurBranchID, @Wdate, AccountID,  
  ProductID, [Name], ClearBalance, Effects, LocalClearBalance, LocalEffects  
  From t_AccountBalance where OurBranchID = @OurBranchID 
    
  Insert Into t_PreviousDayBalances_GL (CreateBy, CreateTime, CreateTerminal, OurBranchID, WorkingDate, AccountID,  
  [Description], AccountClass, AccountType, OpeningBalance, Balance, ForeignOpeningBalance, ForeignBalance,   
  TemporaryBalance, ShadowBalance)  
  Select 'SYSTEM', (@Wdate + ' ' + CONVERT(varchar(10), GetDate(), 108)), host_Name(), OurBranchID, @Wdate, AccountID,  
  [Description], AccountClass, AccountType, OpeningBalance, Balance, ForeignOpeningBalance, ForeignBalance,   
  TemporaryBalance, ShadowBalance  
  From t_GL where OurBranchID = @OurBranchID 
    
  Select 1 as RetVal  
    
 END TRY  
   
 BEGIN CATCH  
   Select 0 as RetVal, ERROR_MESSAGE() as RetMsg  
 END CATCH  
   
END