﻿CREATE PROCEDURE [dbo].[spc_GL_PostTransaction]            
(              
  @OurBranchID varchar(30),               
  @AccountID varchar(30),               
  @VoucherID numeric(10, 0)='0',            
  @SerialID numeric (10,0),              
  @Date datetime =null,              
  @ValueDate datetime  =null,              
  @DescriptionID varchar(3) =' ',               
  @Description varchar (255)= ' ' ,              
  @CurrencyID varchar(30),              
  @Amount  money =0,              
  @ForeignAmount money=0 ,              
  @ExchangeRate numeric(18, 4)=1 ,              
  @IsCredit bit =0 ,              
  @TransactionType Varchar (32) ='JVPosting' ,              
  @OperatorID varchar(30)= ' ',              
  @SupervisorID varchar(30)= ' ',              
  @Indicator char(1)= ' ' ,              
  @TransactionMethod char(1)=' ',              
  @ScrollNo int = 0  ,              
  @SerialNo int = 0,              
  @AddData TEXT = '',      
  @NewRecord bit=1              
 )              
AS              
set nocount on            
begin try            
 IF cast(@NewRecord as bit) = 1              
  BEGIN            
             
         
 if (select count(*) from t_TransactionDescriptions where OurBranchId = @OurBranchId and   
 DescriptionID = @DescriptionID AND IsCredit = @IsCredit AND Type = 'J') = 0            
  begin            
   raiserror ('NarrationIDNotFound', 15, 1)            
  end            
             
 if @VoucherID = '0'            
 begin            
  Select @VoucherID = isnull(Max(VoucherId)+1, 1) From t_GLTransactions Where OurBranchId = @OurBranchId              
 end            
             
 INSERT INTO t_GLTransactions (OurBranchID, AccountID, VoucherID, SerialID,Date, ValueDate, DescriptionID,             
 Description, Amount, CurrencyID, ForeignAmount, ExchangeRate, IsCredit, TransactionType, OperatorID,             
 SupervisorID,Indicator,TransactionMethod,ScrollNo, SerialNo, AdditionalData)              
 VALUES (@OurBranchID, @AccountID, @VoucherID, @serialID,@Date, @ValueDate, @DescriptionID, @Description,             
 @Amount, @CurrencyID, @ForeignAmount, @ExchangeRate, @IsCredit, @TransactionType, @OperatorID, @SupervisorID,             
 @Indicator,@TransactionMethod,@ScrollNo,@SerialNo,@AddData)      
             
 Select 'Ok' AS ReturnStatus, 'JVTransactionPostedSuccessfully' AS ReturnMessage, @VoucherID AS VoucherID             
             
  END              
end try            
begin catch            
 select 'Error' As ReturnStatus, ERROR_MESSAGE() AS ReturnMessage            
end catch