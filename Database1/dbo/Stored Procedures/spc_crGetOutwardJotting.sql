﻿CREATE Procedure [dbo].[spc_crGetOutwardJotting]
(            
 @OurBranchID as Varchar(30)=''            
)            
As            
set nocount on           
 If Exists(SELECT ScrollNo,ChequeID,Amount FROM t_OutwardClearing Where OurBranchID=@OurBranchID AND Status = 'C')  
              
  SELECT '1' As ReturnValue ,ScrollNo,ChequeID,Amount   
  FROM t_OutwardClearing            
  Where OurBranchID=@OurBranchID AND Status = 'C'  
  Order By ScrollNo            
 Else            
  Select '0' As ReturnValue