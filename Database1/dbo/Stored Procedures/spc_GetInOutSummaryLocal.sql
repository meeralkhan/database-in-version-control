﻿CREATE PROCEDURE [dbo].[spc_GetInOutSummaryLocal]    
    
 @OurBranchID varchar(4),    
 @CurrencyID varchar(4)    
    
AS    

set nocount on
    
 DECLARE @ICAmount AS Numeric(18,2)    
 DECLARE @ICForeignAmount AS Numeric(18,2)    
 DECLARE @ICCount AS Integer    
 DECLARE @IUAmount AS Numeric(18,2)    
 DECLARE @IUForeignAmount AS Numeric(18,2)    
 DECLARE @IUCount AS Integer    
 DECLARE @IRAmount AS Numeric(18,2)    
 DECLARE @IRForeignAmount AS Numeric(18,2)    
 DECLARE @IRCount AS Integer    
 DECLARE @IAAmount AS Numeric(18,2)    
 DECLARE @IAForeignAmount AS Numeric(18,2)    
 DECLARE @IACount AS Integer    
    
 DECLARE @OCAmount AS Numeric(18,2)    
 DECLARE @OCForeignAmount AS Numeric(18,2)    
 DECLARE @OCCount AS Integer    
 DECLARE @OUAmount AS Numeric(18,2)    
 DECLARE @OUForeignAmount AS Numeric(18,2)    
 DECLARE @OUCount AS Integer    
 DECLARE @ORAmount AS Numeric(18,2)    
 DECLARE @ORForeignAmount AS Numeric(18,2)    
 DECLARE @ORCount AS Integer    
 DECLARE @OAAmount AS Numeric(18,2)    
 DECLARE @OAForeignAmount AS Numeric(18,2)    
 DECLARE @OACount AS Integer    
    
 IF @OurBranchID<>''     
  BEGIN    
    
 ---------------------------------------------- Inward Clearing -----------------------------------------------    
--For Cleared Transaction    
   SELECT @ICAmount = isNull(SUM(Amount),0), @ICForeignAmount=isNull(SUM(ForeignAmount),0),    
     @ICCount=isNull(Count(*),0)    
   FROM t_InwardClearing    
   WHERE OurBranchID=@OurBranchID  AND    
     Status IN ( '' ) AND TransactionType='D'    
       
--For Under Supervision    
   SELECT @IUAmount = isNull(SUM(Amount),0), @IUForeignAmount=isNull(SUM(ForeignAmount),0),    
    @IUCount=isNull(Count(*),0)    
   FROM t_InwardClearing    
   WHERE OurBranchID=@OurBranchID AND    
      NOT Status IN ( '','R') AND TransactionType='D'    
       
--For Returned Transaction    
   SELECT @IRAmount = isNull(SUM(Amount),0), @IRForeignAmount=isNull(SUM(ForeignAmount),0),    
     @IRCount=isNull(Count(*),0)    
   FROM t_InwardClearing    
   WHERE OurBranchID=@OurBranchID AND     
      Status IN ( 'R') AND TransactionType='D'    
       
--For All Transaction    
   SELECT @IAAmount = isNull(SUM(Amount),0), @IAForeignAmount=isNull(SUM(ForeignAmount),0),    
     @IACount=isNull(Count(*),0)    
   FROM t_InwardClearing    
   WHERE OurBranchID=@OurBranchID AND      
     TransactionType='D'    
       
-------------------------------------------------End Inward Clearing -----------------------------------------------------    
------------------------------------------------- Outward Clearing -----------------------------------------------------    
    
--For Cleared Transaction    
   SELECT @OCAmount = isNull(SUM(Amount),0), @OCForeignAmount=isNull(SUM(ForeignAmount),0),    
     @OCCount=isNull(Count(*),0)    
   FROM t_OutwardClearing    
   WHERE OurBranchID=@OurBranchID AND     
     Status IN ( '','C' ) AND TransactionType='C'    
    
--For Under Supervision    
   SELECT @OUAmount = isNull(SUM(Amount),0), @OUForeignAmount=isNull(SUM(ForeignAmount),0),    
     @OUCount=isNull(Count(*),0)    
   FROM t_OutwardClearing    
   WHERE OurBranchID=@OurBranchID and     
     NOT Status IN ( '','R','C') AND TransactionType='C'    
       
--For Returned Transaction    
   SELECT @ORAmount = isNull(SUM(Amount),0), @ORForeignAmount=isNull(SUM(ForeignAmount),0),    
     @ORCount=isNull(Count(*),0)    
   FROM t_OutwardClearing    
   WHERE OurBranchID=@OurBranchID and     
     Status IN ( 'R') AND TransactionType='C'    
       
--For All Transaction    
   SELECT @OAAmount = isNull(SUM(Amount),0), @OAForeignAmount=isNull(SUM(ForeignAmount),0),     
     @OACount=isNull(Count(*),0)     
   FROM t_OutwardClearing    
   WHERE OurBranchID=@OurBranchID and     
     TransactionType='C'    
    
------------------------------ End Outward Clearing ----------------------------------------------------    
    
---------------------------------------------------------------------- Return Values ----------------------------------------------------------    
  SELECT @ICAmount As ICAmount, @ICForeignAmount As ICForeignAmount, @ICCount AS ICCount,    
      @IUAmount As IUAmount, @IUForeignAmount As IUForeignAmount, @IUCount AS IUCount,    
     @IRAmount As IRAmount, @IRForeignAmount As IRForeignAmount, @IRCount AS IRCount,    
     @IAAmount As IAAmount, @IAForeignAmount As IAForeignAmount, @IACount AS IACount,    
     @OCAmount As OCAmount, @OCForeignAmount As OCForeignAmount, @OCCount AS OCCount,    
     @OUAmount As OUAmount, @OUForeignAmount As OUForeignAmount, @OUCount AS OUCount,    
     @ORAmount As ORAmount, @ORForeignAmount As ORForeignAmount, @ORCount AS ORCount,    
     @OAAmount As OAAmount, @OAForeignAmount As OAForeignAmount, @OACount AS OACount    
    
  END