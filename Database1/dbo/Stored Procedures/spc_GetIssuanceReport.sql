﻿  
CREATE PROCEDURE [dbo].[spc_GetIssuanceReport]            
(            
 @OurBranchID varchar(4)='',            
 @ToDate datetime,            
 @FromDate datetime,            
 @ReportType varchar(50)=''            
)            
  AS            
  set nocount on         
            
 IF (@ReportType = 'Issuance')             
  BEGIN            
   SELECT * from vc_DepositIssuance            
   WHERE OurBranchID=@OurBranchID AND             
   (StartDate BETWEEN @FromDate AND @ToDate)            
   ORDER BY Period, StartDate          
  END            
            
 ELSE            
  BEGIN            
   SELECT * from vc_DepositIssuance            
   WHERE OurBranchID=@OurBranchID AND             
   (StartDate BETWEEN '01/01/1800' AND @ToDate) AND (closedate ='01/01/1900' or CloseDate IS NULL OR CloseDate > @ToDate)            
   ORDER BY Period, StartDate            
  END