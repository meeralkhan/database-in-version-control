﻿CREATE PROCEDURE [dbo].[sp_ATM_AddEditTransferReversalTransaction]  
(  
 @OurBranchID varchar(30),  
 @RefNo varchar(50),  
 @VISATrxID varchar(50)='',  
 @CAVV char(1)='',  
 @ResponseCode varchar(2)='',
 @AuthIDResp varchar(6)=''
)  
 AS  
 SET NOCOUNT ON  
  
 DECLARE @Status            as int  
 DECLARE @AccountID         as varchar (30)  
 DECLARE @mAccountID   as varchar(30)    
 DECLARE @Description       as varchar (255)   
 DECLARE @mwDate            as datetime  
 DECLARE @mLastEOD          as datetime  
 DECLARE @mCONFRETI         as datetime  
 DECLARE @vIsClose          as char(1)  
 DECLARE @vClearBalance as money        
    DECLARE @AvailableBalance as money        
    DECLARE @Value as varchar(2)='00'  
    DECLARE @vClearedEffects as money        
    DECLARE @vFreezeAmount as money        
    DECLARE @vMinBalance as money        
    DECLARE @Limit as money        
 DECLARE @CurrencyCode as varchar(30)  
 DECLARE @vCheckLimit as bit  
     SET @vCheckLimit = 0  
 DECLARE @vODLimitAllow as bit  
     SET @vODLimitAllow = 0  
 DECLARE @HoldAmount as money  
 DECLARE @ForeignAmount as money  
 DECLARE @FreezeAmount as money  
 DECLARE @AcqInstID as varchar(30)  
 DECLARE @remAmount as numeric(18,6)  
    DECLARE @mRefNo as varchar(50)  
    DECLARE @mHoldStatus as char(1)  
 DECLARE @sCount as int  
     SET @sCount = 0  
    DECLARE @cCount as int  
     SET @cCount = 0  
 DECLARE @mCount as int  
  SET @mCount = 0  
 DECLARE @ProcCode as varchar(50)  
 DECLARE @RetRefNum as varchar(15)  
 DECLARE @LocalCurrencyID as varchar(30)  
 DECLARE @IsLocalCurrency as bit  
  SET @IsLocalCurrency = 0  
  
 DECLARE @STAN as varchar(30)
 DECLARE @PHXDate as datetime 
 DECLARE @ExchangeRate as decimal(16,8)
 DECLARE @Amount as money
 DECLARE @MerchantType as varchar(30)
 DECLARE @AcqCountryCode as varchar(3)
 DECLARE @ATMID as varchar(30)
 DECLARE @CurrCodeTran as varchar(3)
 DECLARE @CurrCodeSett as varchar(3)
 DECLARE @SettlmntAmount as money
 DECLARE @ConvRate as decimal(15,9)
 DECLARE @POSEntryMode as varchar(3)
 DECLARE @POSConditionCode as varchar(2)
 DECLARE @POSPINCaptCode as varchar(2)
 DECLARE @CardAccptID as varchar(15)
 DECLARE @CardAccptNameLoc as varchar(50)
 DECLARE @DescriptionID as varchar(30)
 DECLARE @ForwardInstID as varchar(10)
 DECLARE @MobileNo nvarchar(30)
 DECLARE @Email nvarchar(100)
 DECLARE @rCurrencyID nvarchar(30)
 DECLARE @ClientID nvarchar(30)
 DECLARE @NarrationID nvarchar(30)
  SET @MobileNo = ''
  SET @Email = ''
  SET @rCurrencyID = ''
  SET @ClientID = ''
  SET @NarrationID = ''
  
 if (@AuthIDResp = '')
 begin
  SET @AuthIDResp = '0'
 end
  
 SELECT @mLastEOD=LastEOD,@mwDate=WorkingDate,@mCONFRETI=CONFRETI FROM t_Last WHERE (OurBranchID = @OurBranchID)  
 
 SELECT @AccountID=AccountID,@STAN=STAN,@PHXDate=PHXDate,@ForeignAmount=ForeignAmount,@ExchangeRate=ExchangeRate,@Amount=Amount,@MerchantType=MerchantType,
 @AcqCountryCode=AcqCountryCode,@ATMID=ATMID,@CurrCodeTran=CurrCodeTran,@CurrCodeSett=CurrCodeSett,@SettlmntAmount=SettlmntAmount,@ProcCode=ProcCode,@POSEntryMode=POSEntryMode,
 @POSConditionCode=POSConditionCode,@POSPINCaptCode=POSPINCaptCode,@AcqInstID=AcqInstID,@RetRefNum=RetRefNum,
 @AuthIDResp = Case when @AuthIDResp = '0' then ISNULL(AuthIDResp,'0') else @AuthIDResp end,
 @CardAccptID=CardAccptID,@CardAccptNameLoc=CardAccptNameLoc,@Description=ISNULL([Description],''),@CAVV=CAVV,@DescriptionID=DescriptionID
 FROM t_ATM_HoldTrxs WHERE OurBranchID=@OurBranchID AND RefNo = @RefNo
 
 --SET @NarrationID = @DescriptionID
 SET @NarrationID = 'FREVXX'
  
 IF @mLastEOD = @mwDate or @mCONFRETI = '1/1/9999'  
 BEGIN --INVALID_DATE  
  INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, 
  ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, 
  CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
  VALUES (@OurBranchID, @AccountID, @RefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @ForeignAmount, @ExchangeRate, @ConvRate, 
  @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, 
  @AcqInstID, @RetRefNum, @CardAccptID, @CardAccptNameLoc, @Description, @VISATrxID, @CAVV, @ResponseCode, @DescriptionID, '95', 'Day End', 'Reversal Trx', @AuthIDResp, '')
  
  SELECT @Status = 95, @AvailableBalance = 0, @vClearBalance = 0  
  SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value,'784' as CurrencyCode, @AuthIDResp as AuthIDResp,
  @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
  RETURN (1)   
 END  
  
 /* First check whether the pre-authorization exists for the trx */  
  
 SELECT @mCount=count(*), @remAmount = SUM(ISNULL(RemainingAmount,0)), @AccountID=AccountID, @AcqInstID=AcqInstID
 FROM t_ATM_HoldTrxs WHERE (OurBranchID=@OurBranchID) AND HoldStatus = 'H'   
 AND (RefNo IN (@RefNo,@RefNo+'|CHGS1',@RefNo+'|CHGS2') AND VISATrxID=@VISATrxID)  
 group by AccountID, AcqInstID
  
 SELECT @vCheckLimit=CheckLimit,@LocalCurrencyID=LocalCurrencyID FROM t_ATM_GlobalVariables Where OurBranchID = @OurBranchID  
 SELECT @vODLimitAllow=ODLimitAllow FROM t_ATM_Banks WHERE OurBranchID = @OurBranchID AND BankIMD = @AcqInstID  
  
 IF (@mCount > 0)  
 BEGIN  
  SELECT @vClearBalance=ISNULL(B.ClearBalance,0),@vClearedEffects=ISNULL(B.Effects,0),@vFreezeAmount=ISNULL(B.FreezeAmount,0),@Limit=ISNULL(B.Limit,0),
  @vMinBalance=ISNULL(P.MinBalance,0),@vIsClose=ISNULL(A.Status,''),@CurrencyCode = ISNULL(c.CurrencyCode,''),@value=ISNULL(p.productatmcode,'00'), 
  @MobileNo=ISNULL(cust.MobileNo,''),@Email=ISNULL(cust.Email,''),@ClientID=cust.ClientID
  FROM t_AccountBalance B   
  INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND  B.ProductID = A.ProductID   
  INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND  A.ProductID = P.ProductID   
  INNER JOIN t_Currencies C ON A.OurBranchID = C.OurBranchID AND A.CurrencyID = C.CurrencyID  
  INNER JOIN t_Customer cust ON B.OurBranchID = cust.OurBranchID AND A.ClientID = cust.ClientID
  WHERE (A.OurBranchID = @OurBranchID) AND (A.AccountID = @AccountID)  
     
  --exec @Limit = [fnc_GetLimit] @OurBranchID, @AccountID  
  
  IF @Limit > 0 AND @vCheckLimit = 1 AND @vODLimitAllow = 1  
     BEGIN  
      SET @vClearBalance=(@vClearBalance+@Limit)  
     END  
  
  if (@LocalCurrencyID = @CurrencyCode)  
     BEGIN  
      SET @IsLocalCurrency = 1  
     END  
  
  Select * Into #TempTransferReversalTransaction FROM t_ATM_HoldTrxs WHERE (OurBranchID=@OurBranchID) AND (AccountID=@AccountID)   
  AND (RefNo IN (@RefNo,@RefNo+'|CHGS1',@RefNo+'|CHGS2') AND VISATrxID=@VISATrxID) AND HoldStatus = 'H'  
  
  SELECT @cCount=COUNT(*) from #TempTransferReversalTransaction  
  
  WHILE (@cCount <> @sCount)  
     BEGIN  
      SET @sCount = @sCount + 1  
  
      SELECT @mAccountID=AccountID, @mRefNo=RefNo, @mHoldStatus=HoldStatus, @ProcCode=ProcCode, @Description=[Description], @RetRefNum=RetRefNum,   
   @ForeignAmount=ForeignAmount  
      FROM (  
       SELECT AccountID, RefNo, HoldStatus, ProcCode, Description, RetRefNum, ForeignAmount, ROW_NUMBER() OVER (ORDER BY RefNo) AS RowNum  
       FROM #TempTransferReversalTransaction  
      ) AS oTable  
      WHERE oTable.RowNum = @sCount  
  
   --Reverse Hold Trx Status  
      UPDATE t_ATM_HoldTrxs SET HoldStatus = 'R', ReleaseDate = @mwDate + ' ' + convert(varchar(12), GetDate(), 114),  
   [Description] = 'REV-'+@Description, ResponseCode=@ResponseCode  
      WHERE OurBranchID = @OurBranchID AND AccountID = @mAccountID AND HoldStatus = 'H' AND RefNo = @mRefNo  
  
   if (@ProcCode = '20') --Credit Voucher/ Merchandise Return   
   BEGIN  
    --update credit account freeze to unfreeze  
  
    SELECT @HoldAmount=Amount FROM t_AccountFreezeCredit   
       WHERE OurBranchID=@OurBranchID AND AccountID=@mAccountID AND IsFreeze = 1 AND ATMTrxHoldRef = @mRefNo  
  
       Update t_AccountFreezeCredit SET IsFreeze = 0, UnFreezedDate = @mwDate + ' ' + convert(varchar(12), GetDate(), 114),   
       ReleaseComments = 'REV-'+@Description  
       WHERE OurBranchID = @OurBranchID AND AccountID = @mAccountID AND ATMTrxHoldRef = @mRefNo  
      
    update t_Clearing Set Status = 'R'  
    WHERE OurBranchID = @OurBranchID AND AccountID = @mAccountID AND ChequeID = @RetRefNum  
  
    IF @IsLocalCurrency = 1              
       BEGIN              
        Update t_AccountBalance Set Effects =  ISNULL(Effects,0) - @HoldAmount WHERE OurBranchID = @OurBranchID and AccountID = @mAccountID  
       END  
       ELSE  
       BEGIN  
        Update t_AccountBalance Set Effects =  ISNULL(Effects,0) - @ForeignAmount, LocalEffects = ISNULL(LocalEffects,0) - @HoldAmount  
        Where  OurBranchID = @OurBranchID and AccountID = @mAccountID  
       END  
   END  
   ELSE  
   BEGIN  
       --Get Hold Amount  
       SELECT @HoldAmount=Amount FROM t_AccountFreeze   
       WHERE OurBranchID=@OurBranchID AND AccountID=@mAccountID AND IsFreeze = 1 AND ATMTrxHoldRef = @mRefNo  
       
       --update account freeze to unfreeze  
       Update t_AccountFreeze SET IsFreeze = 0, UnFreezedDate = @mwDate + ' ' + convert(varchar(12), GetDate(), 114), ReleaseComments = 'REV-'+@Description  
       WHERE OurBranchID = @OurBranchID AND AccountID = @mAccountID AND ATMTrxHoldRef = @mRefNo  
       
       Update t_AccountBalance SET FreezeAmount = ISNULL(FreezeAmount,0) - ISNULL(@HoldAmount,0)  
       WHERE OurBranchID = @OurBranchID AND AccountID = @mAccountID  
       
       SELECT @FreezeAmount = FreezeAmount FROM t_AccountBalance WHERE OurBranchID = @OurBranchID AND AccountID = @mAccountID  
       
       IF (@FreezeAmount <= 0)  
       BEGIN  
        Update t_AccountBalance SET IsFreezed = 0  
        WHERE OurBranchID = @OurBranchID AND AccountID = @mAccountID  
       END  
   END  
  END  
  
  /* Log the Updated Balance after releasing freeze */  
       
     Insert Into BalChk   
     (  
      OurBranchID, AccountID, ProductID, [Name], LastUpdateTime, ClearBalance, Limit, Effects, Shadow, FreezeAmt, MinBalance, TotalBalance, AvailableBalance  
     )  
     SELECT OurBranchID, AccountID, ProductID, [Name], @mwDate + ' ' + convert(varchar(12), GetDate(), 114), ISNULL(ClearBalance,0), ISNULL(Limit,0), ISNULL(Effects,0), ISNULL(ShadowBalance,0),   
  ISNULL(FreezeAmount,0), @vMinBalance,   
     (isnull(@Limit,0)+ISNULL(ClearBalance,0)-ISNULL(Effects,0)),  
     (isnull(@Limit,0)+ISNULL(ClearBalance,0)-ISNULL(FreezeAmount,0)-@vMinBalance)  
     FROM t_AccountBalance WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID  
        
     /* Get Latest Balance again.. */  
       
     SELECT @vClearBalance=ISNULL(B.ClearBalance,0),@vClearedEffects=ISNULL(B.Effects,0),@vFreezeAmount=ISNULL(B.FreezeAmount,0),@Limit=ISNULL(B.Limit,0),
     @vMinBalance=ISNULL(P.MinBalance,0),@vIsClose=ISNULL(A.Status,''),@CurrencyCode = ISNULL(c.CurrencyCode,''),@value=ISNULL(p.productatmcode,'00')  
     FROM t_AccountBalance B   
     INNER JOIN t_Account A ON B.OurBranchID = A.OurBranchID AND B.AccountID = A.AccountID AND  B.ProductID = A.ProductID   
     INNER JOIN t_Products P ON A.OurBranchID = P.OurBranchID AND  A.ProductID = P.ProductID   
     INNER JOIN t_Currencies C ON A.OurBranchID = C.OurBranchID AND A.CurrencyID = C.CurrencyID  
     WHERE (A.OurBranchID = @OurBranchID) AND (A.AccountID = @AccountID)  
       
     --exec @Limit = [fnc_GetLimit] @OurBranchID, @AccountID  
       
     IF @Limit > 0 AND @vCheckLimit = 1 AND @vODLimitAllow = 1  
     BEGIN  
      SET @vClearBalance=(@vClearBalance+@Limit)  
     END  
  
  SELECT @Status = 0,     
  @AvailableBalance = ( isnull(@vClearBalance,0)-ISNULL(@vFreezeAmount,0)-ISNULL(@vMinBalance,0) )*100,  
  @vClearBalance    = ( IsNull(@vClearBalance,0)-IsNull(@vClearedEffects,0) )*100  
    
  SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value,@CurrencyCode as CurrencyCode, @AuthIDResp as AuthIDResp,
  @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
  RETURN (0)  
  
 END  
 ELSE  
 BEGIN  
 
  INSERT INTO t_RejectedHostTrxs (OurBranchID, AccountID, RefNo, STAN, PHXDate, wDate, Amount, ForeignAmount, ExchangeRate, ConvRate, MerchantType, AcqCountryCode, 
  ForwardInstID, ATMID, CurrCodeTran, CurrCodeSett, SettlmntAmount, ProcCode, POSEntryMode, POSConditionCode, POSPINCaptCode, AcqInstID, RetRefNum, CardAccptID, 
  CardAccptNameLoc, Description, VISATrxID, CAVV, ResponseCode, DescriptionID, RejectReasonCode, RejectReasonDescription, TrxDetails, AuthIDResp, ExtendedData)
  VALUES (@OurBranchID, @AccountID, @RefNo, @STAN, @PHXDate, @mwDate + ' ' + convert(varchar(12), GetDate(), 114), @Amount, @ForeignAmount, @ExchangeRate, @ConvRate, 
  @MerchantType, @AcqCountryCode, @ForwardInstID, @ATMID, @CurrCodeTran, @CurrCodeSett, @SettlmntAmount, @ProcCode, @POSEntryMode, @POSConditionCode, @POSPINCaptCode, 
  @AcqInstID, @RetRefNum, @CardAccptID, @CardAccptNameLoc, @Description, @VISATrxID, @CAVV, @ResponseCode, @DescriptionID, '21', 'No Transaction on Account', 'Reversal Trx', 
  @AuthIDResp, '')
  
     SELECT @Status = 21, @AvailableBalance = 0, @vClearBalance = 0  
     SELECT @Status As Status, @vClearBalance As ClearBalance, @AvailableBalance As AvailableBalance, @value As value,'784' as CurrencyCode, @AuthIDResp as AuthIDResp,
	 @MobileNo as MobileNo, @Email as Email, @rCurrencyID as CurrencyID, @ClientID as ClientID, @NarrationID as NarrationID
        
     RETURN (0)  
 END