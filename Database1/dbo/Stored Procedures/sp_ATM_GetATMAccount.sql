﻿CREATE PROCEDURE [dbo].[sp_ATM_GetATMAccount]
	(
		@OurBranchID varchar(30),
		@ATMID varchar(30)
	)
 
AS
	BEGIN
		SELECT ATMID,  ATMAccountID, ATMAccountTitle, ATMCurrencyID, ATMCurrencyName, ATMProductID
		FROM t_ATM_ATMAccount
		WHERE OurBranchID=@OurBranchID and ATMID= @ATMID
	END