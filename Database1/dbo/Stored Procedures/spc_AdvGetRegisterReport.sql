﻿CREATE PROCEDURE [dbo].[spc_AdvGetRegisterReport]
(                      
 @OurBranchID varchar(30),  
 @AccountID varchar(30)='',  
 @FromDate datetime,              
 @ToDate datetime              
)                      
AS                      
BEGIN                      
                  
SET NOCOUNT ON                  
  
if (@AccountID = '')      
BEGIN  
SELECT d.OurBranchID, d.AccountID, a.Name, a.ClientID, d.DealID, d.ReferenceNo, d.Advancesdate, d.MaturityDate, d.FirstPaymentOn,   
 case d.ProfitRateType   
  when 'Fixed' then d.ProfitRate  
  when 'Floating' then d.BasePercent  
 end as ProfitRate,   
 d.Amount as DisbursementAmount, d.Method, d.PaymentMethod, isnull(d.Fee,0) as BrokerFee, isnull(d.Charges,0) as Charges,  
 d.ScrollNo, IsClosed = isnull(d.IsClosed, 0), d.CloseDate, p.[Description] as ProductDesc,   
 [Status] = Case  
  when ISNULL(d.AuthStatus,'') <> '' then 'Under-Sup'  
  when ISNULL(d.IsClosed,0) = 1 then 'Closed'  
  when ISNULL(d.Status,'') = '*' OR ISNULL(d.Status,'C') = 'U' then 'Trx-Under-Sup'  
  when ISNULL(d.Status,'') = 'R' then 'Rejected'  
  when ISNULL(d.Status,'') = 'P' then 'Remote-Pass-Sup'  
  when ISNULL(d.Status,'') = 'C' then 'Active'  
 end,  
 d.GracePeriod, ISNULL(d.Exempt,'0') Exempt, ISNULL(d.ExemptType,'') ExemptType, ISNULL(d.ExemptPayType,'') ExemptPayType,  
 ISNULL(d.AssetValue,0) AssetValue, c.Description as CategoryDesc, r.Description as RiskDescription, d.FSVCollateralValue,   
 d.LiquidAssetValue, ab.Limit, p.CurrencyID, lm.AccountLimit, lm.DrawingPower, ab.ClearBalance,  
  
 ( select IsNull(Sum(t_Adv_PaymentSchedule.ToDateProfit),0) from t_Adv_PaymentSchedule  
   Where t_Adv_PaymentSchedule.OurBranchID = d.OurBranchID And t_Adv_PaymentSchedule.DealID = d.DealID  
   AND t_Adv_PaymentSchedule.AccountID = d.AccountID      
 ) as InterestAmt,  
 d.CreateBy as Operator, d.SuperviseBy as Supervisor  
  
 FROM t_Disbursement d  
 INNER JOIN t_Account a ON d.OurBranchID = a.OurBranchID AND d.AccountID = a.AccountID       
 INNER JOIN t_Products p ON a.OurBranchID = p.OurBranchID AND a.ProductID = p.ProductID                       
 INNER JOIN t_Adv_Account_LimitMaintain lm on d.OurBranchID = lm.OurBranchID AND d.AccountID = lm.AccountID  
 INNER JOIN t_Accountbalance ab ON d.OurBranchID = ab.OurBranchID AND d.AccountID = ab.AccountID   
 LEFT JOIN t_AdvCategories c ON d.OurBranchID = c.OurBranchID AND d.CategoryID = c.CategoryID  
 LEFT JOIN t_RiskClassification r ON d.OurBranchID = r.OurBranchID AND d.RiskCode = r.Code  
 WHERE d.OurBranchID = @OurBranchID and isnull(d.Status,'') <> 'R' AND d.AdvancesDate between @FromDate AND @ToDate  
 ORDER BY d.AdvancesDate, a.ClientID  
  
END  
ELSE  
BEGIN  
  
 SELECT d.OurBranchID, d.AccountID, a.Name, a.ClientID, d.DealID, d.ReferenceNo, d.Advancesdate, d.MaturityDate, d.FirstPaymentOn,   
 case d.ProfitRateType   
  when 'Fixed' then d.ProfitRate  
  when 'Floating' then d.BasePercent  
 end as ProfitRate,   
 d.Amount as DisbursementAmount, d.Method, d.PaymentMethod, isnull(d.Fee,0) as BrokerFee, isnull(d.Charges,0) as Charges,  
 d.ScrollNo, IsClosed = isnull(d.IsClosed, 0), d.CloseDate, p.[Description] as ProductDesc,   
 [Status] = Case  
  when ISNULL(d.AuthStatus,'') <> '' then 'Under-Sup'  
  when ISNULL(d.IsClosed,0) = 1 then 'Closed'  
  when ISNULL(d.Status,'') = '*' OR ISNULL(d.Status,'C') = 'U' then 'Trx-Under-Sup'  
  when ISNULL(d.Status,'') = 'R' then 'Rejected'  
  when ISNULL(d.Status,'') = 'P' then 'Remote-Pass-Sup'  
  when ISNULL(d.Status,'') = 'C' then 'Active'  
 end,  
 d.GracePeriod, ISNULL(d.Exempt,'0') Exempt, ISNULL(d.ExemptType,'') ExemptType, ISNULL(d.ExemptPayType,'') ExemptPayType,  
 ISNULL(d.AssetValue,0) AssetValue, c.Description as CategoryDesc, r.Description as RiskDescription, d.FSVCollateralValue,   
 d.LiquidAssetValue, ab.Limit, p.CurrencyID, lm.AccountLimit, lm.DrawingPower, ab.ClearBalance,  
  
 ( select IsNull(Sum(t_Adv_PaymentSchedule.ToDateProfit),0) from t_Adv_PaymentSchedule  
   Where t_Adv_PaymentSchedule.OurBranchID = d.OurBranchID And t_Adv_PaymentSchedule.DealID = d.DealID  
   AND t_Adv_PaymentSchedule.AccountID = d.AccountID      
 ) as InterestAmt,  
 d.CreateBy as Operator, d.SuperviseBy as Supervisor  
  
 FROM t_Disbursement d  
 INNER JOIN t_Account a ON d.OurBranchID = a.OurBranchID AND d.AccountID = a.AccountID       
 INNER JOIN t_Products p ON a.OurBranchID = p.OurBranchID AND a.ProductID = p.ProductID                       
 INNER JOIN t_Adv_Account_LimitMaintain lm on d.OurBranchID = lm.OurBranchID AND d.AccountID = lm.AccountID  
 INNER JOIN t_Accountbalance ab ON d.OurBranchID = ab.OurBranchID AND d.AccountID = ab.AccountID   
 LEFT JOIN t_AdvCategories c ON d.OurBranchID = c.OurBranchID AND d.CategoryID = c.CategoryID  
 LEFT JOIN t_RiskClassification r ON d.OurBranchID = r.OurBranchID AND d.RiskCode = r.Code  
 WHERE d.OurBranchID = @OurBranchID and d.AccountID = @AccountID AND isnull(d.Status,'') <> 'R'   
 AND d.AdvancesDate between @FromDate AND @ToDate  
 ORDER BY d.AdvancesDate, a.ClientID  
  
  
END  
END