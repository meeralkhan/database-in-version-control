﻿
CREATE   FUNCTION [dbo].[vc_Function_GroupRiskAVGUtil] ( 
@ClientID Varchar(30) = '',  
@sdate DATETIME = '1900-01-01',          
@edate DATETIME = '2027-05-21'         
)                        
RETURNS TABLE                        
AS                        
RETURN    
Select a.ClientID, a.Name, a.GroupID, Max(Utilization) PeakUtilization, Sum(Utilization)/Count(NoOfDays) AVGUtilization, Count(NoOfDays) NoOFDays from (  
Select GroupID, ClientID, Name, Sum(Balance) Utilization, MAx(NoOfDPD) DPD, wDate, Count(*) NoOfDays  from vc_Risk_AccountBalanceHistory   
Where GroupID IN (Select GroupID from t_Customer WITH (NOLOCK) Where ClientID = @ClientID) AND (wdate >= @sDate AND wDate <= @eDate)  
Group By ClientID, Name, wDate, GroupID   
) as a  Group By a.ClientID, a.Name, a.NoOfDays, a.GroupID