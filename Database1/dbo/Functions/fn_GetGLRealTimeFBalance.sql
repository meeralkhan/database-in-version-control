﻿CREATE FUNCTION [dbo].[fn_GetGLRealTimeFBalance]      
(      
 @OurBranchID NVARCHAR(30),
 @AccountID NVARCHAR(30)
)      
RETURNS Money      
      
AS      
      
BEGIN      
      
--SET NOCOUNT ON      
      
DECLARE @ClearBalance MONEY      
DECLARE @crBal MONEY      
DECLARE @drBal MONEY      
DECLARE @wDate DATETIME      
      
SELECT @wDate = WorkingDate FROM t_Last WHERE OurBranchID = @OurBranchID     
SELECT @ClearBalance = IsNull(ForeignOpeningBalance,0) FROM t_GL WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID      
      
SELECT @crBal = IsNull(SUM(ForeignAmount),0) From t_GLTransactions WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND IsCredit = '1' AND Date <= @wDate      
SELECT @drBal = IsNull(SUM(ForeignAmount),0) From t_GLTransactions WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND IsCredit = '0' AND Date <= @wDate      
      
SET @ClearBalance = @ClearBalance + @crBal - @drBal      
      
SELECT @crBal = ISNULL(SUM(ForeignAmount),0) FROM t_CashTransactionModel WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND TrxType = 'C' AND Supervision = 'C'      
SELECT @drBal = ISNULL(SUM(ForeignAmount),0) FROM t_CashTransactionModel WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND TrxType = 'D' AND Supervision = 'C'      
      
SET @ClearBalance = @ClearBalance + @crBal - @drBal      
      
SELECT @crBal = ISNULL(SUM(ForeignAmount),0) FROM t_CashTransactionModel WHERE OurBranchID = @OurBranchID AND GLID = @AccountID AND TrxType = 'D' AND Supervision = 'C'      
SELECT @drBal = ISNULL(SUM(ForeignAmount),0) FROM t_CashTransactionModel WHERE OurBranchID = @OurBranchID AND GLID = @AccountID AND TrxType = 'C' AND Supervision = 'C'      
      
SET @ClearBalance = @ClearBalance + @crBal - @drBal      
      
SELECT @crBal = ISNULL(SUM(ForeignAmount),0) FROM t_TransferTransactionModel WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND TrxType = 'C' AND Supervision = 'C'      
SELECT @drBal = ISNULL(SUM(ForeignAmount),0) FROM t_TransferTransactionModel WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND TrxType = 'D' AND Supervision = 'C'      
      
SET @ClearBalance = @ClearBalance + @crBal - @drBal      
      
SELECT @crBal = ISNULL(SUM(ForeignAmount),0) FROM t_InWardClearing WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND TransactionType = 'C' AND Status = 'C'      
SELECT @drBal = ISNULL(SUM(ForeignAmount),0) FROM t_InWardClearing WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND TransactionType = 'D' AND Status = 'C'      
      
SET @ClearBalance = @ClearBalance + @crBal - @drBal      
      
SELECT @crBal = ISNULL(SUM(ForeignAmount),0) FROM t_InWardClearing WHERE OurBranchID = @OurBranchID AND AccountID = (SELECT AccountID From t_GLParameters WHERE OurBranchID = @OurBranchID AND SerialID = '2') AND Status = 'C'      
      
SET @ClearBalance = @ClearBalance + @crBal      
      
SELECT @crBal = ISNULL(SUM(ForeignAmount),0) FROM t_BureaDeChange WHERE OurBranchID = @OurBranchID AND CashAccountID = @AccountID AND SellOrBuy = 'B' AND Supervision = 'C'      
SELECT @drBal = ISNULL(SUM(ForeignAmount),0) FROM t_BureaDeChange WHERE OurBranchID = @OurBranchID AND CashAccountID = @AccountID AND SellOrBuy = 'S' AND Supervision = 'C'      
      
SET @ClearBalance = @ClearBalance + @crBal - @drBal      
      
SELECT @crBal = ISNULL(SUM(ForeignAmount),0) FROM t_BureaDeChange WHERE OurBranchID = @OurBranchID AND CommAccountID = @AccountID AND Supervision = 'C'      
      
SET @ClearBalance = @ClearBalance + @crBal       
      
SELECT @crBal = ISNULL(SUM(ForeignAmount),0) FROM t_OnlineCashTransaction WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND TrxType = 'C' AND Supervision = 'C'      
SELECT @drBal = ISNULL(SUM(ForeignAmount),0) FROM t_OnlineCashTransaction WHERE OurBranchID = @OurBranchID AND AccountID = @AccountID AND TrxType = 'D' AND Supervision = 'C'      
      
SET @ClearBalance = @ClearBalance + @crBal - @drBal      
      
RETURN @ClearBalance      
      
END