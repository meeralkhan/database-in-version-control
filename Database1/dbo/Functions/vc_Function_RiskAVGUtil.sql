﻿
CREATE   FUNCTION [dbo].[vc_Function_RiskAVGUtil] ( 
@ClientID Varchar(30) = '',  
@sdate DATETIME = '1900-01-01',          
@edate DATETIME = '2027-05-21'         
)                        
RETURNS TABLE                        
AS                        
RETURN    
Select a.ClientID, a.Name, Max(Utilization) PeakUtilization, Sum(Utilization)/Count(NoOfDays) AVGUtilization, Count(NoOfDays) NoOFDays from (  
Select ClientID, Name, Sum(Balance) Utilization, MAx(NoOfDPD) DPD, wDate, Count(*) NoOfDays  from vc_Risk_AccountBalanceHistory   
Where ClientID = @ClientID AND (wdate >= @sDate AND wDate <= @eDate)  
Group By ClientID, Name, wDate   
) as a  Group By a.ClientID, a.Name, a.NoOfDays