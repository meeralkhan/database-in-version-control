﻿CREATE  FUNCTION [dbo].[GetTrailingValue]
(
	@data varchar(100),
	@RequiredLength int,
	@charactor  char(1),
	@isPre	bit
)
RETURNS varchar(100)
AS
BEGIN
declare @val as varchar(100)
	
	set @val = @data;
	while ( LEN(@val) < @RequiredLength)
	begin
			if(@isPre = 1 )
			 set @val = @charactor+@val
			 else
			 set @val = @val+@charactor
	end

	RETURN @val

END