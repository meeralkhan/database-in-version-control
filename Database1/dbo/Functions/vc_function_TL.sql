﻿CREATE   FUNCTION [dbo].[vc_function_TL] (            
 -- For Term Loan's Ledger Jotting            
    @eDate Date            
)            
RETURNS TABLE            
AS            
RETURN            
Select p.OurBranchID, p.ClientID, p.Name, p.AccountID, p.DealID, p.AccountName, p.ProductID, p.CurrencyID, p.DisbursementDate, p.MaturityDate, p.Limit, p.PDueAmount, p.MDueAmount,            
p.PODAmount, p.MODAmount, p.TotalODAmount, p.InterestAccrued, p.PPaid, p.MPaid, p.PBalance, p.MBalance, p.ProfitRate, p.ProfitRateType, p.PaymentMethod, p.RiskCode, p.RiskDesc, ra.AccountID MAccountID            
, p.CloseDate, ISNULL(p.IsClosed,'0') IsCLosed from (            
Select OurBranchID, ClientID, Name, AccountID, DealID, AccountName, ProductID, CurrencyID, DisbursementDate, MaturityDate, Limit, PDueAmount, MDueAmount, Sum(PODAmount) PODAmount, Sum(MODAmount) MODAmount,             
Sum(TotalODAmount) TotalODAmount, -Accrued InterestAccrued, Limit - (Limit - (PDueAmount - Sum(PODAmount))) PPaid             
, Case When ISNULL(CloseDate,'1900-01-01') <= @eDate AND IsClosed = '1' THEN -Accrued ELSE -Accrued - (-Accrued - (MDueAmount - sum(MODAmount))) END MPaid             
, Limit - (PDueAmount - Sum(PODAmount)) PBalance            
, Case When ISNULL(CloseDate,'1900-01-01') <= @eDate AND IsClosed = '1' THEN 0 ELSE -Accrued - (MDueAmount - sum(MODAmount)) END MBalance, ProfitRate, ProfitRateType, PaymentMethod, RiskCode, RiskDesc, CloseDate, IsClosed  from (             
Select Distinct p.OurBranchID, c.ClientID, c.Name,  p.AccountID, p.DealID, a.Name AccountName,  a.ProductID, d.Amount Limit, p.InstAmount, p.DueDate, d.RiskCode, rc.Description RiskDesc            
,p.PayDate, p.IsPosted, d.MaturityDate , a.CurrencyID, d.DisbursementDate , d.ProfitRate, d.ProfitRateType, d.PaymentMethod, p.InstNo, p.InstallmentStatus, d.closedate, d.isclosed,            
Case When p.InstallmentStatus = 'P' THEN             
 Case WHEN isposted = 1 Then 0 ELSE ISNULL((Select Sum(Amount) from t_TrackReceivables r Where p.OurBranchID = r.OurBranchID AND p.AccountID = r.AccountID AND p.DealID = r.DealID             
 AND p.InstNo = r.InstNo AND r.Type = 'F' AND Cast(wDate as date) <= @eDate AND ((r.IsFreeze = 1 AND (r.ReleaseDate is NULL OR r.ReleaseDate > @eDate)) OR (r.IsFreeze = 0 AND r.ReleaseDate > @eDate))),Case When p.DueDate <= @eDate then p.InstAmount ELSE 0
 END)           
        
 END ELSE 0            
END PODAmount,            
Case When p.InstallmentStatus = 'I' THEN             
 CASE WHEN isposted = 1 Then 0 ELSE ISNULL((Select Sum(Amount) from t_TrackReceivables r Where p.OurBranchID = r.OurBranchID AND p.AccountID = r.AccountID AND p.DealID = r.DealID             
 AND p.InstNo = r.InstNo AND r.Type IN ('N','P') AND Cast(wDate as date) <= @eDate AND ((r.IsFreeze = 1 AND (r.ReleaseDate is NULL OR r.ReleaseDate > @eDate)) OR (r.IsFreeze = 0 AND r.ReleaseDate > @eDate))),Case When p.DueDate <= @eDate then p.InstAmount
 ELSE 0 END)    
               
 END ELSE 0            
END MODAmount,            
CASE WHEN isposted = 1 Then 0 ELSE ISNULL((Select Sum(Amount) from t_TrackReceivables r Where p.OurBranchID = r.OurBranchID AND p.AccountID = r.AccountID AND p.DealID = r.DealID             
AND p.InstNo = r.InstNo AND Cast(wDate as date) <= @eDate AND ((r.IsFreeze = 1 AND (r.ReleaseDate is NULL OR r.ReleaseDate > @eDate)) OR (r.IsFreeze = 0 AND r.ReleaseDate > @eDate))),Case When p.DueDate <= @eDate then p.InstAmount ELSE 0 END)             
    
END TotalODAmount,            
(Select isnull(Sum(Amount),0) from (            
Select Max(a.AccrualDate) AccrualDate, Month(a.Accrualdate) Month, Year(a.AccrualDate) Year, a.OurBranchID, a.AccountID, a.DealID, a.AccountName, a.ProductID, a.CurrencyID,              
(Select Sum(Amount) from t_RptBPAccruals Where AccrualDate = Max(a.AccrualDate) AND AccountID = a.AccountID AND DealID = a.DealID) Amount from t_RptBPAccruals a WHERE AccrualDate <= @eDate             
AND a.AccountID = p.AccountID AND a.DealID = p.DealID Group by a.OurBranchID, a.AccountID, a.DealID, a.AccountName, a.ProductID, a.CurrencyID, Month(a.Accrualdate), Year(a.AccrualDate)) as a) Accrued,             
(Select ISNULL(Sum(InstAmount),0) from t_Adv_PaymentSchedule where AccountID = p.AccountID and DealID = p.DealID and InstallmentStatus = 'P' and DueDate <= @eDate ) PDueAmount,            
(Select ISNULL(Sum(InstAmount),0) from t_Adv_PaymentSchedule where AccountID = p.AccountID and DealID = p.DealID and InstallmentStatus = 'I' and DueDate <= @eDate ) MDueAmount            
from t_Adv_PaymentSchedule p             
Inner Join t_Disbursement d On d.AccountID = p.AccountID AND d.DealID = p.DealID              
Inner Join t_Account a On a.AccountID = p.AccountID AND a.OurBranchID = p.OurBranchID            
Inner Join t_Customer c On a.ClientID = c.ClientID AND a.OurBranchID = c.OurBranchID            
Inner Join t_RiskClassification rc ON rc.OurBranchID = d.OurBranchID AND rc.Code = d.RiskCode            
--Where p.DueDate <= @eDate             
Group By p.OurBranchID, c.ClientID,a.ProductID, p.AccountID, a.Name, c.name, p.DealID ,p.InstNo, p.InstallmentStatus, p.InstAmount, d.RiskCode, rc.Description, d.closedate, d.isclosed            
, p.DueDate, p.IsPosted, d.Amount, p.PayDate, d.MaturityDate, a.CurrencyID, d.RiskCode, d.DisbursementDate, c.OurBranchID, d.ProfitRate, d.ProfitRateType, d.PaymentMethod ) a            
Group By OurBranchID, ClientID, Name, AccountID, DealID, AccountName, ProductID, CurrencyID, Limit, DisbursementDate, MaturityDate, Accrued, PDueAmount, MDueAmount, ProfitRate            
, ProfitRateType, PaymentMethod, RiskCode, RiskDesc, closedate, isclosed ) P            
INNER JOIN t_Account ra ON ra.OurBranchID = p.OurBranchID AND p.ClientID = ra.ClientID AND ra.ProductID = (Select ReceivableProductID from t_Products Where ProductID = p.ProductID AND OurBranchID = p.OurBranchID)