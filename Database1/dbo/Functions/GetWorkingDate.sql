﻿-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date, ,>  
-- Description: <Description, ,>  
-- =============================================  
CREATE FUNCTION [dbo].[GetWorkingDate]  
(  
 @ourbranchID as varchar(100)  
)  
RETURNS Datetime 
AS  
BEGIN  
declare @workingdate  as varchar(100)  
 select @workingdate= DATEADD(dd, DATEDIFF(dd, 0, WORKINGDATE ), 0)+ ' ' + convert(varchar(10), GETDATE(), 108)  
  from t_Last where OurBranchID= @ourbranchID  
 -- Return the result of the function  
 RETURN @workingdate  
  
END