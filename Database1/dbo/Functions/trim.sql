﻿create   function [dbo].[trim](@Text nvarchar(max))
returns nvarchar(max)
as  
begin  
    return LTRIM(RTRIM(@Text))
end