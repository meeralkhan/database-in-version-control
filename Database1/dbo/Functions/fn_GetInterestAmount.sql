﻿
CREATE FUNCTION [dbo].[fn_GetInterestAmount] (    
 @Amount money,  
 @Rate money,  
 @Basis varchar(10),  
 @ProductRounding varchar(10)  
)    
Returns money  
AS    
BEGIN    
 DECLARE @Numerator numeric(24,0), @Denominator numeric(24,0), @IntAmount money  
   
 if (@Basis = '0')  
 BEGIN  
   set @IntAmount = 0  
 END  
 ELSE  
 BEGIN  
   
   set @Numerator  = @Amount * @Rate;  
   set @Denominator = cast(@Basis as int) * 100;  
  
   set @IntAmount = @Numerator / @Denominator;  
  
   set @IntAmount = round(@IntAmount, cast(@ProductRounding as int));  
 END  
   
 RETURN LTRIM(@IntAmount)    
END