﻿CREATE   FUNCTION [dbo].[vc_Function_ODTmp] (            
@sdate DATETIME = '1900-01-01',              
@edate DATETIME = '2027-05-21'             
)                            
RETURNS TABLE                            
AS                            
RETURN             
SELECT c.OurBranchID, c.ClientID, c.Name, 'OverDraft' AS "LoanType", ISNULL(mg.GroupID,'-') GroupID, ISNULL(mg.GroupName,'-') GroupName, ISNULL(r.FullName,'-') "RSManager"            
, a.AccountID, CAST(l.FacilityID AS VARCHAR(20)) "DealID", CAST(l.SerialID AS VARCHAR(20)) SerialID, a.Name AccountName, a.ProductID, a.CurrencyID, l.EffectiveDate "SanctionDate"            
, cc.DateExpiry "ExpiryDate", ab.Limit          
--, Abs(ab.limit + ab.ClearBalance) ODAmount          
, Case When a.ProductID = 'ZXACCIN' AND                            
ISNULL((Select Sum(Amount) from t_TrackReceivablesRetail r  WITH (NOLOCK) Where c.OurBranchID = r.OurBranchID AND a.AccountID = r.AccountID                             
AND r.Type = 'F' AND Cast(wDate as Date) <= @eDate AND ((r.IsFreeze = 1 AND (r.ReleaseDate is NULL OR r.ReleaseDate > @eDate)) OR (r.IsFreeze = 0 AND r.ReleaseDate > @eDate))),0)           
> Abs(ab.limit + ab.ClearBalance) THEN           
ISNULL((Select Sum(Amount) from t_TrackReceivablesRetail r  WITH (NOLOCK) Where c.OurBranchID = r.OurBranchID AND a.AccountID = r.AccountID                             
AND r.Type = 'F' AND Cast(wDate as Date) <= @eDate AND ((r.IsFreeze = 1 AND (r.ReleaseDate is NULL OR r.ReleaseDate > @eDate)) OR (r.IsFreeze = 0 AND r.ReleaseDate > @eDate))),0)          
ELSE Abs(ab.limit + ab.ClearBalance) END ODAmount          
, Abs(ab.limit + ab.ClearBalance) OD          
, Case When a.ProductID = 'ZXACCIN' then                           
ISNULL((Select Sum(Amount) from t_TrackReceivablesRetail r  WITH (NOLOCK) Where c.OurBranchID = r.OurBranchID AND a.AccountID = r.AccountID                             
AND r.Type = 'F' AND Cast(wDate as Date) <= @eDate AND ((r.IsFreeze = 1 AND (r.ReleaseDate is NULL OR r.ReleaseDate > @eDate)) OR (r.IsFreeze = 0 AND r.ReleaseDate > @eDate))),0)                                 
ELSE 0 END PD          
, l.EffectiveDate EffectiveDate, ISNULL(ab.NoOfDPD,0) DPD            
, ISNULL((Select MAX(NoOfDPD) from vc_AccountBalanceHistory Where OurBranchID = a.OurBranchID AND AccountID = a.AccountID AND wDate <= @eDate),0) PeakDPD              
, ab.ClearBalance "ClearBalance", ab.ClearBalance "MaxClearBalance", ISNULL(SH.ShareHolderName,'-') SHName, ISNULL(sh.Country,'-') SHCountry            
, DPD.OverDraftClassID, dpd.Classification ClassificationDesc, dpd.SubClassification             
, Case When DPD.OverDraftClassID in ('0004','0005','0006') Then 'Non-Performing' Else 'Performing' END Classification, ISNULL(cc.LimitSecType,'-') LimitSecType                
, (SELECT isnull(SUM(CollateralValue),0) FROM t_Collaterals WHERE OurBranchID = c.OurBranchID AND ClientID = c.ClientID AND CollateralStatus <> 'C') "InvoiceValue"              
, ISNULL(dpd.DPDFreq,0) DPDFreq, ISNULL(dpd.MaxDPD,0) MaxDPD , ISNULL(dpd.TotalDPD,0) TotalDPD  
, Case When DPDFreq = 0 Then TotalDPD Else TotalDPD/DPDFreq END AvgDPD    
FROM t_Customer c               
INNER JOIN t_Account a  WITH (NOLOCK) ON c.ClientID = a.ClientID AND c.OurBranchID = a.OurBranchID              
INNER JOIN vc_AccountBalanceHistory ab  WITH (NOLOCK) ON ab.AccountID = a.AccountID AND ab.wDate = @eDate            
Cross Apply (Select Top 1 * from t_OverDraftLimits  WITH (NOLOCK) Where EffectiveDate <= @eDate AND AccountID = a.AccountID AND OurBranchID = a.OurBranchID Order by EffectiveDate desc) as l                   
LEFT JOIN t_LinkCIFCollateral cc  WITH (NOLOCK) ON cc.OurBranchID = l.OurBranchID AND cc.ClientID = l.ClientID AND cc.SerialNo = l.FacilityID              
LEFT JOIN t_RelationshipOfficer r  WITH (NOLOCK) ON r.OurBranchID = c.OurBranchID AND r.OfficerID = c.RManager                 
LEFT JOIN t_ManageGroups mg  WITH (NOLOCK) ON mg.OurBranchID = c.OurBranchID AND mg.GroupID = c.GroupID            
Outer Apply (SELECT Top 1 ClientID, ShareHolderID ,ParentShareHolderId,ShareHolderName,OwnershipPercentage,Country FROM               
(SELECT cc.ClientID, s.ShareHolderID, s.ParentShareHolderId, s.ShareHolderName, s.OwnershipPercentage ,CASE WHEN s.ShareholderType = 'IND' THEN s.SHCountry ELSE s.CountryofIncorporation END AS Country,               
ROW_NUMBER() OVER (ORDER BY s.OwnershipPercentage DESC, s.ParentShareHolderId) AS RowNum FROM t_CIF_ShareHolders cc  WITH (NOLOCK)               
INNER JOIN t_ShareHolders s  WITH (NOLOCK) ON s.OurBranchID = c.OurBranchID AND s.ShareHolderID = cc.ShareHolderID WHERE cc.ClientID = c.ClientID) AS SH  Order by OwnershipPercentage desc) SH            
Cross Apply (Select a.wDate, a.OurBranchID, a.AccountID, p.CurrencyID, a.ClearBalance, a.LocalClearBalance, Limit, Abs(Limit+ClearBalance) ODAmount, ISNULL(NoOfDPD,0) NoOfDPD            
--, ISNULL((Select Sum(NoOfDPD) from vc_AccountBalanceHistory  WITH (NOLOCK)  Where NoOfDPD = 1 And AccountID = a.AccountID AND (wDate >= @sDate AND wDate <= @eDate)),0) AS DPDFreq            
, ISNULL((Select ISNULL(Sum(DPD),0) DPD from (Select ISNULL(Sum(NoOfDPD),0) DPD from t_AccountBalanceHistory  WITH (NOLOCK) Where (wDate >= @sDate AND wDate <= @eDate) AND NoOfDPD = 1 And AccountID = a.AccountID  
UNION ALL  
(Select top 1 Case When wDate = @sDate and ISNULL(NoOfDPD,0) <> 0 AND ISNULL(NoOfDPD,0) <> 1 then 1 else 0 END DPD from t_AccountBalanceHistory  WITH (NOLOCK) Where AccountID = a.AccountID AND (wDate >= @sDate AND wDate <= @eDate))) as Bal   
),0) DPDFreq  
, ISNULL((Select Max(NoOfDPD) from vc_AccountBalanceHistory  WITH (NOLOCK) Where AccountID = a.AccountID AND (wDate >= @sDate AND wDate <= @eDate)),0) AS MaxDPD      
, ISNULL((Select Count(NoOfDPD) from vc_AccountBalanceHistory  WITH (NOLOCK) Where NoOfDPD <> 0 AND (wDate >= @sDate AND wDate <= @eDate) And AccountID = a.AccountID),0) AS TotalDPD    
, a.OverdraftClassID, Classification, SubClassification             
from vc_AccountBalanceHistory a WITH (NOLOCK) inner join t_Products p  WITH (NOLOCK) on a.OurBranchID = p.OurBranchID and a.ProductID = p.ProductID             
left join t_OverdraftClassifications o  WITH (NOLOCK) ON a.OurBranchID = o.OurBranchID and a.OverdraftClassID = o.OverdraftClassID            
Where ClearBalance < 0 AND wDate = @eDate AND AccountID = ab.AccountID) as DPD