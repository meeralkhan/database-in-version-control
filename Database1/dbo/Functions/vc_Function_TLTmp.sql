﻿CREATE   FUNCTION [dbo].[vc_Function_TLTmp] (            
@sdate DATETIME = '1900-01-01',              
@edate DATETIME = '2027-05-21'             
)                            
RETURNS TABLE                            
AS                            
RETURN                            
Select p.OurBranchID, p.ClientID, p.Name, 'TermLoan' LoanType, isnull(GroupID,'-') GroupID, isnull(GroupName,'-') GroupName, 
isnull(RSManager,'-') RSManager, p.AccountID, p.DealID              
, p.AccountName, p.ProductID, p.CurrencyID, p.DisbursementDate, p.MaturityDate, p.Limit, p.PDueAmount, p.MDueAmount,
p.PODAmount, p.MODAmount, p.TotalODAmount, p.InterestAccrued              
, p.PPaid, p.MPaid, p.PBalance, p.MBalance, p.ProfitRate, p.ProfitRateType, p.PaymentMethod, p.Method,p.RiskCode, 
p.RiskDesc, Case When p.RiskCode in ('004','005','006') Then 'Non-Performing' Else 'Performing' END Classification            
, ra.AccountID MAccountID, GracePeriod, (Select Count(*) from t_Adv_PaymentSchedule Where AccountID = p.AccountID 
AND DealID = p.DealID) TotInstallments            
, (Select Count(*) from t_Adv_PaymentSchedule Where AccountID = p.AccountID AND DealID = p.DealID AND IsPosted = 0) RemInstallments            
, p.CloseDate, ISNULL(p.IsClosed,'0') IsCLosed, p.LimitSecType             
, (SELECT isnull(SUM(CollateralValue),0) FROM t_Collaterals  WITH (NOLOCK) WHERE OurBranchID = p.OurBranchID
AND ClientID = p.ClientID AND CollateralStatus <> 'C') "InvoiceValue"            
, p.ProvAmount, ISNULL(dpd.NoOfDPD,0) DPD, ISNULL(dpd.DPDFreq,0) DPDFreq, ISNULL(dpd.MaxDPD,0) MaxDPD, ISNULL(dpd.TotalDPD,0) TotalDPD,
Case When DPDFreq = 0 Then TotalDPD Else TotalDPD/DPDFreq END AvgDPD     
, p.DisbursedAmount , sh.ShareHolderName SHName, sh.Country SHCountry from (                            
Select OurBranchID, ClientID, Name, AccountID, DealID, GroupID, GroupName, RSManager, AccountName, ProductID, CurrencyID, DisbursementDate, 
MaturityDate, Limit, PDueAmount, MDueAmount, Sum(PODAmount) PODAmount, Sum(MODAmount) MODAmount,                            
Sum(TotalODAmount) TotalODAmount, -Accrued InterestAccrued, Limit - (Limit - (PDueAmount - Sum(PODAmount))) PPaid                             
, Case When ISNULL(CloseDate,'1900-01-01') <= @eDate AND IsClosed = '1' THEN -Accrued ELSE -Accrued - (-Accrued - (MDueAmount - sum(MODAmount))) END MPaid                             
, Limit - (PDueAmount - Sum(PODAmount)) PBalance                            
, Case When ISNULL(CloseDate,'1900-01-01') <= @eDate AND IsClosed = '1' THEN 0 ELSE -Accrued - (MDueAmount - sum(MODAmount)) END MBalance, ProfitRate, ProfitRateType, PaymentMethod, RiskCode, RiskDesc, CloseDate, IsClosed, GracePeriod                
, LimitSecType, ProvAmount, DisbursedAmount, Method  from (                             
Select Distinct p.OurBranchID, c.ClientID, c.Name,  c.GroupID, mg.GroupName, r.FullName RSManager, p.AccountID, p.DealID, a.Name AccountName,  a.ProductID, ISNULL(d.Amount,0) Limit              
, ISNULL(p.InstAmount,0) InstAmount, p.DueDate, d.RiskCode, rc.Description RiskDesc, p.PayDate, p.IsPosted, d.MaturityDate, a.CurrencyID, d.DisbursementDate , d.ProfitRate              
, d.ProfitRateType, d.PaymentMethod, p.InstNo, p.InstallmentStatus, d.closedate, d.isclosed, d.GracePeriod, cc.LimitSecType, d.Amount DisbursedAmount, d.Method             
, ISNULL(d.ProvAmount,0) ProvAmount,                           
Case When p.InstallmentStatus = 'P' THEN                             
 Case WHEN isposted = 1 Then 0 ELSE ISNULL((Select Sum(Amount) from t_TrackReceivables r  WITH (NOLOCK) Where p.OurBranchID = r.OurBranchID AND p.AccountID = r.AccountID AND p.DealID = r.DealID                             
 AND p.InstNo = r.InstNo AND r.Type = 'F' AND Cast(wDate as Date) <= @eDate AND ((r.IsFreeze = 1 AND (r.ReleaseDate is NULL OR r.ReleaseDate > @eDate)) OR (r.IsFreeze = 0 AND r.ReleaseDate > @eDate))),p.InstAmount)                                 
 END ELSE 0                            
END PODAmount,                            
Case When p.InstallmentStatus = 'I' THEN                             
 CASE WHEN isposted = 1 Then 0 ELSE ISNULL((Select Sum(Amount) from t_TrackReceivables r  WITH (NOLOCK) Where p.OurBranchID = r.OurBranchID AND p.AccountID = r.AccountID AND p.DealID = r.DealID                             
 AND p.InstNo = r.InstNo AND r.Type IN ('N','P') AND Cast(wDate as Date) <= @eDate AND ((r.IsFreeze = 1 AND (r.ReleaseDate is NULL OR r.ReleaseDate > @eDate)) OR (r.IsFreeze = 0 AND r.ReleaseDate > @eDate))),p.InstAmount)                                 
 END ELSE 0                            
END MODAmount,                            
CASE WHEN isposted = 1 Then 0 ELSE ISNULL((Select Sum(Amount) from t_TrackReceivables r  WITH (NOLOCK) Where p.OurBranchID = r.OurBranchID AND p.AccountID = r.AccountID AND p.DealID = r.DealID                             
AND p.InstNo = r.InstNo AND Cast(wDate as Date) <= @eDate AND ((r.IsFreeze = 1 AND (r.ReleaseDate is NULL OR r.ReleaseDate > @eDate)) OR (r.IsFreeze = 0 AND r.ReleaseDate > @eDate))),p.InstAmount)                                 
END TotalODAmount,                            
(Select isnull(Sum(Amount),0) from (                            
Select Max(a.AccrualDate) AccrualDate, Month(a.Accrualdate) Month, Year(a.AccrualDate) Year, a.OurBranchID, a.AccountID, a.DealID, a.AccountName, a.ProductID, a.CurrencyID,               
(Select Sum(Amount) from t_RptBPAccruals  WITH (NOLOCK) Where AccrualDate = Max(a.AccrualDate) AND AccountID = a.AccountID AND DealID = a.DealID) Amount from t_RptBPAccruals a  WITH (NOLOCK) WHERE AccrualDate <= @eDate                             
AND a.AccountID = p.AccountID AND a.DealID = p.DealID Group by a.OurBranchID, a.AccountID, a.DealID, a.AccountName, a.ProductID, a.CurrencyID, Month(a.Accrualdate), Year(a.AccrualDate)) as a) Accrued,                             
(Select ISNULL(Sum(InstAmount),0) from t_Adv_PaymentSchedule  WITH (NOLOCK) where AccountID = p.AccountID and DealID = p.DealID and InstallmentStatus = 'P' and DueDate <= @eDate ) PDueAmount,                            
(Select ISNULL(Sum(InstAmount),0) from t_Adv_PaymentSchedule  WITH (NOLOCK) where AccountID = p.AccountID and DealID = p.DealID and InstallmentStatus = 'I' and DueDate <= @eDate ) MDueAmount                            
from t_Adv_PaymentSchedule p  WITH (NOLOCK)                             
Inner Join t_Disbursement d  WITH (NOLOCK) On d.AccountID = p.AccountID AND d.DealID = p.DealID                              
Inner Join t_Account a  WITH (NOLOCK) On a.AccountID = p.AccountID AND a.OurBranchID = p.OurBranchID                            
Inner Join t_Customer c  WITH (NOLOCK) On a.ClientID = c.ClientID AND a.OurBranchID = c.OurBranchID                            
Inner Join t_RiskClassification rc  WITH (NOLOCK) ON rc.OurBranchID = d.OurBranchID AND rc.Code = d.RiskCode                       
Inner Join t_FacilityProduct l  WITH (NOLOCK) ON l.OurBranchID = d.OurBranchID AND c.ClientID = l.ClientID  AND a.ProductID = l.ProductID                      
Inner Join t_LinkCIFCollateral cc  WITH (NOLOCK) ON cc.OurBranchID = l.OurBranchID AND cc.ClientID = l.ClientID AND cc.SerialNo = l.FacilityID --AND cc.ReferenceNo = d.FacilityID                       
Left Join t_RelationshipOfficer r  WITH (NOLOCK) ON r.OurBranchID = c.OurBranchID AND r.OfficerID = c.RManager                      
Left Join t_ManageGroups mg  WITH (NOLOCK) ON mg.OurBranchID = c.OurBranchID AND mg.GroupID = c.GroupID                      
Where p.DueDate <= @eDate                             
Group By mg.GroupName, r.FullName, d.GracePeriod, c.GroupID, p.OurBranchID, c.ClientID,a.ProductID, p.AccountID, a.Name, c.name, p.DealID ,p.InstNo, p.InstallmentStatus, p.InstAmount            
, d.RiskCode, rc.Description, d.closedate, d.isclosed, p.DueDate, p.IsPosted, d.Amount, p.PayDate, d.MaturityDate, a.CurrencyID, d.RiskCode, d.DisbursementDate, c.OurBranchID            
, d.ProfitRate, d.ProfitRateType, d.PaymentMethod, cc.LimitSecType, d.ProvAmount, d.Amount, d.Method) a                            
Group By GroupID, GroupName, RSManager, GracePeriod ,OurBranchID, ClientID, Name, AccountID, DealID, AccountName, ProductID, CurrencyID, Limit, DisbursementDate, MaturityDate            
, Accrued, PDueAmount, MDueAmount, ProfitRate, ProfitRateType, PaymentMethod, RiskCode, RiskDesc, closedate, isclosed, LimitSecType, ProvAmount, DisbursedAmount, Method ) P                 
INNER JOIN t_Account ra  WITH (NOLOCK) ON ra.OurBranchID = p.OurBranchID AND p.ClientID = ra.ClientID AND ra.ProductID = (Select ReceivableProductID from t_Products  WITH (NOLOCK)             
Where ProductID = p.ProductID AND OurBranchID = p.OurBranchID)                     
INNER JOIN (Select wDate, AccountID, DealID, ISNULL(NoOfDPD,0) NoOfDPD            
--, ISNULL((Select Sum(NoOfDPD) from t_DealBalanceHistory  WITH (NOLOCK) Where (wDate >= @sDate AND wDate <= @eDate) AND NoOfDPD = 1 And AccountID = d.AccountID and DealID = d.DealID),0) AS DPDFreq             
, ISNULL((Select ISNULL(Sum(DPD),0) DPD from (Select ISNULL(Sum(NoOfDPD),0) DPD from t_DealBalanceHistory  WITH (NOLOCK)     
Where (wDate >= @sDate AND wDate <= @eDate) AND NoOfDPD = 1 And AccountID = d.AccountID and DealID = d.DealID    
UNION ALL    
(Select top 1 Case When wDate = @sDate and ISNULL(NoOfDPD,0) <> 0 AND ISNULL(NoOfDPD,0) <> 1 then 1 else 0 END DPD from t_DealBalanceHistory  WITH (NOLOCK)     
Where AccountID = d.AccountID and DealID = d.DealID AND (wDate >= @sDate AND wDate <= @eDate))) as Bal     
),0) DPDFreq    
, ISNULL((Select Max(NoOfDPD) from t_DealBalanceHistory  WITH (NOLOCK) Where (wDate >= @sDate AND wDate <= @eDate) And AccountID = d.AccountID and DealID = d.DealID),0) AS MaxDPD       
, ISNULL((Select Count(NoOfDPD) from t_DealBalanceHistory  WITH (NOLOCK) Where NoOfDPD <> 0 AND (wDate >= @sDate AND wDate <= @eDate) And AccountID = d.AccountID and DealID = d.DealID),0) AS TotalDPD       
from t_DealBalanceHistory d WITH (NOLOCK) ) as DPD ON dpd.wDate = @eDate AND p.AccountID = dpd.AccountID AND p.DealID = dpd.DealID             
Outer Apply (SELECT Top 1 ClientID, ShareHolderID ,ParentShareHolderId,ShareHolderName,OwnershipPercentage,Country FROM               
(SELECT ccc.ClientID, s.ShareHolderID, s.ParentShareHolderId, s.ShareHolderName, s.OwnershipPercentage ,CASE WHEN s.ShareholderType = 'IND' THEN s.SHCountry ELSE s.CountryofIncorporation END AS Country,               
ROW_NUMBER() OVER (ORDER BY s.OwnershipPercentage DESC, s.ParentShareHolderId) AS RowNum FROM t_CIF_ShareHolders ccc  WITH (NOLOCK)               
INNER JOIN t_ShareHolders s  WITH (NOLOCK) ON s.OurBranchID = p.OurBranchID AND s.ShareHolderID = ccc.ShareHolderID WHERE ccc.ClientID = p.ClientID) AS SH  Order by OwnershipPercentage desc) SH