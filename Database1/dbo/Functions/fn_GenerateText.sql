﻿Create   Function [dbo].[fn_GenerateText] (@Length integer)
Returns varchar(256)
as 
Begin

	Declare 
		@RandomValue             varchar(256), 
		@Count                   integer,
		@RandomNumber            float, 
		@RandomNumberInteger     integer, 
		@CurrentCharacter        char(1),
		@ValidCharactersLength   integer,
		@ValidCharacters         varchar(255) 

	Set @RandomValue = '';

	Set @ValidCharacters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'; 

	Set @ValidCharactersLength = Len(@ValidCharacters); 
	Set @CurrentCharacter      = ''; 
	Set @RandomNumber          = 0; 
	Set @RandomNumberInteger   = 0; 
	Set @Count = 1; 

	While @Count <= @Length 
	Begin 
		Set @RandomNumber = (Select RandomNumber from vc_Random); 

		Set @RandomNumberInteger = Convert(integer, ((@ValidCharactersLength - 1) * @RandomNumber + 1)); 
 
		Set @CurrentCharacter = SubString(@ValidCharacters, @RandomNumberInteger, 1); 

		Set @RandomValue = @RandomValue + @CurrentCharacter; 

		Set @Count = @Count + 1; 
	End 

	Set @RandomValue = Upper(@RandomValue);

	Return @RandomValue 

End