﻿create view [dbo].[v_GetUnSupervisedTransferTrx]
AS
SELECT distinct 'Ok' AS ReturnStatus, OurBranchID, ScrollNo FROM t_TransferTransactionModel WHERE OurBranchID IN (select OurBranchID from t_Last) AND Supervision NOT IN ('C','R')