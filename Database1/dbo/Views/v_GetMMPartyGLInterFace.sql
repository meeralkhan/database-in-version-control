﻿CREATE VIEW [dbo].[v_GetMMPartyGLInterFace]  
AS  

SELECT a.IsGLAccount, a.OurBranchID, a.PartyID, a.CurrencyID, a.DueToBankAccount, a.DueFromBankAccount, a.InterestReceivedAccount, a.InterestReceivableAccount, 
a.InterestPaidAccount, a.InterestPayableAccount, a.ContigentAssetsAccount, a.ContigentLiabilityAccount, a.BorrowContigentAssetsAccount, 
a.BorrowContigentLiabilityAccount, a.IsContigentPass, a.BAccrualAcc, a.CAccrualAcc,
t_GL.Description AS DueToBankName, t_GL.CurrencyID AS DueToCurrency, 
t_GL1.Description AS DueFromBankName, t_GL1.CurrencyID AS DueFromCurrency, 
t_GL2.Description AS InterestReceivedName, t_GL2.CurrencyID AS InterestReceivedCurrency, 
t_GL3.Description AS InterestReceivableName, t_GL3.CurrencyID AS InterestReceivableCurrency, 
t_GL4.Description AS InterestPaidName, t_GL4.CurrencyID AS InterestPaidCurrency, 
t_GL5.Description AS InterestPayableName, t_GL5.CurrencyID AS InterestPayableCurrency,   
t_GL6.Description AS ContigentAssetsName, t_GL6.CurrencyID AS ContigentAssetsCurrency,   
t_GL7.Description AS ContigentLiabilityName, t_GL7.CurrencyID AS ContigentLiabilityCurrency,   
t_GL8.Description AS BorrowContigentAssetsName, t_GL8.CurrencyID AS BorrowContigentCurrency,     
t_GL9.Description AS BorrowContigentLiabilityName, t_GL9.CurrencyID AS BorrowLiabilityCurrency     

FROM t_MMPartyGLInterface a

INNER JOIN t_GL ON a.OurBranchID = t_GL.OurBranchID AND a.DueToBankAccount = t_GL.AccountID
INNER JOIN t_GL t_GL1 ON a.OurBranchID = t_GL1.OurBranchID AND a.DueFromBankAccount = t_GL1.AccountID
INNER JOIN t_GL t_GL2 ON a.OurBranchID = t_GL2.OurBranchID AND a.InterestReceivedAccount = t_GL2.AccountID
INNER JOIN t_GL t_GL3 ON a.OurBranchID = t_GL3.OurBranchID AND a.InterestReceivableAccount = t_GL3.AccountID  
INNER JOIN t_GL t_GL4 ON a.OurBranchID = t_GL4.OurBranchID AND a.InterestPaidAccount = t_GL4.AccountID
INNER JOIN t_GL t_GL5 ON a.OurBranchID = t_GL5.OurBranchID AND a.InterestPayableAccount = t_GL5.AccountID  
LEFT JOIN  t_GL t_GL6 ON a.OurBranchID = t_GL6.OurBranchID AND a.ContigentAssetsAccount = t_GL6.AccountID  
LEFT JOIN  t_GL t_GL7 ON a.OurBranchID = t_GL7.OurBranchID AND a.ContigentLiabilityAccount = t_GL7.AccountID  
LEFT JOIN  t_GL t_GL8 ON a.OurBranchID = t_GL8.OurBranchID AND a.BorrowContigentAssetsAccount = t_GL8.AccountID  
LEFT JOIN  t_GL t_GL9 ON a.OurBranchID = t_GL9.OurBranchID AND a.BorrowContigentLiabilityAccount = t_GL9.AccountID