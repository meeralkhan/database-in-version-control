﻿CREATE VIEW [dbo].[vc_GetGLParameters]  
AS   
SELECT t_GLParameters.OurBranchID, t_GLParameters.SerialID,  
    t_GLParameters.AccountID, t_GL.Description,         
    t_GLParameters.Description AS GLDescription        
FROM t_GLParameters LEFT OUTER JOIN        
    t_GL ON         
    t_GLParameters.OurBranchID = t_GL.OurBranchID AND         
    t_GLParameters.AccountID = t_GL.AccountID