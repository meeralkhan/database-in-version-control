﻿CREATE   view [dbo].[v_CCPDrawdownRecon]
AS
select a.OurBranchID, a.ClientID, d.AccountID, c.LocalEq AS Amount, c.Margin, c.DrawingPower AS AfterDeductingMargin, c.ReferenceNo AS CollateralID,
convert(varchar(10),c.CreateTime,120) AS DrawdownDate, convert(varchar(10),c.cExpiryDate,120) AS MaturityDate, e.ProductID,
ISNULL(d.RateID1,'001') AS RateID1, ISNULL(d.Margin1,0) AS Margin1, ISNULL(d.MinRate1,0) AS MinRate1, ISNULL(d.Rate1,0) AS Rate1,
ISNULL(d.RateID2,'') AS RateID2, ISNULL(d.Margin2,0) AS Margin2, ISNULL(d.MinRate2,0) AS MinRate2, ISNULL(d.Rate2,0) AS Rate2,
ISNULL(d.RateID3,'') AS RateID3, ISNULL(d.Margin3,0) AS Margin3, ISNULL(d.MinRate3,0) AS MinRate3, ISNULL(d.Rate3,0) AS Rate3,
ISNULL(d.ExcessRateID,'') AS ExcessRateID, ISNULL(d.ExcessMargin,0) AS ExcessMargin, ISNULL(d.ExcessMinRate,0) AS ExcessMinRate, ISNULL(d.ExcessRate,0) AS ExcessRate,
convert(varchar(10),d.EffectiveDate,120) AS EffectiveDate
from t_LinkCIFCollateral a
inner join t_FacilityCollateral b on a.OurBranchID = b.OurBranchID and a.ClientID = b.ClientID and a.SerialNo = b.FacilityID
inner join t_Collaterals c on a.OurBranchID = c.OurBranchID and a.ClientID = c.ClientID and b.CollateralID = c.CollateralID
inner join t_LinkFacilityAccount d on a.OurBranchID = d.OurBranchID and a.ClientID = d.ClientID and a.SerialNo = d.FacilityID
inner join t_FacilityProduct e on a.OurBranchID = e.OurBranchID and a.ClientID = e.ClientID and a.SerialNo = e.FacilityID
where a.LimitSecType = 'SCF File' and a.GroupID like 'JURIS/%'