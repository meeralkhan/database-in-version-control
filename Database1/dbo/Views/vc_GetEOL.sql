﻿create VIEW [dbo].[vc_GetEOL]  
AS        
      
SELECT t_Adv_Account_LimitMaintain.OurBranchID, t_Account.ClientID, t_Customer.Name AS ClientName,         
    t_Adv_Account_LimitMaintain.AccountID, t_Account.Name AS AccountName,         
    t_Adv_Account_LimitMaintain.SanctionDate, t_Adv_Account_LimitMaintain.AccountLimit as LimitAmount,         
    t_Adv_Account_LimitMaintain.DrawingPower, t_Adv_Account_LimitMaintain.ExpDate,         
    t_Rate001.ChangeDate, t_Rate001.Limit1, t_Rate001.Rate1, t_Rate001.Limit2, t_Rate001.Rate2,       
    t_Rate001.Limit3, t_Rate001.Rate3, t_Adv_Account_Security.ValueOfSecurity,         
    t_Adv_Account_Security.Margin, t_Securities.[Description] AS SecurityDescription,      
    t_SecurityTypes.[Description] AS SecurityType, t_Adv_Account_LimitMaintain.IsBlocked,         
    t_Adv_Account_LimitMaintain.IsCancelled, t_Products.CurrencyID, t_AccountBalance.ClearBalance,       
    t_AccountBalance.Limit        
 FROM t_AccountBalance RIGHT OUTER JOIN        
    t_Account ON         
    t_AccountBalance.OurBranchID = t_Account.OurBranchID AND         
    t_AccountBalance.AccountID = t_Account.AccountID   
    LEFT OUTER JOIN t_Products ON         
    t_Account.OurBranchID = t_Products.OurBranchID AND         
    t_Account.ProductID = t_Products.ProductID   
    LEFT OUTER JOIN t_Customer ON         
    t_Account.OurBranchID = t_Customer.OurBranchID AND  
    t_Account.ClientID = t_Customer.ClientID   
    RIGHT OUTER JOIN t_Adv_Account_LimitMaintain ON         
    t_Account.OurBranchID = t_Adv_Account_LimitMaintain.OurBranchID AND         
    t_Account.AccountID = t_Adv_Account_LimitMaintain.AccountID   
    LEFT OUTER JOIN t_Securities   
                    RIGHT OUTER JOIN t_Adv_Account_Security ON  
                    t_Securities.OurBranchID = t_Adv_Account_Security.OurBranchID AND         
                    t_Securities.SecurityID = t_Adv_Account_Security.SecurityID ON         
    t_Adv_Account_LimitMaintain.OurBranchID = t_Adv_Account_Security.OurBranchID AND        
    t_Adv_Account_LimitMaintain.AccountID = t_Adv_Account_Security.AccountID AND         
    t_Adv_Account_Security.ValueOfSecurity =        
      (  
       SELECT MAX(valueofsecurity)        
       FROM t_Adv_Account_Security        
       WHERE t_Adv_Account_Security.OurBranchID = t_Adv_Account_LimitMaintain.OurBranchID   
       AND t_Adv_Account_Security.accountid = t_Adv_Account_LimitMaintain.AccountID  
      )  
    LEFT OUTER JOIN t_Rate001 ON         
    t_Adv_Account_LimitMaintain.OurBranchID = t_Rate001.OurBranchID AND         
    t_Adv_Account_LimitMaintain.AccountID = t_Rate001.AccountID AND         
    t_Rate001.ChangeDate =        
        ( SELECT MAX(changedate)        
          FROM t_rate001        
          WHERE t_rate001.OurBranchID = t_Adv_Account_LimitMaintain.OurBranchID AND   
          t_rate001.accountid = t_Adv_Account_LimitMaintain.AccountID  
        )        
     INNER JOIN t_SecurityTypes on t_Securities.OurBranchID = t_SecurityTypes.OurBranchID   
     AND t_Securities.SecurityTypeID = t_SecurityTypes.SecurityTypeId      
     WHERE (t_AccountBalance.Limit <> 0) AND (t_AccountBalance.ClearBalance < 0)