﻿CREATE   view [dbo].[v_AdvancesAccruals]
as
select * from t_RptBPAccruals
where AccrualDate IN (select MAX(AccrualDate) from t_RptBPAccruals)
and Balance != 0