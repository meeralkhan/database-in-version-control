﻿create view [dbo].[v_InquireODPaymentDealRefNum]
as
select a.ClientID,b.DealID,b.OurBranchID,b.AccountID,b.ReferenceNo,a.ProductID,b.FacilityID,d.AccountLimitID,isnull(b.Amount,0) Amount,
sum(isnull(case when c.IsPosted = 1 then c.PrincipalAmount else 0 end,0)) totalPaidAmt,
sum(isnull(case when c.IsPosted = 0 and cast(c.DueDate as Date) < e.WORKINGDATE  then c.PrincipalAmount else 0 end,0)) totalDuePrinciple,
sum(isnull(case when c.IsPosted = 0 and cast(c.DueDate as Date) < e.WORKINGDATE  then c.ToDateProfit else 0 end,0)) totalDueInterest,
0 totalDuePenalty, e.WORKINGDATE,c.IsPosted,
sum(isnull(case when c.IsPosted = 0 and cast(c.DueDate as Date) < e.WORKINGDATE  then c.InstAmount else 0 end,0)) totalDueAmount,
c.InstNo,c.PrincipalAmount,c.InstAmount,c.DueDate
from t_Account a
inner join t_Disbursement b on a.AccountID = b.AccountID and a.OurBranchID = b.OurBranchID
inner join t_Last e on e.OurBranchID= b.OurBranchID
inner join t_Adv_PaymentSchedule c on b.AccountID = c.AccountID and b.OurBranchID = c.OurBranchID and b.DealID = c.DealID
inner join t_LinkFacilityAccount d on b.AccountID = d.AccountID and b.OurBranchID = d.OurBranchID
where c.DueDate < e.WORKINGDATE
group by a.ClientID,b.DealID,b.OurBranchID,b.AccountID,b.ReferenceNo,a.ProductID,a.ProductID,b.FacilityID,b.Amount,d.AccountLimitID,
c.InstNo,c.PrincipalAmount,c.InstAmount,c.DueDate,e.WORKINGDATE,c.IsPosted