﻿CREATE   view [dbo].[v_TDBalanceConfirmation]
AS
select a.AccountID,a.OurBranchID,b.ClientID,p.ProductID,p.Description,p.CurrencyID,sum(a.Amount) Balance from t_TDRIssuance a
inner join t_Account b on a.OurBranchID = b.OurBranchID and a.AccountID = b.AccountID and isnull(b.Status,'') <> 'C' and isnull(IsClosed,0) <> 1
inner join t_Products p on b.ProductID = p.ProductID and b.OurBranchID = p.OurBranchID
group by a.AccountID,a.OurBranchID,b.ClientID,p.ProductID,p.Description,p.CurrencyID