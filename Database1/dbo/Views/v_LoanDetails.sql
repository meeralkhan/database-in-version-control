﻿   
CREATE   view [dbo].[v_LoanDetails]  
as  
select a.OurBranchID,b.ProductID,b.ClientID,a.FacilityID,a.ReferenceNo DealID,a.Amount,b.CurrencyID,a.DisbursementDate,a.MaturityDate,a.ProfitRateType InterestRateType,a.ProfitRate InterestRate, 
m.Description as BaseRate, a.Margin,a.TrxAccID FundingAccount,a.AccountID DisbursmentAccount,count(c.InstAmount) TotalInstallment,sum(case c.IsPosted  when 1 then 1 else 0 end) PaidInstallment,  
sum(case when c.IsPosted <> 1 then 1 else 0 end) RemainingInstallment,a.PaymentMethod,0 PastDueAmount, 0 InterestRepaymentAmount, sum(case when c.IsPosted <> 1 and c.InstallmentStatus = 'P' then c.InstAmount else 0 end) OutStandingAmount,  
isnull(d.DueDate,NULL) CurrInstallmentPaymentDate, max(a.ProfitRate) CurrentInstallmentRate, d.InstAmount CurrInstallmentAmount  
from t_Disbursement a  
inner join t_Account b on a.AccountID = b.AccountID and a.OurBranchID = b.OurBranchID  
inner join t_Adv_PaymentSchedule c on c.DealID = a.DealID and a.AccountID = c.AccountID and a.OurBranchID = c.OurBranchID  
left join t_MurabahahBaseRate m on a.OurBranchID = m.OurBranchID and a.BaseRate = m.RateID  
left join t_Adv_PaymentSchedule d on c.DealID = d.DealID and c.AccountID = d.AccountID and c.OurBranchID = d.OurBranchID and d.InstNo  
in (select min(e.InstNo) from t_Adv_PaymentSchedule e where a.DealID = e.DealID and a.AccountID = e.AccountID and a.OurBranchID = e.OurBranchID and isnull(e.IsPosted,0) = 0)  
   
group by a.OurBranchID,b.ProductID,b.ClientID,a.FacilityID,a.DealID,a.ReferenceNo,a.Amount,b.CurrencyID,a.DisbursementDate,a.MaturityDate,a.ProfitRateType,a.ProfitRate,m.Description,a.Margin,     
a.TrxAccID,a.AccountID,d.DueDate,a.PaymentMethod,d.InstAmount