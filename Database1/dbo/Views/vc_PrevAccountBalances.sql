﻿CREATE VIEW [dbo].[vc_PrevAccountBalances] as  
SELECT OurBranchID, AccountID, ProductID, Name, ClearBalance, IsFreezed, FreezeAmount, CurrencyID,
"LocalClearBalance" = CASE WHEN CurrencyID = 'AED' THEN ClearBalance ELSE LocalClearBalance END,
"Status" = CASE
 WHEN Status = 'C' THEN 'Closed'
 WHEN Status = 'I' THEN 'Inactive'
 WHEN Status = 'T' THEN 'Dormant'
 WHEN Status = 'X' THEN 'Deceased'
 WHEN Status = 'D' THEN 'Blocked'
 ELSE
    'Active'
 End
FROM dbo.t_PrevAccountBalances