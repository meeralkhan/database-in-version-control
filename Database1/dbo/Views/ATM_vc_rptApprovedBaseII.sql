﻿--APPROVED BASEII
CREATE   VIEW [dbo].[ATM_vc_rptApprovedBaseII]
AS
 --Query = string.Format(@"
 --SELECT * FROM ATM_vc_rptApprovedBaseII         
 --WHERE OurBranchID = '02' AND
 --convert(varchar(10), ClearingDate, 120) >= '2021-08-04' AND convert(varchar(10), ClearingDate, 120) <= '2021-08-08'
 --order by ClearingDate asc
SELECT a.OurBranchID, a.AccountID, Left(a.RefNo, 16) as PAN, a.STAN, a.RetRefNum, ISNULL(b.VISATrxID,a.VISATrxID) as VISATrxID, ISNULL(b.RefNo,a.RefNo) as RefNo, 
ISNULL(b.ValueDate, a.wDate) as TrxDate, a.ReleaseDate as ClearingDate, 
ISNULL(b.Amount,a.Amount) as Amount, ISNULL(b.ConvRate,a.ConvRate) as ConvRate, ISNULL(b.MerchantType,a.MerchantType) as MerchantType, 
ISNULL(b.AcqCountryCode,a.AcqCountryCode) as AcqCountryCode, ISNULL(b.ForwardInstID,a.ForwardInstID) as ForwardInstID, 
ISNULL(b.CurrCodeTran,a.CurrCodeTran) as CurrCodeTran, ISNULL(b.CurrCodeSett,a.CurrCodeSett) as CurrCodeSett, a.SettlmntAmount, 
Case 
	when a.HoldStatus = 'R' then 'Full Reversal'
	when a.HoldStatus = 'H' then 'Hold'
	when a.HoldStatus = 'C' then 'Cleared'
	when a.HoldStatus = 'P' then 'Partial Reversal'
end as TrxStatus,
Case 
	when LEFT(a.ProcCode,2) = '00' AND LEFT(a.POSEntryMode,2) = '01' then 'ECOM Trx'
	when LEFT(a.ProcCode,2) = '00' then 'POS Trx'
	when LEFT(a.ProcCode,2) = '01' then 'Cash Withdrawal Trx'
	when LEFT(a.ProcCode,2) = '31' then 'Balance Enquiry Trx'
	when LEFT(a.ProcCode,2) = '23' then 'Credit Trx'
	when LEFT(a.ProcCode,2) = '26' then 'Online Credit Trx'
	when LEFT(a.ProcCode,2) = '20' then 'Credit Vouch/ Merch Return Trx'
	when a.ProcCode = 'CANCL CHG' then 'Card Replacement [CANCELLED]'
	when a.ProcCode = 'LT CHG' then 'Card Replacement [LOST]'
	when a.ProcCode = 'STLN CHG' then 'Card Replacement [STOLEN]'
	when a.ProcCode = 'DMG CHG' then 'Card Replacement [DAMAGED]'
	when a.ProcCode = 'LT DELVRY CHG' then 'Card Replacement [LOST IN DELIVERY]'
	when a.ProcCode = 'REP CHG' then 'Card Replacement [NORMAL]'
end as TranDetails, ISNULL(b.DescriptionID,a.DescriptionID) as DescriptionID, ISNULL(b.[Description],a.[Description]) as [Description], ISNULL(b.CardAccptNameLoc,a.CardAccptNameLoc) as CardAccptNameLoc
FROM t_ATM_HoldTrxs a
LEFT JOIN t_Transactions b ON a.OurBranchID=b.OurBranchID AND a.AccountID = b.AccountID And a.RetRefNum=b.RetRefNum and a.STAN=b.STAN
WHERE  ISNULL(a.HoldStatus,'') = 'C'