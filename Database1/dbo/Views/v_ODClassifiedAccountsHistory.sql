﻿create view [dbo].[v_ODClassifiedAccountsHistory]
AS
select a.*, o.Classification, o.SubClassification
from ODClassifiedAccounts a
inner join t_OverdraftClassifications o on a.OurBranchID = o.OurBranchID and a.OverdraftClassID = o.OverdraftClassID