﻿CREATE view [dbo].[vc_rptgetstatementofcon]        
as          
          
 SELECT G.ourbranchid, G.accountid, G.Description, GT.valuedate, OpeningBalance=isnull(G.openingbalance,0)    
   /*isnull(case when iscredit=1 then          
      (amount)          
      end,0)          
   -          
    isnull(case  when iscredit=0 then          
      (amount)          
      end,0)*/          
 ,          
 CrAmount =isnull(case  when iscredit=1 then          
 (amount)    
 end,0),          
 DrAmount =isnull(case  when iscredit=0 then    
 (amount)          
 end,0)    
     
 FROM t_GLTransactions GT    
 right outer join t_Gl G on G.OurBranchid =GT.OurBranchid and G.Accountid =GT.Accountid    
 WHERE           
 --(GT.iscredit=1 or GT.iscredit=0) and    
 ISNULL(GT.Status, '') <> 'R' AND G.Balance <> 0