﻿create view [dbo].[v_InquireDealRefNumODPayCIF]
as
select a.ClientID,b.DealID,b.OurBranchID,b.AccountID,b.ReferenceNo,a.ProductID,b.FacilityID,b.Amount,d.AccountLimitID,
sum(isnull(case when c.IsPosted = 1 then c.PrincipalAmount else 0 end,0)) totalPaidAmt,
sum(isnull(case when c.IsPosted = 0 and cast(c.DueDate as Date) < e.WORKINGDATE  then c.PrincipalAmount else 0 end,0)) totalDuePrinciple,
sum(isnull(case when c.IsPosted = 0 and cast(c.DueDate as Date) < e.WORKINGDATE  then c.ToDateProfit else 0 end,0)) totalDueInterest,
isnull(case when b.esFeeType = 'Amount' then b.minEsFee when  b.esFeeType = 'Percentage' then (isnull(b.esFeePercentage,0)/100) * b.Amount end,0) totalDuePenalty, 
sum(isnull(case when c.IsPosted = 0 and cast(c.DueDate as Date) < e.WORKINGDATE  then c.InstAmount else 0 end,0)) totalDueAmount
from t_Account a
inner join t_Disbursement b on a.AccountID = b.AccountID and a.OurBranchID = b.OurBranchID
inner join t_Last e on e.OurBranchID= b.OurBranchID
inner join t_Adv_PaymentSchedule c on b.AccountID = c.AccountID and b.OurBranchID = c.OurBranchID and b.DealID = c.DealID
inner join t_LinkFacilityAccount d on b.AccountID = d.AccountID and b.OurBranchID = d.OurBranchID
group by a.ClientID,b.DealID,b.OurBranchID,b.AccountID,b.ReferenceNo,a.ProductID,a.ProductID,b.FacilityID,b.Amount,d.AccountLimitID,b.esFeeType,b.minEsFee,b.esFeePercentage