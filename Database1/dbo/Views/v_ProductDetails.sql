﻿create view [dbo].[v_ProductDetails]    
as    
select d.OurBranchID,a.ClientID,d.FacilityID FacilityID,P.FinanceProductType ProductType, f.Description ProductTypeName,p.CurrencyID,min(d.AdvancesDate) StartDate,     
max(d.MaturityDate) MaturityDate, sum(isnull(c.Limit,0)) ApprovedLimit,sum(isnull(d.Amount,0)) UtilizedLimit, sum(isnull(c.Limit,0)) - sum(isnull(d.Amount,0)) AvailableLimit    
from t_Disbursement d    
INNER JOIN t_Account a on a.OurBranchID = d.OurBranchID and a.AccountID = d.AccountID      
INNER JOIN t_Products P on d.OurBranchID = p.OurBranchID and a.ProductID = p.ProductID      
inner join t_FinanceProductTypes f on f.OurBranchID = a.OurBranchID and f.FinanceProductType = p.FinanceProductType    
left join t_LinkCIFCollateral c on c.OurBranchID = a.OurBranchID and c.ReferenceNo = d.FacilityID    
group by d.OurBranchID,a.ClientID,d.FacilityID,P.FinanceProductType, f.Description,p.CurrencyID