﻿  
CREATE view [dbo].[v_VATDrilldown]  
AS  
select a.OurBranchID, a.ScrollNo, a.SerialNo, a.ChannelId, a.ChannelRefID, d.ClientID, d.Name AS ClientName, c.AccountID, c.Name, c.ProductID, c.CurrencyID,  
a.wDate, a.ValueDate, a.TrxTimeStamp, a.ForeignAmount, a.Amount, a.ExchangeRate, a.DescriptionID, a.AppWHTax, a.WHTaxAmount,  
(select top 1 AccountID from t_GLTransactions g where g.OurBranchID = a.OurBranchID and g.ValueDate = a.ValueDate and g.VoucherID = a.ScrollNo and SUBSTRING(g.AccountID,1,1) = '3') FeesGLAccountID  
from t_Transactions a  
inner join t_TransactionDescriptions b on a.OurBranchID = b.OurBranchID and a.DescriptionID = b.DescriptionID and ISNULL(IsVATApplicable,0) = 1 and ISNULL(AppWHTax,0) = 1  
inner join t_Account c on a.OurBranchID = c.OurBranchID and a.AccountID = c.AccountID  
inner join t_Customer d on c.OurBranchID = d.OurBranchID and c.ClientID = d.ClientID