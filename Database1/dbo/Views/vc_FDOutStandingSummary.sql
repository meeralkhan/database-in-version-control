﻿CREATE VIEW [dbo].[vc_FDOutStandingSummary]
AS    
  
 SELECT t.OurBranchID, t.ProductID, p.Description, SUM(t.Amount) AS TotalAmount, COUNT(*) AS NoOfReciepts,   
 CAST(p.PeriodsInFigure as nvarchar(10)) + '-' +     
  Case p.Period     
   when 'Y' then 'Year'    
   when 'M' then 'Month'    
   when 'D' then 'Day'    
  end     
 as Period,    
 d.Description as Deposits  
 FROM t_TDRIssuance t   
 INNER JOIN t_Products p ON t.OurBranchID = p.OurBranchID AND t.ProductID = p.ProductID   
 INNER JOIN t_Account a ON t.OurBranchID = a.OurBranchID AND t.AccountID = a.AccountID     
 INNER JOIN t_DepositEconSect d ON t.OurBranchID = d.OurBranchID AND a.Code = d.Code  
 WHERE ISNULL(t.Isclosed,0) = 0   
 GROUP BY t.OurBranchID, t.ProductID, p.Description, p.Period, p.PeriodsInFigure, d.Description