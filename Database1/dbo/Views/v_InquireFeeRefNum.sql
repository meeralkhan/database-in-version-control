﻿CREATE   view [dbo].[v_InquireFeeRefNum]    
as    
select a.OurBranchID,d.ClientID,a.FacilityID,d.ProductID, db.Charges feeAmount, db.feeReverseAmount feeReverseAmount, db.Charges-db.feeReverseAmount feeAvailReverseAmount,    
a.feesAccountID feeChargesAccNo, '' feeStatus, dc.ChargeID as FeeType, a.ReferenceNo refNo ,db.TrxRefNo  
from t_Disbursement a    
inner join t_Account d on a.OurBranchID = d.OurBranchID and a.AccountID = d.AccountID    
inner join t_Disb_Account_Charges db on a.OurBranchID = db.OurBranchID and a.AccountID = db.AccountID and a.DealID=db.DealID    
inner join t_DisbursementCharges dc on a.OurBranchID = dc.OurBranchID and db.ChargeID = dc.ChargeID