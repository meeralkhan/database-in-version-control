﻿create view [dbo].[v_FacilityCollateral]
AS
select fc.*, c.ReferenceNo from t_FacilityCollateral fc
inner join t_Collaterals c ON fc.OurBranchID = c.OurBranchID and fc.ClientID = c.ClientID and fc.CollateralID = c.CollateralID