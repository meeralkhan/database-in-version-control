﻿CREATE VIEW [dbo].[vc_BRF_AccountBalanceHistory_Loan] as SELECT distinct ab.wDate, ab.OurBranchID, c.ClientID, c.Name, ab.AccountID, ab.ProductID, p.ProductType, ab.AccountName, a.CurrencyID,			
ClearBalance = CASE WHEN a.CurrencyID = 'AED' THEN ab.ClearBalance ELSE ab.LocalClearBalance END, ab.Limit, isnull(d.ProfitRate,0) Rate, ISNULL(d.RiskCode,'0001') ClassID, isnull(i.IndustryType,'-') IndustryType, 
isnull(c.GroupCode,'-') GroupCode, c.Gender, isnull(c.JurEmiratesID,'-') JurEmiratesID, isnull(j.JurEmiratesDesc,'-') JurEmiratesDesc, c.CountryofIncorporationCorp, 
isnull(c.StateCurr,'-') StateCurr, isnull(st.StateName,'-') StateName, 
isnull(c.Nationality,'-') Nationality, c.CategoryId, 
CASE WHEN c.CategoryId = 'Individual' AND c.ResidentNonResident = 'Y' THEN 'Y'	WHEN c.CategoryId = 'Individual' AND c.ResidentNonResident = 'N' THEN 'N'
	 WHEN c.CategoryId <> 'Individual' AND c.JurEmiratesID <> '008' THEN 'Y'	WHEN c.CategoryId <> 'Individual' AND c.JurEmiratesID = '008' THEN 'N'
	 WHEN c.ResidentNonResident IS NULL AND c.JurEmiratesID IS NULL THEN '-'
	 ELSE '-'
END	As Resident, CASE WHEN c.JurTypeID = '001' THEN 'Mainland' WHEN c.JurTypeID = '002' THEN 'Freezones' Else '-' END JurType,
CASE WHEN c.CategoryId = 'Individual' THEN c.Nationality		 WHEN c.CategoryId <> 'Individual' THEN c.CountryofIncorporationCorp
END	AS Nation, ISNULL(con.Name,'-') NationName, ISNULL(c.GroupID,'-') GroupID, ISNULL(g.GroupName,'-') GroupName,ClearBalance CCYBalance , sc.CBCode 
FROM t_AccountBalanceHistory ab WITH (NOLOCK)  
INNER JOIN t_Account a  WITH (NOLOCK) ON a.AccountID = ab.AccountID AND a.OurBranchID = ab.OurBranchID
INNER JOIN t_Customer c WITH (NOLOCK) ON c.ClientID = a.ClientID AND c.OurBranchID = a.OurBranchID
INNER JOIN t_Products p  WITH (NOLOCK) ON a.OurBranchID = p.OurBranchID AND a.ProductID = p.ProductID 
LEFT JOIN t_Disbursement d WITH (NOLOCK) ON a.OurBranchID = d.OurBranchID AND a.AccountID = d.AccountID
LEFT JOIN t_Industry i WITH (NOLOCK) on c.IndustryCorp = i.IndustryType AND c.OurBranchID = i.OurBranchID
LEFT JOIN t_JurEmirates j WITH (NOLOCK) on j.OurBranchID = c.OurBranchID AND j.JurEmiratesID = c.JurEmiratesID
LEFT JOIN vc_BRF_CustomerSicCodeCorp s ON s.ClientID = c.ClientID and s.OurBranchID = c.OurBranchID 
LEFT JOIN vc_BRF_SICCode sc ON sc.SICCode = s.SICCodeCorp and s.OurBranchID = sc.OurBranchID
LEFT JOIN t_State st WITH (NOLOCK) on st.OurBranchID = c.OurBranchID AND st.StateID = c.StateCurr AND st.CountryID = 'ARE'
LEFT JOIN t_ManageGroups g WITH (NOLOCK) on g.OurBranchID = c.OurBranchID and g.GroupID = c.GroupID 
LEFT JOIN t_Country con WITH (NOLOCK) on c.OurBranchID = con.OurBranchID 
AND CASE WHEN c.CategoryId = 'Individual' THEN c.Nationality WHEN c.CategoryId <> 'Individual' THEN c.CountryofIncorporationCorp END = con.CountryID
Where ab.ClearBalance < 0 AND p.ProductType in ('A') AND (p.ProductID NOT LIKE '%NOST' AND p.ProductID NOT LIKE '%PFAED') 
UNION ALL 
SELECT distinct ab.wDate, ab.OurBranchID, c.ClientID, c.Name, ab.AccountID, ab.ProductID, p.ProductType, ab.AccountName, a.CurrencyID,
ClearBalance = CASE WHEN a.CurrencyID = 'AED' THEN ab.ClearBalance ELSE ab.LocalClearBalance END, ab.Limit, isnull(l.Rate1,0) Rate, ISNULL(a.OverDraftClassID,'001') ClassID, isnull(i.IndustryType,'-') IndustryType, 
isnull(c.GroupCode,'-') GroupCode, c.Gender,  isnull(c.JurEmiratesID,'-') JurEmiratesID, isnull(j.JurEmiratesDesc,'-') JurEmiratesDesc, c.CountryofIncorporationCorp, 
isnull(c.StateCurr,'-') StateCurr, isnull(st.StateName,'-') StateName, isnull(c.Nationality,'-') Nationality, c.CategoryId, 
CASE WHEN c.CategoryId = 'Individual' AND c.ResidentNonResident = 'Y' THEN 'Y'	WHEN c.CategoryId = 'Individual' AND c.ResidentNonResident = 'N' THEN 'N'
	 WHEN c.CategoryId <> 'Individual' AND c.JurEmiratesID <> '008' THEN 'Y'	WHEN c.CategoryId <> 'Individual' AND c.JurEmiratesID = '008' THEN 'N'
	 WHEN c.ResidentNonResident IS NULL AND c.JurEmiratesID IS NULL THEN '-'
	 ELSE '-' 
END	As Resident, CASE WHEN c.JurTypeID = '001' THEN 'Mainland' WHEN c.JurTypeID = '002' THEN 'Freezones' Else '-' END JurType,
CASE WHEN c.CategoryId = 'Individual' THEN c.Nationality		 WHEN c.CategoryId <> 'Individual' THEN c.CountryofIncorporationCorp
END	AS Nation, ISNULL(con.Name,'-') NationName, ISNULL(c.GroupID,'-') GroupID, ISNULL(g.GroupName,'-') GroupName, ClearBalance CCYBalance , sc.CBCode 
FROM t_AccountBalanceHistory ab WITH (NOLOCK)  
INNER JOIN t_Account a  WITH (NOLOCK) ON a.AccountID = ab.AccountID AND a.OurBranchID = ab.OurBranchID 
INNER JOIN t_Customer c WITH (NOLOCK) ON c.ClientID = a.ClientID AND c.OurBranchID = a.OurBranchID
INNER JOIN t_Products p  WITH (NOLOCK) ON a.OurBranchID = p.OurBranchID AND a.ProductID = p.ProductID 
LEFT join t_LinkFacilityAccount l  WITH (NOLOCK) ON l.ourbranchid = ab.ourbranchid AND l.accountid = ab.AccountID
LEFT JOIN t_LinkCIFCollateral cc  WITH (NOLOCK) ON cc.OurBranchID = l.OurBranchID AND cc.ClientID = l.ClientID AND cc.SerialNo = l.FacilityID
LEFT JOIN t_Industry i WITH (NOLOCK) on c.IndustryCorp = i.IndustryType  AND c.OurBranchID = i.OurBranchID
LEFT JOIN t_JurEmirates j WITH (NOLOCK) on j.OurBranchID = c.OurBranchID AND j.JurEmiratesID = c.JurEmiratesID
LEFT JOIN vc_BRF_CustomerSicCodeCorp s WITH (NOLOCK) ON s.ClientID = c.ClientID and s.OurBranchID = c.OurBranchID 
LEFT JOIN vc_BRF_SICCode sc WITH (NOLOCK) ON sc.SICCode = s.SICCodeCorp and s.OurBranchID = sc.OurBranchID
LEFT JOIN t_State st WITH (NOLOCK) on st.OurBranchID = c.OurBranchID AND st.StateID = c.StateCurr AND st.CountryID = 'ARE'
LEFT JOIN t_ManageGroups g WITH (NOLOCK) on g.OurBranchID = c.OurBranchID and g.GroupID = c.GroupID 
LEFT JOIN t_Country con WITH (NOLOCK) on c.OurBranchID = con.OurBranchID 
AND CASE WHEN c.CategoryId = 'Individual' THEN c.Nationality WHEN c.CategoryId <> 'Individual' THEN c.CountryofIncorporationCorp END = con.CountryID
WHERE ab.ClearBalance < 0 AND p.ProductType IN ('AE') AND (p.ProductID NOT LIKE '%NOST' AND p.ProductID NOT LIKE '%PFAED');