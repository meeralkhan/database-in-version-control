﻿
--FORCED DEBIT TRXS
CREATE   VIEW [dbo].[ATM_vc_ForcedDebitTrxs]
AS
-- Query = string.Format(@"
--SELECT * FROM ATM_vc_ForcedDebitTrxs
-- WHERE OurBranchID = '02' 
-- AND convert(varchar(10), ValueDate, 120) >= '2021-08-04' AND convert(varchar(10), ValueDate, 120) <= '2021-08-08' 
-- order by wDate asc
SELECT b.ValueDate, a.wDate, a.OurBranchID, a.AccountID, Left(a.RefNo, 16) as PAN, a.STAN, a.RetRefNum, a.VISATrxID, a.RefNo, ISNULL(b.ValueDate,a.wDate) as TrxDate, 
ISNULL(b.Amount,a.Amount) as Amount, a.SettlmntAmount, a.ConvRate, a.AvailableBalance, a.MerchantType, a.AcqCountryCode, a.ForwardInstID, a.CurrCodeTran, a.CurrCodeSett,
Case 
	when LEFT(a.ProcCode,2) = '00' AND LEFT(a.POSEntryMode,2) = '01' then 'ECOM Trx'
	when LEFT(a.ProcCode,2) = '00' then 'POS Trx'
	when LEFT(a.ProcCode,2) = '01' then 'Cash Withdrawal Trx'
	when LEFT(a.ProcCode,2) = '31' then 'Balance Enquiry Trx'
	when LEFT(a.ProcCode,2) = '23' then 'Credit Trx'
	when LEFT(a.ProcCode,2) = '26' then 'Online Credit Trx'
	when LEFT(a.ProcCode,2) = '20' then 'Credit Vouch/ Merch Return Trx'
	when a.ProcCode = 'CANCL CHG' then 'Card Replacement [CANCELLED]'
	when a.ProcCode = 'LT CHG' then 'Card Replacement [LOST]'
	when a.ProcCode = 'STLN CHG' then 'Card Replacement [STOLEN]'
	when a.ProcCode = 'DMG CHG' then 'Card Replacement [DAMAGED]'
	when a.ProcCode = 'LT DELVRY CHG' then 'Card Replacement [LOST IN DELIVERY]'
	when a.ProcCode = 'REP CHG' then 'Card Replacement [NORMAL]'
end as TranDetails, a.DescriptionID, a.[Description], a.CardAccptNameLoc
FROM t_ForceDebitHostTrxs a 
INNER JOIN t_Transactions b ON a.OurBranchID=b.OurBranchID AND a.AccountID = b.AccountID And a.RetRefNum=b.RetRefNum and a.STAN=b.STAN
WHERE isnull(a.ProcCode,'') <> '' 
AND Isnull(a.ProcCode,'') NOT IN ('CANCL CHG', 'LT CHG', 'STLN CHG', 'DMG CHG', 'LT DELVRY CHG', 'REP CHG')