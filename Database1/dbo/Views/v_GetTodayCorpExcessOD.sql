﻿
CREATE   VIEW [dbo].[v_GetTodayCorpExcessOD]
AS
select h.OurBranchID, h.AccountID, Name, h.ProductID, b.CurrencyID
from t_PrevAccountBalances h
inner join t_Products b on h.OurBranchID = b.OurBranchID and h.ProductID = b.ProductID
and ISNULL(b.ProductType,'') = 'AE' and ISNULL(b.SegmentType,'') = 'Corporate'
where ClearBalance+Limit < 0