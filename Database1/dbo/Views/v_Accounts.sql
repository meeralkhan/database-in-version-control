﻿CREATE VIEW [dbo].[v_Accounts] AS   
  
 SELECT a.AccountID AS [Account ID], a.Name AS [Account Title],   
 CASE   
  WHEN ISNULL(b.ClearBalance,0) + ISNULL(b.Limit,0) - ISNULL(b.FreezeAmount,0) - ISNULL(p.MinBalance,0) > 0  
  THEN ISNULL(b.ClearBalance,0) + ISNULL(b.Limit,0) - ISNULL(b.FreezeAmount,0) - ISNULL(p.MinBalance,0)  
 ELSE   
  0 END AS [Drawable Balance]   
 FROM t_Account a   
 INNER JOIN t_AccountBalance b ON a.OurBranchID = b.OurBranchID AND a.AccountID = b.AccountID   
 INNER JOIN t_Products p ON a.OurBranchID = p.OurBranchID AND a.ProductID = p.ProductID