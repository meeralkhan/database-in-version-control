﻿

create VIEW [dbo].[vc_RptNewBankGLAbstract]                
 AS                  
 SELECT G.OurBranchID, G.AccountID, G.Description, G.CurrencyID,               
  MonthDebit=isnull(VC1.MonthDebit,0),     
  MonthCredit=isnull(VC2.MonthCredit,0),    
  Case     
  WHEN isnull(VC2.MonthCredit,0)-isnull(VC1.MonthDebit,0) < 0 then isnull(VC2.MonthCredit,0)-isnull(VC1.MonthDebit,0)    
  ELSE ''    
  End As DebitBalance,    
  Case     
  WHEN isnull(VC2.MonthCredit,0)-isnull(VC1.MonthDebit,0) >= 0 then isnull(VC2.MonthCredit,0)-isnull(VC1.MonthDebit,0)    
  ELSE ''    
  End As CreditBalance            
  FROM dbo.t_GL G          
  LEFT OUTER JOIN dbo.vc_RptGLMonthDebitAbstract VC1 ON G.OurBranchID = VC1.OurBranchID AND G.AccountID = VC1.AccountID           
  LEFT OUTER JOIN dbo.vc_RptGLMonthCreditAbstract VC2 ON G.OurBranchID = VC2.OurBranchID AND G.AccountID = VC2.AccountID          
  where (VC2.MonthCredit > 0 OR VC1.MonthDebit>0)