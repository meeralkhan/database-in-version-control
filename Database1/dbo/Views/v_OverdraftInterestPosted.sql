﻿create view [dbo].[v_OverdraftInterestPosted]
as
select * from t_RptODInterest
where ReceiveDate IN (select MAX(ReceiveDate) from t_RptODInterest)