﻿
CREATE   VIEW [dbo].[v_ClassifyTL1]
AS
select a.OurBranchID, a.AccountID, a.DealID, DATEDIFF(DAY, MAX(a.AccrualDate), MAX(c.WORKINGDATE)) AS Counts
from v_CalculateExcessTL a
inner join t_Products b on a.OurBranchID = b.OurBranchID and a.ProductID = b.ProductID
and ISNULL(b.IsAutoProvisioning,'') = 'Yes' and ISNULL(b.ProductType,'') = 'A' and ISNULL(b.SegmentType,'') = 'Corporate'
inner join t_Last c on a.OurBranchID = c.OurBranchID
inner join t_Disbursement d on a.OurBranchID = d.OurBranchID and a.AccountID = d.AccountID and a.DealID = d.DealID and ISNULL(d.DisableAutoClassification,0) = 0
where ISNULL(d.IsClosed,0) = 0 and ISNULL(d.RiskCode,'') not in ('0004','0005','0006')
group by a.OurBranchID, a.AccountID, a.DealID
having DATEDIFF(DAY, MAX(a.AccrualDate), MAX(c.WORKINGDATE)) >= ISNULL(MAX(b.AccMinDelinquent), 15)