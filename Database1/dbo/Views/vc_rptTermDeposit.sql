﻿
CREATE   VIEW [dbo].[vc_rptTermDeposit]
AS
-- SELECT * FROM vc_rptTermDeposit
-- Where OurBranchID = '{0}' and CAST(vDate AS DATE) = '{1}'
-- ORDER BY ClientID, RecieptID
SELECT v.Date AS vDate, a.OurBranchID, a.ClientID as CIF, i.AccountID as AccountID, 
a.Name as AccountTitle, 
CASE 
	when isnull(i.IsUnderLien,0) = 0 then 'No'
	when IsUnderLien = '0' then 'No'
	when IsUnderLien = '1' then 'Yes'
	ELSE 'No'
END as IsUnderLien,
i.NAccount as NominatedAccount, i.RecieptID as ReceiptID, i.ProductID as ProductID, 
i.IssueDate as IssueDate, i.MaturityDate as MaturityDate, v.Amount, v.FromDate as FromDate, 
v.ToDate as ToDate , v.Rate, v.Days, v.PerDay as PerDay, v.SubTotal as AccrualAmount, 
i.AutoProfit as AutoProfit, i.PaymentPeriod as PaymentPeriod,
a.ClientID, i.RecieptID
FROM dbo.t_AccrualHistory v
INNER JOIN dbo.t_TDRIssuance i ON i.OurBranchID = v.OurBranchID AND i.RecieptID = v.RecieptID
INNER JOIN dbo.t_Account a ON a.OurBranchID = i.OurBranchID AND a.AccountID = i.AccountID