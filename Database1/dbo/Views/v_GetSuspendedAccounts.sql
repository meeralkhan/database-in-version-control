﻿
CREATE   VIEW [dbo].[v_GetSuspendedAccounts]
AS
select l.LASTEOD wDate, a.OurBranchID, AccountID, '0' DealID, ClientID, 'OD' AS [Type], Name, a.ProductID, a.CurrencyID, OverdraftClassID RiskCode,
ISNULL((select ISNULL(SUM(Amount),0) from t_RptODAccruals c where OurBranchID = a.OurBranchID and AccountID = a.AccountID
and AccrualDate = l.LASTEOD group by OurBranchID, AccountID),0) Accrual,
0 PAccrual, GLInterestReceived, GLSuspendedInterest
from t_Account a
inner join t_Last l on a.OurBranchID = l.OurBranchID
inner join t_Products p on a.OurBranchID = p.OurBranchID and a.ProductID = p.ProductID and p.ProductID in (select ProductID from v_Products)
where ISNULL(OverdraftClassID,'') in ('004','005','006')
union all
select l.LASTEOD wDate, a.OurBranchID, a.AccountID, a.DealID, ClientID, 'TL' AS [Type], b.Name, b.ProductID, b.CurrencyID, a.RiskCode,
ISNULL((select ISNULL(SUM(Amount),0) from t_RptBPAccruals c where OurBranchID = a.OurBranchID and AccountID = a.AccountID
and DealID = a.DealID and RateType = 'Deal Rate' and AccrualDate = l.LASTEOD group by OurBranchID, AccountID, DealID),0) Accrual,
ISNULL((select ISNULL(SUM(Amount),0) from t_RptBPAccruals c where OurBranchID = a.OurBranchID and AccountID = a.AccountID
and DealID = a.DealID and RateType = 'Penal Rate' and AccrualDate = l.LASTEOD group by OurBranchID, AccountID, DealID),0) PAccrual,
GLInterestReceived, GLSuspendedInterest
from t_Disbursement a
inner join t_Last l on a.OurBranchID = l.OurBranchID
inner join t_Account b on a.OurBranchID = b.OurBranchID and a.AccountID = b.AccountID
inner join t_Products p on a.OurBranchID = p.OurBranchID and b.ProductID = p.ProductID
where ISNULL(a.IsClosed,0) = 0 and ISNULL(a.RiskCode,'') in ('0004','0005','0006')