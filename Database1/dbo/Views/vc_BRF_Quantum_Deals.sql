﻿CREATE VIEW [dbo].[vc_BRF_Quantum_Deals] AS SELECT d.*, d.ContryCode COUNTRYCODE,   
Case When d.Country = 'United Arab Emirates' OR d.ContryCode in ('UAE','AE') then 'Y' ELSE 'N' END Resident,  
d.Face_Value*c.MeanRate Face_Value_AED, d.Remain_FV*c.MeanRate Remain_FV_AED, d.MKT_Value*c.MeanRate MKT_Value_AED   
FROM Quantum_BRF_Deals d WITH (NOLOCK) 
INNER JOIN t_Currencies c WITH (NOLOCK) ON d.CCY = c.CurrencyID Where OurBranchID = '04';