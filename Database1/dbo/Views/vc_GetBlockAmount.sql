﻿CREATE VIEW [dbo].[vc_GetBlockAmount]    
AS      
   
SELECT	ab.OurBranchID, ab.AccountID, ab.Name, p.CurrencyID, ab.ClearBalance, ab.LocalClearBalance, c.MeanRate, af.Date, af.Amount AS FreezeAmount,
		af.Comments, af.ExpiredOn, af.UnFreezedDate, af.CreateBy
FROM t_AccountBalance ab 
INNER JOIN t_Products p ON ab.OurBranchID = p.OurBranchID AND ab.ProductID = p.ProductID 
INNER JOIN t_Currencies c ON p.OurBranchID = c.OurBranchID AND p.CurrencyID = c.CurrencyID 
RIGHT JOIN t_AccountFreeze af ON ab.OurBranchID = af.OurBranchID AND ab.AccountID = af.AccountID         
WHERE 
(ab.FreezeAmount <> 0 AND ab.IsFreezed = 1) OR (af.UnFreezedDate IS NULL)