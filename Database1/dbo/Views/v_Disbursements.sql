﻿create   view [dbo].[v_Disbursements]      
as      
select d.OurBranchID, a.ClientID, d.AccountID, a.Name, a.ProductID, a.CurrencyID, d.DealID, d.ReferenceNo, d.DisbursementDate, d.MaturityDate, ISNULL(d.IsClosed,0) AS IsClosed, d.CloseDate,      
(select top 1 Balance + PrincipalAmount from t_Adv_PaymentSchedule where OurBranchID = d.OurBranchID and AccountID = d.AccountID and DealID = d.DealID and IsPosted = 0 order by InstNo) AS Principal,      
(select top 1 ToDateProfit from t_Adv_PaymentSchedule where OurBranchID = d.OurBranchID and AccountID = d.AccountID and DealID = d.DealID and IsPosted = 0 order by InstNo) AS Interest,f.GroupID
from t_Disbursement d      
inner join t_Account a on d.OurBranchID = a.OurBranchID and d.AccountID = a.AccountID      
inner join t_LinkCIFCollateral f on a.OurBranchID = f.OurBranchID and a.ClientID = f.ClientID and d.FacilityID = f.ReferenceNo
where ISNULL(d.IsClosed,0) = 0