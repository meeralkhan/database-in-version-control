﻿CREATE    VIEW [dbo].[vc_rptVATDumpReport]
AS    
SELECT a.OurBranchID   AS OurBranchID,  a.ScrollNo AS ScrollNo,  a.Date  AS Date,  gta.AccountID AS IncomeGLNo,    
gta.Description   AS IncomeGLTitle, case isnull(gt.Amount,0) when 0 then ta.Amount else gt.Amount end AS AmountLCY,    
case isnull(gt.ForeignAmount,0) when 0 then isnull(ta.ForeignAmount,0) else gt.ForeignAmount end AS AmountFCY,    
g.AccountID  AS VATGLNo,  g.Description   AS VATGLTitle, a.Amount AS VATAmount,  a.[Percent] AS PercentageofVAT,    
td.DescriptionID   AS IncomeNarrationID, td.Description   AS NarrationIDTitle,  a.CurrencyID   AS CurrID,   a.ExchangeRate   AS ExchangeRate,    
a.TClientId AS CIFNo,  a.TAccountid   AS AccountNo,  c.Name  AS AccountTitle,  a.TProductId   AS ProductID,    
case a.IsCredit   when 0 then 'D' else 'C'   end  AS TrxType,   
case isnull(gt.Description,'') when '' then   ta.Description else gt.Description end AS Description,    
a.Status AS Status, a.OperatorID   AS Maker, a.SupervisorID   AS Checker    
FROM t_GLTransactions a    
LEFT Join t_GLTransactions as gt On a.ValueDate=gt.ValueDate And a.ScrollNo=gt.ScrollNo AND isnull(gt.VatReferenceID,'') = ''   
AND a.Status<>'R' and gt.IsCredit = 1 and substring(gt.AccountID,1,1) not in (5,6) and a.Amount = Round((isnull(gt.Amount,0) / 100 * a.[Percent]),2)    
Left Join t_Transactions as ta On ta.ScrollNo=a.ScrollNo AND ta.Status<>'R' and a.VatReferenceID = ta.VatReferenceID AND isnull(ta.RefNo,'') <> 'VAT'  
and ta.TrxType = 'D' 
LEFT JOIN t_GL gta ON gt.AccountID = gta.AccountID AND gta.OurBranchID = gt.OurBranchID    
INNER JOIN t_GL g ON a.AccountID = g.AccountID AND a.OurBranchID = g.OurBranchID    
INNER JOIN t_VatTypes b ON a.OurBranchID = b.OurBranchID AND a.VatID = b.VatID    
INNER JOIN t_TransactionDescriptions td ON td.DescriptionID = case isnull(ta.DescriptionID,'') when '' then gt.DescriptionID else ta.DescriptionID end   
AND td.OurBranchID = a.OurBranchID    
LEFT JOIN t_Account c ON a.TAccountid = c.AccountID AND a.OurBranchID = c.OurBranchID    
where a.IsCredit = 1 and isnull(a.VatReferenceID,'') <> '' and substring(a.AccountID,1,1) not in (5,6)