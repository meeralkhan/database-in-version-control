﻿create   view [dbo].[v_SupplierDrawableLimit]
as
select a.OurBranchID, a.ClientID, c.Name, ISNULL(f.AccountID,'') AccountID, a.CreditProgramID, b.CreditProgramName, a.Limit, ISNULL(SUM(d.AfterDeductingMargin),0) AS DrawingPower,  
case when ISNULL(MAX(g.ClearBalance),0) < 0 then ( case when ISNULL(SUM(d.AfterDeductingMargin),0)+ISNULL(MAX(g.ClearBalance),0) < 0 then 0 else  
ISNULL(SUM(d.AfterDeductingMargin),0)+ISNULL(MAX(g.ClearBalance),0) end) else ISNULL(SUM(d.AfterDeductingMargin),0) end AS DrawableLimit,

MAX(a.CreateTime) AS FacilityCreateTime, MAX(a.UpdateTime) AS FacilityUpdateTime,
MAX(b.CreateTime) AS CreditProgramCreateTime, MAX(b.UpdateTime) AS CreditProgramUpdateTime,
MAX(f.CreateTime) AS AccountLimitCreateTime, MAX(f.UpdateTime) AS AccountLimitUpdateTime,
MAX(d.CreateTime) AS CollateralAttachTime,
MAX(e.CreateTime) AS CollateralCreateTime, MAX(e.UpdateTime) AS CollateralUpdateTime,
MAX(g.LastUpdateBalanceTime) AS AccountBalanceUpdateTime

from t_LinkCIFCollateral a  
inner join t_CreditPrograms b on a.OurBranchID = b.OurBranchID and a.CreditProgramID = b.CreditProgramID and a.LimitSecType = 'SCF File' and a.Status = ''  
inner join t_Customer c on a.OurBranchID = c.OurBranchID and a.ClientID = c.ClientID  
left join t_LinkFacilityAccount f on a.OurBranchID = f.OurBranchID and a.ClientID = f.ClientID and a.SerialNo = f.FacilityID  
left join t_FacilityCollateral d on a.OurBranchID = d.OurBranchID and a.ClientID = d.ClientID and a.SerialNo = d.FacilityID  
left join t_Collaterals e on d.OurBranchID = e.OurBranchID and d.ClientID = e.ClientID and d.CollateralID = e.CollateralID  
left join t_AccountBalance g on f.OurBranchID = g.OurBranchID and f.AccountID = g.AccountID  
group by a.OurBranchID, a.ClientID, c.Name, f.AccountID, a.CreditProgramID, b.CreditProgramName, a.Limit