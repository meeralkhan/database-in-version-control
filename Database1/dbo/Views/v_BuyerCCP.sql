﻿create view [dbo].[v_BuyerCCP]            
as            
select a.OurBranchID,a.ClientID,a.CreditProgramID,a.CreditProgramExpiryDate,'AED' as CurrencyID,a.CreditProgramName,a.CreditProgramLimit ApprovedLimit,sum(isnull(b.Limit,0)) UtilizedLimit,            
a.CreditProgramLimit-sum(isnull(b.Limit,0)) AvailableLimit from t_CreditPrograms a             
inner join t_LinkCIFCollateral b on a.CreditProgramID = b.CreditProgramID and a.OurBranchID = b.OurBranchID            
group by a.OurBranchID,a.ClientID,a.CreditProgramID,a.CreditProgramExpiryDate,a.CreditProgramName,a.CreditProgramLimit