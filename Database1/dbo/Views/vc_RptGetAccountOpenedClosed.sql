﻿CREATE VIEW [dbo].[vc_RptGetAccountOpenedClosed]  
AS          
        
 SELECT a.OurBranchID, C.ClientID, C.Name AS ClientName, C.Address AS ClientAddress,  
 a.AccountID,a.ProductID, a.Name AS AccountName,a.Address AS AccountAddress,a.IntroducedBy, a.IntroducerAccountNo,           
 a.OpenDate, a.ModeOfOperation,a.CreateBy, a.Status, aob.Name AS OperatorsName,           
 ab.ClearBalance,a.IntroducerAddress, a.CloseDate          
 FROM t_Customer C   
 INNER JOIN t_Account a ON C.ClientID = a.ClientID AND C.OurBranchID = a.OurBranchID  
 INNER JOIN t_AccountBalance ab ON a.OurBranchID = ab.OurBranchID AND a.AccountID = ab.AccountID   
 LEFT OUTER JOIN t_AccountOperatedBy aob ON ab.OurBranchID = aob.OurBranchID AND ab.AccountID = aob.AccountID