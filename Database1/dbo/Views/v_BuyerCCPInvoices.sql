﻿
create view [dbo].[v_BuyerCCPInvoices]
as
select a.OurBranchID,a.CreditProgramID,d.ClientID,g.Name SupplierName,g.TradeLicenseNumberCorp TradeLicenseNum,c.Description InvoiceNumber,
b.LocalEq InvoiceAmount, CONVERT(DATE,a.CreateTime) StartDate, a.DateExpiry MaturityDate
from t_LinkCIFCollateral a
inner join t_Customer g on a.OurBranchID = g.OurBranchID and a.ClientID = g.ClientID
inner join t_FacilityCollateral c on c.OurBranchID = a.OurBranchID and c.FacilityID = a.SerialNo and a.ClientID = c.ClientID
inner join t_Collaterals b on a.OurBranchID = b.OurBranchID and c.ClientID = b.ClientID and c.CollateralID = b.CollateralID
inner join t_CreditPrograms d on a.CreditProgramID = d.CreditProgramID and a.OurBranchID = d.OurBranchID