﻿CREATE VIEW [dbo].[v_GetCashierBalances]       
       
as       
       
--t_CashTransactionModel       
 select OurBranchID,AccountID,TrxType, SUM(ForeignAmount) ForeignAmount, SUM(Amount) Amount from       
( SELECT OurBranchID,AccountID,TrxType, ForeignAmount, Amount       
       
 FROM t_CashTransactionModel       
 WHERE Supervision = 'C'       
       
 UNION ALL       
       
--t_CashTransactionModel Credit Side       
 SELECT OurBranchID,AccountID=GLID,'D' as TrxType, ForeignAmount, Amount       
       
 FROM t_CashTransactionModel       
 WHERE Supervision = 'C' AND TrxType = 'C'       
       
 UNION ALL       
       
--t_CashTransactionModel Debit Side       
 SELECT OurBranchID,AccountID=GLID, 'C' as TrxType, ForeignAmount, Amount       
       
 FROM t_CashTransactionModel       
 WHERE Supervision = 'C' AND TrxType = 'D'       
       
 UNION ALL       
       
--t_TransferTransactionModel       
 SELECT OurBranchID,AccountID, TrxType, ForeignAmount, Amount       
       
 FROM t_TransferTransactionModel       
 WHERE Supervision = 'C' AND AccountType ='G'       
       
 UNION ALL       
       
--t_InWardClearing       
 SELECT OurBranchID,AccountID,TransactionType as TrxType, ForeignAmount, Amount       
       
 FROM t_InWardClearing       
 WHERE Status <>'R' AND AccountType ='G'       
       
 UNION ALL       
       
       
--t_InWardClearing Main Office Account With Gl Parameter Serial No - 2       
 SELECT OurBranchID,AccountID,'C' as TrxType, ForeignAmount, Amount       
       
 FROM t_InWardClearing       
 WHERE AccountID = (    
  SELECT AccountID From t_GLParameters WHERE OurBranchID = t_InWardClearing.OurBranchID AND SerialID ='2'     
 )       
 AND Status <>'R' AND AccountType ='G'       
       
 UNION ALL       
       
--t_BureaDeChange - Cash Account ID       
       
 SELECT OurBranchID, CashAccountID AS AccountID,       
 CASE SellOrBuy WHEN 'B' THEN 'C' ELSE 'D' END AS TrxType, ForeignAmount, NetAmount       
 FROM t_BureaDeChange       
 WHERE Supervision = 'C'       
       
 UNION ALL       
       
--t_BureaDeChange - Currency Account ID       
 SELECT OurBranchID, CurrAccountID AS AccountID,       
 CASE SellOrBuy WHEN 'S' THEN 'C' ELSE 'D' END AS TrxType, ForeignAmount, Amount       
 FROM t_BureaDeChange       
 WHERE Supervision = 'C'       
       
/*       
 UNION ALL       
       
--t_ATM_CashTransaction       
 SELECT OurBranchID,AccountID=GLID,'C' as TrxType, ForeignAmount, Amount       
 FROM t_ATM_CashTransaction       
 WHERE Supervision = 'C'       
       
 UNION ALL       
       
--t_ATM_CashTransaction       
 SELECT OurBranchID,AccountID,'D' as TrxType, ForeignAmount, Amount       
       
 FROM t_ATM_CashTransaction       
 WHERE Supervision = 'C'       
       
 UNION ALL       
       
--'t_ATM_TransferTransaction       
 SELECT OurBranchID,AccountID,TrxType, ForeignAmount, Amount       
       
 FROM t_ATM_TransferTransaction       
 WHERE Supervision = 'C' AND AccountType ='G'       
*/      
       
 UNION ALL       
       
--'t_OnLine_CashTransaction       
 SELECT OurBranchID,AccountID,TrxType, ForeignAmount, Amount       
       
 FROM t_OnLineCashTransaction       
 WHERE Supervision <> 'R' AND AccountType ='G'       
 
 UNION ALL
 
 --t_OnlineLocalCashTransaction Credit Side
 SELECT OurBranchID,AccountID=GLID,'D' as TrxType, ForeignAmount, Amount
 
 FROM t_OnlineLocalCashTransaction
 WHERE Supervision = 'C' AND TrxType = 'C'
 
 UNION ALL
 
 --t_OnlineLocalCashTransaction Debit Side
 SELECT OurBranchID,AccountID=GLID,'C' as TrxType, ForeignAmount, Amount
 
 FROM t_OnlineLocalCashTransaction
 WHERE Supervision = 'C' AND TrxType = 'D'
 
/*       
 UNION ALL       
       
--'t_OL_LocalTransferTransaction       
 SELECT OurBranchID,AccountID,TrxType, ForeignAmount, Amount       
       
 FROM t_OL_LocalTransferTransaction       
 WHERE Supervision <> 'R' AND AccountType ='G'       
 */       
)       
 Asd Group by OurBranchID,AccountID,TrxType