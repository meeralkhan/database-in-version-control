﻿	CREATE VIEW [dbo].[vc_AccountBalance] 
	as
	SELECT ab.OurBranchID, ab.AccountID, ab.ProductID, ab.Name, ab.ClearBalance, ab.IsFreezed, ab.FreezeAmount, ab.Effects, ab.LocalEffects,ab.ShadowBalance, ab.Limit, a.CurrencyID,
	"LocalClearBalance" = CASE WHEN a.CurrencyID = 'AED' THEN ab.ClearBalance ELSE ab.LocalClearBalance END
	FROM dbo.t_AccountBalance ab
	INNER JOIN t_Account a ON ab.OurBranchID = a.OurBranchID and a.AccountID = ab.AccountID;