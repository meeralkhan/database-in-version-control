﻿CREATE VIEW [dbo].[vc_BRF_GLTransactionAccount] as 
Select Cast(g.Date as Date) wDate, g.AccountID GLAccountID, ISNULL(g.TClientId,'-') ClientID, ISNULL(g.TAccountid,'-') AccountID, ISNULL(a.Name,'-') Name, ISNULL(g.TProductID,'-') ProductID, 
Case When g.IsCredit = 1 then g.Amount else -g.Amount End AmountInAED, g.CurrencyID, ISNULL(a.Resident,'-') Resident, a.Nation, a.GroupCOde, a.ProductType, a.IndustryType, a.Gender, a.StateCurr, a.StateName
from t_GLTransactions g WITH (NOLOCK)
LEFT JOIN vc_BRF_Account a WITH (NOLOCK) On a.OurBranchid = g.OurBranchID and g.TAccountID = a.AccountID and g.TClientId = a.ClientID
Where g.Status <> 'R' AND g.Amount <> 0;