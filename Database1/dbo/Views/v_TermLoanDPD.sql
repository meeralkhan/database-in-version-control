﻿
CREATE   VIEW [dbo].[v_TermLoanDPD]
AS
select a.OurBranchID, a.AccountID, a.DealID, DATEDIFF(DAY, MAX(a.AccrualDate), MAX(c.WORKINGDATE)) AS Counts
from v_CalculateExcessTL a
inner join t_Products b on a.OurBranchID = b.OurBranchID and a.ProductID = b.ProductID
and ISNULL(b.IsAutoProvisioning,'') = 'Yes' and ISNULL(b.ProductType,'') = 'A' and ISNULL(b.SegmentType,'') = 'Corporate'
inner join t_Last c on a.OurBranchID = c.OurBranchID
inner join t_Disbursement d on a.OurBranchID = d.OurBranchID and a.AccountID = d.AccountID and a.DealID = d.DealID
where ISNULL(d.IsClosed,0) = 0 and ISNULL(d.RiskCode,'') != '0004'
group by a.OurBranchID, a.AccountID, a.DealID