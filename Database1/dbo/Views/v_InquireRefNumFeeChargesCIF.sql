﻿CREATE   view [dbo].[v_InquireRefNumFeeChargesCIF]      
as      
select  db.TrxRefNo,a.ClientID,b.OurBranchID,b.AccountID,b.ReferenceNo,a.ProductID,b.FacilityID,d.AccountLimitID, '001' RefType, db.Charges feeAmount,      
db.Charges-db.feeReverseAmount feeAvailReverseAmount, db.feeReverseAmount feeReverseAmount, b.feesAccountID feeChargesAccNo, dc.ChargeID AS FeeType      
from t_Account a      
inner join t_Disbursement b on a.OurBranchID = b.OurBranchID and a.AccountID = b.AccountID      
inner join t_Disb_Account_Charges db on b.OurBranchID = db.OurBranchID and a.AccountID = db.AccountID and b.DealID = db.DealID      
inner join t_DisbursementCharges dc on b.OurBranchID = dc.OurBranchID and db.ChargeID = dc.ChargeID      
left join t_LinkFacilityAccount d on b.AccountID = d.AccountID and b.OurBranchID = d.OurBranchID      
where db.Charges-db.feeReverseAmount <> 0