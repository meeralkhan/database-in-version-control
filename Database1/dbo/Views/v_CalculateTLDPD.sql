﻿
CREATE   VIEW [dbo].[v_CalculateTLDPD]
AS
select d.OurBranchID, d.AccountID, d.DealID, DATEDIFF(DAY, ISNULL(MAX(a.AccrualDate), MAX(c.WORKINGDATE)), MAX(c.WORKINGDATE)) AS NoOfDPD
from t_Disbursement d
inner join t_Account ab on ab.OurBranchID = d.OurBranchID and ab.AccountID = d.AccountID
inner join t_Products b on ab.OurBranchID = b.OurBranchID and ab.ProductID = b.ProductID
and ISNULL(b.ProductType,'') = 'A' and ISNULL(b.SegmentType,'') = 'Corporate'
inner join t_Last c on ab.OurBranchID = c.OurBranchID
left join v_CalculateExcessTL a on a.OurBranchID = d.OurBranchID and a.AccountID = d.AccountID and a.DealID = d.DealID
where ISNULL(d.IsClosed,0) = 0
group by d.OurBranchID, d.AccountID, d.DealID