﻿CREATE VIEW [dbo].[vc_BRF_IntRemittance_93] AS SELECT DISTINCT DateOfTrx wDate, IsNull(AmountInAED,0) Amount, (Case When InwardOutward='OUTWARD' then 'D' else 'C' End) TrxType, Txn_ID TrxID,
(CASE 
WHEN c.GroupCode IN ('INDIV','HNIND') AND s.TrxPurpose IN ('CRP','EMI','SLA','SLL','IOL','LIP','LLA','LLL','LNC','LND') THEN 'Debt Settlement'
WHEN s.TrxPurpose IN ('CIN','DLF','LDL','PIN','PPA','PPL','PRR','RDA','RDL') THEN 'Direct Investment'
WHEN c.GroupCode IN ('INDIV','HNIND') AND s.TrxPurpose IN ('EDU') THEN 'Education'
WHEN c.GroupCode IN ('INDIV','HNIND') AND s.TrxPurpose IN ('UTL','AES','ALW','BON','COP','DCP','EOS','FAM','LAS','MWI','MWO','MWP','OVT','PEN','RNT','SAA','SAL') THEN 'Family Support'
WHEN s.TrxPurpose IN ('CEA','CEL','DIV','DLA','DLL','DOE','DSA','DSF','DSL','FDA','FDL','FIA','FIL','FSA','FSL','IGD','IOD','IPO','ISH','ISL','ISS','LDS','PIP','POR','REA','REL') THEN 'Financial Investment'
WHEN c.GroupCode IN ('INDIV','HNIND') AND s.TrxPurpose IN ('ACM','AFA','AFL','CBP','CCP','CHC','COM','DIF','DIL','FIS','GMS','GOS','GRI','IFS','IGT','IID','INS','IPC','IRP','IRW','ITS','LEA','LEL','MCR','OAT','OTS','PMS','POS','PRP','PRS','PRW','RDS','RFS','RLS','SCO','STS','SVI','SVO','SVP','TAX','TCP','TCR','TCS','TOF','UFP','XAT','GDE','GDI','GDS','TTS') THEN 'Other'
WHEN c.GroupCode IN ('INDIV','HNIND') AND s.TrxPurpose IN ('STR','TKT','ATS') THEN 'Tourism'
WHEN c.GroupCode NOT IN ('INDIV','HNIND') AND s.TrxPurpose IN ('CRP','EMI','SLA','SLL','IOL','LIP','LLA','LLL','LNC','LND','EDU','UTL','AES','ALW','BON','COP','DCP','EOS','FAM','LAS','MWI','MWO','MWP','OVT','PEN','RNT','SAA','SAL','ACM','AFA','AFL','CBP','CCP','CHC','COM','DIF','DIL','FIS','GMS','GOS','GRI','IFS','IGT','IID','INS','IPC','IRP','IRW','ITS','LEA','LEL','MCR','OAT','OTS','PMS','POS','PRP','PRS','PRW','RDS','RFS','RLS','SCO','STS','SVI','SVO','SVP','TAX','TCP','TCR','TCS','TOF','UFP','XAT','STR','TKT','ATS') THEN 'Other'
WHEN c.GroupCode NOT IN ('INDIV','HNIND') AND s.TrxPurpose IN ('GDE','GDI','GD.S','TTS') THEN 'Trade'
ELSE 'Other' 
END) AS TrxPurpose,s.TrxPurpose TransPurpose, s.Country CountryID, c.ClientID, s.ToAccountNumber AccountID, t.Name, t.ProductID, c.GroupCode, s.ChannelRefID, 
GCode = CASE 
WHEN c.GroupCode IN ('GOVFE','GOVNF','GOVEG','GOVNE','FORGV') THEN 'Government'
WHEN c.GroupCode IN ('CENBK','COMBK','FORBK','INVBK','ISLBK','NBFIN') THEN 'Corporate'
WHEN c.GroupCode IN ('GREEG','GREFG','GRESA') THEN 'GRE'								
WHEN c.GroupCode IN ('CGOVT','OCORP','LCORP','MDCOP','OPENT','VLCOP','HNIND') THEN 'Corporate'								
WHEN c.GroupCode IN ('SMALL','MEDIU','MENTP') THEN 'SME'								
WHEN c.GroupCode IN ('INDIV') THEN 'Individual'
END
FROM SwiftTransfers s WITH (NOLOCK)  
INNER JOIN t_Transactions ts WITH (NOLOCK) ON s.ChannelRefID = ts.ChannelRefID AND ChannelID = '9' AND DescriptionID in ('100040','200040') 
LEFT JOIN t_Account t WITH (NOLOCK)  ON s.ToAccountNumber = t.AccountID
LEFT JOIN t_Customer c WITH (NOLOCK)  ON c.OurBranchID = t.OurBranchID AND c.ClientID = t.ClientID;