﻿CREATE   VIEW [dbo].[v_GetOverDueSuspendedDeals]  
AS  
select l.LASTEOD wDate, a.OurBranchID, a.AccountID, a.DealID, ClientID, 'TL' AS [Type], c.Name, c.ProductID, c.CurrencyID, b.RiskCode,  
isnull(SUM((case a.[Type] when 'N' then a.Amount else 0 end)),0) Accrual, isnull(sum((case a.[Type] when 'P' then a.Amount else 0 end)),0) PAccrual,  
GLInterestReceived, GLSuspendedInterest  
from t_TrackReceivables a  
inner join t_Last l on a.OurBranchID = l.OurBranchID and FORMAT(a.wDate, 'yyyy-MM') <> FORMAT(l.LASTEOD, 'yyyy-MM')
inner join t_Disbursement b on a.OurBranchID = b.OurBranchID and a.AccountID = b.AccountID and a.DealID = b.DealID  
and ISNULL(b.IsClosed,0) = 0 and ISNULL(b.RiskCode,'') in ('0004','0005','0006') and a.IsFreeze = 1 and a.Type <> 'F'  
inner join t_Account c on a.OurBranchID = c.OurBranchID and a.AccountID = c.AccountID  
inner join t_Products p on c.OurBranchID = p.OurBranchID and c.ProductID = p.ProductID  
group by l.LASTEOD, a.OurBranchID, a.AccountID, a.DealID, ClientID, c.Name, c.ProductID, c.CurrencyID, b.RiskCode,  
GLInterestReceived, GLSuspendedInterest