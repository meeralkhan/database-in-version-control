﻿create view [dbo].[VCI_GetGLAccountDetails]
AS              
 select g.OurBranchID, 'GL' as ProductID, 'General Ledger' as ProductDesc, c.CurrencyID, c.MeanRate,
 b.BankID, b.BranchID, (b.BranchID + ':' + b.Name + ':' + isnull(convert(nvarchar(100), b.Address),'')) as Branch,
 g.AccountID, g.[Description] as AccountTitle, c.CurrencyID + ':' + c.[Description] as Currency, 
 CurrentBalance = g.Balance, g.ForeignBalance EquivalentBalance
 from t_GL g
 INNER JOIN t_Currencies c ON g.OurBranchID = c.OurBranchID AND g.CurrencyID = c.CurrencyID              
 INNER JOIN t_Branches b ON g.OurBranchID = b.OurBranchID AND g.OurBranchID = b.BranchID