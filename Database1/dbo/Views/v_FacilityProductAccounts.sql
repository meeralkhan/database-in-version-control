﻿
CREATE   VIEW [dbo].[v_FacilityProductAccounts]
AS
SELECT a.OurBranchID, a.ClientID, a.SerialNo FacilityID, b.ProductID, c.AccountID, c.CurrencyID, a.ReferenceNo FacilityRefNo,
d.AccountLimitID, a.LimitSecType, a.GroupID, a.Status FacilityStatus, b.Status as FacilityProductStatus, c.Status AccountStatus,
a.Limit FacilityLimit, b.Limit FacilityProductLimit, e.Limit AccountLimit, d.Limit, d.Limit1
FROM t_LinkCIFCollateral a
inner join t_FacilityProduct b on a.OurBranchID = b.OurBranchID and a.ClientID = b.ClientID and a.SerialNo = b.FacilityID
inner join t_Account c on a.OurBranchID = c.OurBranchID and a.ClientID = c.ClientID and b.ProductID = c.ProductID and c.Status != 'C'
inner join t_LinkFacilityAccount d on a.OurBranchID = d.OurBranchID and a.ClientID = d.ClientID and a.SerialNo = d.FacilityID and c.AccountID = d.AccountID
inner join t_AccountBalance e on a.OurBranchID = e.OurBranchID and c.AccountID = e.AccountID