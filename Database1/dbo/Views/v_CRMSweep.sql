﻿create view [dbo].[v_CRMSweep]
as
select a.OurBranchID, a.AccountID, b.SerialNo, b.Type, b.Action, b.Amount, b.SIAccountID, b.SIAccountID2, 
CASE WHEN b.Action = 'D' THEN '-' ELSE CONVERT(VARCHAR(50),b.CreateTime,120) END CreateTime,
--CASE WHEN b.Action <> 'D' THEN '-' ELSE CONVERT(VARCHAR(50),b.CreateTime,120) END DeleteTime,
(SELECT TOP 1 CASE WHEN c.Action = 'D' then CONVERT(VARCHAR(50),c.CreateTime,120) ELSE '-' END FROM dbo.t_AccountSweeps c WHERE c.SerialNo > b.SerialNo AND c.OurBranchID = b.OurBranchID 
AND c.Type = b.type AND c.AccountID = a.AccountID) DeleteTime
from t_AccountSweeps b
INNER JOIN t_Account a on a.OurBranchID = b.OurBranchID and a.AccountID = b.AccountID
WHERE ISNULL(Status,'') NOT IN ('C','I') AND ProductID IN (select ProductID from v_Products) AND b.Action <> 'D' --AND a.AccountID = '0310100000046701' 