﻿create view [dbo].[vc_GetBackDateBureauTrxRecords]
AS      
      
 select OurBranchID, ScrollNo, RefNo, AccountType, AccountID, AccountName, ProductID, CurrencyID, TrxType, wDate, ValueDate, ChequeID, ChequeDate,     
 Amount, ForeignAmount, ExchangeRate, IsLocalCurrency, [Status], Supervision, GlID, OperatorID, SupervisorID, AppWHTax, WHTaxMode,     
 WHTaxAmount, [Description], AdditionalData       
 from (      
       
  select OurBranchID, ScrollNo, '' RefNo, 'G' AccountType, AccountID,     
  AccountName = (SELECT [Description] FROM t_GL WHERE OurBranchID = t_GLTransactions.OurBranchID AND AccountID = t_GLTransactions.AccountID),    
  'GL' ProductID, CurrencyID, CASE IsCredit WHEN 1 THEN 'C' ELSE 'D' END TrxType, [Date] wDate, ValueDate, 'V' ChequeID, NULL ChequeDate, Amount,     
  ForeignAmount, ExchangeRate, CASE CurrencyID     
   WHEN (SELECT LocalCurrency FROM t_GlobalVariables WHERE OurBranchID = t_GLTransactions.OurBranchID) THEN 1      
   ELSE 0       
  END IsLocalCurrency, 'C' [Status], [Status] Supervision, GlID, OperatorID, SupervisorID, '0' AppWHTax, '' WHTaxMode,     
  '0' WHTaxAmount, [Description], AdditionalData    
  from t_GLTransactions       
  where DocType = 'B' and IsMainTrx = '1'  
       
  union all      
       
  select OurBranchID, ScrollNo, '' RefNo, 'G' AccountType, AccountID,     
  AccountName = (SELECT [Description] FROM t_GL WHERE OurBranchID = t_RejectedGLTrx.OurBranchID AND AccountID = t_RejectedGLTrx.AccountID),    
  'GL' ProductID, CurrencyID, CASE IsCredit WHEN 1 THEN 'C' ELSE 'D' END TrxType, [Date] wDate, ValueDate, 'V' ChequeID, NULL ChequeDate, Amount,     
  ForeignAmount, ExchangeRate, CASE CurrencyID       
   WHEN (SELECT LocalCurrency FROM t_GlobalVariables WHERE OurBranchID = t_RejectedGLTrx.OurBranchID) THEN 1    
   ELSE 0       
  END IsLocalCurrency, 'R' [Status], 'R' Supervision, GlID, OperatorID, SupervisorID, '0' AppWHTax, '' WHTaxMode, '0'       
  WHTaxAmount, [Description], AdditionalData       
  from t_RejectedGLTrx    
  where DocType = 'B' and IsMainTrx = '1'      
       
 ) a --order by ScrollNo