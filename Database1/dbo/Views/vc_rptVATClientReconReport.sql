﻿
CREATE   VIEW [dbo].[vc_rptVATClientReconReport]
AS                  
SELECT ta.OurBranchID, ta.ScrollNo, gt.AccountID as GLAccountID, gta.Description as GLTitle,g.AccountID as VATAccountID,                    
g.Description VATGLTitle,c.AccountID, c.Name, c.ProductID,  d.ClientID, d.Name AS ClientName, td.DescriptionID as NarrationID,  
td.Description as Narration,ta.wDate, c.CurrencyID,                    
ta.ExchangeRate, ta.Amount,ta.ForeignAmount, ta.WHTaxAmount as VATAmount, '' as InvoiceNo, '' as InvoiceDate,   
'' as CreditNoteNo, '' as CreditNoteDate,ta.ValueDate            
FROM t_GLTransactions a      
LEFT Join t_GLTransactions as gt On a.ValueDate=gt.ValueDate And a.ScrollNo=gt.ScrollNo AND isnull(gt.VatReferenceID,'') = ''   
AND a.Status<>'R' and gt.IsCredit = 1 AND substring(gt.AccountID,1,1) NOT IN (5,6) and a.Amount = Round((isnull(gt.Amount,0) / 100 * a.[Percent]),2)    
inner Join t_Transactions as ta On ta.ScrollNo=a.ScrollNo AND ta.Status<>'R' and a.VatReferenceID = ta.VatReferenceID AND isnull(ta.RefNo,'') <> 'VAT'   
and ta.TrxType = 'D' 
LEFT JOIN t_GL gta ON gt.AccountID = gta.AccountID AND gta.OurBranchID = gt.OurBranchID                 
INNER JOIN t_GL g ON a.AccountID = g.AccountID AND a.OurBranchID = g.OurBranchID                 
INNER JOIN t_VatTypes b ON a.OurBranchID = b.OurBranchID AND a.VatID = b.VatID      
INNER JOIN t_TransactionDescriptions td ON td.DescriptionID = case isnull(ta.DescriptionID,'') when '' then gt.DescriptionID else ta.DescriptionID end   
AND td.OurBranchID = a.OurBranchID                     
LEFT JOIN t_Account c ON a.TAccountid = c.AccountID AND a.OurBranchID = c.OurBranchID             
left join t_Customer d on c.OurBranchID = d.OurBranchID and c.ClientID = d.ClientID         
where a.IsCredit = 1 and isnull(a.VatReferenceID,'') <> '' AND substring(a.AccountID,1,1) NOT IN (5,6)