﻿CREATE VIEW [dbo].[vc_BRF_AccountBalanceHistory] as SELECT Distinct ab.wDate, ab.OurBranchID, c.ClientID CIF, ab.AccountID, ab.ProductID, p.ProductType, ab.AccountName, a.CurrencyID,
ClearBalance = CASE WHEN a.CurrencyID = 'AED' THEN ab.ClearBalance ELSE ab.LocalClearBalance END, ab.ClearBalance CCYBalance, ab.Limit,isnull(i.IndustryType,'-') IndustryType, 
isnull(c.GroupCode,'-') GroupCode, c.Gender, isnull(c.JurEmiratesID,'-') JurEmiratesID, isnull(j.JurEmiratesDesc,'-') JurEmiratesDesc, c.CountryofIncorporationCorp, 
isnull(c.StateCurr,'-') StateCurr, isnull(s.StateName,'-') StateName, 
isnull(c.Nationality,'-') Nationality, c.CategoryId, 
CASE WHEN c.CategoryId = 'Individual' AND c.ResidentNonResident = 'Y' THEN 'Y'	  WHEN c.CategoryId = 'Individual' AND c.ResidentNonResident = 'N' THEN 'N'
	 WHEN c.CategoryId <> 'Individual' AND c.JurEmiratesID <> '008' THEN 'Y'  WHEN c.CategoryId <> 'Individual' AND c.JurEmiratesID = '008' THEN 'N'
	 WHEN c.ResidentNonResident IS NULL AND c.JurEmiratesID IS NULL THEN '-'
	 ELSE '-' 
END	As Resident, CASE WHEN c.JurTypeID = '001' THEN 'Mainland' WHEN c.JurTypeID = '002' THEN 'Freezones' Else '-' END JurType,
CASE WHEN c.CategoryId = 'Individual' THEN c.Nationality  WHEN c.CategoryId <> 'Individual' THEN c.CountryofIncorporationCorp END AS Nation 
FROM t_AccountBalanceHistory ab WITH (NOLOCK) 
INNER JOIN t_Account a  WITH (NOLOCK) ON a.AccountID = ab.AccountID AND ab.OurBranchID = a.OurBranchID
INNER JOIN t_Customer c WITH (NOLOCK) ON c.OurBranchID = a.OurBranchID AND c.ClientID = a.ClientID
INNER JOIN t_Products p WITH (NOLOCK) ON p.OurBranchID = a.OurBranchID AND p.ProductID = a.ProductID
LEFT JOIN t_Industry i WITH (NOLOCK) on c.IndustryCorp = i.IndustryType AND c.OurBranchID = i.OurBranchID
LEFT JOIN t_JurEmirates j WITH (NOLOCK) on j.OurBranchID = c.OurBranchID AND j.JurEmiratesID = c.JurEmiratesID
LEFT JOIN t_State s WITH (NOLOCK) on s.OurBranchID = c.OurBranchID AND s.StateID = c.StateCurr AND s.CountryID = 'ARE';