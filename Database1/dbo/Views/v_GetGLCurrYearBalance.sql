﻿CREATE view [dbo].[v_GetGLCurrYearBalance]
as
select OurBranchID, AccountID, CurrencyID,YEAR(ValueDate) BalYear, 
SUM(case IsCredit when 1 then Amount else Amount*-1 end) LCYTrxBalance,            
SUM(case IsCredit when 1 then ForeignAmount else ForeignAmount*-1 end) FCYTrxBalance            
from t_GLTransactions where Status != 'R' group by OurBranchID, AccountID, CurrencyID,YEAR(ValueDate)