﻿
create view [dbo].[v_ExpiredCollaterals3]
AS
select c.OurBranchID, c.ClientID, 0 AS FacilityID, c.CollateralID, c.Description, c.ReviewDate, c.cExpiryDate ExpiryDate, c.CollateralValue, c.DrawingPower AfterDeductingMargin
from t_Collaterals c
where cExpiryDate < (select WorkingDate from t_Last where OurBranchID = c.OurBranchID)
and c.CollateralStatus NOT IN ('C','E')