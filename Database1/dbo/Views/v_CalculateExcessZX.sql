﻿
CREATE   VIEW [dbo].[v_CalculateExcessZX]
AS
select a.OurBranchID, b.ClientID, a.AccountID, b.Name AS AccountName, b.ProductID, b.CurrencyID, cast(MIN(a.wDate)+1 as date) AS AccrualDate
from t_TrackReceivablesRetail a
inner join t_Account b on a.OurBranchID = b.OurBranchID and a.AccountID = b.AccountID
where a.IsFreeze = 1
group by a.OurBranchID, b.ClientID, a.AccountID, b.Name, b.ProductID, b.CurrencyID