﻿CREATE VIEW [dbo].[vc_GetFDSlips]
AS    
 SELECT t.OurBranchID, t.AccountID, a.Name, a.Address, t.ProductID, p.Description AS ProducutDescription,   
 c.CurrencyID, c.Description AS CurrencyDescription, t.RecieptID, t.RefNo, t.Amount, t.Rate,   
 CAST(p.PeriodsInFigure as nvarchar(10)) + '-' +     
  Case p.Period     
   when 'Y' then 'Year'    
   when 'M' then 'Month'    
   when 'D' then 'Day'    
  end     
 as Period,  
 t.ExchangeRate, t.IssueDate, t.StartDate, t.MaturityDate, t.Interest, t.CreateBy as OperatorID, t.CloseDate,   
 t.InterestPaid, t.InterestPaidUpto, ISNULL(t.IsUnderLien,0) as IsUnderLien, ISNULL(t.IsLost,0) as IsLost,   
 ISNULL(t.IsClosed,0) as IsClosed, 1 as IsTax, 0 as TaxAmount, t.LostRemarks,  
 [Status] = case  
  when isnull(t.IsClosed,0) = 1 then 'Closed'     
  when isnull(t.IsLost,0) = 1 then 'Lost'    
  when isnull(t.IsUnderLien,0) = 1 then 'Lien'    
  else    
   'Open'    
 end, 0 as IsPrincipalPaid,   
 case isnull(t.Autorollover,'')  
  when '' then 'No'  
  else t.AutoRollover  
 end as Autorollover, t.OptionsAtMaturity, a.Phone1 as Phone, a.ModeOfOperation as IsJoint,   
 0 as IsPremiumRateApplicable, t.AutoProfit, 0 as Weightrate, 0 as Premiumrate, 0 as Prematurerate  
 FROM t_TDRIssuance t  
 INNER JOIN t_Account a ON t.OurBranchID = a.OurBranchID AND t.AccountID = a.AccountID AND t.ProductID = a.ProductID  
 INNER JOIN t_Products p ON a.OurBranchID = p.OurBranchID AND a.ProductID = p.ProductID   
 INNER JOIN t_Currencies c ON p.OurBranchID = c.OurBranchID AND p.CurrencyID = c.CurrencyID