﻿
CREATE   VIEW [dbo].[v_OverdraftDPD]
AS
select a.OurBranchID, a.AccountID, COUNT(a.OurBranchID) Counts, ISNULL(b.SSDays, 60) as LastDays
from v_CalculateExcessOD a
inner join t_Products b on a.OurBranchID = b.OurBranchID and a.ProductID = b.ProductID and ISNULL(b.ProductType,'') = 'AE' and ISNULL(b.SegmentType,'') = 'Corporate'
where a.AccrualDate >= (select WorkingDate-(ISNULL(b.SSDays, 60)) from t_Last where OurBranchID = a.OurBranchID)
and a.AccountID IN (select AccountID from t_Account where ISNULL(OverdraftClassID,'') != '004')
group by a.OurBranchID, a.AccountID, b.SSDays