﻿CREATE VIEW [dbo].[vc_RptGetTOD]
AS
SELECT a.OurBranchID, a.AccountID, MAX(t.wDate) AS TODDate, (ab.ClearBalance + ab.Effects) AS ClearBalance, ab.Name AS AccountName,
ab.ProductID, c.Name AS ClientName, p.CurrencyID, MAX(t.wDate) AS LastTransactionDate, a.ClientID, ab.Limit

FROM t_Transactions t
INNER JOIN t_Account a ON t.OurBranchID = a.OurBranchID AND t.AccountID = a.AccountID
INNER JOIN t_AccountBalance ab ON t.OurBranchID = ab.OurBranchID AND t.AccountID = ab.AccountID
INNER JOIN t_Products p ON a.OurBranchID = p.OurBranchID AND a.ProductID = p.ProductID AND ISNULL(ProductType,'') IN ('AE','L')
AND p.ProductID NOT IN (SELECT ISNULL(ReceivableProductID,'') FROM t_Products WHERE OurBranchID = p.OurBranchID)
AND p.ProductID NOT IN (SELECT ISNULL(CapitalizedProductID,'') FROM t_Products WHERE OurBranchID = p.OurBranchID)
INNER JOIN t_Customer c ON a.OurBranchID = c.OurBranchID AND a.ClientID = c.ClientID

GROUP BY a.OurBranchID, a.AccountID, ab.ClearBalance + ab.Effects,
ab.Name, ab.ProductID, c.Name, p.CurrencyID, a.ClientID, ab.Limit, ab.Effects

HAVING (ab.ClearBalance + ab.Effects < 0) AND (ab.Limit = 0)