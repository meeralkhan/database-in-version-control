﻿create view [dbo].[v_ExpiredFacilites]    
AS    
select f.OurBranchID, f.ClientID, f.SerialNo AS FacilityID, f.ReferenceNo, f.FacilityDesc, f.DateReview AS ReviewDate, f.DateExpiry AS ExpiryDate, f.Limit    
from t_LinkCIFCollateral f    
where f.Status != 'E' and f.DateExpiry < (select WorkingDate from t_Last where OurBranchID = f.OurBranchID)