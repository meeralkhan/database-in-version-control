﻿
CREATE   VIEW [dbo].[v_CalculateZXDPD]
AS
select ab.OurBranchID, ab.AccountID, DATEDIFF(DAY, ISNULL(MAX(a.AccrualDate), MAX(c.WORKINGDATE)), MAX(c.WORKINGDATE)) AS NoOfDPD
from t_Account ab
inner join t_Products b on ab.OurBranchID = b.OurBranchID and ab.ProductID = b.ProductID
and ISNULL(b.ProductType,'') = 'AE' and ISNULL(b.SegmentType,'') = 'Retail'
inner join t_Last c on ab.OurBranchID = c.OurBranchID
left join v_CalculateExcessZX a on a.OurBranchID = ab.OurBranchID and a.AccountID = ab.AccountID
group by ab.OurBranchID, ab.AccountID