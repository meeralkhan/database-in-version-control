﻿  
CREATE   VIEW [dbo].[vc_rptNarrationIDToIncomeGL]
AS             
Select a.OurBranchID, a.ScrollNo,gta.AccountID as GLAccountID, gta.Description as GLTitle,gta.CurrencyID, gt.ExchangeRate,             
CASE WHEN b.[Percent] = 0 THEN 0 ELSE (a.Amount * 100 / b.[Percent]) END AS Amount,  
CASE WHEN b.[Percent] = 0 THEN 0 ELSE (a.ForeignAmount * 100 / b.[Percent]) END AS ForeignAmount,        
td.DescriptionID as DebitDescriptionID, td.Description as DebitDesc,  
gt.DescriptionID as CreditDescriptionID,tdcr.Description as CreditDesc, g.AccountID as VATAccountID,          
g.Description as VATGLTitle, a.Amount as VATAmount,a.Date wdate,  
case isnull(ta.TrxType,'') when '' then (case gt.IsCredit when 1 then 'C' else 'D' end) else ta.TrxType end TrxType,                   
a.ChannelId, a.ChannelRefID, case isnull(ta.Description,'') when '' then gt.Description else ta.Description end Description,a.Status,a.OperatorID,  
a.SupervisorID,              
Case When ntg.CreditAccount is null Then 'Fail' Else 'Pass' End Remarks, a.ValueDate               
FROM t_GLTransactions a      
LEFT Join t_GLTransactions as gt On a.ValueDate=gt.ValueDate And a.ScrollNo=gt.ScrollNo AND isnull(gt.VatReferenceID,'') = '' AND a.Status<>'R'  
and gt.IsCredit = 1 AND substring(gt.AccountID,1,1) NOT IN (5,6) and a.Amount = Round((isnull(gt.Amount,0) / 100 * a.[Percent]),2)    
Left Join t_Transactions as ta On ta.ScrollNo=a.ScrollNo AND ta.Status<>'R' and a.VatReferenceID = ta.VatReferenceID AND isnull(ta.RefNo,'') <> 'VAT'  
and ta.TrxType = 'D' 
LEFT JOIN t_GL gta ON gt.AccountID = gta.AccountID AND gta.OurBranchID = gt.OurBranchID                 
INNER JOIN t_GL g ON a.AccountID = g.AccountID AND a.OurBranchID = g.OurBranchID                 
INNER JOIN t_VatTypes b ON a.OurBranchID = b.OurBranchID AND a.VatID = b.VatID      
INNER JOIN t_TransactionDescriptions td ON td.DescriptionID = case isnull(ta.DescriptionID,'') when '' then gt.DescriptionID else ta.DescriptionID end  
AND td.OurBranchID = a.OurBranchID               
INNER JOIN t_TransactionDescriptions tdcr ON tdcr.DescriptionID = gt.DescriptionID AND tdcr.OurBranchID = gt.OurBranchID           
left join t_NarrationsToGL ntg on ntg.OurBranchID + ntg.CreditAccount + ntg.NarrationId = td.OurBranchID + substring(gta.AccountID,1,10) + td.DescriptionID        
LEFT JOIN t_Account c ON a.TAccountid = c.AccountID AND a.OurBranchID = c.OurBranchID             
where a.IsCredit = 1 and isnull(a.VatReferenceID,'') <> '' and substring(a.AccountID,1,1) NOT IN (5,6)