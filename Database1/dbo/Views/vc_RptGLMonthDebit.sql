﻿CREATE VIEW [dbo].[vc_RptGLMonthDebit]          
AS          
 SELECT GT.OurBranchID, SUM(GT.Amount) AS MonthDebit, GT.AccountID, SUM(GT.ForeignAmount) AS MonthForeignDebit          
 FROM dbo.t_GLTransactions GT     
 INNER JOIN dbo.t_Last TL ON GT.OurBranchID = TL.OurBranchID AND    
    { fn YEAR(GT.Date)           
    } = { fn YEAR(TL.WORKINGDATE) } AND           
    { fn MONTH(GT.Date)           
    } < { fn MONTH(TL.WORKINGDATE) } AND ISNULL(GT.Status,'') <> 'R'
 GROUP BY GT.OurBranchID, GT.AccountID, CONVERT(char(1), GT.IsCredit)    
 HAVING (CONVERT(char(1), GT.IsCredit) = '0')