﻿CREATE VIEW [dbo].[vc_TOD]      
AS        
 SELECT A.OurBranchID, T.AccountID, MAX(T.wDate) AS TODDate, AB.ClearBalance, AB.Name AS AccountName,         
     AB.ProductID, C.Name AS ClientName, P.CurrencyID, MAX(T.wDate) AS LastTransactionDate,         
     A.ClientID, AB.Limit      
          
 FROM t_Transactions T       
 RIGHT OUTER JOIN t_AccountBalance AB ON  T.OurBranchID = AB.OurBranchID AND T.AccountID = AB.AccountID       
 AND T.ProductID = AB.ProductID       
 LEFT OUTER JOIN t_Account A       
  LEFT OUTER JOIN  t_Products P ON A.OurBranchID = P.OurBranchID AND A.ProductID = P.ProductID       
 LEFT OUTER JOIN  t_Customer C ON A.OurBranchID = C.OurBranchID AND A.ClientID = C.ClientID ON AB.OurBranchID = A.OurBranchID       
 AND AB.AccountID = A.AccountID       
 /* RIGHT OUTER JOIN BALANCESUPERVISION BS ON AB.AccountID = BS.AccountID */      
       
 /* WHERE (BS.PClearBalance >= 0) AND (BS.CClearBalance < 0)  */      
 GROUP BY T.AccountID, AB.ClearBalance, AB.Name, AB.ProductID, C.Name, P.CurrencyID, A.ClientID, AB.Limit,       
 AB.Effects, A.OurBranchID      
 HAVING (AB.ClearBalance + AB.Effects < 0) AND (AB.Limit = 0)