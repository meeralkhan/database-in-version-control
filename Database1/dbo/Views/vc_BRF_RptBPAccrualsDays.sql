﻿CREATE VIEW [dbo].[vc_BRF_RptBPAccrualsDays] AS 
Select a.AccrualDate, a.OurBranchID, a.AccountID, a.DealID, a.AccountName, a.ProductID, a.CurrencyID, Sum(a.Amount) Amount,
Case When a.CurrencyID = 'AED' then Sum(a.Amount) else
Sum(a.Amount)*(Select Top 1 MeanRate from t_Currencies_History WITH (NOLOCK) Where OurBranchID = a.OurBranchID AND ChangeDate <= a.accrualdate AND CurrencyID = a.CurrencyID Order By ChangeDate desc) End AmountInAED from t_RptBPAccruals a WITH (NOLOCK)
Group By a.AccrualDate, a.OurBranchID, a.AccountID, a.DealID, a.AccountName, a.ProductID, a.CurrencyID