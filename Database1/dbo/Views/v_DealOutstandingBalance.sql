﻿create    view [dbo].[v_DealOutstandingBalance]  
as  
select a.OurBranchID, b.ClientID, a.AccountID, b.Name, a.DealID, a.ReferenceNo DealRefNo, b.ProductID, b.CurrencyID,  
ISNULL(SUM(case b.CurrencyID when 'AED' then (case c.TrxType when 'C' then c.Amount else c.Amount*-1 end)  
else (case c.TrxType when 'C' then c.ForeignAmount else c.ForeignAmount*-1 end) end), 0) as Balance  
from t_Disbursement a  
inner join t_Account b on a.OurBranchID = b.OurBranchID and a.AccountID = b.AccountID  
inner join t_Transactions c on a.OurBranchID = c.OurBranchID and a.AccountID = c.AccountID  
and c.RefNo = 'ADV-'+a.AccountID+'-'+CONVERT(varchar(30), a.DealID)  
where ISNULL(a.IsClosed,0) = 0  
group by a.OurBranchID, b.ClientID, a.AccountID, b.Name, a.DealID, a.ReferenceNo, b.ProductID, b.CurrencyID