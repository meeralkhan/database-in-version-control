﻿      
CREATE view [dbo].[v_LoanSchedule]      
as      
select a.OurBranchID,ReferenceNo,a.Status,b.ClientID,b.ProductID,d.Description,c.DueDate RepaymentDate,c.PrincipalAmount,c.ToDateProfit,c.InstAmount from t_Disbursement a      
inner join t_Account b on a.OurBranchID = b.OurBranchID and a.AccountID = b.AccountID      
inner join t_Adv_PaymentSchedule c on c.DealID = a.DealID and a.AccountID = c.AccountID and a.OurBranchID = c.OurBranchID       
inner join t_Products d on b.OurBranchID = d.OurBranchID and b.ProductID = d.ProductID