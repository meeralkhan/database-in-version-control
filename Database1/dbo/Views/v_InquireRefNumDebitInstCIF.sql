﻿create view [dbo].[v_InquireRefNumDebitInstCIF]
as
select a.ClientID,b.OurBranchID,b.AccountID,b.ReferenceNo,a.ProductID,b.FacilityID,d.AccountLimitID, '001' RefType
from t_Account a
inner join t_Disbursement b on a.AccountID = b.AccountID and a.OurBranchID = b.OurBranchID
inner join t_LinkFacilityAccount d on b.AccountID = d.AccountID and b.OurBranchID = d.OurBranchID