﻿
CREATE   VIEW [dbo].[v_OverdraftAccrualDelta]
as
select AccrualDate, OurBranchID, AccountID, ProductID, CurrencyID, SUM(Amount) TodayAccrual,
case when DAY(AccrualDate) = 1 then 0 else ISNULL(LAG(SUM(Amount)) OVER (PARTITION BY AccountID order by AccrualDate),0) end AS YesterdayAccrual,
SUM(Amount) - case when DAY(AccrualDate) = 1 then 0 else ISNULL(LAG(SUM(Amount)) OVER (PARTITION BY AccountID order by AccrualDate),0) end AS AccrualDelta
from t_RptODAccruals
group by AccrualDate, OurBranchID, AccountID, ProductID, CurrencyID