﻿CREATE VIEW [dbo].[vc_BRF_TDRIssuanceHistory] AS SELECT c.ClientID, a.Name AccountName, f.Amount ClearBalance, a.CurrencyID, isnull(c.GroupCode,'-') GroupCode, c.CategoryId, Cast(DateDiff(day,f.wDate,f.MaturityDate) as Varchar) DaysToMaturity, 
Case When isnull(c.Gender,'-') = '' then '-' When isnull(c.Gender,'-') = '-' then '-' Else c.Gender End Gender, 
isnull(c.JurEmiratesID,'-') JurEmiratesID, isnull(j.JurEmiratesDesc,'-') JurEmiratesDesc, isnull(c.CountryofIncorporationCorp,'-') CountryofIncorporationCorp, 
isnull(c.StateCurr,'-') StateCurr, isnull(st.StateName,'-') StateName, isnull(c.Nationality,'-') Nationality,
ISNULL(CASE WHEN CategoryId = 'Individual' AND c.ResidentNonResident = 'Y' THEN 'Y'	 WHEN CategoryId = 'Individual' AND c.ResidentNonResident = 'N' THEN 'N'
	 WHEN CategoryId <> 'Individual' AND c.JurEmiratesID <> '008' THEN 'Y'		 WHEN CategoryId <> 'Individual' AND c.JurEmiratesID = '008' THEN 'N'
	 WHEN c.ResidentNonResident IS NULL AND c.JurEmiratesID IS NULL then '-'
END,'-') Resident, n.NatureID, CASE WHEN c.CategoryId = 'Individual' THEN c.Nationality  WHEN c.CategoryId <> 'Individual' THEN c.CountryofIncorporationCorp END AS Nation, f.*  
FROM t_TDRIssuance t WITH (NOLOCK) 
INNER JOIN t_Account a WITH (NOLOCK) ON a.AccountID = t.AccountID AND a.OurBranchID = t.OurBranchID
INNER JOIN t_Customer c WITH (NOLOCK) ON a.ClientID = c.ClientID AND a.OurBranchID = c.OurBranchID 
INNER JOIN t_FDBalanceHistory f WITH (NOLOCK) On f.OurBranchID = t.OurBranchID AND f.AccountID = t.AccountID AND f.RecieptID = t.RecieptID
LEFT JOIN t_JurEmirates j WITH (NOLOCK) on j.OurBranchID = c.OurBranchID AND j.JurEmiratesID = c.JurEmiratesID
LEFT JOIN t_AccountNature n WITH (NOLOCK) on n.OurBranchID = a.OurBranchID AND a.NatureID = n.NatureID
LEFT JOIN t_State st WITH (NOLOCK) on st.OurBranchID = c.OurBranchID AND st.StateID = c.StateCurr AND st.CountryID = 'ARE'