﻿ CREATE view [dbo].[vc_GetTrxToUpdTrxDates]  
AS    
    
 select OurBranchID, ScrollNo, '1' SerialNo, AccountID, CurrencyID, AccountType, ValueDate, wDate, DescriptionID, 
 Amount, TrxType, ForeignAmount, ExchangeRate, IsLocalCurrency
 from t_CashTransactionModel    
 where Supervision = 'C'  AND AccountType = 'C'
 
 UNION ALL    
 
 select OurBranchID, ScrollNo, SerialNo, AccountID, CurrencyID, AccountType, ValueDate, wDate, DescriptionID, 
 Amount, TrxType, ForeignAmount, ExchangeRate, IsLocalCurrency
 from t_TransferTransactionModel    
 where Supervision = 'C' AND AccountType = 'C'    
 
 UNION ALL    
 
 select OurBranchID, ScrollNo, SerialNo, AccountID, CurrencyID, AccountType, ValueDate, wDate, DescriptionID, 
 Amount, TrxType, ForeignAmount, ExchangeRate, IsLocalCurrency
 from t_AllModelTransaction    
 where Supervision = 'C' AND AccountType = 'C'    
 
 UNION ALL    
 
 select OurBranchID, '1' as ScrollNo, SerialNo, AccountID, CurrencyID, AccountType, ValueDate, wDate, DescriptionID, 
 Amount, TransactionType TrxType, ForeignAmount, ExchangeRate, 1 IsLocalCurrency
 from t_InWardClearing    
 where ISNULL(Status, '') = '' AND AccountType = 'C' AND Status <>'R'
 
 UNION ALL    
 
 select OurBranchID, ScrollNo, SerialNo, AccountID, CurrencyID, AccountType, ValueDate, wDate, DescriptionID, 
 Amount, TrxType, ForeignAmount, ExchangeRate, IsLocalCurrency
 from t_OnLineCashTransaction    
 where Supervision = 'C' AND AccountType = 'C'    
 
 UNION ALL
 
 SELECT OurBranchID, ScrollNo, SerialNo, AccountID, CurrencyID, AccountType, ValueDate, wDate, DescriptionID,
 Amount, TrxType, ForeignAmount, ExchangeRate, IsLocalCurrency
 FROM t_InternetTransaction            
 WHERE AccountType = 'C'