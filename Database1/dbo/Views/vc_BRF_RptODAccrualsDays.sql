﻿CREATE VIEW [dbo].[vc_BRF_RptODAccrualsDays] AS 
Select a.Accrualdate, a.OurBranchID, a.AccountID, a.DealID, a.AccountName, a.ProductID, a.CurrencyID, Sum(a.Amount) Amount
, Case When a.CurrencyID = 'AED' then Sum(a.Amount) ELSE 
Sum(a.Amount)*(Select Top 1 MeanRate from t_Currencies_History WITH (NOLOCK) Where OurBranchID = a.OurBranchID AND ChangeDate <= a.AccrualDate 
AND CurrencyID = a.CurrencyID Order By ChangeDate desc) End AmountInAED  
from t_RptBPAccruals a
Group By a.Accrualdate, a.OurBranchID, a.AccountID, a.DealID, a.AccountName, a.ProductID, a.CurrencyID