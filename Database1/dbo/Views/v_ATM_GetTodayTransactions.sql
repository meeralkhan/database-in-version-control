﻿CREATE VIEW [dbo].[v_ATM_GetTodayTransactions]      
AS      
 SELECT AC.OurBranchID, AC.AccountID, AC.wDate, AC.ValueDate, AC.Amount, AC.TrxType, AC.ProcCode, 'AT' as DocumentType  
 FROM t_ATM_CashTransaction AC  
 WHERE (AC.Supervision = 'C') and (AC.TrxType = 'D')  
    
union ALL      
    
 SELECT AC.OurBranchID, AC.AccountID, AC.wDate, AC.ValueDate, AC.Amount, AC.TrxType, AC.ProcCode, 'AT' as DocumentType  
 FROM t_ATM_CashTransaction AC  
 WHERE (AC.Supervision = 'C') and (AC.TrxType = 'C')      
    
union ALL      
    
 SELECT AT.OurBranchID, AT.AccountID, AT.wDate, AT.ValueDate, AT.Amount, AT.TrxType, AT.ProcCode, 'AT' as DocumentType  
 FROM t_ATM_TransferTransaction AT  
 WHERE (AT.Supervision = 'C') and (AT.TrxType = 'D')      
    
union ALL      
    
 SELECT AT.OurBranchID, AT.AccountID, AT.wDate, AT.ValueDate, AT.Amount, AT.TrxType, AT.ProcCode, 'AT' as DocumentType  
 FROM t_ATM_TransferTransaction AT  
 WHERE (AT.Supervision = 'C') and (AT.TrxType = 'C')      
    
union ALL      
    
 SELECT C.OurBranchID, C.AccountID, C.wDate, C.ValueDate, C.Amount, C.TrxType, '' as ProcCode, 'C' as DocumentType  
 FROM t_CashTransactionModel C  
 WHERE (C.Supervision = 'C')  and (C.TrxType = 'D')  
    
union ALL      
    
 SELECT C.OurBranchID, C.AccountID, C.wDate, C.ValueDate, C.Amount, C.TrxType, '' as ProcCode, 'C' as DocumentType  
 FROM t_CashTransactionModel C  
 WHERE (C.Supervision = 'C')  and (C.TrxType = 'C')  
    
union ALL      
    
 SELECT T.OurBranchID, T.AccountID, T.wDate, T.ValueDate, T.Amount, T.TrxType, '' as ProcCode, 'T' as DocumentType  
 FROM t_TransferTransactionModel T  
 WHERE (T.Supervision = 'C') and (T.TrxType = 'D')      
    
union ALL      
    
 SELECT T.OurBranchID, T.AccountID, T.wDate, T.ValueDate, T.Amount, T.TrxType, '' as ProcCode, 'T' as DocumentType  
 FROM t_TransferTransactionModel T  
 WHERE (T.Supervision = 'C') and (T.TrxType = 'C')      
    
union ALL      
    
 SELECT I.OurBranchID, I.AccountID, I.wDate, I.ValueDate, I.Amount, 'D' as TrxType, '' as ProcCode, 'ID' as DocumentType  
 FROM t_InwardClearing I  
 WHERE (I.Status = '')      
    
union ALL      
    
 SELECT AM.OurBranchID, AM.AccountID, AM.wDate, AM.ValueDate, AM.Amount, 'C' as TrxType, '' as ProcCode, 'OC' as DocumentType  
 FROM t_AllModelTransaction AM