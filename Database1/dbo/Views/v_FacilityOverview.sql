﻿CREATE view [dbo].[v_FacilityOverview]                
as                              
select a.OurBranchID,a.ClientID,a.ReferenceNo FacilityID,a.DateReview,a.DateExpiry,a.LimitSecType,a.LimitType,'AED' as CurrencyID,isnull(a.Limit,0) ApprovedLimit,      
sum(isnull(b.Limit,0)) UtilizedLimit,a.Limit - sum(isnull(b.Limit,0)) AvailableLimit,count(distinct P.FinanceProductType) NoOfLoanProducts,c.CreditProgramID,c.CreditProgramName,      
case a.Status when 'C' then 'Closed' when 'B' then 'Blocked' when '' then 'Active' end Status       
from t_LinkCIFCollateral a                
inner join t_FacilityProduct b on a.OurBranchID = b.OurBranchID and a.ClientID = b.ClientID and a.SerialNo = b.FacilityID         
INNER JOIN t_Products P on a.OurBranchID = p.OurBranchID and b.ProductID = p.ProductID      
left join t_CreditPrograms c on c.OurBranchID = a.OurBranchID and a.CreditProgramID = c.CreditProgramID      
group by a.OurBranchID,a.ClientID,a.ReferenceNo,a.DateReview,a.DateExpiry,a.LimitSecType,a.LimitType,
a.Limit,c.CreditProgramID,c.CreditProgramName,a.Status