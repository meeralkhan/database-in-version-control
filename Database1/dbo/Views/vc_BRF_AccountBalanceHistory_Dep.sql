﻿CREATE VIEW [dbo].[vc_BRF_AccountBalanceHistory_Dep] AS SELECT Distinct ab.wDate, ab.OurBranchID, c.ClientID, c.Name ClientName, ab.AccountID, ab.ProductID, p.ProductType, ab.AccountName, a.CurrencyID,
ClearBalance = CASE WHEN a.CurrencyID = 'AED' THEN ab.ClearBalance ELSE ab.LocalClearBalance END, isnull(c.GroupCode,'-') GroupCode, isnull(c.Gender,'-') Gender, 
isnull(c.JurEmiratesID,'-') JurEmiratesID, isnull(j.JurEmiratesDesc,'-') JurEmiratesDesc, isnull(c.CountryofIncorporationCorp,'-') CountryofIncorporationCorp, 
isnull(c.StateCurr,'-') StateCurr, isnull(s.StateName,'-') StateName,  
isnull(c.Nationality,'-') Nationality, c.CategoryId, p.IsTermDeposit, 'No' IsSavingDeposit,
Case When a.ProductID IN ('CCALAED','CCALEUR','CCALGBP','CCALJPY','CCALSAR','CCALUSD') then 'Yes' else 'No' END IsCallDeposit,
CASE WHEN CategoryId = 'Individual' AND c.ResidentNonResident = 'Y' THEN 'Y'	 WHEN CategoryId = 'Individual' AND c.ResidentNonResident = 'N' THEN 'N'
	 WHEN CategoryId <> 'Individual' AND c.JurEmiratesID <> '008' THEN 'Y'	 WHEN CategoryId <> 'Individual' AND c.JurEmiratesID = '008' THEN 'N'
	 WHEN c.ResidentNonResident IS NULL AND c.JurEmiratesID IS NULL then '-'
	 ELSE '-' 		
END As Resident, CASE WHEN c.JurTypeID = '001' THEN 'Mainland' WHEN c.JurTypeID = '002' THEN 'Freezones' Else '-' END JurType, ISNULL(c.GroupID,'-') GroupID, ISNULL(g.GroupName,'-') GroupName, 
CASE WHEN p.IsTermDeposit = 'Yes' THEN (Select ISNULL(AVG(Rate),0) from t_TDRIssuance Where AccountID = a.AccountID AND IsClosed = 0) 
	 ELSE ISNULL((SELECT TOP 1 MonthRate FROM t_DepositRates WHERE Type = 'A' AND ProductID = ab.ProductID AND ab.ClearBalance BETWEEN MinAmount AND MaxAmount ORDER BY EffectiveDate DESC),0) 
END AS DepRate,
CASE WHEN c.CategoryId = 'Individual' THEN c.Nationality  WHEN c.CategoryId <> 'Individual' THEN c.CountryofIncorporationCorp END AS Nation, con.Name NationName, ClearBalance CCYBalance, n.NatureID    
FROM t_AccountBalanceHistory ab  WITH (NOLOCK) 
INNER JOIN t_Account a  WITH (NOLOCK) ON a.AccountID = ab.AccountID AND a.OurBranchID = ab.OurBranchID
INNER JOIN t_Customer c WITH (NOLOCK) ON c.OurBranchID = a.OurBranchID AND c.ClientID = a.ClientID
INNER JOIN t_Products p WITH (NOLOCK) ON p.OurBranchID = a.OurBranchID AND p.ProductID = a.ProductID
LEFT JOIN t_TDRIssuance t WITH (NOLOCK) ON a.AccountID = t.AccountID AND a.OurBranchID = t.OurBranchID
LEFT JOIN t_JurEmirates j WITH (NOLOCK) on j.OurBranchID = c.OurBranchID AND j.JurEmiratesID = c.JurEmiratesID
LEFT JOIN t_AccountNature n WITH (NOLOCK) on n.OurBranchID = a.OurBranchID AND a.NatureID = n.NatureID
LEFT JOIN t_State s WITH (NOLOCK) on s.OurBranchID = c.OurBranchID AND s.StateID = c.StateCurr AND s.CountryID = 'ARE'
LEFT JOIN t_ManageGroups g WITH (NOLOCK) on g.OurBranchID = c.OurBranchID and g.GroupID = c.GroupID 
LEFT JOIN t_Country con WITH (NOLOCK) on c.OurBranchID = con.OurBranchID 
AND CASE WHEN c.CategoryId = 'Individual' THEN c.Nationality WHEN c.CategoryId <> 'Individual' THEN c.CountryofIncorporationCorp END = con.CountryID
where p.ProductType IN ('L','AE') and ab.ClearBalance > 0;