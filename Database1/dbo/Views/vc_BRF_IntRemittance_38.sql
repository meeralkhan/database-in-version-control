﻿CREATE VIEW [dbo].[vc_BRF_IntRemittance_38] AS SELECT DateOfTrx wDate, IsNull(AmountInAED,0) Amount, (Case When InwardOutward='OUTWARD' then 'D' else 'C' End) TrxType, Txn_ID TrxID,
(CASE 
WHEN c.GroupCode IN ('INDIV','HNIND') AND s.TrxPurpose IN ('AES','ALW','BON','COP','CRP','DCP','EDU','EMI','EOS','FAM','LAS','MWI','MWO','MWP','OVT','PEN','RNT','SAA','SAL','SLA','SLL','STR','TKT','UTL') THEN 'Family'
WHEN s.TrxPurpose IN ('CEA','CEL','CIN','DIV','DLA','DLF','DLL','DOE','DSA','DSF','DSL','FDA','FDL','FIA','FIL','FSA','FSL','IGD','IOD','IPO','ISH','ISL','ISS','LDL','LDS','PIN','PIP','POR','PPA','PPL','PRR','RDA','RDL','REA','REL') THEN 'Investment'
WHEN c.GroupCode IN ('INDIV','HNIND') AND s.TrxPurpose NOT IN ('AES','ALW','BON','COP','CRP','DCP','EDU','EMI','EOS','FAM','LAS','MWI','MWO','MWP','OVT','PEN','RNT','SAA','SAL','SLA','SLL','STR','TKT','UTL','CEA','CEL','CIN','DIV','DLA','DLF','DLL','DOE','DSA','DSF','DSL','FDA','FDL','FIA','FIL','FSA','FSL','IGD','IOD','IPO','ISH','ISL','ISS','LDL','LDS','PIN','PIP','POR','PPA','PPL','PRR','RDA','RDL','REA','REL') THEN 'Other'
WHEN c.GroupCode NOT IN ('INDIV','HNIND') AND s.TrxPurpose IN ('GDE','GDI','GDS','TTS') THEN 'Trade'
WHEN c.GroupCode NOT IN ('INDIV','HNIND') AND s.TrxPurpose NOT IN ('GDE','GDI','GDS','TTS','CEA','CEL','CIN','DIV','DLA','DLF','DLL','DOE','DSA','DSF','DSL','FDA','FDL','FIA','FIL','FSA','FSL','IGD','IOD','IPO','ISH','ISL','ISS','LDL','LDS','PIN','PIP','POR','PPA','PPL','PRR','RDA','RDL','REA','REL') THEN 'Other'
END) AS TrxPurpose,TrxPurpose TransPurpose, Country CountryID,  c.ClientID, s.ToAccountNumber AccountID, t.Name, t.ProductID, c.GroupCode, s.ChannelRefID 
from SwiftTransfers s WITH (NOLOCK)  
INNER JOIN t_Transactions ts WITH (NOLOCK) ON s.ChannelRefID = ts.ChannelRefID AND ChannelID = '9' AND DescriptionID in ('100040','200040') 
LEFT JOIN t_Account t WITH (NOLOCK)  ON s.ToAccountNumber = t.AccountID
LEFT JOIN t_Customer c WITH (NOLOCK)  ON c.OurBranchID = t.OurBranchID AND c.ClientID = t.ClientID;