﻿Create View [dbo].[vc_LR_GetCancelLostRecord]
As    
  
 SELECT a.OurBranchId, a.SerialNo, a.ScrollNo, a.InstrumentType, a.InstrumentNo, a.ControlNo, b.BankID, b.FullName as BankName,   
 c.BranchID, c.BranchName, a.PayeeName, a.BeneficiaryName, a.Amount, a.wDate,   
 Mark = Case    
  When isnull(a.Supervision,'') = 'R' then 'Rejected'  
  When isnull(a.Paid,'0') = 1 then 'Paid'     
  when isnull(a.Cancel,'0') = 1 then 'Cancel'  
  when isnull(a.Lost,'0') = 1 and isnull(a.Cancel,'0') = 0 then 'Lost'    
  when isnull(a.Duplicate,'0') = 1 and isnull(a.Cancel,'0') = 0 then 'Duplicate'    
  when isnull(a.[stop],'0') = 1 and isnull(a.Lost,'0') = 0 and isnull(a.Cancel,'0') = 0 and isnull(a.Duplicate,'0') = 0 and isnull(a.Paid,'0') = 0 then 'Stop'  
  When isnull(a.[Stop],'0')= 0 then 'OutStanding'  
 End,    
 MarkDate = Case     
  when isnull(a.Paid,'0') = 1 then PaidDate   
  when isnull(a.Cancel,'0') = 1 then CancelDate    
  when isnull(a.Lost,'0') = 1 and isnull(a.Cancel,'0') = 0 then LostDate  
  when isnull(a.Duplicate,'0') = 1 and isnull(a.Cancel,'0') = 0 then DuplicateDate    
  when isnull(a.[stop],'0') = 1 and isnull(a.Lost,'0') = 0 and isnull(a.Cancel,'0') = 0 and isnull(a.Duplicate,'0') = 0 and isnull(a.Paid,'0') = 0 then StopDate    
  When isnull(a.[Stop],'0')= 0 then wDate  
 End,   
 a.CreateBy, a.OldInstrumentNo   
 FROM t_LR_Issuance a  
 INNER JOIN t_LR_Banks b ON a.OurBranchID = b.OurBranchID AND a.DrawnBankID = b.BankID    
 INNER JOIN t_LR_Branches c ON a.OurBranchID=c.OurBranchID AND b.BankID = c.BankID AND a.DrawnBranchID = c.BranchID