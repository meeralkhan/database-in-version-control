﻿create view [dbo].[v_SupplierInvDetails]
AS
select a.OurBranchID, a.ClientID, c.Name, a.CreditProgramID, b.CreditProgramName, d.CollateralID, e.ReferenceNo AS CollateralRefID, d.CollateralValue, d.AfterDeductingMargin, d.ExpiryDate AS InvoiceExpiryDate
from t_LinkCIFCollateral a
inner join t_CreditPrograms b on a.OurBranchID = b.OurBranchID and a.CreditProgramID = b.CreditProgramID and a.LimitSecType = 'SCF File' and a.Status = ''
inner join t_Customer c on a.OurBranchID = c.OurBranchID and a.ClientID = c.ClientID
inner join t_FacilityCollateral d on a.OurBranchID = d.OurBranchID and a.ClientID = d.ClientID and a.SerialNo = d.FacilityID
inner join t_Collaterals e on d.OurBranchID = e.OurBranchID and d.ClientID = e.ClientID and d.CollateralID = e.CollateralID
union all
select a.OurBranchID, a.ClientID, c.Name, a.CreditProgramID, b.CreditProgramName, d.CollateralID, e.ReferenceNo AS CollateralRefID, d.CollateralValue, d.AfterDeductingMargin, d.ExpiryDate AS InvoiceExpiryDate
from t_LinkCIFCollateral a
inner join t_CreditPrograms b on a.OurBranchID = b.OurBranchID and a.CreditProgramID = b.CreditProgramID and a.LimitSecType = 'SCF File' and a.Status = ''
inner join t_Customer c on a.OurBranchID = c.OurBranchID and a.ClientID = c.ClientID
inner join t_ExpiredCollaterals d on a.OurBranchID = d.OurBranchID and a.ClientID = d.ClientID and a.SerialNo = d.FacilityID and d.ExpiryDate > (select CAST(DATEADD(m, -6, WORKINGDATE) as date) from t_Last where OurBranchID = d.OurBranchID)
inner join t_Collaterals e on d.OurBranchID = e.OurBranchID and d.ClientID = e.ClientID and d.CollateralID = e.CollateralID