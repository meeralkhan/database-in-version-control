﻿
create view [dbo].[v_BalanceConfirmation]
AS
select a.OurBranchID, a.ClientID, a.AccountID AS AccountNumber, 
case p.CreditInterestProcedure when '001' then 'Current' else 'Savings' end AS AccountType,
a.CurrencyID AS Currency, ab.ClearBalance AS Balance
from t_Account a
inner join t_AccountBalance ab on a.OurBranchID = ab.OurBranchID and a.AccountID = ab.AccountID
inner join t_Products p on a.OurBranchID = p.OurBranchID and a.ProductID = p.ProductID
where --a.OurBranchID = '01'
--and a.ClientID = '000000037'
--and 
ISNULL(p.IsTermDeposit,'No') = 'No'
and ISNULL(p.ProductType,'') != 'G'
and ISNULL(p.FinanceProductType,'OD') = 'OD'