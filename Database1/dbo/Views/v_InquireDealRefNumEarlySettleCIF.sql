﻿
create   view [dbo].[v_InquireDealRefNumEarlySettleCIF]
as
select a.ClientID,cast(b.DealID as nvarchar) DealID,b.OurBranchID,b.AccountID,b.ReferenceNo,a.ProductID,b.FacilityID,d.AccountLimitID,
ABS(sum(case a.CurrencyID when 'AED' then (case t.TrxType when 'D' then t.Amount*-1 else t.Amount end) else (case t.TrxType when 'D' then t.ForeignAmount*-1 else t.ForeignAmount end) end)) principleBalance,
case when
max(b.Amount)-ABS(sum(case a.CurrencyID when 'AED' then (case t.TrxType when 'D' then t.Amount*-1 else t.Amount end) else (case t.TrxType when 'D' then t.ForeignAmount*-1 else t.ForeignAmount end) end))
<
(select SUM(PrincipalAmount) from t_Adv_PaymentSchedule c where b.AccountID = c.AccountID and b.OurBranchID = c.OurBranchID and b.DealID = c.DealID and cast(c.DueDate as Date) < e.WORKINGDATE)
then 'Y' else 'N'
end IsOverDue
from t_Disbursement b
inner join t_Account a on a.OurBranchID = b.OurBranchID and a.AccountID = b.AccountID
inner join t_Last e on e.OurBranchID= b.OurBranchID
inner join t_Transactions t on b.AccountID = t.AccountID and b.OurBranchID = t.OurBranchID and t.RefNo = 'ADV-'+a.AccountID+'-'+cast(b.DealID as nvarchar)
inner join t_LinkFacilityAccount d on b.AccountID = d.AccountID and b.OurBranchID = d.OurBranchID
where isnull(b.IsClosed,0) = 0
group by a.ClientID,b.DealID,b.OurBranchID,b.AccountID,b.ReferenceNo,a.ProductID,b.FacilityID,d.AccountLimitID, e.WORKINGDATE