﻿
CREATE   VIEW [dbo].[vc_rptVATReturnDraft]
AS                  
SELECT  a.OurBranchID, a.ValueDate ,          
 isnull(sum(case when isnull(a.VatID,'') in ('003','004','005') then case isnull(gt.Amount,0) when 0 then ta.Amount else gt.Amount end else 0 end), 0) StandardRatedAmount,
 isnull(sum(case when isnull(a.VatID,'') in ('003','004','005') then a.Amount else 0 end), 0) StandardWHTaxAmount,                 
 isnull(sum(case when isnull(b.VatID,'') = '001' then a.Amount else 0 end), 0) ExemptedAmount,                  
isnull(sum(case when isnull(b.VatID,'') = '002' then a.Amount else 0 end), 0) ZeroRatedAmount               
FROM t_GLTransactions a      
LEFT Join t_GLTransactions as gt On a.ValueDate=gt.ValueDate And a.ScrollNo=gt.ScrollNo AND isnull(gt.VatReferenceID,'') = ''   
AND a.Status<>'R' and gt.IsCredit = 1 AND substring(gt.AccountID,1,1) NOT IN (5,6) and a.Amount = Round((isnull(gt.Amount,0) / 100 * a.[Percent]),2)    
Left Join t_Transactions as ta On ta.ScrollNo=a.ScrollNo AND ta.Status<>'R' and a.VatReferenceID = ta.VatReferenceID AND isnull(ta.RefNo,'') <> 'VAT'  
and ta.TrxType = 'D' 
LEFT JOIN t_GL gta ON gt.AccountID = gta.AccountID AND gta.OurBranchID = gt.OurBranchID                 
INNER JOIN t_GL g ON a.AccountID = g.AccountID AND a.OurBranchID = g.OurBranchID                 
INNER JOIN t_VatTypes b ON a.OurBranchID = b.OurBranchID AND a.VatID = b.VatID      
INNER JOIN t_TransactionDescriptions td ON td.DescriptionID = case isnull(ta.DescriptionID,'') when '' then gt.DescriptionID else ta.DescriptionID end    
AND td.OurBranchID = a.OurBranchID                     
LEFT JOIN t_Account c ON a.TAccountid = c.AccountID AND a.OurBranchID = c.OurBranchID             
where a.IsCredit = 1 and isnull(a.VatReferenceID,'') <> ''  and substring(a.AccountID,1,1) NOT IN (5,6)
GROUP BY a.OurBranchID, a.ValueDate