﻿CREATE   VIEW [dbo].[vc_rptTransactionalNarrations]
AS
-- SELECT * FROM vc_rptTransactionalNarrations WHERE OurBranchID = '01'
-- order by DescriptionID
select a.OurBranchID,a.DescriptionID,a.Description,case when a.IsCredit = 1 then 'Credit' else 'Debit' end Type,
case when a.IsVATApplicable = 1 then 'Yes' else 'No' end IsVATApplicable,
case  
 when ISNULL(b.[Percent], 987654321.7869) = 987654321.7869 then 1
 else 0
end as CasePercent, b.[Percent],
case  
 when ISNULL(a.VatID, 'NULL') = 'NULL' then '-'
 else a.VatID
end as VatID,
case  
 when ISNULL(b.Description, 'NULL') = 'NULL' then '-'
 else b.Description
end as VATDescription,
case  
 when ISNULL(b.AccountID, 'NULL') = 'NULL' then '-'
 else b.AccountID
end as AccountID,  
case  
 when ISNULL(g.Description, 'NULL') = 'NULL' then '-'
 else g.Description
end as AccountDesc ,
case when a.IsEnable = 1 then 'Yes' else 'No' end IsEnable
from t_TransactionDescriptions a
left join t_VatTypes b on a.OurBranchID = b.OurBranchID and a.VatID = b.VatID
left join t_GL g on b.AccountID = g.AccountID and g.OurBranchID = b.OurBranchID