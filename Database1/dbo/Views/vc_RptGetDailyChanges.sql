﻿CREATE VIEW [dbo].[vc_RptGetDailyChanges]      
AS        
      
 SELECT t_AccountBalance.OurBranchID, t_AccountBalance.AccountID,   
 (t_AccountBalance.ClearBalance + t_AccountBalance.Effects) As DailyChange,      
 /** - (t_PreviousDayBalances.ClearBalance + t_PreviousDayBalances.Effects) AS DailyChange,   **/      
 (t_AccountBalance.LocalClearBalance + t_AccountBalance.LocalEffects)  AS DailyLocalChange      
 /** - (t_PreviousDayBalances.LocalClearBalance + t_PreviousDayBalances.LocalEffects) AS DailyLocalChange,      
 t_PreviousDayBalances.ClearBalance + t_PreviousDayBalances.Effects AS PreviousBalance,      
 t_PreviousDayBalances.LocalClearBalance + t_PreviousDayBalances.LocalEffects        
 AS PreviousLocalBalance  **/      
 FROM t_AccountBalance