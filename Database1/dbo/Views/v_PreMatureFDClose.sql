﻿create view [dbo].[v_PreMatureFDClose]  
AS  
select OurBranchID,RecieptID,ProductID,AccountID,NAccountType,NAccount,NProductID,Amount,ExchangeRate,IssueDate,StartDate,MaturityDate,Rate,AutoProfit,PaymentPeriod,Treatment,AutoRollover,OptionsAtMaturity,IsClosed,CloseDate  
from t_TDRIssuance  
where ISNULL(IsClosed,0)=1  
and ISNULL(CloseDate,MaturityDate) < MaturityDate