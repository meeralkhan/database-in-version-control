﻿
CREATE VIEW [dbo].[rptUserRights]  
AS  
-- rptUserRights  
SELECT OurBranchID,OperatorID, Program, CreditPostingLimit, CreditSupervisionLimit, DebitSupervisionLimit,   
CASE AddAccess WHEN 1 THEN 'TRUE' WHEN 0 THEN 'FALSE' ELSE 'FALSE' END AS AddAccessVal,  
CASE EditAccess WHEN 1 THEN 'TRUE' WHEN 0 THEN 'FALSE' ELSE 'FALSE' END AS EditAccessVal,  
CASE DeleteAccess WHEN 1 THEN 'TRUE' WHEN 0 THEN 'FALSE' ELSE 'FALSE' END AS DeleteAccessVal,  
CASE VerifyAccess WHEN 1 THEN 'TRUE' WHEN 0 THEN 'FALSE' ELSE 'FALSE' END AS VerifyAccessVal,   
CASE AuthorizeAccess WHEN 1 THEN 'TRUE' WHEN 0 THEN 'FALSE' ELSE 'FALSE' END AS AuthorizeAccessVal  
FROM t_MapDrive