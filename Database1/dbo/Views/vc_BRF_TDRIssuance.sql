﻿CREATE VIEW [dbo].[vc_BRF_TDRIssuance] AS SELECT a.CurrencyID, c.GroupCode, c.CategoryId, 
CASE WHEN CategoryId = 'Individual' AND ResidentNonResident = 'Y' THEN 'Y'
	 WHEN CategoryId = 'Individual' AND ResidentNonResident = 'N' THEN 'N'
	 WHEN CategoryId <> 'Individual' AND JurEmiratesID <> '008' THEN 'Y'
	 WHEN CategoryId <> 'Individual' AND JurEmiratesID = '008' THEN 'N'
	 ELSE '-' 
END	As Resident, CASE WHEN c.JurTypeID = '001' THEN 'Mainland' WHEN c.JurTypeID = '002' THEN 'Freezones' Else '-' END JurType, t.*  
FROM t_TDRIssuance t WITH (NOLOCK) 
INNER JOIN t_Account a WITH (NOLOCK) ON a.AccountID = t.AccountID AND a.OurBranchID = t.OurBranchID
INNER JOIN t_Customer c WITH (NOLOCK) ON a.ClientID = c.ClientID AND a.OurBranchID = c.OurBranchID ;      -- TDRIssuance