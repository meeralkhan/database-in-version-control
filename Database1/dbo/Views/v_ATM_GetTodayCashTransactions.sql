﻿CREATE VIEW [dbo].[v_ATM_GetTodayCashTransactions]    
AS    
SELECT t_ATM_CashTransaction.AccountID,     
    t_ATM_CashTransaction.wDate,     
    t_ATM_CashTransaction.ValueDate,     
    t_ATM_CashTransaction.TrxType, 'v' AS ChequeID,    
    t_ATM_CashTransaction.DescriptionID,     
    t_ATM_CashTransaction.Description,     
    t_ATM_CashTransaction.Amount,     
    t_ATM_CashTransaction.ForeignAmount,     
    t_Account.Name AS AccountName, t_Account.Address,     
    t_ATM_CashTransaction.CurrencyID,     
    t_Currencies.Description AS CurrencyName,     
    t_ATM_CashTransaction.OurBranchID,     
    t_ATM_CashTransaction.ScrollNo,     
    t_ATM_CashTransaction.ScrollNo AS SerialNo    
FROM t_ATM_CashTransaction INNER JOIN    
    t_Account ON     
    t_ATM_CashTransaction.OurBranchID = t_Account.OurBranchID AND    
     t_ATM_CashTransaction.AccountID = t_Account.AccountID INNER    
     JOIN    
    t_Currencies ON     
    t_ATM_CashTransaction.OurBranchID = t_Currencies.OurBranchID    
     AND     
    t_ATM_CashTransaction.CurrencyID = t_Currencies.CurrencyID    
WHERE Supervision <> 'R'