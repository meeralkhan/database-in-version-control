﻿
--ABOVE THRESHOLD TRXS 
CREATE   VIEW [dbo].[ATM_vc_AboveThresholdTrxsBaseIIvsBaseIMismatch]
AS 
-- Query = string.Format(@"
 --SELECT * FROM ATM_vc_AboveThresholdTrxsBaseIIvsBaseIMismatch
 --WHERE OurBranchID = '02'
 --and convert(varchar(10), TwDate, 120) >= '2021-08-04' AND convert(varchar(10), TwDate, 120) <= '2021-08-08'
 --order by RwDate asc
SELECT a.wDate AS RwDate, b.wDate AS TwDate, a.OurBranchID, a.AccountID, Left(a.RefNo, 16) as PAN, a.STAN, a.RetRefNum, a.VISATrxID, a.RefNo, b.wDate as TrxBaseIDate, a.wDate as TrxBaseIIDate, b.Amount as BaseIAmount, 
a.Amount as BaseIIAmount, b.ConvRate, b.AcqCountryCode, b.ForwardInstID, 
b.CurrCodeTran, b.CurrCodeSett,  
Case 
	when LEFT(b.ProcCode,2) = '00' AND LEFT(b.POSEntryMode,2) = '01' then 'ECOM Trx'
	when LEFT(b.ProcCode,2) = '00' then 'POS Trx'
	when LEFT(b.ProcCode,2) = '01' then 'Cash Withdrawal Trx'
	when LEFT(b.ProcCode,2) = '31' then 'Balance Enquiry Trx'
	when LEFT(b.ProcCode,2) = '23' then 'Credit Trx'
	when LEFT(b.ProcCode,2) = '26' then 'Online Credit Trx'
	when LEFT(b.ProcCode,2) = '20' then 'Credit Vouch/ Merch Return Trx'
	when b.ProcCode = 'CANCL CHG' then 'Card Replacement [CANCELLED]'
	when b.ProcCode = 'LT CHG' then 'Card Replacement [LOST]'
	when b.ProcCode = 'STLN CHG' then 'Card Replacement [STOLEN]'
	when b.ProcCode = 'DMG CHG' then 'Card Replacement [DAMAGED]'
	when b.ProcCode = 'LT DELVRY CHG' then 'Card Replacement [LOST IN DELIVERY]'
	when b.ProcCode = 'REP CHG' then 'Card Replacement [NORMAL]'
end as TranDetails, b.DescriptionID, b.[Description], b.CardAccptNameLoc
FROM t_RejectedBaseII a 
LEFT JOIN t_ATM_HoldTrxs b ON a.OurBranchID = b.OurBranchID AND a.AccountID = b.AccountID AND a.RefNo = b.RefNo
WHERE Isnull(a.ProcCode,'') NOT IN ('CANCL CHG', 'LT CHG', 'STLN CHG', 'DMG CHG', 'LT DELVRY CHG', 'REP CHG')