﻿create   view [dbo].[vc_rptDisabledGLReport]
as
select d.OurBranchID, d.AccountID, d.Description, g.AccountType, d.CurrencyID, isnull(g.Balance+g.OpeningBalance, 0) as Balance,
isnull(g.ForeignBalance+g.ForeignOpeningBalance, 0) as ForeignBalance, d.wDate as DisabledDate, d.IsPosting, d.wDate, 
case 
	when MAX(ISNULL(gt.date, '2050-01-01')) = '2050-01-01' then MAX(ISNULL(gt.date, '1900-01-01'))
	else max(gt.Date)
	end as LastTrxTime, 
case 
	when ISNULL(glo.OwnerName, 'NULL') = 'NULL' then '-'
	else glo.OwnerName
	end as OwnerName,  
case 
	when ISNULL(gd.DeptName, 'NULL') = 'NULL' then '-'
	else gd.DeptName
	end as Department, 
case 
	when ISNULL(isnull(ucc.CreateBy, g.CreateBy), 'NULL') = 'NULL' then '-'
	else isnull(ucc.CreateBy, g.CreateBy)
	end as Maker, 
case 
	when ISNULL(isnull(ucu.UpdateBy, g.UpdateBy), 'NULL') = 'NULL' then '-'
	else isnull(ucu.UpdateBy, g.UpdateBy)
	end as Checker
from t_DisabledGLS d inner join t_GL as g
on d.AccountID = g.AccountID and d.OurBranchID = g.OurBranchID
left join t_GLTransactions as gt
on d.AccountID = gt.AccountID and d.OurBranchID = gt.OurBranchID
left join t_GLOwners as glo
on g.GLOwnerID = glo.OwnerID and g.OurBranchID = glo.OurBranchID
left join t_Departments as gd
on glo.OwnerDeptID = gd.DeptID and glo.OurBranchID = gd.OurBranchID
left join t_UserChannels as ucc
on ucc.ChannelID = g.CreateBy and ucc.OurBranchID = g.OurBranchID
left join t_UserChannels as ucu
on ucu.ChannelID = g.UpdateBy and ucu.OurBranchID = g.OurBranchID
group by d.OurBranchID, d.AccountID, d.Description, g.AccountType, d.CurrencyID, g.Balance+g.OpeningBalance,
g.ForeignBalance+g.ForeignOpeningBalance, d.wDate, glo.OwnerName,
gd.DeptName, d.IsPosting, ucc.CreateBy, g.CreateBy, ucu.UpdateBy, g.UpdateBy