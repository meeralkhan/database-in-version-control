﻿
create view [dbo].[v_ExpiredCollaterals2]
AS
select fc.OurBranchID, fc.ClientID, fc.FacilityID, fc.CollateralID, fc.Description, fc.ReviewDate, fc.ExpiryDate, fc.CollateralValue, fc.AfterDeductingMargin
from t_LinkCIFCollateral f
inner join t_FacilityCollateral fc on f.OurBranchID = fc.OurBranchID and f.ClientID = fc.ClientID and f.SerialNo = fc.FacilityID
where f.LimitSecType != 'SCF File'
and fc.ExpiryDate < (select WorkingDate from t_Last where OurBranchID = fc.OurBranchID)