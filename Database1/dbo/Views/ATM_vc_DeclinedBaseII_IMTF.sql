﻿
--DECLINE BASE II (IMTF)
CREATE   VIEW [dbo].[ATM_vc_DeclinedBaseII_IMTF]
AS
-- Query = string.Format(@"
 --SELECT * FROM ATM_vc_DeclinedBaseII_IMTF
 --WHERE OurBranchID = '02' AND convert(varchar(10), TwDate, 120) >= '2021-08-04' AND 
 --convert(varchar(10), TwDate, 120) <= '2021-08-08' order by RwDate asc
SELECT a.wDate AS RwDate, b.wDate AS TwDate, a.OurBranchID, a.AccountID, Left(a.RefNo, 16) as PAN, a.STAN, a.RetRefNum, a.VISATrxID, a.RefNo, b.wDate as TrxDate, 
a.Amount, a.ConvRate, a.MerchantType, a.AcqCountryCode, a.ForwardInstID, a.CurrCodeTran, a.CurrCodeSett, a.SettlmntAmount, 
Case 
	when LEFT(a.ProcCode,2) = '00' AND LEFT(a.POSEntryMode,2) = '01' then 'ECOM Trx'
	when LEFT(a.ProcCode,2) = '00' then 'POS Trx'
	when LEFT(a.ProcCode,2) = '01' then 'Cash Withdrawal Trx'
	when LEFT(a.ProcCode,2) = '31' then 'Balance Enquiry Trx'
	when LEFT(a.ProcCode,2) = '23' then 'Credit Trx'
	when LEFT(a.ProcCode,2) = '26' then 'Online Credit Trx'
	when LEFT(a.ProcCode,2) = '20' then 'Credit Vouch/ Merch Return Trx'
	when a.ProcCode = 'CANCL CHG' then 'Card Replacement [CANCELLED]'
	when a.ProcCode = 'LT CHG' then 'Card Replacement [LOST]'
	when a.ProcCode = 'STLN CHG' then 'Card Replacement [STOLEN]'
	when a.ProcCode = 'DMG CHG' then 'Card Replacement [DAMAGED]'
	when a.ProcCode = 'LT DELVRY CHG' then 'Card Replacement [LOST IN DELIVERY]'
	when a.ProcCode = 'REP CHG' then 'Card Replacement [NORMAL]'
end as TranDetails, a.DescriptionID, a.[Description], a.CardAccptNameLoc, a.RejectReasonCode, a.RejectReasonDescription, a.TrxDetails
FROM t_RejectedHostTrxs a 
LEFT JOIN t_ATM_HoldTrxs b ON a.OurBranchID = b.OurBranchID AND a.AccountID = b.AccountID AND a.RefNo = b.RefNo
WHERE isnull(a.ProcCode,'') <> '' AND a.TrxDetails IN ('IMTF Declines II')