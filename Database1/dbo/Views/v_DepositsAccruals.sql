﻿create view [dbo].[v_DepositsAccruals]
as
select * from t_AccrualHistory
where [Date] IN (select MAX([Date]) from t_AccrualHistory)