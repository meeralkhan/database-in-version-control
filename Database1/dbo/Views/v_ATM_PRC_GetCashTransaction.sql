﻿CREATE VIEW [dbo].[v_ATM_PRC_GetCashTransaction]    
AS    
 SELECT a.*, b.GLControl    
 FROM t_ATM_CashTransaction a, t_Products b    
 WHERE a.ProductID = b.ProductID