﻿create view [dbo].[v_GetTodaysATMTrxReport]  
AS  
  
SELECT OurBranchID, MerchantType, STAN, CustomerBranchID, CustomerBranchID as WithdrawlBranchID, ScrollNo, SerialNo,  RefNo, ATMID,   
AccountID, AccountName, AccountType, ProductID, CurrencyID, ValueDate, wDate, TrxType, Supervision, Amount, ForeignAmount,   
ExchangeRate, DescriptionID, Description, Remarks , OperatorID, SupervisorID,   
Case left(ProcCode,2)  
 when '01' then   
  Case ltrim(substring(ProcCode,4,20))  
   WHEN '' THEN   
   Case TrxType  
    WHEN 'D' THEN 'Cash-With'  
    WHEN 'C' THEN 'Cash-Dep'  
   END  
   ELSE ltrim(substring(ProcCode,4,20))  
  END  
 when '85' then   
  Case ltrim(substring(ProcCode,4,20))  
   WHEN '' THEN  
   Case TrxType  
    WHEN 'D' THEN 'Int.Br.Cash-Withd'  
    WHEN 'C' THEN 'Int.Br.Cash-Dep'  
   END  
   ELSE ltrim(substring(ProcCode,4,20))  
  END  
 when '31' then   
  Case ltrim(substring(ProcCode,4,20))  
   WHEN '' THEN   
   Case TrxType  
    WHEN 'D' THEN 'Bal.Enq.Chg'  
    WHEN 'C' THEN 'Bal.Enq.Chg'  
   END  
   ELSE ltrim(substring(ProcCode,4,20))  
  END  
 when '30' then   
  Case ltrim(substring(ProcCode,4,20))  
   WHEN '' THEN   
   Case TrxType  
    WHEN 'D' THEN 'Bal.Enq.Chg'  
    WHEN 'C' THEN 'Bal.Enq.Chg'  
   END  
   ELSE ltrim(substring(ProcCode,4,20))  
  END  
 when '40' then   
  Case ltrim(substring(ProcCode,4,20))  
   WHEN '' THEN  
   Case TrxType  
    WHEN 'D' THEN 'Int.Br.Fund.Trf.(Db)'  
    WHEN 'C' THEN 'Int.Br.Fund.Trf.(Cr)'  
   END  
   ELSE ltrim(substring(ProcCode,4,20))  
  END  
 when '41' then   
  Case ltrim(substring(ProcCode,4,20))  
   WHEN '' THEN   
   Case TrxType  
    WHEN 'D' THEN 'Int.Bk.Fund.Trf.(Db)'  
    WHEN 'C' THEN 'Int.Bk.Fund.Trf.(Cr)'  
   END  
   ELSE ltrim(substring(ProcCode,4,20))  
  END  
 when '48' then   
  Case ltrim(substring(ProcCode,4,20))  
   WHEN '' THEN   
   Case TrxType  
    WHEN 'D' THEN 'Int.Bk.Fund.Trf.(Db)'  
    WHEN 'C' THEN 'Int.Bk.Fund.Trf.(Cr)'  
   END  
   ELSE ltrim(substring(ProcCode,4,20))  
  END    
 when '53' then   
  Case ltrim(substring(ProcCode,4,20))  
   WHEN '' THEN   
   Case TrxType  
    WHEN 'D' THEN 'Mini.Stat.(Db)'  
    WHEN 'C' THEN 'Mini.Stat.(Cr)'  
   END  
   ELSE ltrim(substring(ProcCode,4,20))  
  END      
 when '73' then   
  Case ltrim(substring(ProcCode,4,20))  
   WHEN '' THEN   
   Case TrxType  
    WHEN 'D' THEN 'Bill-Payment(Db)'  
    WHEN 'C' THEN 'Bill-Payment(Cr)'  
   END  
   ELSE ltrim(substring(ProcCode,4,20))  
  END      
 Else '' end as ProcCode  
FROM t_ATM_CashTransaction   
  
UNION ALL   
  
SELECT OurBranchID, MerchantType, STAN, OurBranchID as CustomerBranchID, WithdrawlBranchID, ScrollNo,  SerialNo, RefNo, ATMID, AccountID,   
AccountName, AccountType, ProductID, CurrencyID,  ValueDate, wDate, TrxType, Supervision, Amount, ForeignAmount, ExchangeRate,  
DescriptionID, Description, Remarks, OperatorID, SupervisorID,   
Case left(ProcCode,2)  
 when '01' then   
  Case ltrim(substring(ProcCode,4,20))  
   WHEN '' THEN   
   Case TrxType  
    WHEN 'D' THEN 'Cash-With'  
    WHEN 'C' THEN 'Cash-Dep'  
   END  
   ELSE ltrim(substring(ProcCode,4,20))  
  END  
 when '85' then   
  Case ltrim(substring(ProcCode,4,20))  
   WHEN '' THEN  
   Case TrxType  
    WHEN 'D' THEN 'Int.Br.Cash-Withd'  
    WHEN 'C' THEN 'Int.Br.Cash-Dep'  
   END  
   ELSE ltrim(substring(ProcCode,4,20))  
  END  
 when '31' then   
  Case ltrim(substring(ProcCode,4,20))  
   WHEN '' THEN   
   Case TrxType  
    WHEN 'D' THEN 'Bal.Enq.Chg'  
    WHEN 'C' THEN 'Bal.Enq.Chg'  
   END  
   ELSE ltrim(substring(ProcCode,4,20))  
  END  
 when '30' then   
  Case ltrim(substring(ProcCode,4,20))  
   WHEN '' THEN   
   Case TrxType  
    WHEN 'D' THEN 'Bal.Enq.Chg'  
    WHEN 'C' THEN 'Bal.Enq.Chg'  
   END  
   ELSE ltrim(substring(ProcCode,4,20))  
  END  
 when '40' then   
  Case ltrim(substring(ProcCode,4,20))  
   WHEN '' THEN  
   Case TrxType  
    WHEN 'D' THEN 'Int.Br.Fund.Trf.(Db)'  
    WHEN 'C' THEN 'Int.Br.Fund.Trf.(Cr)'  
   END  
   ELSE ltrim(substring(ProcCode,4,20))  
  END  
 when '41' then   
  Case ltrim(substring(ProcCode,4,20))  
   WHEN '' THEN   
   Case TrxType  
    WHEN 'D' THEN 'Int.Bk.Fund.Trf.(Db)'  
    WHEN 'C' THEN 'Int.Bk.Fund.Trf.(Cr)'  
   END  
   ELSE ltrim(substring(ProcCode,4,20))  
  END  
 when '48' then   
  Case ltrim(substring(ProcCode,4,20))  
   WHEN '' THEN   
   Case TrxType  
    WHEN 'D' THEN 'Int.Bk.Fund.Trf.(Db)'  
    WHEN 'C' THEN 'Int.Bk.Fund.Trf.(Cr)'  
   END  
   ELSE ltrim(substring(ProcCode,4,20))  
  END    
 when '53' then   
  Case ltrim(substring(ProcCode,4,20))  
   WHEN '' THEN   
   Case TrxType  
    WHEN 'D' THEN 'Mini.Stat.(Db)'  
    WHEN 'C' THEN 'Mini.Stat.(Cr)'  
   END  
   ELSE ltrim(substring(ProcCode,4,20))  
  END      
 when '73' then   
  Case ltrim(substring(ProcCode,4,20))  
   WHEN '' THEN   
   Case TrxType  
    WHEN 'D' THEN 'Bill-Payment(Db)'  
    WHEN 'C' THEN 'Bill-Payment(Cr)'  
   END  
   ELSE ltrim(substring(ProcCode,4,20))  
  END      
 Else '' end as ProcCode  
  
FROM t_ATM_TransferTransaction