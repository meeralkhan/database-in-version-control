﻿create view [dbo].[vc_GetBackDateOLTransferTrxRecords]          
AS          
          
 select CategoryID, CategoryName, OurBranchID, ScrollNo, SerialNo, AccountID, AccountName, ProductID, CurrencyID,           
 TrxType, wDate, ChequeID, ChequeDate, Amount, ForeignAmount, ExchangeRate, IsLocalCurrency, [Status], Supervision,     
 GlID, OperatorID, SupervisorID, DescriptionID, [Description], Remarks, AdditionalData, BranchName, SourceBranch,     
 TargetBranch          
           
 from (          
           
 select '2' As CategoryID, 'Responding' As CategoryName, a.OurBranchID, a.ScrollNo, a.SerialNo, a.AccountID,           
 a.AccountName, a.ProductID, a.CurrencyID, a.TrxType, a.wDate, a.ChequeID, a.ChequeDate, a.Amount, a.ForeignAmount,           
 a.ExchangeRate, a.IsLocalCurrency, 'C' [Status], a.[Status] Supervision, a.GlID, a.OperatorID, a.SupervisorID,           
 a.DescriptionID, a.[Description], a.Remarks, a.AdditionalData, b.BranchName, a.SourceBranch, a.TargetBranch          
 from t_Transactions a          
 LEFT JOIN t_OnlineBranches b ON a.OurBranchID = b.OurBranchID AND a.SourceBranch = b.BranchID          
 where a.DocumentType = 'OT' and a.IsMainTrx = '1' AND a.Descriptionid in ('TR1','TR0')     
           
 union all          
           
 SELECT '1' As CategoryID, 'Originating' As CategoryName, a.OurBranchID, a.ScrollNo, a.SerialNo, a.AccountID,           
 AccountName = (SELECT t_GL.[Description] FROM t_GL WHERE t_GL.OurBranchID = a.OurBranchID           
 AND t_GL.AccountID = a.AccountID), 'GL' ProductID, a.CurrencyID,           
 CASE a.IsCredit WHEN 1 THEN 'C' ELSE 'D' END TrxType, a.[Date] wDate, 'V' ChequeID, '1900-01-01' ChequeDate,     
 a.Amount, a.ForeignAmount, a.ExchangeRate,          
 CASE a.CurrencyID           
  WHEN (SELECT t_GlobalVariables.LocalCurrency FROM t_GlobalVariables WHERE t_GlobalVariables.OurBranchID = a.OurBranchID) THEN 1          
  ELSE 0           
 END IsLocalCurrency, 'C' [Status], a.[Status] Supervision, a.GlID, a.OperatorID, a.SupervisorID, a.DescriptionID,     
 a.[Description], a.Remarks, a.AdditionalData, b.BranchName, a.SourceBranch, a.TargetBranch          
 FROM t_GLTransactions a          
 LEFT JOIN t_OnlineBranches b ON a.OurBranchId = b.OurBranchId AND a.TargetBranch = b.BranchID                    
 WHERE a.DocType = 'OT' and a.IsMainTrx = '1' AND a.Descriptionid in ('TR1','TR0')    
           
 ) a --order by ScrollNo