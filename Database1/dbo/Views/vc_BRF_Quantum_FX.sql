﻿CREATE VIEW [dbo].[vc_BRF_Quantum_FX] AS SELECT d.*, Case When d.Country = 'United Arab Emirates' OR d.CountryCode in ('UAE','AE') then 'Y' ELSE 'N' END Resident, 
Case When d.BUYCCY <> 'AED' then d.BUYAMOUNT*(Select c.BuyingRate from t_Currencies c WITH (NOLOCK) Where d.BUYCCY = c.CurrencyID AND OurBranchID = '01')  else BUYAMOUNT END BUYAMOUNTAED, 
Case When d.SELLCCY <> 'AED' then d.SELLAMOUNT*(Select c.SellingRate from t_Currencies c WITH (NOLOCK) Where d.SELLCCY = c.CurrencyID AND OurBranchID = '01') else SELLAMOUNT END SELLAMOUNTAED  
FROM Quantum_BRF_FX d WITH (NOLOCK);