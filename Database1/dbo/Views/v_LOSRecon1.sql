﻿create view [dbo].[v_LOSRecon1]
AS
select f.ClientID, f.ReferenceNo, p.ProductID, a.Limit, f.DateExpiry, a.AccountID
from t_LinkCIFCollateral f
inner join t_FacilityProduct p ON f.OurBranchID = p.OurBranchID and f.SerialNo = p.FacilityID
inner join t_LinkFacilityAccount a ON a.OurBranchID = f.OurBranchID and a.ClientID = f.ClientID and a.FacilityID = f.SerialNo
inner join t_Account ac ON a.OurBranchID = ac.OurBranchID and a.AccountID = ac.AccountID and p.ProductID = ac.ProductID