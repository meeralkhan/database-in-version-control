﻿
CREATE   VIEW [dbo].[ATM_vc_ATMLocationOfTransactionsWithAuth]
AS
-- SELECT * FROM ATM_vc_ATMLocationOfTransactionsWithAuth
-- WHERE OurBranchID = '02'
-- AND convert(varchar(10), wDate, 120) >= '2021-03-01' AND convert(varchar(10), wDate, 120) <= '2021-03-31' 
-- order by wDate asc
SELECT wDate, OurBranchID, AccountID, Left(RefNo, 16) as PAN, STAN, RetRefNum, VISATrxID, RefNo, wDate as TrxDate, ReleaseDate as ClearingDate, 
Amount, ConvRate, AcqCountryCode, ForwardInstID, CurrCodeTran, CurrCodeSett, SettlmntAmount, 
Case 
	when HoldStatus = 'R' then 'Full Reversal'
	when HoldStatus = 'H' then 'Hold'
	when HoldStatus = 'C' then 'Cleared'
	when HoldStatus = 'P' then 'Partial Reversal'
end as TrxStatus, 
Case 
	when LEFT(ProcCode,2) = '00' AND LEFT(POSEntryMode,2) = '01' then 'ECOM Trx'
	when LEFT(ProcCode,2) = '00' then 'POS Trx'
	when LEFT(ProcCode,2) = '01' then 'Cash Withdrawal Trx'
	when LEFT(ProcCode,2) = '31' then 'Balance Enquiry Trx'
	when LEFT(ProcCode,2) = '23' then 'Credit Trx'
	when LEFT(ProcCode,2) = '26' then 'Online Credit Trx'
	when LEFT(ProcCode,2) = '20' then 'Credit Vouch/ Merch Return Trx'
	when ProcCode = 'CANCL CHG' then 'Card Replacement [CANCELLED]'
	when ProcCode = 'LT CHG' then 'Card Replacement [LOST]'
	when ProcCode = 'STLN CHG' then 'Card Replacement [STOLEN]'
	when ProcCode = 'DMG CHG' then 'Card Replacement [DAMAGED]'
	when ProcCode = 'LT DELVRY CHG' then 'Card Replacement [LOST IN DELIVERY]'
	when ProcCode = 'REP CHG' then 'Card Replacement [NORMAL]'
end as TranDetails, CardAccptNameLoc, DescriptionID, [Description]
FROM t_ATM_HoldTrxs