﻿CREATE VIEW [dbo].[vc_BRF_IntRemittance] AS SELECT DISTINCT s.DateOfTrx wDate, IsNull(s.AmountInAED,0) Amount, (Case When s.InwardOutward='OUTWARD' then 'D' else 'C' End) TrxType,
(Case When TrxPurpose in ('AFA','AFL','ALW','BON','CHC','COP','CRP','DCP','EDU','EMI','EOS','FAM','LAS','LNC','MWI','MWO','MWP','OVT','PEN','PPA','PPL','SAA','SAL') then 'Family' 
When TrxPurpose in ('GDE','GDI','GDS','TTS') then 'Trade' 
When TrxPurpose in ('CEA','CEL','CIN','DLA','DLL','DOE','DSA','DSF','DSL','FDA','FDL','FIA','FIL','FSA','FSL','IGD','IOD','IOL','IPO','ISH','ISL','ISS','PIN','PIP','POR','PRR','RDL','REL') then 'Investment' Else 'Others' END) As TrxPurpose,  
s.TrxPurpose TransPurpose, s.Country CountryID, c.ClientID, s.ToAccountNumber AccountID, t.Name, t.ProductID, c.GroupCode,
JurType = CASE WHEN c.JurTypeID = '001' THEN 'Mainland' WHEN c.JurTypeID = '002' THEN 'Freezones' END, s.Txn_ID TrxID, s.ChannelRefID  	   
FROM SwiftTransfers s WITH (NOLOCK)
INNER JOIN t_Transactions ts WITH (NOLOCK) ON s.ChannelRefID = ts.ChannelRefID AND ChannelID = '9' AND DescriptionID in ('100040','200040')
LEFT JOIN t_Account t WITH (NOLOCK)  ON s.ToAccountNumber = t.AccountID
LEFT JOIN t_Customer c WITH (NOLOCK)  ON c.OurBranchID = t.OurBranchID AND c.ClientID = t.ClientID;