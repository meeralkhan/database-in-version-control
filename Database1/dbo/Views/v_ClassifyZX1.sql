﻿
CREATE   VIEW [dbo].[v_ClassifyZX1]
AS
select a.OurBranchID, a.AccountID, DATEDIFF(DAY, MAX(a.AccrualDate), MAX(c.WORKINGDATE)) AS Counts
from v_CalculateExcessZX a
inner join t_Account ab on a.OurBranchID = ab.OurBranchID and a.AccountID = ab.AccountID
inner join t_Products b on a.OurBranchID = b.OurBranchID and a.ProductID = b.ProductID
and ISNULL(b.ProductType,'') = 'AE' and ISNULL(b.SegmentType,'') = 'Retail'
inner join t_Last c on a.OurBranchID = c.OurBranchID
--where ISNULL(ab.OverdraftClassID, '001') not in ('004','005','006','007')
group by a.OurBranchID, a.AccountID
having DATEDIFF(DAY, MAX(a.AccrualDate), MAX(c.WORKINGDATE)) >= ISNULL(MAX(b.EA1Days), 15)