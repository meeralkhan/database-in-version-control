﻿
create   view [dbo].[v_SupplierDetails]
as
select a.OurBranchID, a.ClientID, c.Name, ISNULL(f.AccountID,'') AccountID, f.Rate1, a.CreditProgramID, b.CreditProgramName,
case when ISNULL(a.Status,'') = '' then 'A' else ISNULL(a.Status,'') end FacilityStatus,
a.Limit, ISNULL(SUM(AfterDeductingMargin),0) AS DrawingPower,  MAX(a.CreateTime) AS FacilityCreateTime,
MAX(a.UpdateTime) AS FacilityUpdateTime, MAX(b.CreateTime) AS CreditProgramCreateTime, MAX(b.UpdateTime) AS CreditProgramUpdateTime,
MAX(f.CreateTime) AS AccountLimitCreateTime, MAX(f.UpdateTime) AS AccountLimitUpdateTime, MAX(d.CreateTime) AS CollateralAttachTime,
MAX(e.CreateTime) AS CollateralCreateTime, MAX(e.UpdateTime) AS CollateralUpdateTime
from t_LinkCIFCollateral a
inner join t_CreditPrograms b on a.OurBranchID = b.OurBranchID and a.CreditProgramID = b.CreditProgramID and a.LimitSecType = 'SCF File'
inner join t_Customer c on a.OurBranchID = c.OurBranchID and a.ClientID = c.ClientID
inner join t_LinkFacilityAccount f on a.OurBranchID = f.OurBranchID and a.ClientID = f.ClientID and a.SerialNo = f.FacilityID
left join t_FacilityCollateral d on a.OurBranchID = d.OurBranchID and a.ClientID = d.ClientID and a.SerialNo = d.FacilityID
left join t_Collaterals e on d.OurBranchID = e.OurBranchID and d.ClientID = e.ClientID and d.CollateralID = e.CollateralID
group by a.OurBranchID, a.ClientID, c.Name, f.AccountID, f.Rate1, a.CreditProgramID, b.CreditProgramName, a.Status, a.Limit