﻿CREATE VIEW [dbo].[vc_RptGLBalances]        
AS          
    
 SELECT G.OurBranchID, G.AccountID, G.Description,     
 G.ForeignOpeningBalance + ISNULL(VC2.MonthForeignCredit,0) - ISNULL(VC1.MonthForeignDebit, 0) AS ForeignOpeningBalance,    
 G.ForeignBalance, G.OpeningBalance + ISNULL(VC2.MonthCredit, 0) - ISNULL(VC1.MonthDebit, 0) AS OpeningBalance,     
 G.Balance, G.AccountType, G.CurrencyID, G.AccountClass
 FROM dbo.t_GL G     
 LEFT OUTER JOIN dbo.vc_RptGLMonthDebit VC1 ON G.OurBranchID = VC1.OurBranchID AND G.AccountID = VC1.AccountID     
 LEFT OUTER JOIN dbo.vc_RptGLMonthCredit VC2 ON G.OurBranchID = VC2.OurBranchID AND G.AccountID = VC2.AccountID