﻿
create view [dbo].[v_VATException]
AS
select a.OurBranchID, a.ScrollNo, a.SerialNo, a.ChannelId, a.ChannelRefID, a.AccountID, a.CurrencyID,
a.Date, a.ValueDate, a.TrxTimeStamp, a.ForeignAmount, a.Amount, a.ExchangeRate, a.DescriptionID,
(select top 1 VatID from t_GLTransactions g where g.OurBranchID = a.OurBranchID and g.ValueDate = a.ValueDate and g.VoucherID = a.VoucherID and g.AccountID IN (select AccountID from t_VatTypes where AccountID IS NOT NULL)) AS VATID
from t_GLTransactions a
where SUBSTRING(a.AccountID,1,1) = '3'
and a.ChannelId NOT IN ('','0')
and a.ChannelRefID IS NOT NULL