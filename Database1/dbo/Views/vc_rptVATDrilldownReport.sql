﻿
CREATE    VIEW [dbo].[vc_rptVATDrilldownReport]
AS                        
SELECT a.OurBranchID, a.ScrollNo, gta.AccountID as GLAccountID, gta.Description as GLTitle, c.AccountID, c.Name, d.ClientID,  
d.Name AS ClientName,  c.ProductID, td.DescriptionID as NarrationID,      
td.Description as NarrationDesc,a.Date wDate, a.ValueDate, a.TrxTimeStamp,a.CurrencyID, a.ExchangeRate,  
case isnull(gt.Amount,0) when 0 then ta.Amount else gt.Amount end AS Amount,                  
case isnull(gt.ForeignAmount,0) when 0 then ta.ForeignAmount else gt.ForeignAmount end AS ForeignAmount, a.Amount as VATAmount,  
b.AccountID as VATAccountID,vg.Description VATGLTitle,      
case a.IsCredit   when 0 then 'D' else 'C'   end  AS TrxType,  
case isnull(gt.Description,'') when '' then   ta.Description else gt.Description end AS Description, a.Status,a.OperatorID,a.SupervisorID      
FROM t_GLTransactions a      
LEFT Join t_GLTransactions as gt On a.ValueDate=gt.ValueDate And a.ScrollNo=gt.ScrollNo AND isnull(gt.VatReferenceID,'') = ''  
AND a.Status<>'R' and gt.IsCredit = 1 AND substring(gt.AccountID,1,1) NOT IN (5,6) and a.Amount = Round((isnull(gt.Amount,0) / 100 * a.[Percent]),2)    
Left Join t_Transactions as ta On ta.ScrollNo=a.ScrollNo AND ta.Status<>'R' and a.VatReferenceID = ta.VatReferenceID AND isnull(ta.RefNo,'') <> 'VAT'   
and ta.TrxType = 'D' 
LEFT JOIN t_GL gta ON gt.AccountID = gta.AccountID AND gta.OurBranchID = gt.OurBranchID                 
INNER JOIN t_GL g ON a.AccountID = g.AccountID AND a.OurBranchID = g.OurBranchID                 
INNER JOIN t_VatTypes b ON a.OurBranchID = b.OurBranchID AND a.VatID = b.VatID      
inner Join t_GL as vg On b.OurBranchID=vg.OurBranchID And b.AccountID=vg.AccountID            
INNER JOIN t_TransactionDescriptions td ON td.DescriptionID = case isnull(ta.DescriptionID,'') when '' then gt.DescriptionID else ta.DescriptionID end   
AND td.OurBranchID = a.OurBranchID                     
LEFT JOIN t_Account c ON a.TAccountid = c.AccountID AND a.OurBranchID = c.OurBranchID             
left join t_Customer d on c.OurBranchID = d.OurBranchID and c.ClientID = d.ClientID           
where a.IsCredit = 1 and isnull(a.VatReferenceID,'') <> ''  and substring(a.AccountID,1,1) NOT IN (5,6)