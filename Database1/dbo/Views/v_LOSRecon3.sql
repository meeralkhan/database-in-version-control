﻿
CREATE   VIEW [dbo].[v_LOSRecon3]
AS
select * from (
   
   select ISNULL(f.UpdateTime,f.CreateTime) AS TrxTimeStamp, f.ClientID, CreditProgramName AS JurisRefNo, f.CreditProgramID AS FacilityID,
   'NotionCap' AS ProductCode, ISNULL(f.CreditProgramLimit,0) AS FedLimit, f.CreditProgramExpiryDate AS ExpiryDate
   from t_CreditPrograms f
   
   UNION ALL
   
   select ISNULL(f.ActionTime, ISNULL(f.UpdateTime,f.CreateTime)) AS TrxTimeStamp, f.ClientID, GroupID AS JurisRefNo, f.ReferenceNo + p.ProductID AS FacilityID,
   p.ProductID AS ProductCode, ISNULL(p.Limit,f.Limit) AS FedLimit, f.DateExpiry AS ExpiryDate
   from t_FacilityProduct p
   inner join t_LinkCIFCollateral f on f.OurBranchID = p.OurBranchID and f.ClientID = p.ClientID and f.SerialNo = p.FacilityID
   where left(f.GroupID,6) = 'JURIS/'
  
) a