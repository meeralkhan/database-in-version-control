﻿create view [dbo].[v_ODClassifiedAccounts]
AS
select a.OurBranchID, a.AccountID, a.Name, a.ClientID, a.ProductID, a.CurrencyID, a.OverdraftClassID, o.Classification, o.SubClassification, o.Description
from t_Account a
inner join t_OverdraftClassifications o on a.OurBranchID = o.OurBranchID and a.OverdraftClassID = o.OverdraftClassID