﻿create   view [dbo].[vc_GLBalances]    
AS    
select OurBranchID, AccountID, PrintID, CategoryID, Description, AccountClass, AccountType, CurrencyID, IsPosting, IsContigent,   
OpeningBalance, Balance, ForeignOpeningBalance, ForeignBalance, TemporaryBalance, Thru, Notes, ShadowBalance,   
AvgBalProcessing, RevaluationProcess, IsReconcile    
from t_GL