﻿create   view [dbo].[vc_AccountBalances]
AS
select a.OurBranchID, a.AccountID, a.ClientID, a.Name, a.ProductID, a.CurrencyID, ISNULL(a.Status,'') AS Status,
a.OpenDate, ISNULL(a.ActivationDate,a.OpenDate) AS ActivationDate, a.CloseDate, a.AccessFunds, a.ExceedAmount,
a.AccessFundsAccountID, a.ShortOfFunds, a.ShortageAmount, a.ShortageAccountID, a.Accrual, a.AccrualUpto, a.tAccrual,
a.LastPayDate, a.LastDebitTransaction, a.LastCreditTransaction, a.LastPayWDate, a.IsCharity, a.CharityAmount, a.SOCID,
ab.ClearBalance, ab.Effects, ab.Limit, ab.ShadowBalance, ab.LocalClearBalance, ab.LocalEffects, ab.IsFreezed,
ab.FreezeAmount, ab.ClearedEffects, 0 AS ExcessExec, 0 AS ShortageExec
from t_Account a
inner join t_AccountBalance ab on a.OurBranchID = ab.OurBranchID and a.AccountID = ab.AccountID
where ISNULL(a.Status,'') NOT IN ('I','C')