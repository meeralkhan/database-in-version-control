﻿  
create   view [dbo].[v_ChargeList]    
AS    
    
select c.DebitNarrationID,c.DebitNarration,c.CreditNarrationID,c.CreditNarration,
c.OurBranchID, c.ChargeId, c.ChargeSerial, c.Description, c.Type, c.Amount, p.SerialID, p.Description AS GLParDesc, g.AccountID,
g.Description AS GLName, g.CurrencyID, g.AccountClass, g.AccountType    
from t_Charges c    
left join t_GLParameters p on c.OurBranchID = p.OurBranchID and c.GLSerialID = p.SerialID    
left join t_GL g on p.OurBranchID = g.OurBranchID and p.AccountID = g.AccountID