﻿create view [dbo].[vc_BalanceSheet]  
as  
  
select GL.OurBranchID,BS.Description as BSDescription, BS.Description as BSAddition, BSS.Description as BSSDescription, count(BSGL.HeadGLID) as Notes,   
sum(GL.balance) as Balance,  
CASE BS.Description  
 WHEN 'Assets' THEN 'ASSETS'   
 WHEN 'Equity' THEN 'EQUITY'   
 WHEN 'Liability' THEN 'LIABILITIES'   
 ELSE 'OTHERS'  
END AS Type,  
CASE BS.Description  
 WHEN 'Assets' THEN abs(sum(GL.balance))  
 else sum(GL.balance)  
END AS AbsBalance,  
CASE BS.Description  
 WHEN 'Assets' THEN 0  
 else sum(GL.balance)  
END AS AEBalance  
from t_ManageBalanceSheetGroup BS  
inner join t_ManageBalanceSheetSubGroup BSS on BS.GroupID = BSS.GroupID  
inner join t_LinkBSGLHead BSGL on BSS.SubGroupID = BSGL.SubGroupID  
inner join t_GL GL on GL.AccountID = BSGL.HeadGLID  
group by BS.Description, BSS.Description, GL.OurBranchID