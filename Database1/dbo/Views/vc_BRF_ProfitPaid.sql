﻿CREATE VIEW [dbo].[vc_BRF_ProfitPaid] AS 
SELECT c.OurBranchID, c.ClientID, a.AccountID, a.Name, p.RecieptID, p.ProductID, p.Amount Balance, p.Rate, p.Days, a.CurrencyID, p.SubTotal Amount, 
Case When a.CurrencyID = 'AED' then p.SubTotal else 
p.SubTotal*(Select Top 1 MeanRate from t_Currencies_History WITH (NOLOCK) Where OurBranchID = a.OurBranchID AND ChangeDate <= p.date AND CurrencyID = a.CurrencyID Order By ChangeDate desc) End  AmountInAED
,p.date CreateTime, ISNULL(c.GroupCode,'-') GroupCode,
CASE WHEN c.CategoryId = 'Individual' AND c.ResidentNonResident = 'Y' THEN 'Y'
	 WHEN c.CategoryId = 'Individual' AND c.ResidentNonResident = 'N' THEN 'N'
	 WHEN c.CategoryId <> 'Individual' AND c.JurEmiratesID <> '008' THEN 'Y'
	 WHEN c.CategoryId <> 'Individual' AND c.JurEmiratesID = '008' THEN 'N'
	 ELSE '-'
END	AS Resident
FROM t_ProfitPayHistory p WITH (NOLOCK)
inner join t_TDRIssuance t WITH (NOLOCK) on p.RecieptID = t.RecieptID and p.OurBranchID = t.OurBranchID
inner join t_Account a WITH (NOLOCK) on a.OurBranchID = t.OurBranchID and a.AccountID = t.AccountID
inner join t_Customer c WITH (NOLOCK) on c.ClientID = a.ClientID and c.OurBranchID = a.OurBranchID
UNION ALL
SELECT c.OurBRanchID, c.ClientID, a.AccountID, a.Name, '-' RecieptID, r.ProductID, r.Balance, r.Rate, r.Days, r.CurrencyID, r.Amount, 
Case When a.CurrencyID = 'AED' then r.Amount else 
r.Amount*(Select Top 1 MeanRate from t_Currencies_History WITH (NOLOCK) Where OurBranchID = a.OurBranchID AND ChangeDate <= r.paydate AND CurrencyID = a.CurrencyID Order By ChangeDate desc) End  AmountInAED
, r.PayDate CreateTime, ISNULL(c.GroupCode,'-') GroupCode,
CASE WHEN c.CategoryId = 'Individual' AND c.ResidentNonResident = 'Y' THEN 'Y'
	 WHEN c.CategoryId = 'Individual' AND c.ResidentNonResident = 'N' THEN 'N'
	 WHEN c.CategoryId <> 'Individual' AND c.JurEmiratesID <> '008' THEN 'Y'
	 WHEN c.CategoryId <> 'Individual' AND c.JurEmiratesID = '008' THEN 'N'
	 ELSE '-' 
END	AS Resident
FROM t_RptSavProfit r WITH (NOLOCK) 
INNER JOIN t_Account a WITH (NOLOCK) ON r.AccountID = a.AccountID AND r.OurBranchID = a.OurBranchID
INNER JOIN t_Customer c WITH (NOLOCK) ON a.ClientID = c.ClientID AND a.OurBranchID = c.OurBranchID
Where r.Amount <> 0;