﻿CREATE   VIEW [dbo].[vc_rptVATExceptionReport2]
AS
SELECT a.Date, b.Description as AccountName, a.OurBranchID, a.SerialNo, a.AccountID, a.ScrollNo, a.ChannelRefID,
a.CurrencyID, a.ExchangeRate, a.DescriptionID, a.Amount, a.ForeignAmount, a.OperatorID, a.SupervisorID
FROM v_VATException2 a inner join t_GL b on a.OurBranchID = b.OurBranchID and a.AccountID = b.AccountID