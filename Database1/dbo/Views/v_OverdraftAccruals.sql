﻿create view [dbo].[v_OverdraftAccruals]
as
select * from t_RptODAccruals
where AccrualDate IN (select MAX(AccrualDate) from t_RptODAccruals)