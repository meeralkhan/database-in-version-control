﻿CREATE VIEW [dbo].[vc_DepositIssuance]       
      
AS       
SELECT DISTINCT       
 t_TDRIssuance.AccountID, t_TDRIssuance.OurBranchID,       
 CAST(PeriodsInFigure as nvarchar(10)) + t_Products.Period as Period, t_Account.Name,       
 t_TDRIssuance.RecieptID,       
 t_TDRIssuance.Amount, t_TDRIssuance.Rate,       
 t_TDRIssuance.StartDate, t_TDRIssuance.MaturityDate,       
 t_Products.Description, '1900-01-01' as CloseDate,       
 0 as Interest,       
 0 as InterestPaid,       
 0 as TaxAmount, t_TDRIssuance.IssueDate,       
 0 as IsPrincipalPaid, t_Products.CurrencyID       
 --Status ????       
       
FROM t_TDRIssuance INNER JOIN       
 t_Products ON       
 t_TDRIssuance.OurBranchID = t_Products.OurBranchID AND       
 t_TDRIssuance.ProductID = t_Products.ProductID   
 INNER JOIN       
 t_Account ON   
 t_TDRIssuance.OurBranchID = t_Account.OurBranchID AND   
 t_TDRIssuance.AccountID = t_Account.AccountID