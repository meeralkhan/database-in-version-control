﻿CREATE VIEW [dbo].[v_MMSecuritySaleSelect]  
AS  
SELECT a.DealNo, a.Amount, ISNULL(b.S_Amount, 0) AS S_Amount, --IsNull(b.IsPledge,0) as IsPledge,
a.SecurityIssue, a.Amount - ISNULL(b.S_Amount, 0) AS Balance, a.AccountID, a.DealDescription, a.TypeOfTransaction, a.MaturityDate,
a.TransactionID, a.InterestRate, a.Settlementdate, a.InterestAmount, a.DealDate, a.Status, a.Price1, a.Capital, a.CapInt, a.IsPostForwardDeal,
a.PremiumAmount, a.InterestPaid, a.Price2, a.CouponRate, a.IsPremiumPaid, a.OurBranchID, a.IsPostedGL

FROM t_MoneyMarketNEW a
LEFT JOIN v_MMSecuritySaleAmount b ON a.DealNo = b.P_DealNo

WHERE a.ParentDealType = 'OP' and a.TypeOfTransaction <> 'PL'