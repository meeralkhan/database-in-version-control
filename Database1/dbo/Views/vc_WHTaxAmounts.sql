﻿create view [dbo].[vc_WHTaxAmounts]  
  
AS  
  
select OurBranchID, ScrollNo, RefNo, AccountID, AccountName, wDate, ValueDate, ChequeID, ChequeDate, Amount,   
DescriptionID, Description, DocType, AppWHTax, SourceBranch, Status,   
case DocType when 'C' then WHTaxAmount else Amount end AS WHTaxAmount, WHTaxMode, WHTScrollNo,  
case RTRIM(DescriptionID) when 'WH1' then 'Y' else 'N' end AS T1,  
case RTRIM(DocType) + RTRIM(WHTaxMode) when 'CC' then 'Y' else 'N' end AS T2  
from t_Transactions  
where Status <> 'R' and (AppWHTax = 1 OR DescriptionID = 'WH1')  
  
union all  
  
select OurBranchID, ScrollNo, RefNo, AccountID, AccountName, wDate, ValueDate, ChequeID, ChequeDate, Amount,   
DescriptionID, Description, DocType, AppWHTax, SourceBranch, Status, WHTaxAmount, WHTaxMode, WHTScrollNo,  
'Y' AS T1, 'Y' AS T2  
from t_Transactions  
where Status <> 'R' and AppWHTax = 1 and DocType = 'OC' and WHTaxMode = 'C'