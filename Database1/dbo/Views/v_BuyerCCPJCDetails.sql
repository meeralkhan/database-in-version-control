﻿CREATE   view [dbo].[v_BuyerCCPJCDetails]
AS
select a.OurBranchID,a.CreditProgramID,a.ClientID BuyerClientID,ac.Name BuyerClientName,a.CreditProgramLimit BuyerTotalLimit,
( select SUM(Limit) from t_LinkCIFCollateral where CreditProgramID = a.CreditProgramID ) BuyerOutstandingLimit, 
a.CreditProgramLimit - (select SUM(Limit) from t_LinkCIFCollateral where CreditProgramID = a.CreditProgramID) BuyerAvailableLimit,b.ClientID SuppClientID, 
bc.Name SupplierClientName,b.Limit SuppTotalLimit,
(case when d.ClearBalance > 0 then 0 else abs(d.ClearBalance) end) SuppOutstandingLimit,
b.Limit - (case when d.ClearBalance > 0 then 0 else abs(d.ClearBalance) end) SuppAvailableLimit, 
case when d.ProductID in (select ProductID from t_Products where ProductType = 'A' and IsAdvancesProduct = 'Yes') then sum(dip.InstAmount) 
else (case when d.ClearBalance > 0 then 0 else abs(d.ClearBalance) end)
end PastOutstandingAmount
from t_CreditPrograms a
inner join t_Customer ac on ac.ClientID = a.ClientID and a.OurBranchID = ac.OurBranchID
inner join t_LinkCIFCollateral b on a.OurBranchID = b.OurBranchID and a.CreditProgramID = b.CreditProgramID
inner join t_Customer bc on bc.ClientID = b.ClientID and b.OurBranchID = bc.OurBranchID
inner join t_LinkFacilityAccount c on b.SerialNo = c.FacilityID and a.OurBranchID = c.OurBranchID and b.ClientID = c.ClientID
inner join t_AccountBalance d on d.OurBranchID = a.OurBranchID and d.AccountID = c.AccountID
left join t_Disbursement di on di.OurBranchID = a.OurBranchID and di.AccountID in (select AccountID from t_Account where
ClientID  = bc.ClientID and ProductID in (select ProductID from t_Products where ProductType = 'A' and IsAdvancesProduct = 'Yes'))
left join t_Adv_PaymentSchedule dip on di.OurBranchID = dip.OurBranchID and di.DealID = dip.DealID and di.AccountID = dip.AccountID
and IsPosted <> 1 and DueDate < (select top 1 WORKINGDATE from t_Last)
group by a.OurBranchID,a.CreditProgramID,a.ClientID,a.CreditProgramLimit,b.ClientID,b.Limit,d.ClearBalance,ac.Name,bc.Name,d.ProductID