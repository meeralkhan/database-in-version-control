﻿CREATE   VIEW [dbo].[v_ClassifyOD0]
AS
select a.OurBranchID, a.AccountID, COUNT(a.OurBranchID) Counts, ISNULL(b.SSDays, 60) as [Days]
from v_CalculateExcessOD a
inner join t_Account ab on a.OurBranchID = ab.OurBranchID and a.AccountID = ab.AccountID and ISNULL(ab.DisableAutoClassification,0) = 0
inner join t_Products b on a.OurBranchID = b.OurBranchID and a.ProductID = b.ProductID and ISNULL(b.ProductType,'') = 'AE' and ISNULL(b.SegmentType,'') = 'Corporate'
where a.AccrualDate >= (select WorkingDate-(ISNULL(b.SSDays, 60)) from t_Last where OurBranchID = a.OurBranchID)
group by a.OurBranchID, a.AccountID, b.SSDays