﻿
CREATE VIEW [dbo].[vc_GLTransactionsVoucher]
AS
SELECT gt.*, g.[Description] AS AccountName, c.[Description] AS CurrencyName
FROM t_GLTransactions gt
INNER JOIN t_GL g ON gt.OurBranchID = g.OurBranchID AND gt.AccountID = g.AccountID AND gt.CurrencyID = g.CurrencyID
INNER JOIN t_Currencies c ON c.OurBranchID = gt.OurBranchID AND c.CurrencyID = gt.CurrencyID AND ISNULL(gt.Status, '') <> 'R'