﻿create view [dbo].[vc_GetQuickBalance_AllAccounts]  
AS  
SELECT a.OurBranchID, a.AccountID, a.ClientID, ISNULL(a.[Name],'') as AccountName, ISNULL(P.ProductID,'') as ProductID, ISNULL(a.CurrencyID,'') as CurrencyID,  
ISNULL(a.Status,'') AS AccountStatus, ISNULL(ab.ClearBalance,0) as ClearBalance, ISNULL(ab.Effects,0) as Effects, ISNULL(ab.Limit,0) as Limit,  
ISNULL(ab.ShadowBalance,0) as ShadowBalance, ISNULL(ab.FreezeAmount,0) as FreezeAmount, ISNULL(P.MinBalance,0) AS ProductMinBalance,  
ISNULL(ab.ClearBalance,0) - ISNULL(ab.FreezeAmount,0) - (ISNULL(P.MinBalance,0)) AS AvailableBalance,  
ISNULL(ab.ClearBalance,0) + ISNULL(ab.Effects,0) + ISNULL(ab.Limit,0) - ISNULL(ab.FreezeAmount,0) - (ISNULL(P.MinBalance,0)) AS TotalBalance,  
ISNULL(a.Reminder,'') as Reminder, ISNULL(P.ProductATMCode,'') AS ProductATMCode, ISNULL(ab.LastUpdateBalanceTime,ab.CreateTime) as LastUpdateBalanceTime,  
a.CountryID, ISNULL(a.UpdateTime,a.CreateTime) as UpdateTime  
FROM t_AccountBalance ab  
INNER JOIN t_Account a ON ab.OurBranchID = a.OurBranchID AND ab.AccountID = a.AccountID  
INNER JOIN t_Products P ON a.OurBranchID = P.OurBranchID AND a.ProductID = P.ProductID