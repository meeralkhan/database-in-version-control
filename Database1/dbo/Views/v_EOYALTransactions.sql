﻿CREATE view [dbo].[v_EOYALTransactions]   
as   
select a.*, b.OpeningBalance,b.ForeignOpeningBalance,b.Balance, b.ForeignBalance,c.RetainedEarningGL from (  
select OurBranchID, AccountID, CurrencyID,   
SUM(case IsCredit when 1 then Amount else Amount*-1 end) LCYTrxBalance, SUM(case IsCredit when 1 then   
ForeignAmount else ForeignAmount*-1 end) FCYTrxBalance from t_GLTransactions   
where  Status != 'R'   
group by OurBranchID, AccountID, CurrencyID) a   
inner join t_GL b on a.OurBranchID = b.OurBranchID and a.AccountID = b.AccountID and b.AccountType IN ('A','L')   
inner join t_Currencies c on c.OurBranchID = a.OurBranchID and c.CurrencyID = a.CurrencyID