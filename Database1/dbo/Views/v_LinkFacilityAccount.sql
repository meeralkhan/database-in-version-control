﻿create view [dbo].[v_LinkFacilityAccount]    
AS    
select f.ReferenceNo AS FacilityRef, fa.*, a.ProductID from t_LinkFacilityAccount fa    
inner join t_LinkCIFCollateral f ON fa.OurBranchID = f.OurBranchID and fa.ClientID = f.ClientID and fa.FacilityID = f.SerialNo    
inner join t_Account a ON fa.OurBranchID = a.OurBranchID and fa.AccountID = a.AccountID