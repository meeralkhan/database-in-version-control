﻿
CREATE VIEW [dbo].[vc_GetTransactionsVoucher]
AS
SELECT t.*, t.WDate As Date, c.[Description] AS CurrencyName, Substring(ExtraDetails,1,9) As VoucherId, CAST(SerialNo As numeric(9,0)) as SerialId
FROM t_Transactions t
INNER JOIN t_Account a ON t.OurBranchID = a.OurBranchID AND t.AccountID = a.AccountID
LEFT JOIN t_Products p ON a.OurBranchId = p.OurBranchId AND a.ProductID = p.ProductId
LEFT JOIN t_Currencies c ON a.OurBranchId= c.OurBranchId AND a.CurrencyId = c.CurrencyID