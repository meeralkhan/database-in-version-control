﻿create View [dbo].[vc_GetCleanCashBook]        
          
as          
     Select Register='Cash',OurBranchID,AccountID,Description,CurrencyID,ProductID,    
     DrAmount=Sum(DrAmount),CrAmount=Sum(CrAmount),NoOfDrTrx=Sum(NoDrOFTrx),NoOfCrTrx=Sum(NoCrOFTrx),    
     DrFAmount=Sum(DrFAmount),CrFAmount=Sum(CrFAmount)     
     from          
      (Select distinct C.OurBranchID,AccountID=P.GLControl,G.Description,C.CurrencyID,P.ProductID,     
       DrAmount=Sum(C.Amount),CrAmount=0,NoDrOFTrx=Count(*),NoCrOFTrx=0,DrFAmount=Sum(C.Amount),CrFAmount=0     
       from t_CashTransactionModel C, t_Products P, t_GL G          
       Where C.OurBranchID = P.OurBranchID AND C.OurBranchID = G.OurBranchID AND C.ProductID=P.ProductID     
       And P.GLControl=G.AccountID And C.TrxType='D' And C.AccountType='C' and C.Supervision='C' and     
       C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = C.OurBranchID)    
       Group by C.OurBranchID,P.ProductID,C.CurrencyID,P.GLControl,G.Description          
               
       Union All          
               
       Select distinct C.OurBranchID, AccountID=P.GLControl, G.Description, C.CurrencyID,P.ProductID,    
       DrAmount=0,CrAmount=Sum(C.Amount),NoDrOFTrx=0,NoCrOFTrx=Count(*), DrFAmount=0,CrFAmount=Sum(C.Amount)     
       from t_CashTransactionModel C ,t_Products P, t_GL G    
       Where C.OurBranchID = P.OurBranchID AND C.OurBranchID = G.OurBranchID AND C.ProductID=P.ProductID     
       And P.GLControl=G.AccountID And C.TrxType='C' And C.AccountType='C' and C.Supervision='C'     
       and C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = C.OurBranchID)     
       Group by C.OurBranchID,P.ProductID,C.CurrencyID,P.GLControl,G.Description    
      )          
      Asd Group by OurBranchID,AccountID,ProductID,Description,CurrencyID          
               
     Union All          
               
     Select Register='Cash',OurBranchID, AccountID, Description, CurrencyID, ProductID,     
     DrAmount=Sum(DrAmount),CrAmount=Sum(CrAmount),NoOfDrTrx=Sum(NoDrOFTrx),NoOfCrTrx=Sum(NoCrOFTrx),    
     DrFAmount=Sum(DrFAmount),CrFAmount=Sum(CrFAmount)     
     from          
      (Select distinct C.OurBranchID, C.AccountID, C.AccountName [Description], C.CurrencyID, C.ProductID,    
       DrAmount=Sum(C.Amount),CrAmount=0,NoDrOFTrx=Count(*),NoCrOFTrx=0,DrFAmount=Sum(C.Amount),CrFAmount=0     
       from t_CashTransactionModel C
       LEFT JOIN t_Products P ON C.OurBranchID = P.OurBranchID AND C.ProductID = P.ProductID 
       WHERE C.TrxType='D' And C.AccountType='G' and C.Supervision='C' 
       and C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = C.OurBranchID)          
       Group by C.OurBranchID, C.ProductID, C.CurrencyID, C.AccountID, C.AccountName          
           
       Union All          
               
       Select distinct C.OurBranchID, C.AccountID,C.AccountName Description,C.CurrencyID,C.ProductID,    
       DrAmount=0,CrAmount=Sum(Amount),NoDrOFTrx=0,NoCrOFTrx=Count(*) ,DrFAmount=0,CrFAmount=Sum(Amount)     
       from t_CashTransactionModel C
       LEFT JOIN t_Products P ON C.OurBranchID = P.OurBranchID AND C.ProductID = P.ProductID 
       Where C.TrxType='C' And C.AccountType='G' and C.Supervision='C'
       and C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = C.OurBranchID) 
       Group by C.OurBranchID,C.ProductID,C.CurrencyID,C.AccountID,C.AccountName    
      )    
      Asd Group by OurBranchID,AccountID,ProductID,Description,CurrencyID          
               
               
     Union All   --Transfer start from herer          
               
     Select Register='Transfer',OurBranchID, AccountID, Description, CurrencyID, ProductID,     
     DrAmount=Sum(DrAmount),CrAmount=Sum(CrAmount),NoOfDrTrx=Sum(NoDrOFTrx),NoOfCrTrx=Sum(NoCrOFTrx),    
     DrFAmount=Sum(DrFAmount),CrFAmount=Sum(CrFAmount)     
     from    
      (Select distinct C.OurBranchID, AccountID=P.GLControl, G.Description, C.CurrencyID, P.ProductID,     
       DrAmount=Sum(C.Amount),CrAmount=0,NoDrOFTrx=Count(*),NoCrOFTrx=0,DrFAmount=Sum(C.Amount),CrFAmount=0     
       from t_TransferTransactionModel C, t_Products P, t_GL G    
       Where C.OurBranchID = P.OurBranchID AND C.OurBranchID = G.OurBranchID AND C.ProductID=P.ProductID     
       And P.GLControl=G.AccountID And C.TrxType='D' And C.AccountType='C' and C.Supervision='C'    
       and C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = C.OurBranchID)    
       Group by C.OurBranchID,P.ProductID,C.CurrencyID,P.GLControl,G.Description    
               
    Union All    
           
       Select distinct C.OurBranchID, AccountID=P.GLControl,G.Description,C.CurrencyID,P.ProductID,    
       DrAmount=0,CrAmount=Sum(C.Amount),NoDrOFTrx=0,NoCrOFTrx=Count(*) ,DrFAmount=0,CrFAmount=Sum(C.Amount)     
       from t_TransferTransactionModel C, t_Products P, t_GL G          
       Where C.OurBranchID = P.OurBranchID AND C.OurBranchID = G.OurBranchID AND C.ProductID=P.ProductID     
       And P.GLControl=G.AccountID And C.TrxType='C' And C.AccountType='C' and C.Supervision='C'    
       and C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = C.OurBranchID)    
       Group by C.OurBranchID, P.ProductID, C.CurrencyID, P.GLControl, G.Description)    
      Asd Group by OurBranchID,AccountID,ProductID,Description,CurrencyID          
               
     Union All          
     
     Select Register='Transfer',OurBranchID,AccountID,Description,CurrencyID,ProductID,    
     DrAmount=Sum(DrAmount),CrAmount=Sum(CrAmount),NoOfDrTrx=Sum(NoDrOFTrx),NoOfCrTrx=Sum(NoCrOFTrx),    
     DrFAmount=Sum(DrFAmount),CrFAmount=Sum(CrFAmount)     
     from    
      (Select distinct C.OurBranchID,C.AccountID,Description=C.AccountName,C.CurrencyID,C.ProductID,    
      DrAmount=Sum(C.Amount),CrAmount=0,NoDrOFTrx=Count(*),NoCrOFTrx=0,DrFAmount=Sum(C.Amount),CrFAmount=0     
      from t_TransferTransactionModel C
      LEFT JOIN t_Products P ON C.OurBranchID = P.OurBranchID AND C.ProductID = P.ProductID 
      Where C.TrxType='D' And C.AccountType='G' and C.Supervision='C' 
      and C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = C.OurBranchID)
      Group by C.OurBranchID,C.ProductID,C.CurrencyID,C.AccountID,C.AccountName          
          
      Union All          
          
      Select distinct C.OurBranchID,C.AccountID,Description=C.AccountName,C.CurrencyID,C.ProductID,DrAmount=0,    
      CrAmount=Sum(C.Amount),NoDrOFTrx=0,NoCrOFTrx=Count(*), DrFAmount=0,CrFAmount=Sum(C.Amount)     
      from t_TransferTransactionModel C
      LEFT JOIN t_Products P ON C.OurBranchID = P.OurBranchID AND C.ProductID = P.ProductID 
      Where C.TrxType='C' And C.AccountType='G' AND c.Supervision='C' 
      AND C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = C.OurBranchID)
      Group by C.OurBranchID, C.ProductID,C.CurrencyID, C.AccountID, C.AccountName)          
     Asd Group by OurBranchID,AccountID,ProductID,Description,CurrencyID  
         
     Union All   --Transfer for BBA    
         
     Select Register='Transfer',OurBranchID,AccountID,Description,CurrencyID,ProductID,DrAmount=Sum(DrAmount),    
     CrAmount=Sum(CrAmount),NoOfDrTrx=Sum(NoDrOFTrx),NoOfCrTrx=Sum(NoCrOFTrx),DrFAmount=Sum(DrFAmount),    
     CrFAmount=Sum(CrFAmount)     
     from     
      (Select distinct C.OurBranchID, AccountID=GP.Accountid, G.Description, C.CurrencyID, ProductID='GL',     
       DrAmount=Sum(C.ChgAmount),CrAmount=0,NoDrOFTrx=Count(*),NoCrOFTrx=0,DrFAmount=Sum(C.ChgAmount),CrFAmount=0     
       from t_AllModelTransaction C, t_Products P, t_GL G, t_Glparameters GP    
       Where C.OurBranchID = P.OurBranchID AND C.OurBranchID = G.OurBranchID AND C.OurBranchID = GP.OurBranchID    
       AND C.ProductID=P.ProductID and GP.Accountid=G.AccountID and GP.Serialid='9002' And C.TrxType='D'     
       And C.AccountType='C' and C.AppChg=1 and C.Supervision='0'     
       and C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = P.OurBranchID)    
       Group by C.OurBranchID,P.ProductID,C.CurrencyID,GP.Accountid,G.Description          
           
       Union All          
           
       Select distinct C.OurBranchID,AccountID=GP.Accountid,G.Description,C.CurrencyID, ProductID='GL',    
       DrAmount=0,CrAmount=Sum(C.ChgAmount),NoDrOFTrx=0,NoCrOFTrx=Count(*) ,DrFAmount=0,CrFAmount=Sum(C.ChgAmount)     
       from t_AllModelTransaction C ,t_Products P, t_GL G, t_Glparameters GP    
       Where C.OurBranchID = P.OurBranchID AND C.OurBranchID = G.OurBranchID AND C.OurBranchID = GP.OurBranchID    
       AND C.ProductID=P.ProductID and GP.Accountid=G.AccountID and GP.Serialid='9002' And C.TrxType='C'     
       And C.AccountType='C' and C.Supervision='0' and C.AppChg=1     
       and C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = P.OurBranchID)    
       Group by C.OurBranchID, C.ProductID, C.CurrencyID, GP.Accountid, G.Description)    
      Asd Group by OurBranchID,AccountID,ProductID,Description,CurrencyID          
          
      union all          
          
      Select Register='Transfer',OurBranchID,AccountID,Description,CurrencyID,ProductID,DrAmount=Sum(DrAmount),    
      CrAmount=Sum(CrAmount),NoOfDrTrx=Sum(NoDrOFTrx),NoOfCrTrx=Sum(NoCrOFTrx),DrFAmount=Sum(DrAmount),    
      CrFAmount=Sum(CrAmount)     
      from    
       (Select distinct C.OurBranchID,AccountID=P.GlControl,G.Description,C.CurrencyID,P.ProductID,    
       DrAmount=Sum(ChgAmount),CrAmount=0,NoDrOFTrx=Count(*),NoCrOFTrx=0,DrFAmount=Sum(ChgAmount),CrFAmount=0     
       from t_AllModelTransaction C, t_Products P, t_GL G        
       Where C.OurBranchID = P.OurBranchID AND C.OurBranchID = G.OurBranchID AND C.ProductID=P.ProductID        
       and P.Glcontrol=G.AccountID And C.TrxType='D' And C.AccountType='C' and C.AppChg=1 and C.Supervision='0'    
       and C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = P.OurBranchID)    
       Group by C.OurBranchID, P.ProductID, C.CurrencyID, P.Glcontrol, G.Description          
           
       Union All          
           
       Select distinct C.OurBranchID,AccountID=P.GlControl,G.Description,C.CurrencyID,P.ProductID,    
       DrAmount=Sum(C.ChgAmount),CrAmount=0,NoDrOFTrx=Count(*),NoCrOFTrx=0 ,DrFAmount=Sum(C.ChgAmount),CrFAmount=0     
       from t_AllModelTransaction C ,t_Products P,t_GL G        
       Where C.OurBranchID = P.OurBranchID AND C.OurBranchID = G.OurBranchID AND C.ProductID=P.ProductID     
       and P.Glcontrol=G.AccountID And C.TrxType='C' And C.AccountType='C' and C.Supervision='0' and C.AppChg=1    
       and C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = P.OurBranchID)    
       Group by C.OurBranchID, P.ProductID, C.CurrencyID, P.Glcontrol, G.Description)    
      Asd Group by OurBranchID, AccountID, ProductID, Description, CurrencyID          
          
      Union All -- Inward Start here          
          
      Select Register='Inward Clearing',OurBranchID,AccountID,Description,CurrencyID,ProductID,    
      DrAmount=Sum(DrAmount),CrAmount=Sum(CrAmount),NoOfDrTrx=Sum(NoDrOFTrx),NoOfCrTrx=Sum(NoCrOFTrx),    
      DrFAmount=Sum(DrFAmount),CrFAmount=Sum(CrFAmount)     
      from          
       (Select distinct C.OurBranchID,AccountID=P.GLControl,G.Description,C.CurrencyID,P.ProductID,    
        DrAmount=Sum(C.Amount),CrAmount=0,NoDrOFTrx=Count(*),NoCrOFTrx=0,DrFAmount=Sum(C.Amount),CrFAmount=0     
        from t_InwardClearing C ,t_Products P,t_GL G     
        Where C.OurBranchID = P.OurBranchID AND C.OurBranchID = G.OurBranchID AND C.ProductID=P.ProductID     
        And P.GLControl=G.AccountID And C.TransactionType='D' And C.AccountType='C' and isnull(C.Status, '') = ''    
        AND C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = P.OurBranchID)    
        Group by C.OurBranchID,P.ProductID,C.CurrencyID,GLControl,G.Description     
            
        Union All    
            
        Select distinct C.OurBranchID,AccountID=P.GLControl,G.Description,C.CurrencyID,P.ProductID,DrAmount=0,    
        CrAmount=Sum(C.Amount),NoDrOFTrx=0,NoCrOFTrx=Count(*), DrFAmount=0,CrFAmount=Sum(C.Amount)     
        from t_InwardClearing C ,t_Products P,t_GL G          
        Where C.OurBranchID = P.OurBranchID AND C.OurBranchID = G.OurBranchID AND C.ProductID=P.ProductID     
        And P.GLControl=G.AccountID And C.TransactionType='C' And C.AccountType='C' AND isnull(c.Status,'')=''    
        AND C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = P.OurBranchID)    
        Group by C.OurBranchID,P.ProductID,C.CurrencyID,P.GLControl,G.Description)    
      Asd Group by OurBranchID,AccountID,ProductID,Description,CurrencyID          
          
      Union All          
          
      Select Register='Inward Clearing',OurBranchID,AccountID,Description,CurrencyID,ProductID,    
      DrAmount=Sum(DrAmount),CrAmount=Sum(CrAmount),NoOfDrTrx=Sum(NoDrOFTrx),NoOfCrTrx=Sum(NoCrOFTrx),    
      DrFAmount=Sum(DrFAmount),CrFAmount=Sum(CrFAmount)     
      from          
       (Select distinct C.OurBranchID,C.AccountID,Description=C.AccountName,C.CurrencyID,C.ProductID,    
        DrAmount=Sum(C.Amount),CrAmount=0,NoDrOFTrx=Count(*),NoCrOFTrx=0,DrFAmount=Sum(C.Amount),CrFAmount=0     
        from t_InwardClearing C LEFT JOIN t_Products P ON C.OurBranchID = P.OurBranchID AND C.ProductID = P.ProductID 
        WHERE C.TransactionType='D' And C.AccountType='G' and isnull(c.Status, '')=''
        AND C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = C.OurBranchID)    
        Group by C.OurBranchID,C.ProductID,C.CurrencyID,AccountID,AccountName    
          
        Union All          
            
        Select distinct C.OurBranchID,C.AccountID,Description=C.AccountName,C.CurrencyID,P.ProductID,DrAmount=0,    
        CrAmount=Sum(C.Amount),NoDrOFTrx=0,NoCrOFTrx=Count(*), DrFAmount=0,CrFAmount=Sum(C.Amount)     
        from t_InwardClearing C LEFT JOIN t_Products P ON C.OurBranchID = P.OurBranchID AND C.ProductID = P.ProductID 
        WHERE C.TransactionType='C' And C.AccountType='G' and isnull(c.Status,'')=''    
        and C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = P.OurBranchID)    
        Group by C.OurBranchID,P.ProductID,C.CurrencyID,C.AccountID,C.AccountName)    
      Asd Group by OurBranchID,AccountID,ProductID,Description,CurrencyID    
          
      --inward contra start here           
          
      Union All          
          
      Select Register='Inward Clearing',OurBranchID, AccountID, Description, CurrencyID, ProductID,     
      DrAmount=Sum(DrAmount),CrAmount=Sum(CrAmount),NoOfDrTrx=Sum(NoDrOFTrx),NoOfCrTrx=Sum(NoCrOFTrx),    
      DrFAmount=Sum(DrFAmount),CrFAmount=Sum(CrFAmount)     
      from     
       (Select distinct C.OurBranchID, AccountID=VC.Accountid, G.Description, C.CurrencyID,ProductID='GL',    
        DrAmount=0,CrAmount=Sum(C.Amount),NoDrOFTrx=0,NoCrOFTrx=Count(*),DrFAmount=0,CrFAmount=Sum(C.Amount)     
        from t_InwardClearing C, t_GL G, vc_GetGLParameters VC    
        Where C.OurBranchID = G.OurBranchID AND C.OurBranchid=VC.OurBranchid and VC.Accountid=G.AccountID     
        And C.TransactionType='D' And C.AccountType='C' and isnull(c.Status,'')='' and VC.SerialID=2          
        and C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = G.OurBranchID)    
        Group by C.OurBranchID,ProductID,C.CurrencyID,VC.Accountid,G.Description          
            
        Union All          
            
        Select distinct C.OurBranchID, AccountID=VC.Accountid,G.Description,C.CurrencyID,ProductID='GL',    
        DrAmount=Sum(C.Amount),CrAmount=0,NoDrOFTrx=Count(*),NoCrOFTrx=0,DrFAmount=Sum(C.Amount),CrFAmount=0     
        from t_InwardClearing C, t_GL G, vc_GetGLParameters VC    
        Where C.OurBranchID = G.OurBranchID AND G.OurBranchid=VC.OurBranchid and VC.Accountid=G.AccountID    
        And C.TransactionType='C' And C.AccountType='C' and isnull(C.Status, '')='' and VC.serialID=2    
        and C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = G.OurBranchID)    
        Group by C.OurBranchID,C.ProductID,C.CurrencyID,VC.Accountid,G.Description)    
      Asd Group by OurBranchID,AccountID,ProductID,Description,CurrencyID          
          
      Union All    
          
      Select Register='Inward Clearing',OurBranchID,AccountID,Description,CurrencyID,ProductID,    
      DrAmount=Sum(DrAmount),CrAmount=Sum(CrAmount),NoOfDrTrx=Sum(NoDrOFTrx),NoOfCrTrx=Sum(NoCrOFTrx),    
      DrFAmount=Sum(DrFAmount),CrFAmount=Sum(CrFAmount)     
      from          
       (Select distinct C.OurBranchID, AccountID=VC.Accountid,G.Description,C.CurrencyID,ProductID='GL',DrAmount=0,    
        CrAmount=Sum(C.Amount),NoDrOFTrx=0,NoCrOFTrx=Count(*),DrFAmount=0,CrFAmount=Sum(C.Amount)    
        from t_InwardClearing C, t_GL G, vc_GetGLParameters VC    
        Where C.OurBranchID = G.OurBranchID AND C.OurBranchid=VC.OurBranchid and VC.Accountid=G.AccountID     
        And C.TransactionType='D' And C.AccountType='G' and isnull(C.Status,'')='' and VC.serialID=2     
        and C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = G.OurBranchID)    
        Group by C.OurBranchID,ProductID,C.CurrencyID,VC.Accountid,G.Description     
           
        Union All          
           
        Select distinct C.OurBranchID,AccountID=VC.Accountid,G.Description,C.CurrencyID,ProductID='GL',    
        DrAmount=Sum(C.Amount),CrAmount=0,NoDrOFTrx=Count(*),NoCrOFTrx=0,DrFAmount=Sum(C.Amount),CrFAmount=0     
        from t_InwardClearing C, t_GL G, vc_GetGLParameters VC    
        Where C.OurBranchID = G.OurBranchID AND C.OurBranchid=VC.OurBranchid and VC.Accountid=G.AccountID           
        And C.TransactionType='C' And C.AccountType='G' and isnull(C.Status,'')='' and VC.serialID=2     
        and C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = G.OurBranchID)     
        Group by C.OurBranchID,ProductID,C.CurrencyID,VC.Accountid,G.Description)    
      Asd Group by OurBranchID,AccountID,ProductID,Description,CurrencyID          
          
      UNION ALL  -- All Transaction Model Start here                
          
      Select Register='All Model Transaction',OurBranchID,AccountID,Description,CurrencyID,ProductID,    
      DrAmount=Sum(DrAmount),CrAmount=Sum(CrAmount),NoOfDrTrx=Sum(NoDrOFTrx),NoOfCrTrx=Sum(NoCrOFTrx),    
      DrFAmount=Sum(DrFAmount),CrFAmount=Sum(CrFAmount)     
      from     
       (Select distinct C.OurBranchID,AccountID=P.GLControl,G.Description,C.CurrencyID,P.ProductID,    
        DrAmount=Sum(C.Amount),CrAmount=0,NoDrOFTrx=Count(*),NoCrOFTrx=0,DrFAmount=Sum(C.Amount),CrFAmount=0     
        from t_AllModelTransaction C, t_Products P, t_GL G    
        Where C.OurBranchID = P.OurBranchID AND C.OurBranchID=G.OurBranchID AND C.ProductID=P.ProductID     
        AND P.GLControl=G.AccountID And C.TrxType='D' And C.AccountType='C' and c.Supervision='C' and C.Supervision='C'    
        and C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = P.OurBranchID)    
        Group by C.OurBranchID,P.ProductID,C.CurrencyID,P.GLControl,G.Description          
            
        Union All          
            
        Select distinct C.OurBranchID, AccountID=P.GLControl,G.Description,C.CurrencyID,P.ProductID,    
        DrAmount=0,CrAmount=Sum(C.Amount),NoDrOFTrx=0,NoCrOFTrx=Count(*) ,DrFAmount=0,CrFAmount=Sum(C.Amount)     
        from t_AllModelTransaction C, t_Products P, t_GL G          
        Where C.OurBranchID = P.OurBranchID AND C.OurBranchID=G.OurBranchID AND C.ProductID=P.ProductID     
        And P.GLControl=G.AccountID And C.TrxType='C' And C.AccountType='C' and C.Supervision='C'     
        and C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = P.OurBranchID)    
        Group by C.OurBranchID,P.ProductID,C.CurrencyID,P.GLControl,G.Description)    
      Asd Group by OurBranchID,AccountID,ProductID,Description,CurrencyID          
          
      Union All          
          
      Select Register='All Transaction Model',OurBranchID,AccountID,Description,CurrencyID,ProductID,    
      DrAmount=Sum(DrAmount),CrAmount=Sum(CrAmount),NoOfDrTrx=Sum(NoDrOFTrx),NoOfCrTrx=Sum(NoCrOFTrx),    
      DrFAmount=Sum(DrFAmount),CrFAmount=Sum(CrFAmount)     
      from         
       (Select distinct C.OurBranchID,C.AccountID,Description=C.AccountName,C.CurrencyID,C.ProductID,DrAmount=Sum(C.Amount),
        CrAmount=0,NoDrOFTrx=Count(*),NoCrOFTrx=0,DrFAmount=Sum(C.Amount),CrFAmount=0     
        from t_AllModelTransaction C LEFT JOIN t_Products P ON C.OurBranchID = P.OurBranchID AND C.ProductID = P.ProductID
        WHERE C.TrxType='D' And C.AccountType='G' AND C.Supervision='C'    
        and C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = P.OurBranchID )    
        Group by C.OurBranchID,C.ProductID,C.CurrencyID,C.AccountID,C.AccountName          
            
        Union All          
            
        Select distinct C.OurBranchID,C.AccountID,Description=C.AccountName,C.CurrencyID,C.ProductID,DrAmount=0,
        CrAmount=Sum(C.Amount),NoDrOFTrx=Count(*),NoCrOFTrx=0 ,DrFAmount=0,CrFAmount=Sum(C.Amount)     
        from t_AllModelTransaction C LEFT JOIN t_Products P ON C.OurBranchID = P.OurBranchID AND C.ProductID = P.ProductID
        WHERE C.TrxType='C' And C.AccountType='G' and C.Supervision='C'
        and C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = P.OurBranchID )    
        Group by C.OurBranchID,C.ProductID,C.CurrencyID,C.AccountID,C.AccountName)    
      Asd Group by OurBranchID,AccountID,ProductID,Description,CurrencyID          
          
      UNION ALL  -- All GL Model Transaction  Start here                
          
      Select Register='GLModel Transaction',OurBranchID,AccountID,Description,CurrencyID,ProductID,    
      DrAmount=Sum(DrAmount),CrAmount=Sum(CrAmount),NoOfDrTrx=Sum(NoDrOFTrx),NoOfCrTrx=Sum(NoCrOFTrx),    
      DrFAmount=Sum(DrFAmount),CrFAmount=Sum(CrFAmount)     
      from          
       (Select distinct C.OurBranchID, AccountID=G.Accountid,G.Description,C.CurrencyID,ProductID='GL',    
        DrAmount=Sum(C.Amount),CrAmount=0,NoDrOFTrx=Count(*),NoCrOFTrx=0,DrFAmount=Sum(C.Amount),CrFAmount=0     
        from t_GLModelTransaction C, t_GL G     
        Where C.OurBranchID = G.OurBranchID AND C.AccountID=G.Accountid --and P.GLControl=G.AccountID      
        And C.IsCredit=0     
        and C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = G.OurBranchID )    
        Group by C.OurBranchID,G.Accountid,G.Description,C.CurrencyID    
            
        Union All          
            
        Select distinct C.OurBranchID,AccountID=C.AccountID,G.Description,C.CurrencyID,ProductID ='GL',    
        DrAmount=0,CrAmount=Sum(C.Amount),NoDrOFTrx=0,NoCrOFTrx=Count(*) ,DrFAmount=0,CrFAmount=Sum(C.Amount)     
        from t_GLModelTransaction C ,t_GL G    
        Where C.OurBranchID = G.OurBranchID AND C.AccountID=G.AccountID And C.isCredit='1'     
        and C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = G.OurBranchID)     
        Group by C.OurBranchID,C.CurrencyID,C.AccountID,G.Description )    
      Asd Group by OurBranchID,AccountID,Description,CurrencyID,ProductID    
          
      Union All          
      --GLModel contra start here           
          
      Select Register='GLModel Transaction',OurBranchID,AccountID,Description,CurrencyID,ProductID,    
      DrAmount=Sum(DrAmount),CrAmount=Sum(CrAmount),NoOfDrTrx=Sum(NoDrOFTrx),NoOfCrTrx=Sum(NoCrOFTrx),    
      DrFAmount=Sum(DrFAmount),CrFAmount=Sum(CrFAmount)     
      from          
       (Select C.OurBranchID,AccountID=P.Glcontrol,G.Description,G.CurrencyID,P.ProductID,DrAmount=0,    
        CrAmount=Sum(C.Amount),NoDrOFTrx=0,NoCrOFTrx=Count(*),DrFAmount=0,CrFAmount=Sum(C.Amount)     
        from t_Clearing C, t_GL G, t_Products P, t_Account A    
        Where C.OurBranchID = G.OurBranchID AND C.OurBranchID = P.OurBranchID AND C.OurBranchID = A.OurBranchID     
        and A.Productid=P.Productid 
        AND C.TrxID='C' And C.AccountType='C' and isnull(C.status,'')=''     
        And C.ValueDate = (SELECT workingdate from t_last Where OurBranchID = G.OurBranchID)     
        AND C.AccountID=A.Accountid and P.Glcontrol=G.Accountid     
        and C.IslocalCurrency in (Select LocalCurrency = 1 from t_Globalvariables where OurBranchID = G.OurBranchID)    
        Group by C.OurBranchID, P.GLcontrol, G.Description, G.CurrencyID, P.Productid    
            
        Union All    
       
        Select C.OurBranchID, AccountID=P.Glcontrol,G.Description,G.CurrencyID,P.ProductID,    
        DrAmount=Sum(C.Amount),CrAmount=0,NoDrOFTrx=Count(*),NoCrOFTrx=0,DrFAmount=Sum(C.Amount),CrFAmount=0     
        from t_Clearing C, t_GL G, t_Products P, t_Account A    
        Where C.OurBranchID = G.OurBranchID AND C.OurBranchID = P.OurBranchID AND C.OurBranchID = A.OurBranchID      
        AND C.TrxID='D' And C.AccountType='C' and isnull(C.status,'')=''     
        And C.ValueDate =(select workingdate from t_last where OurBranchID = G.OurBranchID)     
        and C.Accountid=A.Accountid and A.Productid=P.Productid and P.Glcontrol=G.Accountid     
        and C.IslocalCurrency in (Select LocalCurrency=1 from t_Globalvariables where OurBranchID = G.OurBranchID)    
        Group by C.OurBranchID, G.CurrencyID,P.Productid,P.GLcontrol,G.Description    
       )    
      Asd Group by OurBranchID,AccountID,ProductID,Description,CurrencyID     
          
      UNION ALL  -- All Inward Returns  Start here                
          
      Select Register='Inward Returns',OurBranchID,AccountID,Description,CurrencyID,ProductID,DrAmount=Sum(DrAmount),    
      CrAmount=Sum(CrAmount),NoOfDrTrx=Sum(NoDrOFTrx),NoOfCrTrx=Sum(NoCrOFTrx),DrFAmount=Sum(DrFAmount),    
      CrFAmount=Sum(CrFAmount)     
      from     
       (Select distinct C.OurBranchID,AccountID=P.GLControl,G.Description,C.CurrencyID,C.ProductID,    
        DrAmount=Sum(C.Amount),CrAmount=0,NoDrOFTrx=Count(*),NoCrOFTrx=0,DrFAmount=Sum(C.Amount),CrFAmount=0     
        from t_InwardReturns C, t_Products P, t_GL G    
        Where C.OurBranchID = P.OurBranchID AND C.OurBranchID = G.OurBranchID AND C.ProductID=P.ProductID     
        And P.GLControl=G.AccountID And C.TransactionType='D' And C.AccountType='C' and isnull(C.Status,'')='C'    
        And C.Chequedate =(select workingdate from t_last where OurBranchID = P.OurBranchID)    
        and C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = P.OurBranchID)    
        Group by C.OurBranchID,C.ProductID,C.CurrencyID,P.GLControl,G.Description          
            
        Union All          
            
        Select distinct C.OurBranchID,AccountID=P.GLControl,G.Description,C.CurrencyID,C.ProductID,DrAmount=0,    
        CrAmount=Sum(C.Amount),NoDrOFTrx=0,NoCrOFTrx=Count(*), DrFAmount=0,CrFAmount=Sum(C.Amount)    
        from t_InwardReturns C, t_Products P, t_GL G    
        Where C.OurBranchID = P.OurBranchID AND C.OurBranchID = G.OurBranchID AND C.ProductID=P.ProductID And     
        P.GLControl=G.AccountID And C.TransactionType='C' And C.AccountType='C' and isnull(C.Status,'')='C'    
        And C.Chequedate = (select workingdate from t_last where OurBranchID = P.OurBranchID)    
        and C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = P.OurBranchID)    
        Group by C.OurBranchID,C.ProductID,C.CurrencyID,P.GLControl,G.Description    
       )    
      Asd Group by OurBranchID,AccountID,ProductID,Description,CurrencyID          
          
      Union All          
          
      Select Register='Inward Returns',OurBranchID,AccountID,Description,CurrencyID,ProductID,    
      DrAmount=Sum(DrAmount),CrAmount=Sum(CrAmount),NoOfDrTrx=Sum(NoDrOFTrx),NoOfCrTrx=Sum(NoCrOFTrx),    
      DrFAmount=Sum(DrFAmount),CrFAmount=Sum(CrFAmount)     
      from          
       (Select distinct C.OurBranchID,C.AccountID,Description=C.AccountName,C.CurrencyID,C.ProductID,    
        DrAmount=Sum(C.Amount),CrAmount=0,NoDrOFTrx=Count(*),NoCrOFTrx=0,DrFAmount=Sum(C.Amount),CrFAmount=0     
        from t_InwardReturns C LEFT JOIN t_Products P ON C.OurBranchID = P.OurBranchID AND C.ProductID = P.ProductID 
        WHERE C.TransactionType='D' And C.AccountType='G'
        And C.Chequedate =(select workingdate from t_last where OurBranchID = P.OurBranchID)    
        and C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = P.OurBranchID)    
        Group by C.OurBranchID,C.ProductID,C.CurrencyID,C.AccountID,C.AccountName          
            
        Union All          
            
        Select distinct C.OurBranchID,C.AccountID,Description=C.AccountName,C.CurrencyID,C.ProductID,DrAmount=0,    
        CrAmount=Sum(C.Amount),NoDrOFTrx=0,NoCrOFTrx=Count(*), DrFAmount=0,CrFAmount=Sum(C.Amount)     
        from t_InwardReturns C LEFT JOIN t_Products P ON C.OurBranchID = P.OurBranchID AND C.ProductID = P.ProductID 
        AND C.TransactionType='C' And C.AccountType='G' 
        And C.Chequedate =(select workingdate from t_last where OurBranchID = P.OurBranchID) 
        and C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = P.OurBranchID)    
        Group by C.OurBranchID,C.ProductID,C.CurrencyID,C.AccountID,C.AccountName    
       )          
      Asd Group by OurBranchID,AccountID,ProductID,Description,CurrencyID          
          
      UNION ALL  -- All Outward Returns  Start here                
          
      Select distinct Register='Outward Returns',OurBranchID,AccountID,Description,CurrencyID,ProductID,    
      DrAmount=Sum(DrAmount),CrAmount=Sum(CrAmount),NoOfDrTrx=Sum(NoDrOFTrx),NoOfCrTrx=Sum(NoCrOFTrx),    
      DrFAmount=Sum(DrFAmount),CrFAmount=Sum(CrFAmount)     
      from    
       (Select C.OurBranchID,AccountID=P.GLControl,G.Description,C.CurrencyID,C.ProductID,DrAmount=Sum(C.Amount),    
        CrAmount=0,NoDrOFTrx=Count(*),NoCrOFTrx=0,DrFAmount=Sum(C.Amount),CrFAmount=0     
        from t_OutwardReturns C, t_Products P, t_GL G    
        Where C.OurBranchID = P.OurBranchID AND C.OurBranchID = G.OurBranchID AND C.ProductID=P.ProductID     
        And P.GLControl=G.AccountID And C.TransactionType='D' And C.AccountType='C' And     
        C.wdate =(select workingdate from t_last where OurBranchID = P.OurBranchID)     
        and C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = P.OurBranchID)    
        Group by C.OurBranchID,C.ProductID,C.CurrencyID,P.GLControl,G.Description          
           
        Union All          
           
        Select distinct C.OurBranchID,AccountID=P.GLControl,G.Description,C.CurrencyID,P.ProductID,DrAmount=0,    
        CrAmount=Sum(C.Amount),NoDrOFTrx=0,NoCrOFTrx=Count(*) ,DrFAmount=0,CrFAmount=Sum(C.Amount)     
        from t_OutwardReturns C ,t_Products P,t_GL G          
        Where C.OurBranchID = P.OurBranchID AND C.OurBranchID = G.OurBranchID AND C.ProductID=P.ProductID     
        And P.GLControl=G.AccountID And C.TransactionType='C' And C.AccountType='C'     
        And C.wdate =(select workingdate from t_last where OurBranchID = P.OurBranchID)    
        and C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = P.OurBranchID)    
        Group by C.OurBranchID,P.ProductID,C.CurrencyID,P.GLControl,G.Description    
       )    
      Asd Group by OurBranchID,AccountID,ProductID,Description,CurrencyID          
          
      Union All          
          
      Select Register='Outward Returns',OurBranchID,AccountID,Description,CurrencyID,ProductID,    
      DrAmount=Sum(DrAmount),CrAmount=Sum(CrAmount),NoOfDrTrx=Sum(NoDrOFTrx),NoOfCrTrx=Sum(NoCrOFTrx),    
      DrFAmount=Sum(DrFAmount),CrFAmount=Sum(CrFAmount)     
      from     
       (Select distinct C.OurBranchID,C.AccountID,Description=C.AccountName,C.CurrencyID,C.ProductID, 
       DrAmount=Sum(C.Amount),CrAmount=0,NoDrOFTrx=Count(*),NoCrOFTrx=0,DrFAmount=Sum(C.Amount),CrFAmount=0     
        from t_OutwardReturns C
        LEFT JOIN t_Products P ON C.OurBranchID = P.OurBranchID AND C.ProductID = P.ProductID 
        WHERE C.TransactionType='D' And C.AccountType='G' 
        And C.wdate =(select workingdate from t_last where OurBranchID = C.OurBranchID)
        and C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = C.OurBranchID)    
        Group by C.OurBranchID,C.ProductID,C.CurrencyID,C.AccountID,C.AccountName          
            
        Union All          
            
        Select distinct C.OurBranchID,C.AccountID,Description=C.AccountName,C.CurrencyID,C.ProductID,DrAmount=0,    
        CrAmount=Sum(C.Amount),NoDrOFTrx=0,NoCrOFTrx=Count(*), DrFAmount=0,CrFAmount=Sum(C.Amount)     
        from t_OutwardReturns C
        LEFT JOIN t_Products P ON C.OurBranchID = P.OurBranchID AND C.ProductID = P.ProductID AND C.TransactionType='C' 
        And C.AccountType='G' And C.wdate =(select workingdate from t_last where OurBranchID = P.OurBranchID)    
        AND C.CurrencyId in (Select LocalCurrency from t_Globalvariables where OurBranchID = P.OurBranchID)    
        Group by C.OurBranchID,C.ProductID,C.CurrencyID,C.AccountID,C.AccountName    
       )    
      Asd Group by OurBranchID,AccountID,ProductID,Description,CurrencyID