﻿create view [dbo].[v_GetInternalAccounts]
AS
select OurBranchID, ClientID, AccountID, Name, ProductID, CurrencyID, CreateBy, CreateTime, convert(varchar(10),OpenDate,120) AS OpenWDate
from t_Account where CreateBy = 'System' and ProductID IN (select ReceivableProductID from t_Products)