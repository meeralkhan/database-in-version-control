﻿-- Updated

CREATE   VIEW [dbo].[ATM_vc_DeclinedBaseI_IMTF]
AS
-- SELECT * FROM ATM_vc_DeclinedBaseI_IMTF
-- WHERE OurBranchID = '02'
-- AND convert(varchar(10), wDate, 120) >= '2021-03-01' AND convert(varchar(10), wDate, 120) <= '2021-03-31' 
-- order by wDate asc
SELECT a.wDate, a.OurBranchID, a.AccountID, Left(a.RefNo, 16) as PAN, a.STAN, a.RetRefNum, a.VISATrxID, a.RefNo, a.wDate as TrxDate, 
a.Amount, a.ConvRate, a.MerchantType, a.AcqCountryCode, a.ForwardInstID, a.CurrCodeTran, a.CurrCodeSett, a.SettlmntAmount, 
Case 
	when LEFT(a.ProcCode,2) = '00' AND LEFT(a.POSEntryMode,2) = '01' then 'ECOM Trx'
	when LEFT(a.ProcCode,2) = '00' then 'POS Trx'
	when LEFT(a.ProcCode,2) = '01' then 'Cash Withdrawal Trx'
	when LEFT(a.ProcCode,2) = '31' then 'Balance Enquiry Trx'
	when LEFT(a.ProcCode,2) = '23' then 'Credit Trx'
	when LEFT(a.ProcCode,2) = '26' then 'Online Credit Trx'
	when LEFT(a.ProcCode,2) = '20' then 'Credit Vouch/ Merch Return Trx'
	when a.ProcCode = 'CANCL CHG' then 'Card Replacement [CANCELLED]'
	when a.ProcCode = 'LT CHG' then 'Card Replacement [LOST]'
	when a.ProcCode = 'STLN CHG' then 'Card Replacement [STOLEN]'
	when a.ProcCode = 'DMG CHG' then 'Card Replacement [DAMAGED]'
	when a.ProcCode = 'LT DELVRY CHG' then 'Card Replacement [LOST IN DELIVERY]'
	when a.ProcCode = 'REP CHG' then 'Card Replacement [NORMAL]'
end as TranDetails, a.DescriptionID, a.[Description], a.CardAccptNameLoc, a.RejectReasonCode, a.RejectReasonDescription, a.TrxDetails
FROM t_RejectedHostTrxs a 
WHERE  isnull(a.ProcCode,'') <> '' AND a.TrxDetails = 'IMTF Declines'