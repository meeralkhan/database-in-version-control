﻿create   view [dbo].[v_BuyerCCPDetails]
AS
select case when b.Status <> '' then b.Status else a.Status end AS Status, a.OurBranchID,a.ReferenceNo FacilityID,a.CreditProgramID,c.ClientID AS BuyerClientID,
a.ClientID,e.AccountID,h.Name AccountTitle,d.CurrencyID,g.Name SupplierName, g.TradeLicenseNumberCorp TradeLicenseNo,a.Limit, ISNULL(e.Limit,0) AS AccountLimit,
--case when h.ClearBalance > 0 then 0 else abs(h.ClearBalance) end AccountLimit,
a.Limit - ISNULL(e.Limit,0) AvailableLimit, --(case when h.ClearBalance > 0 then 0 else abs(h.ClearBalance) end) AvailableLimit,
case when e.Rate1>isnull(e.MinRate1,0) then e.Rate1 else e.MinRate1 end as EffectiveRate
from t_LinkCIFCollateral a
inner join t_FacilityProduct b on a.OurBranchID = b.OurBranchID and a.ClientID = b.ClientID and a.SerialNo = b.FacilityID
inner join t_CreditPrograms c on a.CreditProgramID = c.CreditProgramID and a.OurBranchID = c.OurBranchID
inner join t_Products d on b.ProductID = d.ProductID and a.OurBranchID = d.OurBranchID
left join t_LinkFacilityAccount e on e.ClientID = b.ClientID and a.OurBranchID = e.OurBranchID and a.SerialNo = e.FacilityID
inner join t_AccountBalance h on h.OurBranchID = e.OurBranchID and h.AccountID = e.AccountID
inner join t_Customer g on a.OurBranchID = g.OurBranchID and a.ClientID = g.ClientID