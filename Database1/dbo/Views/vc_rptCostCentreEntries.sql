﻿CREATE   VIEW [dbo].[vc_rptCostCentreEntries]
AS
-- SELECT * FROM vc_CostCentreEntries WHERE ValueDate between '2018-01-01' and '2023-01-01' and OurBranchID = '03'
select a.PostExpenseID, a.ScrollNo, a.OurBranchID, a.ValueDate, a.CostCenterID, a.AccountID, b.Description as Title, a.CurrencyID, a.ForeignAmount, a.Amount,
case when a.IsCredit=0 then 'D' else 'C' end as TrxType, a.Description,
CASE Status WHEN 'C' THEN 'Cleared' WHEN 'R' THEN 'Rejected' WHEN '*' THEN 'Under Supervision' ELSE 'UnChecked' END AS Status,
a.OperatorID as Maker, a.SupervisorID as Checker, a.Date,
Case IsCredit when 0 then Amount  else 0 end as DebitTrxType,   
Case IsCredit when 1 then Amount  else 0 end as CreditTrxType,   
Case IsCredit when 0 then 1 else 0 end as DebitCount,   
case IsCredit when 1 then 1  else 0 end as CreditCount
from t_GLTransactions a
inner join t_GL b on a.AccountID = b.AccountID and a.OurBranchID = b.OurBranchID
where isnull(PostExpenseID,'') <> ''