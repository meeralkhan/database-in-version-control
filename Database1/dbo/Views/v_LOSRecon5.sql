﻿CREATE VIEW [dbo].[v_LOSRecon5]
AS
SELECT t.TrxTimeStamp, t.ChannelRefID, t.ScrollNo AS CoreRefID, t.Amount, a.ClientID
FROM dbo.t_Transactions AS t
INNER JOIN dbo.t_Account AS a ON t.OurBranchID = a.OurBranchID AND t.AccountID = a.AccountID
INNER JOIN dbo.t_Customer AS c ON t.OurBranchID = c.OurBranchID AND a.ClientID = c.ClientID
WHERE (LEFT(t.ChannelRefID, 6) = 'JURIS/') AND (ISNULL(t.RefNo, 6) <> 'VAT') AND (ISNULL(t.ChannelId, N'') = '2')