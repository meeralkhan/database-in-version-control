﻿
CREATE   VIEW [dbo].[v_GetRiskCodeClientWise]
AS
select ClientID, convert(int, MAX(RiskClass)) RiskClass from (
   select b.ClientID, MAX(a.RiskCode) RiskClass
   from t_Disbursement a
   inner join t_Account b on a.OurBranchID = b.OurBranchID and a.AccountID = b.AccountID
   where ISNULL(a.IsClosed,0) = 0 and ISNULL(a.RiskCode,'') IN ('0002','0003','0004','0005','0006','0007')
   group by b.ClientID
   union all
   select ClientID, MAX(OverdraftClassID) RiskClass
   from t_Account
   where Status not in ('I','C') and ISNULL(OverdraftClassID,'') IN ('002','003','004','005','006','007')
   and ProductID in (select ProductID from v_Products where ProductID <> 'TDRCOL')
   group by ClientID
) a
group by ClientID