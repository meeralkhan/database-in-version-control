﻿CREATE VIEW [dbo].[vc_crGetInwardMemo]
AS
SELECT a.OurBranchID, a.AccountID, a.ChequeDate, a.AccountName, a.ChequeID, a.Amount, a.Reason, b.FullName, a.Status, c.Name            
FROM t_InwardClearing a
LEFT OUTER JOIN t_branches c ON a.OurBranchID = c.OurBranchID AND a.BankID = c.BankID AND a.BranchID = c.BranchID     
LEFT OUTER JOIN t_Banks b ON a.OurBranchID = b.OurBranchID AND a.BankID = b.BankID