﻿CREATE VIEW [dbo].[vc_BRF_RptSavAccruals] AS 
Select c.ClientID, ISNULL(c.GroupCode,'-') GroupCode, Case When s.CurrencyID = 'AED' then s.Amount else
s.Amount*ISNULL((Select Top 1 MeanRate from t_Currencies_History WITH (NOLOCK) Where OurBranchID = s.OurBranchID AND ChangeDate <= s.accrualdate 
AND CurrencyID = s.CurrencyID Order By ChangeDate desc),1) End "AmountInAED",
CASE WHEN c.CategoryId = 'Individual' AND c.ResidentNonResident = 'Y' THEN 'Y'	WHEN c.CategoryId = 'Individual' AND c.ResidentNonResident = 'N' THEN 'N'
	 WHEN c.CategoryId <> 'Individual' AND c.JurEmiratesID <> '008' THEN 'Y' WHEN c.CategoryId <> 'Individual' AND c.JurEmiratesID = '008' THEN 'N'
	 WHEN c.ResidentNonResident IS NULL AND c.JurEmiratesID IS NULL THEN '-'
	 ELSE '-'
END As Resident, CASE WHEN c.JurTypeID = '001' THEN 'Mainland' WHEN c.JurTypeID = '002' THEN 'Freezones' Else '-' END JurType,
CASE WHEN c.CategoryId = 'Individual' THEN c.Nationality WHEN c.CategoryId <> 'Individual' THEN c.CountryofIncorporationCorp
END AS Nation, s.* 
from t_RptSavAccruals s WITH (NOLOCK) 
Inner Join t_Account a WITH (NOLOCK) ON a.OurBranchID = s.OurBranchID AND a.AccountID = s.AccountID
Inner Join t_Customer c WITH (NOLOCK) ON a.OurBranchID = c.OurBranchID AND a.ClientID = c.ClientID