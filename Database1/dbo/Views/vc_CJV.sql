﻿
create   view [dbo].[vc_CJV]
as
Select  
 CreditTrxType, DebitTrxType, CreditCount, DebitCount, Status,  
 Amount,wDate,OperatorID,CurrencyID, SupervisorID, TrxType,  
 CostCenterID,ValueDate,OurBranchID, ProductID,AccountID,AccountName,ForeignAmount,  
 Description,AdditionalData,ChequeID,ScrollNo  
  from (  
   Select '' CostCenterID,ValueDate,OurBranchID, ProductID,AccountID,AccountName,ForeignAmount,TrxType, Description,AdditionalData,ChequeID,ScrollNo,        
   Amount,wDate,OperatorID,CurrencyID, SupervisorID,  
     
   Case trxType when 'D' then Amount  else 0 end as DebitTrxType,  
   Case trxType when 'C' then Amount  else 0 end as CreditTrxType,  
   Case trxType when 'D' then 1 else 0 end as DebitCount,  
   case trxType when 'C' then 1  else 0 end as CreditCount,      
     
   CASE Status WHEN 'C' THEN 'Cleared' WHEN 'R' THEN 'Rejected' WHEN '*' THEN 'Under Supervision' ELSE 'UnChecked' END AS Status        
   from t_Transactions  union all  
   Select '' CostCenterID,ValueDate,a.OurBranchID, '' ProductID,a.AccountID,b.Description AccountName,ForeignAmount,case when IsCredit=0 then 'D' else 'C' end,  
   a.Description,AdditionalData,'' ChequeID,ScrollNo,Amount,Date,OperatorID,b.CurrencyID, SupervisorID,  
     
   Case IsCredit when 0 then Amount  else 0 end as DebitTrxType,  
   Case IsCredit when 1 then Amount  else 0 end as CreditTrxType,  
   Case IsCredit when 0 then 1 else 0 end as DebitCount,  
   case IsCredit when 1 then 1  else 0 end as CreditCount,        
     
   CASE Status WHEN 'C' THEN 'Cleared' WHEN 'R' THEN 'Rejected' WHEN '*' THEN 'Under Supervision' ELSE 'UnChecked' END AS Status      
   from t_GLTransactions a inner join t_GL b on b.AccountID = a.AccountID and a.OurBranchID = b.OurBranchID  
   ) a