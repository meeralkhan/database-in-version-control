﻿
create view [dbo].[v_VATException2]
AS
select a.OurBranchID, a.ScrollNo, a.SerialNo, a.ChannelId, a.ChannelRefID, a.AccountID, a.CurrencyID, a.Date, a.ValueDate, a.TrxTimeStamp,
a.ForeignAmount, a.Amount, a.ExchangeRate, a.DescriptionID, TClientId, TAccountid, TProductId, OperatorID, SupervisorID, a.CostCenterID
from t_GLTransactions a
where a.AccountID IN (select AccountID from t_VatTypes where AccountID IS NOT NULL)
and a.ChannelId IN ('','0')
and (TClientId IS NULL OR TClientId IS NULL OR TProductId IS NULL)