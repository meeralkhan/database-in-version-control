﻿
create   view [dbo].[v_DailyTablesBk]
as
select distinct name, crdate from (
   select name, crdate from sysobjects where xtype = 'u' and name like 't_Account%' and SUBSTRING([name],LEN([name])-7,LEN([name])) between '00000000' and '99999999'
   union all
   select name, crdate from sysobjects where xtype = 'u' and name like 't_Atm_Cash%' and SUBSTRING([name],LEN([name])-7,LEN([name])) between '00000000' and '99999999'
   union all
   select name, crdate from sysobjects where xtype = 'u' and name like 't_Atm_Transfer%' and SUBSTRING([name],LEN([name])-7,LEN([name])) between '00000000' and '99999999'
   union all
   select name, crdate from sysobjects where xtype = 'u' and name like 't_GL_%' and SUBSTRING([name],LEN([name])-7,LEN([name])) between '00000000' and '99999999'
   union all
   select name, crdate from sysobjects where xtype = 'u' and name like 't_Transfer_%' and SUBSTRING([name],LEN([name])-7,LEN([name])) between '00000000' and '99999999'
   union all
   select name, crdate from sysobjects where xtype = 'u' and name like 'tbl_DC_%' and SUBSTRING([name],LEN([name])-7,LEN([name])) between '00000000' and '99999999'
   union all
   select name, crdate from sysobjects where xtype = 'u' and name like 'tbl_DCP_%' and SUBSTRING([name],LEN([name])-7,LEN([name])) between '00000000' and '99999999'
) a