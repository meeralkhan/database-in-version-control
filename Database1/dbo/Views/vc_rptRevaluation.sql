﻿CREATE   VIEW [dbo].[vc_rptRevaluation]
AS
-- SELECT * FROM vc_rptRevaluation WHERE OurBranchID = '03' AND wDate = '2021-05-05' ORDER BY AccountType
SELECT 
CASE 
	WHEN r.TrxType = 'C' THEN r.PnL
	WHEN r.TrxType = 'D' THEN -(r.PnL)
	ELSE r.PnL
END AS PnL, r.TrxType, 
r.OurBranchID, r.AccountID, r.wDate, r.ProductID, r.AccountType, r.AccountName, r.CurrencyID, r.Amount, r.LocEq, r.ExRate, r.RevAmount, 
CASE 
	WHEN r.AccountType = 'C' THEN 'CUSTOMER' 
	WHEN r.AccountType = 'G' THEN 'GENERAL LEDGER'
	ELSE 'GENERAL LEDGER'
END AS CaseAccountType, 
r.GLControl, g.Description from t_RevaluationTemp AS r LEFT JOIN t_GL AS g on g.OurBranchID = r.OurBranchID AND g.AccountID IN (r.AccountID , r.GLControl)
WHERE PnL <> 0