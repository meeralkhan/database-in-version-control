﻿ 
CREATE   VIEW [dbo].[vc_rptVATInputDumpReport]
AS    
SELECT gt.OurBranchID, gt.CostCenterID CostCentreID, gt.ScrollNo, gt.PostExpenseID ExpenseJVID,gt.AccountID as GLAccountID,  
g.Description as GLTitle,gt.VendorID,v.VendorName,      
InvoiceNo as InvoiceNo,ct.Name as CostType, ls.Name as CustomerLifeCycle,cast(gt.Date as date) wDate, gt.CurrencyID, gt.ExchangeRate,   
gt.Amount, gt.ForeignAmount,      
gt.DescriptionID as NarrationID,b.Description as NarrationDesc,case gt.IsCredit   when 0 then 'D' else 'C'   end  AS TrxType,      
gt.ChannelId,gt.ChannelRefID,gt.Description,gt.Status,gt.OperatorID, gt.SupervisorID,gt.ValueDate                        
FROM t_GLTransactions as gt                        
Inner Join t_GL as g On gt.OurBranchID=g.OurBranchID And gt.AccountID=g.AccountID              
Inner Join t_ManageVATInputNarration as mvi On mvi.ourBranchID =gt.ourBranchID And mvi.NarrationID = gt.DescriptionID                        
inner join t_TransactionDescriptions b on gt.OurBranchID = b.OurBranchID and gt.DescriptionID = b.DescriptionID              
Left Join t_GLVendors as v On v.OurBranchID = gt.OurBranchID And v.VendorID = gt.VendorID      
Left Join t_CostTypes as ct On ct.OurBranchID = gt.OurBranchID And ct.CostTypeID= gt.CostTypeID      
Left Join t_CustomerLifecycles as ls On ls.OurBranchID = gt.OurBranchID And ls.ListCycleID= gt.CustomerLifecycleID             
left join t_PostExpenseDetails as ped on ped.CostCenterID = gt.CostCenterID and ped.PostExpenseID = gt.PostExpenseID   
and gt.AccountID = ped.AccountID and gt.Amount = ped.Amount        
and ped.EntryType = case gt.IsCredit when 1 then 'C' else 'D' end        
where gt.Status<>'R'