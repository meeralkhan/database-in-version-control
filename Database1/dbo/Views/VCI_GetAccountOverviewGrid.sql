﻿create view [dbo].[VCI_GetAccountOverviewGrid]    
AS    
    
 select ab.OurBranchID, ab.AccountID, a.ClientID, p.CurrencyID,     
 AvailableBalance = (ab.ClearBalance + ab.Limit - ab.FreezeAmount - p.MinBalance),   
 CurrentBalance = (ab.ClearBalance + ab.Limit - ab.FreezeAmount - p.MinBalance + Effects),   
 ab.LocalClearBalance    
 from t_AccountBalance ab     
 INNER JOIN t_Account a ON ab.OurBranchID = a.OurBranchID AND ab.AccountID = a.AccountID    
 INNER JOIN t_Products p ON ab.OurBranchID = p.OurBranchID AND ab.ProductID = p.ProductID