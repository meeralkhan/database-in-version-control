﻿
CREATE view [dbo].[vc_rptgetstatementofconGroup]
as
SELECT G.ourbranchid, G.accountid, G.Description, G.AccountType, GT.valuedate, OpeningBalance=isnull(G.openingbalance,0),
OpeningBalanceF = isnull(G.ForeignOpeningBalance,0), G.CurrencyID, CrAmount =isnull(case  when iscredit=1 then (amount) end,0),
DrAmount =isnull(case  when iscredit=0 then (amount) end,0)
FROM t_GLTransactions GT
left join t_Gl G on G.OurBranchid = GT.OurBranchid and G.Accountid = GT.Accountid
WHERE ISNULL(GT.Status, '') <> 'R' AND G.Balance <> 0