﻿create VIEW [dbo].[vc_RptGLMonthCreditAbstract]        
AS          
    
 SELECT GT.OurBranchID, GT.AccountID, isnull(SUM(GT.Amount),0) AS MonthCredit    
 --SUM(t_GLTransactions.ForeignAmount) AS MonthForeignCredit          
 FROM dbo.t_GLTransactions GT     
 INNER JOIN dbo.t_Last TL ON GT.OurBranchID = TL.OurBranchID AND          
     { fn YEAR(GT.Date)           
    } = { fn YEAR(TL.WORKINGDATE) } AND           
    { fn MONTH(GT.Date)           
    } = { fn MONTH(TL.WORKINGDATE) }          
    AND ISNULL(GT.Status,'') <> 'R'
GROUP BY GT.OurBranchID, GT.AccountID, CONVERT(char(1), GT.IsCredit)          
HAVING (CONVERT(char(1), GT.IsCredit) = '1')