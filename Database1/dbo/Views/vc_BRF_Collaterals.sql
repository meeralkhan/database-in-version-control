﻿CREATE VIEW [dbo].[vc_BRF_Collaterals] AS SELECT c.OurBranchID, c.ClientID, c.CollateralID, c.Description CollateralDesc, c.CollateralStatus, c.CollateralTypeID, c1.Description CTypeIDDesc, 
c.CollateralValue, c.CurrencyID, c.ExRate, c.LocalEq, c.Margin, c.DrawingPower, c.ReviewDate, c.cExpiryDate, c.CollateralSubTypeID, c2.Description CSubTypeIDDesc, 
c.CollateralSubType2ID, c3.Description CSubType2IDDesc, 
CASE WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_ASSIGN' AND ISNULL(c.CollateralSubTypeID,'-') = 'POS' AND ISNULL(c.CollateralSubType2ID,'-') = '-' THEN 'CL5'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_ASSIGN' AND ISNULL(c.CollateralSubTypeID,'-') = 'AIR' AND ISNULL(c.CollateralSubType2ID,'-') = '-' THEN 'CL5'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_ASSIGN' AND ISNULL(c.CollateralSubTypeID,'-') = 'ARP' AND ISNULL(c.CollateralSubType2ID,'-') = '-' THEN 'CL5'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_ASSIGN' AND ISNULL(c.CollateralSubTypeID,'-') = 'ASF' AND ISNULL(c.CollateralSubType2ID,'-') = '-' THEN 'CL5'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_ASSIGN' AND ISNULL(c.CollateralSubTypeID,'-') = 'APP' AND ISNULL(c.CollateralSubType2ID,'-') = '-' THEN 'CL5'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_ASSIGN' AND ISNULL(c.CollateralSubTypeID,'-') = 'ALP' AND ISNULL(c.CollateralSubType2ID,'-') = '-' THEN 'CL5'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_ASSIGN' AND ISNULL(c.CollateralSubTypeID,'-') = 'AIP' AND ISNULL(c.CollateralSubType2ID,'-') = 'AOII' THEN 'CL5'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_ASSIGN' AND ISNULL(c.CollateralSubTypeID,'-') = 'AIP' AND ISNULL(c.CollateralSubType2ID,'-') = 'AOIP' THEN 'CL5'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_ASSIGN' AND ISNULL(c.CollateralSubTypeID,'-') = 'AIP' AND ISNULL(c.CollateralSubType2ID,'-') = 'CINS' THEN 'CL5'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_ASSIGN' AND ISNULL(c.CollateralSubTypeID,'-') = 'AIP' AND ISNULL(c.CollateralSubType2ID,'-') = 'OSIN' THEN 'CL5'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_ASSIGN' AND ISNULL(c.CollateralSubTypeID,'-') = 'AIP' AND ISNULL(c.CollateralSubType2ID,'-') = 'INKM' THEN 'CL5'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_ASSIGN' AND ISNULL(c.CollateralSubTypeID,'-') = 'AIP' AND ISNULL(c.CollateralSubType2ID,'-') = 'OCRV' THEN 'CL5'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_ASSIGN' AND ISNULL(c.CollateralSubTypeID,'-') = 'AIP' AND ISNULL(c.CollateralSubType2ID,'-') = 'OPME' THEN 'CL5'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_ASSIGN' AND ISNULL(c.CollateralSubTypeID,'-') = 'AIP' AND ISNULL(c.CollateralSubType2ID,'-') = 'FBUR' THEN 'CL5'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_CASH' AND ISNULL(c.CollateralSubTypeID,'-') = 'PFD' AND ISNULL(c.CollateralSubType2ID,'-') = '-' THEN 'CL1'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_CASH' AND ISNULL(c.CollateralSubTypeID,'-') = 'PAC' AND ISNULL(c.CollateralSubType2ID,'-') = '-' THEN 'CL1'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_CASH' AND ISNULL(c.CollateralSubTypeID,'-') = 'POI' AND ISNULL(c.CollateralSubType2ID,'-') = '-' THEN 'CL5'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_CASH' AND ISNULL(c.CollateralSubTypeID,'-') = 'CBU' AND ISNULL(c.CollateralSubType2ID,'-') = '-' THEN 'CL1'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_GUAR' AND ISNULL(c.CollateralSubTypeID,'-') = 'CGR' AND ISNULL(c.CollateralSubType2ID,'-') = '-' THEN 'CL5'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_GUAR' AND ISNULL(c.CollateralSubTypeID,'-') = 'PGR' AND ISNULL(c.CollateralSubType2ID,'-') = '-' THEN 'CL5'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_PROP' AND ISNULL(c.CollateralSubTypeID,'-') = 'FDM' AND ISNULL(c.CollateralSubType2ID,'-') = 'REMR' THEN 'CL4'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_PROP' AND ISNULL(c.CollateralSubTypeID,'-') = 'FDM' AND ISNULL(c.CollateralSubType2ID,'-') = 'COMM' THEN 'CL4'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_PROP' AND ISNULL(c.CollateralSubTypeID,'-') = 'FDM' AND ISNULL(c.CollateralSubType2ID,'-') = 'LAND' THEN 'CL4'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_PROP' AND ISNULL(c.CollateralSubTypeID,'-') = 'FDM' AND ISNULL(c.CollateralSubType2ID,'-') = 'BILD' THEN 'CL4'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_PROP' AND ISNULL(c.CollateralSubTypeID,'-') = 'FDM' AND ISNULL(c.CollateralSubType2ID,'-') = 'APAR' THEN 'CL4'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_PROP' AND ISNULL(c.CollateralSubTypeID,'-') = 'SDM' AND ISNULL(c.CollateralSubType2ID,'-') = 'REMR' THEN 'CL5'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_PROP' AND ISNULL(c.CollateralSubTypeID,'-') = 'SDM' AND ISNULL(c.CollateralSubType2ID,'-') = 'COMM' THEN 'CL5'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_PROP' AND ISNULL(c.CollateralSubTypeID,'-') = 'SDM' AND ISNULL(c.CollateralSubType2ID,'-') = 'LAND' THEN 'CL5'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_PROP' AND ISNULL(c.CollateralSubTypeID,'-') = 'SDM' AND ISNULL(c.CollateralSubType2ID,'-') = 'BILD' THEN 'CL5'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_PROP' AND ISNULL(c.CollateralSubTypeID,'-') = 'SDM' AND ISNULL(c.CollateralSubType2ID,'-') = 'APAR' THEN 'CL5'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_CHEQUE' AND ISNULL(c.CollateralSubTypeID,'-') = 'SCQ' AND ISNULL(c.CollateralSubType2ID,'-') = '-' THEN 'CL5'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_MACHINE' AND ISNULL(c.CollateralSubTypeID,'-') = 'MOM' AND ISNULL(c.CollateralSubType2ID,'-') = '-' THEN 'CL5'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_OTHER' AND ISNULL(c.CollateralSubTypeID,'-') = 'NPD' AND ISNULL(c.CollateralSubType2ID,'-') = '-' THEN 'CL5'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_OTHER' AND ISNULL(c.CollateralSubTypeID,'-') = 'PNT' AND ISNULL(c.CollateralSubType2ID,'-') = '-' THEN 'CL5'
WHEN ISNULL(c.CollateralTypeID,'-') = 'COLL_OTHER' AND ISNULL(c.CollateralSubTypeID,'-') = 'SAG' AND ISNULL(c.CollateralSubType2ID,'-') = '-' THEN 'CL5'
ELSE '-' END CollateralDDM  
FROM t_Collaterals c WITH (NOLOCK)
Inner Join t_CollateralTypes c1 WITH (NOLOCK) ON c.CollateralTypeID = c1.CollateralTypeID AND c.OurBranchID = c1.OurBranchID 
LEFT Join t_CollateralSubTypes c2 WITH (NOLOCK) ON c.CollateralTypeID = c2.CollateralTypeID AND c.CollateralSubTypeID = c2.CollateralSubTypeID AND c.OurBranchID = c2.OurBranchID 
LEFT Join t_CollateralSubTypes2 c3 WITH (NOLOCK) ON c.CollateralTypeID = c3.CollateralTypeID AND c.CollateralSubTypeID = c3.CollateralSubTypeID 
AND c.CollateralSubType2ID = c3.CollateralSubType2ID AND c.OurBranchID = c3.OurBranchID