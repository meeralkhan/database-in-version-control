﻿CREATE VIEW [dbo].[vc_BRF_RptODAccruals] AS 
Select Case When a.CurrencyID = 'AED' then Amount else
Amount*(Select Top 1 MeanRate from t_Currencies_History WITH (NOLOCK) Where OurBranchID = a.OurBranchID AND ChangeDate <= a.accrualdate AND CurrencyID = a.CurrencyID Order By ChangeDate desc) End "AmountInAED" , a.* from t_RptODAccruals a