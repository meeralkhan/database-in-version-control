﻿create view [dbo].[vc_RptAccountOpeningForm_OperatedBy]
AS

 SELECT OurBranchID, AccountID, Name, [Address], Phone, NIC, DateOfBirth, SeniorCitizen, NICExpiryDate, Gender, MaritalStatus, POBCity, OtherNationality, PermanentResidency, USTaxation, NTNNo, PhoneRes, MobileNo, Email, POBCountry, Occupation, NameOfEmployer, AddressOfEmployer, NameOfFather, NameOfMother, Nationality2
 FROM t_AccountOperatedBy