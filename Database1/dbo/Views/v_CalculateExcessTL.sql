﻿CREATE   VIEW [dbo].[v_CalculateExcessTL]
AS
select a.OurBranchID, b.ClientID, a.AccountID, b.Name AS AccountName, b.ProductID, b.CurrencyID, a.DealID, c.ReferenceNo AS DealRefNo, cast(MIN(a.wDate)+1 as date) AS AccrualDate
from t_TrackReceivables a
inner join t_Account b on a.OurBranchID = b.OurBranchID and a.AccountID = b.AccountID
inner join t_Disbursement c on a.OurBranchID = c.OurBranchID and a.AccountID = c.AccountID and a.DealID = c.DealID and ISNULL(c.IsClosed,0) = 0
where a.IsFreeze = 1
group by a.OurBranchID, b.ClientID, a.AccountID, b.Name, b.ProductID, b.CurrencyID, a.DealID, c.ReferenceNo