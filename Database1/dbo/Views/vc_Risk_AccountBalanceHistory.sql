﻿CREATE   View [dbo].[vc_Risk_AccountBalanceHistory] 
AS  
Select wDate, FacilityType, OurBranchID, GroupID, ClientID, AccountID, ProductID, DealID, Name, Balance, NoOfDPD    
from (    
Select d.wDate, 'TermLoan' FacilityType, a.OurBranchID, c.GroupID, a.ClientID, a.AccountID, a.ProductID, DealID, c.Name    
, Case When OutstandingBalance > 0 then 0 Else Abs(OutstandingBalance) END Balance , d.NoOfDPD     
from t_DealBalanceHistory d WITH (NOLOCK)    
Inner Join t_Account a WITH (NOLOCK) on a.AccountID = d.AccountID Inner Join t_Customer c WITH (NOLOCK)on c.ClientID = a.ClientID    
UNION ALL    
Select ab.wDate, 'OverDraft' FacilityType, a.OurBranchID, c.GroupID, a.ClientID, a.AccountID, a.ProductID, 0 DealID, c.Name    
, Case When ClearBalance > 0 then 0 Else Abs(ClearBalance) END Balance, ab.NoOfDPD     
from t_AccountBalanceHistory ab WITH (NOLOCK)    
Inner Join t_Account a WITH (NOLOCK) on a.AccountID = ab.AccountID Inner Join t_Customer c WITH (NOLOCK) on c.ClientID = a.ClientID     
And a.ProductID IN (Select ProductID from t_Products Where ProductType = 'AE')    
) as a