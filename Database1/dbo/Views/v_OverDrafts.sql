﻿
CREATE   view [dbo].[v_OverDrafts]        
as        
select a.OurBranchID, a.ClientID,f.GroupID, a.FacilityID, f.ReferenceNo AS FacilityRefNo, a.AccountID, aa.Name, aa.ProductID, aa.CurrencyID, ISNULL(aa.[Status],'') AS [Status], a.AccountLimitID, a.Limit,        
case when ab.ClearBalance > 0 then 0 else ABS(ab.ClearBalance) end AS Principal, ISNULL(Accrual,0) + ISNULL(tAccrual,0) AS Interest        
from t_LinkFacilityAccount a        
inner join t_LinkCIFCollateral f on a.OurBranchID = f.OurBranchID and a.ClientID = f.ClientID and a.FacilityID = f.SerialNo        
inner join t_Account aa on a.OurBranchID = aa.OurBranchID and a.AccountID = aa.AccountID and ISNULL(aa.[Status],'') != 'C'        
inner join t_Products p on aa.OurBranchID = p.OurBranchID and aa.ProductID = p.ProductID and p.ProductType = 'AE'        
inner join t_AccountBalance ab on a.OurBranchID = ab.OurBranchID and a.AccountID = ab.AccountID