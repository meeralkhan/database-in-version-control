﻿
CREATE view [dbo].[v_Products]    
as    
select OurBranchID, ProductID, Description, CurrencyID, ProductType from t_Products where ProductID IN ('TDRCOL','NONCHQ')    
union all    
select OurBranchID, ProductID, Description, CurrencyID, ProductType from t_Products where ISNULL(ProductType,'-') IN ('L','AE','N') and ISNULL(IsTermDeposit,'No')='No'