﻿CREATE   VIEW [dbo].[v_CalculateExcessOD]
AS
select h.OurBranchID, h.AccountID, AccountName, h.ProductID, a.CurrencyID, wDate+1 AS AccrualDate
from t_AccountBalanceHistory h
inner join t_Account a on h.OurBranchID = a.OurBranchID and h.AccountID = a.AccountID
where ClearBalance+Limit < 0