﻿CREATE   VIEW [dbo].[vc_AccountBalanceHistory] 
as 
SELECT ab.wDate, ab.OurBranchID, ab.AccountID, ab.ProductID, ab.AccountName, ab.ClearBalance, ab.Effects, ab.LocalEffects, ab.Limit, a.CurrencyID,  
"LocalClearBalance" = CASE WHEN a.CurrencyID = 'AED' THEN ab.ClearBalance ELSE ab.LocalClearBalance END, ab.OverDraftClassID, ab.NoOfDPD
FROM dbo.t_AccountBalanceHistory ab 
INNER JOIN t_Account a ON a.AccountID = ab.AccountID