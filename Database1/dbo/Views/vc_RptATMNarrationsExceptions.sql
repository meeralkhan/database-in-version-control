﻿
create   view [dbo].[vc_RptATMNarrationsExceptions]  
AS  
--select * from vc_RptATMNarrationsExceptions where OurBranchID = '02'  
select a.OurBranchID, a.DescriptionID, a.Description, ISNULL(c.NarrationID,'') ATMNarrations,   
ISNULL(d.ChargesNarrationID,'') ChargesNarrationID, ISNULL(b.[Percent],0) [VATPercent], ISNULL(d.VATPercent,0) ATMPercent, ISNULL(b.AccountID,'') NarrationAccountID,   
ISNULL(d.VATAccountID,'') ATMNarrationAccountID,  
Status = Case   
 when Left(a.DescriptionID,1) < 4 then   
  case when a.DescriptionID <> ISNULL(c.NarrationID,'') then 'Narrations Mismatch' else 'OK' end  
 when Left(a.DescriptionID,1) > 3 then   
  case   
   when ISNULL(b.AccountID,'') <> ISNULL(d.VATAccountID,'') then 'VAT Accounts Mismatch'   
   when a.DescriptionID <> ISNULL(d.ChargesNarrationID,'') then 'Narrations Mismatch'   
   when b.[Percent] <> d.[VATPercent] then 'VAT Rates Mismatch'  
  else 'OK' end  
 else 'OK'  
end  
from t_TransactionDescriptions a   
left join t_VatTypes b ON a.Ourbranchid = b.OurBranchID AND a.VatID = b.VatID  
left join   
 (  
  select OurBranchID, NarrationID from t_ATMNarrations group by OurBranchID, NarrationID  
 ) AS c   
 ON a.OurBranchID = c.OurBranchID AND a.DescriptionID = c.NarrationID  
left join   
 (  
  select OurBranchID, ChargesNarrationID, VATPercent, VATAccountID from t_ATMNarrations group by OurBranchID, ChargesNarrationID, VATPercent, VATAccountID  
 ) AS d   
 ON a.OurBranchID = d.OurBranchID AND a.DescriptionID = d.ChargesNarrationID  
  
where a.IsHost = 1