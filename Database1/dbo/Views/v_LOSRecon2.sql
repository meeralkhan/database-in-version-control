﻿create view [dbo].[v_LOSRecon2]
AS
select ReferenceNo, DisbursementDate, Amount AS LoanAmount, ISNULL(BaseRate,'001') AS RateType, ISNULL(Margin,0) AS Margin, case when Margin < 0 then '-' else '+' end
AS [Sign], ISNULL(FloorRate,0) FloorRate, DATEDIFF(MONTH, DisbursementDate, MaturityDate) AS Tenor, 'M' AS TenorCode
from t_Disbursement d