﻿
CREATE    VIEW [dbo].[vc_OUTPUTEXCEPTIONREPORT]
AS             
SELECT   td.DescriptionID,a.OurBranchID, a.ScrollNo, a.SerialNo, gt.AccountID as GLAccountID, gta.Description as GLTitle,ta.AccountID,     
c.Name, d.ClientID, d.Name AS ClientName,  c.ProductID, td.DescriptionID NarrationID,td.Description as NarrationDesc,a.Date wDate, 
a.ValueDate, a.TrxTimeStamp,a.CurrencyID, a.ExchangeRate, a.Amount, a.ForeignAmount,  
case a.IsCredit when 0 then 'D' else 'C' end AS TrxType, a.ChannelId, a.ChannelRefID, 
case isnull(ta.Description,'') when '' then gt.Description else ta.Description end AS Description,
case a.Status when '' then 'C' else a.status end Status,a.OperatorID,a.SupervisorID 
FROM t_GLTransactions a        
LEFT Join t_GLTransactions as gt On a.ValueDate=gt.ValueDate And a.ScrollNo=gt.ScrollNo AND isnull(gt.VatReferenceID,'') = ''     
AND a.Status<>'R' and gt.IsCredit = 1 AND substring(gt.AccountID,1,1) NOT IN (5,6)
AND a.Amount = Round((isnull(gt.Amount,0) / 100 * a.[Percent]),2)      
Left Join t_Transactions as ta On ta.ScrollNo=a.ScrollNo AND ta.Status<>'R' and a.VatReferenceID = ta.VatReferenceID 
AND isnull(ta.RefNo,'') <> 'VAT'     and ta.TrxType = 'D' 
LEFT JOIN t_GL gta ON gt.AccountID = gta.AccountID AND gta.OurBranchID = gt.OurBranchID                   
INNER JOIN t_GL g ON a.AccountID = g.AccountID AND a.OurBranchID = g.OurBranchID                   
INNER JOIN t_VatTypes b ON a.OurBranchID = b.OurBranchID AND a.VatID = b.VatID        
INNER JOIN t_TransactionDescriptions td ON td.DescriptionID = case isnull(ta.DescriptionID,'') when '' then gt.DescriptionID else ta.DescriptionID end 
AND td.OurBranchID = a.OurBranchID      
LEFT JOIN t_Account c ON a.TAccountid = c.AccountID AND a.OurBranchID = c.OurBranchID
INNER JOIN t_Customer d on c.OurBranchID = d.OurBranchID and c.ClientID = d.ClientID   where a.IsCredit = 1 and isnull(a.VatReferenceID
,'') <> '' AND substring(a.AccountID,1,1) NOT IN (5,6)
AND isnull(gt.OurBranchID,'')+isnull(Substring(isnull(gt.AccountID,''),1,10),'')+td.DescriptionID    
NOT IN(select n.OurBranchID+n.CreditAccount+n.NarrationId from t_NarrationsToGL n)