﻿create VIEW [dbo].[vc_RptGLBalancesAbstract]              
 AS                
 SELECT G.OurBranchID, G.AccountID, G.Description,         
  /*t_GL.ForeignOpeningBalance + ISNULL(vc_RptGLMonthCredit.MonthForeignCredit,                
   0) - ISNULL(vc_RptGLMonthDebit.MonthForeignDebit, 0)                 
  AS ForeignOpeningBalance, t_GL.ForeignBalance,                 
  t_GL.OpeningBalance + ISNULL(vc_RptGLMonthCredit.MonthCredit,                
   0) - ISNULL(vc_RptGLMonthDebit.MonthDebit, 0)                 
  AS OpeningBalance, */                
  MonthDebit=isnull(VC1.MonthDebit,0),   
  MonthCredit=isnull(VC2.MonthCredit,0),  
  Case   
  WHEN isnull(VC2.MonthCredit,0)-isnull(VC1.MonthDebit,0) < 0 then isnull(VC2.MonthCredit,0)-isnull(VC1.MonthDebit,0)  
  ELSE ''  
  End As DebitBalance,  
  Case   
  WHEN isnull(VC2.MonthCredit,0)-isnull(VC1.MonthDebit,0) >= 0 then isnull(VC2.MonthCredit,0)-isnull(VC1.MonthDebit,0)  
  ELSE ''  
  End As CreditBalance  
  --G.Balance, G.AccountType,         
  --G.CurrencyID, G.AccountClass        
  FROM dbo.t_GL G        
  LEFT OUTER JOIN dbo.vc_RptGLMonthDebitAbstract VC1 ON G.OurBranchID = VC1.OurBranchID AND G.AccountID = VC1.AccountID         
  LEFT OUTER JOIN dbo.vc_RptGLMonthCreditAbstract VC2 ON G.OurBranchID = VC2.OurBranchID AND G.AccountID = VC2.AccountID        
  where (VC2.MonthCredit > 0 OR VC1.MonthDebit>0)