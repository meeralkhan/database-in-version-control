﻿
CREATE   VIEW [dbo].[v_ClassifyOD2]
AS
select a.OurBranchID, a.AccountID, COUNT(a.OurBranchID) Counts, ISNULL(b.EA2Days, 30) as [Days]
from v_CalculateExcessOD a
inner join t_Account ab on a.OurBranchID = ab.OurBranchID and a.AccountID = ab.AccountID and ISNULL(ab.DisableAutoClassification,0) = 0
inner join t_Products b on a.OurBranchID = b.OurBranchID and a.ProductID = b.ProductID and ISNULL(b.ProductType,'') = 'AE' and ISNULL(b.SegmentType,'') = 'Corporate'
where a.AccrualDate >= (select WorkingDate-(ISNULL(b.EA2Days, 30)) from t_Last where OurBranchID = a.OurBranchID)
and a.AccountID IN (select AccountID from t_Account where ISNULL(OverdraftClassID,'') not in ('004','005','006'))
group by a.OurBranchID, a.AccountID, b.EA2Days