﻿CREATE VIEW [dbo].[v_MMSecuritySaleAmount]
AS
SELECT ISNULL(SUM(S_Amount), 0) AS S_Amount, P_DealNo--,IsPledge
FROM t_MMSecuritySaleHistory
WHERE [Status] <> 'R'
GROUP BY P_DealNo--,IsPledge