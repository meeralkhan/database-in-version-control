﻿create view [dbo].[vc_WHTaxAmounts2]  
  
AS  
  
select OurBranchID, ScrollNo, RefNo, AccountID, AccountName, wDate, ValueDate, ChequeID, ChequeDate, Amount,   
DescriptionID, Description, DocType, AppWHTax, SourceBranch, Status, WHTaxAmount, WHTaxMode, WHTScrollNo,  
'Y' AS T1, 'Y' AS T2  
from t_Transactions  
where Status <> 'R'  
and AppWHTax = 1