﻿create view [dbo].[VCI_GetAccountDetails]            
AS            
            
 select ab.OurBranchID, a.ClientID, p.ProductID, p.Description as ProductDesc, c.CurrencyID, c.MeanRate,   
 b.BankID, b.BranchID, (b.BranchID + ':' + b.Name + ':' + isnull(convert(nvarchar(100), b.Address),'')) as Branch,  
 ab.AccountID, a.IBAN, a.Name as AccountTitle, c.CurrencyID + ':' + c.Description as Currency,             
 AvailableBalance = (ab.ClearBalance + ab.Limit - ab.FreezeAmount - p.MinBalance),           
 CurrentBalance = (ab.ClearBalance + ab.Limit - ab.FreezeAmount - p.MinBalance + ab.Effects),          
 ab.LocalClearBalance EquivalentBalance            
 from t_AccountBalance ab             
 INNER JOIN t_Account a ON ab.OurBranchID = a.OurBranchID AND ab.AccountID = a.AccountID            
 INNER JOIN t_Products p ON ab.OurBranchID = p.OurBranchID AND ab.ProductID = p.ProductID            
 INNER JOIN t_Currencies c ON ab.OurBranchID = c.OurBranchID AND a.CurrencyID = c.CurrencyID            
 INNER JOIN t_Branches b ON ab.OurBranchID = b.OurBranchID AND ab.OurBranchID = b.BranchID