﻿CREATE VIEW [dbo].[vc_GetDeposits]
AS    
    
 SELECT t.OurBranchID, t.RecieptID, t.ProductID, p.Description, p.CurrencyID, t.AccountID, a.Name, t.Amount,     
 t.IssueDate, t.StartDate, t.MaturityDate, t.Rate,     
 CAST(p.PeriodsInFigure as nvarchar(10)) + '-' +     
  Case p.Period     
   when 'Y' then 'Year'    
   when 'M' then 'Month'    
   when 'D' then 'Day'    
  end     
 as Period,    
 t.AutoProfit, t.PaymentPeriod, t.Treatment, t.AutoRollOver, t.RefNo, isnull(t.IsUnderLien,0) IsUnderLien,     
 isnull(t.IsLost,0) IsLost, isnull(t.LostRemarks,'') LostRemarks, t.Interest, t.InterestPaid,     
 isnull(t.InterestPaidUpTo,'') InterestPaidUpTo, 0 as Tax, isnull(t.IsClosed,0) IsClosed, isnull(t.CloseDate,'') CloseDate,    
 Status = case     
  when isnull(t.IsClosed,0) = 1 then 'Closed'     
  when isnull(t.IsLost,0) = 1 then 'Lost'    
  when isnull(t.IsUnderLien,0) = 1 then 'Lien'    
  else    
   'Open'    
 end    
 FROM t_TDRIssuance t     
 INNER JOIN t_Products p ON t.OurBranchID = p.OurBranchID AND t.ProductID = p.ProductID         
 INNER JOIN t_Account a ON t.OurBranchID = a.OurBranchID AND t.AccountID = a.AccountID