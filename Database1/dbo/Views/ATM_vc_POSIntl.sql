﻿
CREATE   VIEW [dbo].[ATM_vc_POSIntl]
AS
-- select * from ATM_vc_POSIntl
-- WHERE OurBranchID = '02' AND 
-- AND convert(varchar(10), wDate, 120) >= '2021-03-01' AND convert(varchar(10), wDate, 120) <= '2021-03-31' 
-- order by wDate asc
SELECT wDate, OurBranchID, PHXDate, AccountID, Left(RefNo, 16) as PAN, STAN, RetRefNum, VISATrxID, RefNo, PHXDate as TrxDate, ReleaseDate as ClearingDate, Amount, ConvRate, MerchantType, 
AcqCountryCode, ForwardInstID, CurrCodeTran, CurrCodeSett, SettlmntAmount, 
Case 
	when HoldStatus = 'R' then 'Full Reversal'
	when HoldStatus = 'H' then 'Hold'
	when HoldStatus = 'C' then 'Cleared'
	when HoldStatus = 'P' then 'Partial Reversal'
end as TrxStatus, 
Case 
	when LEFT(ProcCode,2) = '00' AND LEFT(POSEntryMode,2) = '01' then 'ECOM Trx'
	when LEFT(ProcCode,2) = '00' then 'POS Trx'
	when LEFT(ProcCode,2) = '01' then 'Cash Withdrawal Trx'
	when LEFT(ProcCode,2) = '31' then 'Balance Enquiry Trx'
	when LEFT(ProcCode,2) = '23' then 'Credit Trx'
	when LEFT(ProcCode,2) = '26' then 'OCT Trx'
	when LEFT(ProcCode,2) = '20' then 'Credit Vouch/ Merch Return Trx'
	when ProcCode = 'CANCL CHG' then 'Card Replacement [CANCELLED]'
	when ProcCode = 'LT CHG' then 'Card Replacement [LOST]'
	when ProcCode = 'STLN CHG' then 'Card Replacement [STOLEN]'
	when ProcCode = 'DMG CHG' then 'Card Replacement [DAMAGED]'
	when ProcCode = 'LT DELVRY CHG' then 'Card Replacement [LOST IN DELIVERY]'
	when ProcCode = 'REP CHG' then 'Card Replacement [NORMAL]'
end as TranDetails, DescriptionID, [Description], CardAccptNameLoc
FROM t_ATM_HoldTrxs WHERE ISNULL(LEFT(ProcCode,2),'') = '00' AND ISNULL(LEFT(POSEntryMode,2),'') <> '01' 
AND CurrCodeTran <> '784' AND CurrCodeSett = '784'