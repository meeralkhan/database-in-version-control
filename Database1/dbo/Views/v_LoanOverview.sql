﻿
CREATE   view [dbo].[v_LoanOverview]
as
select a.OurBranchID,a.ReferenceNo FacilityID,a.ClientID,a.CreditProgramID,c.CreditProgramName,a.DateReview,a.DateExpiry,a.LimitSecType,a.LimitType FLimitType,
count(distinct d.FinanceProductType) NoOfLoanProducts,isnull(a.Limit,0) ApprovedLimit,sum(isnull(b.Limit,0)) UtilizedLimit,a.Limit - sum(isnull(b.Limit,0)) AvailableLimit,
case a.Status when 'C' then 'Closed' when 'B' then 'Blocked' when '' then 'Active' end FStatus,  d.ProductID,d.Description ProductName,d.FinanceProductType ProductType,
d.CurrencyID CurrencyID, b.LimitType PLimitType, 
case b.Status when 'C' then 'Closed' when 'B' then 'Blocked' when '' then 'Active' end PStatus,isnull(b.Limit,0) PApprovedLimit,
SUM(case when isnull(f.ClearBalance,0) >= 0 then 0 when isnull(f.ClearBalance,0) < 0 then abs(isnull(f.ClearBalance,0)) end) PUtilizedLimit, 
b.Limit - SUM(case when isnull(f.ClearBalance,0) >= 0 then 0 when isnull(f.ClearBalance,0) < 0 then abs(isnull(f.ClearBalance,0)) end) PAvailableLimit
from t_LinkCIFCollateral a
inner join t_FacilityProduct b on a.OurBranchID = b.OurBranchID and a.ClientID = b.ClientID and a.SerialNo = b.FacilityID
left join t_CreditPrograms c on a.CreditProgramID = c.CreditProgramID and a.OurBranchID = c.OurBranchID
inner join t_Products d on b.ProductID = d.ProductID and a.OurBranchID = d.OurBranchID
inner join t_Account e on e.ClientID = b.ClientID and a.OurBranchID = e.OurBranchID and b.ProductID = e.ProductID
inner join t_AccountBalance f on a.OurBranchID = e.OurBranchID AND e.AccountID = f.AccountID
group by a.OurBranchID,a.ReferenceNo,a.ClientID,a.CreditProgramID,c.CreditProgramName,a.DateReview,a.DateExpiry,a.LimitSecType,a.LimitType,b.Limit,a.Limit,a.Status,
d.ProductID,d.Description,d.FinanceProductType, d.CurrencyID, b.LimitType, b.Status,isnull(b.Limit,0)