﻿CREATE view [dbo].[v_EOYTransactions]    
as    
select a.*, b.Balance, b.ForeignBalance,c.RetainedEarningGL from    
(select OurBranchID, AccountID, CurrencyID,ValueDate,VoucherID,    
SUM(case IsCredit when 1 then Amount else Amount*-1 end) LCYTrxBalance,    
SUM(case IsCredit when 1 then ForeignAmount else ForeignAmount*-1 end) FCYTrxBalance    
from t_GLTransactions where IsYearEndProcess = 0 AND    
Status != 'R' group by OurBranchID, AccountID, CurrencyID,valuedate,VoucherID)    
a inner join t_GL b on a.OurBranchID = b.OurBranchID and a.AccountID = b.AccountID and b.AccountType IN ('I','E')    
inner join t_Currencies c on c.OurBranchID = a.OurBranchID and c.CurrencyID = a.CurrencyID