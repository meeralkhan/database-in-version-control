﻿create view [dbo].[v_CustomerAddress]  
AS  
  
select c.OurBranchID, c.ClientID, c.Name,   
CASE c.CategoryId when 'Individual' then ISNULL(i1.Name,'') else ISNULL(c1.Name,'') end AS CountryName,  
CASE c.CategoryId when 'Individual' then ISNULL(i2.StateName,'') else ISNULL(c2.StateName,'') end AS StateName,  
CASE c.CategoryId when 'Individual' then ISNULL(i3.Name,'') else ISNULL(c3.Name,'') end AS CityName,  
CASE c.CategoryId when 'Individual' then ISNULL(c.AddressCurr,'') else ISNULL(c.RegisteredAddressCorp,'') end AS [Address]  
  
from t_Customer c  
  
left join t_Country i1 on c.OurBranchID = i1.OurBranchID and c.CountryCurr = i1.CountryID  
left join t_Country c1 on c.OurBranchID = c1.OurBranchID and c.RegAddCountryCorp = c1.CountryID  
  
left join t_State i2 on c.OurBranchID = i2.OurBranchID and c.CountryCurr = i2.CountryID and c.StateCurr = i2.StateID  
left join t_State c2 on c.OurBranchID = c2.OurBranchID and c.RegAddCountryCorp = c2.CountryID and c.RegAddStateCorp = c2.StateID  
  
left join t_City i3 on c.OurBranchID = i3.OurBranchID and c.CountryCurr = i3.CountryID and c.StateCurr = i3.StateID and c.CityCurr = i3.CityID  
left join t_City c3 on c.OurBranchID = c3.OurBranchID and c.RegAddCountryCorp = c3.CountryID and c.RegAddStateCorp = c3.StateID and c.RegAddCityCorp = c3.CityID