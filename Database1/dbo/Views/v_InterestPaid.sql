﻿
CREATE   VIEW [dbo].[v_InterestPaid]
as
select OurBranchID, AccountID, ProductID, CurrencyID, SUM(Amount) InterestPaid
from t_Transactions
where ProductID = 'ZXRACIN'
and DescriptionID <> 'A01'
group by OurBranchID, AccountID, ProductID, CurrencyID