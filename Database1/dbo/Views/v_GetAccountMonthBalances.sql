﻿create view [dbo].[v_GetAccountMonthBalances]
AS
select * from t_AccountMonthBalances
where [Month] IN (select MAX([Month]) from t_AccountMonthBalances)